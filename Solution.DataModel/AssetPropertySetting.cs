namespace iDAS.DataModel
{
    using System;

    public partial class AssetPropertySetting
    {
        public Guid Id { get; set; }

        public Guid? AssetId { get; set; }

        public Guid? PropertyId { get; set; }

        public string Value { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
