namespace iDAS.DataModel
{
    using System;

    public partial class QualityAuditProgramTerm
    {
        public Guid Id { get; set; }

        public Guid? QualityAuditProgramId { get; set; }

        public Guid? ISOTermId { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
