namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanProfileWorkExperience
    {
        public Guid Id { get; set; }

        public Guid? HumanEmployeeId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Position { get; set; }

        public string JobDescription { get; set; }

        public string Department { get; set; }

        public string PlaceOfWork { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
