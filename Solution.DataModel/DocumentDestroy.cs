namespace iDAS.DataModel
{
    using System;

    public partial class DocumentDestroy
    {
        public Guid Id { get; set; }

        public Guid? SuggestId { get; set; }

        public Guid? DocumentId { get; set; }

        public string DestroyType { get; set; }

        public int? Quantity { get; set; }

        public DateTime? DestroyAt { get; set; }

        public bool? IsDestroy { get; set; }

        public string Note { get; set; }

        public int? SuggestQuantity { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
