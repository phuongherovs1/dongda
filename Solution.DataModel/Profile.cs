namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Profile
    {
        public Guid Id { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? CategoryId { get; set; }

        public Guid? SecurityId { get; set; }

        public Guid? DepartmentArchiveId { get; set; }

        public Guid? EmployeeArchiveId { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public DateTime? TimeOpen { get; set; }

        public DateTime? TimeClose { get; set; }

        public string Code { get; set; }

        public int? Status { get; set; }

        public DateTime? ProfileCreateAt { get; set; }

        public string ProfileContent { get; set; }

        public DateTime? ArchiveDate { get; set; }

        public DateTime? ArchiveDeadline { get; set; }

        public int? ProfileNumber { get; set; }

        public int? ArchiveTime { get; set; }

        public int? FormalitySave { get; set; }

        public string ProfileBox { get; set; }

        public int? TypeDate { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        [Column(TypeName = "ntext")]
        public string ContentHtml { get; set; }
    }
}
