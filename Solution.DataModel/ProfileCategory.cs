namespace iDAS.DataModel
{
    using System;

    public partial class ProfileCategory
    {
        public Guid Id { get; set; }

        public Guid? ParentId { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? TransferDepartmentId { get; set; }

        public string Name { get; set; }

        public string CodeRule { get; set; }

        public string Content { get; set; }

        public bool? IsArchivePerson { get; set; }

        public bool? IsArchivePart { get; set; }

        public bool? IsTransferArchive { get; set; }

        public int? ArchiveTime1 { get; set; }

        public int? Times1 { get; set; }

        public int? ArchiveTime2 { get; set; }

        public int? Times2 { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
