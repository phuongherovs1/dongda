namespace iDAS.DataModel
{
    using System;

    public partial class Document
    {
        public Guid Id { get; set; }

        public Guid? ParentId { get; set; }

        public Guid? CategoryId { get; set; }

        public Guid? DepartmentId { get; set; }

        public string DistributeCategoryIds { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string ShortContent { get; set; }

        public Guid? SecurityId { get; set; }

        public string Version { get; set; }

        public string Revision { get; set; }

        public bool? IsSoft { get; set; }

        public bool? IsHard { get; set; }

        public bool? IsNew { get; set; }

        public bool? IsExternal { get; set; }

        public bool? IsWaitWrite { get; set; }

        public bool? IsWrite { get; set; }

        public bool? IsWaitReview { get; set; }

        public bool? IsReviewApprove { get; set; }

        public bool? IsReviewAccept { get; set; }

        public bool? IsWaitPublish { get; set; }

        public bool? IsPublishApprove { get; set; }

        public bool? IsPublishAccept { get; set; }

        public string PublishFrom { get; set; }

        public bool? IsPublish { get; set; }

        public DateTime? PublishAt { get; set; }

        public DateTime? ApplyAt { get; set; }

        public bool? IsApply { get; set; }

        public bool? IsAccept { get; set; }

        public bool? IsExpire { get; set; }

        public DateTime? ExpireAt { get; set; }

        public bool? IsObsolate { get; set; }

        public DateTime? ObsolateAt { get; set; }

        public bool? IsDestroy { get; set; }

        public DateTime? DestroyAt { get; set; }

        public Guid? WriteBy { get; set; }

        public DateTime? WriteAt { get; set; }

        public string Note { get; set; }

        public string NoteCategory { get; set; }

        public bool? IsChangeList { get; set; }

        public bool? IsLock { get; set; }

        public bool? IsExist { get; set; }

        public bool? IsSuggestWite { get; set; }

        public bool? IsRequestWrite { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
