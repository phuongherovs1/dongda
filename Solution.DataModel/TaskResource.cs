namespace iDAS.DataModel
{
    using System;

    public partial class TaskResource
    {
        public Guid Id { get; set; }

        public Guid? TaskId { get; set; }

        public Guid? EmployeeId { get; set; }

        public bool? IsRoleCreate { get; set; }

        public bool? IsRoleMark { get; set; }

        public bool? IsRoleAssign { get; set; }

        public bool? IsRolePerform { get; set; }

        public bool? IsRoleReview { get; set; }

        public bool? IsRoleView { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
