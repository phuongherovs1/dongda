namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Dispatchs")]
    public partial class Dispatch
    {
        public Guid Id { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? CategoryId { get; set; }

        public Guid? SecurityId { get; set; }

        public Guid? SendmethodId { get; set; }

        public Guid? UrgencyId { get; set; }

        public DateTime Sentdate { get; set; }

        public DateTime? Receiveddate { get; set; }

        public string Name { get; set; }

        public bool? IsGo { get; set; }

        public string NumberDispatch { get; set; }

        public string Content { get; set; }

        public string Note { get; set; }

        public string Recipients { get; set; }

        public string Placedelivery { get; set; }

        public string Numberto { get; set; }

        public int? Status { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
