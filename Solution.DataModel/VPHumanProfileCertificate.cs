namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanProfileCertificate
    {
        public Guid Id { get; set; }

        public Guid? HumanEmployeeId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Level { get; set; }

        public string PlaceOfTraining { get; set; }

        public string CertificateType { get; set; }

        public DateTime? DateIssuance { get; set; }

        public DateTime? DateExpiration { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
