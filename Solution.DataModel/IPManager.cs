namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class IPManager
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(20)]
        public string dia_chi_ip { get; set; }

        public string muc_dich_su_dung { get; set; }

        public DateTime? ngay_cap { get; set; }

        public string so_cong_van { get; set; }

        public DateTime? ngay_ban_hanh { get; set; }

        public string ma_to_chuc { get; set; }

        public int? CountInfection { get; set; }

        public string ten_to_chuc { get; set; }

        public string to_chuc_dau_moi { get; set; }

        public int? thanh_vien_mang_luoi { get; set; }

        public string website { get; set; }

        public string email_dau_moi1 { get; set; }

        public string email_dau_moi2 { get; set; }

        public string fingerprint { get; set; }

        public string ten_nguoi_dau_moi_ghc { get; set; }

        public string bo_phan_ghc { get; set; }

        public string dt_co_dinh_ghc { get; set; }

        public string dt_di_dong_ghc { get; set; }

        public string fax { get; set; }

        public string ten_nguoi_dau_moi_nghc { get; set; }

        public string bo_phan_nghc { get; set; }

        public string dt_co_dinh_nghc { get; set; }

        public string dt_di_dong_nghc { get; set; }

        public string ten_lanh_dao { get; set; }

        public string chuc_vu_lanh_dao { get; set; }

        public string dtcd_lanh_dao { get; set; }

        public string dtdd_lanh_dao { get; set; }

        public string fax_lanh_dao { get; set; }

        public string ten_nhan_cv { get; set; }

        public string chuc_vu_cv { get; set; }

        public string dia_chi_nhan_cv { get; set; }

        public string dien_thoai_nhan_cv { get; set; }

        public string yahoo { get; set; }

        public string skype { get; set; }

        public string them_thong_tin { get; set; }

        public string gtalk { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
