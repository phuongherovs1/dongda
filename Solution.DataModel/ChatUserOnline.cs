namespace iDAS.DataModel
{
    using System;

    public partial class ChatUserOnline
    {
        public Guid Id { get; set; }

        public Guid? UserOnlineId { get; set; }

        public string UserName { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? ReadAt { get; set; }
    }
}
