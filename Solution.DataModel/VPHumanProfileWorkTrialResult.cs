namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanProfileWorkTrialResult
    {
        public Guid Id { get; set; }

        public Guid? HumanProfileWorkTrialId { get; set; }

        public string CriteriaName { get; set; }

        public int? EmployeePoint { get; set; }

        public int? ManagerPoint { get; set; }

        public DateTime? CreateAt { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public string Note { get; set; }
    }
}
