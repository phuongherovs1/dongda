namespace iDAS.DataModel
{
    using System;

    public partial class EmployeeJobTitle
    {
        public Guid Id { get; set; }

        public Guid HumanEmployeeId { get; set; }

        public Guid JobTitleId { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
