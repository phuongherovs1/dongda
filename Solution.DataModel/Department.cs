namespace iDAS.DataModel
{
    using System;

    public partial class Department
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public Guid? ParentId { get; set; }

        public int? Level { get; set; }

        public string Functions { get; set; }

        public string Task { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
