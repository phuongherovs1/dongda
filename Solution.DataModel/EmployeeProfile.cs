namespace iDAS.DataModel
{
    using System;

    public partial class EmployeeProfile
    {
        public Guid Id { get; set; }

        public Guid? EmployeeId { get; set; }

        public string ClaimType { get; set; }

        public string ClaimValue { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
