namespace iDAS.DataModel
{
    using System;

    public partial class RiskTargetPlan
    {
        public Guid Id { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? RiskTargetId { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public DateTime? StartAtPlan { get; set; }

        public DateTime? EndAtPlan { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
