namespace iDAS.DataModel
{
    using System;

    public partial class DocumentReference
    {
        public Guid Id { get; set; }

        public Guid? DocumentId { get; set; }

        public Guid? ReferenceId { get; set; }

        public string ReferenceName { get; set; }

        public string ReferenceCode { get; set; }

        public Guid? ReferenceType { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
