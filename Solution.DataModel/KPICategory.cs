namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("KPICategorys")]
    public partial class KPICategory
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public string Description { get; set; }

        public Guid? ParrentId { get; set; }

        public int? Maxscore { get; set; }

        public int? Level { get; set; }

        public int? Weightscore { get; set; }

        public int? Type { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
