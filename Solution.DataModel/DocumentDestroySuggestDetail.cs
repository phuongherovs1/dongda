namespace iDAS.DataModel
{
    using System;

    public partial class DocumentDestroySuggestDetail
    {
        public Guid Id { get; set; }

        public Guid? DestroySuggestId { get; set; }

        public Guid? DocumentId { get; set; }

        public string Reason { get; set; }

        public bool? IsDestroy { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
