namespace iDAS.DataModel
{
    using System;

    public partial class RiskIncidentHandler
    {
        public Guid Id { get; set; }

        public Guid? RiskIncidentId { get; set; }

        public string ContentHandler { get; set; }

        public Guid? DepartmentId { get; set; }

        public decimal? Rate { get; set; }

        public DateTime? DateFinish { get; set; }

        public string ProofDocument { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
