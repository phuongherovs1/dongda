namespace iDAS.DataModel
{
    using System;

    public partial class EmployeeTitle
    {
        public Guid Id { get; set; }

        public Guid TitleId { get; set; }

        public Guid EmployeeId { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
        public Guid? ProfileWorkExperienceId { get; set; }
    }
}
