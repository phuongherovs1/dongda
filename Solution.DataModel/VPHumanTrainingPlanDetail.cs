namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanTrainingPlanDetail
    {
        public Guid Id { get; set; }

        public Guid? VPHumanTrainingPlanId { get; set; }

        public string Contents { get; set; }

        public int? Number { get; set; }

        public int? StatusRequest { get; set; }

        public string Reason { get; set; }

        public bool? Type { get; set; }

        public decimal? ExpectedCost { get; set; }

        public string Certificate { get; set; }

        public string Note { get; set; }

        public bool? IsCommit { get; set; }

        public string CommitContent { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public string Address { get; set; }

        public bool? IsCancel { get; set; }

        public string ReasonCancel { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
