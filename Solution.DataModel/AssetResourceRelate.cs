namespace iDAS.DataModel
{
    using System;

    public partial class AssetResourceRelate
    {
        public Guid Id { get; set; }

        public Guid? AssetId { get; set; }

        public Guid? EmployeeId { get; set; }

        public Guid? DepartmentId { get; set; }

        public bool? IsUsing { get; set; }

        public Guid? RoleId { get; set; }

        public bool? IsUse { get; set; }

        public bool? IsManage { get; set; }

        public bool? IsOwner { get; set; }

        public string Other { get; set; }

        public string ProcessExpediency { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
