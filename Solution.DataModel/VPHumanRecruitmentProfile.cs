namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanRecruitmentProfile
    {
        public Guid Id { get; set; }

        public Guid? EmployeeId { get; set; }

        public Guid? FileId { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public byte[] Avatar { get; set; }

        public string Address { get; set; }

        public DateTime? Birthday { get; set; }

        public bool? Gender { get; set; }

        public string Nationality { get; set; }

        public string People { get; set; }

        public string Religion { get; set; }

        public string PlaceOfBirth { get; set; }

        public string NumberOfIdentityCard { get; set; }

        public DateTime? DateIssueOfIdentityCard { get; set; }

        public string PlaceIssueOfIdentityCard { get; set; }

        public string HomePhone { get; set; }

        public string PlaceOfTranning { get; set; }

        public string Specicalization { get; set; }

        public string LevelOfComputerization { get; set; }

        public string ForeignLanguage { get; set; }

        public string Literacy { get; set; }

        public string Qualifications { get; set; }

        public string ListOfCertificates { get; set; }

        public string Experience { get; set; }

        public decimal? Salary { get; set; }

        public short? YearsOfExperience { get; set; }

        public bool? IsEmployee { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
