namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class CalendarWeekShift
    {
        public Guid Id { get; set; }

        public Guid CalendarId { get; set; }

        public int? DayType { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public TimeSpan? StartTime { get; set; }

        public TimeSpan? EndTime { get; set; }

        public Guid? CopyFrom { get; set; }

        public bool? IsOverride { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? RowId { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
