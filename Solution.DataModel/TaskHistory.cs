namespace iDAS.DataModel
{
    using System;

    public partial class TaskHistory
    {
        public Guid Id { get; set; }

        public Guid? TaskId { get; set; }

        public byte[] Data { get; set; }

        public DateTime? Date { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
