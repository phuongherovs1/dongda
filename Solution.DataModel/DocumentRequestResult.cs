namespace iDAS.DataModel
{
    using System;

    public partial class DocumentRequestResult
    {
        public Guid Id { get; set; }

        public Guid? DocumentRequestId { get; set; }

        public string Name { get; set; }

        public string Content { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public Guid? PerformBy { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
