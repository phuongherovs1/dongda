namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanProfileInsurance
    {
        public Guid Id { get; set; }

        public Guid? HumanEmployeeId { get; set; }

        public string Number { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Type { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public decimal? InSuranceNorms { get; set; }
    }
}
