namespace iDAS.DataModel
{
    using System;

    public partial class ProfileDestroyReport
    {
        public Guid Id { get; set; }

        public Guid? ProfileDestroySuggestId { get; set; }

        public bool? IsComplete { get; set; }

        public DateTime? DestroyAt { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? ReportAt { get; set; }
        public Guid ProfileTypeDestroyId { get; set; }

    }
}
