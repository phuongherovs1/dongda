namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanRecruitmentPlanRequirement
    {
        public Guid Id { get; set; }

        public Guid? HumanRecruitmentRequirementId { get; set; }

        public Guid? HumanRecruitmentPlanId { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
