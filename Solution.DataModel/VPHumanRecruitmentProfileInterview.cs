namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanRecruitmentProfileInterview
    {
        public Guid Id { get; set; }

        public Guid? HumanRecruitmentProfileId { get; set; }

        public Guid? HumanRecruitmentPlanId { get; set; }

        public Guid? HumanRecruitmentRequirementId { get; set; }

        public bool? IsEdit { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
