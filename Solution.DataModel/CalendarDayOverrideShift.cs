namespace iDAS.DataModel
{
    using System;

    public partial class CalendarDayOverrideShift
    {
        public Guid Id { get; set; }

        public Guid? CalendarDayOverrideId { get; set; }

        public TimeSpan? StartTime { get; set; }

        public TimeSpan? EndTime { get; set; }

        public bool? IsOverTime { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CopyFrom { get; set; }

        public bool? IsOverride { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
