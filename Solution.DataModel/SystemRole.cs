namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class SystemRole
    {
        public Guid Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public string Module { get; set; }

        public string Code { get; set; }

        public string Descriptsion { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
