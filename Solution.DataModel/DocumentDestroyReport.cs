namespace iDAS.DataModel
{
    using System;

    public partial class DocumentDestroyReport
    {
        public Guid Id { get; set; }

        public Guid? DestroySuggestId { get; set; }

        public Guid? RequestId { get; set; }

        public DateTime? DestroyAt { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
