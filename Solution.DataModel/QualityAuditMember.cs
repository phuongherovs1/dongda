namespace iDAS.DataModel
{
    using System;

    public partial class QualityAuditMember
    {
        public Guid Id { get; set; }

        public Guid? QualityAuditPlanDetailId { get; set; }

        public Guid? EmployeeId { get; set; }

        public int? RoleType { get; set; }

        public bool? IsNotAvailable { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
