namespace iDAS.DataModel
{
    using System;

    public partial class QualityNCRelation
    {
        public Guid Id { get; set; }

        public Guid? QualityNCId { get; set; }

        public Guid? EmployeeId { get; set; }

        public Guid? DepartmentTitleId { get; set; }

        public Guid? DepartmentId { get; set; }

        public int? Flag { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
