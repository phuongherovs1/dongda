namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class ChatMessenger
    {
        public Guid Id { get; set; }

        public Guid? To { get; set; }

        [Column(TypeName = "ntext")]
        public string Content { get; set; }

        public bool? IsFromDelete { get; set; }

        public bool? IsToDelete { get; set; }

        public bool? IsRead { get; set; }

        public DateTime? ReadAt { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
