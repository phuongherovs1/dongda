namespace iDAS.DataModel
{
    using System;

    public partial class AssetReview
    {
        public Guid Id { get; set; }

        public Guid? AssetResourceRelateId { get; set; }

        public DateTime? DateOfReview { get; set; }

        public Guid? EmployeeId { get; set; }

        public string Other { get; set; }

        public int? ValueOfSecurity { get; set; }

        public int? ValueOfInviblate { get; set; }

        public int? ValueOfReady { get; set; }

        public int? Value { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
