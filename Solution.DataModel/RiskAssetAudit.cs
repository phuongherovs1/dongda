namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class RiskAssetAudit
    {
        public Guid Id { get; set; }

        public Guid? RiskPlanAuditId { get; set; }

        public Guid? AssetResourceRelateId { get; set; }

        public decimal? AssetValue { get; set; }

        [StringLength(500)]
        public string Weakness { get; set; }

        [StringLength(500)]
        public string CurrentCountermove { get; set; }

        [StringLength(500)]
        public string Hazard { get; set; }

        public decimal? ImpactLevel { get; set; }

        public decimal? Possible { get; set; }

        public decimal? RiskLevel { get; set; }

        public int? AcceptCriteria { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
