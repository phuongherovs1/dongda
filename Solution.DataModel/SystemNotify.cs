namespace iDAS.DataModel
{
    using System;

    public partial class SystemNotify
    {
        public Guid Id { get; set; }

        public Guid? EmployeeId { get; set; }

        public string Url { get; set; }

        public string Params { get; set; }

        public string Title { get; set; }

        public string Contents { get; set; }

        public int? Type { get; set; }

        public bool? IsRead { get; set; }

        public DateTime? DateSend { get; set; }

        public DateTime? ReadTime { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
