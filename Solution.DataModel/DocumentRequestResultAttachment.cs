namespace iDAS.DataModel
{
    using System;

    public partial class DocumentRequestResultAttachment
    {
        public Guid Id { get; set; }

        public Guid? ResultId { get; set; }

        public Guid? FileId { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
