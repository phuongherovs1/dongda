namespace iDAS.DataModel
{
    using System;

    public partial class QualityAuditPlanStandard
    {
        public Guid Id { get; set; }

        public Guid? QualityAuditPlanId { get; set; }

        public Guid? ISOStandardId { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
