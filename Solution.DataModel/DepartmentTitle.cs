namespace iDAS.DataModel
{
    using System;

    public partial class DepartmentTitle
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int? Order { get; set; }
        public int? Weightscore { get; set; }
        public string Note { get; set; }
        public Guid? DepartmentId { get; set; }
        public string Conditions { get; set; }
        public string Responsible { get; set; }
        public string Absenreplace { get; set; }
        public string Capacityrequire { get; set; }
        public string ReportTo { get; set; }
        public string Purpose { get; set; }
        public string WorkingConditions { get; set; }
        public string RelationshipIn { get; set; }
        public string RelationshipOut { get; set; }
        public Guid? AbsenreplaceId { get; set; }
        public Guid? ReportToId { get; set; }
        public string Power { get; set; }
        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
