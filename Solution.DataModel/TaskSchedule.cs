namespace iDAS.DataModel
{
    using System;

    public partial class TaskSchedule
    {
        public Guid Id { get; set; }

        public Guid? EmployeeId { get; set; }

        public string Name { get; set; }

        public string Content { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public bool? IsSent { get; set; }

        public bool? IsConfirm { get; set; }

        public bool? IsClosed { get; set; }

        public bool? IsPause { get; set; }

        public bool? IsCancel { get; set; }

        public string ReasonOfPause { get; set; }

        public string ReasonOfCancel { get; set; }

        public DateTime? ClosedAt { get; set; }

        public DateTime? PauseAt { get; set; }

        public DateTime? CancelAt { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
