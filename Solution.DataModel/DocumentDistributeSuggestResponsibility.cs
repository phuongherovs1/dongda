namespace iDAS.DataModel
{
    using System;

    public partial class DocumentDistributeSuggestResponsibility
    {
        public Guid Id { get; set; }

        public Guid? DocumentDistributeSuggestId { get; set; }

        public Guid? DepartmentId { get; set; }

        public int? RoleType { get; set; }

        public bool? IsNew { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }

        public bool? IsComplete { get; set; }

        public bool? IsLock { get; set; }

        public string ContentSuggest { get; set; }

        public string ApproveNote { get; set; }

        public Guid? PerformBy { get; set; }

        public DateTime? PerformAt { get; set; }

        public int? Order { get; set; }

        public Guid? SendTo { get; set; }

        public int? SenderRoleType { get; set; }

        public int? SenderOrder { get; set; }

        public bool? IsCancel { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
