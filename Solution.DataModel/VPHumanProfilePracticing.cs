namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanProfilePracticing
    {
        public Guid Id { get; set; }
        public Guid? EmployeeId { get; set; }
        public string NumberDecisions { get; set; }
        public string Professional { get; set; }
        public string Scope { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? DateAppoint { get; set; }
        public DateTime? CreateAt { get; set; }
        public Guid? CreateBy { get; set; }
        public DateTime? UpdateAt { get; set; }
        public Guid? UpdateBy { get; set; }
    }
}
