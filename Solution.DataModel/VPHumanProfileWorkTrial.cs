namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanProfileWorkTrial
    {
        public Guid Id { get; set; }

        public Guid? HumanEmployeeId { get; set; }

        public Guid? HumanRoleId { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public string Note { get; set; }

        public bool IsEdit { get; set; }

        public bool IsApproval { get; set; }

        public bool IsAccept { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public Guid? ApprovalBy { get; set; }

        public string ApprovalNote { get; set; }

        public int? DirectorApproval { get; set; }

        public DateTime? DirectorApprovalAt { get; set; }

        public int? ContractType { get; set; }

        public DateTime? ContractStartTime { get; set; }

        public Guid? ManagerId { get; set; }

        public bool? Status { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
