namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanRecruitmentCriteria
    {
        public Guid Id { get; set; }

        public Guid? HumanRoleId { get; set; }

        public string Name { get; set; }

        public string Content { get; set; }

        public bool? IsActive { get; set; }

        public int? MinPoint { get; set; }

        public int? MaxPoint { get; set; }

        public int? Factor { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
