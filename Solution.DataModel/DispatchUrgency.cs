namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("DispatchUrgencys")]
    public partial class DispatchUrgency
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool? IsUse { get; set; }

        public string Color { get; set; }

        public string Description { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
