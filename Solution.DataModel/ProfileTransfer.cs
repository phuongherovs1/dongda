namespace iDAS.DataModel
{
    using System;

    public partial class ProfileTransfer
    {
        public Guid Id { get; set; }

        public Guid? SendDepartmentId { get; set; }

        public Guid? SendEmployeeId { get; set; }

        public Guid? DepartmentArchiveId { get; set; }

        public Guid? EmployeeArchiveId { get; set; }

        public DateTime? DateReceive { get; set; }

        public DateTime? DateSend { get; set; }

        public int? SendResult { get; set; }

        public string Contents { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
