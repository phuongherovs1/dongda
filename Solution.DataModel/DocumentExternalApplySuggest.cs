namespace iDAS.DataModel
{
    using System;

    public partial class DocumentExternalApplySuggest
    {
        public Guid Id { get; set; }

        public Guid? ParentId { get; set; }

        public Guid? DocumentId { get; set; }

        public string SuggestContent { get; set; }

        public bool? IsSend { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsApply { get; set; }

        public int? RoleStatus { get; set; }

        public Guid? ApproveBy { get; set; }

        public string ApproveNote { get; set; }

        public DateTime? ApplyAt { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
