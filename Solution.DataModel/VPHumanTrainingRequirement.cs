namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanTrainingRequirement
    {
        public Guid Id { get; set; }

        public Guid? EmployeeId { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public string Contents { get; set; }

        public bool? IsEdit { get; set; }

        public bool? IsApproval { get; set; }

        public int? StatusRequest { get; set; }

        public bool? IsAccept { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public Guid? ApprovalBy { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
