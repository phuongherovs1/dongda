namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class Calendar
    {
        public Guid Id { get; set; }

        public Guid? ParentId { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public string Note { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDefault { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
