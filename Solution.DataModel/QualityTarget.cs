namespace iDAS.DataModel
{
    using System;

    public partial class QualityTarget
    {
        public Guid Id { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? QualityTargetIssueId { get; set; }

        public Guid? QualityUnitId { get; set; }

        public string TargetValueExpression { get; set; }

        public string Name { get; set; }

        public decimal? TargetValue { get; set; }

        public decimal? BaseValue { get; set; }

        public decimal? Scale { get; set; }

        public int? Destination { get; set; }

        public decimal? ResultValue { get; set; }

        public bool? IsScale { get; set; }

        public DateTime? ExpectAt { get; set; }

        public int? TargetType { get; set; }

        public string Note { get; set; }

        public int? Status { get; set; }

        public string Description { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
