namespace iDAS.DataModel
{
    using System;

    public partial class ProfileDestroyDetail
    {
        public Guid Id { get; set; }

        public Guid? ProfileDestroySuggestId { get; set; }

        public Guid? ProfileId { get; set; }

        public int? Status { get; set; }

        public bool? IsAccept { get; set; }

        public string Note { get; set; }

        public bool? IsDestroy { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public string RejectOfDestroy { get; set; }
    }
}
