namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class SystemUser
    {
        public Guid Id { get; set; }

        public bool? EmailConfirm { get; set; }

        public string PasswordHash { get; set; }

        public string SecurityStamp { get; set; }

        public bool? PhoneNumberConfirm { get; set; }

        public bool? TwoFactorEnabled { get; set; }

        public DateTime? LockoutEndDateUtc { get; set; }

        public bool? LockoutEnabled { get; set; }

        public int? AccessFailedCount { get; set; }

        public string ReasonOfLockout { get; set; }

        [StringLength(256)]
        public string UserName { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
