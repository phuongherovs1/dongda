namespace iDAS.DataModel
{
    using System;

    public partial class File
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public long? Size { get; set; }

        public string Type { get; set; }

        public string Extension { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
