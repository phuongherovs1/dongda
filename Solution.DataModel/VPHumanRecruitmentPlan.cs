namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanRecruitmentPlan
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal? TotalCost { get; set; }

        public string Content { get; set; }

        public bool? IsEdit { get; set; }

        public bool? IsApproval { get; set; }

        public bool? IsAccept { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public Guid? ApprovalBy { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
