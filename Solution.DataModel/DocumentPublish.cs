namespace iDAS.DataModel
{
    using System;

    public partial class DocumentPublish
    {
        public Guid Id { get; set; }

        public Guid? DocumentId { get; set; }

        public bool? IsPublish { get; set; }

        public Guid? PublishBy { get; set; }

        public DateTime? PublishAt { get; set; }

        public bool? IsObsolete { get; set; }

        public DateTime? ObsoleteAt { get; set; }

        public string ReasonOfObsolete { get; set; }

        public DateTime? ApplyAt { get; set; }

        public DateTime? ExpireAt { get; set; }

        public int? QuantityPublish { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
