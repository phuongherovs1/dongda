namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanRecruitmentPlanInterview
    {
        public Guid Id { get; set; }

        public Guid? HumanRecruitmentPlanId { get; set; }

        public string Name { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public string Content { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
