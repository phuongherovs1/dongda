namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class Employee
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }
        public string ExtensionNumber { get; set; }

        public byte[] Avatar { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public int? Sex { get; set; }
    }
}
