namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanRecruitmentReview
    {
        public Guid Id { get; set; }

        public Guid? HumanRecruitmentProfileId { get; set; }

        public Guid? HumanRecruitmentCriteriaId { get; set; }

        public int? Point { get; set; }

        public DateTime? Time { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
