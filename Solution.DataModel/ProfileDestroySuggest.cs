namespace iDAS.DataModel
{
    using System;

    public partial class ProfileDestroySuggest
    {
        public Guid Id { get; set; }

        public string ReasonOfReject { get; set; }

        public string ReasonOfSuggest { get; set; }

        public DateTime? SuggestAt { get; set; }

        public int? ProfileDestroyTotal { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }

        public bool? IsSend { get; set; }

        public int? Status { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public Guid? DepartmentId { get; set; }
    }
}
