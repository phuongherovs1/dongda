namespace iDAS.DataModel
{
    using System;

    public partial class ISOProcessStep
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid? ProcessId { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
