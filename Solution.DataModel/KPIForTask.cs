namespace iDAS.DataModel
{
    using System;

    public partial class KPIForTask
    {
        public Guid Id { get; set; }

        public Guid? TaskId { get; set; }

        public string EmployeeName { get; set; }

        public Guid? EmployeeId { get; set; }

        public int? Score { get; set; }

        public int? Weightscore { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
