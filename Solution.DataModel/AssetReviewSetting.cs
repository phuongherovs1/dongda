namespace iDAS.DataModel
{
    using System;

    public partial class AssetReviewSetting
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool? IsSecurity { get; set; }

        public bool? IsInviblate { get; set; }

        public bool? IsReady { get; set; }

        public bool? IsImportant { get; set; }

        public int? From { get; set; }

        public int? To { get; set; }

        public string Color { get; set; }

        public Guid? AssetTempSettingId { get; set; }

        public string Description { get; set; }

        public bool? IsUse { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
