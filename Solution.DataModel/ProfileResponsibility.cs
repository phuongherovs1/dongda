namespace iDAS.DataModel
{
    using System;

    public partial class ProfileResponsibility
    {
        public Guid Id { get; set; }

        public Guid? ProfileId { get; set; }

        public Guid? EmployeeId { get; set; }

        public int? RoleType { get; set; }

        public DateTime? ResponsibiltityAt { get; set; }

        public DateTime? SendAt { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public int? StatusType { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
