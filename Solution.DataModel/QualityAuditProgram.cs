namespace iDAS.DataModel
{
    using System;

    public partial class QualityAuditProgram
    {
        public Guid Id { get; set; }

        public Guid QualityAuditPlanDetailId { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public Guid? AuditBy { get; set; }

        public string BasicAudit { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
