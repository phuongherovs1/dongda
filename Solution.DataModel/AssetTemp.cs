namespace iDAS.DataModel
{
    using System;

    public partial class AssetTemp
    {
        public Guid Id { get; set; }

        public Guid? AssetId { get; set; }

        public Guid? TempSettingId { get; set; }

        public Guid? Paster { get; set; }

        public DateTime? DateOfPaste { get; set; }

        public string LabelTemp { get; set; }

        public bool? IsUse { get; set; }

        public bool? IsNew { get; set; }

        public Guid? TempFile { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
