namespace iDAS.DataModel
{
    using System;

    public partial class ProfileAttachment
    {
        public Guid Id { get; set; }

        public Guid? ProfileId { get; set; }

        public Guid? FileId { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
