namespace iDAS.DataModel
{
    using System;

    public partial class ChatGroupMember
    {
        public Guid Id { get; set; }

        public Guid? MemberId { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public Guid? GroupId { get; set; }
    }
}
