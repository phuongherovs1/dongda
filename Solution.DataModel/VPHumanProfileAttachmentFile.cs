namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanProfileAttachmentFile
    {
        public Guid Id { get; set; }

        public Guid? AttachmentFileId { get; set; }

        public Guid? HumanProfileAttachmentId { get; set; }

        public DateTime? CreateAt { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
