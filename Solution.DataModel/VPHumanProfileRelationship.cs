namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanProfileRelationship
    {
        public Guid Id { get; set; }

        public Guid? HumanEmployeeId { get; set; }

        public bool? IsPartnerRelationShip { get; set; }

        public string Name { get; set; }

        public short? Age { get; set; }

        public bool? IsMale { get; set; }

        public string Relationship { get; set; }

        public string Job { get; set; }

        public string PlaceOfJob { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public string Phone { get; set; }

        public string Adress { get; set; }
    }
}
