namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanProfileCuriculmViate
    {
        public Guid Id { get; set; }

        public Guid? HumanEmployeeId { get; set; }

        public string Aliases { get; set; }

        public string Nationality { get; set; }

        public string People { get; set; }

        public string Religion { get; set; }

        public bool? IsMale { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string PlaceOfBirthCity { get; set; }

        public string PlaceOfBirthDistrict { get; set; }

        public string PlaceOfBirthCommune { get; set; }

        public string HomeTownCity { get; set; }

        public string HomeTownDistrict { get; set; }

        public string HomeTownCommune { get; set; }

        public string PermanentResidence { get; set; }

        public string CurrentResidence { get; set; }

        public string OfficePhone { get; set; }

        public string HomePhone { get; set; }

        public string NumberOfIdentityCard { get; set; }

        public DateTime? DateIssueOfIdentityCard { get; set; }

        public string PlaceIssueOfIdentityCard { get; set; }

        public DateTime? DateOnGroup { get; set; }

        public string PositionGroup { get; set; }

        public string PlaceOfLoadedGroup { get; set; }

        public DateTime? DateJoinRevolution { get; set; }

        public DateTime? DateAtParty { get; set; }

        public DateTime? DateOfJoinParty { get; set; }

        public string PlaceOfLoadedParty { get; set; }

        public string PosititonParty { get; set; }

        public string NumberOfPartyCard { get; set; }

        public DateTime? DateOnArmy { get; set; }

        public string PositionArmy { get; set; }

        public string ArmyRank { get; set; }

        public string PoliticalTheory { get; set; }

        public string GovermentManagement { get; set; }

        public string Likes { get; set; }

        public string Forte { get; set; }

        public string Defect { get; set; }

        public string TaxCode { get; set; }

        public string NumberOfBankAccounts { get; set; }

        public string Bank { get; set; }

        public string NumberOfPassport { get; set; }

        public string PlaceOfPassport { get; set; }

        public DateTime? DateOfIssuePassport { get; set; }

        public DateTime? PassportExpirationDate { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? ProfileId { get; set; }
    }
}
