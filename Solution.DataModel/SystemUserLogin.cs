namespace iDAS.DataModel
{
    using System;

    public partial class SystemUserLogin
    {
        public Guid Id { get; set; }

        public Guid? SystemUserId { get; set; }

        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
