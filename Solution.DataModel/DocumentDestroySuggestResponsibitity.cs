namespace iDAS.DataModel
{
    using System;

    public partial class DocumentDestroySuggestResponsibitity
    {
        public Guid Id { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? DocumentDestroyId { get; set; }

        public int? RoleType { get; set; }

        public bool? IsNew { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }

        public bool? IsComplete { get; set; }

        public bool? IsLock { get; set; }

        public bool? IsArchiver { get; set; }

        public string ApproveNote { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public int? Order { get; set; }

        public bool? IsCancel { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
