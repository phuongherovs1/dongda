namespace iDAS.DataModel
{
    using System;

    public partial class DispatchArchive
    {
        public Guid Id { get; set; }

        public Guid? DispatchId { get; set; }

        public DateTime? ArchiveDate { get; set; }

        public int? FormalitySave { get; set; }

        public int? Type { get; set; }

        public int? ArchiveTime { get; set; }

        public string Note { get; set; }

        public DateTime? DeadlineForArchiving { get; set; }

        public Guid EmployeeArchiveId { get; set; }

        public Guid? DepartmentArchiveId { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
