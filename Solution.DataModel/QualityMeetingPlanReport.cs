namespace iDAS.DataModel
{
    using System;

    public partial class QualityMeetingPlanReport
    {
        public Guid Id { get; set; }

        public Guid? QualityMeetingPlanDetailId { get; set; }

        public Guid? DepartmentId { get; set; }

        public string Contents { get; set; }

        public string ReportContent { get; set; }

        public string ReportSuggest { get; set; }

        public Guid? Reporter { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
