namespace iDAS.DataModel
{
    using System;

    public partial class DocumentRetrive
    {
        public Guid Id { get; set; }

        public Guid? DocumentId { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? TitleId { get; set; }

        public Guid? EmployeeId { get; set; }

        public Guid? ExternalId { get; set; }

        public string Type { get; set; }

        public int? Quantity { get; set; }

        public Guid? ReturnBy { get; set; }

        public DateTime? ReturnAt { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
