namespace iDAS.DataModel
{
    using System;

    public partial class FormApproveStatus
    {
        public Guid Id { get; set; }

        public Guid FormId { get; set; }

        public string TableName { get; set; }

        public bool isFinal { get; set; }

        public int Status { get; set; }

        public string Reason { get; set; }

        public Guid? SentBy { get; set; }

        public Guid? ApprovedBy { get; set; }

        public DateTime? ApprovedAt { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
