namespace iDAS.DataModel
{
    using System;

    public partial class QualityMeetingPlan
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool? IsRoutine { get; set; }

        public int? UnitTime { get; set; }

        public int? MeetingValue { get; set; }

        public int? LastTime { get; set; }

        public Guid? ApproveBy { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
