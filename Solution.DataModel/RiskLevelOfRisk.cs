namespace iDAS.DataModel
{
    using System;

    public partial class RiskLevelOfRisk
    {
        public Guid Id { get; set; }

        public double? Value { get; set; }

        public string Description { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
