namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class RiskEventLog
    {
        public Guid Id { get; set; }

        public Guid? DepartmentId { get; set; }

        public DateTime? Date { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        public string SummaryEvent { get; set; }

        public string Info { get; set; }

        public string ProcessHandler { get; set; }

        public bool? IsIncident { get; set; }

        public string Reason { get; set; }

        [StringLength(50)]
        public string CodeIncident { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
