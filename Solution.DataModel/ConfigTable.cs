namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ConfigTable")]
    public partial class ConfigTable
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Configkey { get; set; }

        public string ConfigValue { get; set; }

        public string ConfigType { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
