namespace iDAS.DataModel
{
    using System;

    public partial class DocumentDestroySuggest
    {
        public Guid Id { get; set; }

        public string ReasonOfReject { get; set; }

        public string ReasonOfSuggest { get; set; }

        public DateTime? DestroyStartAt { get; set; }

        public DateTime? DestroyEstimateAt { get; set; }

        public string Type { get; set; }

        public int? StatusType { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
