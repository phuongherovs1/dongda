namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanProfileDiscipline
    {
        public Guid Id { get; set; }

        public Guid? HumanEmployeeId { get; set; }

        public string NumberOfDecision { get; set; }

        public DateTime? DateOfDecision { get; set; }

        public string Reason { get; set; }

        public string Form { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
