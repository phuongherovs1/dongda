namespace iDAS.DataModel
{
    using System;

    public partial class ProfileDepartmentPermission
    {
        public Guid Id { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? EmployeeId { get; set; }

        public Guid? DepartmentTitleId { get; set; }

        public bool? IsSettingCategory { get; set; }

        public bool? IsArchive { get; set; }

        public string Description { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
