namespace iDAS.DataModel
{
    using System;

    public partial class SmsManager
    {
        public Guid Id { get; set; }

        public string RecieveNumber { get; set; }

        public string SendNumber { get; set; }

        public string MessageContent { get; set; }

        public DateTime? SendDate { get; set; }

        public DateTime? SynchronizedDate { get; set; }

        public string NetworkProvider { get; set; }

        public bool? IsSynchronized { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
