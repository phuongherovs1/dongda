namespace iDAS.DataModel
{
    using System;

    public partial class ProfileBorrow
    {
        public Guid Id { get; set; }

        public Guid? BorrowEmployeeId { get; set; }

        public Guid? AssignEmployeeId { get; set; }

        public Guid? ProfileId { get; set; }

        public Guid? ProfileSuggestBorrowId { get; set; }

        public DateTime? BorrowDate { get; set; }

        public DateTime? ReturnEstimateDate { get; set; }

        public DateTime? ExtendDate { get; set; }

        public string ReasonExtend { get; set; }

        public DateTime? ReturnEstimateDateExtend { get; set; }

        public int? Status { get; set; }

        public DateTime? ReturnDate { get; set; }

        public bool? IsClose { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
