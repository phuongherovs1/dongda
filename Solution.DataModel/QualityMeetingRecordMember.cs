namespace iDAS.DataModel
{
    using System;

    public partial class QualityMeetingRecordMember
    {
        public Guid Id { get; set; }

        public Guid? QualityMeetingRecordId { get; set; }

        public Guid? QualityMeetingPlanDetailId { get; set; }

        public Guid? EmployeeId { get; set; }

        public Guid? DepartmentTitleId { get; set; }

        public bool? IsNotAvailable { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
