namespace iDAS.DataModel
{
    using System;

    public partial class ProfileAssign
    {
        public Guid Id { get; set; }

        public Guid? ProfileId { get; set; }

        public Guid? DepartmentId { get; set; }

        public string ContentRequire { get; set; }

        public string ContentOfSuggest { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public Guid? PerformBy { get; set; }

        public DateTime? AssignAt { get; set; }

        public Guid? AssignBy { get; set; }

        public DateTime? DateUpdateProfile { get; set; }

        public int? StatusType { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
