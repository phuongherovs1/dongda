namespace iDAS.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DocumentSuggestResponsibility
    {
        public Guid Id { get; set; }

        public Guid? DocumentSuggestId { get; set; }

        public int? RoleType { get; set; }

        public bool? IsNew { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }

        public bool? IsComplete { get; set; }

        public string ContentSuggest { get; set; }

        public string ApproveNote { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public int? Order { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
