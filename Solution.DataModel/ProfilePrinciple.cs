namespace iDAS.DataModel
{
    using System;

    public partial class ProfilePrinciple
    {
        public Guid Id { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? CategoryId { get; set; }

        public int? ArchiveType { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? DocumentPublishId { get; set; }

        public bool? IsUse { get; set; }

        public int? Order { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
