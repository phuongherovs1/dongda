namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("DispatchCategoriTos")]
    public partial class DispatchCategoriTo
    {
        public Guid Id { get; set; }

        public Guid? DispatchId { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? CategoryId { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
