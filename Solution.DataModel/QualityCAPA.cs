namespace iDAS.DataModel
{
    using System;

    public partial class QualityCAPA
    {
        public Guid Id { get; set; }

        public Guid? QualityNCId { get; set; }

        public string Solution { get; set; }

        public Guid? AssignBy { get; set; }

        public Guid? EmployeeId { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public int? Status { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
