namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("HistoryBotnet")]
    public partial class HistoryBotnet
    {
        public Guid Id { get; set; }

        public Guid IdHeadquarters { get; set; }

        public Guid? IdInfection { get; set; }

        public Guid? IdBotnet { get; set; }

        public DateTime? InfectionDate { get; set; }

        public long? Status { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
        public string Abbreviation { get; set; }
    }
}
