namespace iDAS.DataModel
{
    using System;

    public partial class QualityMeetingPlanReportIssue
    {
        public Guid Id { get; set; }

        public Guid? QualityIssueId { get; set; }

        public Guid? QualityMeetingPlanReportId { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
