namespace iDAS.DataModel
{
    using System;

    public partial class TaskSchedulePerform
    {
        public Guid Id { get; set; }

        public Guid? ScheduleId { get; set; }

        public Guid? EmployeeId { get; set; }

        public bool? IsSent { get; set; }

        public bool? IsAccept { get; set; }

        public bool? IsChange { get; set; }

        public bool? IsReject { get; set; }

        public bool? IsPause { get; set; }

        public bool? IsCancel { get; set; }

        public bool? IsEdit { get; set; }

        public string ReasonOfPause { get; set; }

        public string ReasonOfCancel { get; set; }

        public string Comment { get; set; }

        public string Feedback { get; set; }

        public DateTime? SentAt { get; set; }

        public DateTime? ConfirmAt { get; set; }

        public DateTime? PauseAt { get; set; }

        public DateTime? CancelAt { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
