namespace iDAS.DataModel
{
    using System;

    public partial class AssetTempSetting
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Rule { get; set; }

        public string Method { get; set; }

        public string Description { get; set; }

        public Guid? Template { get; set; }

        public bool? IsUse { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
