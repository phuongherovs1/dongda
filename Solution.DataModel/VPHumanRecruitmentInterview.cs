namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanRecruitmentInterview
    {
        public Guid Id { get; set; }

        public Guid? HumanRecruitmentProfileInterviewId { get; set; }

        public Guid? HumanRecruitmentPlanInterviewId { get; set; }

        public string Result { get; set; }

        public DateTime? Time { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
