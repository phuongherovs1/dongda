namespace iDAS.DataModel
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    public partial class SolutionEntities : DbContext
    {
        public SolutionEntities()
            : base("name=SolutionEntities")
        {
            var objectContext = (this as IObjectContextAdapter).ObjectContext;
            objectContext.CommandTimeout = 300000;
        }

        public virtual DbSet<ConfigTable> ConfigTables { get; set; }
        public virtual DbSet<BotnetInfection> BotnetInfections { get; set; }
        public virtual DbSet<IPManager> IPManagers { get; set; }
        public virtual DbSet<SmsKeywords> SmsKeywordss { get; set; }
        public virtual DbSet<SmsType> SmsTypes { get; set; }
        public virtual DbSet<SmsBlockList> SmsBlockLists { get; set; }
        public virtual DbSet<SmsManagersSynchronized> SmsManagersSynchronizeds { get; set; }
        public virtual DbSet<BotnetManager> BotnetManagers { get; set; }
        public virtual DbSet<SmsManager> SmsManagers { get; set; }
        public virtual DbSet<SmsKeywordCategories> SmsKeywordCategoriess { get; set; }
        public virtual DbSet<AssetGroup> AssetGroups { get; set; }
        public virtual DbSet<AssetProperty> AssetProperties { get; set; }
        public virtual DbSet<AssetPropertySetting> AssetPropertySettings { get; set; }
        public virtual DbSet<AssetResourceRelate> AssetResourceRelates { get; set; }
        public virtual DbSet<AssetReview> AssetReviews { get; set; }
        public virtual DbSet<AssetReviewSetting> AssetReviewSettings { get; set; }
        public virtual DbSet<Asset> Assets { get; set; }
        public virtual DbSet<AssetTemp> AssetTemps { get; set; }
        public virtual DbSet<AssetTempSetting> AssetTempSettings { get; set; }
        public virtual DbSet<AssetTypeProperty> AssetTypeProperties { get; set; }
        public virtual DbSet<AssetType> AssetTypes { get; set; }
        public virtual DbSet<CalendarDayOverride> CalendarDayOverrides { get; set; }
        public virtual DbSet<CalendarDayOverrideShift> CalendarDayOverrideShifts { get; set; }
        public virtual DbSet<Calendar> Calendars { get; set; }
        public virtual DbSet<CalendarWeekShift> CalendarWeekShifts { get; set; }
        public virtual DbSet<ChatGroupMember> ChatGroupMembers { get; set; }
        public virtual DbSet<ChatGroup> ChatGroups { get; set; }
        public virtual DbSet<ChatMessenger> ChatMessengers { get; set; }
        public virtual DbSet<ChatPublic> ChatPublics { get; set; }
        public virtual DbSet<ChatUserOnline> ChatUserOnlines { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<DepartmentTitle> DepartmentTitles { get; set; }
        public virtual DbSet<DepartmentTitleRole> DepartmentTitleRoles { get; set; }
        public virtual DbSet<JobTitle> JobTitles { get; set; }
        public virtual DbSet<DispatchArchive> DispatchArchives { get; set; }
        public virtual DbSet<DispatchAttachment> DispatchAttachments { get; set; }
        public virtual DbSet<DispatchCategory> DispatchCategories { get; set; }
        public virtual DbSet<DispatchCategoriTo> DispatchCategoriTos { get; set; }
        public virtual DbSet<DispatchRecipientsExternal> DispatchRecipientsExternals { get; set; }
        public virtual DbSet<DispatchRecipientsInternal> DispatchRecipientsInternals { get; set; }
        public virtual DbSet<DispatchResponsibility> DispatchResponsibilities { get; set; }
        public virtual DbSet<Dispatch> Dispatchs { get; set; }
        public virtual DbSet<DispatchSecurity> DispatchSecurities { get; set; }
        public virtual DbSet<DispatchSendmethod> DispatchSendmethods { get; set; }
        public virtual DbSet<DispatchTask> DispatchTasks { get; set; }
        public virtual DbSet<DispatchUrgency> DispatchUrgencys { get; set; }
        public virtual DbSet<DocumentAdjustHistory> DocumentAdjustHistories { get; set; }
        public virtual DbSet<DocumentAdjustSuggest> DocumentAdjustSuggests { get; set; }
        public virtual DbSet<DocumentArchive> DocumentArchives { get; set; }
        public virtual DbSet<DocumentAttachment> DocumentAttachments { get; set; }
        public virtual DbSet<DocumentCategory> DocumentCategories { get; set; }
        public virtual DbSet<DocumentDepartmentPermission> DocumentDepartmentPermissions { get; set; }
        public virtual DbSet<DocumentDestination> DocumentDestinations { get; set; }
        public virtual DbSet<DocumentDestroyReportDetail> DocumentDestroyReportDetails { get; set; }
        public virtual DbSet<DocumentDestroyReport> DocumentDestroyReports { get; set; }
        public virtual DbSet<DocumentDestroy> DocumentDestroys { get; set; }
        public virtual DbSet<DocumentDestroySuggestDetail> DocumentDestroySuggestDetails { get; set; }
        public virtual DbSet<DocumentDestroySuggestResponsibitity> DocumentDestroySuggestResponsibitities { get; set; }
        public virtual DbSet<DocumentDestroySuggest> DocumentDestroySuggests { get; set; }
        public virtual DbSet<DocumentDistribute> DocumentDistributes { get; set; }
        public virtual DbSet<DocumentDistributeSuggestResponsibility> DocumentDistributeSuggestResponsibilities { get; set; }
        public virtual DbSet<DocumentDistributeSuggest> DocumentDistributeSuggests { get; set; }
        public virtual DbSet<DocumentExternalApplySuggest> DocumentExternalApplySuggests { get; set; }
        public virtual DbSet<DocumentExternal> DocumentExternals { get; set; }
        public virtual DbSet<DocumentHistory> DocumentHistories { get; set; }
        public virtual DbSet<DocumentPermission> DocumentPermissions { get; set; }
        public virtual DbSet<DocumentProcess> DocumentProcesses { get; set; }
        public virtual DbSet<DocumentPublish> DocumentPublishes { get; set; }
        public virtual DbSet<DocumentPublishSuggest> DocumentPublishSuggests { get; set; }
        public virtual DbSet<DocumentReferenceAttachment> DocumentReferenceAttachments { get; set; }
        public virtual DbSet<DocumentReference> DocumentReferences { get; set; }
        public virtual DbSet<DocumentReferenceType> DocumentReferenceTypes { get; set; }
        public virtual DbSet<DocumentRequestResponsibility> DocumentRequestResponsibilities { get; set; }
        public virtual DbSet<DocumentRequestResultAttachment> DocumentRequestResultAttachments { get; set; }
        public virtual DbSet<DocumentRequestResult> DocumentRequestResults { get; set; }
        public virtual DbSet<DocumentRequest> DocumentRequests { get; set; }
        public virtual DbSet<DocumentRequestTransfer> DocumentRequestTransfers { get; set; }
        public virtual DbSet<DocumentResponsibility> DocumentResponsibilities { get; set; }
        public virtual DbSet<DocumentRetrive> DocumentRetrives { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<DocumentSecurity> DocumentSecurities { get; set; }
        public virtual DbSet<DocumentWriteSuggest> DocumentWriteSuggests { get; set; }
        public virtual DbSet<EmployeeProfile> EmployeeProfiles { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<EmployeeTitle> EmployeeTitles { get; set; }
        public virtual DbSet<EmployeeTimeOff> EmployeeTimeOffs { get; set; }
        public virtual DbSet<EmployeeWorkOut> EmployeeWorkOuts { get; set; }
        public virtual DbSet<EmployeeOT> EmployeeOTs { get; set; }
        public virtual DbSet<EmployeeJobTitle> EmployeeJobTitles { get; set; }
        public virtual DbSet<File> Files { get; set; }
        public virtual DbSet<FormApproveStatus> FormApproveStatus { get; set; }
        public virtual DbSet<ISOProcess> ISOProcesses { get; set; }
        public virtual DbSet<ISOProcessGraphic> ISOProcessGraphics { get; set; }
        public virtual DbSet<ISOProcessRelationship> ISOProcessRelationships { get; set; }
        public virtual DbSet<ISOProcessStepDependency> ISOProcessStepDependencies { get; set; }
        public virtual DbSet<ISOProcessStep> ISOProcessSteps { get; set; }
        public virtual DbSet<ISOStandard> ISOStandards { get; set; }
        public virtual DbSet<ISOTerm> ISOTerms { get; set; }
        public virtual DbSet<ProfileAssign> ProfileAssigns { get; set; }
        public virtual DbSet<ProfileAttachment> ProfileAttachments { get; set; }
        public virtual DbSet<ProfileBorrow> ProfileBorrows { get; set; }
        public virtual DbSet<ProfileCategory> ProfileCategories { get; set; }
        public virtual DbSet<ProfileComponentAttachment> ProfileComponentAttachments { get; set; }
        public virtual DbSet<ProfileComponent> ProfileComponents { get; set; }
        public virtual DbSet<ProfileDepartmentArchiveRole> ProfileDepartmentArchiveRoles { get; set; }
        public virtual DbSet<ProfileDepartmentPermission> ProfileDepartmentPermissions { get; set; }
        public virtual DbSet<ProfileDestroyDetail> ProfileDestroyDetails { get; set; }
        public virtual DbSet<ProfileDestroyReportDetail> ProfileDestroyReportDetails { get; set; }
        public virtual DbSet<ProfileDestroyReport> ProfileDestroyReports { get; set; }
        public virtual DbSet<ProfileDestroySuggest> ProfileDestroySuggests { get; set; }
        public virtual DbSet<ProfilePrincipleAttachment> ProfilePrincipleAttachments { get; set; }
        public virtual DbSet<ProfilePrinciple> ProfilePrinciples { get; set; }
        public virtual DbSet<ProfileResponsibility> ProfileResponsibilities { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<ProfileSecurity> ProfileSecurities { get; set; }
        public virtual DbSet<ProfileSuggestBorrow> ProfileSuggestBorrows { get; set; }
        public virtual DbSet<ProfileTransferDetail> ProfileTransferDetails { get; set; }
        public virtual DbSet<ProfileTransfer> ProfileTransfers { get; set; }
        public virtual DbSet<ProfileUpdateAssignment> ProfileUpdateAssignments { get; set; }
        public virtual DbSet<QualityAuditMember> QualityAuditMembers { get; set; }
        public virtual DbSet<QualityAuditNotebookNC> QualityAuditNotebookNCs { get; set; }
        public virtual DbSet<QualityAuditNotebook> QualityAuditNotebooks { get; set; }
        public virtual DbSet<QualityAuditPlanDetail> QualityAuditPlanDetails { get; set; }
        public virtual DbSet<QualityAuditPlan> QualityAuditPlans { get; set; }
        public virtual DbSet<QualityAuditPlanStandard> QualityAuditPlanStandards { get; set; }
        public virtual DbSet<QualityAuditProgramDepartment> QualityAuditProgramDepartments { get; set; }
        public virtual DbSet<QualityAuditProgram> QualityAuditPrograms { get; set; }
        public virtual DbSet<QualityAuditProgramTerm> QualityAuditProgramTerms { get; set; }
        public virtual DbSet<QualityAuditReport> QualityAuditReports { get; set; }
        public virtual DbSet<QualityCAPADetail> QualityCAPADetails { get; set; }
        public virtual DbSet<QualityCAPA> QualityCAPAs { get; set; }
        public virtual DbSet<QualityIssue> QualityIssues { get; set; }
        public virtual DbSet<QualityMeetingPlanDetail> QualityMeetingPlanDetails { get; set; }
        public virtual DbSet<QualityMeetingPlanNC> QualityMeetingPlanNCs { get; set; }
        public virtual DbSet<QualityMeetingPlanReportIssue> QualityMeetingPlanReportIssues { get; set; }
        public virtual DbSet<QualityMeetingPlanReport> QualityMeetingPlanReports { get; set; }
        public virtual DbSet<QualityMeetingPlan> QualityMeetingPlans { get; set; }
        public virtual DbSet<QualityMeetingRecordMember> QualityMeetingRecordMembers { get; set; }
        public virtual DbSet<QualityMeetingRecord> QualityMeetingRecords { get; set; }
        public virtual DbSet<QualityNCConfirmPermission> QualityNCConfirmPermissions { get; set; }
        public virtual DbSet<QualityNCRelation> QualityNCRelations { get; set; }
        public virtual DbSet<QualityNC> QualityNCs { get; set; }
        public virtual DbSet<QualityPlan> QualityPlans { get; set; }
        public virtual DbSet<QualityTargetIssue> QualityTargetIssues { get; set; }
        public virtual DbSet<QualityTargetResponsibility> QualityTargetResponsibilities { get; set; }
        public virtual DbSet<QualityTarget> QualityTargets { get; set; }
        public virtual DbSet<QualityTask> QualityTasks { get; set; }
        public virtual DbSet<QualityUnit> QualityUnits { get; set; }
        public virtual DbSet<RiskAssetAudit> RiskAssetAudits { get; set; }
        public virtual DbSet<RiskEventLog> RiskEventLogs { get; set; }
        public virtual DbSet<RiskHandle> RiskHandles { get; set; }
        public virtual DbSet<RiskIncidentHandler> RiskIncidentHandlers { get; set; }
        public virtual DbSet<RiskIncident> RiskIncidents { get; set; }
        public virtual DbSet<RiskLevelOfRisk> RiskLevelOfRisks { get; set; }
        public virtual DbSet<RiskLikelihood> RiskLikelihoods { get; set; }
        public virtual DbSet<RiskHandlingMethod> RiskHandlingMethods { get; set; }
        public virtual DbSet<RiskPlanAudit> RiskPlanAudits { get; set; }
        public virtual DbSet<RiskTargetPlan> RiskTargetPlans { get; set; }
        public virtual DbSet<RiskTarget> RiskTargets { get; set; }
        public virtual DbSet<RiskTargetTask> RiskTargetTasks { get; set; }
        public virtual DbSet<SystemNotify> SystemNotifies { get; set; }
        public virtual DbSet<SystemRole> SystemRoles { get; set; }
        public virtual DbSet<SystemRoutine> SystemRoutines { get; set; }
        public virtual DbSet<SystemTitleRole> SystemTitleRoles { get; set; }
        public virtual DbSet<SystemUserLogin> SystemUserLogins { get; set; }
        public virtual DbSet<SystemUserRole> SystemUserRoles { get; set; }
        public virtual DbSet<SystemUser> SystemUsers { get; set; }
        public virtual DbSet<TaskAssociate> TaskAssociates { get; set; }
        public virtual DbSet<TaskCategory> TaskCategories { get; set; }
        public virtual DbSet<TaskSource> TaskSources { get; set; }
        public virtual DbSet<TaskSecurity> TaskSecurities { get; set; }
        public virtual DbSet<TaskCommunicationFile> TaskCommunicationFiles { get; set; }
        public virtual DbSet<TaskCommunication> TaskCommunications { get; set; }
        public virtual DbSet<TaskCommunicationShare> TaskCommunicationShares { get; set; }
        public virtual DbSet<TaskFile> TaskFiles { get; set; }
        public virtual DbSet<TaskHistory> TaskHistories { get; set; }
        public virtual DbSet<TaskPerform> TaskPerforms { get; set; }
        public virtual DbSet<TaskResource> TaskResources { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<TaskSchedulePerform> TaskSchedulePerforms { get; set; }
        public virtual DbSet<TaskSchedule> TaskSchedules { get; set; }
        public virtual DbSet<VPHumanProfileAssess> VPHumanProfileAssesses { get; set; }
        public virtual DbSet<VPHumanProfileAttachmentFile> VPHumanProfileAttachmentFiles { get; set; }
        public virtual DbSet<VPHumanProfileAttachment> VPHumanProfileAttachments { get; set; }
        public virtual DbSet<VPHumanProfileCertificate> VPHumanProfileCertificates { get; set; }
        public virtual DbSet<VPHumanProfileContract> VPHumanProfileContracts { get; set; }
        public virtual DbSet<VPHumanProfileCuriculmViate> VPHumanProfileCuriculmViates { get; set; }
        public virtual DbSet<VPHumanProfileDiploma> VPHumanProfileDiplomas { get; set; }
        public virtual DbSet<VPHumanProfileDiscipline> VPHumanProfileDisciplines { get; set; }
        public virtual DbSet<VPHumanProfileInsurance> VPHumanProfileInsurances { get; set; }
        public virtual DbSet<VPHumanProfileRelationship> VPHumanProfileRelationships { get; set; }
        public virtual DbSet<VPHumanProfileReward> VPHumanProfileRewards { get; set; }
        public virtual DbSet<VPHumanProfileSalary> VPHumanProfileSalaries { get; set; }
        public virtual DbSet<VPHumanProfileTraining> VPHumanProfileTrainings { get; set; }
        public virtual DbSet<VPHumanProfileWorkExperience> VPHumanProfileWorkExperiences { get; set; }
        public virtual DbSet<VPHumanProfileWorkTrialResult> VPHumanProfileWorkTrialResults { get; set; }
        public virtual DbSet<VPHumanProfileWorkTrial> VPHumanProfileWorkTrials { get; set; }
        public virtual DbSet<VPHumanRecruitmentCriteria> VPHumanRecruitmentCriterias { get; set; }
        public virtual DbSet<VPHumanRecruitmentInterview> VPHumanRecruitmentInterviews { get; set; }
        public virtual DbSet<VPHumanRecruitmentPlanInterview> VPHumanRecruitmentPlanInterviews { get; set; }
        public virtual DbSet<VPHumanRecruitmentPlanRequirement> VPHumanRecruitmentPlanRequirements { get; set; }
        public virtual DbSet<VPHumanRecruitmentPlan> VPHumanRecruitmentPlans { get; set; }
        public virtual DbSet<VPHumanRecruitmentProfileInterview> VPHumanRecruitmentProfileInterviews { get; set; }
        public virtual DbSet<VPHumanRecruitmentProfileResult> VPHumanRecruitmentProfileResults { get; set; }
        public virtual DbSet<VPHumanRecruitmentProfile> VPHumanRecruitmentProfiles { get; set; }
        public virtual DbSet<VPHumanRecruitmentRequirement> VPHumanRecruitmentRequirements { get; set; }
        public virtual DbSet<VPHumanRecruitmentReview> VPHumanRecruitmentReviews { get; set; }
        public virtual DbSet<VPHumanRecruitmentTask> VPHumanRecruitmentTasks { get; set; }
        public virtual DbSet<VPHumanTrainingPlanDetail> VPHumanTrainingPlanDetails { get; set; }
        public virtual DbSet<VPHumanTrainingPlanRequirement> VPHumanTrainingPlanRequirements { get; set; }
        public virtual DbSet<VPHumanTrainingPlan> VPHumanTrainingPlans { get; set; }
        public virtual DbSet<VPHumanTrainingPractioner> VPHumanTrainingPractioners { get; set; }
        public virtual DbSet<VPHumanTrainingQuestionCategory> VPHumanTrainingQuestionCategories { get; set; }
        public virtual DbSet<VPHumanTrainingQuestion> VPHumanTrainingQuestions { get; set; }
        public virtual DbSet<VPHumanTrainingRequirementEmployee> VPHumanTrainingRequirementEmployees { get; set; }
        public virtual DbSet<VPHumanTrainingRequirement> VPHumanTrainingRequirements { get; set; }
        public virtual DbSet<HistoryBotnet> HistoryBotnets { get; set; }
        public virtual DbSet<KPIForTask> KPIForTasks { get; set; }
        public virtual DbSet<KPICategory> KPICategorys { get; set; }
        public virtual DbSet<ProfileTypeDestroy> ProfileTypeDestroys { get; set; }
        public virtual DbSet<SmsFormContent> SmsFormContents { get; set; }
        public virtual DbSet<VPHumanProfileAllowance> VPHumanProfileAllowances { get; set; }
        public virtual DbSet<VPHumanProfilePracticing> VPHumanProfilePracticings { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DispatchCategoriTo>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<DispatchResponsibility>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<RiskAssetAudit>()
                .Property(e => e.AssetValue)
                .HasPrecision(18, 0);

            modelBuilder.Entity<RiskAssetAudit>()
                .Property(e => e.ImpactLevel)
                .HasPrecision(18, 0);

            modelBuilder.Entity<RiskAssetAudit>()
                .Property(e => e.Possible)
                .HasPrecision(18, 0);

            modelBuilder.Entity<RiskAssetAudit>()
                .Property(e => e.RiskLevel)
                .HasPrecision(18, 0);

            modelBuilder.Entity<RiskHandle>()
                .Property(e => e.AssetValue)
                .HasPrecision(18, 0);

            modelBuilder.Entity<RiskHandle>()
                .Property(e => e.ImpactLevel)
                .HasPrecision(18, 0);

            modelBuilder.Entity<RiskHandle>()
                .Property(e => e.Possible)
                .HasPrecision(18, 0);

            modelBuilder.Entity<RiskHandle>()
                .Property(e => e.RiskLevel)
                .HasPrecision(18, 0);

            modelBuilder.Entity<RiskIncidentHandler>()
                .Property(e => e.Rate)
                .HasPrecision(18, 0);
        }
    }
}
