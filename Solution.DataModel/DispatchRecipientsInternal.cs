namespace iDAS.DataModel
{
    using System;

    public partial class DispatchRecipientsInternal
    {
        public Guid Id { get; set; }

        public Guid? DispatchId { get; set; }

        public Guid? EmployeeId { get; set; }

        public Guid? DepartmentId { get; set; }

        public int? Flag { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
