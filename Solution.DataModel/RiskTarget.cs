namespace iDAS.DataModel
{
    using System;

    public partial class RiskTarget
    {
        public Guid Id { get; set; }

        public Guid? DepartmentId { get; set; }

        public string Name { get; set; }

        public string Content { get; set; }

        public bool? IsTarget { get; set; }

        public bool? IsPolicy { get; set; }

        public bool? IsScene { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
