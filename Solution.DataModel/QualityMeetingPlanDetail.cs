namespace iDAS.DataModel
{
    using System;

    public partial class QualityMeetingPlanDetail
    {
        public Guid Id { get; set; }

        public Guid? QualityMeetingPlanId { get; set; }

        public int? OrderTime { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public Guid? DepartmentId { get; set; }

        public string Location { get; set; }

        public string Note { get; set; }

        public Guid? ApproveBy { get; set; }

        public int? Status { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
