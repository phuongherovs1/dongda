namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanRecruitmentTask
    {
        public Guid Id { get; set; }

        public Guid? HumanRecruitmentPlanId { get; set; }

        public Guid? TaskId { get; set; }

        public decimal? Cost { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
