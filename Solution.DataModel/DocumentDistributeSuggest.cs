namespace iDAS.DataModel
{
    using System;

    public partial class DocumentDistributeSuggest
    {
        public Guid Id { get; set; }

        public Guid? DocumentId { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public bool? IsSend { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }

        public bool? IsSendFromPromulgate { get; set; }

        public bool? IsComplete { get; set; }

        public string Note { get; set; }

        public string ReasonOfSuggest { get; set; }

        public string ReasonOfReject { get; set; }

        public bool? IsSoft { get; set; }

        public bool? IsHard { get; set; }

        public int? Quantity { get; set; }

        public DateTime? ReceiveAt { get; set; }

        public DateTime? ApplyAt { get; set; }

        public DateTime? ExpireAt { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
