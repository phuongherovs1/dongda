namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanProfileSalary
    {
        public Guid Id { get; set; }

        public Guid? HumanEmployeeId { get; set; }

        public string Level { get; set; }
        public string CodeJob { get; set; }
        public string Wage { get; set; }
        public string Extra { get; set; }

        public DateTime? DateOfApp { get; set; }
        public DateTime? DateOfOff { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public string Note { get; set; }
    }
}
