namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanTrainingPlanRequirement
    {
        public Guid Id { get; set; }

        public Guid? VBHumanTrainingRequirementId { get; set; }

        public Guid? VBHumanTrainingPlanId { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
