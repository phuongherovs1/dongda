namespace iDAS.DataModel
{
    using System;

    public partial class QualityAuditPlanDetail
    {
        public Guid Id { get; set; }

        public Guid QualityAuditPlanId { get; set; }

        public int? Orders { get; set; }

        public int? Year { get; set; }

        public DateTime? StartPlanAt { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public string Scope { get; set; }

        public string Location { get; set; }

        public string RequestContents { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public bool? IsAccept { get; set; }

        public bool? IsSend { get; set; }

        public string ApprovalNote { get; set; }

        public int? Status { get; set; }

        public string ReportContents { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
