namespace iDAS.DataModel
{
    using System;

    public partial class DepartmentTitleRole
    {
        public Guid Id { get; set; }

        public Guid DepartmentId { get; set; }
        public Guid TitleId { get; set; }

        public bool ViewAll { get; set; }
        public bool UpdateAll { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
