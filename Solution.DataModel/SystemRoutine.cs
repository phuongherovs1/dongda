namespace iDAS.DataModel
{
    using System;

    public partial class SystemRoutine
    {
        public Guid Id { get; set; }

        public DateTime? DatePerform { get; set; }

        public int? Type { get; set; }

        public string Params { get; set; }

        public bool? IsComplete { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
