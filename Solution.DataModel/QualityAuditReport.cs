namespace iDAS.DataModel
{
    using System;

    public partial class QualityAuditReport
    {
        public Guid Id { get; set; }

        public Guid? QualityAuditPlanDetailId { get; set; }

        public bool? IsSave { get; set; }

        public string Contents { get; set; }

        public string Scope { get; set; }

        public string Location { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
