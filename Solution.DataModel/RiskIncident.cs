namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class RiskIncident
    {
        public Guid Id { get; set; }

        [StringLength(50)]
        public string CodeIncident { get; set; }

        public DateTime? Date { get; set; }

        public Guid? DepartmentId { get; set; }

        public string Description { get; set; }

        public string Reason { get; set; }

        public string AnotherIdea { get; set; }

        public string Conclude { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
