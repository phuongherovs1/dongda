namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanTrainingQuestionCategory
    {
        public Guid Id { get; set; }

        public string Contents { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
