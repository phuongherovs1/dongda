namespace iDAS.DataModel
{
    using System;

    public partial class DocumentRequest
    {
        public Guid Id { get; set; }

        public Guid? DepartmentId { get; set; }

        public int? Order { get; set; }

        public Guid? ParentId { get; set; }

        public Guid? DocumentId { get; set; }

        public Guid? DocumentSuggestId { get; set; }

        public Guid? PerformBy { get; set; }

        public bool? TypeDestroy { get; set; }

        public bool? TypeWrite { get; set; }

        public bool? TypeCompile { get; set; }

        public bool? TypeArchive { get; set; }

        public bool? TypeAdjust { get; set; }

        public bool? IsSend { get; set; }

        public bool? IsPerform { get; set; }

        public bool? IsComplete { get; set; }

        public bool? IsFinish { get; set; }

        public string Note { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public DateTime? EstimateAt { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public DateTime? SendAt { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }

        public bool? IsDestroy { get; set; }

        public string ReasonOfReject { get; set; }

        public int? TypeRole { get; set; }

        public int? ProcessStatus { get; set; }

        public int? RequestFrom { get; set; }

        public int? RequestTo { get; set; }

        public int? RequestSource { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
        public Guid? CheckerBy { get; set; }
        public Guid? ApproverBy { get; set; }
        public Guid? PromulgaterBy { get; set; }
    }
}
