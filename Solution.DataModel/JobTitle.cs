namespace iDAS.DataModel
{
    using System;

    public partial class JobTitle
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid? Rank { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
