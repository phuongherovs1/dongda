namespace iDAS.DataModel
{
    using System;

    public partial class QualityAuditNotebookNC
    {
        public Guid Id { get; set; }

        public Guid? QualityAuditNotebookId { get; set; }

        public Guid? QualityNCId { get; set; }

        public bool? IsAccept { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
