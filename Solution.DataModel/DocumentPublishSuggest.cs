namespace iDAS.DataModel
{
    using System;

    public partial class DocumentPublishSuggest
    {
        public Guid Id { get; set; }

        public Guid? DocumentId { get; set; }

        public Guid ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public string Note { get; set; }

        public bool? IsSend { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }

        public string ReasonOfReject { get; set; }

        public string ReasonOfSuggest { get; set; }

        public DateTime? SuggestAt { get; set; }

        public DateTime? ApplyAt { get; set; }

        public DateTime? ExpireAt { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
