namespace iDAS.DataModel
{
    using System;

    public partial class DocumentArchive
    {
        public Guid Id { get; set; }

        public Guid? DocumentId { get; set; }

        public int? Quantity { get; set; }

        public string Position { get; set; }

        public int? DurationMonth { get; set; }

        public DateTime? StartAt { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
