namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanRecruitmentProfileResult
    {
        public Guid Id { get; set; }

        public Guid? HumanRecruitmentProfileInterviewId { get; set; }

        public int? TotalPoint { get; set; }

        public DateTime? StartTime { get; set; }

        public decimal? Salary { get; set; }

        public bool? IsApproval { get; set; }

        public bool? IsPass { get; set; }

        public bool? IsTrial { get; set; }

        public bool? IsEmployee { get; set; }

        public Guid? EmployeeId { get; set; }

        public Guid? ApprovalBy { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
