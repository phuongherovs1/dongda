namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("SmsManagersSynchronized")]
    public partial class SmsManagersSynchronized
    {
        public Guid Id { get; set; }

        [StringLength(50)]
        public string RecieveNumber { get; set; }

        [StringLength(50)]
        public string SendNumber { get; set; }

        public bool? FilterContentStatus { get; set; }

        public string MessageContent { get; set; }

        public DateTime? SendDate { get; set; }

        public DateTime? FilterContentDate { get; set; }

        public DateTime? TypingDate { get; set; }

        public string NetworkProvider { get; set; }

        public bool? IsTyping { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
