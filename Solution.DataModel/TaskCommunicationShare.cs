namespace iDAS.DataModel
{
    using System;

    public partial class TaskCommunicationShare
    {
        public Guid Id { get; set; }

        public Guid? TaskCommunicationId { get; set; }

        public Guid? TaskResourceId { get; set; }

        public bool? IsRead { get; set; }

        public DateTime? ReadAt { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
