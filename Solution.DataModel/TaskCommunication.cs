namespace iDAS.DataModel
{
    using System;

    public partial class TaskCommunication
    {
        public Guid Id { get; set; }

        public Guid? ParentId { get; set; }

        public Guid? TaskResourceId { get; set; }

        public int? Type { get; set; }

        public string Content { get; set; }

        public bool? IsPublic { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
