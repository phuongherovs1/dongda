namespace iDAS.DataModel
{
    using System;

    public partial class DispatchRecipientsExternal
    {
        public Guid Id { get; set; }

        public Guid? DispatchId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string EmployeeName { get; set; }

        public string Address { get; set; }

        public string Note { get; set; }

        public DateTime? date { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
