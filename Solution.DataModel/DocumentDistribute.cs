namespace iDAS.DataModel
{
    using System;

    public partial class DocumentDistribute
    {
        public Guid Id { get; set; }

        public Guid? DocumentId { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? ReceiveBy { get; set; }

        public Guid? ExternalId { get; set; }

        public bool? IsExternal { get; set; }

        public string Type { get; set; }

        public string Code { get; set; }

        public DateTime? DistributesAt { get; set; }

        public int? Quantity { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
