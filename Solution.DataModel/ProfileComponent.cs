namespace iDAS.DataModel
{
    using System;

    public partial class ProfileComponent
    {
        public Guid Id { get; set; }

        public Guid? ProfileId { get; set; }

        public Guid? ProfilePrincipleId { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public int? ArchiveType { get; set; }

        public DateTime? ProfileUpdateAt { get; set; }

        public Guid? EmployeeId { get; set; }

        public bool? IsUpdated { get; set; }

        public bool? IsCancel { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public bool? IsProfilePrinciple { get; set; }
    }
}
