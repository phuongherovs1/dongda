namespace iDAS.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DocumentSuggest
    {
        public Guid Id { get; set; }

        public Guid? DepartmentId { get; set; }

        public string ReasonOfSuggest { get; set; }

        public DateTime? SuggestAt { get; set; }

        public Guid? SuggestBy { get; set; }

        public int? SuggestType { get; set; }

        public int? StatusType { get; set; }

        public int? Order { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
