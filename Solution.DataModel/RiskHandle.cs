namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public partial class RiskHandle
    {
        public Guid Id { get; set; }

        public Guid? RiskAssetAuditId { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? EmployeeId { get; set; }

        public int? MethodHandle { get; set; }

        [StringLength(500)]
        public string MeasureControl { get; set; }

        [StringLength(500)]
        public string Clause { get; set; }

        [StringLength(500)]
        public string CombineComponent { get; set; }

        public DateTime? DateFinish { get; set; }

        public DateTime? DateCheck { get; set; }

        public Guid? EmployeeCheckId { get; set; }

        [StringLength(500)]
        public string Conclude { get; set; }

        public decimal? AssetValue { get; set; }

        public decimal? ImpactLevel { get; set; }

        public decimal? Possible { get; set; }

        public decimal? RiskLevel { get; set; }

        public int? AcceptCriteria { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
