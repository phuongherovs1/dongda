namespace iDAS.DataModel
{
    using System;

    public partial class QualityNC
    {
        public Guid Id { get; set; }

        public Guid? QualityTargetId { get; set; }

        public Guid? QualityAuditPlanId { get; set; }

        public string Name { get; set; }

        public string Contents { get; set; }

        public DateTime? DiscoveredAt { get; set; }

        public int? Type { get; set; }

        public string Reason { get; set; }

        public string Solution { get; set; }

        public int? Status { get; set; }

        public string OldIds { get; set; }

        public bool? IsOld { get; set; }

        public Guid? ConfirmBy { get; set; }

        public DateTime? ConfirmAt { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? DiscovereBy { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
