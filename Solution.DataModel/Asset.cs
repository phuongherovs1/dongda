namespace iDAS.DataModel
{
    using System;

    public partial class Asset
    {
        public Guid Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string TypeTitleId { get; set; }

        public Guid? TitleId { get; set; }

        public Guid? AssetGroupId { get; set; }

        public string Unit { get; set; }

        public double? Quantity { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
