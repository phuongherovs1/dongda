namespace iDAS.DataModel
{
    using System;

    public partial class QualityAuditNotebook
    {
        public Guid Id { get; set; }

        public Guid? QualityAuditProgramId { get; set; }

        public Guid? DepartmentId { get; set; }

        public DateTime? AuditAt { get; set; }

        public Guid? AuditBy { get; set; }

        public string Contents { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
