namespace iDAS.DataModel
{
    using System;

    public partial class SystemTitleRole
    {
        public Guid Id { get; set; }

        public Guid? DepartmentTitleId { get; set; }

        public Guid? SystemRoleId { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
