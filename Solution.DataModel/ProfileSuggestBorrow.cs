namespace iDAS.DataModel
{
    using System;

    public partial class ProfileSuggestBorrow
    {
        public Guid Id { get; set; }

        public Guid? ProfileId { get; set; }

        public DateTime? BorrowDate { get; set; }

        public DateTime? ReturnEstimateDate { get; set; }

        public string ReasonBorrow { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public bool? IsSend { get; set; }

        public DateTime? SendAt { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }

        public string ReasonReject { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
