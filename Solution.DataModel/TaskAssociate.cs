namespace iDAS.DataModel
{
    using System;

    public partial class TaskAssociate
    {
        public Guid Id { get; set; }

        public Guid? TaskId { get; set; }

        public Guid? TaskAssociateId { get; set; }

        public int? Type { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
