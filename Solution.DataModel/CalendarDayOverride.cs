namespace iDAS.DataModel
{
    using System;

    public partial class CalendarDayOverride
    {
        public Guid Id { get; set; }

        public Guid CalendarId { get; set; }

        public int? DayType { get; set; }

        public DateTime? Date { get; set; }

        public bool? IsHoliday { get; set; }

        public bool? IsWorkingDay { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CopyFrom { get; set; }

        public bool? IsOverride { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
