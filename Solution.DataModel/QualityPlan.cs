namespace iDAS.DataModel
{
    using System;

    public partial class QualityPlan
    {
        public Guid Id { get; set; }

        public Guid? QualityTargetId { get; set; }

        public Guid? ParentId { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? Planner { get; set; }

        public int? Status { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
