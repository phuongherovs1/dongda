namespace iDAS.DataModel
{
    using System;

    public partial class DocumentWriteSuggest
    {
        public Guid Id { get; set; }

        public Guid? DocumentId { get; set; }

        public string Name { get; set; }

        public string ReasonOfSuggest { get; set; }

        public DateTime? EstimateAt { get; set; }

        public Guid? EmployeeAssignId { get; set; }

        public Guid? EmployeeWriteId { get; set; }

        public DateTime? SuggestAt { get; set; }

        public Guid? SuggestBy { get; set; }

        public int? StatusType { get; set; }

        public int? Order { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
