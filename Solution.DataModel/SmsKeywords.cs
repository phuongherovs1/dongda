﻿using System;

namespace iDAS.DataModel
{
    public partial class SmsKeywords
    {
        public Guid Id { get; set; }

        public string KeywordValue { get; set; }

        public string Description { get; set; }

        public Guid? KeywordCategoryId { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
