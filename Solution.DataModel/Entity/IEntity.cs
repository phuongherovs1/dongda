﻿using System;

namespace iDAS.DataModel
{
    public interface IEntity
    {
        System.Guid Id { get; set; }
        Nullable<bool> IsDelete { get; set; }
        Nullable<System.DateTime> CreateAt { get; set; }
        Nullable<System.Guid> CreateBy { get; set; }
        Nullable<System.DateTime> UpdateAt { get; set; }
        Nullable<System.Guid> UpdateBy { get; set; }
    }
}
