namespace iDAS.DataModel
{
    using System;

    public partial class DocumentAdjustHistory
    {
        public Guid Id { get; set; }

        public Guid? DocumentId { get; set; }

        public string AuditTrail { get; set; }

        public string AdjustContent { get; set; }

        public int? AdjustTimes { get; set; }

        public DateTime? AdjustAt { get; set; }

        public Guid? AdjustBy { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
