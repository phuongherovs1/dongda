namespace iDAS.DataModel
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("SmsBlockList")]
    public partial class SmsBlockList
    {
        public Guid Id { get; set; }

        public Guid? IdSms { get; set; }

        [StringLength(50)]
        public string SendNumber { get; set; }

        public string MessageContent { get; set; }

        public string NetworkProvider { get; set; }

        public string Keyword { get; set; }

        [StringLength(10)]
        public string Type { get; set; }

        public DateTime? SendDate { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
