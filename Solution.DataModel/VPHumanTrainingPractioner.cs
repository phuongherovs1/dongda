namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanTrainingPractioner
    {
        public Guid Id { get; set; }

        public Guid? VPHumanTrainingPlanDetailId { get; set; }

        public Guid? EmployeeId { get; set; }

        public int? Rank { get; set; }

        public bool? IsRegister { get; set; }

        public bool? IsAcceptCommit { get; set; }

        public DateTime? TimeRegister { get; set; }

        public bool? IsJoin { get; set; }

        public string ResonUnJoin { get; set; }

        public int? NumberPresence { get; set; }

        public int? NumberAbsence { get; set; }

        public decimal? TotalPoint { get; set; }

        public string CommentOfTeacher { get; set; }

        public bool? IsInProfile { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
