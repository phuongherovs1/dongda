namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanRecruitmentRequirement
    {
        public Guid Id { get; set; }

        public Guid? HumanRoleId { get; set; }

        public string Name { get; set; }

        public int? ApproveNumber { get; set; }

        public int? StatusApproval { get; set; }

        public int? Number { get; set; }

        public DateTime? DateRequired { get; set; }

        public string Form { get; set; }

        public string Reason { get; set; }

        public bool? IsEdit { get; set; }

        public bool? IsApproval { get; set; }

        public bool? IsAccept { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public Guid? ApprovalBy { get; set; }

        public string Note { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? UpdateBy { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
