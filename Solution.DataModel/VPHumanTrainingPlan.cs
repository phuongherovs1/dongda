namespace iDAS.DataModel
{
    using System;

    public partial class VPHumanTrainingPlan
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public decimal? Cost { get; set; }

        public bool? IsEdit { get; set; }

        public bool? IsEnd { get; set; }

        public DateTime? EndAt { get; set; }

        public bool? IsApproval { get; set; }

        public bool? IsAccept { get; set; }

        public bool? IsDelete { get; set; }

        public Guid? ApprovalBy { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public string Contents { get; set; }

        public string Note { get; set; }

        public DateTime? CreateAt { get; set; }

        public Guid? CreateBy { get; set; }

        public DateTime? UpdateAt { get; set; }

        public Guid? UpdateBy { get; set; }
    }
}
