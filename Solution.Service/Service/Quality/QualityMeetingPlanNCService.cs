﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityMeetingPlanNCService : BaseService<QualityMeetingPlanNCDTO, QualityMeetingPlanNCBO>, IQualityMeetingPlanNCService
    {
        public string CountNC(Guid issueId, Guid meetingPlanReportIssueId)
        {
            var result = "Không có điểm không phù hợp nào";
            var count = GetQuery().Count(i => i.QualityIssueId == issueId && i.QualityMeetingPlanReportIssueId == meetingPlanReportIssueId);
            result = count == 0 ? result : count.ToString();
            return result;
        }

        /// <summary>
        /// Danh sách điểm không phù hợp theo vấn đề cuộc họp
        /// </summary>
        /// <param name="issueId"></param>
        /// <param name="meetingPlanReportIssueId"></param>
        /// <returns></returns>
        public IEnumerable<QualityNCBO> GetByMeetingIssue(Guid issueId, Guid meetingPlanReportIssueId)
        {
            var NCIds = GetQuery().Where(i => i.QualityIssueId == issueId && i.QualityMeetingPlanReportIssueId == meetingPlanReportIssueId)
                .Select(i => i.QualityNCId.Value);
            var data = ServiceFactory<QualityNCService>().GetByIds(NCIds);
            var employeeIds = data.Where(i => i.DiscovereBy.HasValue).Select(i => i.DiscovereBy.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            var result = data.AsEnumerable().Select(i =>
             {
                 var memberCount = 0;
                 i.MemberRelation = ServiceFactory<QualityNCRelationService>().CountQualityNCRelation(i.Id, out memberCount);
                 i.EmployeeDiscovereBy = employees.FirstOrDefault(u => u.Id == i.DiscovereBy);
                 return i;
             });
            return result;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="issueId"></param>
        /// <param name="meetingPlanReportIssueId"></param>
        /// <returns></returns>
        public QualityMeetingPlanNCBO GetFormItem(Guid issueId, Guid meetingPlanReportIssueId)
        {
            var data = new QualityMeetingPlanNCBO()
            {
                QualityIssueId = issueId,
                QualityMeetingPlanReportIssueId = meetingPlanReportIssueId,
                QualityNC = new QualityNCBO()
                {

                }
            };
            return data;
        }

        /// <summary>
        /// Tạo mới sự không phù hợp
        /// </summary>
        /// <param name="NC"></param>
        /// <returns></returns>
        public Guid Save(QualityNCBO NC, Guid issueId, Guid meetingPlanReportId, string datajson)
        {
            try
            {
                // Tạo mới sự không phù hợp
                var NCId = ServiceFactory<QualityNCService>().InsertQualityNCApproval(NC, datajson);
                var data = new QualityMeetingPlanNCBO()
                {
                    QualityIssueId = issueId,
                    QualityMeetingPlanReportIssueId = meetingPlanReportId,
                    QualityNCId = NCId
                };
                base.Insert(data, false);
                SaveTransaction();
                return NC.Id;
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
