﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityAuditNotebookNCService : BaseService<QualityAuditNotebookNCDTO, QualityAuditNotebookNCBO>, IQualityAuditNotebookNCService
    {
        public List<Guid> GetNC(Guid notebookId)
        {
            return base.GetQuery()
                .Where(c => c.QualityAuditNotebookId == notebookId)
                .Where(c => c.QualityNCId.HasValue)
                .Select(c => c.QualityNCId.Value)
                .Distinct()
                .ToList();
        }
        public List<Guid> GetNCByPlanDetail(Guid qualityAuditPlanDetailId)
        {
            var programIds = ServiceFactory<QualityAuditProgramService>().GetQuery().Where(c => c.QualityAuditPlanDetailId == qualityAuditPlanDetailId).Select(t => t.Id).ToList();
            var notebookIds = ServiceFactory<QualityAuditNotebookService>().GetQuery().Where(c => programIds.Any(i => i == c.QualityAuditProgramId)).Select(i => i.Id).ToList();
            var ncIds = ServiceFactory<QualityAuditNotebookNCService>().GetQuery().Where(c => notebookIds.Any(i => i == c.QualityAuditNotebookId)).Where(c => c.QualityNCId.HasValue).Select(c => c.QualityNCId.Value).Distinct().ToList();
            return ncIds;
        }


        public List<Guid> GetNCIsConfirmByPlanDetail(Guid qualityAuditPlanDetailId)
        {
            var programIds = ServiceFactory<QualityAuditProgramService>().GetQuery().Where(c => c.QualityAuditPlanDetailId == qualityAuditPlanDetailId).Select(t => t.Id).ToList();
            var notebookIds = ServiceFactory<QualityAuditNotebookService>().GetQuery().Where(c => programIds.Any(i => i == c.QualityAuditProgramId)).Select(i => i.Id).ToList();
            var ncIds = ServiceFactory<QualityAuditNotebookNCService>().GetQuery()
                .Where(c => notebookIds.Any(i => i == c.QualityAuditNotebookId))
                .Where(c => c.QualityNCId.HasValue)
                .Select(c => c.QualityNCId.Value)
                .Distinct()
                .ToList();
            var ncConfirmIds = ServiceFactory<QualityNCService>().GetQuery().Where(c => ncIds.Any(n => n == c.Id)
                && (c.Status != (int)iDAS.Service.Common.Resource.QualityNCStatus.NotConfirm && c.Status != (int)iDAS.Service.Common.Resource.QualityNCStatus.WaitConfirm))
                .Select(c => c.Id)
                .ToList();
            return ncConfirmIds;
        }
    }
}
