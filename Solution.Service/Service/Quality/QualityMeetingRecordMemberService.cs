﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityMeetingRecordMemberService : BaseService<QualityMeetingRecordMemberDTO, QualityMeetingRecordMemberBO>, IQualityMeetingRecordMemberService
    {
        public string CountByMeetingId(Guid meetingId, out int count)
        {
            count = GetQuery().Where(i => i.QualityMeetingPlanDetailId == meetingId).Count();
            return count == 0 ? "Chưa thiết lập" : count.ToString();
        }

        public IEnumerable<EmployeeBO> GetData(Guid meetingId)
        {
            var employeeIds = base.GetQuery().Where(i => i.QualityMeetingPlanDetailId == meetingId && i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value);
            var data = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds);
            return data;
        }

        public void AddMember(string memberIds, Guid meetingId)
        {
            try
            {
                var members = new List<QualityMeetingRecordMemberBO>();
                var employeeStr = memberIds.Trim(',').Split(',');
                foreach (var str in employeeStr)
                {
                    QualityMeetingRecordMemberBO member;
                    if (str.Contains('_'))
                    {
                        var _index = str.IndexOf('_');
                        member = new QualityMeetingRecordMemberBO()
                        {
                            QualityMeetingPlanDetailId = meetingId,
                            EmployeeId = new Guid(str.Substring(_index + 1))
                        };
                    }
                    else
                    {
                        member = new QualityMeetingRecordMemberBO()
                        {
                            QualityMeetingPlanDetailId = meetingId,
                            EmployeeId = new Guid(str)
                        };
                    }
                    var checkExistUser = Get(i => i.QualityMeetingPlanDetailId == meetingId && i.EmployeeId == member.EmployeeId.Value).Any();
                    if (!checkExistUser)
                        members.Add(member);
                }
                base.InsertRange(members);

            }
            catch (Exception)
            {

            }
        }

        public void Delete(Guid meetingId, Guid memberId)
        {
            var data = Get(i => i.QualityMeetingPlanDetailId == meetingId && i.EmployeeId == memberId).Select(i => i.Id);
            DeleteRange(data);
        }


        public void ChangeAvailable(Guid employeeId, Guid meetingId)
        {
            var data = GetQuery().Where(i => i.EmployeeId == employeeId && i.QualityMeetingPlanDetailId == meetingId).FirstOrDefault();

            data.IsNotAvailable = data.IsNotAvailable == true ? false : true;
            base.Update(data);
        }

        /// <summary>
        ///Cập nhật trạng thái vắng mặt
        /// </summary>
        /// <param name="id"></param>
        public void ChangeAvailable(Guid id)
        {
            var data = GetById(id);
            data.IsNotAvailable = data.IsNotAvailable == true ? false : true;
            base.Update(data);
        }

        public IEnumerable<QualityMeetingRecordMemberBO> GetMember(Guid meetingId)
        {
            var data = base.GetQuery().Where(i => i.QualityMeetingPlanDetailId == meetingId && i.EmployeeId.HasValue).AsEnumerable();
            var employeeIds = data.Select(i => i.EmployeeId.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds);
            data = data.Select(i =>
            {
                i.Employee = employees.FirstOrDefault(u => i.EmployeeId == u.Id);
                return i;
            });
            return data;
        }

        public IEnumerable<QualityMeetingRecordMemberBO> GetAvailableMember(Guid meetingId)
        {
            var data = base.GetQuery().Where(i => i.QualityMeetingPlanDetailId == meetingId && i.IsNotAvailable != true && i.EmployeeId.HasValue);
            var employeeIds = data.Select(i => i.EmployeeId.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds);
            var result = data.AsEnumerable().Select(i =>
            {
                i.Employee = employees.FirstOrDefault(u => i.EmployeeId == u.Id);
                return i;
            });
            return result;
        }
    }
}
