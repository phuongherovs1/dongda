﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityTargetService : BaseService<QualityTargetDTO, QualityTargetBO>, IQualityTargetService
    {
        /// <summary>
        /// Danh sách mục tiêu theo nhóm mục tiêu
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public IEnumerable<QualityTargetBO> GetByDepartment(int pageIndex, int pageSize, out int count, string departmentId, Guid? issueId,
                   Resource.TargetStatus status, string filter, DateTime? endAt)
        {
            try
            {
                var units = ServiceFactory<QualityUnitService>().GetQuery();
                var issues = ServiceFactory<QualityTargetIssueService>().GetQuery();
                var data = Enumerable.Empty<QualityTargetBO>();
                data = base.GetQuery().Where(p => p.DepartmentId == new Guid(departmentId));
                // Cập nhật mục tiêu về trạng thái quá hạn nếu ngày hết hạn nhỏ hơn ngày hiện tại
                var date = DateTime.Now.Date;
                foreach (var item in data)
                {
                    if (item.Status != (int)Resource.TargetStatus.Pass || item.Status != (int)Resource.TargetStatus.NotPass)
                    {
                        if (item.ExpectAt.HasValue && item.ExpectAt.Value.Date < date)
                        {
                            item.Status = (int)Resource.TargetStatus.Expire;
                            base.Update(item, false);
                        }
                    }
                }
                SaveTransaction(true);

                data = data.Where(p => status == Resource.TargetStatus.All ? true : p.Status == (int)status)
                           .Where(p => issueId == null ? true : p.QualityTargetIssueId == issueId)
                           .Where(p => endAt == null ? true : p.ExpectAt == endAt);

                if (!String.IsNullOrEmpty(filter))
                    data = data.Where(p => p.Name.ToLower().Contains(filter.ToLower()));
                count = data.Count();
                data = Page(data, pageIndex, pageSize).Select(i =>
                {
                    var issue = issues.FirstOrDefault(p => p.Id == i.QualityTargetIssueId.Value);
                    i.Issue = issues == null ? string.Empty : issue.Name;
                    return i;
                }).OrderByDescending(p => p.CreateAt);
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Chi tiết mục tiêu
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public QualityTargetBO GetDetail(string id, string departmentId)
        {
            try
            {
                var data = new QualityTargetBO();
                if (!String.IsNullOrEmpty(id))
                {
                    data = base.GetById(new Guid(id));
                    if (data.DepartmentId.HasValue)
                    {
                        var department = ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value);
                        data.DepartmentName = department == null ? string.Empty : department.Name;
                    }
                    data.IsAccept = data.Status == (int)Resource.TargetStatus.Approve ? true : data.Status == (int)Resource.TargetStatus.NotApprove ? false : (bool?)null;
                    if (!String.IsNullOrEmpty(data.TargetValueExpression) && data.TargetValueExpression != "[]")
                    {
                        var Expressions = JsonConvert.DeserializeObject<List<ExpressionBO>>(data.TargetValueExpression);
                        if (Expressions != null && Expressions.Count > 0)
                        {
                            data.Expression1 = Expressions[0].Operator;
                            data.ValueExpression1 = Expressions[0].Value;
                            data.Show1 = true;

                            data.Expression2 = Expressions.Count > 1 ? Expressions[1].Operator : (int?)null;
                            data.ValueExpression2 = Expressions.Count > 1 ? Expressions[1].Value : (int?)null;
                            data.Show2 = Expressions.Count > 1 ? true : false;

                            data.Expression3 = Expressions.Count > 2 ? Expressions[2].Operator : (int?)null;
                            data.ValueExpression3 = Expressions.Count > 2 ? Expressions[2].Value : (int?)null;
                            data.Show3 = Expressions.Count > 2 ? true : false;

                            data.Expression4 = Expressions.Count > 3 ? Expressions[3].Operator : (int?)null;
                            data.ValueExpression4 = Expressions.Count > 3 ? Expressions[3].Value : (int?)null;
                            data.Show4 = Expressions.Count > 3 ? true : false;

                            data.Expression5 = Expressions.Count > 4 ? Expressions[4].Operator : (int?)null;
                            data.ValueExpression5 = Expressions.Count > 4 ? Expressions[4].Value : (int?)null;
                            data.Show5 = Expressions.Count > 4 ? true : false;
                        }
                    }
                    else
                    {
                        data.Show1 = false;
                        data.Show2 = false;
                        data.Show3 = false;
                        data.Show4 = false;
                        data.Show5 = false;
                    }
                }
                else
                {
                    var department = new DepartmentBO();
                    if (!String.IsNullOrEmpty(departmentId))
                    {
                        if (departmentId.Contains('_'))
                        {
                            var items = departmentId.Split('_').ToList();
                            if (items != null && items.Count() == 2)
                            {
                                department = ServiceFactory<DepartmentService>().GetById(new Guid(items[1]));
                                data.DepartmentId = new Guid(items[1]);
                                data.DepartmentName = department == null ? string.Empty : department.Name;
                            }
                        }
                        else
                        {
                            department = ServiceFactory<DepartmentService>().GetById(new Guid(departmentId));
                            data.DepartmentId = new Guid(departmentId);
                            data.DepartmentName = department == null ? string.Empty : department.Name;
                        }
                    }
                    data.Show1 = false;
                    data.Show2 = false;
                    data.Show3 = false;
                    data.Show4 = false;
                    data.Show5 = false;
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Thêm mới mục tiêu
        /// </summary>
        /// <param name="data"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public Guid InsertTarget(QualityTargetBO data, string jsonData)
        {
            try
            {
                var issue = ServiceFactory<QualityTargetIssueService>().GetQuery().Where(p => p.Id == data.QualityTargetIssueId.Value).FirstOrDefault();
                var unit = ServiceFactory<QualityUnitService>().GetQuery().Where(p => p.Id == data.QualityUnitId.Value).FirstOrDefault();
                var issueName = issue == null ? string.Empty : issue.Name;
                var unitName = unit == null ? string.Empty : unit.Name.ToLower();
                if (data.IsScale == true)// phần trăm
                {
                    data.TargetValueExpression = string.Empty;
                    data.TargetValue = 0;
                }
                else// giá trị
                {
                    data.Scale = 0;
                }
                if (data.Id == null || data.Id == Guid.Empty)
                {
                    switch (data.Destination)
                    {
                        case (int)Resource.TargetDestination.Up:
                            if (data.IsScale == true)
                                data.Name = String.Format("[{0}] {1} {2} % {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Up, data.Scale, unitName, data.ExpectAt.Value);
                            else
                                data.Name = String.Format("[{0}] {1} {2} {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Up, data.TargetValue, unitName, data.ExpectAt.Value);
                            break;
                        case (int)Resource.TargetDestination.Down:
                            if (data.IsScale == true)
                                data.Name = String.Format("[{0}] {1} {2} % {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Up, data.Scale, unitName, data.ExpectAt.Value);
                            else
                                data.Name = String.Format("[{0}] {1} {2} {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Down, data.TargetValue, unitName, data.ExpectAt.Value);
                            break;
                        case (int)Resource.TargetDestination.Equals:
                            if (data.IsScale == true)
                                data.Name = String.Format("[{0}] {1} {2} % {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Up, data.Scale, unitName, data.ExpectAt.Value);
                            else
                            {
                                if (!String.IsNullOrEmpty(data.TargetValueExpression) && data.TargetValueExpression != "[]")
                                {
                                    var Expressions = JsonConvert.DeserializeObject<List<ExpressionBO>>(data.TargetValueExpression);
                                    if (Expressions != null && Expressions.Count > 0)
                                    {
                                        switch (Expressions[0].Operator)
                                        {
                                            case (int)Resource.TargetExpression.Grand:
                                                data.Name = String.Format("[{0}] {1} {2} {3}", issueName, Resource.TargetExpressionText.Grand, Expressions[0].Value, unitName);
                                                break;
                                            case (int)Resource.TargetExpression.Less:
                                                data.Name += !String.IsNullOrEmpty(data.Name) ? String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Less, Expressions[0].Value, unitName) :
                                                            String.Format("[{0}] {1} {2} {3}", issueName, Resource.TargetExpressionText.Less, Expressions[0].Value, unitName);
                                                //data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Less, Expressions[0].Value, unitName);
                                                break;
                                            case (int)Resource.TargetExpression.Equal:
                                                data.Name += !String.IsNullOrEmpty(data.Name) ? String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Equal, Expressions[0].Value, unitName) :
                                                            String.Format("[{0}] {1} {2} {3}", issueName, Resource.TargetExpressionText.Equal, Expressions[0].Value, unitName);
                                                //data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Equal, Expressions[0].Value, unitName);
                                                break;
                                            case (int)Resource.TargetExpression.GrandOrEqual:
                                                data.Name += !String.IsNullOrEmpty(data.Name) ? String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.GrandOrEqual, Expressions[0].Value, unitName) :
                                                            String.Format("[{0}] {1} {2} {3}", issueName, Resource.TargetExpressionText.GrandOrEqual, Expressions[0].Value, unitName);
                                                //data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.GrandOrEqual, Expressions[0].Value, unitName);
                                                break;
                                            case (int)Resource.TargetExpression.LessOrEqual:
                                                data.Name += !String.IsNullOrEmpty(data.Name) ? String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.LessOrEqual, Expressions[0].Value, unitName) :
                                                            String.Format("[{0}] {1} {2} {3}", issueName, Resource.TargetExpressionText.LessOrEqual, Expressions[0].Value, unitName);
                                                //data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.LessOrEqual, Expressions[0].Value, unitName);
                                                break;
                                        }
                                        if (Expressions.Count > 1)
                                        {
                                            switch (Expressions[1].Operator)
                                            {
                                                case (int)Resource.TargetExpression.Grand:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Grand, Expressions[1].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Less:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Less, Expressions[1].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Equal:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Equal, Expressions[1].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.GrandOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.GrandOrEqual, Expressions[1].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.LessOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.LessOrEqual, Expressions[1].Value, unitName);
                                                    break;
                                            }
                                        }
                                        if (Expressions.Count > 2)
                                        {
                                            switch (Expressions[2].Operator)
                                            {
                                                case (int)Resource.TargetExpression.Grand:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Grand, Expressions[2].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Less:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Less, Expressions[2].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Equal:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Equal, Expressions[2].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.GrandOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.GrandOrEqual, Expressions[2].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.LessOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.LessOrEqual, Expressions[2].Value, unitName);
                                                    break;
                                            }
                                        }
                                        if (Expressions.Count > 3)
                                        {
                                            switch (Expressions[3].Operator)
                                            {
                                                case (int)Resource.TargetExpression.Grand:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Grand, Expressions[3].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Less:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Less, Expressions[3].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Equal:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Equal, Expressions[3].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.GrandOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.GrandOrEqual, Expressions[3].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.LessOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.LessOrEqual, Expressions[3].Value, unitName);
                                                    break;
                                            }
                                        }
                                        if (Expressions.Count > 4)
                                        {
                                            switch (Expressions[4].Operator)
                                            {
                                                case (int)Resource.TargetExpression.Grand:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Grand, Expressions[4].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Less:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Less, Expressions[4].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Equal:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Equal, Expressions[4].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.GrandOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.GrandOrEqual, Expressions[4].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.LessOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.LessOrEqual, Expressions[4].Value, unitName);
                                                    break;
                                            }
                                        }
                                    }
                                    data.Name += !String.IsNullOrEmpty(data.Name) ? String.Format(" đến ngày {0:dd/MM/yyyy}", data.ExpectAt.Value) :
                                                  String.Format("[{0}] {1} {2} {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Equals, data.TargetValue, unitName, data.ExpectAt.Value);
                                }
                                else
                                    data.Name = String.Format("[{0}] {1} {2} {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Equals, data.TargetValue, unitName, data.ExpectAt.Value);
                            }
                            break;
                        default:
                            break;
                    }
                    data.Status = (int)Resource.TargetStatus.New;
                    data.Id = base.Insert(data, true);
                    var responsibility = new QualityTargetResponsibilityBO();
                    responsibility.EmployeeId = UserId;
                    responsibility.RoleType = (int)Resource.TargetResponsibility.Create;
                    responsibility.QualityTargetId = data.Id;
                    ServiceFactory<QualityTargetResponsibilityService>().Insert(responsibility, false);
                    ServiceFactory<QualityTargetResponsibilityService>().Create(data.Id, jsonData, false);
                }
                else
                {
                    var target = base.GetById(data.Id);
                    if (target != null && target.CreateBy != UserId)
                        throw new AccessDenyException();
                    switch (data.Destination)
                    {
                        case (int)Resource.TargetDestination.Up:
                            if (data.IsScale == true)
                                data.Name = String.Format("[{0}] {1} {2} % {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Up, data.Scale, unitName, data.ExpectAt.Value);
                            else
                                data.Name = String.Format("[{0}] {1} {2} {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Up, data.TargetValue, unitName, data.ExpectAt.Value);
                            break;
                        case (int)Resource.TargetDestination.Down:
                            if (data.IsScale == true)
                                data.Name = String.Format("[{0}] {1} {2} % {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Up, data.Scale, unitName, data.ExpectAt.Value);
                            else
                                data.Name = String.Format("[{0}] {1} {2} {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Down, data.TargetValue, unitName, data.ExpectAt.Value);
                            break;
                        case (int)Resource.TargetDestination.Equals:
                            if (data.IsScale == true)
                                data.Name = String.Format("[{0}] {1} {2} % {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Up, data.Scale, unitName, data.ExpectAt.Value);
                            else
                            {
                                if (!String.IsNullOrEmpty(data.TargetValueExpression) && data.TargetValueExpression != "[]")
                                {
                                    var Expressions = JsonConvert.DeserializeObject<List<ExpressionBO>>(data.TargetValueExpression);
                                    if (Expressions != null && Expressions.Count > 0)
                                    {
                                        switch (Expressions[0].Operator)
                                        {
                                            case (int)Resource.TargetExpression.Grand:
                                                data.Name += !String.IsNullOrEmpty(data.Name) ? String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Grand, Expressions[0].Value, unitName) :
                                                            String.Format("[{0}] {1} {2} {3}", issueName, Resource.TargetExpressionText.Grand, Expressions[0].Value, unitName);
                                                //data.Name = String.Format("[{0}] {1} {2} {3}", issueName, Resource.TargetExpressionText.Grand, Expressions[0].Value, unitName);
                                                break;
                                            case (int)Resource.TargetExpression.Less:
                                                data.Name += !String.IsNullOrEmpty(data.Name) ? String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Less, Expressions[0].Value, unitName) :
                                                            String.Format("[{0}] {1} {2} {3}", issueName, Resource.TargetExpressionText.Less, Expressions[0].Value, unitName);
                                                //data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Less, Expressions[0].Value, unitName);
                                                break;
                                            case (int)Resource.TargetExpression.Equal:
                                                data.Name += !String.IsNullOrEmpty(data.Name) ? String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Equal, Expressions[0].Value, unitName) :
                                                            String.Format("[{0}] {1} {2} {3}", issueName, Resource.TargetExpressionText.Equal, Expressions[0].Value, unitName);
                                                //data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Equal, Expressions[0].Value, unitName);
                                                break;
                                            case (int)Resource.TargetExpression.GrandOrEqual:
                                                data.Name += !String.IsNullOrEmpty(data.Name) ? String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.GrandOrEqual, Expressions[0].Value, unitName) :
                                                            String.Format("[{0}] {1} {2} {3}", issueName, Resource.TargetExpressionText.GrandOrEqual, Expressions[0].Value, unitName);
                                                //data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.GrandOrEqual, Expressions[0].Value, unitName);
                                                break;
                                            case (int)Resource.TargetExpression.LessOrEqual:
                                                data.Name += !String.IsNullOrEmpty(data.Name) ? String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.LessOrEqual, Expressions[0].Value, unitName) :
                                                            String.Format("[{0}] {1} {2} {3}", issueName, Resource.TargetExpressionText.LessOrEqual, Expressions[0].Value, unitName);
                                                //data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.LessOrEqual, Expressions[0].Value, unitName);
                                                break;
                                        }
                                        if (Expressions.Count > 1)
                                        {
                                            switch (Expressions[1].Operator)
                                            {
                                                case (int)Resource.TargetExpression.Grand:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Grand, Expressions[1].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Less:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Less, Expressions[1].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Equal:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Equal, Expressions[1].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.GrandOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.GrandOrEqual, Expressions[1].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.LessOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.LessOrEqual, Expressions[1].Value, unitName);
                                                    break;
                                            }
                                        }
                                        if (Expressions.Count > 2)
                                        {
                                            switch (Expressions[2].Operator)
                                            {
                                                case (int)Resource.TargetExpression.Grand:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Grand, Expressions[2].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Less:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Less, Expressions[2].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Equal:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Equal, Expressions[2].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.GrandOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.GrandOrEqual, Expressions[2].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.LessOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.LessOrEqual, Expressions[2].Value, unitName);
                                                    break;
                                            }
                                        }
                                        if (Expressions.Count > 3)
                                        {
                                            switch (Expressions[3].Operator)
                                            {
                                                case (int)Resource.TargetExpression.Grand:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Grand, Expressions[3].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Less:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Less, Expressions[3].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Equal:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Equal, Expressions[3].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.GrandOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.GrandOrEqual, Expressions[3].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.LessOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.LessOrEqual, Expressions[3].Value, unitName);
                                                    break;
                                            }
                                        }
                                        if (Expressions.Count > 4)
                                        {
                                            switch (Expressions[4].Operator)
                                            {
                                                case (int)Resource.TargetExpression.Grand:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Grand, Expressions[4].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Less:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Less, Expressions[4].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.Equal:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.Equal, Expressions[4].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.GrandOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.GrandOrEqual, Expressions[4].Value, unitName);
                                                    break;
                                                case (int)Resource.TargetExpression.LessOrEqual:
                                                    data.Name += String.Format(" và {0} {1} {2}", Resource.TargetExpressionText.LessOrEqual, Expressions[4].Value, unitName);
                                                    break;
                                            }
                                        }
                                    }
                                    data.Name += !String.IsNullOrEmpty(data.Name) ? String.Format(" đến ngày {0:dd/MM/yyyy}", data.ExpectAt.Value) :
                                                  String.Format("[{0}] {1} {2} {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Equals, data.TargetValue, unitName, data.ExpectAt.Value);
                                }
                                else
                                    data.Name = String.Format("[{0}] {1} {2} {3} đến ngày {4:dd/MM/yyyy}", issueName, Resource.TargetDestinationText.Equals, data.TargetValue, unitName, data.ExpectAt.Value);
                            }
                            break;
                        default:
                            break;
                    }
                    // Expression
                    if (data.Status == (int)Resource.TargetStatus.Expire)
                        if (data.ExpectAt.Value.Date >= DateTime.Now.Date)
                            data.Status = (int)Resource.TargetStatus.New;
                    base.Update(data);
                    ServiceFactory<QualityTargetResponsibilityService>().Create(data.Id, jsonData);
                }
                return data.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Xóa mục tiêu
        /// </summary>
        /// <param name="id"></param>
        public void DeleteTarget(Guid id)
        {
            try
            {
                var target = base.GetById(id);
                if (target.CreateBy != UserId)
                    throw new AccessDenyException();
                // Xóa trách nhiệm liên quan mục tiêu
                var responsibilities = ServiceFactory<QualityTargetResponsibilityService>().GetQuery().Where(p => p.IsDelete != true && p.QualityTargetId == id).AsEnumerable().Select(p => p.Id);
                ServiceFactory<QualityTargetResponsibilityService>().DeleteRange(responsibilities, false);
                // Xóa kế hoạch và công việc
                var plans = ServiceFactory<QualityPlanService>().GetQuery().Where(p => p.QualityTargetId == id).AsEnumerable();
                var tasks = ServiceFactory<QualityTaskService>().GetQuery().Where(p => plans.Select(i => i.Id).Contains(p.QualityPlanId.Value)).AsEnumerable().Select(p => p.TaskId.Value).Distinct();
                ServiceFactory<QualityPlanService>().DeleteRange(plans.Select(p => p.Id), false);
                ServiceFactory<TaskService>().DeleteRange(tasks, false);
                // Xóa mục tiêu
                base.Delete(id, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Hủy mục tiêu
        /// </summary>
        /// <param name="id"></param>
        public void DestroyTarget(Guid id)
        {
            try
            {
                var data = base.GetById(id);
                if (data.CreateBy != UserId)
                    throw new AccessDenyException();
                data.Status = (int)Resource.TargetStatus.Destroy;
                base.Update(data);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Cập nhật kết quả thực hiện
        /// </summary>
        /// <param name="data"></param>
        public void UpdateResult(QualityTargetBO data)
        {
            try
            {
                var target = base.GetById(data.Id);

                if (target.Destination.HasValue)
                {
                    switch (target.Destination.Value)
                    {
                        case (int)Resource.TargetDestination.Up:
                            if (target.TargetValue.HasValue && target.TargetValue.Value <= data.ResultValue.Value)
                                data.Status = (int)Resource.TargetStatus.Pass;
                            else
                                data.Status = (int)Resource.TargetStatus.NotPass;
                            break;
                        case (int)Resource.TargetDestination.Down:
                            if (target.TargetValue.HasValue && target.TargetValue.Value <= data.ResultValue.Value)
                                data.Status = (int)Resource.TargetStatus.Pass;
                            else
                                data.Status = (int)Resource.TargetStatus.NotPass;
                            break;
                        case (int)Resource.TargetDestination.Equals:
                            if (String.IsNullOrEmpty(target.TargetValueExpression))
                            {
                                if (target.TargetValue.HasValue && target.TargetValue.Value == data.ResultValue.Value)
                                    data.Status = (int)Resource.TargetStatus.Pass;
                                else
                                    data.Status = (int)Resource.TargetStatus.NotPass;
                            }
                            else
                            {
                                var Expressions = JsonConvert.DeserializeObject<List<ExpressionBO>>(target.TargetValueExpression);
                                if (Expressions != null && Expressions.Count > 0)
                                {
                                    var rs1 = true;
                                    var rs2 = true;
                                    var rs3 = true;
                                    var rs4 = true;
                                    var rs5 = true;
                                    rs1 = CompareExpression((Resource.TargetExpression)Expressions[0].Operator, Convert.ToDecimal(Expressions[0].Value), Convert.ToDecimal(data.ResultValue));
                                    if (Expressions.Count > 1)
                                        rs2 = CompareExpression((Resource.TargetExpression)Expressions[1].Operator, Convert.ToDecimal(Expressions[1].Value), Convert.ToDecimal(data.ResultValue));
                                    if (Expressions.Count > 2)
                                        rs3 = CompareExpression((Resource.TargetExpression)Expressions[2].Operator, Convert.ToDecimal(Expressions[2].Value), Convert.ToDecimal(data.ResultValue));
                                    if (Expressions.Count > 3)
                                        rs4 = CompareExpression((Resource.TargetExpression)Expressions[3].Operator, Convert.ToDecimal(Expressions[3].Value), Convert.ToDecimal(data.ResultValue));
                                    if (Expressions.Count > 4)
                                        rs5 = CompareExpression((Resource.TargetExpression)Expressions[4].Operator, Convert.ToDecimal(Expressions[4].Value), Convert.ToDecimal(data.ResultValue));
                                    if (rs1 && rs2 && rs3 && rs4 && rs5)
                                        data.Status = (int)Resource.TargetStatus.Pass;
                                    else
                                        data.Status = (int)Resource.TargetStatus.NotPass;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                base.Update(data);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool CompareExpression(Resource.TargetExpression expression, decimal expressionValue, decimal value)
        {
            switch (expression)
            {
                case Resource.TargetExpression.Grand:
                    return expressionValue < value;
                case Resource.TargetExpression.Less:
                    return expressionValue > value;
                case Resource.TargetExpression.Equal:
                    return expressionValue == value;
                case Resource.TargetExpression.GrandOrEqual:
                    return expressionValue <= value;
                case Resource.TargetExpression.LessOrEqual:
                    return expressionValue >= value;
                default:
                    return true;
            }
        }

        /// <summary>
        /// Khôi phục mục tiêu đã hủy
        /// </summary>
        /// <param name="id"></param>
        public void RevertTarget(Guid id)
        {
            try
            {
                var data = base.GetById(id);
                if (data.CreateBy != UserId)
                    throw new AccessDenyException();
                data.Status = (int)Resource.TargetStatus.New;
                base.Update(data);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Phê duyệt mục tiêu
        /// </summary>
        /// <param name="data"></param>
        public void Approval(QualityTargetBO data)
        {
            try
            {
                var responsibility = ServiceFactory<QualityTargetResponsibilityService>().GetQuery().Where(p => p.QualityTargetId == data.Id && p.RoleType == (int)Resource.TargetResponsibility.Approval).Select(p => p.EmployeeId.Value);
                if (!responsibility.Contains(UserId))
                    throw new AccessDenyException();
                // Danh sách kế hoạch theo mục tiêu
                var plans = ServiceFactory<QualityPlanService>().GetQuery().Where(p => p.QualityTargetId == data.Id);
                if (data.IsAccept == true)
                {
                    data.Status = (int)Resource.TargetStatus.Approve;
                    // Cập nhật kế hoạch về trạng thái Duyệt
                    if (plans != null && plans.Count() > 0)
                    {
                        foreach (var item in plans)
                        {
                            item.Status = (int)Resource.QualityPlan.Approved;
                            ServiceFactory<QualityPlanService>().Update(item, false);
                        }
                    }
                }
                else
                {
                    data.Status = (int)Resource.TargetStatus.NotApprove;
                    // Cập nhật trạng thái kế hoạch về trạng thái Không duyệt
                    if (plans != null && plans.Count() > 0)
                    {
                        foreach (var item in plans)
                        {
                            item.Status = (int)Resource.QualityPlan.NotApprove;
                            ServiceFactory<QualityPlanService>().Update(item, false);
                        }
                    }
                }
                base.Update(data, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
