﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityMeetingPlanReportIssueService : BaseService<QualityMeetingPlanReportIssueDTO, QualityMeetingPlanReportIssueBO>, IQualityMeetingPlanReportIssueService
    {

        public IEnumerable<QualityIssueBO> GetIssueReport(Guid meetingPlanReportId)
        {
            var issueIds = GetQuery().Where(i => i.QualityMeetingPlanReportId == meetingPlanReportId).Select(i => i.QualityIssueId.Value);
            var data = ServiceFactory<QualityIssueService>().GetByIds(issueIds);
            return data;

        }

        public Guid AddIssue(Guid issueId, Guid meetingPlanReportId)
        {
            var data = new QualityMeetingPlanReportIssueBO()
            {
                QualityIssueId = issueId,
                QualityMeetingPlanReportId = meetingPlanReportId
            };
            data.Id = base.Insert(data);
            return data.Id;
        }

        /// <summary>
        /// Đưa danh sách vấn đề, số lượng điểm không phù hợp
        /// </summary>
        /// <param name="meetingPlanReportId"></param>
        /// <returns></returns>
        public IEnumerable<QualityMeetingPlanReportIssueBO> GetIssueMeetingReport(Guid meetingPlanReportId)
        {
            var data = GetQuery().Where(i => i.QualityMeetingPlanReportId == meetingPlanReportId).AsEnumerable();
            var issueIds = data.Select(i => i.QualityIssueId.Value);
            var issues = ServiceFactory<QualityIssueService>().GetByIds(issueIds);
            data = data.Select(i =>
            {
                var issue = issues.FirstOrDefault(u => i.QualityIssueId == u.Id);
                i.IssueName = issue.Name;
                i.CountNCText = ServiceFactory<QualityMeetingPlanNCService>().CountNC(i.QualityIssueId.Value, i.Id);
                return i;
            });
            return data;
        }
    }
}
