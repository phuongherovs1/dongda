﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityNCRelationService : BaseService<QualityNCRelationDTO, QualityNCRelationBO>, IQualityNCRelationService
    {
        public IEnumerable<QualityNCRelationBO> GetDataQualityNCRelation(int pageIndex, int pageSize, out int count, Guid QualiyNCId)
        {
            var dataQuery = base.GetQuery()
              .Where(i => i.QualityNCId == QualiyNCId)
              .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            results = results.Select(i =>
            {
                var employee = ServiceFactory<EmployeeService>().GetById(i.EmployeeId.HasValue ? i.EmployeeId.Value : Guid.Empty);
                var Department = ServiceFactory<DepartmentService>().GetById(i.DepartmentId.HasValue ? i.DepartmentId.Value : Guid.Empty);
                var Title = ServiceFactory<DepartmentTitleService>().GetById(i.DepartmentTitleId.HasValue ? i.DepartmentTitleId.Value : Guid.Empty);
                if (i.DepartmentId.HasValue)
                {
                    i.TypeName = Department.Name;
                    i.IdRelation = Department.Id;
                }
                if (i.EmployeeId.HasValue)
                {
                    i.TypeName = employee.Name;
                    i.IdRelation = employee.Id;
                }
                if (i.DepartmentTitleId.HasValue)
                {
                    i.TypeName = Title.Name;
                    i.IdRelation = Title.Id;
                }
                return i;
            }).ToList();
            return results;
        }

        public string CountQualityNCRelation(Guid QualityNCId, out int count)
        {
            count = GetQuery().Where(i => i.QualityNCId == QualityNCId).Count();
            return count == 0 ? "Không có" : count.ToString();
        }
    }
}
