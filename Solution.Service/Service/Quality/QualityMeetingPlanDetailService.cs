﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityMeetingPlanDetailService : BaseService<QualityMeetingPlanDetailDTO, QualityMeetingPlanDetailBO>, IQualityMeetingPlanDetailService
    {
        public QualityMeetingPlanDetailBO GetNewestMeeting(Guid planId)
        {
            var data = base.GetQuery().Where(i => i.QualityMeetingPlanId == planId).OrderByDescending(i => i.OrderTime).FirstOrDefault();
            return data;
        }

        public IEnumerable<QualityMeetingPlanDetailBO> GetByPlanId(Guid planId)
        {
            var data = base.GetQuery().Where(i => i.QualityMeetingPlanId == planId).OrderByDescending(i => i.OrderTime).AsEnumerable();
            data = data.Select(i =>
            {
                var memberJoinCount = 0;
                i.MeetingMemberJoin = ServiceFactory<QualityMeetingRecordMemberService>().CountByMeetingId(i.Id, out memberJoinCount);
                i.MeetingMemberJoinCount = memberJoinCount;

                var countSubject = 0;
                i.SubjectText = ServiceFactory<QualityMeetingPlanReportService>().CountByMeetingId(i.Id, out countSubject);
                i.SubjectCount = countSubject;

                if (i.DepartmentId.HasValue)
                {
                    var department = ServiceFactory<DepartmentService>().GetById(i.DepartmentId.Value);
                    i.MeetingDepartmentName = department.Name;
                }
                return i;
            });
            return data;
        }

        /// <summary>
        /// Tạo cuộc họp theo định kỳ
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public Guid CreateByRoutine(Guid planId)
        {
            try
            {
                var result = Guid.Empty;
                var lastMeeting = GetNewestMeeting(planId);
                var plan = ServiceFactory<QualityMeetingPlanService>().GetById(planId);
                /// check plan định kỳ
                if (plan.IsRoutine == true)
                {
                    // Tạo cuộc họp sắp tới
                    var newMeeting = lastMeeting;
                    newMeeting.Id = Guid.NewGuid();
                    newMeeting.OrderTime = lastMeeting.OrderTime.Value + 1;
                    newMeeting.Status = (int)Resource.MeetingStatus.New;
                    switch (plan.UnitTime)
                    {
                        case (int)Resource.UnitTime.Year:
                            {
                                newMeeting.StartAt = newMeeting.StartAt.Value.AddYears(plan.MeetingValue.Value);
                                newMeeting.EndAt = newMeeting.EndAt.Value.AddYears(plan.MeetingValue.Value);
                            }
                            break;
                        case (int)Resource.UnitTime.Month:
                            {
                                newMeeting.StartAt = newMeeting.StartAt.Value.AddMonths(plan.MeetingValue.Value);
                                newMeeting.EndAt = newMeeting.EndAt.Value.AddMonths(plan.MeetingValue.Value);
                            }
                            break;
                        case (int)Resource.UnitTime.Week:
                            {
                                newMeeting.StartAt = newMeeting.StartAt.Value.AddDays(plan.MeetingValue.Value * 7);
                                newMeeting.EndAt = newMeeting.EndAt.Value.AddDays(plan.MeetingValue.Value * 7);
                            }
                            break;
                        case (int)Resource.UnitTime.Day:
                            {
                                newMeeting.StartAt = newMeeting.StartAt.Value.AddYears(plan.MeetingValue.Value);
                                newMeeting.EndAt = newMeeting.EndAt.Value.AddYears(plan.MeetingValue.Value);
                            }
                            break;
                        default:
                            break;
                    }
                    // Cập nhật thông tin lần họp cuối cùng
                    plan.LastTime = lastMeeting.OrderTime.Value + 1;

                    result = Insert(newMeeting, false);
                    ServiceFactory<QualityMeetingPlanService>().Update(plan, false);
                    SaveTransaction();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public QualityMeetingPlanDetailBO GetFullInfo(Guid id)
        {
            try
            {
                var data = GetById(id);
                if (data.Status != (int)Resource.MeetingStatus.Perform && data.Status != (int)Resource.MeetingStatus.Finish && data.Status != (int)Resource.MeetingStatus.Cancel)
                {
                    data.Status = (int)Resource.MeetingStatus.Perform;
                    base.Update(data);
                }
                var plan = ServiceFactory<QualityMeetingPlanService>().GetById(data.QualityMeetingPlanId.Value);
                data.PlanName = plan.Name;
                return data;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public void Approve(Guid id)
        {
            var item = GetById(id);
            var plan = ServiceFactory<QualityMeetingPlanService>().GetById(item.QualityMeetingPlanId.Value);
            var nextTime = DateTime.Now;
            switch (plan.UnitTime)
            {
                case (int)Resource.UnitTime.Year:
                    nextTime = DateTime.Now.AddYears(plan.MeetingValue.Value);
                    break;
                case (int)Resource.UnitTime.Month:
                    nextTime = DateTime.Now.AddMonths(plan.MeetingValue.Value);
                    break;
                case (int)Resource.UnitTime.Week:
                    nextTime = DateTime.Now.AddDays(plan.MeetingValue.Value * 7);
                    break;
                case (int)Resource.UnitTime.Day:
                    nextTime = DateTime.Now.AddDays(plan.MeetingValue.Value);
                    break;
                default:
                    break;
            }

            ServiceFactory<SystemRoutineService>().InsertRoutine(nextTime, Resource.RoutineType.QualityMeetingPlan, id.ToString());
            item.Status = (int)Resource.MeetingStatus.Approve;
            Update(item);
        }

        public QualityMeetingPlanDetailBO GetNewMeetingForm(Guid planId)
        {
            var lastMeeting = GetQuery().Where(i => i.QualityMeetingPlanId == planId).OrderByDescending(i => i.OrderTime).FirstOrDefault();
            var plan = ServiceFactory<QualityMeetingPlanService>().GetById(planId);

            var newMeeting = new QualityMeetingPlanDetailBO()
            {
                Plan = plan,
                QualityMeetingPlanId = planId,
                OrderTime = lastMeeting.OrderTime.Value + 1,
                StartAt = DateTime.Now,
                StartAtTime = DateTime.Now.TimeOfDay,

                DepartmentId = lastMeeting.DepartmentId,
                Location = lastMeeting.Location
            };
            return newMeeting;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid NewMeeting(QualityMeetingPlanDetailBO data)
        {
            data.StartAt = data.StartAt.Value.Add(data.StartAtTime.Value);
            data.EndAt = data.EndAt.Value.Add(data.EndAtTime.Value);
            data.Id = base.Insert(data);
            return data.Id;
        }


        public void SendApprove(QualityMeetingPlanDetailBO data)
        {
            try
            {

                data.Status = (int)Resource.MeetingStatus.WaitApprove;
                if (data.EmployeeApprove != null)
                    data.ApproveBy = data.EmployeeApprove.Id;
                if (data.Id == Guid.Empty)
                    Insert(data);
                else
                    Update(data);
                SaveTransaction();
            }
            catch (Exception)
            {

            }
        }
    }
}
