﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityAuditMemberService : BaseService<QualityAuditMemberDTO, QualityAuditMemberBO>, IQualityAuditMemberService
    {
        public IEnumerable<QualityAuditMemberBO> GetAll(int pageIndex, int pageSize, out int count, Guid? qualityAuditPlanDetailId)
        {
            var data = base.GetQuery().Where(c => c.QualityAuditPlanDetailId == qualityAuditPlanDetailId);
            count = data.Count();
            var results = Page(data.OrderBy(i => i.RoleType), pageIndex, pageSize).ToList();
            var employeeIds = results.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results
                      .Select(i =>
                      {
                          i.Member = employees.FirstOrDefault(u => u.Id == i.EmployeeId);
                          return i;
                      }).ToList();
            return results;
        }
        public EmployeeBO GetLeader(Guid qualityAuditProgramId)
        {
            var qualityAuditPlanDetailId = ServiceFactory<QualityAuditProgramService>().GetById(qualityAuditProgramId).QualityAuditPlanDetailId;
            var leaderId = base.GetQuery()
                .Where(c => c.QualityAuditPlanDetailId == qualityAuditPlanDetailId && c.RoleType == (int)iDAS.Service.Common.Resource.RoleAudit.Leader)
                .Where(c => c.EmployeeId.HasValue)
                .Select(c => c.EmployeeId.Value)
                .FirstOrDefault();
            return ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(leaderId);
        }
        public void InsertMember(string qualityAuditMembers = default(string), bool allowSave = true)
        {
            if (!string.IsNullOrEmpty(qualityAuditMembers) && qualityAuditMembers != default(string))
            {
                var dataMembers = JsonConvert.DeserializeObject<List<QualityAuditMemberBO>>(qualityAuditMembers);
                if (dataMembers.Count > 0)
                {
                    var qualityAuditPlanDetailId = dataMembers[0].QualityAuditPlanDetailId.Value;
                    var employeeExits = base.GetQuery()
                        .Where(c => c.QualityAuditPlanDetailId == qualityAuditPlanDetailId)
                        .Where(c => c.EmployeeId.HasValue)
                        .Select(c => c.EmployeeId.Value)
                        .Distinct()
                        .ToList();
                    dataMembers = dataMembers.Where(c => !employeeExits.Any(p => p == c.EmployeeId.Value)).ToList();
                    base.InsertRange(dataMembers, allowSave);
                }
            }
        }
        public void UpdateRoleType(Guid id, int roleType)
        {
            var obj = base.GetById(id);
            obj.RoleType = roleType;
            base.Update(obj);
        }


        public void UpdateNotAvailable(Guid id, bool isNotAvailable)
        {
            var obj = base.GetById(id);
            obj.IsNotAvailable = isNotAvailable;
            base.Update(obj);
        }


        public bool ExitsLeader(Guid id)
        {
            var obj = base.GetById(id);
            return base.GetQuery().Any(c => c.QualityAuditPlanDetailId == obj.QualityAuditPlanDetailId && c.RoleType == (int)iDAS.Service.Common.Resource.RoleAudit.Leader);
        }

    }
}
