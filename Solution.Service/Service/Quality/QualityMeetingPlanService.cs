﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityMeetingPlanService : BaseService<QualityMeetingPlanDTO, QualityMeetingPlanBO>, IQualityMeetingPlanService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public QualityMeetingPlanBO GetFormItem(string id)
        {
            QualityMeetingPlanBO result;
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    result = new QualityMeetingPlanBO()
                    {
                        UnitTime = (int)Resource.UnitTime.Day,
                        MeetingValue = 1,
                        IsRoutine = true,
                        LastTime = 1,
                        Status = (int)Resource.MeetingStatus.New
                    };
                    result.Detail = new QualityMeetingPlanDetailBO()
                    {
                        OrderTime = 1,
                        Status = (int)Resource.MeetingStatus.New
                    };
                }
                else
                {
                    var planId = new Guid(id);
                    result = GetById(planId);
                    var lastMeeting = ServiceFactory<QualityMeetingPlanDetailService>().GetNewestMeeting(planId);
                    lastMeeting.StartAtTime = lastMeeting.StartAt.Value.TimeOfDay;
                    lastMeeting.EndAtTime = lastMeeting.EndAt.Value.TimeOfDay;
                    result.Detail = lastMeeting;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public Guid Save(QualityMeetingPlanBO data, bool autoSave = true)
        {
            var planId = Guid.Empty;
            if (data.Id == null || data.Id == Guid.Empty)
                planId = base.Insert(data);
            else
            {
                planId = data.Id;
                base.Update(data);
            }
            data.Detail.QualityMeetingPlanId = planId;
            data.Detail.StartAt = data.Detail.StartAt.Value.Add(data.Detail.StartAtTime.Value);
            data.Detail.EndAt = data.Detail.EndAt.Value.Add(data.Detail.EndAtTime.Value);
            ServiceFactory<QualityMeetingPlanDetailService>().Update(data.Detail);
            SaveTransaction();
            return planId;
        }

        public IEnumerable<QualityMeetingPlanBO> GetData(int pageIndex, int pageSize, out int count, string key, List<int> status = null)
        {
            count = 0;
            var planIds = ServiceFactory<QualityMeetingPlanDetailService>()
                .Get(i => (status == null || status.Count == 0 ||
                    (status.Count > 0 && i.Status.HasValue && status.Contains(i.Status.Value)))
                    && i.QualityMeetingPlanId.HasValue)
                    .AsEnumerable()
                .Select(i => i.QualityMeetingPlanId.Value).ToList();

            var data = base.GetQuery()
                .Where(i => planIds.Contains(i.Id)
                    && (string.IsNullOrEmpty(key)
                    || (!string.IsNullOrEmpty(i.Name) && i.Name.ToUpper().Contains(key.ToUpper())))).AsEnumerable();
            count = data.Count();
            data = Page(data, pageIndex, pageSize);

            // var planIds = data.Select(i => i.Id);
            var planDetails = ServiceFactory<QualityMeetingPlanDetailService>().GetQuery().Where(i => planIds.Contains(i.QualityMeetingPlanId.Value))
                .OrderByDescending(i => i.OrderTime);

            data = data.Select(
                i =>
                {
                    var lastMeetingDetail = planDetails.FirstOrDefault(u => u.QualityMeetingPlanId == i.Id);
                    {
                        if (lastMeetingDetail != null)
                        {
                            i.Detail = lastMeetingDetail;
                            i.MeetingId = lastMeetingDetail.Id;
                            i.MeetingLocation = lastMeetingDetail.Location;

                            i.Status = (int)lastMeetingDetail.Status;
                            i.StatusText = lastMeetingDetail.StatusText;
                            i.StatusColor = lastMeetingDetail.StatusColor;

                            i.MeetingTimeText = lastMeetingDetail.MeetingTimeText;
                            var countMemberJoin = 0;
                            i.MeetingMemberJoin = ServiceFactory<QualityMeetingRecordMemberService>().CountByMeetingId(lastMeetingDetail.Id, out countMemberJoin);
                            i.MeetingMemmberJoinCount = countMemberJoin;
                            var countSubject = 0;
                            i.SubjectText = ServiceFactory<QualityMeetingPlanReportService>().CountByMeetingId(lastMeetingDetail.Id, out countSubject);
                            i.SubjectCount = countSubject;
                            if (lastMeetingDetail.DepartmentId.HasValue)
                            {
                                var department = ServiceFactory<DepartmentService>().GetById(lastMeetingDetail.DepartmentId.Value);
                                i.MeetingDepartmentName = department.Name;
                            }
                            i.IsExpander = i.IsRoutine == true;
                        }
                        else
                        {
                            i.IsExpander = false;
                        }
                    }

                    return i;
                });
            return data;
        }
    }
}
