﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ISOTermService : BaseService<ISOTermDTO, ISOTermBO>, IISOTermService
    {

        public IEnumerable<ISOTermBO> GetAll(int pageIndex, int pageSize, out int count, Guid? isoStandardId, string query = default(string))
        {
            var data = base.GetQuery().Where(c => c.ISOStandardId == isoStandardId)
                .Where(i => string.IsNullOrEmpty(query) || (!string.IsNullOrEmpty(query) && (!string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(query.ToLower())) ||
                              (!string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(query.ToLower()))));
            count = data.Count();
            var result = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            return result;
        }


        public IEnumerable<ISOTermBO> GetByISOStandards(int pageIndex, int pageSize, out int count, Guid planId, Guid programId)
        {
            var isoIds = ServiceFactory<QualityAuditPlanStandardService>().GetQuery().Where(c => c.QualityAuditPlanId == planId).Select(c => c.ISOStandardId).ToList();
            var data = base.GetQuery().Where(c => isoIds.Any(i => i == c.ISOStandardId));
            count = data.Count();
            var result = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            var isoStandardIds = result.Where(c => c.ISOStandardId.HasValue).Select(c => c.ISOStandardId.Value);
            var isoStandards = ServiceFactory<ISOStandardService>().GetByIds(isoStandardIds);
            foreach (var item in result)
            {
                item.IsSelected = ServiceFactory<QualityAuditProgramTermService>().GetQuery().Any(c => c.QualityAuditProgramId == programId && c.ISOTermId == item.Id);
                item.ISOStandardName = isoStandards.FirstOrDefault(c => c.Id == item.ISOStandardId).Name;
            }
            return result;
        }
    }
}
