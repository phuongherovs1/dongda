﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public class QualityTargetCategoryService : BaseService<QualityTargetCategoryDTO, QualityTargetCategoryBO>, IQualityTargetCategoryService
    {
        /// <summary>
        /// Danh sách nhóm mục tiêu theo phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <param name="checkAll"></param>
        /// <returns></returns>
        public IEnumerable<QualityTargetCategoryBO> GetTreeTargetCategory(string departmentId, bool checkAll)
        {
            try
            {
                var targets = ServiceFactory<QualityTargetService>().GetQuery().Where(p => p.IsDelete == false);
                var category = Enumerable.Empty<QualityTargetCategoryBO>();
                if (!String.IsNullOrEmpty(departmentId))
                {
                    if (checkAll)
                        category = base.GetQuery().AsEnumerable().Select(i =>
                        {
                            i.CountTarget = targets.Where(p => p.DepartmentId.HasValue && p.DepartmentId.Value == i.Id).Count();
                            return i;
                        });
                    else
                    {
                        var categoryIds = Enumerable.Empty<Guid>();
                        if (departmentId.Contains('_'))
                        {
                            var department = departmentId.Split('_').ToList();
                            if (department != null && department.Count == 2)
                            {
                                categoryIds = targets.Where(p => p.DepartmentId == new Guid(department[1])).Select(p => p.DepartmentId.Value).Distinct();
                            }
                        }
                        else
                        {
                            categoryIds = targets.Where(p => p.DepartmentId == new Guid(departmentId)).Select(p => p.DepartmentId.Value).Distinct();
                        }
                        category = base.GetByIds(categoryIds).Select(i =>
                        {
                            i.CountTarget = targets.Where(p => p.DepartmentId.HasValue && p.DepartmentId.Value == i.Id).Count();
                            return i;
                        });
                    }
                }
                return category;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Chi tiết nhóm mục tiêu
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public QualityTargetCategoryBO GetDetail(string id)
        {
            try
            {
                var data = new QualityTargetCategoryBO();
                if (!String.IsNullOrEmpty(id))
                {
                    data = GetById(new Guid(id));
                }
                else
                {
                    data = new QualityTargetCategoryBO()
                    {
                        Id = Guid.Empty
                    };
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Xóa nhóm mục tiêu
        /// </summary>
        /// <param name="id"></param>
        public void DeleteCategory(Guid id)
        {
            try
            {
                var target = ServiceFactory<QualityTargetService>().GetQuery().Where(p => p.DepartmentId == id);
                if (target != null && target.Count() > 0)
                    throw new TargetCategoryExitsTarget();
                var category = base.GetById(id);
                if (category.CreateBy != UserId)
                    throw new AccessDenyException();
                base.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
