﻿using iDAS.DataAccess;
using System;
using System.Linq;

namespace iDAS.Service
{
    public class QualityTaskService : BaseService<QualityTaskDTO, QualityTaskBO>, IQualityTaskService
    {
        /// <summary>
        /// Xóa công việc
        /// </summary>
        /// <param name="id"></param>
        public void DeleteTask(Guid id)
        {
            try
            {
                ServiceFactory<TaskService>().Delete(id, false);
                var planTaskIds = base.GetQuery().Where(p => p.TaskId == id && p.IsDelete != true).Select(p => p.Id);
                base.DeleteRange(planTaskIds, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
