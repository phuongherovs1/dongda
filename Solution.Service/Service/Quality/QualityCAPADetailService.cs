﻿using iDAS.DataAccess;
using System;
using System.Linq;

namespace iDAS.Service
{
    public class QualityCAPADetailService : BaseService<QualityCAPADetailDTO, QualityCAPADetailBO>, IQualityCAPADetailService
    {
        /// <summary>
        /// Xóa công việc
        /// </summary>
        /// <param name="id"></param>
        public void DeleteTask(Guid id)
        {
            try
            {
                ServiceFactory<TaskService>().Delete(id, false);
                var CAPATaskIds = base.GetQuery().Where(p => p.TaskId == id && p.IsDelete != true).Select(p => p.Id);
                base.DeleteRange(CAPATaskIds, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeletePerformTask(Guid id)
        {
            try
            {
                ServiceFactory<TaskService>().Delete(id, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
