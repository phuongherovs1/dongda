﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ISOStandardService : BaseService<ISOStandardDTO, ISOStandardBO>, IISOStandardService
    {

        public IEnumerable<ISOStandardBO> GetByPlan(Guid? planId)
        {
            return base.GetQuery()
            .AsEnumerable()
            .Select(i =>
            {
                i.IsSelected = ServiceFactory<QualityAuditPlanStandardService>().GetQuery().Any(c => c.QualityAuditPlanId == planId && c.ISOStandardId == i.Id);
                return i;
            });
        }
    }
}
