﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityAuditProgramService : BaseService<QualityAuditProgramDTO, QualityAuditProgramBO>, IQualityAuditProgramService
    {
        public override QualityAuditProgramBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var obj = base.GetById(id, allowDeleted, allowDefaultIfNull);
            if (obj.AuditBy.HasValue)
                obj.ObjectName = ServiceFactory<EmployeeService>().GetById(obj.AuditBy.Value).Name;
            return obj;
        }
        public IEnumerable<QualityAuditProgramBO> GetAll(int pageIndex, int pageSize, out int count, Guid? qualityAuditPlanDetailId)
        {
            var data = base.GetQuery().Where(c => c.QualityAuditPlanDetailId == qualityAuditPlanDetailId);
            count = data.Count();
            var results = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            var employeeIds = results.Where(i => i.AuditBy.HasValue).Select(i => i.AuditBy.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();

            results = results
                      .Select(i =>
                       {
                           var termIds = ServiceFactory<QualityAuditProgramTermService>().GetQuery().Where(c => c.QualityAuditProgramId == i.Id && c.ISOTermId.HasValue).Select(c => c.ISOTermId.Value).Distinct();
                           var terms = ServiceFactory<ISOTermService>().GetByIds(termIds).Select(c => c.Name).ToList();
                           var departmentIds = ServiceFactory<QualityAuditProgramDepartmentService>().GetQuery().Where(c => c.QualityAuditProgramId == i.Id && c.DepartmentId.HasValue).Select(c => c.DepartmentId.Value).Distinct();
                           var departments = ServiceFactory<DepartmentService>().GetByIds(departmentIds).Select(c => c.Name).ToList();
                           i.Auditer = employees.FirstOrDefault(u => u.Id == i.AuditBy);
                           i.IsAuditer = i.AuditBy == UserId;
                           i.DepartmentAudits = string.Join("<br/>", departments);
                           i.TermAudits = string.Join("<br/>", terms);
                           i.CountReferences = ServiceFactory<QualityAuditNotebookService>().GetQuery().Any(c => c.QualityAuditProgramId == i.Id);
                           return i;
                       }).ToList();
            return results;
        }

        public void UpdateProgram(QualityAuditProgramBO data, string qualityAuditProgramTerms = default(string), string qualityAuditProgramDepartments = default(string), bool allowSave = true)
        {
            if ((!string.IsNullOrEmpty(qualityAuditProgramTerms) && qualityAuditProgramTerms != default(string)) && (!string.IsNullOrEmpty(qualityAuditProgramDepartments) && qualityAuditProgramDepartments != default(string)))
            {
                base.Update(data, false);
                var dataTerms = JsonConvert.DeserializeObject<List<QualityAuditProgramTermBO>>(qualityAuditProgramTerms);
                ServiceFactory<QualityAuditProgramTermService>().UpdateTermRange(dataTerms, data.Id, false);
                var dataDepartments = JsonConvert.DeserializeObject<List<QualityAuditProgramDepartmentBO>>(qualityAuditProgramDepartments);
                ServiceFactory<QualityAuditProgramDepartmentService>().UpdateDepartmentRange(dataDepartments, data.Id, false);
                SaveTransaction(allowSave);
            }
        }
        public Guid InsertProgram(QualityAuditProgramBO data, string qualityAuditProgramTerms = default(string), string qualityAuditProgramDepartments = default(string), bool allowSave = true)
        {
            if ((!string.IsNullOrEmpty(qualityAuditProgramTerms) && qualityAuditProgramTerms != default(string)) && (!string.IsNullOrEmpty(qualityAuditProgramDepartments) && qualityAuditProgramDepartments != default(string)))
            {

                data.Id = Insert(data);
                var dataTerms = JsonConvert.DeserializeObject<List<QualityAuditProgramTermBO>>(qualityAuditProgramTerms);
                ServiceFactory<QualityAuditProgramTermService>().InsertTermRange(dataTerms, data.Id);
                var dataDepartments = JsonConvert.DeserializeObject<List<QualityAuditProgramDepartmentBO>>(qualityAuditProgramDepartments);
                ServiceFactory<QualityAuditProgramDepartmentService>().InsertDepartmentRange(dataDepartments, data.Id);
            }
            return data.Id;
        }
    }
}
