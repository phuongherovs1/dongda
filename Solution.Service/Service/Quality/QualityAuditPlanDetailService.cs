﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityAuditPlanDetailService : BaseService<QualityAuditPlanDetailDTO, QualityAuditPlanDetailBO>, IQualityAuditPlanDetailService
    {
        public override QualityAuditPlanDetailBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var obj = base.GetById(id, allowDeleted, allowDefaultIfNull);
            if (obj.ApproveBy.HasValue)
                obj.EmployeeApproval = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(obj.ApproveBy.Value);
            obj.IsLeader = ServiceFactory<QualityAuditMemberService>().GetQuery().Any(c => c.QualityAuditPlanDetailId == id && c.EmployeeId == UserId && c.RoleType == (int)iDAS.Service.Common.Resource.RoleAudit.Leader);
            return obj;
        }

        public QualityAuditPlanDetailBO GetReportById(Guid id)
        {
            var obj = base.GetById(id);
            //var auditgroup= ServiceFactory<QualityAuditMemberService>()
            //    .GetQuery()
            //    .Where(c=>c.QualityAuditPlanDetailId==id)
            //    .Where(c=>c.RoleType==(int)iDAS.Service.Common.Resource.RoleAudit.Leader || c.RoleType==(int)iDAS.Service.Common.Resource.RoleAudit.Auditer)
            //    .Where(c=>c.EmployeeId.HasValue)
            //    .Select(c=>new {emp=c.EmployeeId.Value, role=c.RoleType})
            //    .ToList();
            //var employees= ServiceFactory<EmployeeService>().GetByIds(auditgroup.Select(c=>c.emp));

            //var leaderIds = auditgroup
            //    .Where(c=>c.role==(int)iDAS.Service.Common.Resource.RoleAudit.Leader)
            //    .Select(c=>c.emp)
            //    .ToList();
            // var auditerIds = auditgroup
            //    .Where(c=>c.role==(int)iDAS.Service.Common.Resource.RoleAudit.Auditer)
            //    .Select(c=>c.emp)
            //    .ToList();
            //var leaders = employees.Where(c=>leaderIds.Any(l=>l==c.Id)).Select(c=>c.Name).ToList();
            //var auditers = employees.Where(c=>auditerIds.Any(l=>l==c.Id)).Select(c=>c.Name).ToList();
            //obj.GroupAudit = "Trưởng nhóm gồm Ông/bà: " + String.Join(", ", leaders) + " " + " Đánh giá viên gồm Ông/bà: " + String.Join(", ", auditers);
            return obj;
        }
        public override Guid Insert(QualityAuditPlanDetailBO data, bool allowSave = true)
        {
            data.ApproveBy = data.EmployeeApproval.Id;
            data.Status = data.IsSend == true ? (int)iDAS.Service.Common.Resource.PlanAuditStatus.Pending : (int)iDAS.Service.Common.Resource.PlanAuditStatus.New;
            return base.Insert(data, allowSave);
        }
        public override void Update(QualityAuditPlanDetailBO data, bool allowSave = true)
        {
            data.ApproveBy = data.EmployeeApproval.Id;
            data.Status = data.IsSend == true ? (int)iDAS.Service.Common.Resource.PlanAuditStatus.Pending : (int)iDAS.Service.Common.Resource.PlanAuditStatus.New;
            base.Update(data, allowSave);
        }
        public void Report(QualityAuditPlanDetailBO data)
        {
            data.Status = (int)iDAS.Service.Common.Resource.PlanAuditStatus.Finished;
            var plan = ServiceFactory<QualityAuditPlanService>().GetById(data.QualityAuditPlanId);
            InsertRoutine(plan.Id, plan.TimeValue, plan.UnitTime, false);
            base.Update(data, false);
            base.SaveTransaction();
        }
        private int GetMaxOrder(Guid planId)
        {
            int max = 0;
            var obj = base.GetQuery().Where(p => p.QualityAuditPlanId == planId);
            if (obj.Count() > 0)
                max = obj.Max(c => c.Orders).Value;
            return max;
        }
        public Guid InsertRoutine(Guid planId, int? timeValue, int? unitTime, bool allowSave = true)
        {
            var order = GetMaxOrder(planId);
            var firstDetail = base.GetQuery().Where(c => c.QualityAuditPlanId == planId && c.Orders == order).FirstOrDefault();
            var obj = new QualityAuditPlanDetailBO();
            obj.QualityAuditPlanId = planId;
            obj.Location = firstDetail != null ? firstDetail.Location : string.Empty;
            obj.Orders = order + 1;
            obj.RequestContents = firstDetail != null ? firstDetail.RequestContents : string.Empty;
            obj.Scope = firstDetail != null ? firstDetail.Scope : string.Empty;
            obj.Year = DateTime.Now.Year;
            obj.Status = (int)iDAS.Service.Common.Resource.PlanAuditStatus.New;
            if (firstDetail != null)
            {
                switch (unitTime)
                {
                    case (int)iDAS.Service.Common.Resource.UnitTime.Day:
                        obj.StartPlanAt = firstDetail.EndAt.HasValue ? firstDetail.EndAt.Value.AddDays(timeValue.HasValue ? timeValue.Value : 0) : DateTime.Now;
                        obj.StartAt = firstDetail.EndAt.HasValue ? firstDetail.EndAt.Value.AddDays(timeValue.HasValue ? timeValue.Value : 0) : DateTime.Now;
                        break;
                    case (int)iDAS.Service.Common.Resource.UnitTime.Month:
                        obj.StartPlanAt = firstDetail.EndAt.HasValue ? firstDetail.EndAt.Value.AddMonths(timeValue.HasValue ? timeValue.Value : 0) : DateTime.Now;
                        obj.StartAt = firstDetail.EndAt.HasValue ? firstDetail.EndAt.Value.AddMonths(timeValue.HasValue ? timeValue.Value : 0) : DateTime.Now;
                        break;
                    case (int)iDAS.Service.Common.Resource.UnitTime.Week:
                        obj.StartPlanAt = firstDetail.EndAt.HasValue ? firstDetail.EndAt.Value.AddDays(7 * (timeValue.HasValue ? timeValue.Value : 0)) : DateTime.Now;
                        obj.StartAt = firstDetail.EndAt.HasValue ? firstDetail.EndAt.Value.AddDays(7 * (timeValue.HasValue ? timeValue.Value : 0)) : DateTime.Now;
                        break;
                    case (int)iDAS.Service.Common.Resource.UnitTime.Year:
                        obj.StartPlanAt = firstDetail.EndAt.HasValue ? firstDetail.EndAt.Value.AddYears((timeValue.HasValue ? timeValue.Value : 0)) : DateTime.Now;
                        obj.StartAt = firstDetail.EndAt.HasValue ? firstDetail.EndAt.Value.AddYears((timeValue.HasValue ? timeValue.Value : 0)) : DateTime.Now;
                        break;
                    default:
                        break;
                }
                obj.ApproveBy = firstDetail.ApproveBy;
                obj.EndAt = obj.StartAt;
            }
            else
            {
                obj.StartPlanAt = DateTime.Now;
                obj.StartAt = DateTime.Now;
                obj.EndAt = obj.StartAt;
            }
            var id = base.Insert(obj, false);
            if (firstDetail != null)
            {
                //Copy dữ liệu bảng chương trình
                var programs = ServiceFactory<QualityAuditProgramService>().GetQuery().Where(c => c.QualityAuditPlanDetailId == firstDetail.Id).ToList();
                if (programs.Count() > 0)
                {
                    var p = new List<QualityAuditProgramBO>();
                    foreach (var item in programs)
                    {
                        var idProgram = Guid.NewGuid();
                        p.Add(new QualityAuditProgramBO
                        {
                            Id = idProgram,
                            QualityAuditPlanDetailId = id,
                            AuditBy = item.AuditBy,
                            BasicAudit = item.BasicAudit,
                            EndAt = item.EndAt,
                            StartAt = item.StartAt
                        });
                        //Copy dữ liệu bảng phòng ban theo chương trình
                        var departments = ServiceFactory<QualityAuditProgramDepartmentService>().GetQuery().Where(c => c.QualityAuditProgramId == item.Id).ToList();
                        if (departments.Count() > 0)
                        {
                            var d = new List<QualityAuditProgramDepartmentBO>();
                            foreach (var department in departments)
                            {
                                d.Add(new QualityAuditProgramDepartmentBO
                                {
                                    QualityAuditProgramId = idProgram,
                                    DepartmentId = department.DepartmentId
                                });
                            }
                            ServiceFactory<QualityAuditProgramDepartmentService>().InsertRange(d, false);
                        }

                        //Copy dữ liệu bảng tiêu chuẩn đánh giá của chương trình
                        var isoTerms = ServiceFactory<QualityAuditProgramTermService>().GetQuery().Where(c => c.QualityAuditProgramId == item.Id).ToList();
                        if (isoTerms.Count() > 0)
                        {
                            var t = new List<QualityAuditProgramTermBO>();
                            foreach (var term in isoTerms)
                            {
                                t.Add(new QualityAuditProgramTermBO
                                {
                                    QualityAuditProgramId = idProgram,
                                    ISOTermId = term.ISOTermId
                                });
                            }
                            ServiceFactory<QualityAuditProgramTermService>().InsertRange(t, false);
                        }
                    }
                    ServiceFactory<QualityAuditProgramService>().InsertRange(p, false);
                }
                //Copy dữ liệu bảng thành viên
                var members = ServiceFactory<QualityAuditMemberService>().GetQuery().Where(c => c.QualityAuditPlanDetailId == firstDetail.Id).ToList();
                if (members.Count() > 0)
                {
                    var m = new List<QualityAuditMemberBO>();
                    foreach (var item in members)
                    {
                        m.Add(new QualityAuditMemberBO
                        {
                            QualityAuditPlanDetailId = id,
                            EmployeeId = item.EmployeeId,
                            RoleType = item.RoleType
                        });
                    }
                    ServiceFactory<QualityAuditMemberService>().InsertRange(m, false);
                }
            }
            return id;
        }

        public IEnumerable<QualityAuditPlanDetailBO> GetAll(int pageIndex, int pageSize, out int count, Guid? planId)
        {
            var data = base.GetQuery().Where(c => c.QualityAuditPlanId == planId).OrderByDescending(i => i.Orders);
            count = data.Count();
            var result = Page(data.OrderByDescending(i => i.Orders), pageIndex, pageSize)
            .AsEnumerable()
            .Select(i =>
            {
                i.IsLeader = ServiceFactory<QualityAuditMemberService>().GetQuery().Any(c => c.QualityAuditPlanDetailId == i.Id && c.EmployeeId == UserId && c.RoleType == (int)iDAS.Service.Common.Resource.RoleAudit.Leader);
                i.IsAllowApproval = (i.ApproveBy == UserId);
                return i;
            });
            return result;
        }

        public void Destroy(Guid id)
        {
            var obj = base.GetById(id);
            obj.Status = (int)iDAS.Service.Common.Resource.PlanAuditStatus.Destroy;
            var plan = ServiceFactory<QualityAuditPlanService>().GetById(obj.QualityAuditPlanId);
            InsertRoutine(plan.Id, plan.TimeValue, plan.UnitTime, false);
            base.Update(obj, false);
            base.SaveTransaction();
        }

        public void Approval(QualityAuditPlanDetailBO data)
        {
            data.Status = data.IsAccept == true ? (int)iDAS.Service.Common.Resource.PlanAuditStatus.Approval : (int)iDAS.Service.Common.Resource.PlanAuditStatus.NotApprove;
            base.Update(data);
        }


        public void ChangeStatus(Guid id, int status)
        {
            var obj = base.GetById(id);
            obj.Status = status;
            base.Update(obj);
        }
        public void Copy(Guid planId, int? timeValue, int? unitTime, bool allowSave = true)
        {
            InsertRoutine(planId, timeValue, unitTime, allowSave);
            base.SaveTransaction();
        }
    }
}
