﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityUnitService : BaseService<QualityUnitDTO, QualityUnitBO>, IQualityUnitService
    {
        /// <summary>
        /// Thêm mới/sửa đơn vị
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid InsertUnit(QualityUnitBO data)
        {
            try
            {
                if (data.Id == null || data.Id == Guid.Empty)
                    data.Id = base.Insert(data, false);
                else
                    base.Update(data, false);
                SaveTransaction(true);
                return data.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Lấy danh sách đơn vị
        /// </summary>
        /// <returns></returns>
        public IEnumerable<QualityUnitBO> GetUnit()
        {
            try
            {
                var data = base.GetQuery().Where(p => p.IsDelete != true).OrderBy(p => p.Name).AsEnumerable();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Thông tin chi tiết đơn vị
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public QualityUnitBO GetDetail(string id)
        {
            try
            {
                var data = new QualityUnitBO();
                if (!String.IsNullOrEmpty(id))
                    data = GetById(new Guid(id));
                else
                {
                    data = new QualityUnitBO()
                    {
                        Id = Guid.Empty
                    };
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
