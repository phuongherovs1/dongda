﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityNCConfirmPermissionService : BaseService<QualityNCConfirmPermissionDTO, QualityNCConfirmPermissionBO>, IQualityNCConfirmPermissionService
    {
        public void InsertPermissionQualityNC(QualityNCConfirmPermissionBO item, string strData)
        {
            if (!string.IsNullOrEmpty(strData))
            {
                var permissions = new List<QualityNCConfirmPermissionBO>();
                var objects = strData.Split(',').ToList();
                if (objects != null && objects.Count() > 0)
                {
                    for (int i = 0; i < objects.Count(); i++)
                    {
                        var name = objects[i].Split('_').ToList();
                        if (name != null && name.Count() == 2)
                        {
                            switch (name[0])
                            {
                                case "Title":
                                    permissions.Add(new QualityNCConfirmPermissionBO
                                    {
                                        DepartmentId = item.DepartmentId,
                                        Description = item.Description,
                                        DepartmentTitleId = new Guid(name[1])

                                    });
                                    break;
                                case "Employee":
                                    permissions.Add(new QualityNCConfirmPermissionBO
                                    {
                                        DepartmentId = item.DepartmentId,
                                        Description = item.Description,
                                        EmployeeId = new Guid(name[1])
                                    });
                                    break;
                            }
                        }
                    }
                }
                base.InsertRange(permissions);
            }
        }


        public IEnumerable<QualityNCConfirmPermissionBO> GetAllNCConfirmPermission(int pageSize, int pageIndex, out int count, Guid departmentId)
        {
            var result = Enumerable.Empty<QualityNCConfirmPermissionBO>();
            result = base.GetQuery().Where(p => p.DepartmentId == departmentId);
            count = result.Count();
            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize)
            .Select(i =>
            {
                if (i.DepartmentTitleId.HasValue && i.DepartmentTitleId != Guid.Empty)
                    i.DepartmentTitle = ServiceFactory<DepartmentTitleService>().GetById(i.DepartmentTitleId.Value);
                else
                    i.Employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.EmployeeId.Value);
                return i;
            });
            return data;
        }


        public QualityNCConfirmPermissionBO GetDetail(Guid id)
        {
            var obj = base.GetById(id);
            obj.DepartmentName = ServiceFactory<DepartmentService>().GetById(obj.DepartmentId.Value).Name;
            if (obj.DepartmentTitleId.HasValue)
                obj.ObjectName = ServiceFactory<DepartmentTitleService>().GetById(obj.DepartmentTitleId.Value).Name;
            else
                obj.ObjectName = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(obj.EmployeeId.Value).Name;
            return obj;
        }

        public void UpdatePermission(QualityNCConfirmPermissionBO item)
        {
            var obj = base.GetById(item.Id);
            obj.DepartmentTitleId = (item.DepartmentTitleId.HasValue && item.DepartmentTitleId != Guid.Empty) ? item.DepartmentTitleId.Value : Guid.Empty;
            obj.EmployeeId = (item.EmployeeId.HasValue && item.EmployeeId != Guid.Empty) ? item.EmployeeId.Value : Guid.Empty;
            obj.Description = item.Description;
            obj.DepartmentId = item.DepartmentId;
            base.Update(obj);
        }

    }
}
