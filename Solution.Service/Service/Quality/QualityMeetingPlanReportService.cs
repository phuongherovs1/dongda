﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityMeetingPlanReportService : BaseService<QualityMeetingPlanReportDTO, QualityMeetingPlanReportBO>, IQualityMeetingPlanReportService
    {
        public IEnumerable<QualityMeetingPlanReportBO> GetSubjectByMeeting(Guid meetingId)
        {
            var data = base.GetQuery().Where(i => i.QualityMeetingPlanDetailId == meetingId).AsEnumerable();
            var employeeIds = data.Where(i => i.Reporter.HasValue).Select(i => i.Reporter.Value);
            var employees = ServiceFactory<EmployeeService>().GetByIds(employeeIds);

            data = data.Select(i =>
            {
                var department = ServiceFactory<DepartmentService>().GetById(i.DepartmentId.Value);
                var reporter = employees.FirstOrDefault(u => i.Reporter == u.Id);

                i.DepartmentName = department.Name;
                if (reporter != null)
                {
                    i.ReporterName = reporter.Name;
                }
                return i;
            });
            return data;
        }

        public IEnumerable<QualityMeetingPlanReportBO> GetByMeeting(Guid meetingId)
        {
            var data = base.GetQuery().Where(i => i.QualityMeetingPlanDetailId == meetingId).AsEnumerable();
            return data;
        }

        public QualityMeetingPlanReportBO GetFormItem(string id, Guid meetingId)
        {
            QualityMeetingPlanReportBO data;
            if (string.IsNullOrEmpty(id))
            {
                data = new QualityMeetingPlanReportBO()
                {
                    QualityMeetingPlanDetailId = meetingId
                };
            }
            else
            {
                data = base.GetById(new Guid(id));
                var derpartment = ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value);
                data.DepartmentName = derpartment.Name;

            }
            return data;
        }

        public Guid Save(QualityMeetingPlanReportBO data, string issueJsonData)
        {
            try
            {
                if (data.Id != Guid.Empty)
                {
                    base.Update(data);
                }
                else
                {
                    data.Id = base.Insert(data, false);
                    var issueIds = JsonConvert.DeserializeObject<List<Guid>>(issueJsonData);
                    var reportIssues = new List<QualityMeetingPlanReportIssueBO>();
                    foreach (var issueId in issueIds)
                    {
                        var item = new QualityMeetingPlanReportIssueBO()
                        {
                            QualityIssueId = issueId,
                            QualityMeetingPlanReportId = data.Id
                        };
                        reportIssues.Add(item);
                    }
                    ServiceFactory<QualityMeetingPlanReportIssueService>().InsertRange(reportIssues, false);
                    SaveTransaction();
                }

                return data.Id;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Đưa ra thông tin thành phần tham gia cuộc họp
        /// </summary>
        /// <param name="meetingId"></param>
        /// <returns></returns>
        public string CountByMeetingId(Guid meetingId, out int count)
        {
            var result = string.Empty;
            var data = base.GetQuery().Where(i => i.QualityMeetingPlanDetailId == meetingId);
            count = data.Count();
            result = count == 0 ? "Chưa thiết lập" : count == 1 ? data.FirstOrDefault().Contents : count.ToString();
            return result;

        }


        public void SaveReport(QualityMeetingPlanReportBO data)
        {
            data.Reporter = UserId;
            base.Update(data);
        }
    }
}
