﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Linq;

namespace iDAS.Service
{
    public class QualityCAPAService : BaseService<QualityCAPADTO, QualityCAPABO>, IQualityCAPAService
    {
        /// <summary>
        /// Tạo/cập nhật yêu cầu hủy
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public QualityCAPABO CreateRequestQualityCAPA(Guid id)
        {
            QualityCAPABO data;
            QualityNCBO QualityNC;
            if (id == Guid.Empty)
                throw new DataIsNotValidException();
            else QualityNC = ServiceFactory<QualityNCService>().GetById(id);
            if (QualityNC != null)
            {

                if (QualityNC.ConfirmBy.HasValue && QualityNC.ConfirmBy != Guid.Empty)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(QualityNC.ConfirmBy.Value);
                    QualityNC.EmployeeConfirmBy = employee;
                }

                if (QualityNC.DepartmentId.HasValue && QualityNC.DepartmentId != Guid.Empty)
                {
                    var NameDepartment = ServiceFactory<DepartmentService>().GetById(QualityNC.DepartmentId.Value);
                    QualityNC.DepartmentName = NameDepartment.Name;
                }

            }
            var RequestQualityCAPA = base.Get(p => p.QualityNCId == id).FirstOrDefault();
            if (RequestQualityCAPA != null)
            {
                data = RequestQualityCAPA;
                if (RequestQualityCAPA.AssignBy.HasValue && RequestQualityCAPA.AssignBy != Guid.Empty)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(RequestQualityCAPA.AssignBy.Value);
                    data.EmployeePerformBy = employee;
                }
                data.QualityNC = QualityNC;
            }
            else
            {
                data = new QualityCAPABO()
                {
                    QualityNC = QualityNC,
                    QualityNCId = id
                };
            }

            return data;
        }
    }
}
