﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityAuditNotebookService : BaseService<QualityAuditNotebookDTO, QualityAuditNotebookBO>, IQualityAuditNotebookService
    {

        public IEnumerable<QualityAuditNotebookBO> GetAll(int pageIndex, int pageSize, out int count, Guid? programId)
        {
            var data = base.GetQuery()
                .Where(c => c.QualityAuditProgramId == programId);
            count = data.Count();
            var results = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            var departmentIds = results.Where(i => i.DepartmentId.HasValue).Select(i => i.DepartmentId.Value).Distinct().ToList();
            var departments = ServiceFactory<DepartmentService>().GetByIds(departmentIds).ToList();
            results = results
                      .Select(i =>
                      {
                          if (i.DepartmentId.HasValue)
                              i.DepartmentName = departments.FirstOrDefault(c => c.Id == i.DepartmentId).Name;
                          else
                              i.DepartmentName = "Bản nháp";
                          i.IsAuditer = i.AuditBy == UserId;
                          return i;

                      }).ToList();
            return results;
        }
    }
}
