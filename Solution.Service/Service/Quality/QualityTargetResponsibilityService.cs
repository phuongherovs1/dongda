﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityTargetResponsibilityService : BaseService<QualityTargetResponsibilityDTO, QualityTargetResponsibilityBO>, IQualityTargetResponsibilityService
    {
        /// <summary>
        /// Danh sách trách nhiệm theo mục tiêu
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="targetId"></param>
        /// <returns></returns>
        public IEnumerable<QualityTargetResponsibilityBO> GetByTargetId(int pageIndex, int pageSize, out int count, string targetId)
        {
            try
            {
                var data = Enumerable.Empty<QualityTargetResponsibilityBO>();
                var employees = ServiceFactory<EmployeeService>().GetQuery();
                var departments = ServiceFactory<DepartmentService>().GetQuery();
                var titles = ServiceFactory<EmployeeTitleService>().GetQuery();
                var departmentTitle = ServiceFactory<DepartmentTitleService>().GetQuery();
                if (!String.IsNullOrEmpty(targetId) && new Guid(targetId) != Guid.Empty)
                {
                    data = base.GetQuery().Where(p => p.QualityTargetId == new Guid(targetId) && p.IsDelete != true);
                    data = Page(data, pageIndex, pageSize)
                    .Select(i =>
                    {
                        var employee = employees.FirstOrDefault(p => p.Id == i.EmployeeId.Value);
                        i.EmployeeName = employee == null ? string.Empty : employee.Name;
                        var title = titles.Where(p => p.EmployeeId == i.EmployeeId.Value).FirstOrDefault();
                        if (title != null)
                        {
                            var result = departmentTitle.FirstOrDefault(p => p.Id == title.TitleId);
                            if (result.DepartmentId.HasValue)
                            {
                                var department = departments.FirstOrDefault(p => p.Id == result.DepartmentId.Value);
                                i.DepartmentName = department == null ? string.Empty : department.Name;
                            }
                        }
                        return i;
                    });
                }
                count = data.Count();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Thêm mới trách nhiệm
        /// </summary>
        /// <param name="targetId"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public IEnumerable<Guid> Create(Guid targetId, string jsonData = "", bool allowSave = true)
        {
            try
            {
                var result = Enumerable.Empty<Guid>();
                if (!String.IsNullOrEmpty(jsonData))
                {
                    var data = JsonConvert.DeserializeObject<List<QualityTargetResponsibilityBO>>(jsonData);
                    var createResponsibilities = ServiceFactory<QualityTargetResponsibilityService>().GetQuery().Where(p => p.QualityTargetId == targetId &&
                                                    p.RoleType == (int)Resource.TargetResponsibility.Create && p.IsDelete != true);
                    if (data != null && data.Count() > 0)
                    {
                        var ids = ServiceFactory<QualityTargetResponsibilityService>().Get(p => p.QualityTargetId == targetId &&
                                        (p.RoleType == (int)Resource.TargetResponsibility.Approval ||
                                         p.RoleType == (int)Resource.TargetResponsibility.Control)).Select(p => p.Id);
                        if (ids != null && ids.Count() > 0)
                            base.DeleteRange(ids, true);
                        var datas = new List<QualityTargetResponsibilityBO>();
                        foreach (var item in data)
                        {
                            item.QualityTargetId = targetId;
                            if (createResponsibilities != null && createResponsibilities.Count() > 0)
                            {
                                if (item.RoleType != (int)Resource.TargetResponsibility.Create)
                                    datas.Add(item);
                            }
                            else
                                datas.Add(item);

                        }
                        result = InsertRange(datas, false);
                    }
                    SaveTransaction(allowSave);
                }
                return result;
            }
            catch
            {
                throw;
            }
        }

        public bool CheckResponsibilityCurrentUser(Guid targetId)
        {
            return base.GetQuery().Any(p => p.QualityTargetId == targetId && p.IsDelete != true && p.EmployeeId == UserId);
        }
    }
}
