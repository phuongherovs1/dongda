﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityTargetIssueService : BaseService<QualityTargetIssueDTO, QualityTargetIssueBO>, IQualityTargetIssueService
    {
        /// <summary>
        /// Thêm mới vấn đề
        /// </summary>
        /// <param name="data"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public Guid InsertIssue(QualityTargetIssueBO data)
        {
            try
            {
                var exist = base.GetQuery().Where(p => p.Name == data.Name && p.Id != data.Id);
                if (exist != null && exist.Count() > 0)
                    throw new DataHasBeenExistedException();
                if (data.Id == null || data.Id == Guid.Empty)
                    data.Id = base.Insert(data, false);
                else
                    base.Update(data, false);
                SaveTransaction(true);
                return data.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Danh sách vấn đề theo nhóm mục tiêu
        /// </summary>
        /// <returns></returns>
        public IEnumerable<QualityTargetIssueBO> GetIssue()
        {
            try
            {
                var data = base.GetQuery().AsEnumerable();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Chi tiết vấn đề
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public QualityTargetIssueBO GetDetail(string id)
        {
            try
            {
                var data = new QualityTargetIssueBO();
                if (!String.IsNullOrEmpty(id))
                    data = GetById(new Guid(id));
                else
                {
                    data = new QualityTargetIssueBO()
                    {
                        Id = Guid.Empty
                    };
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
