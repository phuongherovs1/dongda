﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityAuditProgramTermService : BaseService<QualityAuditProgramTermDTO, QualityAuditProgramTermBO>, IQualityAuditProgramTermService
    {
        public void InsertTermRange(List<QualityAuditProgramTermBO> data, Guid programId, bool autoSave = true)
        {
            foreach (var item in data)
            {
                item.QualityAuditProgramId = programId;
            }
            base.InsertRange(data, autoSave);
        }
        public void UpdateTermRange(List<QualityAuditProgramTermBO> data, Guid programId, bool autoSave = true)
        {
            var iSOTermIds = data.Select(i => i.ISOTermId).ToList();
            var notexits = base.GetQuery()
                .Where(c => c.QualityAuditProgramId == programId && !iSOTermIds.Any(i => i == c.ISOTermId))
                .Select(c => c.Id);
            base.DeleteRange(notexits, false);
            foreach (var item in data)
            {
                var obj = base.GetQuery().Any(c => c.ISOTermId == item.ISOTermId && c.QualityAuditProgramId == programId);
                if (!obj)
                {
                    item.QualityAuditProgramId = programId;
                    base.Insert(item, autoSave);
                }
            }
        }
    }
}
