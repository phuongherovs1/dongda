﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Linq;

namespace iDAS.Service
{
    public class QualityMeetingRecordService : BaseService<QualityMeetingRecordDTO, QualityMeetingRecordBO>, IQualityMeetingRecordService
    {

        public QualityMeetingRecordBO GetFormItem(Guid meetingId)
        {
            var data = base.Get(i => i.QualityMeetinPlanDetailId == meetingId).FirstOrDefault();
            if (data == null)
            {

                var meeting = ServiceFactory<QualityMeetingPlanDetailService>().GetById(meetingId);
                data = new QualityMeetingRecordBO()
                {
                    QualityMeetinPlanDetailId = meetingId,
                    Contents = GetContentRecord(meetingId),
                    MeetingStatus = meeting.Status
                };
            }
            return data;
        }

        public void Finish(Guid meetingId)
        {
            var meeting = ServiceFactory<QualityMeetingPlanDetailService>().GetById(meetingId);
            meeting.Status = (int)Resource.MeetingStatus.Finish;
            ServiceFactory<QualityMeetingPlanDetailService>().Update(meeting);
        }

        public string GetContentRecord(Guid meetingId)
        {
            var reports = ServiceFactory<QualityMeetingPlanReportService>().GetByMeeting(meetingId).ToList();
            var contents = string.Empty;
            int index = 1;
            foreach (var report in reports)
            {
                //var subject = string.Format("<b>{0}. {1}</b> </br>", index, report.Contents);
                //var reportContent = string.Format("Nội dung báo cáo: {0}.</br>" , report.ReportContent);
                //var suggest = string.Format("Đề xuất ý kiến: {0}.</br>" , report.ReportSuggest);
                var subject = string.Format("{0}. {1}", index, report.Contents);
                var reportContent = report.ReportContent;
                var suggest = report.Contents;
                contents += subject + reportContent + suggest;
                index++;
            }
            return contents;
        }

        public void Save(QualityMeetingRecordBO data)
        {
            if (data.Id == Guid.Empty)
            {
                base.Insert(data);
            }
            else
            {
                base.Update(data);
            }
        }
    }
}
