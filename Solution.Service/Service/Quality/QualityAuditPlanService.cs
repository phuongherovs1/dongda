﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityAuditPlanService : BaseService<QualityAuditPlanDTO, QualityAuditPlanBO>, IQualityAuditPlanService
    {
        public Guid InsertPlan(QualityAuditPlanBO data, string qualityAuditPlanStandards = default(string), bool allowSave = true)
        {
            if (!string.IsNullOrEmpty(qualityAuditPlanStandards) && qualityAuditPlanStandards != default(string))
            {
                var dataProperties = JsonConvert.DeserializeObject<List<QualityAuditPlanStandardBO>>(qualityAuditPlanStandards);
                var isoIds = dataProperties.Select(c => c.ISOStandardId.Value).ToList();
                //var exits = CheckExits(data.Id, data.IsRoutine, data.TimeValue, data.UnitTime, isoIds);
                //if (exits)
                //{
                //    var ex = new DataHasBeenExistedException();
                //    throw ex;
                //}
                //else
                {
                    data.Id = Insert(data);
                    ServiceFactory<QualityAuditPlanStandardService>().InsertStandardRange(dataProperties, data.Id);
                    ServiceFactory<QualityAuditPlanDetailService>().InsertRoutine(data.Id, data.TimeValue, data.UnitTime);
                }
            }
            return data.Id;
        }


        public bool CheckExits(Guid? planId, bool? isRountine, int? timeValue, int? unitTime, List<Guid> iSOStandardIds)
        {
            var plans = base.GetQuery().Where(c => c.IsRoutine == isRountine && c.TimeValue == timeValue && c.UnitTime == unitTime && c.Id != planId).ToList();
            foreach (var item in plans)
            {
                var e = ServiceFactory<QualityAuditPlanStandardService>().GetQuery()
                    .Where(c => c.QualityAuditPlanId == item.Id)
                    .Select(c => c.ISOStandardId.Value)
                    .ToList();
                if (e.Count() == iSOStandardIds.Count() && e.Concat(iSOStandardIds).Distinct().Count() == e.Count())
                    return true;
            }
            return false;
        }


        public IEnumerable<QualityAuditPlanBO> GetAll()
        {
            IEnumerable<QualityAuditPlanBO> result_all;
            List<QualityAuditPlanBO> result = new List<QualityAuditPlanBO>();
            try
            {
                result_all = base.GetQuery().AsEnumerable();
                if (result_all != null)
                {
                    foreach (var item in result_all)
                    {
                        var isoStandardIds = ServiceFactory<QualityAuditPlanStandardService>().GetQuery().Where(c => c.QualityAuditPlanId == item.Id).Select(c => c.ISOStandardId.Value).ToList();
                        var isos = ServiceFactory<ISOStandardService>().GetByIds(isoStandardIds).Select(c => c.Name).ToList();
                        item.ISO = string.Join(" & ", isos);
                        result.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result.AsEnumerable();
        }


        public void UpdatePlan(QualityAuditPlanBO data, string qualityAuditPlanStandards = default(string), bool allowSave = true)
        {
            if (!string.IsNullOrEmpty(qualityAuditPlanStandards) && qualityAuditPlanStandards != default(string))
            {
                var dataProperties = JsonConvert.DeserializeObject<List<QualityAuditPlanStandardBO>>(qualityAuditPlanStandards);
                var isoIds = dataProperties.Select(c => c.ISOStandardId.Value).ToList();
                var exits = CheckExits(data.Id, data.IsRoutine, data.TimeValue, data.UnitTime, isoIds);
                if (exits)
                {
                    var ex = new DataHasBeenExistedException();
                    throw ex;
                }
                else
                {
                    base.Update(data, false);
                    ServiceFactory<QualityAuditPlanStandardService>().UpdateStandardRange(dataProperties, data.Id, false);
                    SaveTransaction(allowSave);
                }
            }
        }
    }
}
