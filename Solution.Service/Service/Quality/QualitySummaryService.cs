﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualitySummaryService : BaseService<QualityTargetDTO, QualityTargetBO>, IQualitySummaryService
    {
        public List<PieChartBO> TargetStatusAnalytic(Guid departmentId)
        {

            var summaryData = new List<PieChartBO>();
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.TargetStatusText.WaitApproval,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.TargetStatus.WaitApproval && c.DepartmentId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.TargetStatusText.Approve,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.TargetStatus.Approve && c.DepartmentId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.TargetStatusText.NotApprove,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.TargetStatus.NotApprove && c.DepartmentId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.TargetStatusText.Perform,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.TargetStatus.Perform && c.DepartmentId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.TargetStatusText.Expire,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.TargetStatus.Expire && c.DepartmentId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.TargetStatusText.Pass,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.TargetStatus.Pass && c.DepartmentId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.TargetStatusText.NotPass,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.TargetStatus.NotPass && c.DepartmentId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.TargetStatusText.Destroy,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.TargetStatus.Destroy && c.DepartmentId == departmentId).Count()
            });
            return summaryData;

        }

        public List<PieChartBO> QualityNCAnalytic(Guid departmentId)
        {
            var summaryData = new List<PieChartBO>();
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.QualityNCStatusText.WaitApprove,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.QualityNCStatus.WaitApprove && c.DepartmentId == departmentId).Count()
            });

            summaryData.Add(new PieChartBO()
            {
                Name = Resource.QualityNCStatusText.Approve,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.QualityNCStatus.Approve && c.DepartmentId == departmentId).Count()
            });

            summaryData.Add(new PieChartBO()
            {
                Name = Resource.QualityNCStatusText.NotApprove,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.QualityNCStatus.NotApprove && c.DepartmentId == departmentId).Count()
            });

            summaryData.Add(new PieChartBO()
            {
                Name = Resource.QualityNCStatusText.WaitConfirm,
                Value = ServiceFactory<QualityNCService>().GetQuery().Where(c => c.Status == (int)Resource.QualityNCStatus.WaitConfirm && c.DepartmentId == departmentId).Count()
            });

            summaryData.Add(new PieChartBO()
            {
                Name = Resource.QualityNCStatusText.Confirm,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.QualityNCStatus.Confirm && c.DepartmentId == departmentId).Count()
            });

            summaryData.Add(new PieChartBO()
            {
                Name = Resource.QualityNCStatusText.NotConfirm,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.QualityNCStatus.NotConfirm && c.DepartmentId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.QualityNCStatusText.Pass,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.QualityNCStatus.Pass && c.DepartmentId == departmentId).Count()
            });

            summaryData.Add(new PieChartBO()
            {
                Name = Resource.QualityNCStatusText.NotPass,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.QualityNCStatus.NotPass && c.DepartmentId == departmentId).Count()
            });

            summaryData.Add(new PieChartBO()
            {
                Name = Resource.QualityNCStatusText.Destroy,
                Value = ServiceFactory<QualityTargetService>().GetQuery().Where(c => c.Status == (int)Resource.QualityNCStatus.Destroy && c.DepartmentId == departmentId).Count()
            });
            return summaryData;
        }
    }
}
