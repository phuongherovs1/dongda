﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityAuditPlanStandardService : BaseService<QualityAuditPlanStandardDTO, QualityAuditPlanStandardBO>, IQualityAuditPlanStandardService
    {

        public void InsertStandardRange(List<QualityAuditPlanStandardBO> data, Guid planId, bool autoSave = true)
        {
            foreach (var item in data)
            {
                item.QualityAuditPlanId = planId;
            }
            base.InsertRange(data, autoSave);
        }

        public void UpdateStandardRange(List<QualityAuditPlanStandardBO> data, Guid planId, bool autoSave = true)
        {
            var isoIds = data.Select(i => i.ISOStandardId).ToList();
            var notexits = base.GetQuery()
                .Where(c => c.QualityAuditPlanId == planId &&
                isoIds.Any(i => i != c.ISOStandardId))
                .Select(c => c.Id);
            base.DeleteRange(notexits, false);
            foreach (var item in data)
            {
                var obj = base.GetQuery().Any(c => c.ISOStandardId == item.ISOStandardId && c.QualityAuditPlanId == planId);
                if (!obj)
                {
                    item.QualityAuditPlanId = planId;
                    base.Insert(item, autoSave);
                }
            }
        }
    }
}
