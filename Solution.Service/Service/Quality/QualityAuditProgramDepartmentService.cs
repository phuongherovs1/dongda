﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityAuditProgramDepartmentService : BaseService<QualityAuditProgramDepartmentDTO, QualityAuditProgramDepartmentBO>, IQualityAuditProgramDepartmentService
    {
        public void InsertDepartmentRange(List<QualityAuditProgramDepartmentBO> data, Guid programId, bool autoSave = true)
        {
            foreach (var item in data)
            {
                item.QualityAuditProgramId = programId;
            }
            base.InsertRange(data, autoSave);
        }
        public void UpdateDepartmentRange(List<QualityAuditProgramDepartmentBO> data, Guid programId, bool autoSave = true)
        {
            var departmentIds = data.Select(i => i.DepartmentId).ToList();
            var notexits = base.GetQuery()
                .Where(c => c.QualityAuditProgramId == programId && !departmentIds.Any(i => i == c.DepartmentId))
                .Select(c => c.Id);
            base.DeleteRange(notexits, false);
            foreach (var item in data)
            {
                var obj = base.GetQuery().Any(c => c.DepartmentId == item.DepartmentId && c.QualityAuditProgramId == programId);
                if (!obj)
                {
                    item.QualityAuditProgramId = programId;
                    base.Insert(item, autoSave);
                }
            }
        }
        public List<Guid> GetDepartmentByProgram(Guid programId)
        {
            var data = base.GetQuery().Where(c => c.QualityAuditProgramId == programId).Where(c => c.DepartmentId.HasValue).Select(c => c.DepartmentId.Value).Distinct().ToList();
            return data;
        }
    }
}
