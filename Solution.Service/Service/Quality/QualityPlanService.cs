﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityPlanService : BaseService<QualityPlanDTO, QualityPlanBO>, IQualityPlanService
    {
        /// <summary>
        /// Thêm mới/ sửa kế hoạch
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid Create(QualityPlanBO data, string TaskId = "")
        {
            try
            {
                // Cập nhật mục tiêu về trạng thái chờ duyệt
                if (data.QualityTargetId.HasValue)
                {
                    var target = ServiceFactory<QualityTargetService>().GetById(data.QualityTargetId.Value);
                    if (target != null)
                    {
                        target.Status = (int)Resource.TargetStatus.WaitApproval;
                        ServiceFactory<QualityTargetService>().Update(target);
                    }
                }
                //data.ApproveBy = data.EmployeeApproveBy.Id;
                if (data.Id == null || data.Id == Guid.Empty)
                {
                    data.Status = data.Status == null ? (int)Resource.QualityPlan.New : data.Status;
                    data.Id = base.Insert(data);
                }
                else
                {
                    base.Update(data);
                }
                //cập nhật các task vào trong bảng QualityTask
                if (TaskId != "")
                {
                    List<string> listTask = TaskId.Split(',').ToList();
                    var qualityPlanTask = ServiceFactory<QualityTaskService>().GetQuery().Where(p => p.QualityPlanId == data.Id);
                    foreach (var item in listTask)
                    {
                        if (qualityPlanTask.Where(m => m.TaskId.Value.ToString().Equals(item)).Any() == false)
                        {
                            // Thêm vào bảng trung gian giữa kế hoạch và công việc
                            var QualityTask = new QualityTaskBO()
                            {
                                QualityPlanId = data.Id,
                                TaskId = Guid.Parse(item)
                            };
                            ServiceFactory<QualityTaskService>().Insert(QualityTask);
                        }
                    }
                }
                return data.Id;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Cập nhật kế hoạch
        /// </summary>
        /// <param name="data"></param>
        public void UpdatePlan(QualityPlanBO data)
        {
            try
            {
                data.ApproveBy = data.EmployeeApproveBy.Id;
                base.Update(data, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Thông tin chi tiết kế hoạch
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <param name="targetId"></param>
        /// <returns></returns>
        public QualityPlanBO GetDetail(string id, string departmentId, Guid targetId)
        {
            try
            {
                var target = ServiceFactory<QualityTargetService>().GetById(targetId);
                if (target.CreateBy != UserId)
                    throw new AccessDenyException();
                var data = new QualityPlanBO();
                if (!String.IsNullOrEmpty(id))
                {
                    data = base.GetById(new Guid(id));
                    data.TargetName = target == null ? string.Empty : target.Name;
                    var department = ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value);
                    data.DepartmentName = department == null ? string.Empty : department.Name;
                    if (data.ApproveBy.HasValue && data.ApproveBy != Guid.Empty)
                    {
                        var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.ApproveBy.Value);
                        data.EmployeeApproveBy = employee;
                    }
                    if (data.CreateBy.HasValue && data.CreateBy != Guid.Empty)
                    {
                        var employeeCreate = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.CreateBy.Value);
                        data.EmployeeCreateBy = employeeCreate;
                    }
                    if (data.Status.HasValue)
                    {
                        if (data.Status == (int)Resource.QualityPlan.Approved)
                            data.IsAccept = true;
                        if (data.Status == (int)Resource.QualityPlan.NotApprove)
                            data.IsAccept = false;
                    }
                    var totalTask = ServiceFactory<QualityTaskService>().GetQuery().Where(p => p.QualityPlanId == data.Id && p.IsDelete != true).Select(p => p.TaskId.Value).Distinct();
                    var taskFinish = ServiceFactory<TaskService>().GetQuery().Where(p => totalTask.Contains(p.Id) && p.IsFinish == true && p.IsDelete != true);
                    data.TotalTask = totalTask == null ? 0 : totalTask.Count();
                    data.TaskFinish = taskFinish == null ? 0 : taskFinish.Count();
                    data.ResultTask = data.TotalTask == 0 ? 0 : (float)(data.TaskFinish.Value / data.TotalTask.Value) * 100;
                }
                else
                {
                    var department = new DepartmentBO();
                    if (departmentId.Contains('_'))
                    {
                        var items = departmentId.Split('_').ToList();
                        if (items != null && items.Count == 2)
                        {
                            department = ServiceFactory<DepartmentService>().GetById(new Guid(items[1]));
                        }
                    }
                    else
                    {
                        department = ServiceFactory<DepartmentService>().GetById(new Guid(departmentId));
                    }
                    data = new QualityPlanBO()
                    {
                        Id = Guid.Empty,
                        QualityTargetId = targetId,
                        TargetName = target == null ? string.Empty : target.Name,
                        DepartmentId = department == null ? (Guid?)null : department.Id,
                        DepartmentName = department == null ? string.Empty : department.Name,
                        StartAt = DateTime.Now
                    };
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Xóa kế hoạch
        /// </summary>
        /// <param name="id"></param>
        public void DeletePlan(Guid id)
        {
            try
            {
                var data = base.GetById(id);
                if (data.CreateBy != UserId)
                    throw new AccessDenyException();
                var qualityPlanTaskIds = ServiceFactory<QualityTaskService>().GetQuery().Where(p => p.QualityPlanId == data.Id && p.IsDelete != true).AsEnumerable().Select(p => p.TaskId.Value);
                ServiceFactory<TaskService>().DeleteRange(qualityPlanTaskIds, false);
                base.Delete(id, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Danh sách kế hoạch theo mục tiêu
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="targetId"></param>
        /// <returns></returns>
        public IEnumerable<QualityPlanBO> GetByTargetId(int pageIndex, int pageSize, out int count, Guid targetId, string filter, Resource.QualityPlan status)
        {
            try
            {
                var data = base.GetQuery().Where(p => p.QualityTargetId == targetId && p.IsDelete != true)
                    .Where(p => status == Resource.QualityPlan.All ? true : p.Status == (int)status).AsEnumerable();
                if (!String.IsNullOrEmpty(filter))
                    data = data.Where(p => p.Name.ToLower().Contains(filter.ToLower()));
                count = data.Count();
                var result = Page(data, pageIndex, pageSize);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Gửi phê duyệt kế hoạch
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public void Send(QualityPlanBO data, string TaskId = "")
        {
            data.Status = (int)Resource.QualityPlan.WaitApprove;
            Create(data, TaskId);
        }

        /// <summary>
        /// Phê duyệt kế hoạch
        /// </summary>
        /// <param name="data"></param>
        public void Approval(QualityPlanBO data)
        {
            try
            {
                var qualityPlanApproval = base.GetById(data.Id);
                if (qualityPlanApproval.ApproveBy.HasValue && qualityPlanApproval.ApproveBy != UserId)
                    throw new AccessDenyException();
                if (data.IsAccept == true)
                {
                    data.Status = (int)Resource.QualityPlan.Approved;
                    var target = ServiceFactory<QualityTargetService>().GetById(data.QualityTargetId.Value);
                    target.Status = (int)Resource.TargetStatus.WaitApproval;
                    ServiceFactory<QualityTargetService>().Update(target, false);
                }
                else
                    data.Status = (int)Resource.QualityPlan.NotApprove;
                base.Update(data, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
