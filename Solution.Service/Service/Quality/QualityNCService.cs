﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class QualityNCService : BaseService<QualityNCDTO, QualityNCBO>, IQualityNCService
    {



        public IEnumerable<QualityNCBO> GetDataQualityNC(int pageIndex, int pageSize, out int count, Guid? departmentId, string filterName = default(string), Common.Resource.QualityNCStatus status = 0)
        {
            //GET Dữ liệu quản lý sự không phù hợp theo từng phòng ban 
            var dataQuery = base.GetQuery()
                .Where(i => (status == Resource.QualityNCStatus.All ? true : (i.Status.Value == (int)status)) && (i.IsOld != true) && (i.DepartmentId == departmentId) && (i.CreateBy == UserId || i.ApproveBy == UserId || i.ConfirmBy == UserId || i.ApproveBy == UserId))
                .Where(i => string.IsNullOrEmpty(filterName) || (!string.IsNullOrEmpty(filterName) && (!string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(filterName.ToLower()))
               ))
                .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            //Lấy danh sách người phát hiện sự phát sinh
            var employeeIds = results.Where(i => i.DiscovereBy.HasValue).Select(i => i.DiscovereBy.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results.Select(i =>
            {
                var memberCount = 0;
                // Lấy số thành phần liên quan
                i.MemberRelation = ServiceFactory<QualityNCRelationService>().CountQualityNCRelation(i.Id, out memberCount);
                i.EmployeeDiscovereBy = employees.FirstOrDefault(u => u.Id == i.DiscovereBy);
                return i;
            }).ToList();
            return results;
        }
        public IEnumerable<QualityNCBO> GetAllDataQualityNC(int pageIndex, int pageSize, out int count, string filterName = default(string), Common.Resource.QualityNCStatus status = 0)
        {
            //GET Dữ liệu quản lý sự không phù hợp theo từng phòng ban 
            var dataQuery = base.GetQuery()
                .Where(i => (status == Resource.QualityNCStatus.All ? true : (i.Status.Value == (int)status)) && (i.IsOld != true) && (i.CreateBy == UserId || i.ApproveBy == UserId || i.ConfirmBy == UserId || i.ApproveBy == UserId))
                .Where(i => string.IsNullOrEmpty(filterName) || (!string.IsNullOrEmpty(filterName) && (!string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(filterName.ToLower()))
               ))
                .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            //Lấy danh sách người phát hiện sự phát sinh
            var employeeIds = results.Where(i => i.DiscovereBy.HasValue).Select(i => i.DiscovereBy.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results.Select(i =>
            {
                var memberCount = 0;
                // Lấy số thành phần liên quan
                i.MemberRelation = ServiceFactory<QualityNCRelationService>().CountQualityNCRelation(i.Id, out memberCount);
                i.EmployeeDiscovereBy = employees.FirstOrDefault(u => u.Id == i.DiscovereBy);
                return i;
            }).ToList();
            return results;
        }
        public QualityNCBO DetailQualityNC(Guid id)
        {  //Lấy chi tiết dữ liệu sự không phù hợp theo Id
            var QualityNC = base.GetById(id);
            //Kiểm tra QualityNC có bị null hay không
            if (QualityNC != null)
            {

                if (QualityNC.ConfirmBy.HasValue && QualityNC.ConfirmBy != Guid.Empty)
                {
                    //  Lấy người xác nhận
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(QualityNC.ConfirmBy.Value);
                    QualityNC.EmployeeConfirmBy = employee;
                }

                if (QualityNC.DepartmentId.HasValue && QualityNC.DepartmentId != Guid.Empty)
                {
                    //Lấy tên phòng ban
                    var NameDepartment = ServiceFactory<DepartmentService>().GetById(QualityNC.DepartmentId.Value);
                    QualityNC.DepartmentName = NameDepartment.Name;
                }

            }
            return QualityNC;
        }

        public IEnumerable<QualityNCBO> GetDataWaitConfirmNC(int pageIndex, int pageSize, out int count)
        {
            // Get dữ liệu chờ xác nhận
            var dataQuery = base.GetQuery()
                .Where(i => i.Status == (int)iDAS.Service.Common.Resource.QualityNCStatus.WaitConfirm)
                .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            var employeeIds = results.Where(i => i.DiscovereBy.HasValue).Select(i => i.DiscovereBy.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results.Select(i =>
            {
                i.EmployeeDiscovereBy = employees.FirstOrDefault(u => u.Id == i.DiscovereBy);
                return i;
            }).ToList();
            return results;
        }

        public IEnumerable<QualityNCBO> GetDataNotConfirmNC(int pageIndex, int pageSize, out int count)
        {
            var dataQuery = base.GetQuery()
                .Where(i => i.Status == (int)iDAS.Service.Common.Resource.QualityNCStatus.NotConfirm)
                .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            var employeeIds = results.Where(i => i.DiscovereBy.HasValue).Select(i => i.DiscovereBy.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results.Select(i =>
            {
                i.EmployeeDiscovereBy = employees.FirstOrDefault(u => u.Id == i.DiscovereBy);
                return i;
            }).ToList();
            return results;
        }


        public QualityNCBO CreatePerformQualityCAPA(Guid id)
        {
            QualityNCBO QualityNC;
            if (id == Guid.Empty)
                throw new DataIsNotValidException();
            else QualityNC = base.GetById(id);
            if (QualityNC != null)
            {

                if (QualityNC.ConfirmBy.HasValue && QualityNC.ConfirmBy != Guid.Empty)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(QualityNC.ConfirmBy.Value);
                    QualityNC.EmployeeConfirmBy = employee;
                }

                if (QualityNC.DepartmentId.HasValue && QualityNC.DepartmentId != Guid.Empty)
                {
                    var NameDepartment = ServiceFactory<DepartmentService>().GetById(QualityNC.DepartmentId.Value);
                    QualityNC.DepartmentName = NameDepartment.Name;
                }

            }

            return QualityNC;
        }

        public IEnumerable<QualityNCBO> GetDataConfirmNC(int pageIndex, int pageSize, out int count)
        {

            var dataQuery = base.GetQuery()
                .Where(i => (i.Status == (int)iDAS.Service.Common.Resource.QualityNCStatus.Confirm || i.Status == (int)iDAS.Service.Common.Resource.QualityNCStatus.Pass || i.Status == (int)iDAS.Service.Common.Resource.QualityNCStatus.Approve || i.Status == (int)iDAS.Service.Common.Resource.QualityNCStatus.WaitApprove) && (i.IsOld != true))
                .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            var employeeIds = results.Where(i => i.DiscovereBy.HasValue).Select(i => i.DiscovereBy.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results.Select(i =>
            {
                i.EmployeeDiscovereBy = employees.FirstOrDefault(u => u.Id == i.DiscovereBy);
                i.CountReferences = i.OldIds == null ? 0 : 1;
                return i;
            }).ToList();
            return results;
        }


        public Guid InsertQualityNCApproval(QualityNCBO item, string datajson = "", bool allowSave = true)
        {
            item.Status = (int)iDAS.Service.Common.Resource.QualityNCStatus.Confirm;
            item.Id = InsertQualityNC(item, datajson);
            return item.Id;
        }
        public void DestroyQualityNC(Guid Id)
        {
            var item = base.GetById(Id);
            item.Status = (int)iDAS.Service.Common.Resource.QualityNCStatus.Destroy;
            base.Update(item);
        }

        public Guid InsertQualityNC(QualityNCBO item, string datajson = "", bool allowSave = true)
        {
            // Insert dữ liệu không phù hợp vào bảng dữ liệu
            if (item.Status == null)
            {
                if (item.QualityTargetId == null || item.QualityTargetId == Guid.Empty)
                    item.Status = (int)iDAS.Service.Common.Resource.QualityNCStatus.WaitConfirm;
                else
                    item.Status = (int)iDAS.Service.Common.Resource.QualityNCStatus.Confirm;
            }

            item.ConfirmBy = item.EmployeeConfirmBy.Id;
            item.DiscovereBy = UserId;
            item.Id = Insert(item);
            if (!string.IsNullOrEmpty(datajson))
            {
                var QualityNCRelation = JsonConvert.DeserializeObject<List<QualityNCRelationBO>>(datajson);

                var details = new List<QualityNCRelationBO>();
                foreach (var it in QualityNCRelation)
                {
                    it.QualityNCId = item.Id;
                    // Insert nhân sự , phòng ban , chức danh
                    if (it.TypeId.Contains('_'))
                    {
                        string[] DepartmentIdsl = it.TypeId.Trim().Split('_');
                        if (DepartmentIdsl[0].ToString().Trim().Equals("Employee"))
                        {
                            it.EmployeeId = new Guid(DepartmentIdsl[1]);
                            it.Flag = 1;
                        }

                        if (DepartmentIdsl[0].ToString().Trim().Equals("Title"))
                        {
                            it.DepartmentTitleId = new Guid(DepartmentIdsl[1]);
                            it.Flag = 2;
                        }

                        if (DepartmentIdsl[0].ToString().Trim().Equals("Department"))
                        {
                            it.DepartmentId = new Guid(DepartmentIdsl[1]);
                            it.Flag = 3;
                        }
                    }
                    details.Add(it);
                }

                ServiceFactory<QualityNCRelationService>().InsertRange(details, true);
            }
            if (item.QualityAuditNotebookId != Guid.Empty)
            {
                var notebookNC = new QualityAuditNotebookNCBO();
                notebookNC.QualityNCId = item.Id;
                notebookNC.QualityAuditNotebookId = item.QualityAuditNotebookId;
                ServiceFactory<QualityAuditNotebookNCService>().Insert(notebookNC);
            }
            //SaveTransaction(allowSave);
            return item.Id;
        }



        public void UpdateQualityNC(QualityNCBO item, string datajson = "", bool allowSave = true)
        {
            // Cập nhật sự không phu hợp
            item.ConfirmBy = item.EmployeeConfirmBy.Id;
            item.DiscovereBy = UserId;
            base.Update(item, false);
            var ids = ServiceFactory<QualityNCRelationService>().Get(m => m.QualityNCId == item.Id).Select(i => i.Id);
            ServiceFactory<QualityNCRelationService>().DeleteRange(ids, false);
            if (!string.IsNullOrEmpty(datajson))
            {
                var QualityNCRelation = JsonConvert.DeserializeObject<List<QualityNCRelationBO>>(datajson);

                var details = new List<QualityNCRelationBO>();
                foreach (var it in QualityNCRelation)
                {
                    it.QualityNCId = item.Id;
                    if (it.TypeId != null)
                    {
                        if (it.TypeId.Contains('_'))
                        {
                            string[] DepartmentIdsl = it.TypeId.Trim().Split('_');
                            if (DepartmentIdsl[0].ToString().Trim().Equals("Employee"))
                            {
                                it.EmployeeId = new Guid(DepartmentIdsl[1]);
                                it.Flag = 1;
                            }

                            if (DepartmentIdsl[0].ToString().Trim().Equals("Title"))
                            {
                                it.DepartmentTitleId = new Guid(DepartmentIdsl[1]);
                                it.Flag = 2;
                            }

                            if (DepartmentIdsl[0].ToString().Trim().Equals("Department"))
                            {
                                it.DepartmentId = new Guid(DepartmentIdsl[1]);
                                it.Flag = 3;

                            }
                        }
                    }
                    else
                    {
                        if (it.Flag == 1)
                        {
                            it.EmployeeId = it.IdRelation;
                        }

                        if (it.Flag == 2)
                        {
                            it.DepartmentTitleId = it.IdRelation;
                        }

                        if (it.Flag == 3)
                        {
                            it.DepartmentId = it.IdRelation;
                        }
                    }


                    details.Add(it);
                }

                ServiceFactory<QualityNCRelationService>().InsertRange(details, false);
            }
            SaveTransaction(allowSave);
        }
        /// <summary>
        /// Gửi duyệt
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>

        public void SendApprovalNC(QualityNCBO item, bool allowSave = true)
        {

            item.ApproveBy = item.EmployeeApprovalBy.Id;
            item.ConfirmBy = item.EmployeeConfirmBy.Id;
            item.Status = (int)iDAS.Service.Common.Resource.QualityNCStatus.WaitApprove;
            base.Update(item, false);
            SaveTransaction(allowSave);
        }
        /// <summary>
        /// Phê duyệt
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        public void ApprovalNC(QualityNCBO item, bool allowSave = true)
        {
            item.ConfirmBy = item.EmployeeConfirmBy.Id;
            item.ApproveBy = UserId;
            item.ApproveAt = DateTime.Now;
            if (item.IsAccept == true)
            {
                item.Status = (int)iDAS.Service.Common.Resource.QualityNCStatus.Approve;
            }
            else
            {
                item.Status = (int)iDAS.Service.Common.Resource.QualityNCStatus.NotApprove;
            }
            base.Update(item, false);
            SaveTransaction(allowSave);
        }

        public IEnumerable<QualityNCBO> GetDataRowExpander(int pageIndex, int pageSize, out int count, Guid Id)
        {
            var item = base.GetById(Id);
            if (item.OldIds.Contains(','))
            {
                var OldIdsl = item.OldIds.Trim().Split(',').ToList();
                var dataQuery = base.GetQuery()
                .Where(i => OldIdsl.Contains(i.Id.ToString()))
                .OrderByDescending(i => i.CreateAt);
                count = dataQuery.Count();
                var results = Page(dataQuery, pageIndex, pageSize).ToList();
                var employeeIds = results.Where(i => i.DiscovereBy.HasValue).Select(i => i.DiscovereBy.Value).Distinct();
                var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
                results = results.Select(i =>
                {
                    i.EmployeeDiscovereBy = employees.FirstOrDefault(u => u.Id == i.DiscovereBy);
                    return i;
                }).ToList();
                return results;
            }
            else
            {
                var IdConvert = iDAS.Service.Common.Utilities.ConvertToGuid(item.OldIds);
                var dataQuery = base.GetQuery()
                 .Where(i => i.Id == IdConvert)
                 .OrderByDescending(i => i.CreateAt);
                count = dataQuery.Count();
                var results = Page(dataQuery, pageIndex, pageSize).ToList();
                var employeeIds = results.Where(i => i.DiscovereBy.HasValue).Select(i => i.DiscovereBy.Value).Distinct();
                var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
                results = results.Select(i =>
                {
                    i.EmployeeDiscovereBy = employees.FirstOrDefault(u => u.Id == i.DiscovereBy);
                    return i;
                }).ToList();
                return results;
            }


        }


        public IEnumerable<QualityNCBO> GetDataByIds(List<Guid> ids, int pageIndex, int pageSize, out int count)
        {
            var dataQuery = base.GetQuery()
                .Where(i => ids.Any(c => c == i.Id))
                .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            var employeeIds = results.Where(i => i.DiscovereBy.HasValue).Select(i => i.DiscovereBy.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            var departmentIds = results.Where(i => i.DepartmentId.HasValue).Select(i => i.DepartmentId.Value).Distinct();
            var departments = ServiceFactory<DepartmentService>().GetByIds(departmentIds).ToList();
            results = results.Select(i =>
            {
                if (i.DepartmentId.HasValue)
                    i.DepartmentName = departments.FirstOrDefault(c => c.Id == i.DepartmentId).Name;
                i.EmployeeDiscovereBy = employees.FirstOrDefault(u => u.Id == i.DiscovereBy);
                return i;
            }).ToList();
            return results;
        }
        public void ConfirmNCs(string qualityNCs = default(string), bool allowSave = true)
        {
            if (!string.IsNullOrEmpty(qualityNCs) && qualityNCs != default(string))
            {
                var dataNCs = JsonConvert.DeserializeObject<List<QualityNCBO>>(qualityNCs);
                foreach (var item in dataNCs)
                {
                    base.Update(item, false);
                }
                base.SaveTransaction();
            }
        }

        /// <summary>
        /// Xóa sự không phù hợp
        /// </summary>
        /// <param name="id"></param>
        public void DeleteQualityNC(Guid id)
        {
            try
            {
                base.Delete(id, false);
                var IdRelations = ServiceFactory<QualityNCRelationService>().GetQuery().Where(p => p.QualityNCId == id && p.IsDelete != true).Select(p => p.Id);
                ServiceFactory<QualityNCRelationService>().DeleteRange(IdRelations, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Gửi đến khung chưa xác nhận
        /// </summary>
        /// <param name="id"></param>
        public void FromWaitConfirm(Guid id)
        {
            try
            {
                var item = base.GetById(id);
                item.IsOld = false;
                item.Status = (int)iDAS.Service.Common.Resource.QualityNCStatus.WaitConfirm;
                base.Update(item, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Gửi đến khung xác nhận
        /// </summary>
        /// <param name="id"></param>
        public void ToConfirm(Guid id)
        {
            try
            {
                var item = base.GetById(id);
                item.Status = (int)iDAS.Service.Common.Resource.QualityNCStatus.Confirm;
                base.Update(item, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Gửi đền khung không đạt
        /// </summary>
        /// <param name="id"></param>
        public void ToNotPass(Guid id)
        {
            try
            {
                var item = base.GetById(id);
                item.Status = (int)iDAS.Service.Common.Resource.QualityNCStatus.NotConfirm;
                base.Update(item, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Kéo vào nhau
        /// </summary>
        /// <param name="toId"></param>
        /// <param name="?"></param>
        public void ToFromQuality(Guid toId, Guid formId, bool allowSave = true)
        {
            try
            {
                var itemform = base.GetById(formId);
                itemform.IsOld = true;
                base.Update(itemform, false);

                var itemto = base.GetById(toId);
                itemto.Status = (int)iDAS.Service.Common.Resource.QualityNCStatus.Confirm;
                if (itemto.OldIds == null)
                {
                    if (itemform.OldIds == null)
                        itemto.OldIds = itemform.Id.ToString().Trim();
                    else
                        itemto.OldIds = itemform.Id.ToString().Trim() + "," + itemform.OldIds.Trim();
                }
                else
                {
                    if (itemform.OldIds == null)
                        itemto.OldIds = itemto.OldIds + "," + itemform.Id.ToString().Trim();
                    else
                        itemto.OldIds = itemto.OldIds + "," + itemform.Id.ToString().Trim() + "," + itemform.OldIds.Trim();
                }
                base.Update(itemto, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }



        /// <summary>
        /// Danh sách sự không phù hợp theo mục tiêu
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="targetId"></param>
        /// <returns></returns>
        public IEnumerable<QualityNCBO> GetByTargetId(int pageIndex, int pageSize, out int count, Guid targetId, string filter, Resource.QualityNCStatus status)
        {
            try
            {
                var dataQuery = base.GetQuery().Where(p => p.QualityTargetId == targetId)
                                .Where(p => status == Resource.QualityNCStatus.All ? true : p.Status == (int)status)
                                .Where(p => String.IsNullOrEmpty(filter) ? true : p.Name.ToLower().Contains(filter.ToLower()))
                                .OrderByDescending(i => i.CreateAt);
                count = dataQuery.Count();
                var results = Page(dataQuery, pageIndex, pageSize).ToList();
                var employeeIds = results.Where(i => i.DiscovereBy.HasValue).Select(i => i.DiscovereBy.Value).Distinct();
                var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
                results = results.Select(i =>
                {
                    var memberCount = 0;
                    i.MemberRelation = ServiceFactory<QualityNCRelationService>().CountQualityNCRelation(i.Id, out memberCount);
                    i.EmployeeDiscovereBy = employees.FirstOrDefault(u => u.Id == i.DiscovereBy);
                    return i;
                }).ToList();
                return results;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public IEnumerable<QualityNCBO> GetTotalNCs(List<Guid> ids, int pageIndex, int pageSize, out int count)
        {
            var dataQuery = base.GetQuery()
                .Where(i => ids.Any(c => c == i.Id))
                .GroupBy(c => c.DepartmentId)
                .Select(c => c.FirstOrDefault())
                .OrderByDescending(c => c.CreateAt);
            var dataNC = base.GetQuery()
                .Where(i => ids.Any(c => c == i.Id))
                .ToList();
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            var departmentIds = results.Where(i => i.DepartmentId.HasValue).Select(i => i.DepartmentId.Value).Distinct();
            var departments = ServiceFactory<DepartmentService>().GetByIds(departmentIds).ToList();
            results = results.Select(i =>
            {
                i.TotalMax = dataNC.Where(c => c.DepartmentId == i.DepartmentId && c.Type == (int)iDAS.Service.Common.Resource.QualityNCTypeStatus.M).Count();
                i.Totalmin = dataNC.Where(c => c.DepartmentId == i.DepartmentId && c.Type == (int)iDAS.Service.Common.Resource.QualityNCTypeStatus.m).Count();
                i.TotalObs = dataNC.Where(c => c.DepartmentId == i.DepartmentId && c.Type == (int)iDAS.Service.Common.Resource.QualityNCTypeStatus.obs).Count();
                if (i.DepartmentId.HasValue)
                    i.DepartmentName = departments.FirstOrDefault(c => c.Id == i.DepartmentId).Name;
                return i;
            }).ToList();
            return results;
        }
    }
}
