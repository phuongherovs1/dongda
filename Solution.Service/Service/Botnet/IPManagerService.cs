﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class IPManagerService : BaseService<IPManagerDTO, IPManagerBO>, IIPManagerService
    {
        public IEnumerable<IPManagerBO> GetAll(int pageIndex, int pageSize, bool history, out int count)
        {
            if (history)
            {
                count = Get(x => x.CountInfection > 0).Count();
                var data = Page(Get(x => x.CountInfection > 0).OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
                return data;

            }
            else
            {
                count = GetAll().Count();
                var data = Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
                return data;
            }
        }

        public IEnumerable<IPManagerBO> GetByfilterName(int pageIndex, int pageSize, string filterName, bool history, out int count)
        {
            if (history)
            {
                filterName = filterName.ToLower();
                count = Get(x => (x.dia_chi_ip.ToLower().Contains(filterName) || x.ten_to_chuc.ToLower().Contains(filterName)) && x.CountInfection > 0
                ).Count();
                var data = Page(Get(x => (x.dia_chi_ip.ToLower().Contains(filterName) || x.ten_to_chuc.ToLower().Contains(filterName)) && x.CountInfection > 0
                ).OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
                return data;
            }
            else
            {
                filterName = filterName.ToLower();
                count = Get(x => x.dia_chi_ip.ToLower().Contains(filterName) || x.ten_to_chuc.ToLower().Contains(filterName)
                ).Count();
                var data = Page(Get(x => x.dia_chi_ip.ToLower().Contains(filterName) || x.ten_to_chuc.ToLower().Contains(filterName)
                ).OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
                return data;
            }
        }
        public IEnumerable<IPManagerBO> GetByabbreviation(int pageIndex, int pageSize, string abbreviation, bool history, out int count)
        {
            abbreviation = abbreviation.ToLower();
            count = Get(x => x.ma_to_chuc.ToLower().Contains(abbreviation)).Count();
            var data = Page(Get(x => x.ma_to_chuc.ToLower().Contains(abbreviation)).OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            return data;
        }
        public IPManagerBO GetByIp(string filterName, out int count)
        {
            count = Get(x => x.dia_chi_ip.Equals(filterName.Trim())
            ).Count();
            var data = Get(x => x.dia_chi_ip.Equals(filterName.Trim())
            ).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            return data;
        }
        public IPManagerBO GetFormItem()
        {
            try
            {
                var data = new IPManagerBO()
                {
                    Id = Guid.NewGuid()
                };
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Guid Insert(IPManagerBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.dia_chi_ip == data.dia_chi_ip && i.Id != data.Id).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            data.Id = Insert(data, true);
            return data.Id;
        }

        public void Update(IPManagerBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Id == data.Id).Any();
            if (!existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            Update(data, true);
        }

        public int Import(List<IPManagerBO> botnetImport)
        {
            throw new NotImplementedException();
        }
        public IEnumerable<IPManagerBO> GetByCountInfection(int pageIndex, int pageSize, out int count)
        {
            count = Get(x => x.CountInfection > 0).Count();
            if (pageSize == 0)
            {
                var data = Get(x => x.CountInfection > 0);
                return data;
            }
            else
            {
                var data = Page(Get(x => x.CountInfection > 0).OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
                return data;
            }
        }
    }
}
