﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service.Service.Botnet
{
    class HistoryBotnetService : BaseService<HistoryBotnetDTO, HistoryBotnetBO>, IHistoryBotnetService
    {
        public IEnumerable<HistoryBotnetBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            count = GetAll().Count();
            var data = Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            return data;
        }
        public Guid Insert(HistoryBotnetBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            data.Id = Insert(data, true);
            return data.Id;
        }
    }
}
