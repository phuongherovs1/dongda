﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service.Service.Botnet
{
    public class BotnetManagerService : BaseService<BotnetManagerDTO, BotnetManagerBO>, IBotnetManagerService
    {
        public IEnumerable<BotnetManagerBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            count = GetAll().Count();
            var data = Page(GetAll().OrderByDescending(i => i.TimeStamp), pageIndex, pageSize);
            return data;
        }

        public IEnumerable<BotnetManagerBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count)
        {
            count = Get(x => x.Ip.Contains(filterName)
                        || x.Port.Contains(filterName)
                        || x.Asn.Contains(filterName)
                        || x.Geo.Contains(filterName)
                        || x.Region.Contains(filterName)
                        || x.City.Contains(filterName)
                        || x.HostName.Contains(filterName)
                        || x.Type.Contains(filterName)
                        || x.Infection.Contains(filterName)
                        || x.Url.Contains(filterName)
                        || x.Agent.Contains(filterName)
            ).Count();
            var data = Page(Get(x => x.Ip.Contains(filterName)
                        || x.Port.Contains(filterName)
                        || x.Asn.Contains(filterName)
                        || x.Geo.Contains(filterName)
                        || x.Region.Contains(filterName)
                        || x.City.Contains(filterName)
                        || x.HostName.Contains(filterName)
                        || x.Type.Contains(filterName)
                        || x.Infection.Contains(filterName)
                        || x.Url.Contains(filterName)
                        || x.Agent.Contains(filterName)
            ).OrderByDescending(i => i.TimeStamp), pageIndex, pageSize);
            return data;
        }

        public BotnetManagerBO GetFormItem()
        {
            try
            {
                var data = new BotnetManagerBO()
                {
                    //Id = Guid.NewGuid(),
                    //Name = string.Empty,
                    //Description = string.Empty
                };
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Guid Insert(BotnetManagerBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            throw new NotImplementedException();
        }

        public void Update(BotnetManagerBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Id == data.Id).Any();
            if (!existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            Update(data, true);
        }

        //public override IEnumerable<Guid> InsertRange(IEnumerable<BotnetManagerBO> botnetImports, bool allowSave = true)
        //{
        //    IEnumerable<Guid> ids = new IEnumerable<Guid>();//InsertRange(botnetImports, false);
        //    //SaveTransaction(allowSave);
        //    //Dispose();
        //    //UnitOfWork unit = new UnitOfWork();
        //    //SetDbContext(unit);
        //    return ids;
        //}

        public int Import(List<BotnetManagerBO> botnetImport)
        {
            throw new NotImplementedException();
        }
    }
}
