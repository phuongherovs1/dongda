﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class BotnetInfectionService : BaseService<BotnetInfectionDTO, BotnetInfectionBO>, IBotnetInfectionService
    {
        public IEnumerable<BotnetInfectionBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            count = GetAll().Count();
            var data = Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            return data;
        }

        public IEnumerable<BotnetInfectionBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count)
        {
            count = Get(x => x.Name.Contains(filterName) || x.Description.Contains(filterName)).Count();
            var data = Page(Get(x => x.Name.Contains(filterName) || x.Description.Contains(filterName)).OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            return data;
        }

        public BotnetInfectionBO GetFormItem()
        {
            try
            {
                var data = new BotnetInfectionBO()
                {
                    Id = Guid.NewGuid(),
                    Name = string.Empty,
                    Description = string.Empty
                };
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Guid Insert(BotnetInfectionBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Name == data.Name && i.Id != data.Id).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            data.Id = Insert(data, true);
            return data.Id;
        }

        public void Update(BotnetInfectionBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Id == data.Id).Any();
            if (!existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            Update(data, true);
        }

        public int Import(List<BotnetInfectionBO> botnetImport)
        {
            throw new NotImplementedException();
        }
    }
}
