﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public class DocumentSuggestResponsibilityService : BaseService<DocumentSuggestResponsibilityDTO, DocumentSuggestResponsibilityBO>, IDocumentSuggestResponsibilityService
    {
        const int _minOrder = 0;
        public IEnumerable<DocumentSuggestResponsibilityBO> GetByCurrentUser()
        {
            var data = Get().Where(i => i.ApproveBy == UserId && !(i.IsNew == null && i.IsApprove == null));
            return data;
        }
        public IEnumerable<DocumentSuggestResponsibilityBO> GetBySuggests(IEnumerable<Guid> suggestIds)
        {
            var data = Get().Where(i => suggestIds.Contains((Guid)i.DocumentSuggestId));
            return data;
        }
        public IEnumerable<DocumentSuggestResponsibilityBO> GetBySuggest(Guid suggestId)
        {
            var data = Get().Where(i => i.DocumentSuggestId == suggestId);
            return data;
        }
        public IEnumerable<DocumentSuggestResponsibilityBO> GetEmployeeRoleBySuggest(Guid suggestId)
        {
            var data = Get().Where(i => i.DocumentSuggestId == suggestId)
                            .Select(item =>
                            {
                                item.EmployeeApprove = ServiceFactory<EmployeeService>().GetById((Guid)item.ApproveBy);
                                return item;
                            });
            return data;
        }
        public IEnumerable<DocumentSuggestResponsibilityBO> GetReviewPerformingByCurrentUser()
        {
            var data = Get().Where(i => i.ApproveBy == UserId
                && i.RoleType == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit && i.IsNew == true && i.IsDelete == false);
            return data;
        }
        public DocumentSuggestResponsibilityBO GetDetail(Guid id)
        {
            var data = GetById(id);
            if (data != null)
            {
                data.DocumentSuggest = ServiceFactory<DocumentSuggestService>().GetDetail((Guid)data.DocumentSuggestId);
            }
            return data;
        }
        public void Send(DocumentSuggestResponsibilityBO data, bool allowSave = true)
        {
            var exit = Get(i => i.DocumentSuggestId == data.DocumentSuggestId && i.RoleType == (int)data.RoleType).Any();
            if (!exit)
            {
                data.IsNew = true;
                base.Insert(data, false);
                // Cập nhật trạng thái cho đề nghị -----------------------------------------------------------------------------
                var status = 0;
                switch (data.RoleType)
                {
                    case (int)Resource.DocumentProcessRole.ReviewWriteAndEdit: status = (int)Resource.DocumentSuggestStatusType.Review;
                        break;
                    case (int)Resource.DocumentProcessRole.ApproveDocument: status = (int)Resource.DocumentSuggestStatusType.Approve;
                        break;
                    default: status = (int)Resource.DocumentSuggestStatusType.New;
                        break;

                }
                ServiceFactory<DocumentSuggestService>().UpdateStatus((Guid)data.DocumentSuggestId, status, data.Order, false);
                //----------------------------------------------------------------------------------------------------------------
                if (allowSave) SaveTransaction(allowSave);
            }
            else
            {
                data.IsNew = true;
                base.Update(data, false);
                // Cập nhật trạng thái cho đề nghị -----------------------------------------------------------------------------
                var status = 0;
                switch (data.RoleType)
                {
                    case (int)Resource.DocumentProcessRole.ReviewWriteAndEdit: status = (int)Resource.DocumentSuggestStatusType.Review;
                        break;
                    case (int)Resource.DocumentProcessRole.ApproveDocument: status = (int)Resource.DocumentSuggestStatusType.Approve;
                        break;
                    default: status = (int)Resource.DocumentSuggestStatusType.New;
                        break;

                }
                ServiceFactory<DocumentSuggestService>().UpdateStatus((Guid)data.DocumentSuggestId, status, data.Order, false);
                //----------------------------------------------------------------------------------------------------------------
                if (allowSave) SaveTransaction(allowSave);

            }
        }
        public void RevertDocument(DocumentSuggestResponsibilityBO data, bool allowSave = true)
        {
            var deleteItem = Get(i => i.DocumentSuggestId == data.DocumentSuggestId && i.RoleType == (int)data.RoleType).FirstOrDefault();
            if (deleteItem != null)
            {
                var revertItem = Get(i => i.DocumentSuggestId == data.DocumentSuggestId && i.Id != deleteItem.Id).OrderByDescending(i => i.Order).FirstOrDefault();
                if (revertItem != null)
                {
                    revertItem.IsAccept = null;
                    revertItem.IsApprove = null;
                    revertItem.IsNew = true;
                    base.Update(revertItem, false);
                    base.Remove(deleteItem.Id, false);
                    // Cập nhật trạng thái cho đề nghị--------------------------------------------------------
                    var status = 0;
                    switch (revertItem.RoleType)
                    {
                        case (int)Resource.DocumentProcessRole.ReviewWriteAndEdit: status = (int)Resource.DocumentSuggestStatusType.Review;
                            break;
                        case (int)Resource.DocumentProcessRole.ApproveDocument: status = (int)Resource.DocumentSuggestStatusType.Approve;
                            break;
                        default: status = (int)Resource.DocumentSuggestStatusType.New;
                            break;

                    }
                    ServiceFactory<DocumentSuggestService>().UpdateStatus((Guid)revertItem.DocumentSuggestId, status, revertItem.Order, false);
                    //--------------------------------------------------------
                }
                else
                {
                    base.Remove(deleteItem.Id, false);
                    // Cập nhật trạng thái cho đề nghị về mới ----------------------------------
                    ServiceFactory<DocumentSuggestService>().UpdateStatus((Guid)deleteItem.DocumentSuggestId, (int)Resource.DocumentSuggestStatusType.New, _minOrder, false);
                    //-------------------------------------------------------------------------
                }
                if (allowSave) SaveTransaction(allowSave);
            }
        }
        public void UpdateApproval(DocumentSuggestResponsibilityBO data)
        {
            var obj = base.GetById(data.Id);
            if (obj != null)
            {
                obj.IsAccept = data.IsAccept;
                obj.IsApprove = true;
                obj.ApproveNote = data.ApproveNote;
            }
            base.Update(obj);
        }

        public void ChangeEmployeeApproval(DocumentSuggestResponsibilityBO data)
        {
            var obj = base.GetById(data.Id);
            if (obj != null)
            {
                obj.ApproveBy = data.EmployeeApprove.Id;
                obj.ApproveNote = string.Empty;
                obj.ContentSuggest = data.ContentSuggest;
            }
            base.Update(obj);
        }
        public void RevertApproval(Guid id)
        {
            var obj = base.GetById(id);
            if (obj != null)
            {
                obj.ApproveNote = string.Empty;
                obj.IsApprove = false;
                obj.IsAccept = false;
            }
            base.Update(obj);
        }
    }
}
