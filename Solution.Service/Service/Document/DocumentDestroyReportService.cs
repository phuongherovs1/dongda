﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentDestroyReportService : BaseService<DocumentDestroyReportDTO, DocumentDestroyReportBO>, IDocumentDestroyReportService
    {
        /// <summary>
        /// Lấy thông tin biên bản theo đề nghị hủy
        /// </summary>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public DocumentDestroyReportBO GetReportByDestroySuggestId(Guid destroySuggestId)
        {
            try
            {
                var suggest = ServiceFactory<DocumentDestroySuggestService>().GetById(destroySuggestId);
                var data = Get(p => p.DestroySuggestId == destroySuggestId).FirstOrDefault();
                if (data == null)
                    data = new DocumentDestroyReportBO();
                data.DestroySuggest = suggest;
                // lấy vai trò người phê duyệt
                var responsibility = ServiceFactory<DocumentDestroySuggestResponsibitityService>().Get(p => p.DocumentDestroyId == destroySuggestId
                                    && p.RoleType == (int)Resource.DocumentProcessRole.ApproveDocument).FirstOrDefault();
                if (responsibility != null)
                {
                    var employee = responsibility.ApproveBy.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(responsibility.ApproveBy.Value) : null;
                    data.EmployeeApproveSuggest = employee;
                    data.ApproveAt = responsibility.ApproveAt;
                    data.SuggestNote = responsibility.ApproveNote;
                }
                // Người tạo đề nghị
                if (suggest.CreateBy.HasValue)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(suggest.CreateBy.Value);
                    data.EmployeeSuggestCreate = employee;
                }
                // Lấy tên tài liệu nếu đề nghị hủy được tạo từ 1 tài liệu phân phối
                var detail = ServiceFactory<DocumentDestroySuggestDetailService>().GetDocumentIdBySuggestId(destroySuggestId);
                // Đề nghị hủy tạo từ tài liệu phân phối thì sẽ có 1 tài liệu
                if (detail != null && detail.Count() == 1)
                {
                    var document = ServiceFactory<DocumentService>().GetById(detail.FirstOrDefault());
                    data.Document = document == null ? new DocumentBO() : document;
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Tạo/Sửa biên bản
        /// </summary>
        /// <param name="report"></param>
        /// <param name="jsonDocument"></param>
        /// <param name="jsonDetail"></param>
        /// <returns></returns>
        public Guid Create(DocumentDestroyReportBO report, string jsonDocument = "", string jsonDetail = "", bool allowSave = true)
        {
            try
            {
                if (report.Id == Guid.Empty)
                    report.Id = base.Insert(report, false);
                else
                    base.Update(report, false);
                if (!String.IsNullOrEmpty(jsonDocument))
                    // Update tài liệu đề nghị hủy
                    ServiceFactory<DocumentDestroySuggestDetailService>().UpdateRange(jsonDocument);
                if (!String.IsNullOrEmpty(jsonDetail))
                {
                    // Tạo chi tiết biên bản
                    ServiceFactory<DocumentDestroyReportDetailService>().Create(report.Id, jsonDetail);
                }
                // Cập nhật đề nghị hủy về trạng thái hủy
                var destroySuggest = ServiceFactory<DocumentDestroySuggestService>().GetById(report.DestroySuggestId.Value);
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.BillCreated;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                // Khi tạo biên bản hủy thì lock vai trò người tạo biên bản hủy
                var lockItem = ServiceFactory<DocumentDestroySuggestResponsibitityService>().Get(i => i.DocumentDestroyId == report.DestroySuggestId.Value
                                && i.RoleType == (int)Resource.DocumentProcessRole.DestroyBillCreater).OrderByDescending(i => i.CreateAt).FirstOrDefault();
                if (lockItem != null)
                {
                    lockItem.IsLock = true;
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Update(lockItem, false);
                }
                SaveTransaction(allowSave);
                return report.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Lấy thông tin biên bản hủy theo yêu cầu hủy
        /// </summary>
        /// <param name="requestDestroyId"></param>
        /// <returns></returns>
        public DocumentDestroyReportBO GetReportByRequestDestroyId(Guid requestDestroyId)
        {
            try
            {
                var request = ServiceFactory<DocumentRequestService>().GetById(requestDestroyId);
                var data = Get(p => p.RequestId == requestDestroyId).FirstOrDefault();
                if (request.CreateBy.HasValue)
                {
                    if (data == null)
                        data = new DocumentDestroyReportBO();
                    // lấy thông tin người yêu cầu
                    var employee = request.CreateBy.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(request.CreateBy.Value) : null;
                    data.EmployeeSuggestCreate = employee;
                    data.RequestAt = request.CreateAt;
                    data.SuggestNote = request.Note;
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Tạo/sửa biên bản hủy từ yêu cầu hủy
        /// </summary>
        /// <param name="report"></param>
        /// <param name="jsonDetail"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid CreateFromRequest(DocumentDestroyReportBO report, string jsonDetail = "", bool allowSave = true)
        {
            try
            {
                if (report.Id == Guid.Empty)
                    report.Id = base.Insert(report, false);
                else
                    base.Update(report, false);
                // Update tài liệu yêu cầu hủy về trạng thái kết thúc
                if (report.RequestId.HasValue)
                {
                    var request = ServiceFactory<DocumentRequestService>().GetById(report.RequestId.Value);
                    request.IsFinish = true;
                    ServiceFactory<DocumentRequestService>().Update(request, false);
                }
                if (!String.IsNullOrEmpty(jsonDetail))
                {
                    // Tạo chi tiết biên bản
                    ServiceFactory<DocumentDestroyReportDetailService>().Create(report.Id, jsonDetail);
                }
                SaveTransaction(allowSave);
                return report.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Tạo mới biên bản cho tài liệu phân phối đề nghị hủy
        /// </summary>
        /// <param name="report"></param>
        /// <param name="jsonDetail"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid CreateReportDistribute(DocumentDestroyReportBO report, string jsonDetail = "", bool allowSave = true)
        {
            try
            {
                if (report.Id == Guid.Empty)
                    report.Id = base.Insert(report, false);
                else
                    base.Update(report, false);
                if (!String.IsNullOrEmpty(jsonDetail))
                {
                    // Tạo chi tiết biên bản bao gồm nhân sự tham gia hủy
                    ServiceFactory<DocumentDestroyReportDetailService>().Create(report.Id, jsonDetail);
                }
                // Cập nhật đề nghị hủy về trạng thái đã lập biên bản và cập nhật hình thức hủy và thời gian thực hiện cho đề nghị hủy
                var destroySuggest = ServiceFactory<DocumentDestroySuggestService>().GetById(report.DestroySuggestId.Value);
                destroySuggest.DestroyStartAt = report.DestroySuggest.DestroyStartAt;
                destroySuggest.DestroyEstimateAt = report.DestroySuggest.DestroyEstimateAt;
                destroySuggest.Type = report.DestroySuggest.Type;
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.BillCreated;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                // Cập nhật tài liệu đề nghị hủy về trạng thái hủy
                var details = ServiceFactory<DocumentDestroySuggestDetailService>().Get(p => p.DestroySuggestId == report.DestroySuggestId.Value);
                if (details != null && details.Count() == 1)
                {
                    var detail = details.FirstOrDefault();
                    detail.IsDestroy = true;
                    ServiceFactory<DocumentDestroySuggestDetailService>().Update(detail, false);
                }
                // Khi tạo biên bản hủy thì lock vai trò người tạo biên bản hủy
                var lockItem = ServiceFactory<DocumentDestroySuggestResponsibitityService>().Get(i => i.DocumentDestroyId == report.DestroySuggestId.Value
                                && i.RoleType == (int)Resource.DocumentProcessRole.DestroyBillCreater).OrderByDescending(i => i.CreateAt).FirstOrDefault();
                if (lockItem != null)
                {
                    lockItem.IsLock = true;
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Update(lockItem, false);
                }
                SaveTransaction(allowSave);
                return report.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Tài liệu yêu cầu hủy
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentRequestBO> GetDocumentByRequestId(Guid requestId)
        {
            try
            {
                var data = ServiceFactory<DocumentRequestService>().Get(p => p.Id == requestId).Select(i =>
                {
                    if (i.DocumentId.HasValue)
                    {
                        var document = ServiceFactory<DocumentService>().GetById(i.DocumentId.Value);
                        i.Document = document;
                    }
                    return i;
                });
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
