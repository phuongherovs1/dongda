﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentPublishService : BaseService<DocumentPublishDTO, DocumentPublishBO>, IDocumentPublishService
    {

        /// <summary>
        /// Ban hành tài liệu
        /// Nếu tài liệu là yêu cầu sửa đổi khi ban hành thì cập nhật tài liệu thành lỗi thời
        /// Nếu tài liệu là yêu cầu tạo mới khi ban hành thì tài liệu cùng ban hành
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public override System.Guid Insert(iDAS.Service.DocumentPublishBO data, bool allowSave = true)
        {
            data.Id = base.Insert(data, true);
            var document = ServiceFactory<DocumentService>().GetById(data.DocumentId.Value);
            if (document != null)
            {
                var items = ServiceFactory<DocumentService>().Get(p => p.Code == document.Code && p.IsObsolate != true && p.Id != data.DocumentId.Value);
                // Nếu có nhiều hơn 1 tài liệu có Code trùng nhau thì cập nhật tài liệu là Lỗi thời
                if (items != null && items.Count() > 0)
                {
                    foreach (var item in items)
                    {
                        item.IsObsolate = true;
                        ServiceFactory<DocumentService>().Update(item, true);
                    }
                }
            }
            SaveTransaction(allowSave);
            return data.Id;
        }
        /// <summary>
        /// Insert tài liệu ban hành
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        public void IsertPublish(DocumentPublishBO data, bool allowSave = true)
        {
            try
            {
                var documents = ServiceFactory<DocumentService>().GetAll();
                var documentCode = string.Empty;
                if (data.DocumentId.HasValue)
                    documentCode = documents.Where(p => p.Id == data.DocumentId.Value).FirstOrDefault().Code;
                data.PublishBy = UserId;
                data.IsPublish = true;
                var documentPublish = base.GetAll();
                if (documentPublish.Any(p => p.DocumentId == data.DocumentId) == false)
                {
                    // 1. kiểm tra tai lieu nay da ban hành hay chưa
                    var times = 1;
                    if (!String.IsNullOrEmpty(documentCode))
                    {
                        var codes = documents.Where(p => p.Code == documentCode && p.Id != data.DocumentId.Value);
                        if (codes != null && codes.Count() > 0)
                        {
                            foreach (var item in codes)
                            {
                                var publish = Get(p => p.DocumentId == item.Id).FirstOrDefault();
                                if (publish != null)
                                {
                                    times++;
                                    publish.IsObsolete = true;
                                    publish.IsPublish = false;
                                    base.Update(publish, true);
                                }
                            }
                        }
                    }
                    data.QuantityPublish = times;
                    // 2. Insert bảng publish
                    data.Id = base.Insert(data, true);
                    // 3. Cập nhật bảng Request
                    var updateRequest = new DocumentResponsibilityBO()
                    {
                        Id = data.RequestId.HasValue ? data.RequestId.Value : Guid.NewGuid(),
                        IsComplete = true
                    };
                    ServiceFactory<DocumentResponsibilityService>().Update(updateRequest, true);

                    // 5. Cập nhật bảng document
                    var document = new DocumentBO()
                    {
                        Id = data.DocumentId.Value,
                        SecurityId = data.Document.SecurityId.HasValue ? Guid.NewGuid() : data.Document.SecurityId,
                        IsHard = data.Document.IsHard,
                        IsSoft = data.Document.IsSoft,
                        IsPublish = data.IsPublish,
                        PublishAt = data.PublishAt,
                        Revision = data.QuantityPublish.ToString(),
                        ApplyAt = data.ApplyAt,
                        ExpireAt = data.ExpireAt
                    };
                    ServiceFactory<DocumentService>().Update(document, true);
                    SaveTransaction(allowSave);
                }
                else
                {
                    throw new FormatException(Resource.ExceptionErrorMessage.DocumentPublishExisted);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Lấy dữ liệu ban hành
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="typeWrite"></param>
        /// <param name="status"></param>
        /// <param name="search">Tìm kiếm theo tên hoặc mã tài liệu</param>
        /// <returns></returns>
        public IEnumerable<DocumentPublishBO> GetdataPublish(int pageIndex, int pageSize, out int count, string search)
        {
            var data = Enumerable.Empty<DocumentPublishBO>();
            try
            {
                data = base.Get();
                count = data.Count();
                data = Page(data, pageIndex, pageSize).ToList();
                var documents = ServiceFactory<DocumentService>().GetByIds(data.Select(i => i.DocumentId.Value).Distinct());
                if (documents != null)
                {
                    data = data.Select(i =>
                    {
                        if (i.DocumentId.HasValue)
                        {
                            var document = documents.Where(u => u.Id == i.DocumentId).FirstOrDefault();
                            i.Document = document;
                            var sercurity = document != null ? document.SecurityId.HasValue ? ServiceFactory<DocumentSecurityService>().GetById(document.SecurityId.Value) : null : null;
                            document.Security = sercurity == null ? null : sercurity;
                            if (document.CategoryId != null && document.CategoryId != Guid.Empty)
                            {
                                var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
                                i.IsInternal = category.IsExternal != true ? true : false;
                                i.CheckCategory = true;
                                i.DocumentCategoryName = category.Name;
                            }
                            else
                            {
                                i.CheckCategory = false;
                            }

                        }
                        return i;
                    }).OrderByDescending(p => p.CreateAt);
                    // Tìm kiếm theo tên hoặc mã tài liệu
                    if (!String.IsNullOrEmpty(search))
                    {
                        data = data.Where(p => p.CodeDocument.ToLower().Contains(search) || p.NameDocument.ToLower().Contains(search));
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return data.ToList();
        }

        /// <summary>
        /// Get dư liệu lần ban hành
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<DocumentPublishBO> GetCoutPublish(int pageIndex, int pageSize, out int count, Guid Id)
        {
            try
            {
                var item = base.GetById(Id);
                var documentitem = ServiceFactory<DocumentService>().GetById(item.DocumentId.Value);

                var data = base.GetAll();
                var documents = ServiceFactory<DocumentService>().GetByIds(data.Select(i => i.DocumentId.Value).Distinct());
                var employees = ServiceFactory<EmployeeService>().GetByIds(data.Select(i => i.CreateBy.Value).Distinct());
                //data = data.Select(i =>
                foreach (var i in data)
                {
                    var employee = employees.Where(u => u.Id == i.CreateBy).FirstOrDefault();
                    i.Publisher = employee;
                    var title = ServiceFactory<EmployeeTitleService>().Get(m => m.EmployeeId == employee.Id).FirstOrDefault();
                    if (title != null)
                    {
                        var result = ServiceFactory<DepartmentTitleService>().GetById(title.TitleId);
                        i.TitleEmploy = result.Name;
                    }
                    var document = documents.Where(u => u.Id == i.DocumentId).FirstOrDefault();
                    i.Document = document;
                    //return i;
                }
                var resultdata = data.Where(i => i.Document.Code == documentitem.Code);
                count = resultdata.Count();
                data = Page(resultdata, pageIndex, pageSize);
                return data;
            }
            catch (Exception)
            {

                throw;
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public DocumentPublishBO GetByDocumentId(Guid documentId)
        {
            var data = Get(i => i.DocumentId == documentId && i.IsPublish == true && i.IsObsolete == null).OrderByDescending(p => p.CreateAt).FirstOrDefault();
            return data;
        }

        public Guid Insert(DocumentPublishBO data, string jsonData)
        {
            data.Id = base.Insert(data, false);
            ServiceFactory<DocumentDestinationService>().InsertRange(jsonData, data.DocumentId.Value, false);
            SaveTransaction();
            return data.Id;
        }

        public DocumentPublishBO CreateDefault(Guid documentId)
        {
            var document = ServiceFactory<DocumentService>().GetById(documentId);
            var data = new DocumentPublishBO()
            {
                Document = document,
                DocumentId = documentId
            };
            return data;
        }



        /// <summary>
        /// Danh sách tài liệu ban hành có trạng thái lỗi thời hoặc hết hạn và những tài liệu yêu cầu hủy để tạo đề nghị hủy
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="isCreate">True: không load chi tiết tài liệu của đề nghị hủy, False: ngược lại</param>
        /// <returns></returns>
        public IEnumerable<DataDocumentDestroyBO> GetAllForDestroySuggest(int pageIndex, int pageSize, out int count, bool isCreate = true, string destroySuggestId = "")
        {
            var lstDocument = Enumerable.Empty<DataDocumentDestroyBO>();
            var documents = ServiceFactory<DocumentService>().GetAll();
            // Lấy tất cả những tài liệu của tất cả đề nghị hủy
            var documentDetail = ServiceFactory<DocumentDestroySuggestDetailService>().GetAll();
            // Lấy những tài liệu hết thời gian lưu trữ
            var documentArchive = ServiceFactory<DocumentArchiveService>().Get(p => (p.StartAt.Value.AddMonths(p.DurationMonth == null ? 0 : p.DurationMonth.Value)) < DateTime.Now);
            if (documentArchive != null && documentArchive.Count() > 0)
            {
                // Gán những tài liệu đã hết thời gian lưu trữ ở trên vào danh sách
                foreach (var n in documentArchive)
                {
                    var documentItem = documents.Where(p => p.Id == n.DocumentId.Value).FirstOrDefault();
                    if (documentItem != null)
                    {
                        var documentDestroy = new DataDocumentDestroyBO();
                        documentDestroy.DocumentId = documentItem.Id;
                        documentDestroy.DocumentName = documentItem.Name;
                        documentDestroy.DocumentCode = documentItem.Code;
                        documentDestroy.StatusText = "Quá hạn";
                        lstDocument = lstDocument.Concat(new[] { documentDestroy });
                    }
                }
            }
            // Lấy những tài liệu có trạng thái quá hạn
            var expireDocument = documents.Where(p => p.IsExpire == true);
            if (expireDocument != null && expireDocument.Count() > 0)
            {
                // Gán những tài liệu đã quá hạn ở trên vào danh sách
                foreach (var n in expireDocument)
                {
                    var documentItem = documents.Where(p => p.Id == n.Id).FirstOrDefault();
                    if (documentItem != null)
                    {
                        var documentDestroy = new DataDocumentDestroyBO();
                        documentDestroy.DocumentId = documentItem.Id;
                        documentDestroy.DocumentName = documentItem.Name;
                        documentDestroy.DocumentCode = documentItem.Code;
                        documentDestroy.StatusText = "Hết hiệu lực";
                        lstDocument = lstDocument.Concat(new[] { documentDestroy });
                    }
                }
            }
            lstDocument = lstDocument.Distinct();
            if (!String.IsNullOrEmpty(destroySuggestId))
            {
                var destroySuggestDetails = documentDetail.Where(p => p.DestroySuggestId != new Guid(destroySuggestId)).Select(n => n.DocumentId);
                lstDocument = lstDocument.Where(p => !destroySuggestDetails.Contains(p.DocumentId));
            }
            count = lstDocument.Count();
            foreach (var item in lstDocument)
            {
                bool exits = false;
                // False: Xem chi tiết tài liệu đã thêm trong đề nghị hủy
                if (!isCreate)
                {
                    if (!String.IsNullOrEmpty(destroySuggestId))
                    {
                        var destroySuggestDetails = documentDetail.Where(p => p.DocumentId == item.DocumentId && p.DestroySuggestId == new Guid(destroySuggestId));
                        exits = destroySuggestDetails.Count() > 0;
                    }
                }
                item.IsCheck = exits;
            }
            return lstDocument;
        }

        /// <summary>
        /// Danh sách tài liệu đã ban hành dùng cho form tạo đề nghị phân phối
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filter"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentPublishBO> GetAllForDistributeSuggest(int pageIndex, int pageSize, out int count, string filter = "", string departmentId = "")
        {
            // Lấy những tài liệu đã ban hành nhưng không lấy tài liệu có trạng thái lỗi thời hoặc đã hết hạn
            var data = base.Get(p => p.Status != Resource.DocumentStatus.Obsolate && p.Status != Resource.DocumentStatus.Expire);
            foreach (var item in data)
            {
                if (item.DocumentId.HasValue)
                {
                    var document = ServiceFactory<DocumentService>().GetById(item.DocumentId.Value);
                    item.Document = document == null ? null : document;
                    item.DocumentName = document == null ? string.Empty : document.Name;
                    item.DocumentCode = document == null ? string.Empty : document.Code;
                    item.DocumentSecurityName = document == null ? string.Empty : document.SecurityName;
                    item.DocumentSecurityColor = document == null ? string.Empty : document.SecurityColor;
                    item.DocumentReversion = document == null ? string.Empty : document.Revision;
                    if (document.CategoryId.HasValue)
                    {
                        var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
                        item.DepartmentId = category == null ? null : category.DepartmentId;
                    }
                }
            }
            if (!String.IsNullOrEmpty(departmentId))
            {
                if (departmentId.IndexOf("Department_") != -1)
                {
                    var department = departmentId.Split('_').ToList();
                    if (department != null && department.Count() == 2)
                    {
                        data = data.Where(p => p.DepartmentId == new Guid(department[1]));
                    }
                }
                else
                {
                    data = data.Where(p => p.DepartmentId == new Guid(departmentId));
                }
            }
            if (!String.IsNullOrEmpty(filter))
            {
                data = data.Where(p => p.DocumentName.Contains(filter));
            }
            data = Page(data, pageIndex, pageSize);
            count = data.Count();
            return data;
        }

        /// <summary>
        /// Lay thong tin yeu cau de ban hanh
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public DocumentPublishBO GetRequestPromulgateByUser(Guid Id)
        {
            var request = ServiceFactory<DocumentResponsibilityService>().GetById(Id);
            var document = ServiceFactory<DocumentService>().GetById(request.DocumentId != null ? request.DocumentId.Value : Guid.Empty);
            var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(request.PerformBy != null ? request.PerformBy.Value : Guid.Empty);
            var publish = base.Get(i => i.DocumentId == request.DocumentId.Value).FirstOrDefault();
            var data = new DocumentPublishBO()
            {
                Document = document,
                RequestId = Id,
                DepartmentId = document.DepartmentId,
                ReasonOfObsolete = publish == null ? null : publish.ReasonOfObsolete,
                EmployeeApproveBy = employee,
                DocumentResponsibility = request,
                ApproveAt = request == null ? null : request.PerformAt,
                RequestPublish = request == null ? null : request.ApproveNote,
                DocumentId = document.Id,
                IsCompile = request == null ? null : request.IsComplete,
                PublishAt = publish == null ? null : publish.PublishAt,
                ApplyAt = publish == null ? null : publish.ApplyAt,
                ExpireAt = publish == null ? null : publish.ExpireAt,
                Note = publish == null ? null : publish.Note
            };
            var temp = ServiceFactory<DocumentAttachmentService>().Get(x => x.DocumentId == data.DocumentId.Value).Select(t => t.FileId.HasValue ? t.FileId.Value : Guid.Empty);
            data.Document.FileAttachs = new FileUploadBO()
            {
                Files = temp.ToList()
            };
            return data;
        }

    }
}
