﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentDestinationService : BaseService<DocumentDestinationDTO, DocumentDestinationBO>, IDocumentDestinationService
    {
        public IEnumerable<Guid> InsertRange(string jsonData, Guid documentId, bool allowSave = false)
        {
            var destinations = JsonConvert.DeserializeObject<List<DocumentDestinationBO>>(jsonData);
            var result = Enumerable.Empty<Guid>();
            if (destinations != null && destinations.Count > 0)
            {
                var data = new List<DocumentDestinationBO>();
                foreach (var item in destinations)
                {
                    var destination = new DocumentDestinationBO();
                    destination.DocumentId = documentId;
                    var destinationId = item.DestinationId.Split('_').ToList();
                    switch (destinationId[0])
                    {
                        case "Department":
                            destination.DepartmentId = new Guid(destinationId[1]);
                            break;
                        case "Title":
                            destination.TitleId = new Guid(destinationId[1]);
                            break;
                        case "Employee":
                            destination.EmployeeId = new Guid(destinationId[1]);
                            break;
                        case "External":
                            destination.ExternalId = new Guid(destinationId[1]);
                            break;
                    }
                    data.Add(destination);
                }
                result = InsertRange(data, allowSave);
            }
            return result;
        }

        public IEnumerable<DocumentDestinationBO> GetByDocument(Guid documentId)
        {
            var data = Get(i => i.DocumentId == documentId);
            return data;
        }

        /// <summary>
        /// Danh sách nơi nhận theo tài liệu
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="count"></param>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentDestinationBO> GetByDocumentId(int pageSize, int pageIndex, out int count, Guid? documentId)
        {
            var data = Enumerable.Empty<DocumentDestinationBO>();
            if (documentId != null || documentId != Guid.Empty)
            {
                data = Get(p => p.DocumentId.Value == documentId.Value);
                data = Page(data, pageIndex, pageSize).Select(
                    i =>
                    {
                        if (i.DepartmentId.HasValue)
                        {
                            var department = ServiceFactory<DepartmentService>().GetById(i.DepartmentId.Value);
                            i.DestinationId = "Department_" + department.Id;
                            i.DestinationName = department.Name;
                            i.DestinationKind = "Nội bộ";
                        }
                        if (i.TitleId.HasValue)
                        {
                            var title = ServiceFactory<DepartmentTitleService>().GetById(i.TitleId.Value);
                            i.DestinationId = "Title_" + title.Id;
                            i.DestinationName = title.Name;
                            i.DestinationKind = "Nội bộ";
                        }
                        if (i.EmployeeId.HasValue)
                        {
                            var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.EmployeeId.Value);
                            i.DestinationId = "Employee_" + employee.Id;
                            i.DestinationName = employee.Name;
                            i.DestinationKind = "Nội bộ";
                        }
                        if (i.ExternalId.HasValue)
                        {
                            var external = ServiceFactory<DepartmentTitleService>().GetById(i.ExternalId.Value);
                            i.DestinationId = "External_" + external.Id;
                            i.DestinationName = external.Name;
                            i.DestinationKind = "Bên ngoài";
                        }
                        return i;
                    }
                    ).ToList();
            }
            count = data.Count();
            return data;
        }

        public IEnumerable<Guid> GetDocumentIdByCurrentUser(IEnumerable<Guid> departmentIds, IEnumerable<Guid> titleIds, Guid userId)
        {
            var data = Get(i => (i.DepartmentId.HasValue && departmentIds.Contains(i.DepartmentId.Value))
                || (i.TitleId.HasValue && titleIds.Contains(i.TitleId.Value)) || i.EmployeeId == userId)
                .Select(i => i.DocumentId.Value).Distinct();
            return data;
        }
    }
}
