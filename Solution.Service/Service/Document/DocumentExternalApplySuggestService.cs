﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentExternalApplySuggestService : BaseService<DocumentExternalApplySuggestDTO, DocumentExternalApplySuggestBO>, IDocumentExternalApplySuggestService
    {
        /// <summary>
        /// Lấy thông tin tài liệu bên ngoài đã ban hành
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public DocumentExternalApplySuggestBO CreateDefault(string documentId)
        {
            try
            {
                DocumentExternalApplySuggestBO applySuggest;
                if (String.IsNullOrEmpty(documentId))
                    applySuggest = new DocumentExternalApplySuggestBO() { Document = new DocumentBO() };
                else
                {
                    //applySuggest = new DocumentExternalApplySuggestBO();
                    var document = ServiceFactory<DocumentService>().GetById(new Guid(documentId));
                    applySuggest = base.Get(p => p.DocumentId == new Guid(documentId)).OrderBy(p => p.CreateAt).FirstOrDefault();
                    if (applySuggest == null)
                    {
                        applySuggest = new DocumentExternalApplySuggestBO();
                        applySuggest.Document = document;
                        applySuggest.DocumentId = document.Id;
                    }
                    else
                        // Thông tin người phê duyệt
                        applySuggest.EmployeeApprove = applySuggest.ApproveBy.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(applySuggest.ApproveBy.Value) : null;
                    applySuggest.Document = document == null ? new DocumentBO() : document;
                    // Thông tin ban hành tài liệu
                    applySuggest.PublishTime = document == null || string.IsNullOrEmpty(document.Revision) ? (int?)null : Convert.ToInt16(document.Revision);
                    applySuggest.PublishApplyAt = document.ApplyAt;
                    applySuggest.PublishExpireAt = document.ExpireAt;
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(document.CreateBy.Value);
                    applySuggest.EmployeePublish = employee == null ? null : employee;
                }
                return applySuggest;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Gửi thông tin đề nghị ban hành áp dụng đối với các tài liệu bên ngoài
        /// </summary>
        /// <param name="applySuggest"></param>
        /// <returns></returns>
        public Guid Send(DocumentExternalApplySuggestBO applySuggest)
        {
            try
            {
                applySuggest.ApproveBy = applySuggest.EmployeeApprove.Id;
                applySuggest.IsSend = true;
                if (applySuggest.Id == null || applySuggest.Id == Guid.Empty)
                    applySuggest.Id = applySuggest.Id = base.Insert(applySuggest, false);
                else
                    base.Update(applySuggest, false);
                // Update tài liệu về trạng thái chờ duyệt
                var document = ServiceFactory<DocumentService>().GetById(applySuggest.DocumentId.Value);
                document.IsApply = true;
                ServiceFactory<DocumentService>().Update(document, false);
                SaveTransaction(true);
                return applySuggest.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override DocumentExternalApplySuggestBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.Get(p => p.Id == id).FirstOrDefault();
            if (data != null)
            {
                var document = ServiceFactory<DocumentService>().GetById(data.DocumentId.Value);
                data.Document = document == null ? new DocumentBO() : document;
                // Người tạo đề nghị
                var employeeCreate = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.CreateBy.Value);
                data.EmployeePublish = employeeCreate;
                data.PublishTime = Convert.ToInt16(document.Revision);
                data.PublishApplyAt = document.ApplyAt;
                data.PublishExpireAt = document.ExpireAt;
                // Người phê duyệt
                var employeeApprove = data.ApproveBy.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.ApproveBy.Value) : null;
                data.EmployeeApprove = employeeApprove;
                if (data.CreateBy == data.ApproveBy)
                    data.IsShow = false;
                else
                    data.IsShow = true;

            }
            return data;
        }

        /// <summary>
        /// Danh sách đề nghị áp dụng đã gửi phê duyệt
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentExternalApplySuggestBO> GetSuggestSend()
        {
            try
            {
                var data = base.GetQuery()
                    .Where(p => p.IsSend == true && p.ApproveBy == UserId && p.IsApprove != true && p.IsApply != true).OrderByDescending(p => p.CreateAt);
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Phê duyệt
        /// Nếu người nhận là người xem xét thì có thể gửi người xem xét khác
        /// Nếu người nhận là người phê duyệt thì kết thúc
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid NextSend(DocumentExternalApplySuggestBO data)
        {
            try
            {
                var parent = GetById(data.Id);
                //if (parent.RoleStatus == (int)Resource.DocumentExternalApplyStatus.Review)
                //{
                var newData = new DocumentExternalApplySuggestBO()
                {
                    ParentId = parent.Id,
                    DocumentId = parent.DocumentId,
                    SuggestContent = parent.SuggestContent,
                    IsSend = true,
                    ApproveBy = data.EmployeeApprove.Id,
                    ApplyAt = parent.ApplyAt,
                    RoleStatus = data.RoleStatus
                };
                data.Id = base.Insert(newData, false);
                //}
                //else
                //{
                //    var document = ServiceFactory<DocumentService>().GetById(data.DocumentId.Value);
                //    document.IsApply = true;
                //    document.IsAccept = data.IsApply;
                //    ServiceFactory<DocumentService>().Update(document, false);
                //}
                SaveTransaction(true);
                return data.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Danh sách nhân sự kiểm tra hoặc phê duyệt
        /// </summary>
        /// <param name="applySuggestId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentExternalApplySuggestBO> GetEmployeesReview(Guid applySuggestId)
        {
            var result = new List<DocumentExternalApplySuggestBO>();
            var data = GetById(applySuggestId);
            var result1 = new DocumentExternalApplySuggestBO()
            {
                Id = data.Id,
                IsApprove = data.IsApprove,
                IsApply = data.IsApply,
                EmployeeSend = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)data.ApproveBy),
                ApproveNote = data.ApproveNote,
                CreateAt = data.CreateAt
            };
            result.Add(result1);
            var listEmployees = Get(p => p.ParentId == data.Id)
                                .Select(item => new DocumentExternalApplySuggestBO
                                {
                                    Id = item.Id,
                                    IsApprove = item.IsApprove,
                                    IsApply = item.IsApply,
                                    EmployeeSend = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)item.ApproveBy),
                                    ApproveNote = item.ApproveNote,
                                    CreateAt = item.CreateAt
                                }).ToList();
            if (listEmployees.Count > 0) { result.AddRange(listEmployees); }
            return result.Distinct().OrderBy(p => p.CreateAt);
        }

        /// <summary>
        /// Phê duyệt
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid Approve(DocumentExternalApplySuggestBO data)
        {
            try
            {
                var parent = GetById(data.Id);
                if (data.RoleStatus == (int)Resource.DocumentExternalApplyStatus.Review)
                {
                    var newData = new DocumentExternalApplySuggestBO()
                    {
                        ParentId = parent.Id,
                        DocumentId = parent.DocumentId,
                        SuggestContent = parent.SuggestContent,
                        IsSend = true,
                        ApproveBy = data.EmployeeApprove.Id,
                        ApplyAt = parent.ApplyAt,
                        RoleStatus = data.RoleStatus
                    };
                    data.Id = base.Insert(newData, false);
                }
                else
                {
                    // Update đề nghị áp dụng
                    data.ApproveBy = data.EmployeeApprove.Id;
                    data.IsApprove = true;
                    base.Update(data, false);
                    // Update tài liệu
                    var document = ServiceFactory<DocumentService>().GetById(data.DocumentId.Value);
                    document.IsApply = true;
                    document.IsAccept = data.IsApply;
                    ServiceFactory<DocumentService>().Update(document, false);
                }
                SaveTransaction(true);
                return data.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
