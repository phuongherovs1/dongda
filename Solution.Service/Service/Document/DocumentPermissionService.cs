﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentPermissionService : BaseService<DocumentPermissionDTO, DocumentPermissionBO>, IDocumentPermissionService
    {
        /// <summary>
        /// Danh sách quyền truy cập theo tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentPermissionBO> GetByDocument(Guid documentId)
        {
            var data = Get(i => i.DocumentId == documentId).Select(
                i =>
                {
                    // nếu employeeId có giá trị, lấy thông tin của employee
                    if (i.EmployeeId.HasValue)
                    {
                        var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.EmployeeId.Value);
                        i.ObjectImage = employee.AvatarUrl;
                        i.ObjectName = employee.Name;
                    }
                    // nếu chức danh có giá trị, lấy thông tin chức danh
                    if (i.TitleId.HasValue)
                    {
                        var title = ServiceFactory<DepartmentTitleService>().GetById(i.TitleId.Value);
                        i.ObjectName = title.Name;
                    }
                    // 
                    if (i.DepartmentId.HasValue)
                    {
                        var department = ServiceFactory<DepartmentService>().GetById(i.DepartmentId.Value);
                        i.ObjectName = department.Name;
                    }
                    i.FromView = false;
                    return i;
                });
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jsonData"></param>
        public void Update(string jsonData)
        {
            try
            {
                //1. Convert dữ liệu từ string => list Permission
                var data = JsonConvert.DeserializeObject<List<DocumentPermissionViewBO>>(jsonData);
                var insertData = new List<DocumentPermissionBO>();
                var deleteIds = new List<Guid>();
                foreach (var item in data)
                {
                    if (!item.FromView)//(item.Id != Guid.Empty)
                    {
                        if (item.Disallow == true)
                        {
                            var id = Utilities.ConvertToGuid(item.Id);
                            if (id != Guid.Empty)
                                deleteIds.Add(id);
                        }
                        else
                        {
                            var itemUpdate = new DocumentPermissionBO()
                            {
                                Id = Utilities.ConvertToGuid(item.Id),
                                AllowView = item.AllowView,
                                AllowDownload = item.AllowDownload
                            };
                            if (itemUpdate.Id != Guid.Empty)
                                Update(itemUpdate, false);
                        }
                    }
                    else
                    {
                        var itemInsert = new DocumentPermissionBO();
                        itemInsert.DocumentId = item.DocumentId;
                        itemInsert.AllowView = item.AllowView;
                        itemInsert.AllowDownload = item.AllowDownload;
                        var objectData = item.ObjectId.Split('_');
                        if (objectData[0] == "Department")
                        {
                            itemInsert.DepartmentId = new Guid(objectData[1]);
                        }
                        else if (objectData[0] == "Title")
                        {
                            itemInsert.TitleId = new Guid(objectData[1]);
                        }
                        else
                        {
                            itemInsert.EmployeeId = new Guid(objectData[1]);
                        }
                        if (!itemInsert.Disallow)
                            insertData.Add(itemInsert);
                    }
                }
                DeleteRange(deleteIds, false);
                InsertRange(insertData, false);
                SaveTransaction();
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Guid> GetDocumentIdShareToCurrentUser()
        {
            //1. Phòng ban người hiện tại
            var departmentIds = ServiceFactory<DepartmentService>().GetDepartmentsByCurrentUser().Select(i => i.Id);
            //2. Chức danh người hiện tại
            var titleIds = ServiceFactory<EmployeeTitleService>().GetTitleIdsByCurrentUser();
            var data = GetDocumentIdShareToCurrentUser(departmentIds, titleIds, UserId);
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="departmentIds"></param>
        /// <param name="titleIds"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<Guid> GetDocumentIdShareToCurrentUser(IEnumerable<Guid> departmentIds, IEnumerable<Guid> titleIds, Guid userId)
        {
            var data = Get(i => (i.DepartmentId.HasValue && departmentIds.Contains(i.DepartmentId.Value)) || (i.TitleId.HasValue && titleIds.Contains(i.TitleId.Value)) || i.EmployeeId == userId);
            return data.Select(i => i.DocumentId.Value);
        }
        /// <summary>
        /// Check quyền user truy cập vào tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public bool CheckPermission(Guid documentId)
        {
            //1. Lấy ra chức danh người đăng nhập hệ thống
            var currentUserTitles = ServiceFactory<EmployeeTitleService>().GetTitleIdsByCurrentUser();
            //2. Check phân quyền có trong chức danh hay user đó không
            var check = Get(i => (i.TitleId.HasValue && currentUserTitles.Contains(i.TitleId.Value) || i.EmployeeId == UserId)).Any();
            return check;
        }
    }
}
