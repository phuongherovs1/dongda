﻿namespace iDAS.Service
{
    //public class DocumentReviewService : BaseService<DocumentReviewDTO, DocumentReviewBO>, IDocumentReviewService
    //{
    //    public void Approve(DocumentReviewBO review)
    //    {

    //    }
    //    /// <summary>
    //    /// Duyệt đề nghị kiểm duyệt
    //    /// View Kiểm duyệt
    //    /// </summary>
    //    /// <returns></returns>
    //    public int CountSuggestReview()
    //    {
    //        var summary = Get(i => i.ReviewBy == UserId && i.IsReview != true).Count();
    //        return summary;
    //    }

    //    /// <summary>
    //    /// Danh sách đề nghị được tạo ra bởi người đăng nhập hệ thống
    //    /// 
    //    /// </summary>
    //    /// <param name="pageIndex"></param>
    //    /// <param name="pageSize"></param>
    //    /// <param name="count"></param>
    //    /// <param name="approveStatus">Bao gồm: mới - chưa xem xét, đang duyệt - đã xem, Duyệt đạt , duyệt không đạt</param>
    //    /// <returns></returns>
    //    public IEnumerable<DocumentReviewBO> GetAll(int pageIndex, int pageSize, out int count, Resource.ApproveStatus approveStatus)
    //    {
    //        count = GetAll().Count();
    //        var data = Page(GetAll().Where(p => p.CreateBy == UserId).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
    //        foreach (var item in data)
    //        {
    //            if (item.ReviewBy.HasValue)
    //            {
    //                var employee = ServiceFactory<EmployeeService>().GetById(item.ReviewBy.Value);
    //                item.Reviewer = employee.Name;
    //            }
    //            if (item.DocumentId.HasValue)
    //            {
    //                var document = ServiceFactory<DocumentService>().GetById(item.DocumentId.Value);
    //                item.Document = document;
    //            }
    //        }
    //        return data;
    //    }

    //    /// <summary>
    //    /// Khởi tạo giá trị mặc định với documentId
    //    /// Trả về thông tin đầy đủ của tài liệu
    //    /// Khi id rỗng tức là tạo mới
    //    /// </summary>
    //    /// <param name="documentId"></param>
    //    /// <returns></returns>
    //    public DocumentReviewBO CreateDefault(Guid documentId, string id)
    //    {
    //        DocumentReviewBO data;
    //        var document = ServiceFactory<DocumentService>().GetById(documentId);
    //        // Lấy tên và màu sắc mức độ bảo mật
    //        if (document.SecurityId.HasValue && document.SecurityId != Guid.Empty)
    //        {
    //            var sercurity = ServiceFactory<DocumentSecurityService>().GetById(document.SecurityId.Value);
    //            document.Security = sercurity;
    //        }
    //        // Lấy tên danh mục
    //        if (document.CategoryId.HasValue && document.CategoryId != Guid.Empty)
    //        {
    //            var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
    //            document.CategoryName = category.Name;
    //        }

    //        if (!string.IsNullOrEmpty(id))
    //        {
    //            data = GetById(new Guid(id));
    //            // Hiển thị tên người phê duyệt
    //            if (data.ReviewBy.HasValue && data.ReviewBy != Guid.Empty)
    //            {
    //                var employeeId = data.ReviewBy;
    //                data.EmployeeReviewBy = base.GetEmployee(employeeId.Value);
    //            }
    //            data.Document = document;
    //        }
    //        else
    //        {
    //            // Thực hiện chuyển trạng thái của tài liệu từ mới sang đang soạn thảo
    //            ServiceFactory<DocumentService>().Perform(documentId);
    //            data = new DocumentReviewBO()
    //            {
    //                Document = document,
    //                DocumentId = documentId
    //            };
    //        }
    //        // Lấy file đính kèm
    //        var temp = ServiceFactory<DocumentAttachmentService>().GetByDocument(document.Id);
    //        document.FileAttachs = new FileUploadBO()
    //        {
    //            Files = temp.ToList()
    //        };

    //        return data;
    //    }

    //    /// <summary>
    //    /// Gửi xem xét
    //    /// </summary>
    //    /// <param name="item"></param>
    //    /// <returns></returns>
    //    public Guid Send(DocumentReviewBO item)
    //    {
    //        try
    //        {
    //            item.ReviewBy = item.EmployeeReviewBy.Id;
    //            item.IsSend = true;
    //            if (item.Id == Guid.Empty)
    //                item.Id = Insert(item);
    //            else Update(item);
    //        }
    //        catch (Exception)
    //        {
    //            throw;
    //        }
    //        return item.Id;
    //    }


    //}
}