﻿using iDAS.DataAccess;
using System;

namespace iDAS.Service
{
    public class DocumentRequestTransferService : BaseService<DocumentRequestTransferDTO, DocumentRequestTransferBO>, IDocumentRequestTransferService
    {
        public void Send(DocumentRequestTransferBO item)
        {
            try
            {
                item.TransferTo = item.EmployeeTransferTo.Id;
                var request = ServiceFactory<DocumentRequestService>().GetById(item.RequestId.Value);
                request.PerformBy = item.TransferTo;
                ServiceFactory<DocumentRequestService>().Update(request, false);
                base.Insert(item, false);
                SaveTransaction();
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}