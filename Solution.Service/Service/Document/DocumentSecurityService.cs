﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentSecurityService : BaseService<DocumentSecurityDTO, DocumentSecurityBO>, IDocumentSecurityService
    {

        public IEnumerable<DocumentSecurityBO> GetContain(string name)
        {
            var data = Get(i => i.Name.ToUpper().Contains(name.Trim().ToUpper()));
            return data;
        }

        public bool CheckExist(string name)
        {
            var existName = Get(i => !string.IsNullOrEmpty(i.Name) && i.Name.Trim().ToUpper() == name.Trim().ToUpper()).Any();
            return existName;
        }
        public override Guid Insert(DocumentSecurityBO data, bool allowSave = true)
        {
            if (CheckExist(data.Name))
                throw new DataHasBeenExistedException();
            return base.Insert(data, allowSave);
        }
    }
}
