﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentDistributeSuggestService : BaseService<DocumentDistributeSuggestDTO, DocumentDistributeSuggestBO>, IDocumentDistributeSuggestService
    {
        /// <summary>
        /// Hàm khởi tạo mặc định phân phối cho tài liệu xác định
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public DocumentDistributeSuggestBO CreateDefault(Guid documentId, string id = default(string))
        {
            var document = ServiceFactory<DocumentService>().GetById(documentId);
            // Lấy tên và màu sắc mức độ bảo mật
            if (document.SecurityId.HasValue && document.SecurityId != Guid.Empty)
            {
                var sercurity = ServiceFactory<DocumentSecurityService>().GetById(document.SecurityId.Value);
                document.Security = sercurity;
            }
            DocumentDistributeSuggestBO data = new DocumentDistributeSuggestBO();
            data.Document = document;
            data.DocumentId = documentId;
            if (!string.IsNullOrEmpty(id))
            {
                data = GetById(new Guid(id));
                // Hiển thị tên người phê duyệt
                if (data.ApproveBy.HasValue && data.ApproveBy != Guid.Empty)
                {
                    var employeeId = data.ApproveBy;
                    data.EmployeeApproveBy = base.GetEmployee(employeeId.Value);
                }
                // Hiển thị ngày ban hành
                if (data.DocumentId.HasValue && data.DocumentId != Guid.Empty)
                {
                    var publish = ServiceFactory<DocumentPublishService>().GetByDocumentId(data.DocumentId.Value);
                    data.PublishAt = publish == null ? null : publish.PublishAt;
                }

            }
            else
            {
                if (document.CategoryId.HasValue)
                    data.DepartmentId = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value).DepartmentId;
            }
            // Lấy file đính kèm
            var temp = ServiceFactory<DocumentAttachmentService>().GetByDocument(document.Id);
            document.FileAttachs = new FileUploadBO()
            {
                Files = temp.ToList()
            };
            return data;
        }


        /// <summary>
        /// Đề nghị phân phối
        /// View soạn thảo
        /// </summary>
        /// <returns></returns>
        public int CountSuggest()
        {
            var summary = Get(i => i.CreateBy == UserId).Count();
            return summary;
        }

        /// <summary>
        /// Duyệt đề nghị phân phối
        /// View Kiểm duyệt
        /// </summary>
        /// <returns></returns>
        public int CountApproveSuggest()
        {
            var summary = Get(i => i.ApproveBy == UserId && i.IsApprove != true).Count();
            return summary;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public IEnumerable<DocumentDistributeSuggestBO> GetAll(int pageIndex, int pageSize, out int count, Resource.ApproveStatus status = Common.Resource.ApproveStatus.All)
        {
            var data = Get(i => i.CreateBy == UserId && status == Resource.ApproveStatus.New ? (i.IsSend != true && i.IsApprove != true && i.IsAccept != true) :
                status == Resource.ApproveStatus.Wait ? i.IsSend == true :
                status == Resource.ApproveStatus.Approve ? (i.IsApprove == true && i.IsAccept == true) :
                status == Resource.ApproveStatus.Reject ? (i.IsApprove == true && i.IsAccept == false) : true
                );
            count = data.Count();
            data = Page(data, pageIndex, pageSize).Select(
                i =>
                {
                    if (i.ApproveBy.HasValue)
                    {
                        var approver = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.ApproveBy.Value);
                        i.EmployeeApproveBy = approver;
                        var title = ServiceFactory<EmployeeTitleService>().Get(p => p.EmployeeId == i.ApproveBy.Value).FirstOrDefault();
                        if (title != null)
                        {
                            var result = ServiceFactory<DepartmentTitleService>().GetById(title.TitleId);
                            i.ApproveTitle = result.Name;
                        }
                    }
                    if (i.DocumentId.HasValue)
                    {
                        var document = ServiceFactory<DocumentService>().GetById(i.DocumentId.Value);
                        i.Document = document == null ? null : document;
                        if (document.CategoryId.HasValue)
                        {
                            i.CategoryId = document.CategoryId;
                            var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
                            i.IsExternal = category.IsExternal;
                        }
                    }
                    return i;
                }).OrderByDescending(p => p.CreateAt);
            return data;
        }

        /// <summary>
        /// Gửi duyệt đề nghị phân phối
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Guid Send(DocumentDistributeSuggestBO item)
        {
            try
            {
                item.ApproveBy = item.EmployeeApproveBy.Id;
                item.IsSend = true;
                if (item.Id == Guid.Empty)
                    item.Id = Insert(item);
                else Update(item);
            }
            catch (Exception)
            {
                throw;
            }
            return item.Id;
        }

        /// <summary>
        /// Thu hồi đề nghị phân phối tài liệu
        /// </summary>
        /// <param name="id"></param>
        public void Revert(DocumentDistributeSuggestBO distributeSuggest)
        {
            try
            {
                distributeSuggest.ApproveBy = distributeSuggest.EmployeeApproveBy.Id;
                distributeSuggest.IsSend = false;
                Update(distributeSuggest);
            }
            catch
            {
                throw;
            }
        }


        public void updateSuggestDistribute(Guid id)
        {
            try
            {
                var suggestDistribute = new DocumentDistributeSuggestBO()
                {
                    Id = id,
                    IsSend = false
                };
                Update(suggestDistribute);
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Danh sách đề nghị phân phối được gửi đến người đang đăng nhập hệ thống
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="approveStatus"></param>
        /// <returns></returns>
        public IEnumerable<DocumentDistributeSuggestBO> GetByApprover(int pageIndex, int pageSize, out int count, Resource.ApproveStatus approveStatus = Resource.ApproveStatus.All)
        {
            //1. Hiển thị danh sách với các điều kiện
            var data = Get(i => i.ApproveBy == UserId && i.IsSend == true)
                .Where(i => approveStatus == Resource.ApproveStatus.Wait ? (i.IsSend == true && i.IsApprove != true)
                    : approveStatus == Resource.ApproveStatus.Approve ? (i.IsApprove == true && i.IsAccept == true)
                    : approveStatus == Resource.ApproveStatus.Reject ? (i.IsApprove == true && i.IsAccept != true)
                    : true);
            //2. Đếm số lượng kết quả
            count = data.Count();
            //3. Phân trang dữ liệu
            data = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            foreach (var item in data)
            {
                //4. Hiển thị tên người đề nghị
                var creator = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.CreateBy.Value);
                item.SuggestorName = creator.Name;
                if (item.DocumentId.HasValue)
                {
                    // Thông tin tài liệu
                    var document = ServiceFactory<DocumentService>().GetById(item.DocumentId.Value);
                    item.Document = document;
                }
                if (item.ApproveBy.HasValue)
                {
                    var approver = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.ApproveBy.Value);
                    item.EmployeeApproveBy = approver;
                }
            }
            return data;
        }
        public void SendToPromulgate(DocumentDistributeSuggestBO item)
        {
            try
            {
                item.DocumentId = item.Document.Id;
                item.ApproveBy = item.EmployeeApproveBy.Id;
                item.IsSend = true;
                if (item.Id == Guid.Empty)
                    item.Id = Insert(item);
                else Update(item);
            }
            catch (Exception)
            {
                throw;
            }
            return;
        }
        public void SendFromPromulgateToDistribute(DocumentDistributeSuggestBO data)
        {
            var suggest = GetById(data.Id);
            suggest.Note = data.Note;
            suggest.IsComplete = true;
            // Hàm tạo yêu cầu phân phối
            //------------------------
        }
        public IEnumerable<EmployeeBO> GetPromulgateEmployees(int pageIndex, int pageSize, out int totalCount, string fillter, string departmentId)
        {
            var result = ServiceFactory<EmployeeService>().GetEmployeeInProcessDocument(pageIndex, pageSize, out totalCount, fillter, departmentId: departmentId,
                process: (int)Resource.DocumentTypeProcess.WriteAndRelation, roleType: (int)Resource.DocumentProcessRole.Promulgate);
            return result;
            //return ServiceFactory<DocumentProcessService>().GetEmployeeByRole(pageIndex, pageSize, out totalCount, departmentId: departmentId,
            //    process: (int)Resource.DocumentTypeProcess.WriteAndRelation, roleType: (int)Resource.DocumentProcessRole.Promulgate);
        }
        public IEnumerable<DocumentDistributeSuggestBO> GetPromulateDasboard()
        {
            var suggester = Get(i => i.CreateBy == UserId && i.IsSend == true)
                        .Select(
                        i =>
                        {
                            i.RoleType = (int)Resource.DocumentProcessRole.DistributeSuggester;
                            return i;
                        }
                       );
            var promulate = Get(i => i.ApproveBy == UserId && i.IsSend == true)
                        .Select(
                        i =>
                        {
                            i.RoleType = (int)Resource.DocumentProcessRole.Promulgate;
                            return i;
                        }
                       );
            return suggester.Union(promulate);
        }
        public DocumentDistributeSuggestBO GetDetailPerform(Guid id)
        {
            var data = GetById(id);
            //// Hiển thị tên người phê duyệt
            if (data.CreateBy.HasValue && data.CreateBy != Guid.Empty)
            {
                var employeeId = data.CreateBy;
                data.EmployeeSuggest = base.GetEmployee(employeeId.Value);
            }
            if (data.DocumentId.HasValue)
            {
                var document = ServiceFactory<DocumentService>().GetById(data.DocumentId.Value);
                // Lấy tên và màu sắc mức độ bảo mật
                if (document.SecurityId.HasValue && document.SecurityId != Guid.Empty)
                {
                    var sercurity = ServiceFactory<DocumentSecurityService>().GetById(document.SecurityId.Value);
                    document.Security = sercurity;
                }
                data.Document = document;
                if (document.CategoryId.HasValue)
                    data.DepartmentId = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value).DepartmentId;
            }
            data.UserId = UserId;
            return data;
        }
        public bool ExitsResponsibility(Guid suggestId)
        {
            return ServiceFactory<DocumentDistributeSuggestResponsibilityService>().Get(i => i.DocumentDistributeSuggestId == suggestId).Any();
        }
        public void AcceptDistributte(Guid id)
        {
            var item = GetById(id);
            item.IsComplete = true;
            Update(item);
        }
    }
}
