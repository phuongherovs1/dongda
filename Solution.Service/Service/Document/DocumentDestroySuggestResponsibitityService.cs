﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentDestroySuggestResponsibitityService : BaseService<DocumentDestroySuggestResponsibitityDTO, DocumentDestroySuggestResponsibitityBO>, IDocumentDestroySuggestResponsibitityService
    {
        /// <summary>
        /// Lấy danh sách trách nhiệm của nhân sự đăng nhập
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentDestroySuggestResponsibitityBO> GetByCurrentUser()
        {
            return Get(i => i.ApproveBy == UserId && i.IsNew != null);
        }

        /// <summary>
        /// Lấy danh sách vai trò theo DocumentDestroyId
        /// </summary>
        /// <param name="destroyId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentDestroySuggestResponsibitityBO> GetByDestroyId(Guid destroyId)
        {
            return Get(i => i.DocumentDestroyId == destroyId);
        }

        /// <summary>
        /// lấy thông tin đề nghị hủy
        /// </summary>
        /// <param name="responsibilityId"></param>
        /// <returns></returns>
        public DocumentDestroySuggestResponsibitityBO GetDestroySuggestByResponsibilityId(Guid responsibilityId)
        {
            try
            {
                var data = base.GetById(responsibilityId);
                if (data.DocumentDestroyId.HasValue)
                {
                    // Lấy thông tin đề nghị hủy
                    var destroySuggest = ServiceFactory<DocumentDestroySuggestService>().GetById(data.DocumentDestroyId.Value);
                    data.DestroySuggest = destroySuggest;
                    // Lấy thông tin người đề nghị
                    var employee = destroySuggest != null ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(destroySuggest.CreateBy.Value) : null;
                    data.EmployeeDestroyCreate = employee;
                    // Lấy thông tin phòng ban đề nghị
                    if (data.DepartmentId.HasValue)
                    {
                        var department = ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value);
                        data.DepartmentName = department == null ? string.Empty : department.Name;
                    }
                    // Lấy danh sách tài liệu của đề nghị hủy
                    var detail = ServiceFactory<DocumentDestroySuggestDetailService>().Get(i => i.DestroySuggestId.Value == data.DocumentDestroyId.Value);
                    data.CountDocument = detail.Count();
                    if (detail.Count() != null && detail.Count() == 1)
                    {
                        if (detail.FirstOrDefault().DocumentId.HasValue)
                        {
                            var document = ServiceFactory<DocumentService>().GetById(detail.FirstOrDefault().DocumentId.Value);
                            data.Document = document == null ? null : document;
                        }
                    }
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// lấy thông tin đề nghị hủy
        /// </summary>
        /// <param name="responsibilityId"></param>
        /// <returns></returns>
        public DocumentDestroySuggestBO GetDataDestroySuggestByResponsibilityId(Guid id)
        {
            try
            {
                var responsility = base.GetById(id);

                // Lấy thông tin đề nghị hủy
                var data = ServiceFactory<DocumentDestroySuggestService>().GetById(responsility.DocumentDestroyId.Value);
                // Lấy thông tin người đề nghị
                var employee = data != null ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.CreateBy.Value) : null;
                data.EmployeeApprove = new DocumentEmployeeBO()
                {
                    Id = employee.Id,
                    Name = employee.Name,
                    AvatarUrl = employee.AvatarUrl,
                    RoleNames = employee.RoleNames
                };

                // Lấy thông tin phòng ban đề nghị
                if (data.DepartmentId.HasValue)
                {
                    var department = ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value);
                    data.DepartmentName = department == null ? string.Empty : department.Name;
                }

                var detail = ServiceFactory<DocumentDestroySuggestDetailService>().Get(i => i.DestroySuggestId.Value == responsility.DocumentDestroyId.Value).FirstOrDefault();
                if (detail.DocumentId.HasValue)
                {
                    var document = ServiceFactory<DocumentService>().GetById(detail.DocumentId.Value);
                    data.Document = document == null ? null : document;
                }

                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Lấy danh sách trách nhiệm theo đề nghị
        /// </summary>
        /// <param name="suggestId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentDestroySuggestResponsibitityBO> GetEmployeeRoleByDestroySuggest(Guid destroySuggestId)
        {
            var resposibily = GetByDestroyId(destroySuggestId)
                            .Where(i => (i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.DestroyCreater
                                            || i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.DestroyReviewer
                                            || i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.DestroyApproval) && i.IsNew != false)
                            .Select(item => new DocumentDestroySuggestResponsibitityBO
                            {
                                Id = item.Id,
                                RoleType = item.RoleType,
                                IsApprove = item.IsApprove,
                                IsAccept = item.IsAccept,
                                EmployeeSend = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)item.ApproveBy),
                                ApproveNote = item.ApproveNote,
                                CreateAt = item.CreateAt
                            });
            return resposibily.Distinct().OrderBy(p => p.CreateAt);
        }


        /// <summary>
        /// Lấy danh sách trách nhiệm liên quan theo đề nghị theo quy trình ban hành
        /// </summary>
        /// <param name="suggestId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentDestroySuggestResponsibitityBO> GetEmployeeRoleByPublishProcess(Guid destroySuggestId)
        {
            var resposibily = GetByDestroyId(destroySuggestId)
                            .Where(i => (i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.Check
                                            || i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.ApproveDocument
                                            || i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.Promulgate
                                            || i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.DestroyCreater) && i.IsNew != false)
                            .Select(item => new DocumentDestroySuggestResponsibitityBO
                            {
                                Id = item.Id,
                                RoleType = item.RoleType,
                                IsApprove = item.IsApprove,
                                IsAccept = item.IsAccept,
                                //  EmployeeSend = ServiceFactory<EmployeeService>().GetById((Guid)item.ApproveBy),
                                EmployeeSend = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)item.ApproveBy),
                                ApproveNote = item.ApproveNote,
                                CreateAt = item.CreateAt,
                                IsNew = item.IsNew
                            });
            return resposibily.Distinct().OrderBy(p => p.CreateAt);
        }
        public IEnumerable<DocumentDestroySuggestResponsibitityBO> GetDataDocumentDestroySuggestResponsibitity()
        {
            var data = Enumerable.Empty<DocumentDestroySuggestResponsibitityBO>();
            data = base.GetQuery().Where(t => (t.ApproveBy.HasValue && t.ApproveBy.Value == UserId) || (t.CreateBy.HasValue && t.CreateBy.Value == UserId))
                .Where(t => t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.Check || t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument)
                .Where(t => t.IsNew == true && t.IsComplete != true && t.IsLock != true)
                //.Select(i =>
                //{
                //    i.UserId = UserId;
                //    //i.EmployeeSend = ServiceFactory<EmployeeService>().GetById(i.CreateBy.Value);
                //    return i;
                //})
                .OrderByDescending(t => t.CreateAt);
            return data;
        }

        public IEnumerable<DocumentDestroySuggestResponsibitityBO> GetDataDocumentSuggestPublishResponsibitity()
        {
            var data = Enumerable.Empty<DocumentDestroySuggestResponsibitityBO>();
            data = base.GetQuery().Where(t => (t.ApproveBy.HasValue && t.ApproveBy.Value == UserId) || (t.CreateBy.HasValue && t.CreateBy.Value == UserId))
                .Where(t => t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate)
                .Where(t => t.IsNew == true && t.IsComplete != true && t.IsLock != true)
                .AsEnumerable()
                .Select(i =>
                {
                    i.UserId = UserId;
                    return i;
                })
                .OrderByDescending(t => t.CreateAt);
            return data;
        }
        /// <summary>
        /// Phê duyệt xem xét
        /// </summary>
        /// <param name="data"></param>
        public void Review(DocumentDestroySuggestResponsibitityBO data)
        {
            var responsibility = base.GetById(data.Id);
            responsibility.IsApprove = true;
            responsibility.ApproveNote = data.ApproveNote;
            responsibility.IsAccept = data.IsAccept;
            responsibility.ApproveAt = DateTime.Now;
            ServiceFactory<DocumentDestroySuggestResponsibitityService>().Update(responsibility, false);
            var lockItem = ServiceFactory<DocumentDestroySuggestResponsibitityService>().Get(i => i.DocumentDestroyId == responsibility.DocumentDestroyId
                                                                    && i.Id != data.Id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            if (lockItem != null)
            {
                if (lockItem.IsNew == true)
                {
                    lockItem.IsLock = true;
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Update(lockItem, false);
                }
            }
            // Nếu duyệt không đạt thì cập nhật trạng thái đề nghị về Không đạt
            var destroySuggest = ServiceFactory<DocumentDestroySuggestService>().GetById(responsibility.DocumentDestroyId.Value);
            if (data.IsAccept == false)
            {
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.AcceptFail;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
            }
            // Nếu duyệt đạt mà chưa gửi thì trạng thái đề nghị là Chờ gửi
            if (data.IsComplete != true && data.IsAccept == true)
            {
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.SendWait;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
            }
            SaveTransaction(true);
            return;
        }

        /// <summary>
        /// Gửi bước tiếp theo từ view xem xét
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool SendFromReview(DocumentDestroySuggestResponsibitityBO data)
        {
            var responsibility = base.GetById(data.Id);
            responsibility.IsNew = true;
            responsibility.IsComplete = true;
            responsibility.ApproveAt = DateTime.Now;
            base.Update(responsibility, false);
            // Lấy thông tin đề nghị hủy
            var destroySuggest = ServiceFactory<DocumentDestroySuggestService>().GetById((Guid)responsibility.DocumentDestroyId);
            // Kiểm tra đề nghị hủy đã tồn tại vai trò chưa
            var exits = base.Get(i => i.DocumentDestroyId == responsibility.DocumentDestroyId
                                    && i.Order == data.EmployeeRecive.NextOrder && i.RoleType == data.EmployeeRecive.RoleType).FirstOrDefault();
            var nextResponsibility = new DocumentDestroySuggestResponsibitityBO();
            if (exits != null)
            {
                nextResponsibility = exits;
                nextResponsibility.ApproveBy = data.EmployeeRecive.Id;
            }
            else
            {
                nextResponsibility = new DocumentDestroySuggestResponsibitityBO()
                {
                    RoleType = data.EmployeeRecive.RoleType,
                    DepartmentId = data.DepartmentId,
                    DocumentDestroyId = responsibility.DocumentDestroyId,
                    ApproveBy = data.EmployeeRecive.Id,
                    Order = data.EmployeeRecive.NextOrder
                };
            }
            // Kiểm tra vai trò của người tiếp theo
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.DestroyReviewer) // Trường hợp người nhận trách nhiệm tiếp theo là người xem xét
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                if (nextResponsibility.Id == null || nextResponsibility.Id == Guid.Empty)
                {
                    base.Insert(nextResponsibility, false);
                }
                else
                {
                    base.Update(nextResponsibility, false);
                }
                // Cập nhật lại đề nghị hủy
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.Review;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.DestroyApproval) // Trường hợp tiếp theo người nhận trách nhiệm là người phê duyệt
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                if (nextResponsibility.Id == null || nextResponsibility.Id == Guid.Empty)
                {
                    base.Insert(nextResponsibility, false);
                }
                else
                {
                    base.Update(nextResponsibility, false);
                }
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.Approve;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.DestroyBillCreater) // Trường hợp tiếp theo người nhận trách nhiệm là tạo biên bản hủy
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                if (nextResponsibility.Id == null || nextResponsibility.Id == Guid.Empty)
                {
                    base.Insert(nextResponsibility, false);
                }
                else
                {
                    base.Update(nextResponsibility, false);
                }
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.WaitBillCreate;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.Check) // Trường hợp tiếp theo người nhận trách nhiệm là người kiểm tra
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                if (nextResponsibility.Id == null || nextResponsibility.Id == Guid.Empty)
                {
                    base.Insert(nextResponsibility, false);
                }
                else
                {
                    base.Update(nextResponsibility, false);
                }
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.Review;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.ApproveDocument) // Trường hợp tiếp theo người nhận trách nhiệm là người phê duyệt
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                if (nextResponsibility.Id == null || nextResponsibility.Id == Guid.Empty)
                {
                    base.Insert(nextResponsibility, false);
                }
                else
                {
                    base.Update(nextResponsibility, false);
                }
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.Approve;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.Promulgate) // Trường hợp tiếp theo người nhận trách nhiệm là người ban hành hủy tài liệu
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                if (nextResponsibility.Id == null || nextResponsibility.Id == Guid.Empty)
                {
                    nextResponsibility.IsApprove = true;
                    base.Insert(nextResponsibility, false);
                }
                else
                {
                    base.Update(nextResponsibility, false);
                }
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.WaitDestroy;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                SaveTransaction();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gửi cho người kiểm tra hoặc phê duyệt (không theo quy trình)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool SendReviewDestroySuggest(DocumentDestroySuggestResponsibitityBO data)
        {
            var responsibility = base.GetById(data.Id);
            responsibility.IsNew = true;
            responsibility.IsComplete = true;
            responsibility.ApproveAt = DateTime.Now;
            base.Update(responsibility, false);
            // Lấy thông tin đề nghị hủy
            var destroySuggest = ServiceFactory<DocumentDestroySuggestService>().GetById((Guid)responsibility.DocumentDestroyId);
            // Kiểm tra đề nghị hủy đã tồn tại vai trò chưa
            //var exits = base.Get(i => i.DocumentDestroyId == responsibility.DocumentDestroyId
            //                        && i.DepartmentId == responsibility.DepartmentId.Value && i.RoleType == data.RoleType).FirstOrDefault();
            var nextResponsibility = new DocumentDestroySuggestResponsibitityBO();
            //if (exits != null)
            //{
            //    nextResponsibility = exits;
            //    nextResponsibility.ApproveBy = data.EmployeeApproveBy.Id;
            //}
            //else
            //{
            nextResponsibility = new DocumentDestroySuggestResponsibitityBO()
            {
                RoleType = data.RoleType,
                DepartmentId = data.DepartmentId,
                DocumentDestroyId = responsibility.DocumentDestroyId,
                ApproveBy = data.EmployeeApproveBy.Id,
                Order = 0,
                IsArchiver = true
            };
            //}
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.Check) // Trường hợp tiếp theo người nhận trách nhiệm là người kiểm tra
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                //if (nextResponsibility.Id == null || nextResponsibility.Id == Guid.Empty)
                //{
                base.Insert(nextResponsibility, false);
                //}
                //else
                //{
                //    base.Update(nextResponsibility, false);
                //}
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.Review;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.ApproveDocument) // Trường hợp tiếp theo người nhận trách nhiệm là người phê duyệt
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                if (nextResponsibility.Id == null || nextResponsibility.Id == Guid.Empty)
                {
                    base.Insert(nextResponsibility, false);
                }
                else
                {
                    base.Update(nextResponsibility, false);
                }
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.Approve;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                SaveTransaction();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Phê duyệt
        /// </summary>
        /// <param name="data"></param>
        public void Approve(DocumentDestroySuggestResponsibitityBO data)
        {
            var responsibility = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetById(data.Id);
            responsibility.IsApprove = true;
            responsibility.ApproveNote = data.ApproveNote;
            responsibility.IsAccept = data.IsAccept;
            responsibility.ApproveAt = DateTime.Now;
            base.Update(responsibility, false);
            var lockItem = base.Get(i => i.DocumentDestroyId == responsibility.DocumentDestroyId
                                && i.Id != data.Id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            if (lockItem != null)
            {
                lockItem.IsLock = true;
                base.Update(lockItem, false);
            }
            // Nếu duyệt không đạt thì cập nhật trạng thái đề nghị về Không duyệt
            var destroySuggest = ServiceFactory<DocumentDestroySuggestService>().GetById(responsibility.DocumentDestroyId.Value);
            if (data.IsAccept == false)
            {
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.ApproveFail;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
            }
            // Nếu duyệt đạt thì chuyển đề nghị về chờ thực hiện lập biên bản và gửi cho người tạo đề nghị lập biên bản
            if (data.IsComplete != true && data.IsAccept == true)
            {
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.WaitDestroy;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                // Thêm vai trò người tạo biên bản là người đề nghị

            }
            SaveTransaction(true);
            return;
        }

        /// <summary>
        /// Phê duyệt đề nghị xong gửi sang người tạo đề nghị lập biên bản
        /// </summary>
        /// <param name="data"></param>
        public void SendApproveDestroySuggest(DocumentDestroySuggestResponsibitityBO data)
        {
            var responsibility = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetById(data.Id);
            responsibility.IsApprove = true;
            responsibility.ApproveNote = data.ApproveNote;
            responsibility.IsAccept = data.IsAccept;
            responsibility.ApproveAt = DateTime.Now;
            responsibility.IsComplete = true;
            base.Update(responsibility, false);
            var lockItem = base.Get(i => i.DocumentDestroyId == responsibility.DocumentDestroyId
                                && i.Id != data.Id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            if (lockItem != null)
            {
                lockItem.IsLock = true;
                base.Update(lockItem, false);
            }
            // Nếu duyệt không đạt thì cập nhật trạng thái đề nghị về Không duyệt
            var destroySuggest = ServiceFactory<DocumentDestroySuggestService>().GetById(responsibility.DocumentDestroyId.Value);
            if (data.IsAccept == false)
            {
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.ApproveFail;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
            }
            // Nếu duyệt đạt thì chuyển đề nghị về chờ thực hiện lập biên bản và gửi cho người tạo đề nghị lập biên bản
            if (data.IsComplete != true && data.IsAccept == true)
            {
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.WaitBillCreate;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
            }
            SaveTransaction(true);
            return;
        }

        /// <summary>
        /// Thực hiện hủy tài liệu khung ban hành
        /// </summary>
        /// <param name="data"></param>
        public void FormPublishDestroySuggestDocument(Guid Id)
        {
            var responsibility = base.GetById(Id);
            responsibility.IsNew = true;
            responsibility.IsComplete = true;
            responsibility.ApproveAt = DateTime.Now;
            base.Update(responsibility, false);

            var documents = ServiceFactory<DocumentDestroySuggestDetailService>().GetDocumentIdBySuggestId(responsibility.DocumentDestroyId.Value);
            foreach (var item in documents)
            {
                var document = ServiceFactory<DocumentService>().GetById(item);
                document.IsExpire = true;
                document.IsObsolate = false;
                document.IsPublish = false;
                document.DestroyAt = DateTime.Now;
                ServiceFactory<DocumentService>().Update(document, false);
            }

            var destroySuggest = new DocumentDestroySuggestBO()
            {
                Id = responsibility.DocumentDestroyId.Value,
                StatusType = (int)Resource.DocumentSuggestStatusType.Destroyed
            };

            ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
            SaveTransaction(true);
        }

        /// <summary>
        /// Chức năng thu hồi
        /// </summary>
        /// <param name="id"></param>
        public void Revert(Guid id)
        {
            try
            {
                var data = base.GetById(id);
                // Thu hồi vai trò của đề nghị hủy
                var revertItem = base.Get(i => i.DocumentDestroyId == data.DocumentDestroyId
                                && i.Id != data.Id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
                revertItem.IsNew = false;
                base.Update(revertItem, false);
                // Update trạng thái đề nghị hủy về mới
                var destroySuggest = ServiceFactory<DocumentDestroySuggestService>().GetById(data.DocumentDestroyId.Value);
                destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.New;
                ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public void RevertResponsibility(Guid id)
        {
            try
            {
                var data = base.GetById(id);
                data.IsComplete = false;
                data.IsLock = false;
                base.Update(data, false);
                // Thu hồi vai trò trước 
                var preResponsibility = base.Get(p => p.DepartmentId == data.DepartmentId && p.DocumentDestroyId == data.DocumentDestroyId && p.Id != data.Id
                                        && p.IsComplete != true && p.RoleType != (int)Resource.DocumentProcessRole.DestroyCreater).OrderBy(p => p.CreateAt).FirstOrDefault();
                preResponsibility.IsNew = false;
                preResponsibility.IsComplete = false;
                base.Update(preResponsibility, false);
                // Cập nhật đề nghị hủy về trạng thái Mới
                if (data.DocumentDestroyId.HasValue)
                {
                    var destroySuggest = ServiceFactory<DocumentDestroySuggestService>().GetById(data.DocumentDestroyId.Value);
                    destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.New;
                    ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                }
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Chuyển người khác thực hiện
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool ChangePerform(DocumentDestroySuggestResponsibitityBO data)
        {
            try
            {
                var responsibility = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetById(data.Id);
                if (data.EmployeeChange == null)
                    return false;
                responsibility.ApproveBy = data.EmployeeChange.Id;
                ServiceFactory<DocumentDestroySuggestResponsibitityService>().Update(responsibility);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Hủy vai trò của đề nghị
        /// </summary>
        /// <param name="id"></param>
        public void Destroy(Guid id)
        {
            try
            {
                var data = base.GetById(id);
                data.IsNew = false;
                data.IsComplete = false;
                data.IsLock = false;
                base.Update(data, false);
                // Cập nhật vai trò trước 
                var preResponsibility = base.Get(p => p.DepartmentId == data.DepartmentId && p.DocumentDestroyId == data.DocumentDestroyId && p.Id != data.Id).OrderByDescending(p => p.CreateAt).FirstOrDefault();
                preResponsibility.IsComplete = false;
                base.Update(preResponsibility, false);
                // Cập nhật đề nghị hủy về trạng thái Hủy
                if (data.DocumentDestroyId.HasValue)
                {
                    var destroySuggest = ServiceFactory<DocumentDestroySuggestService>().GetById(data.DocumentDestroyId.Value);
                    if (destroySuggest.StatusType == (int)Resource.DocumentSuggestStatusType.Review)
                    {
                        destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.New;
                    }
                    else if (destroySuggest.StatusType == (int)Resource.DocumentSuggestStatusType.Approve)
                    {
                        destroySuggest.StatusType = (int)Resource.DocumentSuggestStatusType.SendWait;
                    }
                    ServiceFactory<DocumentDestroySuggestService>().Update(destroySuggest, false);
                }
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Danh sách vai trò người xem xét/phê duyệt hiển thị trong khung phê duyệt
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentDestroySuggestResponsibitityBO> GetDestroySuggestApprove()
        {
            var destroySuggest = ServiceFactory<DocumentDestroySuggestService>()
                .Get(i => (i.StatusType == (int)Resource.DocumentSuggestStatusType.Review
                            || i.StatusType == (int)Resource.DocumentSuggestStatusType.Approve)
                            && i.IsDelete == false);
            var data = Enumerable.Empty<DocumentDestroySuggestResponsibitityBO>();
            data = base.Get(t => (t.ApproveBy.HasValue && t.ApproveBy.Value == UserId))
                .Where(t => t.DocumentDestroyId.HasValue && destroySuggest.Select(w => w.Id).Contains(t.DocumentDestroyId.Value))
                .Where(t => t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer
                            || t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval)
                .Where(t => t.IsNew == true || (t.IsComplete != true && t.IsLock != true))
                .Select(i => { i.UserId = UserId; return i; })
                .OrderByDescending(t => t.CreateAt);
            return data;
        }

        public IEnumerable<DocumentDestroySuggestResponsibitityBO> GetDestroySuggestCreateBill()
        {
            var destroySuggest = ServiceFactory<DocumentDestroySuggestService>()
                .Get(i => (i.StatusType == (int)Resource.DocumentSuggestStatusType.Review
                            || i.StatusType == (int)Resource.DocumentSuggestStatusType.Approve)
                            && i.IsDelete == false);
            var data = Enumerable.Empty<DocumentDestroySuggestResponsibitityBO>();
            data = base.Get(t => (t.ApproveBy.HasValue && t.ApproveBy.Value == UserId))
                .Where(t => t.DocumentDestroyId.HasValue && destroySuggest.Select(w => w.Id).Contains(t.DocumentDestroyId.Value))
                .Where(t => t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater)
                .Where(t => t.IsNew == true || (t.IsComplete != true && t.IsLock != true))
                .Select(i => { i.UserId = UserId; return i; })
                .OrderByDescending(t => t.CreateAt);
            return data;
        }
    }
}
