﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentDistributeService : BaseService<DocumentDistributeDTO, DocumentDistributeBO>, IDocumentDistributeService
    {

        /// <summary>
        /// Insert Distribute
        /// </summary>
        /// <param name="suggest"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid InsertDistribute(DocumentDistributeBO data, bool allowSave = true)
        {
            if (data.IsExternal != true)
            {
                data.Id = base.Insert(data, true);
                if (data.RequestId != Guid.Empty)
                {
                    var request = new DocumentRequestBO()
                    {
                        Id = data.RequestId.Value,
                        IsComplete = true
                    };
                    ServiceFactory<DocumentRequestService>().Update(request, true);
                }
            }
            else
            {
                var DocumentExternal = new DocumentExternalBO
                {
                    Name = data.DocumentExternal.Name,
                    Phone = data.DocumentExternal.Phone,
                    Email = data.DocumentExternal.Email,
                    Address = data.DocumentExternal.Address,
                    EmployeeName = data.DocumentExternal.EmployeeName,
                    Note = data.Note,
                };
                DocumentExternal.Id = ServiceFactory<DocumentExternalService>().Insert(DocumentExternal, true);
                data.ExternalId = DocumentExternal.Id;
                data.ReceiveBy = data.EmployeeReceive == null ? (Guid?)null : data.EmployeeReceive.Id;
                data.Id = base.Insert(data, true);
                if (data.RequestId != Guid.Empty)
                {
                    var request = new DocumentRequestBO()
                    {
                        Id = data.RequestId.Value,
                        IsComplete = true
                    };
                    ServiceFactory<DocumentRequestService>().Update(request, true);
                }
            }
            return data.Id;
        }
        /// <summary>
        /// Đếm số lượng tài liệu được phân phối cho từng đối tượng
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="destination"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        public int CountByDestination(Guid documentId, Guid destination, Resource.DestinationType destinationType)
        {
            var data = base.Get(p => p.DocumentId.Value == documentId);
            switch (destinationType)
            {
                //case Resource.DestinationType.Department:
                //    data = data.Where(p => p.DepartmentId == destination);
                //    break;
                case Resource.DestinationType.Employee:
                    data = data.Where(p => p.ReceiveBy == destination);
                    break;
                case Resource.DestinationType.External:
                    data = data.Where(p => p.ExternalId == destination);
                    break;
            }
            return data.Count();
        }

        /// <summary>
        /// Hàm khởi tạo mặc định phân phối cho tài liệu xác định
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public DocumentDistributeBO CreateDefault(Guid documentId)
        {
            var document = ServiceFactory<DocumentService>().GetById(documentId);
            // Lấy tên và màu sắc mức độ bảo mật
            if (document.SecurityId.HasValue && document.SecurityId != Guid.Empty)
            {
                var sercurity = ServiceFactory<DocumentSecurityService>().GetById(document.SecurityId.Value);
                document.Security = sercurity;
            }
            // Lấy tên danh mục
            if (document.CategoryId.HasValue && document.CategoryId != Guid.Empty)
            {
                var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
                document.CategoryName = category.Name;
            }
            // Lấy tên người soạn thảo
            if (document.WriteBy.HasValue && document.WriteBy != Guid.Empty)
            {
                var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(document.WriteBy.Value);
                document.Writer = employee.Name;
            }
            // Lấy file đính kèm
            var temp = ServiceFactory<AssetTempService>().GetTempByAssetId(document.Id);
            document.FileAttachs = new FileUploadBO()
            {
                Files = temp.ToList()
            };
            var data = new DocumentDistributeBO()
            {
                Document = document,
                DocumentId = documentId
            };
            return data;
        }
        /// <summary>
        /// Đưa ra danh sách Id tài liệu phân phối đến người đang đăng nhập hệ thống
        /// Bao gồm phòng ban, chức danh, cá nhân
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Guid> GetDocumentIdsDistribute()
        {
            //1. Lấy ra danh sách phòng ban liên quan
            var departmentIds = ServiceFactory<DepartmentService>().GetDepartmentsByCurrentUser().Select(i => i.Id);
            //2. Lấy ra danh sách chức danh liên quan
            var titleIds = ServiceFactory<EmployeeTitleService>().GetTitleIdsByCurrentUser();
            //3. Đưa ra điều kiện là phòng ban hoặc chức danh hoặc cá nhân
            /// var data = Get(i => departmentIds.Contains(i.DepartmentId.Value) || titleIds.Contains(i.TitleId.Value) || i.ReceiveBy == UserId).Select(i => i.DocumentId.Value).Distinct();
            //4. Lấy danh sách Id với điều kiện ở bước 3
            // return data;
            return departmentIds;
        }
        /// <summary>
        /// Lấy danh sách tài liệu của phòng ban được phân phối đến user đăng nhập
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Guid> GetDocumentIdsDistributeByEmployee(Guid? departmentId)
        {
            var departmentIds = ServiceFactory<DepartmentService>().GetDepartmentsByCurrentUser().Select(i => i.Id);
            //3. Danh sách tài liệu phân phối tới tôi
            // var data = Get(i => i.DepartmentId == departmentId && i.ReceiveBy == UserId).Select(i => i.DocumentId.Value).Distinct();
            //4. Lấy danh sách Id với điều kiện ở bước 3
            return departmentIds;
        }

        /// <summary>
        /// Lưu nơi nhận phân phối khi duyệt đề nghị ban hành
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="jsonData"></param>
        public IEnumerable<Guid> SaveDistribute(Guid documentId, string jsonData = "", bool allowSave = true)
        {
            var destinations = JsonConvert.DeserializeObject<List<DocumentDestinationBO>>(jsonData);
            var result = Enumerable.Empty<Guid>();
            if (destinations != null && destinations.Count > 0)
            {
                var data = new List<DocumentDistributeBO>();
                foreach (var item in destinations)
                {
                    var distribute = new DocumentDistributeBO();
                    distribute.DocumentId = documentId;
                    var destinationId = item.DestinationId.Split('_').ToList();
                    switch (destinationId[0])
                    {
                        case "Department":
                            //   distribute.DepartmentId = new Guid(destinationId[1]);
                            break;
                        case "Title":
                            //    distribute.TitleId = new Guid(destinationId[1]);
                            break;
                        case "Employee":
                            distribute.ReceiveBy = new Guid(destinationId[1]);
                            break;
                        case "External":
                            distribute.ExternalId = new Guid(destinationId[1]);
                            break;
                    }
                    data.Add(distribute);
                }
                result = InsertRange(data, allowSave);
            }
            return result;
        }

        public IEnumerable<DocumentPermissionBO> GetPermissionData()
        {
            int count = 0;
            var employees = ServiceFactory<EmployeeService>().Page(out count, 1, 20);
            var data = new List<DocumentPermissionBO>();
            int index = 1;
            foreach (var i in employees)
            {
                var rd = new Random();
                var item = new DocumentPermissionBO()
                {
                    Id = i.Id,
                    ObjectName = i.Name,
                    ObjectImage = i.AvatarUrl,
                    ObjectTitle = "Tổng giám đốc",
                    AllowView = rd.Next(100000) % 2 == 1,
                    AllowDownload = rd.Next(100000) % 2 == 1

                };
                data.Add(item);
                index++;
            }
            return data;
        }

        /// <summary>
        /// Detele Distribute
        /// </summary>
        /// <param name="DistributeId"></param>
        /// <param name="allowSave"></param>

        public void DeleteDistribute(Guid DistributeId, bool allowSave = true)
        {
            var data = base.GetById(DistributeId);
            if (data.IsExternal != true)
            {
                base.Delete(DistributeId, false);
            }
            else
            {
                ServiceFactory<DocumentExternalService>().Delete(data.ExternalId.Value, false);
                base.Delete(DistributeId, false);
            }
            SaveTransaction(allowSave);

        }
        /// <summary>
        /// Get Distribute by DocumentId 
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<DocumentDistributeBO> GetAll(Guid DocumentId, int pageIndex, int pageSize, out int count)
        {
            var data = base.Get(i => i.DocumentId == DocumentId);
            count = data.Count();
            data = Page(data, pageIndex, pageSize).ToList();
            foreach (var item in data)
            {
                if (item.ReceiveBy.HasValue && item.ReceiveBy.Value != Guid.Empty)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.ReceiveBy.Value);
                    item.EmployeeReceive = employee;
                    var title = ServiceFactory<EmployeeTitleService>().Get(i => i.EmployeeId == employee.Id).FirstOrDefault();
                    if (title != null)
                    {
                        var result = ServiceFactory<DepartmentTitleService>().GetById(title.TitleId);
                        item.Title = result.Name;
                    }
                }
                if (item.DepartmentId.HasValue && item.DepartmentId.Value != Guid.Empty)
                {
                    var department = ServiceFactory<DepartmentService>().GetById(item.DepartmentId == null ? Guid.Empty : item.DepartmentId.Value);
                    item.NameDepartment = department.Name;
                }

                if (item.ExternalId.HasValue && item.ExternalId.Value != Guid.Empty)
                {
                    var External = ServiceFactory<DocumentExternalService>().GetById(item.ExternalId.Value);
                    item.DocumentExternal = External;
                }
            }

            return data;
        }
        /// <summary>
        /// GetByid Distribute
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public DocumentDistributeBO GetDetailDistribute(Guid? DistributeId)
        {
            var data = base.GetById(DistributeId.Value);
            if (data.IsExternal != true)
            {
                var department = ServiceFactory<DepartmentService>().GetById(data.DepartmentId == null ? Guid.Empty : data.DepartmentId.Value);
                var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.ReceiveBy.Value);
                data.NameEmploy = employee.Name;
                data.NameDepartment = department.Name;
                return data;
            }
            else
            {
                var External = ServiceFactory<DocumentExternalService>().GetById(data.ExternalId.Value);
                data.DocumentExternal = External;
                return data;
            }
        }


        public IEnumerable<Guid> GetDocumentIdByCurrentUser()
        {
            //1. Phòng ban người hiện tại
            var departmentIds = ServiceFactory<DepartmentService>().GetDepartmentsByCurrentUser().Select(i => i.Id);
            //2. Chức danh người hiện tại
            var titleIds = ServiceFactory<EmployeeTitleService>().GetTitleIdsByCurrentUser();
            var data = GetDocumentIdByCurrentUser(departmentIds, UserId);
            return data;
        }
        public IEnumerable<Guid> GetDocumentIdByCurrentUser(IEnumerable<Guid> departmentIds, Guid userId)
        {
            var data = Get(i => (i.DepartmentId.HasValue && departmentIds.Contains(i.DepartmentId.Value)) || i.ReceiveBy == userId);
            return data.Select(i => i.DocumentId.Value);
        }



    }
}
