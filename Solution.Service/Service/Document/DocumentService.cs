﻿using iDAS.ADO;
using iDAS.DataAccess;
using iDAS.Service.Common;
using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace iDAS.Service
{
    public class DocumentService : BaseService<DocumentDTO, DocumentBO>, IDocumentService
    {
        /// <summary>
        /// Lấy danh sách yêu cầu biên soạn chưa thực hiện theo người đăng nhập
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetDocumentRequestByCurrentUser()
        {
            var request = ServiceFactory<DocumentRequestService>().Get(p => p.PerformBy == UserId);
            var data = base.GetByIds(request.Select(p => p.DocumentId.Value)).Where(p => p.IsWrite == null);
            return data;
        }
        public List<Guid> GetDepartmentsListParent(Guid ID, List<Guid> LstOut)
        {
            if (LstOut == null || LstOut.Count == 0)
                LstOut = new List<Guid>();
            if (LstOut.IndexOf(ID) == -1)
                LstOut.Add(ID);
            List<DocumentCategoryBO> department = ServiceFactory<DocumentCategoryService>().Get(x => x.ParentId.HasValue && x.ParentId.Value == ID).ToList();
            if (department.Count > 0)
            {
                foreach (DocumentCategoryBO item in department)
                    GetDepartmentsListParent(item.Id, LstOut);
            }
            return LstOut;
        }
        /// <summary>
        /// Đếm số lượng tài liệu theo danh mục tài liệu
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public int CountByCategory(Guid categoryId)
        {
            List<Guid> lstCategory = new List<Guid>();
            lstCategory = GetDepartmentsListParent(categoryId, lstCategory);
            var result = Get(i => i.CategoryId.HasValue && lstCategory.Contains(i.CategoryId.Value)).Count();
            return result;
        }
        public int CountByDistributeCategory(Guid categoryId)
        {
            var result = Get(i => !string.IsNullOrEmpty(i.DistributeCategoryIds) && i.DistributeCategoryIds.Contains(categoryId.ToString())).Count();
            return result;
        }

        /// <summary>
        /// Lấy danh sách các tài liệu đang soạn thảo
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetDocumentWriteByCurrentUser()
        {
            var request = ServiceFactory<DocumentRequestService>().Get(p => p.PerformBy == UserId);
            var data = base.GetByIds(request.Select(p => p.DocumentId.Value)).Where(p => p.IsWrite == true);
            return data;
        }

        /// <summary>
        /// Lấy danh sách tài liệu theo danh mục và phân phối
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="distribute"></param>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetAll(Guid categoryId, int pageIndex, int pageSize, out int count, string filterName = "", bool distribute = true)
        {
            var result = base.Get(p => p.CategoryId == categoryId
                && (string.IsNullOrEmpty(filterName) ? true : p.Name.Contains(filterName)));
            count = result.Count();
            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            var securities = ServiceFactory<DocumentSecurityService>().GetByIds(data.Select(i => i.SecurityId.Value).Distinct());
            foreach (var item in data)
            {
                var sercurity = securities.FirstOrDefault(i => i.Id == item.SecurityId.Value);
                item.Security = sercurity;
                if (item.IsPublish == true)
                {
                    var publish = ServiceFactory<DocumentPublishService>().GetByDocumentId(item.Id);
                    item.PublishAt = publish.PublishAt;
                }
                var temp = ServiceFactory<DocumentAttachmentService>().GetByDocument(item.Id);
                item.FileAttachs = new FileUploadBO()
                {
                    Files = temp.ToList()
                };
            }
            return data;
        }
        /// <summary>
        /// Lay document theo dieu kien chua yeu cau huy
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filterName"></param>
        /// <param name="distribute"></param>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetAllByRequestDestroy()
        {
            var RequestDocument = ServiceFactory<DocumentRequestService>().Get(i => i.TypeDestroy == true).Select(p => p.DocumentId);
            // Lấy những tài liệu đã ban hành nhưng không lấy tài liệu có trạng thái lỗi thời hoặc đã hết hạn
            var documentPublish = ServiceFactory<DocumentPublishService>().Get(p => p.Status != Resource.DocumentStatus.Obsolate && p.Status != Resource.DocumentStatus.Expire);
            // Lấy những tài liệu theo DocumentId đã ban hành ở trên
            var data = base.Get(p => documentPublish.Select(n => n.DocumentId).Contains(p.Id));
            data = data.Where(p => !RequestDocument.Contains(p.Id)).Select(i =>
            {
                if (i.SecurityId.HasValue)
                {
                    var security = ServiceFactory<DocumentSecurityService>().GetById(i.SecurityId.Value);
                    i.Security = security;
                }
                // Lấy ngày ban hành, lần ban hành và trạng thái ban hành từ danh sách tài liệu đã ban hành ở bước 2
                var publishItem = documentPublish.Where(p => p.DocumentId.Value == i.Id).FirstOrDefault();
                i.PublishTime = publishItem == null ? string.Empty : publishItem.QuantityPublish.ToString();
                i.PublishAt = publishItem == null ? null : publishItem.PublishAt;
                i.PublishStatus = publishItem == null ? string.Empty : publishItem.StatusText;
                return i;
            });
            return data;
        }
        /// <summary>
        /// get tai lieu phan phoi
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetAllByRequestDistribute()
        {
            var RequestDocument = ServiceFactory<DocumentRequestService>().Get(i => i.TypeRole == (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeRequester).Select(p => p.DocumentId);
            // Lấy những tài liệu đã ban hành nhưng không lấy tài liệu có trạng thái lỗi thời hoặc đã hết hạn
            var documentPublish = ServiceFactory<DocumentPublishService>().Get(p => p.Status != Resource.DocumentStatus.Obsolate && p.Status != Resource.DocumentStatus.Expire);
            // Lấy những tài liệu theo DocumentId đã ban hành ở trên
            var data = base.Get(p => documentPublish.Select(n => n.DocumentId).Contains(p.Id));
            data = data.Where(p => !RequestDocument.Contains(p.Id)).Select(i =>
            {
                if (i.SecurityId.HasValue)
                {
                    var security = ServiceFactory<DocumentSecurityService>().GetById(i.SecurityId.Value);
                    i.Security = security;
                }
                // Lấy ngày ban hành, lần ban hành và trạng thái ban hành từ danh sách tài liệu đã ban hành ở bước 2
                var publishItem = documentPublish.Where(p => p.DocumentId.Value == i.Id).FirstOrDefault();
                i.PublishTime = publishItem == null ? string.Empty : publishItem.QuantityPublish.ToString();
                i.PublishAt = publishItem == null ? null : publishItem.PublishAt;
                i.PublishStatus = publishItem == null ? string.Empty : publishItem.StatusText;
                return i;
            });
            return data;
        }
        /// <summary>
        ///  Lấy danh sách tài liệu chưa tạo yêu cầu sửa đổi
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetAllByRequestAdjust(int pageIndex, int pageSize, out int count, bool isDestroy = false, string filter = "", bool isDistribute = true, string departmentId = "")
        {
            // Lấy những tài liệu đã yêu cầu sửa đổi
            var RequestDocument = ServiceFactory<DocumentRequestService>().Get(i => i.TypeAdjust == true).Select(p => p.DocumentId.Value);
            // Lấy những tài liệu đã ban hành nhưng không lấy tài liệu có trạng thái lỗi thời hoặc đã hết hạn
            var documentPublish = ServiceFactory<DocumentPublishService>().Get(p => p.IsPublish == true);
            // Lấy những tài liệu theo DocumentId đã ban hành ở trên
            var data = base.Get(p => documentPublish.Select(n => n.DocumentId).Contains(p.Id));
            // Lấy những tài liệu đã ban hành nhưng chưa tạo yêu cầu sửa đổi
            data = data.Where(p => !RequestDocument.Contains(p.Id)).Select(i =>
            {
                if (i.CategoryId.HasValue)
                {
                    var category = ServiceFactory<DocumentCategoryService>().GetById(i.CategoryId.Value);
                    // Lấy DepartmentId theo danh mục
                    i.DepartmentId = category == null ? null : category.DepartmentId;
                }
                if (i.SecurityId.HasValue)
                {
                    var security = ServiceFactory<DocumentSecurityService>().GetById(i.SecurityId.Value);
                    i.Security = security;
                }
                // Lấy ngày ban hành, lần ban hành và trạng thái ban hành từ danh sách tài liệu đã ban hành ở bước 2
                var publishItem = documentPublish.Where(p => p.DocumentId.Value == i.Id).FirstOrDefault();
                i.PublishTime = publishItem == null ? string.Empty : publishItem.QuantityPublish.ToString();
                i.PublishAt = publishItem == null ? null : publishItem.PublishAt;
                i.PublishStatus = publishItem == null ? string.Empty : publishItem.StatusText;
                return i;
            });
            // Tìm kiếm theo phòng ban
            if (!String.IsNullOrEmpty(departmentId))
            {
                if (departmentId.IndexOf("Department_") != -1)
                {
                    var department = departmentId.Split('_').ToList();
                    if (department != null && department.Count() == 2)
                    {
                        data = data.Where(p => p.DepartmentId == new Guid(department[1]));
                    }
                }
                else
                {
                    data = data.Where(p => p.DepartmentId == new Guid(departmentId));
                }
            }
            // Tìm kiếm theo tên hoặc mã tài liệu
            if (!String.IsNullOrEmpty(filter))
            {
                data = data.Where(p => p.Name.ToLower().Contains(filter) || p.Code.ToLower().Contains(filter));
            }
            data = Page(data, pageIndex, pageSize);
            count = data.Count();
            return data;
        }
        /// <summary>
        /// Đưa ra chức danh có quyền phê duyệt tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public Guid GetByPublishApprove(Guid documentId)
        {
            var result = Guid.Empty;
            var document = GetByIdFullInfo(documentId);
            if (document != null)
            {
                var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
                //   result = category == null ? Guid.Empty : category.ApproveRole.Value;
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetDistributeByCategory(Guid categoryId, int pageIndex, int pageSize, out int count, string filter, Resource.DocumentStatus status)
        {
            var data = Get(i => !string.IsNullOrEmpty(i.DistributeCategoryIds) && i.DistributeCategoryIds.Contains(categoryId.ToString()))
                .Where(i => string.IsNullOrEmpty(filter) || (!string.IsNullOrEmpty(i.Name) && i.Name.StartsWith(filter)))
                .Where(i => status == Resource.DocumentStatus.All ||
                    (status == Resource.DocumentStatus.Destroy ? i.IsDestroy == true
                    : status == Resource.DocumentStatus.Expire ? i.IsExpire == true
                    : status == Resource.DocumentStatus.Publish ? i.IsPublish == true
                    : status == Resource.DocumentStatus.Obsolate ? i.IsObsolate == true
                    : false));
            count = data.Count();
            data = Page(data, pageIndex, pageSize);
            var securityIds = data.Where(i => i.SecurityId.HasValue).Select(i => i.SecurityId.Value);
            var securities = ServiceFactory<DocumentSecurityService>().GetByIds(securityIds);
            var attachments = ServiceFactory<DocumentAttachmentService>().GetByDocuments(data.Select(i => i.Id));
            data = data.Select(i =>
            {
                if (i.SecurityId.HasValue)
                {
                    var security = securities.FirstOrDefault(u => i.SecurityId == u.Id);
                    i.Security = security;
                }
                var attachment = attachments.Where(u => u.DocumentId == i.Id).Select(u => u.FileId.Value);
                i.FileAttachs = new FileUploadBO() { Files = attachment.ToList() };
                return i;
            });
            return data;
        }

        public IEnumerable<DocumentBO> GetByCategory(Guid categoryId, int pageIndex, int pageSize, out int count, string filter, Resource.DocumentStatus status)
        {
            List<Guid> lstCategory = new List<Guid>();
            lstCategory = GetDepartmentsListParent(categoryId, lstCategory);
            var data = GetQuery().Where(i => i.CategoryId.HasValue && lstCategory.Contains(i.CategoryId.Value))
                .Where(i => string.IsNullOrEmpty(filter) || (!string.IsNullOrEmpty(i.Name) && i.Name.ToUpper().Contains(filter.ToUpper())) || (!string.IsNullOrEmpty(i.Code.ToUpper()) && i.Code.Contains(filter.ToUpper())))
                .Where(i => status == Resource.DocumentStatus.All ||
                    (status == Resource.DocumentStatus.Destroy ? i.IsDestroy == true
                    : status == Resource.DocumentStatus.Expire ? i.IsExpire == true
                    : status == Resource.DocumentStatus.Publish ? i.IsPublish == true
                    : status == Resource.DocumentStatus.Obsolate ? i.IsObsolate == true
                    : false));
            count = data.Count();
            var result = Page(data.OrderBy(i => i.CreateAt), pageIndex, pageSize).ToList();
            var documentIds = result.Select(i => i.Id).ToList();
            var securityIds = data.Where(i => i.SecurityId.HasValue).Select(i => i.SecurityId.Value);
            var securities = ServiceFactory<DocumentSecurityService>().GetByIds(securityIds).ToList();
            var attachments = ServiceFactory<DocumentAttachmentService>().GetByDocuments(data.Select(i => i.Id)).ToList();
            var references = ServiceFactory<DocumentReferenceService>().GetByDocumentIds(documentIds);
            return result.Select(i =>
            {
                if (i.SecurityId.HasValue)
                {
                    var security = securities.FirstOrDefault(u => i.SecurityId == u.Id);
                    i.Security = security;
                }
                var countReferences = references.Count(u => u.DocumentId == i.Id);
                i.CountReferences = countReferences;
                var attachment = attachments.Where(u => u.DocumentId == i.Id).Select(u => u.FileId.Value);
                i.FileAttachs = new FileUploadBO() { Files = attachment.ToList() };
                return i;
            });
        }
        /// <summary>
        /// Lấy danh sách tài liệu đã ban hành
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filter">Tìm kiếm theo tên tài liệu hoặc mã tài liệu</param>
        /// <param name="isDistribute">True: tài liệu phân phối đến tôi, False: tài liệu không phân phối đến tôi</param>
        /// <param name="departmentId">Tài liệu theo phòng ban</param>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetDocumentPublish(int pageIndex, int pageSize, out int count, bool isDestroy = false, string filter = "", bool isDistribute = true, string departmentId = "")
        {
            try
            {
                // Lấy những tài liệu đã tạo yêu cầu sửa đổi
                var adjustRequest = ServiceFactory<DocumentRequestService>().Get(p => p.TypeAdjust == true);
                //1. Lấy những DocumentId đã tạo yêu cầu hủy
                var destroyRequest = ServiceFactory<DocumentRequestService>().Get(p => p.TypeDestroy == true).Select(p => p.DocumentId);
                //2. Danh sách tài liệu phân phối tới tôi
                var documentDistributeIds = ServiceFactory<DocumentDistributeService>().GetDocumentIdByCurrentUser();
                //3. Lấy những tài liệu đã ban hành nhưng không lấy tài liệu có trạng thái lỗi thời hoặc đã hết hạn
                var documentPublish = ServiceFactory<DocumentPublishService>().Get(p => p.Status != Resource.DocumentStatus.Obsolate && p.Status != Resource.DocumentStatus.Expire);
                //4. Lấy những tài liệu theo DocumentId đã ban hành ở trên
                var data = base.Get(p => documentPublish.Select(n => n.DocumentId).Contains(p.Id));
                if (isDestroy)
                {
                    //5. Lấy những tài liệu đã ban hành nhưng chưa tạo yêu cầu hủy
                    if (destroyRequest != null && destroyRequest.Count() > 0)
                        data = data.Where(p => !destroyRequest.Contains(p.Id));
                }
                //6. Trường hợp tài liệu phân phối đến tôi
                if (isDistribute)
                {
                    data = data.Where(i => documentDistributeIds.Contains(i.Id));
                }
                // Trường hợp tài liệu không phân phối tới tôi
                //else
                //{
                //    data = data.Where(i => documentDistributeIds.Any() ? true : !documentDistributeIds.Contains(i.Id));
                //}
                foreach (var item in data)
                {
                    if (item.CategoryId.HasValue)
                    {
                        var category = ServiceFactory<DocumentCategoryService>().GetById(item.CategoryId.Value);
                        // Lấy DepartmentId theo danh mục
                        item.DepartmentId = category == null ? null : category.DepartmentId;
                    }
                    // Lấy màu sắc và mức độ mật
                    if (item.SecurityId.HasValue)
                    {
                        var sercurity = ServiceFactory<DocumentSecurityService>().GetById(item.SecurityId.Value);
                        item.Security = sercurity == null ? null : sercurity;
                    }
                    // Lấy file đính kèm
                    var temp = ServiceFactory<DocumentAttachmentService>().GetByDocument(item.Id);
                    item.FileAttachs = new FileUploadBO()
                    {
                        Files = temp.ToList()
                    };
                    // Lấy ngày ban hành, lần ban hành và trạng thái ban hành từ danh sách tài liệu đã ban hành ở bước 2
                    var publishItem = documentPublish.Where(p => p.DocumentId.Value == item.Id).FirstOrDefault();
                    item.PublishTime = publishItem == null ? string.Empty : publishItem.QuantityPublish.ToString();
                    item.PublishAt = publishItem == null ? null : publishItem.PublishAt;
                    item.PublishStatus = publishItem == null ? string.Empty : publishItem.StatusText;
                    // Gán nội dung tài liệu đã tạo yêu cầu sửa đổi
                    var adjustItem = adjustRequest.Where(p => p.DocumentId.Value == item.Id).FirstOrDefault();
                    if (adjustItem != null)
                    {
                        item.AdjustNote = adjustItem == null ? string.Empty : adjustItem.Note;
                        item.AdjustStartAt = adjustItem == null ? null : adjustItem.StartAt;
                        item.AdjustEndAt = adjustItem == null ? null : adjustItem.EndAt;
                        item.RequestId = adjustItem.Id;
                        item.EmployeeAdjustPerform = adjustItem == null ? null : ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(adjustItem.PerformBy.Value);
                    }
                }
                // Tìm kiếm theo phòng ban
                if (!String.IsNullOrEmpty(departmentId))
                {
                    if (departmentId.IndexOf("Department_") != -1)
                    {
                        var department = departmentId.Split('_').ToList();
                        if (department != null && department.Count() == 2)
                        {
                            data = data.Where(p => p.DepartmentId == new Guid(department[1]));
                        }
                    }
                    else
                    {
                        data = data.Where(p => p.DepartmentId == new Guid(departmentId));
                    }
                }
                // Tìm kiếm theo tên hoặc mã tài liệu
                if (!String.IsNullOrEmpty(filter))
                {
                    data = data.Where(p => p.Name.ToLower().Contains(filter) || p.Code.ToLower().Contains(filter));
                }
                data = Page(data, pageIndex, pageSize);
                count = data.Count();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Danh sách tài liệu theo phòng ban đã ban hành & phân phối đến người đăng nhập hệ thống
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="key"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetDocumentDistributeToCurrentUser(int pageIndex, int pageSize, out int count, string key, string departmentIdString)
        {
            try
            {
                var data = Enumerable.Empty<DocumentBO>();
                Guid departmentId;
                // Tìm kiếm theo phòng ban
                if (!string.IsNullOrEmpty(departmentIdString) && departmentIdString.Contains("Department_"))
                {
                    departmentIdString = departmentIdString.Replace("Department_", "");
                }
                departmentId = new Guid(departmentIdString);
                /// Đưa ra danh sách phòng ban
                var currentDepartmentIds = ServiceFactory<DepartmentService>().GetDepartmentsByCurrentUser().Select(i => i.Id);
                /// Danh sách danh mục tương ứng của phòng ban đó
                var distributeCategoryIds = ServiceFactory<DocumentCategoryService>().Get(i => currentDepartmentIds.Contains(i.DepartmentId.Value)).Select(u => u.Id);

                // Lấy danh sách tài liệu có trạng thái là ban hành
                data = Get(i => i.IsPublish == true && i.IsObsolate != true && i.IsExpire != true && i.IsDestroy != true && i.DepartmentId == departmentId
                    && (
                    string.IsNullOrEmpty(key) ||
                    ((!string.IsNullOrEmpty(i.Name) && i.Name.ToUpper().Contains(key.ToUpper()))
                    || (!string.IsNullOrEmpty(i.Code) && i.Code.ToUpper().Contains(key.ToUpper())))
                    ));
                //.Where(i => i.CategoryId.HasValue && (distributeCategoryIds.Contains(i.CategoryId.Value)
                //    || i.DistributeCategoryIds.Contains()
                //    ));

                //.Where()
                count = data.Count();
                data = Page(data, pageIndex, pageSize);
                var categoryIds = data.Where(i => i.CategoryId.HasValue).Select(i => i.CategoryId.Value);
                var categories = ServiceFactory<DocumentCategoryService>().GetByIds(categoryIds);
                data = data.Select(i =>
                {
                    if (i.CategoryId.HasValue)
                    {
                        var category = categories.FirstOrDefault(u => u.Id == i.CategoryId.Value);
                        i.Category = category;
                        i.CategoryName = category.Name;
                    }
                    return i;
                });
                return data;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documents"></param>
        /// <param name="codes"></param>
        /// <returns></returns>
        private IEnumerable<DocumentBO> GetDistinctByCode(IEnumerable<DocumentBO> documents, IEnumerable<string> codes)
        {
            var data = new List<DocumentBO>();
            foreach (var code in codes)
            {
                // Lấy ra tài liệu cuối cùng 
                var lastPublishItem = documents.Where(i => i.Code == code).OrderByDescending(i => i.CreateAt).FirstOrDefault();
                if (lastPublishItem != null)
                {
                    var security = lastPublishItem.SecurityId.HasValue ? ServiceFactory<DocumentSecurityService>().GetById(lastPublishItem.SecurityId.Value) : null;
                    lastPublishItem.Security = security;
                }
                data.Add(lastPublishItem);
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetReferencesByDocument(Guid documentId, int pageIndex, int pageSize, out int totalCount)
        {
            // var referenceIds = ServiceFactory<DocumentReferenceService>().GetByDocument(documentId);
            var data = Enumerable.Empty<DocumentBO>();
            //data = base.GetByIds(referenceIds)
            //.Select(i =>
            //{
            //    i.FileAttachs = new FileUploadBO()
            //    {
            //        Files = ServiceFactory<DocumentAttachmentService>().GetByDocument(i.Id)
            //    };
            //    return i;
            //});
            //totalCount = data.Count();
            //data = Page(data, pageIndex, pageSize);
            totalCount = 0;
            return data;
        }

        /// <summary>
        /// Danh sách tài liệu là duy nhất đưa ra với phiên bản cuối cùng được ban hành
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetReferences()
        {
            //1. Danh sách id tài liệu được phân phối tới tôi
            var documentDistributeIds = ServiceFactory<DocumentDistributeService>().GetDocumentIdsDistribute();
            //2. Đưa ra danh sách mã tài liệu đã được ban hành, dùng để xác định tài liệu là duy nhất
            var documents = Get(i => i.IsPublish == true);
            var documentCodes = documents.Select(i => i.Code).Distinct();
            documents = GetDistinctByCode(documents, documentCodes).Select(
                i =>
                {
                    var temp = ServiceFactory<DocumentAttachmentService>().GetByDocument(i.Id);
                    i.FileAttachs = new FileUploadBO()
                    {
                        Files = temp.ToList()
                    };
                    return i;
                }
                );
            return documents;
        }

        public List<DocumentRoleObjectBO> GetRoleByText(string names, string values)
        {
            var data = new List<DocumentRoleObjectBO>();
            try
            {
                if (!string.IsNullOrEmpty(names) && !string.IsNullOrEmpty(values) && names.Contains(',') && values.Contains(','))
                {
                    var listName = names.Trim(',').Split(',');
                    var listValue = values.Trim(',').Split(',');
                    if (listName.Length > 0)
                    {
                        for (int i = 0; i < listName.Length; i++)
                        {
                            var item = new DocumentRoleObjectBO() { Name = listName[i] };
                            if (listValue[i].IndexOf('_') != -1)
                            {
                                var value = listValue[i].Trim('_').Split('_');
                                item.Id = Common.Utilities.ConvertToGuid(value[1]);
                                item.Type = value[0];
                            }
                            else
                            {
                                var value = listValue[i].Trim('_').Split('_');
                                item.Id = Common.Utilities.ConvertToGuid(value[0]);
                                item.Type = value[0];
                            }
                            data.Add(item);
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return data.ToList();
        }

        public DocumentBO GetByIdFullInfo(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id, allowDeleted, allowDefaultIfNull);
            if (data.CategoryId.HasValue)
            {
                var category = ServiceFactory<DocumentCategoryService>().GetById(data.CategoryId.Value);
                data.CategoryId = category.Id;
                data.CategoryName = category.Name;
            }
            data.Security = ServiceFactory<DocumentSecurityService>().GetById(data.SecurityId.HasValue ? data.SecurityId.Value : Guid.Empty);
            if (data.DepartmentId.HasValue)
                data.DepartmentName = ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value).Name;
            data.Archive = ServiceFactory<DocumentArchiveService>().GetByDocument(id);
            data.IsReferenceExits = ServiceFactory<DocumentReferenceService>().Get(t => t.DocumentId == id).Count() > 0;

            var files = ServiceFactory<DocumentAttachmentService>().GetByDocument(data.Id);
            data.FileAttachs = new FileUploadBO()
            {
                Files = files.ToList()
            };
            return data;
        }

        public DocumentBO GetFormItem(Guid defaultCategory, string id, Guid departmentId)
        {
            var data = new DocumentBO()
            {
                Security = new DocumentSecurityBO()
            };
            try
            {
                var checkRole = true;// ServiceFactory<DocumentDepartmentPermissionService>().CheckRoleCanUpdateCategory(departmentId);
                if (string.IsNullOrEmpty(id))
                {
                    // Quyền thao tác với chức năng thêm mới tài liệu
                    if (!checkRole)
                    {
                        throw new AccessDenyException();
                    }
                    var category = ServiceFactory<DocumentCategoryService>().GetById(defaultCategory);
                    data.CategoryId = category.Id;
                    data.CategoryName = category.Name;
                    //data.DistributeCategoryIds = defaultCategory.ToString();
                    data.DepartmentId = departmentId;
                }
                else
                {
                    //2. Quyền truy cập với tài liệu
                    var checkAccess = ServiceFactory<DocumentPermissionService>().CheckPermission(data.Id);

                    var currentDepartmentIds = ServiceFactory<DepartmentService>().GetDepartmentsByCurrentUser().Select(i => i.Id);
                    // Để xem được chi tiết thì người đó phải có quyền truy cập vào tài liệu hoặc quyền quản lý danh mục tài liệu

                    data = GetByIdFullInfo(Common.Utilities.ConvertToGuid(id));

                    if (!checkRole && !checkAccess && !(data.DepartmentId.HasValue && currentDepartmentIds.Contains(data.DepartmentId.Value)))
                        throw new AccessDenyException();
                    data.CategoryId = defaultCategory;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return data;
        }

        /// <summary>
        /// Cập nhật tài liệu vào danh muc
        /// </summary>
        /// <returns></returns>
        public void InsertDocumentCategory(DocumentCategoryBO item, bool allowSave = true)
        {
            var result = new DocumentBO();
            result.Id = item.DocumentId.Value;
            // Cập nhật danh mục nội bộ
            if (item.Id != null && item.Id != Guid.Empty)
            {
                result.CategoryId = item.Id;
                result.IsExternal = false;
            }
            // Cập nhật danh mục bên ngoài
            if (item.ExternalId != null && item.ExternalId != Guid.Empty)
            {
                result.CategoryId = item.ExternalId;
                result.IsExternal = true;
            }
            result.NoteCategory = item.NoteCategory;
            result.Code = item.DocumentCode;
            base.Update(result, false);
            SaveTransaction(allowSave);
        }

        /// <summary>
        /// Xóa tài liệu vào danh muc phân phối
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        public void DeleteDocumentCategoryDistribute(DocumentCategoryBO item, bool allowSave = true)
        {
            var document = base.GetById(item.DocumentId.Value);
            if (!String.IsNullOrEmpty(document.DistributeCategoryIds))
            {
                if (document.DistributeCategoryIds.Contains(item.DistributeCategoryIds.ToString().Trim()))
                {
                    if (document.DistributeCategoryIds.ToString().Trim().Contains(","))
                    {
                        var strDistributeCategory = "";
                        var DistributeCategoryid = document.DistributeCategoryIds.ToString().Trim().Split(',').ToList();
                        for (int i = 0; i < DistributeCategoryid.Count; i++)
                        {
                            if (DistributeCategoryid[i].Contains(item.DistributeCategoryIds.ToString().Trim()))
                            {
                                DistributeCategoryid[i] = DistributeCategoryid[i].Replace(DistributeCategoryid[i], null);
                            }
                        }
                        foreach (var cate in DistributeCategoryid)
                        {
                            if (!String.IsNullOrEmpty(cate))
                                strDistributeCategory += cate + ",";
                        }
                        strDistributeCategory = strDistributeCategory.Remove(strDistributeCategory.Length - 1, 1);
                        var result1 = new DocumentBO()
                        {
                            Id = item.DocumentId.Value,
                            NoteCategory = string.Empty,
                            //DistributeCategoryIds = string.Join(",", DistributeCategoryid)
                            DistributeCategoryIds = strDistributeCategory
                        };
                        base.Update(result1, false);
                    }
                    else
                    {
                        var result1 = new DocumentBO()
                        {
                            Id = item.DocumentId.Value,
                            NoteCategory = string.Empty,
                            DistributeCategoryIds = string.Empty
                        };
                        base.Update(result1, false);
                    }
                }

            }


            SaveTransaction(allowSave);
        }
        /// <summary>
        /// Cập nhật tài liệu vào danh muc phân phối
        /// </summary>
        /// <returns></returns>
        public void InsertDocumentCategoryDistribute(DocumentCategoryBO item, bool allowSave = true)
        {
            var document = base.GetById(item.DocumentId.Value);
            var Category = ServiceFactory<DocumentCategoryService>().Get(i => i.DepartmentId == item.DepartmentId && i.IsDistribute == true);
            if (!String.IsNullOrEmpty(document.DistributeCategoryIds))
            {
                foreach (var cate in Category)
                {
                    if (document.DistributeCategoryIds.Contains(cate.Id.ToString()))
                    {
                        string oldId = cate.Id.ToString().Trim();
                        string newId = item.DistributeCategoryIds.Value.ToString().Trim();

                        document.DistributeCategoryIds = document.DistributeCategoryIds.Replace(oldId, newId);
                    }
                }
                var result1 = new DocumentBO()
                {
                    Id = item.DocumentId.Value,
                    NoteCategory = item.NoteCategory,
                    DistributeCategoryIds = document.DistributeCategoryIds.ToString()
                };
                base.Update(result1, false);
            }
            else
            {
                var result = new DocumentBO()
                {
                    Id = item.DocumentId.Value,
                    NoteCategory = item.NoteCategory,
                    DistributeCategoryIds = item.DistributeCategoryIds.ToString()
                };
                base.Update(result, false);
            }

            SaveTransaction(allowSave);
        }
        /// <summary>
        /// Xóa tài liệu khỏi danh mục
        /// </summary>
        /// <param name="item"></param>
        public void DeleteDocumentCategory(Guid DocumentId)
        {
            var result = new DocumentBO()
            {
                Id = DocumentId,
                CategoryId = Guid.Empty,
                NoteCategory = string.Empty

            };
            base.Update(result, true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        public void RemoveFromCategory(Guid documentId)
        {
            var document = GetByIdFullInfo(documentId);
            document.CategoryId = Guid.Empty;
            base.Update(document);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public Guid CreatePublishDocument(DocumentBO item, bool allowSave = true)
        {
            try
            {
                item.Id = base.Insert(item, allowSave);
                // Cập nhật đính kèm
                //upload file
                var fileIds = ServiceFactory<FileService>().UploadRange(item.FileAttachs, allowSave);
                //Insert file upload
                foreach (var file in fileIds)
                {
                    var obj = new DocumentAttachmentBO();
                    obj.DocumentId = item.Id;
                    obj.FileId = file;
                    ServiceFactory<DocumentAttachmentService>().Insert(obj, allowSave);
                }
                if (item.CategoryType == Resource.DocumentCategoryType.Distribute)
                {
                    // Insert distribute
                    var distribute = new DocumentDistributeBO()
                    {
                        ReceiveBy = UserId,
                        DocumentId = item.Id
                    };
                    ServiceFactory<DocumentDistributeService>().Insert(distribute, allowSave);
                }
                return item.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public void UpdateDocument(DocumentBO item, bool allowSave = true)
        {
            try
            {
                item.DepartmentId = item.IsExternal == true ? Guid.Empty : item.DepartmentId;
                item.PublishFrom = item.IsExternal == true ? item.PublishFrom : string.Empty;
                base.Update(item, false);
                //upload file
                var fileIds = ServiceFactory<FileService>().UploadRange(item.FileAttachs, false);
                var documentFileIds = ServiceFactory<DocumentAttachmentService>().GetIds(item.Id, item.FileAttachs.FileRemoves);
                ServiceFactory<DocumentAttachmentService>().DeleteRange(documentFileIds, false);
                //Insert file upload
                foreach (var file in fileIds)
                {
                    var obj = new DocumentAttachmentBO();
                    obj.DocumentId = item.Id;
                    obj.FileId = file;
                    ServiceFactory<DocumentAttachmentService>().Insert(obj, false);
                }
                if (item.Archive != null)
                {
                    if (item.Archive.Id != Guid.Empty)
                        ServiceFactory<DocumentArchiveService>().Update(item.Archive, false);
                    else
                        ServiceFactory<DocumentArchiveService>().Insert(item.Archive, false);
                }
                SaveTransaction(allowSave);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UpdateDocumentCategory(Guid Id, Guid CategoryId, bool allowSave = true)
        {
            try
            {
                var item = base.GetById(Id);
                item.CategoryId = CategoryId;
                base.Update(item, false);
                SaveTransaction(allowSave);
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Get Distribute 
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetDistribute()
        {
            var distribute = ServiceFactory<DocumentDistributeService>().Get(i => i.ReceiveBy == UserId).Select(p => p.DocumentId).Distinct();
            var documents = base.GetAll();
            var title = ServiceFactory<EmployeeTitleService>().Get(m => m.EmployeeId == UserId).Select(p => p.TitleId);
            var departments = ServiceFactory<DepartmentTitleService>().Get(p => title.Contains(p.Id)).Select(p => p.DepartmentId).Distinct();
            var data = documents.Where(p => distribute.Contains(p.Id)).Select(i =>
            {
                i.Check = true;
                if (i.SecurityId.HasValue)
                {
                    var sercurity = ServiceFactory<DocumentSecurityService>().GetById(i.SecurityId.Value);
                    i.Security = sercurity == null ? null : sercurity;
                }
                return i;
            });
            var data1 = documents.Where(p => departments.Contains(p.DepartmentId) && p.CategoryId == null).Select(i =>
            {
                i.Check = true;
                if (i.SecurityId.HasValue)
                {
                    var sercurity = ServiceFactory<DocumentSecurityService>().GetById(i.SecurityId.Value);
                    i.Security = sercurity == null ? null : sercurity;
                }
                i.Check = false;
                return i;
            });

            var result = Enumerable.Empty<DocumentBO>();
            result = data.Union(data1);
            return result;
        }

        /// <summary>
        /// Thông tin tài liệu con trong trường hợp tài liệu có tài liệu sửa đổi
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DocumentBO GetChild(Guid id)
        {
            var document = Get(i => i.ParentId == id).FirstOrDefault();
            if (document == null)
            {
                var parent = GetByIdFullInfo(id);
                document = new DocumentBO()
                {
                    ParentId = id,
                    Name = parent.Name,
                    Code = parent.Code,
                    CategoryId = parent.CategoryId
                };
            }
            return document;
        }

        /// <summary>
        /// Danh sách tài liệu đã ban hành
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetdataPublish(int pageIndex, int pageSize, out int count, string search)
        {
            try
            {
                var data = base.Get(p => p.IsPublish == true && p.IsDestroy != true && p.IsDelete == false);//.Select(i =>
                foreach (var i in data)
                {
                    if (i.CategoryId.HasValue)
                    {
                        var category = ServiceFactory<DocumentCategoryService>().GetById(i.CategoryId.Value);
                        i.CategoryId = category.Id;
                        i.CategoryName = category.Name;
                    }
                    if (i.SecurityId.HasValue)
                    {
                        var sercurity = ServiceFactory<DocumentSecurityService>().GetById(i.SecurityId.Value);
                        i.Security = sercurity == null ? null : sercurity;
                    }
                    var files = ServiceFactory<DocumentAttachmentService>().GetByDocument(i.Id);
                    i.FileAttachs = new FileUploadBO()
                    {
                        Files = files.ToList()
                    };
                    //return i;
                }//).OrderByDescending(p => p.CreateAt);
                data = Page(data, pageIndex, pageSize);
                count = data.Count();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Thêm mới tài liệu ban hành bên ngoài
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid InsertDocument(DocumentBO item, bool allowSave = true)
        {
            try
            {
                item.PublishFrom = item.IsExternal != true ? string.Empty : item.PublishFrom;
                item.Id = base.Insert(item);
                // Cập nhật đính kèm
                ServiceFactory<DocumentAttachmentService>().Update(item.FileAttachs, item.Id, allowSave);
                // Insert publish
                if (item.PublishAt.HasValue && item.ApplyAt.HasValue && item.ExpireAt.HasValue)
                {
                    var publish = new DocumentPublishBO()
                    {
                        DocumentId = item.Id,
                        IsPublish = true,
                        PublishAt = item.PublishAt,
                        ApplyAt = item.ApplyAt,
                        ExpireAt = item.ExpireAt,
                        PublishBy = UserId,
                        QuantityPublish = Convert.ToInt32(item.Revision)
                    };
                    ServiceFactory<DocumentPublishService>().Insert(publish);
                }

                if (!string.IsNullOrEmpty(item.Archive.Position))
                {
                    item.Archive.DocumentId = item.Id;
                    ServiceFactory<DocumentArchiveService>().Insert(item.Archive);
                }
                return item.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<DocumentBO> GetAllDocument(int pageIndex, int pageSize, out int count, string filterName = default(string), Resource.EDocumentStatus status = 0)
        {
            var result = Enumerable.Empty<DocumentBO>();
            result = base.GetQuery().Where(p => string.IsNullOrEmpty(filterName) || p.Name.Contains(filterName));
            result = result.Where(t => t.Status == status || status == 0);
            count = result.Count();
            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize)
            .Select(i =>
            {
                i.Security = ServiceFactory<DocumentSecurityService>().GetById(i.SecurityId.HasValue ? i.SecurityId.Value : Guid.Empty);
                i.Category = ServiceFactory<DocumentCategoryService>().GetById(i.CategoryId.HasValue ? i.CategoryId.Value : Guid.Empty);
                i.FileAttachs = new FileUploadBO()
                {
                    Files = ServiceFactory<DocumentAttachmentService>().GetByDocument(i.Id)
                };
                return i;
            });
            return data;
        }

        /// <summary>
        /// Danh sách người liên quan đến người đăng nhập hệ thống
        /// Bao gồm: Tài liệu phân phối đến người đăng nhập hệ thống
        /// Tài liệu người đó nằm trong trách nhiệm liên quan
        /// Tài liệu của phòng ban người đó
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filterName"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public IEnumerable<DocumentBO> GetDocumentForCurrentUser(int pageIndex, int pageSize, out int count, string filterName, Resource.EDocumentStatus status = 0)
        {
            //1. Danh sách chức danh của nhân sự hiện tại
            var titleIds = ServiceFactory<EmployeeTitleService>().GetQuery().Where(i => i.EmployeeId == UserId).Select(i => i.TitleId).Distinct();
            //2. Danh sách chức danh theo phòng ban
            var departmentTitles = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds);
            //3. Danh sách phòng ban của người đăng nhập hệ thống
            var currentDepartmentIds = departmentTitles.Where(i => i.DepartmentId.HasValue).Select(i => i.DepartmentId.Value);
            //4. Danh sách tài liệu liên quan người đăng nhập với vai trò lưu trữ/ phân quyền theo chức danh hoặc cá nhân
            var departmentIdsByPermission = ServiceFactory<DocumentDepartmentPermissionService>().GetQuery().Where(i => i.EmployeeId == UserId || (i.DepartmentTitleId.HasValue && titleIds.Contains(i.DepartmentTitleId.Value))).Select(i => i.DepartmentTitleId.Value);
            //5. Danh sách tài liệu mà liên quan đến người đăng nhập 
            var documentIdsByResponsibility = ServiceFactory<DocumentResponsibilityService>().GetQuery().Where(i => i.PerformBy == UserId && i.DocumentId.HasValue).Select(i => i.DocumentId.Value);
            //6. Danh sách danh mục theo các phòng ban trên
            //var categoryIdsByDepartment = ServiceFactory<DocumentCategoryService>()
            //DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_Documents_GetDocumentForCurrentUser",
            //        parameter: new
            //        {
            //            DepartmentId = currentDepartmentIds.Count() > 0? ","+string.Join(",",currentDepartmentIds)+"," : "",
            //            documentIdsByResponsibility = documentIdsByResponsibility.Count() > 0? ","+string.Join(",", documentIdsByResponsibility) +"," : "",
            //            UserId = UserId,
            //            Name = !string.IsNullOrEmpty(filterName) ? filterName : "",
            //            status = status,
            //            pageIndex = pageIndex,
            //            pageSize = pageSize
            //        }
            //    );
            var data = base.GetQuery().Where(i => (i.DepartmentId.HasValue && currentDepartmentIds.Contains(i.DepartmentId.Value))
                || documentIdsByResponsibility.Contains(i.Id)
                || i.CreateBy == UserId)
                .Where(i => string.IsNullOrEmpty(filterName) || (!string.IsNullOrEmpty(i.Name) && i.Name.ToUpper().Contains(filterName.ToUpper())))
                .Where(i => status == Resource.EDocumentStatus.All || (status == Resource.EDocumentStatus.Destroy ? i.IsDestroy == true
                    : status == Resource.EDocumentStatus.Expire ? i.IsExpire == true && i.IsDestroy != true
                    : status == Resource.EDocumentStatus.Obsolate ? i.IsObsolate == true && i.IsDestroy != true && i.IsExpire != true
                    : status == Resource.EDocumentStatus.Publish ? i.IsPublish == true && i.IsDestroy != true && i.IsExpire != true && i.IsObsolate != true
                    : status == Resource.EDocumentStatus.WaitPublish ? i.IsWaitPublish == true && i.IsPublish != true && i.IsDestroy != true && i.IsExpire != true && i.IsObsolate != true
                    : i.IsWaitPublish != true && i.IsPublish != true && i.IsDestroy != true && i.IsExpire != true && i.IsObsolate != true
                  ))
                .OrderByDescending(i => i.CreateAt);
            count = data.Count();
            var result = Page(data.AsEnumerable(), pageIndex, pageSize).ToList();
            var documentIds = data.Select(i => i.Id);
            var categoryIds = data.Where(i => i.CategoryId.HasValue).Select(i => i.CategoryId.Value);
            var securityIds = data.Where(i => i.SecurityId.HasValue).Select(i => i.SecurityId.Value);
            var categories = ServiceFactory<DocumentCategoryService>().GetByIds(categoryIds).ToList();
            var securities = ServiceFactory<DocumentSecurityService>().GetByIds(securityIds).ToList();
            var attachments = ServiceFactory<DocumentAttachmentService>().GetByDocuments(documentIds).ToList();
            var references = ServiceFactory<DocumentReferenceService>().GetByDocumentIds(documentIds).ToList();
            result = result.Select(
                i =>
                {
                    if (i.CategoryId.HasValue)
                    {
                        var category = categories.FirstOrDefault(u => u.Id == i.CategoryId.Value);
                        if (category != null)
                        {
                            i.Category = category;
                            i.CategoryName = category.Name;
                        }
                    }
                    if (i.SecurityId.HasValue)
                    {
                        var security = securities.FirstOrDefault(u => u.Id == i.SecurityId.Value);
                        i.Security = security;
                    }
                    var file = new FileUploadBO()
                    {
                        Files = attachments.Where(u => u.DocumentId == i.Id).Select(u => u.FileId.Value)
                    };
                    i.FileAttachs = file;
                    var countReferences = references.Count(u => u.DocumentId == i.Id);
                    i.CountReferences = countReferences;
                    return i;
                }).ToList();
            return result.Distinct();
        }

        /// <summary>
        /// Thông tin chi tiết tài liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DocumentBO GetDetail(Guid id)
        {
            try
            {
                var data = base.GetById(id);
                var archive = ServiceFactory<DocumentArchiveService>().GetQuery().FirstOrDefault(p => p.DocumentId == data.Id);
                var file = new FileUploadBO()
                {
                    Files = ServiceFactory<DocumentAttachmentService>().GetQuery().Where(u => u.DocumentId == data.Id).Select(u => u.FileId.Value)
                };
                data.FileAttachs = file;
                data.Archive = archive == null ? new DocumentArchiveBO() : archive;
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<DocumentBO> GetByCode(string documentCode)
        {
            var data = Get(i => !string.IsNullOrEmpty(i.Code) && i.Code == documentCode);
            return data;
        }

        public IEnumerable<FileBO> GetFileByDocumentId(Guid id)
        {
            var fileIds = ServiceFactory<DocumentAttachmentService>().GetQuery().Where(u => u.DocumentId == id).Select(u => u.FileId.Value);
            var data = ServiceFactory<FileService>().GetByIds(fileIds).Where(i => !string.IsNullOrEmpty(i.Extension) && i.Extension == Common.Resource.FileExtension.Word2007);
            return data;
        }

        public string Compare(Guid src, Guid dst)
        {
            try
            {
                var htmlCompare = string.Empty;
                var source = ServiceFactory<FileService>().Download(src);
                var dest = ServiceFactory<FileService>().Download(dst);
                var tempFolder = AppDomain.CurrentDomain.BaseDirectory;

                object srcDoc = tempFolder + "CompareTemp\\src.docx";
                object srcHtml = tempFolder + "CompareTemp\\src.html";
                object dstDoc = tempFolder + "CompareTemp\\dst.docx";
                object dstHtml = tempFolder + "CompareTemp\\dst.html";
                CreateFile(source.Stream, srcDoc, srcHtml);
                CreateFile(dest.Stream, dstDoc, dstHtml);
                var srcHtmlContent = File.ReadAllText(srcHtml.ToString(), Encoding.Default);
                var dstHtmlContent = File.ReadAllText(dstHtml.ToString(), Encoding.Default);
                var diffHelper = new HtmlDiff.HtmlDiff(System.Web.HttpUtility.HtmlDecode(dstHtmlContent), System.Web.HttpUtility.HtmlDecode(srcHtmlContent));
                diffHelper.AddBlockExpression(new System.Text.RegularExpressions.Regex(@"[\d]{1,2}[\s]*(Jan|Feb)[\s]*[\d]{4}", System.Text.RegularExpressions.RegexOptions.IgnoreCase));
                htmlCompare = diffHelper.Build();
                return htmlCompare;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CreateFile(Stream stream, object doc, object html)
        {
            using (Stream file = File.Create(doc.ToString()))
            {
                stream.CopyTo(file);
            }
            var applicationClass = new ApplicationClass();
            object missingType = Type.Missing;
            object readOnly = true;
            object isVisible = true;
            object documentFormat = WdSaveFormat.wdFormatHTML;
            object encoding = Microsoft.Office.Core.MsoEncoding.msoEncodingVietnamese;
            object allowSubstitutions = false;
            applicationClass.Documents.Open(ref doc, ref readOnly
                , ref missingType, ref missingType, ref missingType
                , ref missingType, ref missingType, ref missingType
                , ref missingType, ref missingType, ref encoding
                , ref missingType, ref missingType, ref missingType
                , ref missingType, ref missingType);
            applicationClass.Visible = false;
            Document document = applicationClass.ActiveDocument;
            document.SaveAs2(ref html, ref documentFormat, ref missingType
                , ref missingType, ref missingType, ref missingType
                , ref missingType, ref missingType, ref missingType
                , ref missingType, ref missingType, ref encoding
                , ref missingType, ref allowSubstitutions, ref missingType
                , ref missingType, ref missingType);
            document.Close(ref missingType, ref missingType, ref missingType);
        }

    }
}