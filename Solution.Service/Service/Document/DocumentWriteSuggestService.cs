﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentWriteSuggestService : BaseService<DocumentWriteSuggestDTO, DocumentWriteSuggestBO>, IDocumentWriteSuggestService
    {
        const int _minOrder = 0;
        public IEnumerable<DocumentWriteSuggestResponsibilityBO> GetByCurrentUser(int pageIndex, int pageSize, out int totalCount, iDAS.Service.Common.Resource.DocumentProcessRoleFilterSuggest roleType, Resource.DocumentSuggestStatusType statusType)
        {
            var writeSuggest = base.Get(i => (statusType == Resource.DocumentSuggestStatusType.All || i.StatusType == (int)statusType) && i.IsDelete == false);
            var resposibily = ServiceFactory<DocumentResponsibilityService>().GetByCurrentUser()
                                    .Where(i => (roleType == Resource.DocumentProcessRoleFilterSuggest.All
                                                || i.RoleType == (int)roleType) && i.IsNew != false && i.IsDelete == false
                                                && i.ExtendId.HasValue
                                                && writeSuggest.Select(w => w.Id).Contains(i.ExtendId.Value));
            var data = resposibily.Join(writeSuggest, r => r.ExtendId, w => w.Id,
                                        (r, w) => new DocumentWriteSuggestResponsibilityBO()
                                        {
                                            Id = r.Id,
                                            Name = w.Name,
                                            SuggestAt = w.SuggestAt,
                                            EstimateAt = w.EstimateAt,
                                            RoleType = r.RoleType,
                                            StatusType = w.StatusType,
                                            IsComplete = r.IsComplete,
                                            IsAccept = r.IsAccept,
                                            IsApprove = r.IsApprove,
                                            IsCancel = r.IsCancel,
                                            IsLock = r.IsLock,
                                            CreateAt = r.CreateAt
                                        }
                                      ).OrderByDescending(i => i.CreateAt);
            totalCount = data.Count();
            var result = data.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return result;
        }
        public IEnumerable<DocumentResponsibilityBO> GetEmployeeRoleBySuggest(Guid suggestId)
        {
            var resposibily = ServiceFactory<DocumentResponsibilityService>().GetByExtend(suggestId)
                                    .Where(i =>
                                            //(i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.SuggesterWriteAndEdit
                                            //                || i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.ApprovalWriteAndEdit
                                            //                || i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.ReviewWriteAndEdit)
                                            //  && 
                                            i.IsNew != false)
                                    .Select(item => new DocumentResponsibilityBO
                                    {
                                        Id = item.Id,
                                        RoleType = item.RoleType,
                                        IsApprove = item.IsApprove,
                                        IsAccept = item.IsAccept,
                                        IsComplete = item.IsComplete,
                                        EmployeeSend = item.PerformBy.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)item.PerformBy) : null,
                                        ApproveNote = item.ApproveNote,
                                        CreateAt = item.CreateAt,
                                    }
                                     ).OrderBy(i => i.CreateAt);
            return resposibily;
        }
        public DocumentWriteSuggestResponsibilityBO GetDetailForSugest(Guid id)
        {
            var data = GetDetail(id);
            var nextResponsibility = ServiceFactory<DocumentResponsibilityService>()
                .GetByExtend((Guid)data.ExtendId)
                .Where(i => i.CreateBy == data.SuggestBy && i.Id != id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            if (nextResponsibility != null && nextResponsibility.PerformBy.HasValue)
            {
                var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)nextResponsibility.PerformBy);
                data.EmployeeRecive = new DocumentEmployeeBO
                {
                    Id = nextResponsibility.PerformBy.Value,
                    Name = employee.Name,
                    AvatarUrl = employee.AvatarUrl,
                    RoleNames = employee.RoleNames,
                    NextOrder = nextResponsibility.Order.Value,
                    RoleType = nextResponsibility.RoleType.Value
                };
            }
            return data;
        }
        public DocumentWriteSuggestResponsibilityBO GetDetailForDocument(Guid id)
        {
            var data = new DocumentWriteSuggestResponsibilityBO();
            var suggest = Get(i => i.DocumentId == id).FirstOrDefault();
            if (suggest != null)
            {
                //var suggestId = suggest.Id;
                data = GetDetailByDocument(id);
                var nextResponsibility = ServiceFactory<DocumentResponsibilityService>()
                    .GetByExtend((Guid)data.ExtendId)
                    .Where(i => i.CreateBy == data.SuggestBy && i.Id != id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
                if (nextResponsibility != null && nextResponsibility.PerformBy.HasValue)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)nextResponsibility.PerformBy);
                    data.EmployeeRecive = new DocumentEmployeeBO
                    {
                        Id = nextResponsibility.PerformBy.Value,
                        Name = employee.Name,
                        AvatarUrl = employee.AvatarUrl,
                        RoleNames = employee.RoleNames,
                        NextOrder = nextResponsibility.Order.Value,
                        RoleType = nextResponsibility.RoleType.Value
                    };
                }
            }
            return data;
        }
        public DocumentWriteSuggestResponsibilityBO GetDetail(Guid id)
        {
            var result = new DocumentWriteSuggestResponsibilityBO();
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(id);
            var writesugest = GetById((Guid)responsibility.ExtendId);
            result.Id = responsibility.Id;
            result.DocumentId = responsibility.DocumentId;
            result.DepartmentId = responsibility.DepartmentId;
            result.ExtendId = responsibility.ExtendId;
            result.Order = responsibility.Order;
            result.ReasonOfSuggest = writesugest.ReasonOfSuggest;
            result.ApproveNote = responsibility.ApproveNote;
            result.IsNew = responsibility.IsNew;
            result.IsApprove = responsibility.IsApprove;
            result.IsAccept = responsibility.IsAccept;
            result.IsComplete = responsibility.IsComplete;
            result.IsLock = responsibility.IsLock;
            result.SuggestBy = writesugest.SuggestBy;
            result.SuggestAt = writesugest.SuggestAt;
            result.EmployeeSuggest = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)writesugest.SuggestBy);
            result.RoleType = responsibility.RoleType;
            result.StatusType = writesugest.StatusType;
            result.Name = writesugest.Name;
            result.EstimateAt = writesugest.EstimateAt;
            result.EmployeeAssignId = writesugest.EmployeeAssignId;
            result.EmployeeWriteId = writesugest.EmployeeWriteId;
            return result;
        }
        public DocumentWriteSuggestResponsibilityBO GetDetailByDocument(Guid documentId)
        {
            var result = new DocumentWriteSuggestResponsibilityBO();
            var writesugest = Get(i => i.DocumentId == documentId).FirstOrDefault();
            if (writesugest != null)
            {
                var responsibility = ServiceFactory<DocumentResponsibilityService>().GetByExtend(writesugest.Id)
                                            .Where(i => i.PerformBy == UserId)
                                            .OrderByDescending(i => i.CreateAt).FirstOrDefault();
                result.Id = responsibility.Id;
                result.DocumentId = responsibility.DocumentId;
                result.DepartmentId = responsibility.DepartmentId;
                result.ExtendId = responsibility.ExtendId;
                result.Order = responsibility.Order;
                result.ReasonOfSuggest = writesugest.ReasonOfSuggest;
                result.ApproveNote = responsibility.ApproveNote;
                result.IsNew = responsibility.IsNew;
                result.IsApprove = responsibility.IsApprove;
                result.IsAccept = responsibility.IsAccept;
                result.IsComplete = responsibility.IsComplete;
                result.IsLock = responsibility.IsLock;
                result.SuggestBy = writesugest.SuggestBy;
                result.SuggestAt = writesugest.SuggestAt;
                result.EmployeeSuggest = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)writesugest.SuggestBy);
                result.RoleType = responsibility.RoleType;
                result.StatusType = writesugest.StatusType;
                result.Name = writesugest.Name;
                result.EstimateAt = writesugest.EstimateAt;
                result.EmployeeAssignId = writesugest.EmployeeAssignId;
                result.EmployeeWriteId = writesugest.EmployeeWriteId;
            }
            return result;
        }
        // thêm mới đề nghị viết mới và lưu trữ thông tin trách nhiệm của người thực hiện
        public Guid InsertAndSaveRole(DocumentWriteSuggestResponsibilityBO data)
        {
            var documentId = ServiceFactory<DocumentService>().Insert(new DocumentBO() { Name = data.Name, IsSuggestWite = true });
            var writeSugest = new DocumentWriteSuggestBO()
            {
                DocumentId = documentId,
                Name = data.Name,
                EstimateAt = data.EstimateAt,
                SuggestBy = UserId,
                EmployeeAssignId = data.EmployeeAssignId,
                EmployeeWriteId = data.EmployeeWriteId,
                StatusType = (int)Resource.DocumentSuggestStatusType.New,
                Order = _minOrder,
                ReasonOfSuggest = data.ReasonOfSuggest,
            };
            var writeSugestId = base.Insert(writeSugest);
            var responsibility = new DocumentResponsibilityBO()
            {
                RoleType = (int)Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit,
                DepartmentId = data.DepartmentId,
                DocumentId = documentId,
                ExtendId = writeSugestId,
                PerformBy = UserId,
                IsNew = true,
                Order = _minOrder,
                SendTo = data.EmployeeRecive.Id,
                SenderRoleType = data.EmployeeRecive.RoleType,
                SenderOrder = data.EmployeeRecive.NextOrder
            };
            var responsibilityId = ServiceFactory<DocumentResponsibilityService>().Insert(responsibility);
            var nextResponsibility = new DocumentResponsibilityBO()
            {
                RoleType = data.EmployeeRecive.RoleType,
                DepartmentId = data.DepartmentId,
                DocumentId = documentId,
                ExtendId = writeSugestId,
                PerformBy = data.EmployeeRecive.Id,
                Order = data.EmployeeRecive.NextOrder
            };
            ServiceFactory<DocumentResponsibilityService>().Insert(nextResponsibility);
            return responsibilityId;
        }
        public void UpdateAndSaveRole(DocumentWriteSuggestResponsibilityBO data)
        {
            // Lưu thông tin đề nghị viết mới
            var writeSugest = base.GetById((Guid)data.ExtendId);
            writeSugest.Name = data.Name;
            writeSugest.EstimateAt = data.EstimateAt;
            writeSugest.EmployeeAssignId = data.EmployeeAssignId;
            writeSugest.EmployeeWriteId = data.EmployeeWriteId;
            writeSugest.SuggestBy = UserId;
            writeSugest.ReasonOfSuggest = data.ReasonOfSuggest;
            writeSugest.StatusType = (int)Resource.DocumentSuggestStatusType.New;
            base.Update(writeSugest);
            //-------------------------------------------------------
            // Lưu thông tin trách nhiệm liên quan
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(data.Id);
            responsibility.SendTo = data.EmployeeRecive.Id;
            responsibility.SenderRoleType = data.EmployeeRecive.RoleType;
            responsibility.SenderOrder = data.EmployeeRecive.NextOrder;
            ServiceFactory<DocumentResponsibilityService>().Update(responsibility);
            var nextResponsibility = ServiceFactory<DocumentResponsibilityService>().GetByExtend((Guid)data.ExtendId)
                                    .Where(i => i.CreateBy == responsibility.PerformBy && i.Id != data.Id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            nextResponsibility.RoleType = data.EmployeeRecive.RoleType;
            nextResponsibility.DepartmentId = data.DepartmentId;
            nextResponsibility.PerformBy = data.EmployeeRecive.Id;
            nextResponsibility.Order = data.EmployeeRecive.NextOrder;
            ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility);
            return;
        }
        public void Review(DocumentWriteSuggestResponsibilityBO data)
        {
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(data.Id);
            responsibility.IsApprove = true;
            responsibility.ApproveNote = data.ApproveNote;
            responsibility.IsAccept = data.IsAccept;
            ServiceFactory<DocumentResponsibilityService>().Update(responsibility);
            var lockItem = ServiceFactory<DocumentResponsibilityService>().Get(i => i.ExtendId == responsibility.ExtendId
                                                                    && i.Id != data.Id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            if (lockItem != null)
            {
                lockItem.IsLock = true;
                ServiceFactory<DocumentResponsibilityService>().Update(lockItem);
            }
            return;
        }
        public void SendAndSave(DocumentWriteSuggestResponsibilityBO data)
        {
            if (data.Id == null || data.Id == Guid.Empty)
            {
                // Lưu thông tin đề nghị
                var responsibilityId = InsertAndSaveRole(data);
                // Gửi thông tin đề nghị
                SendFromSuggest(responsibilityId);
                return;
            }
            else
            {
                // Cập nhật thông tin đề nghị
                UpdateAndSaveRole(data);
                // Gửi thông tin đề nghị
                SendFromSuggest(data.Id);
                return;
            }
        }
        public void DeleteByResponsibility(Guid responsibilityId, bool allowSave = true)
        {
            var responsibity = ServiceFactory<DocumentResponsibilityService>().GetById(responsibilityId);
            // Xóa tài liệu viết mới
            ServiceFactory<DocumentService>().Delete((Guid)responsibity.DocumentId);
            // Xóa đề nghị viết mới
            base.Delete((Guid)responsibity.ExtendId);
            // Xóa trách nhiệm liên quan
            ServiceFactory<DocumentResponsibilityService>().Delete(responsibilityId, false);
            SaveTransaction(true);
        }
        public void DestroyByResponsibility(Guid responsibilityId, bool allowSave = true)
        {
            var responsibity = ServiceFactory<DocumentResponsibilityService>().GetById(responsibilityId);
            responsibity.IsCancel = true;
            ServiceFactory<DocumentResponsibilityService>().Update(responsibity, false);
            var writeSuggest = base.GetById(responsibity.ExtendId.Value);
            writeSuggest.StatusType = (int)Resource.DocumentSuggestStatusType.Destroy;
            base.Update(writeSuggest, allowSave);
        }
        public bool SendFromSuggest(Guid responsibilityId)
        {
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(responsibilityId);
            responsibility.IsNew = true;
            responsibility.IsComplete = true;
            ServiceFactory<DocumentResponsibilityService>().Update(responsibility, false);
            var writeSugest = base.GetById((Guid)responsibility.ExtendId);
            var nextResponsibility = ServiceFactory<DocumentResponsibilityService>().GetByExtend((Guid)responsibility.ExtendId)
                                    .Where(i => i.CreateBy == writeSugest.SuggestBy && i.Id != responsibilityId).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            // Kiểm tra vai trò của người tiếp theo
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit) // Trường hợp người nhận trách nhiệm tiếp theo là người xem xét
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility, false);
                var writeSuggestStatus = (int)Resource.DocumentSuggestStatusType.Review;
                if (writeSugest.StatusType != (int)Resource.DocumentSuggestStatusType.New) return false;
                writeSugest.SuggestAt = DateTime.Now;
                writeSugest.StatusType = writeSuggestStatus;
                base.Update(writeSugest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit) // Trường hợp tiếp theo người nhận trách nhiệm là người phê duyệt
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility, false);
                var writeSuggestStatus = (int?)Resource.DocumentSuggestStatusType.Approve;
                if (writeSuggestStatus == null) return false;
                if (writeSugest.StatusType != (int)Resource.DocumentSuggestStatusType.New) return false;
                writeSugest.SuggestAt = DateTime.Now;
                writeSugest.StatusType = writeSuggestStatus;
                base.Update(writeSugest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit) // Trường hợp tiếp theo người nhận trách nhiệm là người phân công
            {
                // thêm mới phân công
                Assign(nextResponsibility.PerformBy, responsibility.ExtendId, nextResponsibility.DepartmentId, nextResponsibility.DocumentId, nextResponsibility.Order, Resource.RequestFrom.Suggestor);
                return true;
            }
            return false;
        }
        public bool CheckAndAutoSend(Guid responsibilityId)
        {
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(responsibilityId);
            var exitProcess = ServiceFactory<DocumentProcessService>().CheckExitProcess(responsibility.DepartmentId, (int)Resource.DocumentTypeProcess.WriteAndEditSuggest);
            // Kiểm tra tồn tại quy trình
            // Nếu quy trình tồn tại tiếp tục kiểm tra
            if (exitProcess)
            {
                var exitsNextProcessItem = ServiceFactory<DocumentProcessService>()
                    .Get(i => i.DepartmentId == responsibility.DepartmentId
                        && i.TypeProcess == (int)Resource.DocumentTypeProcess.WriteAndEditSuggest
                        && i.Order > responsibility.Order
                        ).Any();
                // Kiểm tra nếu tồn tại công đoạn tiếp theo
                // Nếu tồn tại thì vẫn chạy theo quy trình
                if (exitsNextProcessItem)
                {
                    return true;
                }
                else
                {
                    //var writeSugest = base.GetById((Guid)responsibility.ExtendId);
                    // writeSugest.StatusType
                    //var nextResponsibility = ServiceFactory<DocumentResponsibilityService>().GetByExtend((Guid)responsibility.ExtendId)
                    //                        .Where(i => i.Id != responsibilityId).OrderByDescending(i => i.CreateAt).FirstOrDefault();
                    //responsibility.IsNew = true;
                    //responsibility.IsComplete = true;
                    //ServiceFactory<DocumentResponsibilityService>().Update(responsibility, false);
                }
            }
            return true;
        }
        public bool SendFromReview(DocumentWriteSuggestResponsibilityBO data)
        {
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(data.Id);
            responsibility.IsNew = true;
            responsibility.IsComplete = true;

            ServiceFactory<DocumentResponsibilityService>().Update(responsibility, false);
            var writeSugest = base.GetById((Guid)responsibility.ExtendId);
            var exits = ServiceFactory<DocumentResponsibilityService>().Get(i => i.ExtendId == responsibility.ExtendId
                                                && i.Order == data.EmployeeRecive.NextOrder && i.RoleType == data.EmployeeRecive.RoleType).FirstOrDefault();
            var nextResponsibility = new DocumentResponsibilityBO();
            if (exits != null)
            {
                nextResponsibility = exits;
                nextResponsibility.PerformBy = data.EmployeeRecive.Id;
                nextResponsibility.ContentSuggest = data.ContentSuggest;
            }
            else
            {

                nextResponsibility = new DocumentResponsibilityBO()
                {
                    RoleType = data.EmployeeRecive.RoleType,
                    DepartmentId = data.DepartmentId,
                    DocumentId = responsibility.DocumentId,
                    ExtendId = responsibility.ExtendId,
                    PerformBy = data.EmployeeRecive.Id,
                    ContentSuggest = data.ContentSuggest,
                    Order = data.EmployeeRecive.NextOrder
                };
            }

            // Kiểm tra vai trò của người tiếp theo
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit) // Trường hợp người nhận trách nhiệm tiếp theo là người xem xét
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                if (nextResponsibility.Id == null || nextResponsibility.Id == Guid.Empty)
                {
                    ServiceFactory<DocumentResponsibilityService>().Insert(nextResponsibility, false);
                }
                else
                {
                    ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility, false);
                }
                writeSugest.SuggestAt = DateTime.Now;
                writeSugest.StatusType = (int)Resource.DocumentSuggestStatusType.Review;
                base.Update(writeSugest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit) // Trường hợp tiếp theo người nhận trách nhiệm là người phê duyệt
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                if (nextResponsibility.Id == null || nextResponsibility.Id == Guid.Empty)
                {
                    ServiceFactory<DocumentResponsibilityService>().Insert(nextResponsibility, false);
                }
                else
                {
                    ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility, false);
                }
                writeSugest.SuggestAt = DateTime.Now;
                writeSugest.StatusType = (int)Resource.DocumentSuggestStatusType.Approve;
                base.Update(writeSugest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit) // Trường hợp tiếp theo người nhận trách nhiệm là người phân công
            {
                // Phân công soạn thảo
                Assign(data.EmployeeRecive.Id, responsibility.ExtendId, data.DepartmentId, responsibility.DocumentId, data.EmployeeRecive.NextOrder, Resource.RequestFrom.Reviewer);
                writeSugest.SuggestAt = DateTime.Now;
                writeSugest.StatusType = (int)Resource.DocumentSuggestStatusType.Assigned;
                base.Update(writeSugest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.Write)
            // Trường hợp tiếp theo người nhận trách nhiệm là người soạn thảo
            {
                // Thêm mới soạn thảo ---
                Write(data.EmployeeRecive.Id, responsibility.ExtendId, data.DepartmentId, responsibility.DocumentId, data.EmployeeRecive.NextOrder, Resource.RequestFrom.Requestor, Resource.RequestTo.Compilator);
                writeSugest.StatusType = (int)Resource.DocumentSuggestStatusType.Assigned;
                base.Update(writeSugest, false);

            }
            return false;
        }
        public bool SendFromApprove(DocumentWriteSuggestResponsibilityBO data)
        {
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(data.Id);
            responsibility.IsNew = true;
            responsibility.IsComplete = true;
            //responsibility.IsLock = true;
            ServiceFactory<DocumentResponsibilityService>().Update(responsibility, false);
            if (data.EmployeeRecive.RoleType == (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit)//-- Người gửi tiếp theo là người phân công
            {
                //-- Thêm mới phân công 
                Assign(data.EmployeeRecive.Id, responsibility.ExtendId, data.DepartmentId, responsibility.DocumentId, data.EmployeeRecive.NextOrder, Resource.RequestFrom.Requestor);
                // Cập nhật trạng thái
                var writeSugest = base.GetById((Guid)responsibility.ExtendId);
                writeSugest.StatusType = (int)Resource.DocumentSuggestStatusType.Assigned;
                base.Update(writeSugest, false);
                //---------------------------------------------
                SaveTransaction();
            }
            if (data.EmployeeRecive.RoleType == (int)Resource.DocumentProcessRole.Write)//-- Người gửi tiếp theo là người phân công
            {
                // Thêm mới soạn thảo
                Write(data.EmployeeRecive.Id, responsibility.ExtendId, data.DepartmentId, responsibility.DocumentId, data.EmployeeRecive.NextOrder, Resource.RequestFrom.Requestor, Resource.RequestTo.Compilator);
                var writeSugest = base.GetById((Guid)responsibility.ExtendId);
                writeSugest.StatusType = (int)Resource.DocumentSuggestStatusType.Assigned;
                base.Update(writeSugest, false);
            }
            return true;
        }
        /// <summary>
        /// Thu hồi tài liệu. Có 2 trường hợp:
        /// TH1: Thu hồi về đề nghị
        /// TH2: Thu hồi về xem xét
        /// </summary>
        /// <param name="responsibilityId"></param>
        /// <returns></returns>
        public bool RevertDocument(Guid responsibilityId)
        {
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(responsibilityId);
            if (responsibility.IsLock == true) return false;
            int? writeSuggestStatus = null;
            var writeSugest = base.GetById((Guid)responsibility.ExtendId);
            if (writeSugest != null)
            {
                // Trường hợp 1: Thu hồi từ đề nghị đã giao phân công hoặc soạn thảo
                if (writeSugest.StatusType == (int)Resource.DocumentSuggestStatusType.AssignWrite)
                {
                    ServiceFactory<DocumentRequestService>().RevertAssignBySuggestId(writeSugest.Id);
                    RevertFromRequest(writeSugest.Id);
                    return true;
                }
                // Trường hợp 2: Thu hồi từ đề nghị chưa giao soạn thảo
                else
                {
                    switch (responsibility.RoleType)
                    {
                        case (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit:
                            // TH1:Trạng thái của đề nghị viết mới trở về trạng thái mới
                            writeSuggestStatus = (int)Resource.DocumentSuggestStatusType.New;
                            break;
                        case (int)Resource.DocumentProcessRole.ReviewWriteAndEdit:
                            //TH2:  Trạng thái của đề nghị xem xét trở về trạng thái xem xét
                            writeSuggestStatus = (int)Resource.DocumentSuggestStatusType.Review;
                            break;
                    }
                    if (writeSuggestStatus == null) return false;
                    //---Cập nhật trạng thái của bản ghi thu hồi về thực hiện
                    responsibility.IsNew = true;
                    responsibility.IsComplete = false;
                    //--- Cập nhật trạng thái của đề nghị viết mới

                    writeSugest.StatusType = writeSuggestStatus;
                    //--- Cập nhật trạng thái của người gửi
                    var nextResponsibility = ServiceFactory<DocumentResponsibilityService>().GetByExtend((Guid)responsibility.ExtendId)
                                            .Where(i => //i.CreateBy == writeSugest.SuggestBy && 
                                                i.Id != responsibilityId)
                                            .OrderByDescending(i => i.CreateAt).FirstOrDefault();
                    if (nextResponsibility.IsApprove == true) return false;
                    nextResponsibility.IsNew = false;
                    ServiceFactory<DocumentResponsibilityService>().Update(responsibility, false);
                    base.Update(writeSugest, false);
                    ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility, false);
                    SaveTransaction();
                    return true;
                }
            }
            else
                return false;
        }
        public bool RevertFromRequest(Guid suggestId)
        {
            var writeSugest = base.GetById(suggestId);
            var responsibilityRevert = ServiceFactory<DocumentResponsibilityService>().GetByExtend(suggestId)
                                            .Where(i => i.IsDelete == false && i.IsComplete == true)
                                            .Where(i => i.RoleType == (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit
                                                    || i.RoleType == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit
                                                    || i.RoleType == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit)
                                            .OrderByDescending(i => i.CreateAt).FirstOrDefault();
            if (responsibilityRevert != null)
            {
                responsibilityRevert.IsNew = true;
                responsibilityRevert.IsComplete = false;
                if (responsibilityRevert.RoleType == (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit)
                {
                    writeSugest.StatusType = (int)Resource.DocumentSuggestStatusType.New;
                }
                if (responsibilityRevert.RoleType == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit)
                {
                    writeSugest.StatusType = (int)Resource.DocumentSuggestStatusType.Review;
                }
                if (responsibilityRevert.RoleType == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit)
                {
                    writeSugest.StatusType = (int)Resource.DocumentSuggestStatusType.Approve;
                }
                base.Update(writeSugest, false);
                ServiceFactory<DocumentResponsibilityService>().Update(responsibilityRevert, false);
                SaveTransaction();
                return true;
            }
            return false;
        }
        public void Approve(DocumentWriteSuggestResponsibilityBO data)
        {
            //--- Thay đổi trạng thái duyệt cho bản ghi
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(data.Id);
            responsibility.IsApprove = true;
            responsibility.ApproveNote = data.ApproveNote;
            responsibility.IsAccept = data.IsAccept;
            ServiceFactory<DocumentResponsibilityService>().Update(responsibility, false);
            var writeSugest = base.GetById((Guid)responsibility.ExtendId);
            //writeSugest.SuggestAt = DateTime.Now;
            writeSugest.StatusType = responsibility.IsAccept == true ? (int)Resource.DocumentSuggestStatusType.ApproveAccept
                                    : (int)Resource.DocumentSuggestStatusType.ApproveFail;
            base.Update(writeSugest, false);
            //--- Thực hiện khóa những bản ghi để không tác động thu hồi
            var lockItem = ServiceFactory<DocumentResponsibilityService>().Get(i => i.ExtendId == responsibility.ExtendId
                                                                   && i.Id != data.Id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            if (lockItem != null)
            {
                lockItem.IsLock = true;
                ServiceFactory<DocumentResponsibilityService>().Update(lockItem, false);
            }
            SaveTransaction();
            return;
        }
        public bool ChangePerform(DocumentWriteSuggestResponsibilityBO data)
        {
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(data.Id);
            if (data.EmployeeChange == null) return false;
            responsibility.PerformBy = data.EmployeeChange.Id;
            ServiceFactory<DocumentResponsibilityService>().Update(responsibility);
            return true;
        }

        public bool AllowSuggestByCurrentUser()
        {
            var result = false;
            var departments = ServiceFactory<DepartmentService>().GetDepartmentsByCurrentUser()
                                    .Select(item => new DepartmentBO()
                                    {
                                        Id = item.Id,
                                        Name = item.Name,
                                    }).ToList();
            foreach (var department in departments)
            {
                // Kiểm tra xem phòng ban nào của người đăng nhập cho phép thêm mới đề nghị.
                if (AllowSuggestByEmployee(department.Id, UserId))
                {
                    return true;
                }
            }
            return result;
        }
        // Lấy danh sách phòng ban mà cho phép tạo mới danh mục tài liệu
        public List<DepartmentBO> GetDepartmentAllowSuggests()
        {
            var result = new List<DepartmentBO>();
            var departments = ServiceFactory<DepartmentService>().GetDepartmentsByCurrentUser()
                                    .Select(item => new DepartmentBO()
                                    {
                                        Id = item.Id,
                                        Name = item.Name,
                                    }).ToList();
            foreach (var department in departments)
            {
                // Kiểm tra xem phòng ban nào của người đăng nhập cho phép thêm mới đề nghị.
                if (AllowSuggestByEmployee(department.Id, UserId))
                {
                    result.Add(department);
                }
            }
            return result;
        }
        // cho phép đề nghị của nhân sự theo phòng ban
        public bool AllowSuggestByEmployee(Guid departmentId, Guid employeeId)
        {
            var result = false;
            //-- kiểm tra tồn tại quy trình
            if (!ServiceFactory<DocumentProcessService>().CheckExitProcess(departmentId, (int)Resource.DocumentTypeProcess.WriteAndEditSuggest))
                // Nếu không tồn tại cho phép thêm mới
                result = true;
            else // Trường hợp có quy trình
            {
                // Kiểm tra nhân sự với vai trò đề nghị
                var suggester = ServiceFactory<DocumentProcessService>().Get(i => i.DepartmentId == departmentId && i.TypeProcess == (int)Resource.DocumentTypeProcess.WriteAndEditSuggest && i.TypeRole == (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit);
                // Trường hợp không tồn tại thiết lập người đề nghị
                if (!suggester.Any())
                {
                    result = true;
                }
                else
                {
                    var employeeIds = suggester.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId).ToList();
                    if (employeeIds.Contains(employeeId)) result = true;
                    else
                    {
                        var departmentTitleAllowSuggests = suggester.Where(i => i.DepartmentTitleId.HasValue)
                                                                        .Select(i => i.DepartmentTitleId).ToList();
                        var departmentEmployeeTitleIds = ServiceFactory<DepartmentTitleService>()
                                                        .GetRoleIdsByDepartment(departmentId);
                        result = departmentTitleAllowSuggests.Any(i => departmentEmployeeTitleIds.Contains(i.Value));
                    }
                }
            }
            return result;
        }

        public void Assign(Guid? performBy, Guid? suggestId, Guid? departmentId, Guid? documentId, int? order, Resource.RequestFrom from)
        {
            //var assignItem = new DocumentRequestBO()
            //{
            //    ApproveBy = UserId,
            //    PerformBy = performBy,
            //    DocumentSuggestId = suggestId,
            //    TypeWrite = true,
            //    Check = false,
            //    IsSend = true,
            //    SendAt = DateTime.Now,
            //    StartAt = DateTime.Now,
            //    DepartmentId = departmentId,
            //    DocumentId = documentId,
            //};
            //var requesId = ServiceFactory<DocumentRequestService>().SaveRequestFromSuggestWriteNew(assignItem);
            var requesId = ServiceFactory<DocumentRequestService>().CreateFromSuggest(suggestId.Value, from, Resource.RequestSource.SuggestWrite, documentId.Value,
                        DateTime.Now, DateTime.Now, DateTime.Now, "", performBy.Value, departmentId, true, order, UserId, true);
            //---------------------------------------------
            //-- Thêm mới vai trò phân công cho tài liệu đó
            if (requesId != null || requesId != Guid.Empty)
            {

                var nextResponsibility = ServiceFactory<DocumentResponsibilityService>()
                    .Get(i => i.ExtendId == suggestId && i.RoleType == (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit).FirstOrDefault();
                if (nextResponsibility != null)
                {
                    nextResponsibility.Order = order;
                    nextResponsibility.PerformBy = performBy;
                    nextResponsibility.IsNew = true;
                    ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility);
                }
                else
                {
                    ServiceFactory<DocumentResponsibilityService>().Insert(new DocumentResponsibilityBO()
                    {
                        RoleType = (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit,
                        DepartmentId = departmentId,
                        DocumentId = documentId,
                        ExtendId = requesId,
                        PerformBy = performBy,
                        Order = order,
                        IsNew = true,
                    });
                }
            }
        }
        public void Write(Guid? performBy, Guid? suggestId, Guid? departmentId, Guid? documentId, int? order, Resource.RequestFrom from, Resource.RequestTo to)
        {
            var requesId = ServiceFactory<DocumentRequestService>().CreateFromSuggest(suggestId.Value, from, to, Resource.RequestSource.SuggestWrite, documentId.Value,
                        DateTime.Now, DateTime.Now, DateTime.Now, "", performBy.Value, departmentId, true, order, UserId, true);
            //---------------------------------------------
            //-- Thêm mới vai trò soạn thảo cho tài liệu đó
            if (requesId != null || requesId != Guid.Empty)
            {

                var nextResponsibility = ServiceFactory<DocumentResponsibilityService>()
                    .Get(i => i.ExtendId == suggestId && i.RoleType == (int)Resource.DocumentProcessRole.Write).FirstOrDefault();
                if (nextResponsibility != null)
                {
                    nextResponsibility.Order = order;
                    nextResponsibility.PerformBy = performBy;
                    nextResponsibility.IsNew = true;
                    ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility);
                }
                else
                {
                    ServiceFactory<DocumentResponsibilityService>().Insert(new DocumentResponsibilityBO()
                    {
                        RoleType = (int)Resource.DocumentProcessRole.Write,
                        DepartmentId = departmentId,
                        DocumentId = documentId,
                        ExtendId = requesId,
                        PerformBy = performBy,
                        Order = order,
                        IsNew = true,
                    });
                }
            }
        }

    }
}
