﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentAdjustSuggestService : BaseService<DocumentAdjustSuggestDTO, DocumentAdjustSuggestBO>, IDocumentAdjustSuggestService
    {
        const int _minOrder = 0;
        public IEnumerable<DocumentAdjustSuggestResponsibilityBO> GetByCurrentUser(int pageIndex, int pageSize, out int totalCount, Resource.DocumentProcessRoleFilterSuggest roleType, Resource.DocumentSuggestStatusType statusType)
        {
            var adjustSuggest = base.Get(i => (statusType == Resource.DocumentSuggestStatusType.All || i.StatusType == (int)statusType));
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetByCurrentUser()
                                    .Where(i => (roleType == Resource.DocumentProcessRoleFilterSuggest.All
                                                || i.RoleType == (int)roleType) && i.IsNew != false && i.IsDelete == false
                                                && i.ExtendId.HasValue
                                                && adjustSuggest.Select(w => w.Id).Contains(i.ExtendId.Value));

            var data = responsibility.Join(adjustSuggest, r => r.ExtendId, aj => aj.Id,
                                        (r, aj) => new DocumentAdjustSuggestResponsibilityBO()
                                        {
                                            Id = r.Id,
                                            DocumentId = aj.DocumentId,
                                            SuggestAt = aj.SuggestAt,
                                            EstimateAt = aj.EstimateAt,
                                            RoleType = r.RoleType,
                                            StatusType = aj.StatusType,
                                            IsComplete = r.IsComplete,
                                            IsAccept = r.IsAccept,
                                            IsApprove = r.IsApprove,
                                            IsCancel = r.IsCancel,
                                            IsLock = r.IsLock,
                                            CreateAt = r.CreateAt
                                        }
                                      ).OrderByDescending(i => i.CreateAt);
            totalCount = data.Count();
            var result = data.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            var documentIds = result.Select(i => i.DocumentId.Value);
            var documents = ServiceFactory<DocumentService>().GetByIds(documentIds);
            result = result.Select(i =>
                {
                    var document = documents.FirstOrDefault(u => u.Id == i.DocumentId.Value);
                    i.Name = document.Name;
                    return i;
                });
            return result;
        }
        public IEnumerable<DocumentResponsibilityBO> GetEmployeeRoleBySuggest(Guid suggestId)
        {
            var resposibily = ServiceFactory<DocumentResponsibilityService>().GetByExtend(suggestId)
                                     .Where(i => (i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.SuggesterWriteAndEdit
                                                     || i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.ApprovalWriteAndEdit
                                                     || i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.ReviewWriteAndEdit)
                                             && i.IsNew != false)
                                         .Select(item => new DocumentResponsibilityBO
                                         {
                                             Id = item.Id,
                                             RoleType = item.RoleType,
                                             IsApprove = item.IsApprove,
                                             IsAccept = item.IsAccept,
                                             EmployeeSend = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)item.PerformBy),
                                             CreateAt = item.CreateAt,
                                         }
                                     ).OrderBy(i => i.CreateAt);
            return resposibily;
        }

        public Guid InsertAdjustSuggest(DocumentAdjustSuggestResponsibilityBO data)
        {
            var adjustSuggest = new DocumentAdjustSuggestBO()
            {
                DocumentId = data.Document.Id,
                EstimateAt = data.EstimateAt,
                SuggestBy = UserId,
                EmployeeAssignId = data.EmployeeAssignId,
                EmployeeWriteId = data.EmployeeWriteId,
                StatusType = (int)Resource.DocumentSuggestStatusType.New,
                Order = _minOrder,
                ReasonOfSuggest = data.ReasonOfSuggest,
            };
            var adjustSuggestId = base.Insert(adjustSuggest);
            var responsibility = new DocumentResponsibilityBO()
            {
                RoleType = (int)Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit,
                DepartmentId = data.DepartmentId,
                DocumentId = data.Document.Id,
                ExtendId = adjustSuggestId,
                PerformBy = UserId,
                IsNew = true,
                Order = _minOrder
            };
            var responsibilityId = ServiceFactory<DocumentResponsibilityService>().Insert(responsibility);
            var nextResponsibility = new DocumentResponsibilityBO()
            {
                RoleType = data.EmployeeRecive.RoleType,
                DepartmentId = data.DepartmentId,
                DocumentId = data.Document.Id,
                ExtendId = adjustSuggestId,
                PerformBy = data.EmployeeRecive.Id,
                Order = data.EmployeeRecive.NextOrder
            };
            ServiceFactory<DocumentResponsibilityService>().Insert(nextResponsibility);
            return responsibilityId;
        }

        public void UpdateAdjustSuggest(DocumentAdjustSuggestResponsibilityBO data)
        {
            // Lưu thông tin đề nghị viết mới
            var adjustSuggest = base.GetById((Guid)data.ExtendId);
            adjustSuggest.EstimateAt = data.EstimateAt;
            adjustSuggest.EmployeeAssignId = data.EmployeeAssignId;
            adjustSuggest.EmployeeWriteId = data.EmployeeWriteId;
            adjustSuggest.SuggestBy = UserId;
            adjustSuggest.ReasonOfSuggest = data.ReasonOfSuggest;
            adjustSuggest.StatusType = (int)Resource.DocumentSuggestStatusType.New;
            base.Update(adjustSuggest);
            //-------------------------------------------------------
            // Lưu thông tin trách nhiệm liên quan
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(data.Id);
            ServiceFactory<DocumentResponsibilityService>().Update(responsibility);
            var nextResponsibility = ServiceFactory<DocumentResponsibilityService>().GetByExtend((Guid)data.ExtendId)
                                    .Where(i => i.CreateBy == responsibility.PerformBy && i.Id != data.Id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            nextResponsibility.RoleType = data.EmployeeRecive.RoleType;
            nextResponsibility.DepartmentId = data.DepartmentId;
            nextResponsibility.PerformBy = data.EmployeeRecive.Id;
            nextResponsibility.Order = data.EmployeeRecive.NextOrder;
            ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility);
            return;
        }

        public DocumentAdjustSuggestResponsibilityBO GetDetail(Guid id)
        {
            var result = new DocumentAdjustSuggestResponsibilityBO();
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(id);
            var adjustSuggest = GetById((Guid)responsibility.ExtendId);
            result.Id = responsibility.Id;
            result.DocumentId = responsibility.DocumentId;
            result.Name = ServiceFactory<DocumentService>().GetById(adjustSuggest.DocumentId.Value).Name;
            result.DepartmentId = responsibility.DepartmentId;
            result.ExtendId = responsibility.ExtendId;
            result.Order = responsibility.Order;
            result.ReasonOfSuggest = adjustSuggest.ReasonOfSuggest;
            result.ApproveNote = responsibility.ApproveNote;
            result.IsNew = responsibility.IsNew;
            result.IsApprove = responsibility.IsApprove;
            result.IsAccept = responsibility.IsAccept;
            result.IsComplete = responsibility.IsComplete;
            result.IsLock = responsibility.IsLock;
            result.SuggestBy = adjustSuggest.SuggestBy;
            result.SuggestAt = adjustSuggest.SuggestAt;
            result.EmployeeSuggest = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)adjustSuggest.SuggestBy);
            result.RoleType = responsibility.RoleType;
            result.StatusType = adjustSuggest.StatusType;
            result.EstimateAt = adjustSuggest.EstimateAt;
            result.EmployeeAssignId = adjustSuggest.EmployeeAssignId;
            result.EmployeeWriteId = adjustSuggest.EmployeeWriteId;
            return result;
        }
        public DocumentAdjustSuggestResponsibilityBO GetDetailByDocument(Guid documentId)
        {
            var result = new DocumentAdjustSuggestResponsibilityBO();
            var adjustSuggest = Get(i => i.DocumentId == documentId).FirstOrDefault();
            if (adjustSuggest != null)
            {
                /// Chỗ này chưa check null
                var responsibility = ServiceFactory<DocumentResponsibilityService>().GetByExtend(adjustSuggest.Id)
                                            .Where(i => i.PerformBy == UserId)
                                            .OrderByDescending(i => i.CreateAt).FirstOrDefault();
                if (responsibility != null)
                {
                    result.Id = responsibility.Id;
                    result.DocumentId = responsibility.DocumentId;
                    result.DepartmentId = responsibility.DepartmentId;
                    result.ExtendId = responsibility.ExtendId;
                    result.Order = responsibility.Order;
                    result.ReasonOfSuggest = adjustSuggest.ReasonOfSuggest;
                    result.ApproveNote = responsibility.ApproveNote;
                    result.IsNew = responsibility.IsNew;
                    result.IsApprove = responsibility.IsApprove;
                    result.IsAccept = responsibility.IsAccept;
                    result.IsComplete = responsibility.IsComplete;
                    result.IsLock = responsibility.IsLock;
                    result.SuggestBy = adjustSuggest.SuggestBy;
                    result.SuggestAt = adjustSuggest.SuggestAt;
                    result.EmployeeSuggest = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)adjustSuggest.SuggestBy);
                    result.RoleType = responsibility.RoleType;
                    result.StatusType = adjustSuggest.StatusType;
                    result.Name = adjustSuggest.Name;
                    result.EstimateAt = adjustSuggest.EstimateAt;
                    result.EmployeeAssignId = adjustSuggest.EmployeeAssignId;
                    result.EmployeeWriteId = adjustSuggest.EmployeeWriteId;
                }
            }
            return result;
        }
        public DocumentAdjustSuggestResponsibilityBO GetDetailForSugest(Guid id)
        {
            var data = GetDetail(id);
            var nextResponsibility = ServiceFactory<DocumentResponsibilityService>()
                .GetByExtend((Guid)data.ExtendId)
                .Where(i => i.CreateBy == data.SuggestBy && i.Id != id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            if (nextResponsibility != null)
            {
                var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)nextResponsibility.PerformBy);
                data.EmployeeRecive = new DocumentEmployeeBO
                {
                    Id = nextResponsibility.PerformBy.Value,
                    Name = employee.Name,
                    AvatarUrl = employee.AvatarUrl,
                    RoleNames = employee.RoleNames,
                    NextOrder = nextResponsibility.Order.Value,
                    RoleType = nextResponsibility.RoleType.Value
                };
            }
            return data;
        }

        public DocumentAdjustSuggestResponsibilityBO GetDetailForDocument(Guid id)
        {
            var data = new DocumentAdjustSuggestResponsibilityBO();
            /// Lấy thông tin của tài liệu
            var document = ServiceFactory<DocumentService>().GetById(id);

            /// Lấy thông tin của đề nghị 
            /// Kiểm tra thông tin của 
            var suggest = Get(i => i.DocumentId == id).FirstOrDefault();
            if (suggest != null)
            {
                //var suggestId = suggest.Id;
                data = GetDetailByDocument(id);
                var nextResponsibility = ServiceFactory<DocumentResponsibilityService>()
                    .GetByExtend((Guid)data.ExtendId)
                    .Where(i => i.CreateBy == data.SuggestBy && i.Id != id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
                if (nextResponsibility != null && nextResponsibility.PerformBy.HasValue)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)nextResponsibility.PerformBy);
                    data.EmployeeRecive = new DocumentEmployeeBO
                    {
                        Id = nextResponsibility.PerformBy.Value,
                        Name = employee.Name,
                        AvatarUrl = employee.AvatarUrl,
                        RoleNames = employee.RoleNames,
                        NextOrder = nextResponsibility.Order.Value,
                        RoleType = nextResponsibility.RoleType.Value
                    };
                }
            }
            else
            {
                if (document != null && document.DepartmentId.HasValue)
                {
                    var department = ServiceFactory<DepartmentService>().GetById(document.DepartmentId.Value);
                    document.DepartmentName = department == null ? string.Empty : department.Name;
                }
                data = new DocumentAdjustSuggestResponsibilityBO()
                {
                    Document = document,
                    DepartmentId = document.DepartmentId,
                    DepartmentName = document.DepartmentName
                };
            }
            data.Document = document;

            return data;
        }
        public bool SendFromSuggest(Guid responsibilityId)
        {
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(responsibilityId);
            responsibility.IsNew = true;
            responsibility.IsComplete = true;
            ServiceFactory<DocumentResponsibilityService>().Update(responsibility, false);
            var adjustSuggest = base.GetById((Guid)responsibility.ExtendId);
            var nextResponsibility = ServiceFactory<DocumentResponsibilityService>().GetByExtend((Guid)responsibility.ExtendId)
                                    .Where(i => i.CreateBy == adjustSuggest.SuggestBy && i.Id != responsibilityId).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            // Kiểm tra vai trò của người tiếp theo
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit) // Trường hợp người nhận trách nhiệm tiếp theo là người xem xét
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility, false);
                var writeSuggestStatus = (int)Resource.DocumentSuggestStatusType.Review;
                if (adjustSuggest.StatusType != (int)Resource.DocumentSuggestStatusType.New) return false;
                adjustSuggest.SuggestAt = DateTime.Now;
                adjustSuggest.StatusType = writeSuggestStatus;
                base.Update(adjustSuggest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit) // Trường hợp tiếp theo người nhận trách nhiệm là người phê duyệt
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility, false);
                var writeSuggestStatus = (int?)Resource.DocumentSuggestStatusType.Approve;
                if (writeSuggestStatus == null) return false;
                if (adjustSuggest.StatusType != (int)Resource.DocumentSuggestStatusType.New) return false;
                adjustSuggest.SuggestAt = DateTime.Now;
                adjustSuggest.StatusType = writeSuggestStatus;
                base.Update(adjustSuggest, false);
                SaveTransaction();
                return true;
            }
            return false;
        }

        public void SendAndSave(DocumentAdjustSuggestResponsibilityBO data)
        {
            if (data.Id == null || data.Id == Guid.Empty)
            {
                var responsibilityId = InsertAdjustSuggest(data);
                SendFromSuggest(responsibilityId);
                return;
            }
            else
            {
                UpdateAdjustSuggest(data);
                SendFromSuggest(data.Id);
                return;
            }
        }

        public void Review(DocumentAdjustSuggestResponsibilityBO data)
        {
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(data.Id);
            responsibility.IsApprove = true;
            responsibility.ApproveNote = data.ApproveNote;
            responsibility.IsAccept = data.IsAccept;
            ServiceFactory<DocumentResponsibilityService>().Update(responsibility);
            var lockItem = ServiceFactory<DocumentResponsibilityService>().Get(i => i.ExtendId == responsibility.ExtendId
                                                                    && i.Id != data.Id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            if (lockItem != null)
            {
                lockItem.IsLock = true;
                ServiceFactory<DocumentResponsibilityService>().Update(lockItem);
            }
            return;
        }

        public bool SendFromReview(DocumentAdjustSuggestResponsibilityBO data)
        {
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(data.Id);
            responsibility.IsNew = true;
            responsibility.IsComplete = true;

            ServiceFactory<DocumentResponsibilityService>().Update(responsibility, false);
            var adjustSuggest = base.GetById((Guid)responsibility.ExtendId);
            var exits = ServiceFactory<DocumentResponsibilityService>().Get(i => i.ExtendId == responsibility.ExtendId
                                                && i.Order == data.EmployeeRecive.NextOrder && i.RoleType == data.EmployeeRecive.RoleType).FirstOrDefault();
            var nextResponsibility = new DocumentResponsibilityBO();
            if (exits != null)
            {
                nextResponsibility = exits;
                nextResponsibility.PerformBy = data.EmployeeRecive.Id;
                nextResponsibility.ContentSuggest = data.ContentSuggest;
            }
            else
            {

                nextResponsibility = new DocumentResponsibilityBO()
                {
                    RoleType = data.EmployeeRecive.RoleType,
                    DepartmentId = data.DepartmentId,
                    DocumentId = responsibility.DocumentId,
                    ExtendId = responsibility.ExtendId,
                    PerformBy = data.EmployeeRecive.Id,
                    ContentSuggest = data.ContentSuggest,
                    Order = data.EmployeeRecive.NextOrder
                };
            }

            // Kiểm tra vai trò của người tiếp theo
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit) // Trường hợp người nhận trách nhiệm tiếp theo là người xem xét
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                if (nextResponsibility.Id == null || nextResponsibility.Id == Guid.Empty)
                {
                    ServiceFactory<DocumentResponsibilityService>().Insert(nextResponsibility, false);
                }
                else
                {
                    ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility, false);
                }
                adjustSuggest.SuggestAt = DateTime.Now;
                adjustSuggest.StatusType = (int)Resource.DocumentSuggestStatusType.Review;
                base.Update(adjustSuggest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit) // Trường hợp tiếp theo người nhận trách nhiệm là người phê duyệt
            {
                // Thay đổi trạng thái thực hiện của người vai trò tiếp theo và cập nhật trạng thái cho đề nghị
                nextResponsibility.IsNew = true;
                if (nextResponsibility.Id == null || nextResponsibility.Id == Guid.Empty)
                {
                    ServiceFactory<DocumentResponsibilityService>().Insert(nextResponsibility, false);
                }
                else
                {
                    ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility, false);
                }
                adjustSuggest.SuggestAt = DateTime.Now;
                adjustSuggest.StatusType = (int)Resource.DocumentSuggestStatusType.Approve;
                base.Update(adjustSuggest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit) // Trường hợp tiếp theo người nhận trách nhiệm là người phân công
            {
                // Phân công soạn thảo
                Assign(data.EmployeeRecive.Id, responsibility.ExtendId, data.DepartmentId, responsibility.DocumentId, data.EmployeeRecive.NextOrder, Resource.RequestFrom.Reviewer);
                adjustSuggest.SuggestAt = DateTime.Now;
                adjustSuggest.StatusType = (int)Resource.DocumentSuggestStatusType.Assigned;
                base.Update(adjustSuggest, false);
                SaveTransaction();
                return true;
            }
            if (nextResponsibility.RoleType == (int)Resource.DocumentProcessRole.Write)
            // Trường hợp tiếp theo người nhận trách nhiệm là người soạn thảo
            {
                // Thêm mới soạn thảo ---
                Write(data.EmployeeRecive.Id, responsibility.ExtendId, data.DepartmentId, responsibility.DocumentId, data.EmployeeRecive.NextOrder, Resource.RequestFrom.Requestor, Resource.RequestTo.Compilator);
                adjustSuggest.StatusType = (int)Resource.DocumentSuggestStatusType.Assigned;
                base.Update(adjustSuggest, false);

            }
            return false;
        }

        public void Approve(DocumentAdjustSuggestResponsibilityBO data)
        {
            //--- Thay đổi trạng thái duyệt cho bản ghi
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(data.Id);
            responsibility.IsApprove = true;
            responsibility.ApproveNote = data.ApproveNote;
            responsibility.IsAccept = data.IsAccept;
            ServiceFactory<DocumentResponsibilityService>().Update(responsibility, false);
            var writeSugest = base.GetById((Guid)responsibility.ExtendId);
            writeSugest.SuggestAt = DateTime.Now;
            writeSugest.StatusType = responsibility.IsAccept == true ? (int)Resource.DocumentSuggestStatusType.ApproveAccept
                                    : (int)Resource.DocumentSuggestStatusType.ApproveFail;
            base.Update(writeSugest, false);
            //--- Thực hiện khóa những bản ghi để không tác động thu hồi
            var lockItem = ServiceFactory<DocumentResponsibilityService>().Get(i => i.ExtendId == responsibility.ExtendId && i.Id != data.Id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            if (lockItem != null)
            {
                lockItem.IsLock = true;
                ServiceFactory<DocumentResponsibilityService>().Update(lockItem, false);
            }
            SaveTransaction();
            return;
        }

        public bool SendFromApprove(DocumentAdjustSuggestResponsibilityBO data)
        {
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(data.Id);
            responsibility.IsNew = true;
            responsibility.IsComplete = true;
            //responsibility.IsLock = true;
            ServiceFactory<DocumentResponsibilityService>().Update(responsibility, false);
            if (data.EmployeeRecive.RoleType == (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit)//-- Người gửi tiếp theo là người phân công
            {
                //-- Thêm mới phân công 
                Assign(data.EmployeeRecive.Id, responsibility.ExtendId, data.DepartmentId, responsibility.DocumentId, data.EmployeeRecive.NextOrder, Resource.RequestFrom.Requestor);
                // Cập nhật trạng thái
                var adjustSuggest = base.GetById((Guid)responsibility.ExtendId);
                adjustSuggest.StatusType = (int)Resource.DocumentSuggestStatusType.Assigned;
                base.Update(adjustSuggest, false);
                //---------------------------------------------
                SaveTransaction();
            }
            if (data.EmployeeRecive.RoleType == (int)Resource.DocumentProcessRole.Write)//-- Người gửi tiếp theo là người phân công
            {
                // Thêm mới soạn thảo
                Write(data.EmployeeRecive.Id, responsibility.ExtendId, data.DepartmentId, responsibility.DocumentId, data.EmployeeRecive.NextOrder, Resource.RequestFrom.Requestor, Resource.RequestTo.Compilator);
                var adjustSuggest = base.GetById((Guid)responsibility.ExtendId);
                adjustSuggest.StatusType = (int)Resource.DocumentSuggestStatusType.Assigned;
                base.Update(adjustSuggest, false);
            }
            SaveTransaction();
            return true;
        }

        public bool RevertDocument(Guid responsibilityId)
        {
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(responsibilityId);
            if (responsibility.IsLock == true) return false;
            int? adjustSuggestStatus = null;
            var adjustSuggest = base.GetById((Guid)responsibility.ExtendId);
            if (adjustSuggest != null)
            {
                // Trường hợp 1: Thu hồi từ đề nghị đã giao phân công hoặc soạn thảo
                if (adjustSuggest.StatusType == (int)Resource.DocumentSuggestStatusType.AssignWrite)
                {
                    ServiceFactory<DocumentRequestService>().RevertAssignBySuggestId(adjustSuggest.Id);
                    RevertFromRequest(adjustSuggest.Id);
                    return true;
                }
                // Trường hợp 2: Thu hồi từ đề nghị chưa giao soạn thảo
                else
                {
                    // Trường hợp 1: Thu hồi từ xem xét đề nghị về đề nghị
                    switch (responsibility.RoleType)
                    {
                        case (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit:
                            // TH1:Trạng thái của đề nghị viết mới trở về trạng thái mới
                            adjustSuggestStatus = (int)Resource.DocumentSuggestStatusType.New;
                            break;
                        case (int)Resource.DocumentProcessRole.ReviewWriteAndEdit:
                            //TH2:  Trạng thái của đề nghị xem xét trở về trạng thái xem xét
                            adjustSuggestStatus = (int)Resource.DocumentSuggestStatusType.Review;
                            break;
                    }
                    if (adjustSuggestStatus == null) return false;
                    //---Cập nhật trạng thái của bản ghi thu hồi về thực hiện
                    responsibility.IsNew = true;
                    responsibility.IsComplete = false;
                    //--- Cập nhật trạng thái của đề nghị sửa đổi

                    adjustSuggest.StatusType = adjustSuggestStatus;
                    //--- Cập nhật trạng thái của người gửi
                    var nextResponsibility = ServiceFactory<DocumentResponsibilityService>().GetByExtend((Guid)responsibility.ExtendId)
                                            .Where(i =>// i.CreateBy == adjustSuggest.SuggestBy && 
                                                i.Id != responsibilityId).OrderByDescending(i => i.CreateAt).FirstOrDefault();
                    if (nextResponsibility.IsApprove == true) return false;
                    nextResponsibility.IsNew = false;
                    ServiceFactory<DocumentResponsibilityService>().Update(responsibility, false);
                    base.Update(adjustSuggest, false);
                    ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility, false);
                    SaveTransaction();
                    return true;
                }
            }
            else return false;
        }
        public bool RevertFromRequest(Guid suggestId)
        {
            var adjustSugest = base.GetById(suggestId);
            var responsibilityRevert = ServiceFactory<DocumentResponsibilityService>().GetByExtend(suggestId)
                                            .Where(i => i.IsDelete == false && i.IsComplete == true)
                                            .Where(i => i.RoleType == (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit
                                                    || i.RoleType == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit
                                                    || i.RoleType == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit)
                                            .OrderByDescending(i => i.CreateAt).FirstOrDefault();
            if (responsibilityRevert != null)
            {
                responsibilityRevert.IsNew = true;
                responsibilityRevert.IsComplete = false;
                if (responsibilityRevert.RoleType == (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit)
                {
                    adjustSugest.StatusType = (int)Resource.DocumentSuggestStatusType.New;
                }
                if (responsibilityRevert.RoleType == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit)
                {
                    adjustSugest.StatusType = (int)Resource.DocumentSuggestStatusType.Review;
                }
                if (responsibilityRevert.RoleType == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit)
                {
                    adjustSugest.StatusType = (int)Resource.DocumentSuggestStatusType.Approve;
                }
                base.Update(adjustSugest, false);
                ServiceFactory<DocumentResponsibilityService>().Update(responsibilityRevert, false);
                SaveTransaction();
                return true;
            }
            return false;
        }
        public bool DestroyDocument(Guid id)
        {
            throw new NotImplementedException();
        }

        public void DeleteByReposibility(Guid responsibityId, bool allowSave)
        {
            var responsibity = ServiceFactory<DocumentResponsibilityService>().GetById(responsibityId);
            // Xóa tài liệu viết mới
            ServiceFactory<DocumentService>().Delete((Guid)responsibity.DocumentId);
            // Xóa đề nghị viết mới
            base.Delete((Guid)responsibity.ExtendId);
            // Xóa trách nhiệm liên quan
            ServiceFactory<DocumentResponsibilityService>().Delete(responsibityId, false);
            SaveTransaction(true);
        }

        public bool ChangePerform(DocumentAdjustSuggestResponsibilityBO data)
        {
            var responsibility = ServiceFactory<DocumentResponsibilityService>().GetById(data.Id);
            if (data.EmployeeChange == null) return false;
            responsibility.PerformBy = data.EmployeeChange.Id;
            ServiceFactory<DocumentResponsibilityService>().Update(responsibility);
            return true;
        }

        /// <summary>
        /// TuNT Hàm tạo item mặc định khi tạo đề nghị sửa đổi
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public DocumentAdjustSuggestResponsibilityBO GetAjustFormItem(Guid documentId)
        {
            try
            {
                // Check quyền xem người đó có được quyền đề nghị sửa đổi tài liệu này không
                // Check xem người đề nghị sửa đổi tài liệu của người này đã kết thúc chưa
                //bool checkProcess = ServiceFactory<DocumentProcessService>().CheckRoleTypeExits(Resource.DocumentProcessRole.SuggesterWriteAndEdit);
                //if (!checkProcess)
                //    throw new AccessDenyException();
                //bool checkFinishSuggestAdjust = true;
                //if (!checkFinishSuggestAdjust)
                //    throw new SuggestAdjustIsNotFinishException();
                var document = ServiceFactory<DocumentService>().GetById(documentId);
                if (document != null && document.DepartmentId.HasValue)
                {
                    var department = ServiceFactory<DepartmentService>().GetById(document.DepartmentId.Value);
                    document.DepartmentName = department == null ? string.Empty : department.Name;
                }
                var data = new DocumentAdjustSuggestResponsibilityBO()
                {
                    Document = document,
                    DepartmentId = document.DepartmentId,
                    DepartmentName = document.DepartmentName
                };
                return data;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Assign(Guid? performBy, Guid? suggestId, Guid? departmentId, Guid? documentId, int? order, Resource.RequestFrom from)
        {
            //var assignItem = new DocumentRequestBO()
            //{
            //    ApproveBy = UserId,
            //    PerformBy = performBy,
            //    DocumentSuggestId = suggestId,
            //    TypeWrite = true,
            //    Check = false,
            //    IsSend = true,
            //    SendAt = DateTime.Now,
            //    StartAt = DateTime.Now,
            //    DepartmentId = departmentId,
            //    DocumentId = documentId,
            //};
            //var requesId = ServiceFactory<DocumentRequestService>().SaveRequestFromSuggestWriteNew(assignItem);
            var requesId = ServiceFactory<DocumentRequestService>().CreateFromSuggest(suggestId.Value, from, Resource.RequestSource.SuggestAdjust, documentId.Value,
                        DateTime.Now, DateTime.Now, DateTime.Now, "", performBy.Value, departmentId, true, order, UserId, true);
            //---------------------------------------------
            //-- Thêm mới vai trò phân công cho tài liệu đó
            if (requesId != null || requesId != Guid.Empty)
            {

                var nextResponsibility = ServiceFactory<DocumentResponsibilityService>()
                    .Get(i => i.ExtendId == suggestId && i.RoleType == (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit).FirstOrDefault();
                if (nextResponsibility != null)
                {
                    nextResponsibility.Order = order;
                    nextResponsibility.PerformBy = performBy;
                    nextResponsibility.IsNew = true;
                    ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility);
                }
                else
                {
                    ServiceFactory<DocumentResponsibilityService>().Insert(new DocumentResponsibilityBO()
                    {
                        RoleType = (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit,
                        DepartmentId = departmentId,
                        DocumentId = documentId,
                        ExtendId = requesId,
                        PerformBy = performBy,
                        Order = order,
                        IsNew = true,
                    });
                }
            }
        }
        public void Write(Guid? performBy, Guid? suggestId, Guid? departmentId, Guid? documentId, int? order, Resource.RequestFrom from, Resource.RequestTo to)
        {
            var requesId = ServiceFactory<DocumentRequestService>().CreateFromSuggest(suggestId.Value, from, to, Resource.RequestSource.SuggestAdjust, documentId.Value,
                        DateTime.Now, DateTime.Now, DateTime.Now, "", performBy.Value, departmentId, true, order, UserId, true);
            //---------------------------------------------
            //-- Thêm mới vai trò soạn thảo cho tài liệu đó
            if (requesId != null || requesId != Guid.Empty)
            {

                var nextResponsibility = ServiceFactory<DocumentResponsibilityService>()
                    .Get(i => i.ExtendId == suggestId && i.RoleType == (int)Resource.DocumentProcessRole.Write).FirstOrDefault();
                if (nextResponsibility != null)
                {
                    nextResponsibility.Order = order;
                    nextResponsibility.PerformBy = performBy;
                    nextResponsibility.IsNew = true;
                    ServiceFactory<DocumentResponsibilityService>().Update(nextResponsibility);
                }
                else
                {
                    ServiceFactory<DocumentResponsibilityService>().Insert(new DocumentResponsibilityBO()
                    {
                        RoleType = (int)Resource.DocumentProcessRole.Write,
                        DepartmentId = departmentId,
                        DocumentId = documentId,
                        ExtendId = requesId,
                        PerformBy = performBy,
                        Order = order,
                        IsNew = true,
                    });
                }
            }
        }
    }
}