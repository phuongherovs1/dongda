﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentDestroySuggestDetailService : BaseService<DocumentDestroySuggestDetailDTO, DocumentDestroySuggestDetailBO>, IDocumentDestroySuggestDetailService
    {
        /// <summary>
        /// Lưu thông tin tài liệu đề nghị hủy
        /// </summary>
        /// <param name="documentIds"></param>
        /// <param name="destroySuggestId"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public IEnumerable<Guid> InsertRange(string documentIds, Guid destroySuggestId, bool allowSave = true)
        {
            try
            {
                var result = Enumerable.Empty<Guid>();
                if (!String.IsNullOrEmpty(documentIds))
                {
                    var data = new List<DocumentDestroySuggestDetailBO>();
                    var documents = JsonConvert.DeserializeObject<List<DocumentBO>>(documentIds);
                    if (documents != null && documents.Count() > 0)
                    {
                        // Xóa những tài liệu
                        var ids = base.Get(p => p.DestroySuggestId == destroySuggestId).Select(p => p.Id);
                        base.DeleteRange(ids, allowSave);
                        foreach (var item in documents)
                        {
                            var detail = new DocumentDestroySuggestDetailBO();
                            detail.DocumentId = item.Id;
                            detail.DestroySuggestId = destroySuggestId;
                            data.Add(detail);
                        }
                        result = InsertRange(data, allowSave);
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Lấy thông tin tài liệu chi tiết theo destroySuggestId hoặc requestId
        /// </summary>
        /// <param name="destroySuggestId"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentDestroySuggestDetailBO> GetByDestroySuggestId(Guid destroySuggestId)
        {
            var data = Get(i => i.DestroySuggestId.Value == destroySuggestId)
                .Select(i =>
                {
                    if (i.DocumentId.HasValue)
                    {
                        var document = ServiceFactory<DocumentService>().GetById(i.DocumentId.Value);
                        i.Document = document == null ? null : document;
                        i.DocumentName = document == null ? string.Empty : document.Name;
                        i.DocumentCode = document == null ? string.Empty : document.Code;
                    }
                    var suggest = ServiceFactory<DocumentDestroySuggestService>().GetById(destroySuggestId);
                    i.DestroyType = suggest.Type;
                    return i;
                });
            return data;
        }

        /// <summary>
        /// Lấy danh sách DocumentId theo đề nghị hủy
        /// </summary>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public IEnumerable<Guid> GetDocumentIdBySuggestId(Guid destroySuggestId)
        {
            var data = Get(i => i.DestroySuggestId.Value == destroySuggestId)
                .Select(i =>
                {
                    if (i.DocumentId.HasValue)
                    {
                        var document = ServiceFactory<DocumentService>().GetById(i.DocumentId.Value);
                        i.Document = document == null ? null : document;
                    }
                    return i.Document.Id;
                });
            return data;
        }

        /// <summary>
        /// Danh sách tài liệu chi tiết của đề nghị hủy
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentDestroySuggestDetailBO> GetAll(int pageIndex, int pageSize, out int count, Guid destroySuggestId)
        {
            try
            {
                var suggest = ServiceFactory<DocumentDestroySuggestService>().GetById(destroySuggestId);
                var data = Get(p => p.DestroySuggestId == destroySuggestId);
                count = data.Count();
                data = Page(data, pageIndex, pageSize).Select(i =>
                {
                    i.DestroyType = suggest.Type;
                    return i;
                });
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Xóa chi tiết đề nghị hủy
        /// </summary>
        /// <param name="suggestId"></param>
        /// <param name="allowSave"></param>
        public void DeleteDetail(Guid suggestId, bool allowSave = true)
        {
            try
            {
                var ids = base.Get(p => p.DestroySuggestId == suggestId).Select(p => p.Id);
                base.DeleteRange(ids, allowSave);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Cập nhật tài liệu của đề nghị hủy
        /// DÙng khi tạo biên bản hủy
        /// </summary>
        /// <param name="jsonData"></param>
        public void UpdateRange(string jsonData)
        {
            try
            {
                var documentDetails = JsonConvert.DeserializeObject<List<DocumentDestroySuggestDetailBO>>(jsonData);
                if (documentDetails != null && documentDetails.Count() > 0)
                {
                    var documents = new List<DocumentDestroySuggestDetailBO>();
                    foreach (var item in documentDetails)
                    {
                        base.Update(item, false);
                    }
                    SaveTransaction(true);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
