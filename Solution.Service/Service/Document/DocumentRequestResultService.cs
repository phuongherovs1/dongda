﻿using iDAS.DataAccess;
using System;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentRequestResultService : BaseService<DocumentRequestResultDTO, DocumentRequestResultBO>, IDocumentRequestResultService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="startAt"></param>
        /// <param name="endAt"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid Update(Guid requestId, bool allowSave = true)
        {
            var data = Get(i => i.DocumentRequestId == requestId).FirstOrDefault();

            base.Update(data, allowSave);
            return data.Id;
        }

        public DocumentRequestResultBO GetSingleByRequest(Guid requestId)
        {
            var data = Get(i => i.DocumentRequestId == requestId).FirstOrDefault();
            if (data != null)
            {
                var fileIds = ServiceFactory<DocumentRequestResultAttachmentService>().GetFileIds(data.Id);
                data.FileAttachs = new FileUploadBO() { Files = fileIds };
            }
            return data;
        }

        public DocumentBO GetDocumentWrite(Guid documentRequestId, Guid documentId)
        {
            var document = new DocumentBO();
            document = ServiceFactory<DocumentService>().GetById(documentId);
            var data = base.Get(t => t.DocumentRequestId == documentRequestId).FirstOrDefault();
            if (data != null)
            {
                document.Name = data.Name;
                document.ShortContent = data.Content;
                var files = ServiceFactory<DocumentRequestResultAttachmentService>().GetFileIds(data.Id);
                data.FileAttachs = new FileUploadBO()
                {
                    Files = files.ToList()
                };
            }
            return document;
        }

        /// <summary>
        /// Cập nhật kết quả và đính kèm
        /// </summary>
        /// <param name="item"></param>
        public Guid SaveResult(DocumentRequestResultBO item, bool allowSave = true)
        {
            if (item.Id == Guid.Empty)
            {
                item.Id = base.Insert(item, false);
            }
            else
            {
                Update(item, false);
            }

            var fileIds = Enumerable.Empty<Guid>();
            //1. Upload đính kèm
            if (item.FileAttachs != null && item.FileAttachs.FileAttachments.Count > 0)
            {
                fileIds = ServiceFactory<FileService>().UploadRange(item.FileAttachs, false);
            }
            if (item.FileAttachs != null && item.FileAttachs.FileRemoves != null && item.FileAttachs.FileRemoves.Count > 0)
                ServiceFactory<DocumentRequestResultAttachmentService>().DeleteRange(item.Id, item.FileAttachs.FileRemoves);

            foreach (var fileId in fileIds)
            {
                var attachment = new DocumentRequestResultAttachmentBO()
                {
                    FileId = fileId,
                    ResultId = item.Id
                };
                ServiceFactory<DocumentRequestResultAttachmentService>().Insert(attachment, false);
            }

            SaveTransaction(allowSave);
            return item.Id;
        }
    }
}
