﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentResponsibilityService : BaseService<DocumentResponsibilityDTO, DocumentResponsibilityBO>, IDocumentResponsibilityService
    {
        /// <summary>
        /// Lấy danh sách trách nhiệm của nhân sự đăng nhập
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentResponsibilityBO> GetByCurrentUser()
        {
            return Get(i => i.PerformBy == UserId && i.IsNew != null);
        }
        public IEnumerable<DocumentResponsibilityBO> GetByExtend(Guid extendId)
        {
            return Get(i => i.ExtendId == extendId);
        }
        public IEnumerable<DocumentResponsibilityBO> GetByExtends(List<Guid?> extendIds)
        {
            return Get(i => extendIds.Contains(i.ExtendId));
        }
        private DocumentResponsibilityBO getResponsibilityExits(Guid departmentId, Guid documentId, int roleType, int order)
        {
            return base.Get(t => t.DocumentId == documentId && t.DepartmentId == departmentId && t.RoleType == roleType && t.Order == order).FirstOrDefault();
        }
        public System.Guid InsertRole(DocumentResponsibilityBO data, bool allowSave = true)
        {
            data.PerformBy = data.EmployeeRecive.Id;
            data.RoleType = data.EmployeeRecive.RoleType;
            data.DepartmentId = data.EmployeeRecive.DepartmentId;
            data.Order = data.EmployeeRecive.NextOrder;
            data.IsNew = data.IsNew;
            if (data.ExtendId.HasValue)
            {
                var obj = base.GetById(data.ExtendId.Value);
                data.PublishDate = obj.PublishDate;
                data.ApplyDate = obj.ApplyDate;
            }
            if (data.EmployeeRecive.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate || data.EmployeeRecive.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.Archiver)
            {
                data.IsApprove = true;
                data.IsAccept = true;
                data.PerformAt = DateTime.Now;
            }
            return base.Insert(data, allowSave);
        }
        public void UpdateRole(DocumentResponsibilityBO data, bool allowSave = true)
        {
            data.PerformBy = data.EmployeeRecive.Id;
            data.RoleType = data.EmployeeRecive.RoleType;
            data.DepartmentId = data.EmployeeRecive.DepartmentId;
            data.Order = data.EmployeeRecive.NextOrder;
            data.IsNew = data.IsNew;
            if (data.EmployeeRecive.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate || data.EmployeeRecive.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.Archiver)
            {
                data.IsApprove = true;
                data.IsAccept = true;
            }
            base.Update(data, allowSave);
        }
        public void Approval(DocumentResponsibilityBO data, bool allowSave = true)
        {
            var obj = base.GetById(data.Id);
            obj.ApplyDate = data.ApplyDate;
            obj.PublishDate = data.PublishDate;
            obj.IsApprove = true;
            obj.IsAccept = data.IsAccept;
            obj.ApproveNote = data.ApproveNote;
            obj.PerformAt = DateTime.Now;
            base.Update(obj, allowSave);
        }

        /// <summary>
        /// Hàm này không có ai comment
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentResponsibilityBO> GetForCheckApproval()
        {
            var data = Enumerable.Empty<DocumentResponsibilityBO>();
            data = base.GetQuery().Where(t => (t.PerformBy.HasValue && t.PerformBy.Value == UserId) || (t.CreateBy.HasValue && t.CreateBy.Value == UserId))
                .Where(t => t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.Check || t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument)
                .Where(t => t.IsNew == true || (t.IsComplete == true && t.IsLock == true))
                //.Select(i =>
                //{
                //    i.UserId = UserId;
                //    // i.EmployeeSend = ServiceFactory<EmployeeService>().GetById(i.CreateBy.Value);
                //    return i;
                //})
                .OrderByDescending(t => t.CreateAt);
            return data;
        }
        public IEnumerable<DocumentResponsibilityBO> GetWriteSuggestApprove()
        {
            var writeSuggest = ServiceFactory<DocumentWriteSuggestService>()
                .GetQuery().Where(i => (i.StatusType == (int)Resource.DocumentSuggestStatusType.Review
                            || i.StatusType == (int)Resource.DocumentSuggestStatusType.Approve)
                            && i.IsDelete == false);
            var data = Enumerable.Empty<DocumentResponsibilityBO>();
            data = base.GetQuery().Where(t => (t.PerformBy.HasValue && t.PerformBy.Value == UserId))
                .Where(t => t.ExtendId.HasValue
                            && writeSuggest.Select(w => w.Id).Contains(t.ExtendId.Value))
                .Where(t => t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit
                            || t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit
                            || t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit
                      )
                .Where(t => t.IsNew == true && t.IsLock != true)
                //.Select(i =>
                //{
                //    i.UserId = UserId;
                //    //  i.EmployeeSend = ServiceFactory<EmployeeService>().GetById(i.CreateBy.Value);
                //    return i;
                //})
                .OrderByDescending(t => t.CreateAt);
            return data;
        }
        public IEnumerable<DocumentResponsibilityBO> GetAdjustSuggestApprove()
        {
            var adjustSuggest = ServiceFactory<DocumentAdjustSuggestService>()
                .Get(i => (i.StatusType == (int)Resource.DocumentSuggestStatusType.Review
                            || i.StatusType == (int)Resource.DocumentSuggestStatusType.Approve)
                            && i.IsDelete == false);
            var data = Enumerable.Empty<DocumentResponsibilityBO>();
            data = base.Get(t => (t.PerformBy.HasValue && t.PerformBy.Value == UserId))
                .Where(t => t.ExtendId.HasValue
                            && adjustSuggest.Select(w => w.Id).Contains(t.ExtendId.Value))
               .Where(t => t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit
                            || t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit
                            || t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit
                      )
                 .Where(t => t.IsNew == true && t.IsLock != true)
                .Select(i =>
                {
                    i.UserId = UserId;
                    // i.EmployeeSend = ServiceFactory<EmployeeService>().GetById(i.CreateBy.Value);
                    return i;
                })
                .OrderByDescending(t => t.CreateAt);
            return data;
        }
        public IEnumerable<DocumentResponsibilityBO> GetForPromulgate()
        {
            var data = Enumerable.Empty<DocumentResponsibilityBO>();
            data = base.GetQuery().Where(t => (t.PerformBy.HasValue && t.PerformBy.Value == UserId) || (t.CreateBy.HasValue && t.CreateBy.Value == UserId))
                .Where(t => t.IsNew == true)
                .Where(t => t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate)
                .AsEnumerable()
                .Select(i =>
                {
                    i.UserId = UserId;
                    return i;
                })
                .OrderByDescending(t => t.CreateAt);
            return data;
        }
        public IEnumerable<DocumentResponsibilityBO> GetForArchive()
        {
            var data = Enumerable.Empty<DocumentResponsibilityBO>();
            data = base.GetQuery().Where(t => (t.PerformBy.HasValue && t.PerformBy.Value == UserId) || (t.CreateBy.HasValue && t.CreateBy.Value == UserId))
                .Where(t => t.IsNew == true)
                .Where(t => t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.Archiver)
                .AsEnumerable()
                .Select(i =>
                {
                    i.UserId = UserId;
                    return i;
                })
                .OrderByDescending(t => t.CreateAt);
            return data;
        }
        public DocumentResponsibilityBO GetDetail(Guid id)
        {
            var data = base.GetById(id);
            data.EmployeeSend = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.CreateBy.HasValue ? data.CreateBy.Value : Guid.Empty);
            if (data.DocumentId.HasValue)
            {
                data.Document = ServiceFactory<DocumentService>().GetById(data.DocumentId.Value);
                data.Archive = ServiceFactory<DocumentArchiveService>().GetByDocument(data.DocumentId.Value);
            }
            else
            {
                data.Document = new DocumentBO();
                data.Archive = new DocumentArchiveBO();
            }
            var temp = ServiceFactory<DocumentAttachmentService>().Get(x => x.DocumentId == data.DocumentId.Value).Select(t => t.FileId.HasValue ? t.FileId.Value : Guid.Empty);
            data.Document.FileAttachs = new FileUploadBO()
            {
                Files = temp.ToList()
            };
            return data;
        }
        public IEnumerable<DocumentResponsibilityBO> GetByDocument(Guid documentId, int pageIndex, int pageSize, out int totalCount)
        {
            var data = Enumerable.Empty<DocumentResponsibilityBO>();
            data = base.Get(t => t.DocumentId == documentId)
                .Select(i =>
                {
                    var em = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.PerformBy.Value);
                    i.EmployeeRecive = new DocumentEmployeeBO
                    {
                        Id = i.PerformBy.Value,
                        Name = em.Name,
                        AvatarUrl = em.AvatarUrl,
                        RoleNames = em.RoleNames,
                        NextOrder = i.Order.Value,
                        RoleType = i.RoleType.Value
                    };
                    return i;
                });
            totalCount = data.Count();
            data = Page(data, pageIndex, pageSize);
            return data;
        }
        public void ChangeRole(DocumentResponsibilityBO item)
        {
            var obj = base.GetById(item.Id);
            obj.PerformBy = item.EmployeeChange.Id;
            obj.ContentSuggest = item.ContentSuggest;
            obj.IsApprove = false;
            obj.IsAccept = false;
            base.Update(obj);
        }
        public void Cancel(Guid id)
        {
            var obj = base.GetById(id);
            obj.IsCancel = true;
            base.Update(obj);
        }
        public void RevertApproval(Guid id)
        {
            var obj = base.GetById(id);
            obj.IsApprove = false;
            obj.IsCancel = false;
            obj.IsAccept = false;
            obj.ApproveNote = string.Empty;
            base.Update(obj);
        }
        private DocumentResponsibilityBO getDocumentResponsibilityCurrent(Guid departmentId, Guid documentId, int order)
        {

            var processItem = base.Get(t => t.DepartmentId == departmentId && t.DocumentId == documentId && t.Order == order)
                .FirstOrDefault();
            return processItem;
        }
        private int GetMinOrder(Guid departmentId, Guid documentId)
        {
            return base.Get(t => t.DepartmentId == departmentId && t.DocumentId == documentId).Min(t => t.Order.HasValue ? t.Order.Value : 0);
        }
        public void GetLastResponsibility(Guid departmentId, Guid documentId, int order, ref DocumentResponsibilityBO documentResponsibility)
        {
            var min = GetMinOrder(departmentId, documentId);
            if (order < min)
                return;
            else
            {
                var responsibility = getDocumentResponsibilityCurrent(departmentId, documentId, order);
                if (responsibility != null)
                {
                    documentResponsibility = responsibility;
                    return;
                }
                else
                {
                    order--;
                    GetLastResponsibility(departmentId, documentId, order, ref documentResponsibility);
                }
            }
        }
        public void UpdateCompleteAndLock(Guid id, bool isComplete = true, bool allowSave = true)
        {
            var obj = base.GetById(id);
            if (obj.Id != Guid.Empty)
            {
                obj.IsComplete = isComplete;
                obj.IsLock = isComplete;
                base.Update(obj, allowSave);
            }
            else
            {
                var request = ServiceFactory<DocumentRequestService>().GetById(id);
                if (request != null)
                {
                    request.IsComplete = isComplete;
                    ServiceFactory<DocumentRequestService>().Update(request);
                }
            }
        }
        public void RevertComplete(Guid id, bool isComplete = true, bool allowSave = true)
        {
            var obj = GetByExtendId(id);
            if (obj != null && obj.Id != Guid.Empty)
            {
                obj.IsLock = isComplete;
                obj.IsComplete = isComplete;
                base.Update(obj, allowSave);
            }
            else
            {
                var request = ServiceFactory<DocumentRequestService>().GetById(id);
                if (request != null)
                {
                    request.IsComplete = isComplete;
                    request.IsFinish = isComplete;
                    ServiceFactory<DocumentRequestService>().Update(request);
                }
            }
        }
        public void UpdateArchiveComplete(Guid id, bool isComplete = true, bool allowSave = true)
        {
            var obj = base.GetById(id);
            obj.IsComplete = isComplete;
            obj.IsLock = isComplete;
            base.Update(obj, allowSave);
        }
        public void InsertResponsibility(DocumentResponsibilityBO item, bool allowSave = true)
        {
            item.IsNew = true;
            var id = InsertRole(item, true);
            item.Id = id;
            if (item.ExtendId.HasValue)
                UpdateCompleteAndLock(item.ExtendId.Value, item.IsNew.HasValue ? item.IsNew.Value : false, true);
        }
        public void UpdateResponsibility(DocumentResponsibilityBO item)
        {
            UpdateRole(item, false);
            if (item.ExtendId.HasValue)
                UpdateCompleteAndLock(item.ExtendId.Value, item.IsNew.HasValue ? item.IsNew.Value : false, false);
            base.SaveTransaction();
        }

        public DocumentResponsibilityBO GetResponsibilitybyPublish(Guid documentId)
        {
            var data = base.Get(t => t.DocumentId == documentId && t.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate)
                .FirstOrDefault();
            return data;
        }
        public void RevertRole(Guid id)
        {
            var obj = base.GetById(id);
            obj.IsNew = false;
            obj.IsLock = false;
            obj.IsComplete = false;
            if (obj.ExtendId.HasValue)
                RevertComplete(obj.ExtendId.Value, false, false);
            base.Update(obj, false);
            base.SaveTransaction();
        }
        private DocumentResponsibilityBO GetByExtendId(Guid extendId)
        {
            return base.Get(t => t.Id == extendId).FirstOrDefault();
        }

        public void SendRequestArchive(DocumentResponsibilityBO item)
        {
            try
            {
                item.EmployeeRecive = new DocumentEmployeeBO()
                {
                    Id = item.EmployeePerformBy.Id,
                    DepartmentId = item.DepartmentId,
                    RoleType = (int)Resource.DocumentProcessRole.Archiver

                };
                item.SendTo = UserId;
                item.IsNew = true;
                InsertRole(item);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


    }
}
