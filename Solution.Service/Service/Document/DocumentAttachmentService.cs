﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentAttachmentService : BaseService<DocumentAttachmentDTO, DocumentAttachmentBO>, IDocumentAttachmentService
    {
        /// <summary>
        /// Đưa ra danh sách Id đính kèm theo tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public IEnumerable<Guid> GetByDocument(Guid documentId)
        {
            var data = Get(i => i.DocumentId.Value == documentId)
                .Select(
                i => i.FileId.Value
                );
            return data;
        }
        public IQueryable<Guid> GetIds(Guid documentId, IEnumerable<Guid> fileIds)
        {
            return base.GetQuery().Where(i => i.DocumentId == documentId && fileIds.Any(z => z == i.FileId)).Select(i => i.Id);
        }
        /// <summary>
        /// Đưa ra danh sách Id đính kèm theo tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentAttachmentBO> GetByDocuments(IEnumerable<Guid> documentIds)
        {
            var data = GetQuery().Where(i => i.DocumentId.HasValue && documentIds.Contains(i.DocumentId.Value));
            return data;
        }
        /// <summary>
        /// Thực hiệp cập nhạt 
        /// </summary>
        /// <param name="fileUpload"></param>
        /// <param name="documentId"></param>
        /// <param name="allowSave"></param>
        public void Update(FileUploadBO fileUpload, Guid documentId, bool allowSave = true)
        {
            if (fileUpload != null)
            {
                RemoveRange(fileUpload.FileRemoves, false);
                var fileIds = ServiceFactory<FileService>().UploadRange(fileUpload, false);
                var data = fileIds.Select(i => new DocumentAttachmentBO
                {
                    DocumentId = documentId,
                    FileId = i
                });
                InsertRange(data, false);
                SaveTransaction(allowSave);
            }
        }
        public void Update(DocumentBO document, bool allowSave = true)
        {
            Update(document.FileAttachs, document.Id, allowSave);
        }
    }
}
