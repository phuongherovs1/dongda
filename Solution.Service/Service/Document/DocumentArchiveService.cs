﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentArchiveService : BaseService<DocumentArchiveDTO, DocumentArchiveBO>, IDocumentArchiveService
    {

        public DocumentArchiveBO GetByDocument(Guid documentId)
        {
            return base.Get(t => t.DocumentId == documentId).FirstOrDefault();
        }
        public IEnumerable<DocumentArchiveBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            var result = base.Get(p => p.CreateBy == UserId);
            count = result.Count();
            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            foreach (var item in data)
            {
                item.Document = ServiceFactory<DocumentService>().GetById(item.DocumentId.HasValue ? item.DocumentId.Value : Guid.Empty);
            }
            return data;
        }

        public void SaveArchive(Guid pesponsibilityId, DocumentArchiveBO item)
        {
            base.Insert(item, true);
            ServiceFactory<DocumentResponsibilityService>().UpdateArchiveComplete(pesponsibilityId, true, true);
        }
        public void UpdateArchive(Guid pesponsibilityId, DocumentArchiveBO item)
        {
            base.Update(item, false);
            ServiceFactory<DocumentResponsibilityService>().UpdateArchiveComplete(pesponsibilityId, true, false);
            base.SaveTransaction();
        }
    }
}
