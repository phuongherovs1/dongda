﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public class DocumentWriteSuggestPerformService : BaseService<DocumentWriteSuggestPerformDTO, DocumentWriteSuggestPerformBO>, IDocumentWriteSuggestPerformService
    {
        public IEnumerable<DocumentWriteSuggestPerformBO> GetByCurrentUser()
        {
            var data = Get().Where(i => i.PerformBy == UserId);
            return data;
        }
        public IEnumerable<DocumentWriteSuggestPerformBO> GetBySuggest(Guid suggestId)
        {
            var data = Get().Where(i => i.WriteSuggestId == suggestId);
            return data;
        }
        public IEnumerable<DocumentWriteSuggestPerformBO> GetEmployeeRoleBySuggest(Guid suggestId)
        {
            var data = Get().Where(i => i.WriteSuggestId == suggestId)
                            .Select(item =>
                            {
                                item.EmployeePerformBy = ServiceFactory<EmployeeService>().GetById((Guid)item.PerformBy);
                                return item;
                            });
            return data;
        }
        public IEnumerable<DocumentWriteSuggestPerformBO> GetReviewPerformingByCurrentUser()
        {
            var data = Get().Where(i => i.PerformBy == UserId && i.IsReviewRole == true && i.IsNew == true && i.IsDelete == false);
            return data;
        }
        public DocumentWriteSuggestPerformBO GetDetail(Guid id)
        {
            var data = GetById(id);
            if (data != null)
            {
                data.WriteSuggest = ServiceFactory<DocumentWriteSuggestService>().GetDetail((Guid)data.WriteSuggestId);
            }
            return data;
        }
    }
}
