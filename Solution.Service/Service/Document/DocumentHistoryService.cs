﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentHistoryService : BaseService<DocumentHistoryDTO, DocumentHistoryBO>, IDocumentHistoryService
    {
        /// <summary>
        /// Danh sách lịch sử sửa đổi của tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentHistoryBO> GetByDocumentId(Guid documentId)
        {
            var data = Enumerable.Empty<DocumentHistoryBO>();
            var item = Get(i => i.DocumentId == documentId).FirstOrDefault();
            if (item != null && !string.IsNullOrEmpty(item.AuditTrail))
            {
                var documentIds = item.AuditTrail;
                data = Get(i => i.DocumentId.HasValue && documentIds.Contains(i.DocumentId.Value.ToString()));
            }
            return data;
        }


    }
}
