﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentSummaryService : BaseService<DocumentDTO, DocumentBO>, IDocumentSummaryService
    {

        public List<PieChartBO> DocumentStatusAnalytic(Guid departmentId)
        {
            var summaryData = new List<PieChartBO>();
            summaryData.Add(new PieChartBO()
            {
                Name = iDAS.Service.Common.Resource.EDocumentStatusText.New,
                Value = ServiceFactory<DocumentService>().GetQuery().Where(c => c.IsNew == true && c.DepartmentId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = iDAS.Service.Common.Resource.EDocumentStatusText.WaitPublish,
                Value = ServiceFactory<DocumentService>().GetQuery().Where(c => c.IsWaitPublish == true && c.DepartmentId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = iDAS.Service.Common.Resource.EDocumentStatusText.Publish,
                Value = ServiceFactory<DocumentService>().GetQuery().Where(c => c.IsPublish == true && c.DepartmentId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = iDAS.Service.Common.Resource.EDocumentStatusText.Obsolate,
                Value = ServiceFactory<DocumentService>().GetQuery().Where(c => c.IsObsolate == true && c.DepartmentId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = iDAS.Service.Common.Resource.EDocumentStatusText.Expire,
                Value = ServiceFactory<DocumentService>().GetQuery().Where(c => c.IsExpire == true && c.DepartmentId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = iDAS.Service.Common.Resource.EDocumentStatusText.Destroy,
                Value = ServiceFactory<DocumentService>().GetQuery().Where(c => c.IsDestroy == true && c.DepartmentId == departmentId).Count()
            });
            return summaryData;

        }
        public List<PieChartBO> DocumentApproveActorAnalytic()
        {
            var summaryData = new List<PieChartBO>();


            return summaryData;
        }
        public List<PieChartBO> DocumentPubishActorAnalytic()
        {
            var summaryData = new List<PieChartBO>();

            return summaryData;
        }
        public List<PieChartBO> DocumentArchiveActorAnalytic()
        {
            var summaryData = new List<PieChartBO>();

            return summaryData;
        }

        public IEnumerable<DocumentDasboardApproveBO> GetDocumentApproveByCurrentUser()
        {

            var result = new List<DocumentDasboardApproveBO>();

            // Lấy danh sách đề nghị xem xét tài liệu
            var listCheckAndApprove = ServiceFactory<DocumentResponsibilityService>().GetForCheckApproval()
                                        .Select(item => new DocumentDasboardApproveBO
                                        {
                                            Id = item.Id,
                                            Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                                            IsNew = item.IsNew,
                                            IsAccept = item.IsAccept,
                                            IsApprove = item.IsApprove,
                                            IsComplete = item.IsComplete,
                                            DocumentId = item.DocumentId,
                                            RoleType = item.RoleType,
                                            RoleView = item.RoleView,
                                            IsDestroy = item.IsCancel.HasValue ? item.IsCancel.Value : false,
                                            IsCreater = item.CreateBy == UserId,
                                            IsPerform = item.IsPerform,
                                            CreateBy = item.CreateBy,
                                            CreateAt = item.CreateAt,
                                        }).ToList();

            if (listCheckAndApprove.Count > 0) result.AddRange(listCheckAndApprove);
            var listWriteSuggest = ServiceFactory<DocumentResponsibilityService>().GetWriteSuggestApprove()
                                     .Select(item => new DocumentDasboardApproveBO
                                     {
                                         Id = item.Id,
                                         Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                                         IsNew = item.IsNew,
                                         IsAccept = item.IsAccept,
                                         IsApprove = item.IsApprove,
                                         IsComplete = item.IsComplete,
                                         RoleType = item.RoleType,
                                         CreateBy = item.CreateBy,
                                         DocumentId = item.DocumentId,
                                         RoleView = item.RoleView,
                                         IsCreater = item.CreateBy == UserId,
                                         IsPerform = item.IsPerform,
                                         IsLock = item.IsLock,
                                         IsWrite = true,
                                         IsAdjust = false,
                                         CreateAt = item.CreateAt
                                     });
            result.AddRange(listWriteSuggest);
            // Lấy danh sách đề nghị sửa đổi tài liệu
            var listAdjustSuggest = ServiceFactory<DocumentResponsibilityService>().GetAdjustSuggestApprove()
                                    .Select(item => new DocumentDasboardApproveBO
                                    {
                                        Id = item.Id,
                                        Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                                        IsNew = item.IsNew,
                                        IsAccept = item.IsAccept,
                                        IsApprove = item.IsApprove,
                                        IsComplete = item.IsComplete,
                                        RoleType = item.RoleType,
                                        RoleView = item.RoleView,
                                        IsCreater = item.CreateBy == UserId,
                                        DocumentId = item.DocumentId,
                                        IsPerform = item.IsPerform,
                                        CreateBy = item.CreateBy,
                                        IsLock = item.IsLock,
                                        IsAdjust = true,
                                        IsWrite = false,
                                        CreateAt = item.CreateAt
                                    });
            result.AddRange(listAdjustSuggest);
            // Lấy danh sách đề nghị hủy tài liệu
            var ListGetDataDestroySuggest = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetDataDocumentDestroySuggestResponsibitity()
                                     .Select(item => new DocumentDasboardApproveBO
                                     {
                                         Id = item.Id,
                                         DocumentDestroyId = item.DocumentDestroyId,
                                         Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                                         IsNew = item.IsNew,
                                         IsAccept = item.IsAccept,
                                         IsApprove = item.IsApprove,
                                         IsComplete = item.IsComplete,
                                         RoleType = item.RoleType,
                                         RoleView = item.RoleView,
                                         IsCreater = item.CreateBy == UserId,
                                         CreateBy = item.CreateBy,
                                         CheckDestroy = true,
                                         IsLock = item.IsLock,
                                         IsArchiver = item.IsArchiver,
                                         CreateAt = item.CreateAt
                                     });
            result.AddRange(ListGetDataDestroySuggest);
            // Danh sách đề nghị phân phối tài liệu
            var suggestDistributes = ServiceFactory<DocumentDistributeSuggestResponsibilityService>().GetResponsibilityForDasboard();
            var listSuggestDistributes = suggestDistributes
                                     .Select(item => new DocumentDasboardApproveBO
                                     {
                                         Id = item.Id,
                                         Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                                         IsNew = item.IsNew,
                                         IsAccept = item.IsAccept,
                                         IsApprove = item.IsApprove,
                                         IsComplete = item.IsComplete,
                                         RoleType = item.RoleType,
                                         CreateBy = item.CreateBy,
                                         RoleView = item.RoleView,
                                         IsDistribute = true,
                                         IsLock = item.IsLock,
                                         CreateAt = item.CreateAt
                                     });
            result.AddRange(listSuggestDistributes);
            // Lấy danh sách đề nghị áp dụng
            //var listSuggestApply = ServiceFactory<DocumentExternalApplySuggestService>().GetSuggestSend()
            //                    .Select(item => new DocumentDasboardApproveBO
            //                    {
            //                        Id = item.Id,
            //                        Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
            //                        IsNew = item.IsSend,
            //                        IsAccept = item.IsApply,
            //                        IsApprove = item.IsApprove,
            //                        DocumentId = item.DocumentId,
            //                        CreateBy = item.CreateBy,
            //                        CreateAt = item.CreateAt,
            //                        RoleType = item.RoleStatus,
            //                        IsSuggestApply = true
            //                    });

            return result.OrderByDescending(i => i.CreateAt);
        }
        public IEnumerable<DocumentDasboardApproveBO> GetPromulgate()
        {
            var result = Enumerable.Empty<DocumentDasboardApproveBO>();
            var promulgates = ServiceFactory<DocumentResponsibilityService>().GetForPromulgate()
                        .Select(item => new DocumentDasboardApproveBO
                        {
                            Id = item.Id,
                            Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                            IsNew = item.IsNew,
                            IsAccept = item.IsAccept,
                            IsApprove = item.IsApprove,
                            CreateBy = item.CreateBy,
                            IsComplete = item.IsComplete,
                            DocumentId = item.DocumentId,
                            RoleType = item.RoleType,
                            IsDestroy = item.IsCancel.HasValue ? item.IsCancel.Value : false,
                            RoleView = item.RoleView,
                            IsCreater = item.IsCreater,
                            IsPerform = item.IsPerform
                        });
            if (promulgates.Count() > 0) { result = result.Union(promulgates); }

            var requestDesTroys = ServiceFactory<DocumentRequestService>().GetRequestDesTroy()
                       .Select(item => new DocumentDasboardApproveBO
                       {
                           Id = item.Id,
                           Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                           IsNew = item.IsSend,
                           IsAccept = item.IsAccept,
                           IsApprove = item.IsApprove,
                           IsComplete = item.IsComplete,
                           RoleType = item.TypeRole,
                           DocumentId = item.DocumentId,
                           TypeDestroy = item.TypeDestroy,
                           CreateBy = item.CreateBy,
                           RoleView = item.RoleView,
                           IsCreater = item.IsCreater,
                           IsPerform = item.IsPerformBy
                       });
            if (requestDesTroys.Count() > 0) { result = result.Union(requestDesTroys); }

            var ListGetDataDocumentSuggestPublishResponsibitity = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetDataDocumentSuggestPublishResponsibitity()
                                  .Select(item => new DocumentDasboardApproveBO
                                  {
                                      Id = item.Id,
                                      DocumentDestroyId = item.DocumentDestroyId,
                                      Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                                      IsNew = item.IsNew,
                                      IsAccept = item.IsAccept,
                                      IsApprove = item.IsApprove,
                                      IsComplete = item.IsComplete,
                                      CreateBy = item.CreateBy,
                                      RoleType = item.RoleType,
                                      RoleView = item.RoleView,
                                      IsCreater = item.IsCreater,
                                      CheckDestroy = true,
                                      IsLock = item.IsLock,
                                      CreateAt = item.CreateAt
                                  });
            if (ListGetDataDocumentSuggestPublishResponsibitity.Count() > 0) { result = result.Union(ListGetDataDocumentSuggestPublishResponsibitity); }

            var promulgateFromDistributes = ServiceFactory<DocumentDistributeSuggestService>().GetPromulateDasboard()
                            .Select(item => new DocumentDasboardApproveBO
                            {
                                Id = item.Id,
                                Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                                IsAccept = item.IsAccept,
                                IsApprove = item.IsApprove,
                                IsComplete = item.IsComplete,
                                DocumentId = item.DocumentId,
                                CreateBy = item.CreateBy,
                                RoleType = item.RoleType,
                                RoleView = item.RoleView,
                                IsDistribute = true
                            }
                            );
            if (promulgateFromDistributes.Count() > 0)
            {
                result = result.Concat(promulgateFromDistributes);
            }
            return result.OrderByDescending(i => i.CreateAt);
        }
        public IEnumerable<DocumentDasboardApproveBO> GetArchive()
        {
            var result = Enumerable.Empty<DocumentDasboardApproveBO>();
            var archives = ServiceFactory<DocumentResponsibilityService>().GetForArchive()
                        .Select(item => new DocumentDasboardApproveBO
                        {
                            Id = item.Id,
                            Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                            IsNew = item.IsNew,
                            IsAccept = item.IsAccept,
                            IsApprove = item.IsApprove,
                            IsComplete = item.IsComplete,
                            DocumentId = item.DocumentId,
                            RoleType = item.RoleType,
                            RoleView = item.RoleView,
                            CreateBy = item.CreateBy,
                            IsDestroy = item.IsCancel.HasValue ? item.IsCancel.Value : false,
                            IsCreater = item.IsCreater,
                            IsPerform = item.IsPerform
                        });
            if (archives.Count() > 0) { result = result.Union(archives); }
            var distributeRequests = ServiceFactory<DocumentRequestService>().GetDistributeRequest()
                        .Select(item => new DocumentDasboardApproveBO
                        {
                            Id = item.Id,
                            Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                            IsNew = item.IsSend,
                            IsAccept = item.IsAccept,
                            IsApprove = item.IsApprove,
                            IsComplete = item.IsComplete,
                            RoleType = item.TypeRole,
                            DocumentId = item.DocumentId,
                            RoleView = item.RoleView,
                            CreateBy = item.CreateBy,
                            IsCreater = item.IsCreater,
                            IsPerform = item.IsPerformBy
                        });
            if (distributeRequests.Count() > 0) { result = result.Union(distributeRequests); }
            var requestDesTroys = ServiceFactory<DocumentRequestService>().GetRequestDesTroy()
                        .Select(item => new DocumentDasboardApproveBO
                        {
                            Id = item.Id,
                            Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                            IsNew = item.IsSend,
                            IsAccept = item.IsAccept,
                            IsApprove = item.IsApprove,
                            IsComplete = item.IsComplete,
                            RoleType = item.TypeRole,
                            DocumentId = item.DocumentId,
                            TypeDestroy = item.TypeDestroy,
                            CreateBy = item.CreateBy,
                            RoleView = item.RoleView,
                            IsCreater = item.IsCreater,
                            IsPerform = item.IsPerformBy
                        });
            if (requestDesTroys.Count() > 0) { result = result.Union(requestDesTroys); }
            return result.OrderByDescending(i => i.CreateAt);
        }
        public IEnumerable<DocumentDasboardApproveBO> GetDocumentPaggingApproveByCurrentUser(int pageIndex, int pageSize, out int count, string filterName = default(string), Resource.EDocumentStatus status = 0)
        {
            var result = Enumerable.Empty<DocumentDasboardApproveBO>();
            result = GetDocumentApproveByCurrentUser();
            count = result.Count();
            var data = result.OrderByDescending(i => i.CreateAt).Skip(--pageIndex * pageSize).Take(pageSize);
            var employeeIds = data.Select(i => i.CreateBy.Value);
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds);
            var documentIds = data.Where(i => i.DocumentId.HasValue).Select(i => i.DocumentId.Value);
            var documents = ServiceFactory<DocumentService>().GetByIds(documentIds);
            data = data.Select(i =>
                {
                    if (i.DocumentId.HasValue)
                        i.Document = documents.FirstOrDefault(u => i.DocumentId == u.Id);
                    i.Sender = employees.FirstOrDefault(u => i.CreateBy == u.Id);
                    return i;
                });
            return data;
        }
        public IEnumerable<DocumentDasboardApproveBO> GetPaggingPromulgate(int pageIndex, int pageSize, out int count, string filterName = default(string), Resource.EDocumentStatus status = 0)
        {
            var result = Enumerable.Empty<DocumentDasboardApproveBO>();
            result = GetPromulgate();
            count = result.Count();
            var data = result.OrderByDescending(i => i.CreateAt).Skip(--pageIndex * pageSize).Take(pageSize);
            var employeeIds = data.Select(i => i.CreateBy.Value);
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds);
            var documentIds = data.Where(i => i.DocumentId.HasValue).Select(i => i.DocumentId.Value);
            var documents = ServiceFactory<DocumentService>().GetByIds(documentIds);
            data = data.Select(i =>
                {
                    if (i.DocumentId.HasValue)
                        i.Document = documents.FirstOrDefault(u => i.DocumentId == u.Id);
                    i.Sender = employees.FirstOrDefault(u => i.CreateBy == u.Id);
                    return i;
                });
            return data;
        }

        public IEnumerable<DocumentDasboardApproveBO> GetPaggingArchive(int pageIndex, int pageSize, out int count, string filterName = default(string), Resource.EDocumentStatus status = 0)
        {
            var result = Enumerable.Empty<DocumentDasboardApproveBO>();
            result = GetArchive();
            count = result.Count();
            var data = result.OrderByDescending(i => i.CreateAt).Skip(--pageIndex * pageSize).Take(pageSize);
            var employeeIds = data.Select(i => i.CreateBy.Value);
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds);
            var documentIds = data.Where(i => i.DocumentId.HasValue).Select(i => i.DocumentId.Value);
            var documents = ServiceFactory<DocumentService>().GetByIds(documentIds);
            data = data.Select(i =>
                {
                    if (i.DocumentId.HasValue)
                        i.Document = documents.FirstOrDefault(u => i.DocumentId == u.Id);
                    i.Sender = employees.FirstOrDefault(u => i.CreateBy == u.Id);
                    return i;
                });
            return data;
        }


        public List<PieChartBO> DocumentTypeAnalytic(Guid departmentId)
        {
            var summaryData = new List<PieChartBO>();
            var cateInternalIds = ServiceFactory<DocumentCategoryService>().GetQuery().Where(c => c.IsInternal == true && c.DepartmentId == departmentId).Select(c => c.Id).ToList();
            var cateExternalIds = ServiceFactory<DocumentCategoryService>().GetQuery().Where(c => c.IsExternal == true && c.DepartmentId == departmentId).Select(c => c.Id).ToList();
            var cateDistributeIds = ServiceFactory<DocumentCategoryService>().GetQuery().Where(c => c.IsDistribute == true && c.DepartmentId == departmentId).Select(c => c.Id).ToList();
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.DocumentCategoryTypeText.Internal,
                Value = ServiceFactory<DocumentService>().GetQuery().Where(c => cateInternalIds.Any(i => i == c.CategoryId)).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.DocumentCategoryTypeText.External,
                Value = ServiceFactory<DocumentService>().GetQuery().Where(c => cateExternalIds.Any(i => i == c.CategoryId)).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.DocumentCategoryTypeText.Distribute,
                Value = ServiceFactory<DocumentService>().GetQuery().Where(c => cateDistributeIds.Any(i => i == c.CategoryId)).Count()
            });
            return summaryData;
        }
    }
}
