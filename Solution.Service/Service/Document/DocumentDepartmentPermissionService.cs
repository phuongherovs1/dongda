﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentDepartmentPermissionService : BaseService<DocumentDepartmentPermissionDTO, DocumentDepartmentPermissionBO>, IDocumentDepartmentPermissionService
    {
        /// <summary>
        /// Kiểm tra người đăng nhập có quyền cập nhật trong phòng ban hay không 
        /// </summary>
        /// <param name="departmentId"></param>
        /// <param name="userId"></param>
        /// <returns>True: có quyền, False: không có quyền</returns>
        public bool CheckRoleSettingCategory(Guid departmentId, Guid userId)
        {
            try
            {
                var titleOfUser = ServiceFactory<EmployeeTitleService>().Get(u => u.EmployeeId == userId).Select(i => i.TitleId).Distinct();
                return base.GetQuery().Any(p => p.DepartmentId == departmentId && p.IsSettingCategory == true && (p.EmployeeId == userId || (p.DepartmentTitleId.HasValue && titleOfUser.Contains(p.DepartmentTitleId.Value))));
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool CheckRoleSettingProcess(Guid departmentId, Guid userId)
        {
            try
            {
                var titleOfUser = ServiceFactory<EmployeeTitleService>().Get(u => u.EmployeeId == userId).Select(i => i.TitleId).Distinct();
                return base.GetQuery().Any(p => p.DepartmentId == departmentId && p.IsSettingProcess == true && (p.EmployeeId == userId || (p.DepartmentTitleId.HasValue && titleOfUser.Contains(p.DepartmentTitleId.Value))));
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool CheckRoleArchive(Guid departmentId, Guid userId)
        {
            try
            {
                var titleOfUser = ServiceFactory<EmployeeTitleService>().Get(u => u.EmployeeId == userId).Select(i => i.TitleId).Distinct();
                return base.GetQuery().Any(p => p.DepartmentId == departmentId && p.IsArchive == true && (p.EmployeeId == userId || (p.DepartmentTitleId.HasValue && titleOfUser.Contains(p.DepartmentTitleId.Value))));
            }
            catch (Exception)
            {
                throw;
            }
        }
        public override DocumentDepartmentPermissionBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var obj = base.GetById(id, allowDeleted, allowDefaultIfNull);
            obj.DepartmentName = ServiceFactory<DepartmentService>().GetById(obj.DepartmentId.Value).Name;
            if (obj.DepartmentTitleId.HasValue)
                obj.ObjectName = ServiceFactory<DepartmentTitleService>().GetById(obj.DepartmentTitleId.Value).Name;
            else
                obj.ObjectName = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(obj.EmployeeId.Value).Name;
            return obj;
        }

        /// <summary>
        /// Lấy danh sách phòng ban mà user có quyền
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<Guid> GetDepartmentRoleSettingCategoryByUser()
        {
            try
            {
                var result = Enumerable.Empty<Guid>();
                var titleOfUser = ServiceFactory<EmployeeTitleService>().Get(u => u.EmployeeId == UserId).Select(i => i.TitleId).Distinct();
                var query = base.GetQuery();
                result = query.Where(p => p.IsSettingCategory == true && (p.EmployeeId == UserId
                    || (p.DepartmentTitleId.HasValue && titleOfUser.Contains(p.DepartmentTitleId.Value))))
                    .Select(p => p.DepartmentId.Value);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<Guid> GetDepartmentRoleSettingProcessByUser()
        {
            try
            {
                var result = Enumerable.Empty<Guid>();
                var titleOfUser = ServiceFactory<EmployeeTitleService>().Get(u => u.EmployeeId == UserId).Select(i => i.TitleId).Distinct();
                var query = base.GetQuery();
                result = query.Where(p => p.IsSettingProcess == true && (p.EmployeeId == UserId
                    || (p.DepartmentTitleId.HasValue && titleOfUser.Contains(p.DepartmentTitleId.Value))))
                    .Select(p => p.DepartmentId.Value);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<Guid> GetDepartmentRoleArchiveByUser()
        {
            try
            {
                var result = Enumerable.Empty<Guid>();
                var titleOfUser = ServiceFactory<EmployeeTitleService>().Get(u => u.EmployeeId == UserId).Select(i => i.TitleId).Distinct();
                var query = base.GetQuery();
                result = query.Where(p => p.IsArchive == true && (p.EmployeeId == UserId
                    || (p.DepartmentTitleId.HasValue && titleOfUser.Contains(p.DepartmentTitleId.Value))))
                    .Select(p => p.DepartmentId.Value);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Danh sách
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        /// 
        public IEnumerable<DocumentDepartmentPermissionBO> GetAll(int pageSize, int pageIndex, out int count, Guid departmentId)
        {
            var result = Enumerable.Empty<DocumentDepartmentPermissionBO>();
            result = base.GetQuery().Where(p => p.DepartmentId == departmentId);
            count = result.Count();
            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize)
            .Select(i =>
            {
                if (i.DepartmentTitleId.HasValue && i.DepartmentTitleId != Guid.Empty)
                    i.DepartmentTitle = ServiceFactory<DepartmentTitleService>().GetById(i.DepartmentTitleId.Value);
                else
                    i.Employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.EmployeeId.Value);
                return i;
            });
            return data;
        }

        public void UpdatePermission(DocumentDepartmentPermissionBO item)
        {
            var obj = base.GetById(item.Id);
            obj.DepartmentTitleId = (item.DepartmentTitleId.HasValue && item.DepartmentTitleId != Guid.Empty) ? item.DepartmentTitleId.Value : Guid.Empty;
            obj.EmployeeId = (item.EmployeeId.HasValue && item.EmployeeId != Guid.Empty) ? item.EmployeeId.Value : Guid.Empty;
            obj.Description = item.Description;
            obj.IsArchive = item.IsArchive.HasValue ? item.IsArchive.Value : false;
            obj.IsSettingCategory = item.IsSettingCategory.HasValue ? item.IsSettingCategory.Value : false;
            obj.IsSettingProcess = item.IsSettingProcess.HasValue ? item.IsSettingProcess.Value : false;
            obj.DepartmentId = item.DepartmentId;
            base.Update(obj);
        }

        public bool CheckRoleSettingCategory(Guid departmentId)
        {
            return CheckRoleSettingCategory(departmentId, UserId);
        }

        public bool CheckRoleSettingProcess(Guid departmentId)
        {
            return CheckRoleSettingProcess(departmentId, UserId);
        }

        public bool CheckRoleArchive(Guid departmentId)
        {
            return CheckRoleArchive(departmentId, UserId);
        }

        public void InsertPermission(DocumentDepartmentPermissionBO item, string strData)
        {
            if (!string.IsNullOrEmpty(strData))
            {
                var permissions = new List<DocumentDepartmentPermissionBO>();
                var objects = strData.Split(',').ToList();
                if (objects != null && objects.Count() > 0)
                {
                    for (int i = 0; i < objects.Count(); i++)
                    {
                        var name = objects[i].Split('_').ToList();
                        if (name != null && name.Count() == 2)
                        {
                            switch (name[0])
                            {
                                case "Title":
                                    permissions.Add(new DocumentDepartmentPermissionBO
                                    {
                                        DepartmentId = item.DepartmentId,
                                        IsArchive = item.IsArchive,
                                        IsSettingCategory = item.IsSettingCategory,
                                        IsSettingProcess = item.IsSettingProcess,
                                        Description = item.Description,
                                        DepartmentTitleId = new Guid(name[1])

                                    });
                                    break;
                                case "Employee":
                                    permissions.Add(new DocumentDepartmentPermissionBO
                                    {
                                        DepartmentId = item.DepartmentId,
                                        IsArchive = item.IsArchive,
                                        IsSettingCategory = item.IsSettingCategory,
                                        IsSettingProcess = item.IsSettingProcess,
                                        Description = item.Description,
                                        EmployeeId = new Guid(name[1])
                                    });
                                    break;
                            }
                        }
                    }
                }
                base.InsertRange(permissions);
            }
        }

        public void UpdateRecordPermission(Guid id, bool isSettingProcess, bool isSettingCategory, bool isArchive)
        {
            var obj = base.GetById(id);
            obj.IsSettingProcess = isSettingProcess;
            obj.IsArchive = isArchive;
            obj.IsSettingCategory = isSettingCategory;
            base.Update(obj);
        }

        public bool CheckRoleCanUpdateCategory(Guid departmentId)
        {
            var check = Get(i => i.DepartmentId == departmentId && (i.IsArchive == true || i.IsSettingCategory == true)).Any();
            return check;
        }
    }

}