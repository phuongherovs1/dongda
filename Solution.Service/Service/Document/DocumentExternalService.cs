﻿using iDAS.DataAccess;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentExternalService : BaseService<DocumentExternalDTO, DocumentExternalBO>, IDocumentExternalService
    {

        public IEnumerable<DocumentExternalBO> GetContainName(string name)
        {
            var data = Get(i => i.Name.ToUpper().Contains(name.Trim().ToUpper()));
            return data;
        }
        public bool checkExist(string name)
        {
            var existName = Get(i => i.Name.Trim().ToUpper() == name.Trim().ToUpper()).Any();
            return existName;
        }
    }
}
