﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentReferenceService : BaseService<DocumentReferenceDTO, DocumentReferenceBO>, IDocumentReferenceService
    {
        /// <summary>
        /// Lấy danh sách tài liệu tham chiếu theo tài liệu được chọn
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public IEnumerable<Guid> GetByDocument(Guid documentId)
        {
            var data = base.Get(p => p.DocumentId == documentId)
                .Select(i => i.ReferenceId.Value)
                .Distinct();
            return data;
        }

        public void UpdateReference(DocumentReferenceBO data, bool allowSave = true)
        {
            var fileIds = Enumerable.Empty<Guid>();
            if (data != null && data.FileAttachs != null && data.FileAttachs.FileAttachments.Any())
            {
                if (data != null && data.FileAttachs != null)
                {
                    fileIds = ServiceFactory<FileService>().UploadRange(data.FileAttachs, false);
                }
            }
            base.Update(data, false);
            if (data.FileAttachs != null && data.FileAttachs.FileRemoves != null && data.FileAttachs.FileRemoves.Count > 0)
                ServiceFactory<DocumentReferenceAttachmentService>().DeleteRange(data.Id, data.FileAttachs.FileRemoves);

            // Insert đính kèm tương ứng với bảng kết quả
            foreach (var fileId in fileIds)
            {
                var attachment = new DocumentReferenceAttachmentBO()
                {
                    FileId = fileId,
                    ReferenceId = data.Id
                };
                ServiceFactory<DocumentReferenceAttachmentService>().Insert(attachment, false);
            }
            SaveTransaction(allowSave);

        }

        public Guid Save(DocumentReferenceBO reference)
        {
            try
            {
                reference.Id = base.Insert(reference, false);
                if (reference.FileAttachs != null && reference.FileAttachs.FileAttachments != null && reference.FileAttachs.FileAttachments.Count > 0)
                {
                    ServiceFactory<DocumentReferenceAttachmentService>().InsertRange(reference.FileAttachs, reference.Id, false);
                }
                SaveTransaction();
                return reference.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Guid> InsertRange(Guid documentId, string jsonData, bool allowSave = true)
        {
            var result = Enumerable.Empty<Guid>();
            try
            {
                if (!string.IsNullOrEmpty(jsonData))
                {
                    var data = JsonConvert.DeserializeObject<List<DocumentReferenceViewBO>>(jsonData);
                    var references = new List<DocumentReferenceBO>();
                    if (data != null && data.Count > 0)
                    {
                        foreach (var item in data)
                        {
                            var reference = new DocumentReferenceBO();
                            reference.DocumentId = documentId;
                            if (!string.IsNullOrEmpty(item.ReferenceId))
                            {
                                reference.ReferenceId = new Guid(item.ReferenceId);
                            }
                            else
                            {
                                reference.ReferenceName = item.ReferenceName;
                                reference.ReferenceCode = item.ReferenceCode;
                                reference.ReferenceType = string.IsNullOrEmpty(item.ReferenceType) ? (Guid?)null : new Guid(item.ReferenceType);
                                reference.Note = item.Note;
                            }
                            references.Add(reference);
                        }
                    }
                    result = InsertRange(references, allowSave);
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public IEnumerable<Guid> Update(Guid documentId, string jsonData, bool allowSave = true)
        {
            var data = Enumerable.Empty<Guid>();
            try
            {
                if (!string.IsNullOrEmpty(jsonData))
                {
                    // convert string => list Guid
                    var references = JsonConvert.DeserializeObject<List<Guid>>(jsonData);
                    //1. Danh sách  tài liệu tham chiếu theo documentId
                    var documentReferences = Get(i => i.DocumentId == documentId);
                    //2. Danh sách thêm mới có ở json data nhưng không có ở trong database
                    var insertItems = references.Where(i => !documentReferences.Select(u => u.ReferenceId.Value).Contains(i)).Select(
                        i => new DocumentReferenceBO()
                        {
                            DocumentId = documentId,
                            ReferenceId = i
                        });
                    //3. Đưa ra danh sách đã xóa: không có trong jsonData nhưng lại có trong database
                    //4.
                    var referenceDeleteIds = documentReferences.Select(i => i.ReferenceId.Value).Except(references);
                    var deleteItemIds = documentReferences.Where(u => referenceDeleteIds.Contains(u.ReferenceId.Value)).Select(i => i.Id);
                    // var deleteData = Get(i => i.DocumentId == documentId && deleteItems.Contains(i.ReferenceId.Value)).Select(i => i.Id);
                    InsertRange(insertItems, false);
                    DeleteRange(deleteItemIds, false);
                    SaveTransaction(allowSave);
                }
            }
            catch (Exception)
            {
            }
            return data;
        }

        public DocumentReferenceBO GetFormItem(string id, Guid documentId)
        {
            DocumentReferenceBO data;
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    data = new DocumentReferenceBO()
                    {
                        DocumentId = documentId
                    };
                }
                else
                {
                    data = GetById(new Guid(id));
                    var temp = ServiceFactory<DocumentReferenceAttachmentService>().GetFile(data.Id);
                    data.FileAttachs = new FileUploadBO()
                    {
                        Files = temp.ToList()
                    };
                    //var document = ServiceFactory<DocumentService>().GetById(data.DocumentId.Value);
                    //data.ReferenceName = document.Name;
                    //data.ReferenceCode = document.Code;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return data;
        }

        public IEnumerable<DocumentReferenceBO> GetByDocumentId(Guid documentId, int pageIndex, int pageSize, out int count)
        {
            var data = Get(i => i.DocumentId == documentId);
            count = data.Count();
            data = Page(data, pageIndex, pageSize);
            var referenceIds = data.Where(i => i.ReferenceId.HasValue).Select(i => i.ReferenceId.Value);
            var references = ServiceFactory<DocumentService>().GetByIds(referenceIds);
            var documentAttachments = ServiceFactory<DocumentAttachmentService>().Get(i => referenceIds.Contains(i.DocumentId.Value));
            var attachments = ServiceFactory<DocumentReferenceAttachmentService>().GetByReferenceIds(data.Select(u => u.Id));
            data = data.Select(i =>
                {
                    if (i.ReferenceId.HasValue && i.ReferenceId != Guid.Empty)
                    {
                        var refer = references.FirstOrDefault(u => i.ReferenceId == u.Id);
                        if (refer != null && refer.CategoryId.HasValue)
                        {
                            var category = ServiceFactory<DocumentCategoryService>().GetById(refer.CategoryId.Value);
                            i.ReferenceTypeName = category.Name;
                        }
                        i.ReferenceName = refer.Name;
                        i.ReferenceCode = refer.Code;
                        var fileIds = documentAttachments.Where(u => u.DocumentId.Value == i.ReferenceId.Value).Select(u => u.FileId.Value);
                        i.FileAttachs = new FileUploadBO()
                        {
                            Files = fileIds.ToList()
                        };
                    }
                    else
                    {
                        if (i.ReferenceType.HasValue)
                        {
                            var type = ServiceFactory<DocumentReferenceTypeService>().GetById(i.ReferenceType.Value);
                            i.ReferenceTypeName = type.Name;
                        }
                        var fileIds = attachments.Where(u => u.ReferenceId == i.Id).Select(u => u.FileId.Value);
                        i.FileAttachs = new FileUploadBO()
                        {
                            Files = fileIds.ToList()
                        };
                    }
                    return i;
                });
            return data;
        }

        public IEnumerable<DocumentReferenceBO> GetByDocumentIds(IEnumerable<Guid> documentIds)
        {
            var data = GetQuery().Where(i => i.DocumentId.HasValue && documentIds.Contains(i.DocumentId.Value));
            return data;
        }

        public IEnumerable<Guid> InsertBySelectDocument(List<Guid> selectedDocumentIds, Guid documentId)
        {
            var data = selectedDocumentIds.Select(i => new DocumentReferenceBO()
            {
                DocumentId = documentId,
                ReferenceId = i
            });
            var result = InsertRange(data);
            return result;
        }
    }
}
