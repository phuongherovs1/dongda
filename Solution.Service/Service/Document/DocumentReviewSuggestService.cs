﻿namespace iDAS.Service
{
    //public class DocumentReviewSuggestService : BaseService<DocumentReviewSuggestDTO, DocumentReviewSuggestBO>, IDocumentReviewSuggestService
    //{
    //    /// <summary>
    //    /// Lấy ra đề nghị kiểm duyệt theo yêu cầu soạn thảo
    //    /// </summary>
    //    /// <param name="requestId"></param>
    //    /// <returns></returns>
    //    public DocumentReviewSuggestBO GetByRequestId(Guid requestId)
    //    {
    //        var data = Get(i => i.RequestId == requestId).FirstOrDefault();
    //        return data;
    //    }
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="review"></param>
    //    public void Approve(DocumentReviewSuggestBO item)
    //    {
    //        item.IsApprove = true;
    //        base.Update(item);
    //    }
    //    /// <summary>
    //    /// Duyệt đề nghị kiểm duyệt
    //    /// View Kiểm duyệt
    //    /// </summary>
    //    /// <returns></returns>
    //    public int CountSuggestReview()
    //    {
    //        var summary = Get(i => i.ApproveBy == UserId && i.IsApprove != true).Count();
    //        return summary;
    //    }
    //    /// <summary>
    //    /// Danh sách đề nghị gửi đi
    //    /// </summary>
    //    /// <param name="pageIndex"></param>
    //    /// <param name="pageSize"></param>
    //    /// <param name="count"></param>
    //    /// <param name="approveStatus"></param>
    //    /// <returns></returns>
    //    public IEnumerable<DocumentReviewSuggestBO> GetAll(int pageIndex, int pageSize, out int count, Resource.ApproveStatus approveStatus)
    //    {
    //        var data = GetAll().Where(p => p.CreateBy == UserId);
    //        count = data.Count();
    //        data = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
    //        foreach (var item in data)
    //        {
    //            if (item.ApproveBy.HasValue)
    //            {
    //                var employee = ServiceFactory<EmployeeService>().GetById(item.ApproveBy.Value);
    //                item.ApproveBy = employee.Id;
    //                item.EmployeeApproveBy = employee;
    //                item.Approver = employee.Name;
    //                item.ApproverAvatarUrl = employee.AvatarUrl;
    //            }
    //            if (item.RequestId.HasValue)
    //            {
    //                var request = ServiceFactory<DocumentRequestService>().GetById(item.RequestId.Value);
    //                item.Document = request.Document;
    //            }
    //            item.ResultReview = item.IsApprove == true && item.IsAccept == true ? "Đạt" : item.IsApprove == true && item.IsAccept == false ? "Không đạt" : "";
    //            // Test Trách nhiệm
    //            item.TextRole = item.IsSend == true ? "Kiểm tra" : "Phê duyệt";
    //        }
    //        return data;
    //    }
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="requestId"></param>
    //    /// <param name="id"></param>
    //    /// <returns></returns>
    //    public DocumentReviewSuggestBO CreateDefault(Guid? requestId, string id)
    //    {
    //        var data = new DocumentReviewSuggestBO();
    //        var request = new DocumentRequestBO();
    //        try
    //        {
    //            if (!string.IsNullOrEmpty(id))
    //            {
    //                data = GetById(new Guid(id));
    //                // Hiển thị tên người phê duyệt
    //                if (data.ApproveBy.HasValue && data.ApproveBy != Guid.Empty)
    //                {
    //                    var employeeId = data.ApproveBy;
    //                    data.EmployeeApproveBy = base.GetEmployee(employeeId.Value);
    //                }
    //                if (data.RequestId != null)
    //                {
    //                    request = ServiceFactory<DocumentRequestService>().GetById(data.RequestId.Value);
    //                    data.Document = request == null ? null : request.Document;
    //                    if (data.Document != null)
    //                    {
    //                        // Lấy file đính kèm
    //                        var temp = ServiceFactory<DocumentAttachmentService>().GetByDocument(data.Document.Id);
    //                        data.Document.FileAttachs = new FileUploadBO()
    //                        {
    //                            Files = temp.ToList()
    //                        };
    //                        // Người soạn thảo
    //                        if (data.Document.WriteBy.HasValue)
    //                        {
    //                            var employeeWrite = ServiceFactory<EmployeeService>().GetById(data.Document.CreateBy.Value);
    //                            data.EmployeeWriteBy = employeeWrite;
    //                        }
    //                    }
    //                }
    //                else
    //                {
    //                    data.Document = new DocumentBO();
    //                }
    //            }
    //            else
    //            {
    //                //1. Thông tin yêu cầu tài liệu
    //                request = ServiceFactory<DocumentRequestService>().GetById(requestId.Value);
    //                data.Document = request == null ? null : request.Document;
    //                // Thực hiện chuyển trạng thái của tài liệu từ mới sang đang soạn thảo
    //                ServiceFactory<DocumentService>().Perform(data.Document.Id);
    //            }
    //        }
    //        catch (Exception)
    //        {
    //            throw;
    //        }

    //        return data;
    //    }
    //    /// <summary>
    //    /// Gửi đề nghị kiểm duyệt
    //    /// </summary>
    //    /// <param name="item"></param>
    //    /// <returns></returns>
    //    public Guid Send(DocumentReviewSuggestBO item, string references)
    //    {
    //        try
    //        {
    //            item.IsSend = true;
    //            Save(item, references);
    //        }
    //        catch (Exception)
    //        {
    //            throw;
    //        }
    //        return item.Id;
    //    }
    //    /// <summary>
    //    /// Thu hồi đề nghị kiểm duyệt
    //    /// </summary>
    //    /// <param name="requestId"></param>
    //    public void Revert(Guid id)
    //    {
    //        var suggest = GetById(id);
    //        suggest.IsSend = false;
    //        base.Update(suggest);
    //    }
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="item"></param>
    //    /// <param name="references"></param>
    //    /// <returns></returns>
    //    public Guid Save(DocumentReviewSuggestBO item, string references)
    //    {
    //        try
    //        {
    //            //1. Cập nhật thông tin của tài liệu
    //            ServiceFactory<DocumentService>().Update(item.Document, references);
    //            //2. Cập nhật tài liệu tham chiếu
    //            //ServiceFactory<DocumentReferenceService>().Update(item.Document.Id, references);
    //            //3. 
    //            item.ApproveBy = item.EmployeeApproveBy.Id;
    //            if (item.Id == Guid.Empty)
    //            {
    //                //var documentId = ServiceFactory<DocumentService>().Insert(item.Document, false);
    //                item.Id = Insert(item);
    //            }
    //            else
    //            {
    //                Update(item);
    //            }
    //        }
    //        catch (Exception)
    //        {
    //            throw;
    //        }
    //        return item.Id;
    //    }

    //    /// <summary>
    //    /// Danh sách đề nghị kiểm duyệt gửi đến người đang đăng nhập hệ thống
    //    /// </summary>
    //    /// <param name="pageIndex"></param>
    //    /// <param name="pageSize"></param>
    //    /// <param name="count"></param>
    //    /// <param name="approveStatus"></param>
    //    /// <returns></returns>
    //    public IEnumerable<DocumentReviewSuggestBO> GetByApprover(int pageIndex, int pageSize, out int count, Resource.ApproveStatus approveStatus = Resource.ApproveStatus.All)
    //    {
    //        //1. Hiển thị danh sách với các điều kiện
    //        var data = Get(i => i.ApproveBy == UserId && i.IsSend == true)
    //            .Where(i => approveStatus == Resource.ApproveStatus.Wait ? (i.IsSend == true && i.IsApprove != true)
    //                : approveStatus == Resource.ApproveStatus.Approve ? (i.IsApprove == true && i.IsAccept == true)
    //                : approveStatus == Resource.ApproveStatus.Reject ? (i.IsApprove == true && i.IsAccept != true)
    //                : true);
    //        //2. Đếm số lượng kết quả
    //        count = data.Count();
    //        //3. Phân trang dữ liệu
    //        data = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
    //        foreach (var item in data)
    //        {
    //            //4. Hiển thị tên người đề nghị
    //            var creator = ServiceFactory<EmployeeService>().GetById(item.CreateBy.Value);
    //            item.SuggestorName = creator.Name;
    //            if (item.RequestId != null)
    //            {
    //                // Lấy thông tin tài liệu
    //                var request = ServiceFactory<DocumentRequestService>().GetById(item.RequestId.Value);
    //                item.Document = request == null ? null : request.Document;
    //            }
    //        }
    //        return data;
    //    }
    //    public void ApproveSuggest(DocumentReviewSuggestBO item)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}