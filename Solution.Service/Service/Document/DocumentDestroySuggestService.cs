﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentDestroySuggestService : BaseService<DocumentDestroySuggestDTO, DocumentDestroySuggestBO>, IDocumentDestroySuggestService
    {
        /// <summary>
        /// Danh sách đề nghị hủy
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="isApproveRole">thực hiện load dữ liệu với quyền người đang đăng nhập
        /// True (Người với quyền phê duyệt): Danh sách đề nghị gửi đến người đó
        /// False (Người đề nghị): Danh sách đề nghị người đó tạo ra
        /// </param>
        /// <param name="status"></param>
        /// <returns></returns>
        public IEnumerable<DocumentDestroySuggestResponsibitityBO> GetAll(int pageIndex, int pageSize, out int count, Resource.DocumentSuggestStatusType status = Common.Resource.DocumentSuggestStatusType.All,
            Common.Resource.DocumentProcessRoleFilterSuggest roleType = Common.Resource.DocumentProcessRoleFilterSuggest.All)
        {
            try
            {
                var destroySuggest = Get(i => (status == Resource.DocumentSuggestStatusType.All || i.StatusType == (int)status) && i.IsDelete == false);
                var resposibily = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetByCurrentUser()
                                .Where(i => (i.RoleType == (int)Resource.DocumentProcessRoleFilterSuggest.DestroyCreater) && i.IsNew != false && i.IsDelete == false
                                            && i.DocumentDestroyId.HasValue
                                            && destroySuggest.Select(w => w.Id).Contains(i.DocumentDestroyId.Value));
                count = resposibily.Count();
                var data = resposibily.Skip((pageIndex - 1) * pageSize).Take(pageSize);
                foreach (var i in data)
                {
                    // Lấy những tài liệu hủy của đề nghị
                    var destroyDocument = ServiceFactory<DocumentDestroySuggestDetailService>().GetByDestroySuggestId(i.DocumentDestroyId.Value);
                    var item = destroySuggest.Where(p => p.Id == i.DocumentDestroyId.Value).FirstOrDefault();
                    item.IsNew = i.IsNew;
                    item.IsAccept = i.IsAccept;
                    item.IsApprove = i.IsApprove;
                    // Đếm số tài liệu hủy
                    item.CountDocument = destroyDocument.Count();
                    i.DestroySuggest = item;
                    // Lấy danh sách DocumentId của đề nghị hủy
                    var details = destroyDocument.Select(p => p.DocumentId.Value);
                    if (details != null && details.Count() > 0)
                    {
                        i.DocumentIds = details.ToList();
                    }
                }

                return data;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Thêm mới đề nghị hủy
        /// </summary>
        /// <param name="item"></param>
        /// <param name="documentIds"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid Create(DocumentDestroySuggestBO data, string documentIds, bool allowSave = true)
        {
            try
            {
                Guid result = Guid.Empty;
                data.ApproveBy = data.EmployeeApprove.Id;
                data.RoleType = data.EmployeeApprove.RoleType;
                if (data.Id == null || data.Id == Guid.Empty)
                {
                    data.StatusType = (int)Resource.DocumentSuggestStatusType.New;
                    data.Id = base.Insert(data, false);
                }
                else
                    base.Update(data, false);
                // Kiểm tra nếu đề nghị chỉ lưu chưa gửi xem xét thì cập nhật lại vai trò
                var item = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetByDestroyId((Guid)data.Id)
                            .Where(p => p.DepartmentId == data.DepartmentId && p.RoleType == (int)Common.Resource.DocumentProcessRole.DestroyCreater
                            && p.IsNew == true && p.IsComplete != true && p.IsLock != true).FirstOrDefault();
                if (item != null)
                {
                    item.IsNew = true;
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Update(item, false);
                }
                else // Ngược lại thì thêm mới
                {
                    // Thêm mới vai trò
                    var responsibility = new DocumentDestroySuggestResponsibitityBO()
                    {
                        DepartmentId = data.DepartmentId,
                        DocumentDestroyId = data.Id,
                        RoleType = (int)Common.Resource.DocumentProcessRole.DestroyCreater,
                        IsNew = true,
                        ApproveBy = UserId,
                        Order = 0
                    };
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Insert(responsibility, false);
                }
                // cập nhật bước tiếp theo nếu đề nghị chỉ mới lưu
                var updateResponsibility = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetByDestroyId((Guid)data.Id)
                                            .Where(p => p.DepartmentId == data.DepartmentId && p.RoleType == (int)data.RoleType
                                            && p.IsNew == false && p.IsComplete != true && p.IsLock != true).FirstOrDefault();
                if (updateResponsibility != null)
                {
                    updateResponsibility.IsNew = true;
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Update(updateResponsibility, false);
                }
                else // Ngược lại thì tạo mới
                {
                    var nextResponsibility = new DocumentDestroySuggestResponsibitityBO()
                    {
                        RoleType = data.RoleType,
                        DepartmentId = data.DepartmentId,
                        DocumentDestroyId = data.Id,
                        IsNew = data.Temp,
                        ApproveBy = data.ApproveBy,
                        Order = data.EmployeeApprove.NextOrder
                    };
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Insert(nextResponsibility, false);
                }
                if (!String.IsNullOrEmpty(documentIds))// Chọn nhiều tài liệu
                {
                    // Cập nhật danh sách tài liệu đề nghị hủy
                    ServiceFactory<DocumentDestroySuggestDetailService>().InsertRange(documentIds, data.Id);
                }
                else // Tạo đề nghị hủy từ 1 tài liệu
                {
                    var detail = new DocumentDestroySuggestDetailBO()
                    {
                        DocumentId = data.Document.Id,
                        DestroySuggestId = data.Id
                    };
                    // Xóa những tài liệu của đề nghị
                    var ids = ServiceFactory<DocumentDestroySuggestDetailService>().Get(p => p.DestroySuggestId == data.Id).Select(p => p.Id);
                    ServiceFactory<DocumentDestroySuggestDetailService>().DeleteRange(ids, allowSave);
                    ServiceFactory<DocumentDestroySuggestDetailService>().Insert(detail, false);
                }
                SaveTransaction(allowSave);
                result = data.Id;
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Lưu thông tin đề nghị hủy
        /// </summary>
        /// <param name="data"></param>
        /// <param name="documentIds"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid SaveDestroySuggest(DocumentDestroySuggestBO data, string documentIds, bool allowSave = true)
        {
            try
            {
                Guid result = Guid.Empty;
                data.ApproveBy = data.EmployeeApproveBy.Id;
                if (data.Id == null || data.Id == Guid.Empty)
                {
                    data.StatusType = (int)Resource.DocumentSuggestStatusType.New;
                    data.Id = base.Insert(data, false);
                }
                else
                    base.Update(data, false);
                // Kiểm tra nếu đề nghị chỉ lưu chưa gửi xem xét thì cập nhật lại vai trò
                var item = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetByDestroyId((Guid)data.Id)
                            .Where(p => p.DepartmentId == data.DepartmentId && p.RoleType == (int)Common.Resource.DocumentProcessRole.DestroyCreater
                            && p.IsNew == true && p.IsComplete != true && p.IsLock != true).FirstOrDefault();
                if (item != null)
                {
                    item.IsNew = true;
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Update(item, false);
                }
                else // Ngược lại thì thêm mới
                {
                    // Thêm mới vai trò
                    var responsibility = new DocumentDestroySuggestResponsibitityBO()
                    {
                        DepartmentId = data.DepartmentId,
                        DocumentDestroyId = data.Id,
                        RoleType = (int)Common.Resource.DocumentProcessRole.DestroyCreater,
                        IsNew = true,
                        ApproveBy = UserId,
                        Order = 0,
                        IsArchiver = true
                    };
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Insert(responsibility, false);
                }
                // cập nhật bước tiếp theo nếu đề nghị chỉ mới lưu
                var updateResponsibility = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetByDestroyId((Guid)data.Id)
                                            .Where(p => p.DepartmentId == data.DepartmentId && p.RoleType == (int)data.RoleType
                                            && p.IsNew == false && p.IsComplete != true && p.IsLock != true).FirstOrDefault();
                if (updateResponsibility != null)
                {
                    updateResponsibility.IsNew = true;
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Update(updateResponsibility, false);
                }
                else // Ngược lại thì tạo mới
                {
                    var nextResponsibility = new DocumentDestroySuggestResponsibitityBO()
                    {
                        RoleType = data.RoleType,
                        DepartmentId = data.DepartmentId,
                        DocumentDestroyId = data.Id,
                        IsNew = data.Temp,
                        ApproveBy = data.ApproveBy,
                        Order = 0,
                        IsArchiver = true
                    };
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Insert(nextResponsibility, false);
                }
                if (!String.IsNullOrEmpty(documentIds))// Chọn nhiều tài liệu
                {
                    // Cập nhật danh sách tài liệu đề nghị hủy
                    ServiceFactory<DocumentDestroySuggestDetailService>().InsertRange(documentIds, data.Id);
                }
                else // Tạo đề nghị hủy từ 1 tài liệu
                {
                    var detail = new DocumentDestroySuggestDetailBO()
                    {
                        DocumentId = data.Document.Id,
                        DestroySuggestId = data.Id
                    };
                    // Xóa những tài liệu của đề nghị
                    var ids = ServiceFactory<DocumentDestroySuggestDetailService>().Get(p => p.DestroySuggestId == data.Id).Select(p => p.Id);
                    ServiceFactory<DocumentDestroySuggestDetailService>().DeleteRange(ids, allowSave);
                    ServiceFactory<DocumentDestroySuggestDetailService>().Insert(detail, false);
                }
                SaveTransaction(allowSave);
                result = data.Id;
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Gửi duyệt đề nghị
        /// </summary>
        /// <param name="data"></param>
        /// <param name="documentIds"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid Send(DocumentDestroySuggestBO data, string documentIds, bool allowSave = true)
        {
            try
            {
                Guid result = Guid.Empty;
                data.ApproveBy = data.EmployeeApproveBy.Id;
                //data.RoleType = data.EmployeeApprove.RoleType;
                if (data.RoleType == (int)Common.Resource.DocumentProcessRole.Check)
                    data.StatusType = (int)Resource.DocumentSuggestStatusType.Review;
                else if (data.RoleType == (int)Common.Resource.DocumentProcessRole.ApproveDocument)
                    data.StatusType = (int)Resource.DocumentSuggestStatusType.Approve;
                else
                    data.StatusType = (int)Resource.DocumentSuggestStatusType.New;

                if (data.Id == null || data.Id == Guid.Empty)
                    data.Id = base.Insert(data, false);
                else
                    base.Update(data, false);
                // Kiểm tra nếu đề nghị chỉ lưu chưa gửi xem xét thì cập nhật lại vai trò
                var item = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetByDestroyId((Guid)data.Id)
                            .Where(p => p.DepartmentId == data.DepartmentId && p.RoleType == (int)Common.Resource.DocumentProcessRole.DestroyCreater
                            && p.IsNew == true && p.IsComplete != true && p.IsLock != true).FirstOrDefault();
                if (item != null)
                {
                    item.IsNew = true;
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Update(item, false);
                }
                else // Ngược lại thì thêm mới
                {
                    // Thêm mới vai trò
                    var responsibility = new DocumentDestroySuggestResponsibitityBO()
                    {
                        DepartmentId = data.DepartmentId,
                        DocumentDestroyId = data.Id,
                        RoleType = (int)Common.Resource.DocumentProcessRole.DestroyCreater,
                        IsNew = data.Temp,
                        ApproveBy = UserId,
                        Order = 0,
                        IsArchiver = true
                    };
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Insert(responsibility, false);
                }
                // cập nhật bước tiếp theo nếu đề nghị chỉ mới lưu
                var updateResponsibility = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetByDestroyId((Guid)data.Id)
                                            .Where(p => p.DepartmentId == data.DepartmentId && p.RoleType == (int)data.RoleType
                                            && p.IsNew == false && p.IsComplete != true && p.IsLock != true).FirstOrDefault();
                if (updateResponsibility != null)
                {
                    updateResponsibility.IsNew = true;
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Update(updateResponsibility, false);
                }
                else // Ngược lại thì tạo mới
                {
                    var nextResponsibility = new DocumentDestroySuggestResponsibitityBO()
                    {
                        RoleType = data.RoleType,
                        DepartmentId = data.DepartmentId,
                        DocumentDestroyId = data.Id,
                        IsNew = data.Temp,
                        ApproveBy = data.ApproveBy,
                        Order = 0,
                        IsArchiver = true
                    };
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Insert(nextResponsibility, false);
                }
                if (!String.IsNullOrEmpty(documentIds))// Chọn nhiều tài liệu
                {
                    // Cập nhật danh sách tài liệu đề nghị hủy
                    ServiceFactory<DocumentDestroySuggestDetailService>().InsertRange(documentIds, data.Id);
                }
                else // Tạo đề nghị hủy từ 1 tài liệu
                {
                    var detail = new DocumentDestroySuggestDetailBO()
                    {
                        DocumentId = data.Document.Id,
                        DestroySuggestId = data.Id
                    };
                    // Xóa những tài liệu của đề nghị
                    var ids = ServiceFactory<DocumentDestroySuggestDetailService>().Get(p => p.DestroySuggestId == data.Id).Select(p => p.Id);
                    ServiceFactory<DocumentDestroySuggestDetailService>().DeleteRange(ids, allowSave);
                    ServiceFactory<DocumentDestroySuggestDetailService>().Insert(detail, false);
                }
                SaveTransaction(allowSave);
                result = data.Id;
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="documentIds"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>

        public Guid SendSuggestDestroy(DocumentDestroySuggestBO data, bool allowSave = true)
        {
            try
            {
                Guid result = Guid.Empty;
                data.ApproveBy = data.EmployeeApprove.Id;
                data.RoleType = data.EmployeeApprove.RoleType;
                if (data.RoleType == (int)Common.Resource.DocumentProcessRole.Check)
                    data.StatusType = (int)Resource.DocumentSuggestStatusType.Review;
                else if (data.RoleType == (int)Common.Resource.DocumentProcessRole.ApproveDocument)
                    data.StatusType = (int)Resource.DocumentSuggestStatusType.Approve;
                else if (data.RoleType == (int)Common.Resource.DocumentProcessRole.Promulgate)
                    data.StatusType = (int)Resource.DocumentSuggestStatusType.WaitDestroy;
                else
                    data.StatusType = (int)Resource.DocumentSuggestStatusType.New;

                if (data.Id == null || data.Id == Guid.Empty)
                    data.Id = base.Insert(data, false);
                else
                    base.Update(data, false);
                // Kiểm tra nếu đề nghị chỉ lưu chưa gửi xem xét thì cập nhật lại vai trò
                var item = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetByDestroyId((Guid)data.Id)
                            .Where(p => p.DepartmentId == data.DepartmentId && p.RoleType == (int)Common.Resource.DocumentProcessRole.DestroyCreater
                            && p.IsNew == true && p.IsComplete != true && p.IsLock != true).FirstOrDefault();
                if (item != null)
                {
                    item.IsNew = true;
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Update(item, false);
                }
                else // Ngược lại thì thêm mới
                {
                    // Thêm mới vai trò
                    var responsibility = new DocumentDestroySuggestResponsibitityBO()
                    {
                        DepartmentId = data.DepartmentId,
                        DocumentDestroyId = data.Id,
                        RoleType = (int)Common.Resource.DocumentProcessRole.DestroyCreater,
                        IsNew = data.Temp,
                        ApproveBy = UserId,
                        Order = 0
                    };
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Insert(responsibility, false);
                }
                // cập nhật bước tiếp theo nếu đề nghị chỉ mới lưu
                var updateResponsibility = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetByDestroyId((Guid)data.Id)
                                            .Where(p => p.DepartmentId == data.DepartmentId && p.RoleType == (int)data.RoleType
                                            && p.IsNew == false && p.IsComplete != true && p.IsLock != true).FirstOrDefault();
                if (updateResponsibility != null)
                {
                    updateResponsibility.IsNew = true;
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Update(updateResponsibility, false);
                }
                else // Ngược lại thì tạo mới
                {
                    var nextResponsibility = new DocumentDestroySuggestResponsibitityBO()
                    {
                        RoleType = data.RoleType,
                        DepartmentId = data.DepartmentId,
                        DocumentDestroyId = data.Id,
                        IsNew = data.Temp,
                        ApproveBy = data.ApproveBy,
                        Order = data.EmployeeApprove.NextOrder
                    };
                    ServiceFactory<DocumentDestroySuggestResponsibitityService>().Insert(nextResponsibility, false);
                }// Tạo đề nghị hủy từ 1 tài liệu
                var detail = new DocumentDestroySuggestDetailBO()
                {
                    DocumentId = data.Document.Id,
                    DestroySuggestId = data.Id
                };
                // Xóa những tài liệu của đề nghị
                var ids = ServiceFactory<DocumentDestroySuggestDetailService>().Get(p => p.DestroySuggestId == data.Id).Select(p => p.Id);
                ServiceFactory<DocumentDestroySuggestDetailService>().DeleteRange(ids, allowSave);
                ServiceFactory<DocumentDestroySuggestDetailService>().Insert(detail, false);
                SaveTransaction(allowSave);
                result = data.Id;
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="allowDeleted"></param>
        /// <param name="allowDefaultIfNull"></param>
        /// <returns></returns>
        public override DocumentDestroySuggestBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id);
            if (data != null)
            {
                if (data.ApproveBy.HasValue)
                {
                    var employeeApprove = data.ApproveBy.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.ApproveBy.Value) : null;
                    var documentEmployee = new DocumentEmployeeBO();
                    documentEmployee.DepartmentId = data.DepartmentId.Value;
                    documentEmployee.Process = (int)Resource.DocumentTypeProcess.CancelSuggest;
                    documentEmployee.RoleType = data.RoleType.Value;
                    documentEmployee.AvatarUrl = employeeApprove.AvatarUrl;
                    documentEmployee.Id = employeeApprove.Id;
                    documentEmployee.Name = employeeApprove.Name;
                    var title = ServiceFactory<EmployeeTitleService>().Get(p => p.EmployeeId == data.ApproveBy.Value).FirstOrDefault();
                    if (title != null)
                    {
                        var result = ServiceFactory<DepartmentTitleService>().GetById(title.TitleId);
                        documentEmployee.RoleNames = result == null ? string.Empty : result.Name;
                    }
                    data.EmployeeApprove = documentEmployee;
                }
            }
            return data;
        }

        /// <summary>
        /// Chức năng thu hồi đề nghị thực hiện trên danh sách đề nghị
        /// </summary>
        /// <param name="destroySuggestId"></param>
        public void Revert(Guid destroySuggestId)
        {
            try
            {
                var data = base.GetById(destroySuggestId);
                //data.IsSend = false;
                base.Update(data, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Lấy đề nghị hủy theo ResponsibilityId
        /// </summary>
        /// <param name="responsibilityId"></param>
        /// <returns></returns>
        public DocumentDestroySuggestBO GetByResponsibilityId(string responsibilityId)
        {
            try
            {
                var responsibility = ServiceFactory<DocumentDestroySuggestResponsibitityService>().GetById(new Guid(responsibilityId));
                var destroySuggest = GetById(responsibility.DocumentDestroyId.Value);
                // Lấy thông tin phê duyệt gần nhất đã hoàn thành
                var resultResponsibility = ServiceFactory<DocumentDestroySuggestResponsibitityService>().Get(p => p.DocumentDestroyId == responsibility.DocumentDestroyId.Value
                                            && p.Id != responsibility.Id && p.DepartmentId == responsibility.DepartmentId.Value);
                if (resultResponsibility != null && resultResponsibility.Count() > 0)
                {
                    var approve = resultResponsibility.Where(p => p.IsComplete == true).OrderByDescending(p => p.CreateAt).FirstOrDefault();
                    if (approve != null)
                    {
                        destroySuggest.Role = approve.RoleText.ToLower();
                        destroySuggest.IsNew = approve.IsNew;
                        destroySuggest.IsAccept = approve.IsAccept;
                        destroySuggest.IsApprove = approve.IsApprove;
                        destroySuggest.ReasonOfReject = approve.ApproveNote;
                        destroySuggest.IsArchiver = approve.IsArchiver;
                    }

                    // Lấy thông tin vai trò người đề nghị đã gửi
                    var responsibilitySend = resultResponsibility.OrderBy(p => p.CreateAt).FirstOrDefault();
                    destroySuggest.RoleType = responsibilitySend.RoleType;
                    destroySuggest.IsArchiver = responsibilitySend.IsArchiver;
                    destroySuggest.IsShowButtonRevert = responsibility.IsApprove;
                    destroySuggest.EmployeeApproveBy = responsibilitySend.ApproveBy.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(responsibilitySend.ApproveBy.Value) : null;
                }

                // Lấy thông tin người nhận
                //var employee = destroySuggest != null ? ServiceFactory<EmployeeService>().GetById(responsibilityCheck.ApproveBy.Value) : null;
                //destroySuggest.EmployeeApproveDetail = new DocumentEmployeeBO()
                //{
                //    DepartmentId = responsibilityCheck.DepartmentId.Value,
                //    Process = (int)Resource.DocumentTypeProcess.CancelSuggest,
                //    RoleType = responsibilityCheck.RoleType.Value,
                //    AvatarUrl = employee.AvatarUrl,
                //    Id = employee.Id,
                //    Name = employee.Name,

                //    //Id = employee.Id,
                //    //RoleType = responsibility.RoleType.Value,
                //    //Name = employee.Name,
                //    //AvatarUrl = employee.AvatarUrl,
                //};

                // Lấy thông tin phòng ban đề nghị
                if (responsibility.DepartmentId.HasValue)
                {
                    var department = ServiceFactory<DepartmentService>().GetById(responsibility.DepartmentId.Value);
                    destroySuggest.DepartmentName = department == null ? string.Empty : department.Name;
                }

                var detail = ServiceFactory<DocumentDestroySuggestDetailService>().Get(i => i.DestroySuggestId.Value == destroySuggest.Id);
                destroySuggest.CountDocument = detail.Count();
                if (detail != null && detail.Count() == 1)
                {
                    if (detail.FirstOrDefault().DocumentId.HasValue)
                    {
                        var document = ServiceFactory<DocumentService>().GetById(detail.FirstOrDefault().DocumentId.Value);
                        document.DepartmentId = responsibility.DepartmentId.Value;
                        destroySuggest.Document = document == null ? null : document;
                    }
                }
                return destroySuggest;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Khởi tạo đối tượng mặc định
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public DocumentDestroySuggestBO CreateDefault(string documentId, string departmentId)
        {
            try
            {
                var model = new DocumentDestroySuggestBO()
                {
                    Document = new DocumentBO(),
                    Id = Guid.Empty
                };
                if (!String.IsNullOrEmpty(documentId))
                {
                    var detail = ServiceFactory<DocumentDestroySuggestDetailService>().Get(p => p.DocumentId == new Guid(documentId)).FirstOrDefault();
                    var document = ServiceFactory<DocumentService>().GetById(new Guid(documentId));
                    if (!document.DepartmentId.HasValue)
                    {
                        if (document.CategoryId.HasValue)
                        {
                            var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
                            var department = category.DepartmentId.HasValue ? ServiceFactory<DepartmentService>().GetById(category.DepartmentId.Value) : null;
                            document.DepartmentId = department == null ? Guid.Empty : department.Id;
                            document.DepartmentName = department == null ? string.Empty : department.Name;
                        }
                    }
                    if (detail == null)
                        model.Document = document == null ? new DocumentBO() : document;
                    else
                    {
                        var destroySuggest = base.GetById(detail.DestroySuggestId.Value);
                        model = destroySuggest;
                        model.Document = document == null ? new DocumentBO() : document;
                    }
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Ẩn button thêm mới đề nghị hủy khi người đăng nhập là người phê duyệt theo quy trình, ngược lại thì hiển thị
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public bool ShowOrHideButtonCreate(string departmentId)
        {
            try
            {
                if (!String.IsNullOrEmpty(departmentId))
                {
                    if (departmentId.Contains('_'))
                    {
                        var items = departmentId.Split('_').ToList();
                        if (items != null && items.Count == 2)
                        {
                            return ServiceFactory<DocumentProcessService>().CheckExitEmployeeInProcess(new Guid(items[1]), UserId, (int)Resource.DocumentTypeProcess.CancelSuggest,
                                    (int)Resource.DocumentProcessRole.DestroyApproval);
                        }
                    }
                    else
                    {
                        return ServiceFactory<DocumentProcessService>().CheckExitEmployeeInProcess(new Guid(departmentId), UserId, (int)Resource.DocumentTypeProcess.CancelSuggest,
                                    (int)Resource.DocumentProcessRole.DestroyApproval);
                    }
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
