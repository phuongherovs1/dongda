﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentRequestService : BaseService<DocumentRequestDTO, DocumentRequestBO>, IDocumentRequestService
    {
        public override DocumentRequestBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id, allowDeleted, allowDefaultIfNull);
            if (data != null)
            {
                data.Document = ServiceFactory<DocumentService>().GetById(data.DocumentId.HasValue ? data.DocumentId.Value : Guid.Empty);
                data.EmployeeCreateBy = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.CreateBy.HasValue ? data.CreateBy.Value : Guid.Empty);
                data.EmployeeApproveBy = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.ApproveBy.HasValue ? data.ApproveBy.Value : Guid.Empty);
                data.EmployeePerformBy = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy.HasValue ? data.PerformBy.Value : Guid.Empty);
            }
            return data;
        }
        /// <summary>
        /// Ghi đề hàm Insert
        /// Lưu ý: Employee Perform By là người thực hiện, không được phép NULL
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public override Guid Insert(iDAS.Service.DocumentRequestBO data, bool allowSave = true)
        {
            try
            {
                data.ApproveBy = UserId;
                data.IsApprove = true;
                data.IsAccept = true;
                data.PerformBy = data.EmployeePerform.Id;
                return base.Insert(data, allowSave);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Danh sách yêu cầu biên soạn/soạn thảo thực hiện bởi người đăng nhập hệ thống
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="status"></param>
        /// <param name="typeWrite">True: Soạn thảo, False: biên soạn</param>
        /// <returns></returns>
        public IEnumerable<DocumentRequestBO> GetPeformByCurrentUser(int pageIndex, int pageSize, out int count, Resource.RequestStatus status = Resource.RequestStatus.All, bool typeWrite = true, bool isAdjust = false)
        {
            var data = Enumerable.Empty<DocumentRequestBO>();
            try
            {
                //1. 
                var filterParent = Enumerable.Empty<DocumentRequestBO>();
                if (typeWrite)
                {
                    //2.
                    filterParent = Get(i => i.TypeWrite != true && i.IsSend == true && i.PerformBy == UserId
                          && (status == Resource.RequestStatus.New ? (i.IsPerform != true && i.IsComplete != true && i.IsFinish != true)
                          : status == Resource.RequestStatus.Perform ? i.IsPerform == true
                          : status == Resource.RequestStatus.Complete ? i.IsComplete == true
                          : status == Resource.RequestStatus.Finish ? i.IsFinish == true
                          : true));
                    //3.
                    data = Get(i => i.TypeWrite == true && i.IsSend == true && i.ParentId.HasValue && filterParent.Select(u => u.Id).Contains(i.ParentId.Value));
                }
                else
                {
                    data = Get(i => i.TypeWrite != true && i.PerformBy == UserId
                        && (isAdjust == true ? i.TypeAdjust == true : i.TypeAdjust != true)
                        && (status == Resource.RequestStatus.New ? (i.IsPerform != true && i.IsComplete != true && i.IsFinish != true)
                        : status == Resource.RequestStatus.Perform ? i.IsPerform == true
                        : status == Resource.RequestStatus.Complete ? i.IsComplete == true
                        : status == Resource.RequestStatus.Finish ? i.IsFinish == true
                        : true)
                   );
                }
                count = data.Count();
                data = Page(data, pageIndex, pageSize).ToList();
                var employees = ServiceFactory<EmployeeService>().GetByIds(data.Select(i => i.CreateBy.Value).Distinct());
                var documents = ServiceFactory<DocumentService>().GetByIds(data.Select(i => i.DocumentId.Value).Distinct());
                var parents = typeWrite ? GetByIds(data.Select(u => u.ParentId.Value).Distinct()) : Enumerable.Empty<DocumentRequestBO>();
                data = data.Select(i =>
                {
                    var employee = employees.Where(u => u.Id == i.CreateBy).FirstOrDefault();

                    var document = documents.Where(u => u.Id == i.DocumentId).FirstOrDefault();
                    i.Document = document;
                    // trong trường hợp là soạn thảo thì đưa ra thông tin yêu cầu biên soạn
                    if (typeWrite)
                    {
                        i.Parent = parents.FirstOrDefault(u => u.Id == i.ParentId);
                        i.Parent.Document = document;
                    }
                    return i;
                }).ToList();


            }
            catch (Exception)
            {

                throw;
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DocumentRequestBO CreateDefault(Guid documentId, string id = "")
        {
            DocumentRequestBO data;
            DocumentBO document;
            if (documentId == Guid.Empty)
                document = new DocumentBO();
            else document = ServiceFactory<DocumentService>().GetById(documentId); ;
            if (document != null)
            {
                // Lấy tên và màu sắc mức độ bảo mật
                if (document.CategoryId.HasValue && document.CategoryId != Guid.Empty)
                {
                    var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
                    document.CategoryName = category.Name;
                }
                // 
                if (document.SecurityId.HasValue && document.SecurityId != Guid.Empty)
                {
                    var sercurity = ServiceFactory<DocumentSecurityService>().GetById(document.SecurityId.Value);
                    document.Security = sercurity;
                }
                // Lấy tên người soạn thảo
                if (document.CreateBy.HasValue && document.CreateBy != Guid.Empty)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(document.CreateBy.Value);
                    document.Writer = employee.Name;
                }
                var temp = ServiceFactory<DocumentAttachmentService>().GetByDocument(document.Id);
                document.FileAttachs = new FileUploadBO()
                {
                    Files = temp.ToList()
                };
            }
            if (!String.IsNullOrEmpty(id))
            {
                data = GetById(new Guid(id));
                // Lấy người phê duyệt
                if (data.PerformBy.HasValue && data.PerformBy != Guid.Empty)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy.Value);
                    data.EmployeePerformBy = employee;
                }
                data.Document = document;
            }
            else
            {
                data = new DocumentRequestBO()
                {
                    Document = document,
                    DocumentId = documentId
                };
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public DocumentRequestBO GetdataDetailRequest(Guid RequestId)
        {
            var data = base.GetById(RequestId);
            DocumentBO document;
            if (data.DocumentId == Guid.Empty)
                document = new DocumentBO();
            else document = ServiceFactory<DocumentService>().GetById(data.DocumentId == null ? Guid.Empty : data.DocumentId.Value); ;
            if (document != null)
            {
                // Lấy tên và màu sắc mức độ bảo mật
                if (document.CategoryId.HasValue && document.CategoryId != Guid.Empty)
                {
                    var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
                    document.CategoryName = category.Name;
                }
                // 
                if (document.SecurityId.HasValue && document.SecurityId != Guid.Empty)
                {
                    var sercurity = ServiceFactory<DocumentSecurityService>().GetById(document.SecurityId.Value);
                    document.Security = sercurity;
                }
                // Lấy tên người soạn thảo
                if (document.CreateBy.HasValue && document.CreateBy != Guid.Empty)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(document.CreateBy.Value);
                    document.Writer = employee.Name;
                }
                var temp = ServiceFactory<DocumentRequestResultAttachmentService>().GetFileIds(data.Id);
                data.FileAttachs = new FileUploadBO()
                {
                    Files = temp.ToList()
                };
            }
            data.Document = document;
            var employeePerformBy = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy == null ? Guid.Empty : data.PerformBy.Value);
            data.EmployeePerform = new DocumentEmployeeBO()
            {
                Id = employeePerformBy.Id,
                Name = employeePerformBy.Name,
                AvatarUrl = employeePerformBy.AvatarUrl,
            };

            return data;
        }
        /// <summary>
        /// Chi tiết yêu cầu viết mới
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public DocumentRequestBO GetdataDetailRequestAdjust(Guid RequestId)
        {
            var data = base.GetById(RequestId);
            //var Request = base.Get(p => p.ParentId == data.Id).FirstOrDefault();
            var employeePerformBy = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy == null ? Guid.Empty : data.PerformBy.Value);
            if (data.RequestTo == (int)iDAS.Service.Common.Resource.RequestTo.Compilator)
            {
                data.notCheck = true;
                data.Check = false;
                data.EmployeePerformwrite = new DocumentEmployeeBO()
                {
                    Id = employeePerformBy.Id,
                    Name = employeePerformBy.Name,
                    AvatarUrl = employeePerformBy.AvatarUrl,
                };
            }
            else
            {
                data.Check = true;
                data.notCheck = false;
                data.EmployeePerform = new DocumentEmployeeBO()
                {
                    Id = employeePerformBy.Id,
                    Name = employeePerformBy.Name,
                    AvatarUrl = employeePerformBy.AvatarUrl,
                };
            }

            var temp = ServiceFactory<DocumentRequestResultAttachmentService>().GetFileIds(data.Id);
            data.FileAttachs = new FileUploadBO()
            {
                Files = temp.ToList()
            };
            DocumentBO document;
            if (data.DocumentId == Guid.Empty)
                document = new DocumentBO();
            else document = ServiceFactory<DocumentService>().GetById(data.DocumentId == null ? Guid.Empty : data.DocumentId.Value); ;
            if (document != null)
            {
                // Lấy tên và màu sắc mức độ bảo mật
                if (document.CategoryId.HasValue && document.CategoryId != Guid.Empty)
                {
                    var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
                    document.CategoryName = category.Name;
                }
                // 
                if (document.SecurityId.HasValue && document.SecurityId != Guid.Empty)
                {
                    var sercurity = ServiceFactory<DocumentSecurityService>().GetById(document.SecurityId.Value);
                    document.Security = sercurity;
                }
                // Lấy tên người soạn thảo
                if (document.CreateBy.HasValue && document.CreateBy != Guid.Empty)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(document.CreateBy.Value);
                    document.Writer = employee.Name;
                }
            }
            data.Document = document;
            return data;
        }
        /// <summary>
        /// Tạo/cập nhật yêu cầu hủy
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DocumentRequestBO CreateDestroyRequest(Guid documentId, string id = "")
        {
            DocumentRequestBO data;
            DocumentBO document;
            if (documentId == Guid.Empty)
                throw new DataIsNotValidException();
            else document = ServiceFactory<DocumentService>().GetById(documentId);
            if (!document.DepartmentId.HasValue)
                throw new DocumentIsNotInDepartmentException();
            else
            {
                var checkRole = ServiceFactory<DocumentProcessService>().CheckCurrentUserRole(document.DepartmentId.Value, (int)Resource.DocumentTypeProcess.WriteAndRelation, (int)Resource.DocumentProcessRole.ApproveDocument);
                if (!checkRole)
                    throw new AccessDenyException();
            }
            if (document != null)
            {
                // Lấy tên và màu sắc mức độ bảo mật
                if (document.CategoryId.HasValue && document.CategoryId != Guid.Empty)
                {
                    var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
                    document.CategoryName = category.Name;
                }
                // 
                if (document.SecurityId.HasValue && document.SecurityId != Guid.Empty)
                {
                    var sercurity = ServiceFactory<DocumentSecurityService>().GetById(document.SecurityId.Value);
                    document.Security = sercurity;
                }
                // Lấy tên người soạn thảo
                if (document.CreateBy.HasValue && document.CreateBy != Guid.Empty)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(document.CreateBy.Value);
                    document.Writer = employee.Name;
                }
                var temp = ServiceFactory<DocumentAttachmentService>().GetByDocument(document.Id);
                document.FileAttachs = new FileUploadBO()
                {
                    Files = temp.ToList()
                };
            }
            if (!string.IsNullOrEmpty(id))
            {
                data = GetById(new Guid(id));
                // Lấy người phê duyệt
                if (data.PerformBy.HasValue && data.PerformBy != Guid.Empty)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy.Value);
                    data.EmployeePerformBy = employee;
                }
                data.Document = document;
            }
            else
            {
                // Nếu tài liệu đã yêu cầu hủy thì cho chỉnh sửa
                var requestDestroy = base.Get(p => p.DocumentId == documentId && p.TypeDestroy == true && p.CreateBy == UserId).FirstOrDefault();
                if (requestDestroy != null)
                {
                    data = requestDestroy;
                    data.Document = document;
                    data.DocumentId = documentId;
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy.Value);
                    data.EmployeePerformBy = employee;
                    var temp = ServiceFactory<DocumentRequestResultAttachmentService>().GetFileIds(requestDestroy.Id);
                    data.FileAttachs = new FileUploadBO()
                    {
                        Files = temp.ToList()
                    };
                }
                // Ngược lại cho thêm mới
                else
                {
                    data = new DocumentRequestBO()
                    {
                        Document = document,
                        DocumentId = documentId,
                    };
                }
            }
            return data;
        }
        /// <summary>
        /// Hủy tài liệu
        /// </summary>
        /// <param name="Id"></param>
        public void DestroyRequestDocument(Guid Id)
        {
            var DestroyRequest = base.GetById(Id);
            DestroyRequest.IsComplete = true;
            base.Update(DestroyRequest, false);
            var document = ServiceFactory<DocumentService>().GetById(DestroyRequest.DocumentId.Value);
            document.IsExpire = true;
            document.IsObsolate = false;
            document.IsPublish = false;
            document.DestroyAt = DateTime.Now;
            ServiceFactory<DocumentService>().Update(document, false);
            SaveTransaction(true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentRequestBO> GetRequestDesTroy()
        {
            var data = Enumerable.Empty<DocumentRequestBO>();
            try
            {
                data = base.GetQuery().Where(i => i.TypeDestroy == true && (i.CreateBy == UserId || i.PerformBy == UserId) && i.IsSend == true)
                       .AsEnumerable()
                       .Select(i =>
                       {
                           i.UserId = UserId;
                           return i;
                       }).OrderByDescending(t => t.CreateAt);
            }
            catch (Exception)
            {
                throw;
            }
            return data;
        }
        /// <summary>
        /// Tạo/cập nhật yêu phân phối
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DocumentRequestBO CreateDistributeRequest(Guid documentId, string id = "")
        {
            DocumentRequestBO data;
            DocumentBO document;
            if (documentId == Guid.Empty)
                throw new DataIsNotValidException();
            document = ServiceFactory<DocumentService>().GetById(documentId);

            var checkPermission = ServiceFactory<DocumentProcessService>().CheckRoleTypeExits(iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument);
            if (!checkPermission)
                throw new AccessDenyException();

            if (document != null)
            {
                // Lấy tên và màu sắc mức độ bảo mật
                if (document.CategoryId.HasValue && document.CategoryId != Guid.Empty)
                {
                    var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
                    document.CategoryName = category.Name;
                }
                // 
                if (document.SecurityId.HasValue && document.SecurityId != Guid.Empty)
                {
                    var sercurity = ServiceFactory<DocumentSecurityService>().GetById(document.SecurityId.Value);
                    document.Security = sercurity;
                }
                // Lấy tên người soạn thảo
                if (document.CreateBy.HasValue && document.CreateBy != Guid.Empty)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(document.CreateBy.Value);
                    document.Writer = employee.Name;
                }
                var temp = ServiceFactory<DocumentAttachmentService>().GetByDocument(document.Id);
                document.FileAttachs = new FileUploadBO()
                {
                    Files = temp.ToList()
                };
            }
            if (!String.IsNullOrEmpty(id))
            {
                data = GetById(new Guid(id));
                // Lấy người phê duyệt
                if (data.PerformBy.HasValue && data.PerformBy != Guid.Empty)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy.Value);
                    data.EmployeePerformBy = employee;
                }
                data.Document = document;
            }
            else
            {
                // Nếu tài liệu đã yêu cầu hủy thì cho chỉnh sửa
                var requestDistribute = base.Get(p => p.DocumentId == documentId && p.TypeRole == (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeRequester && p.CreateBy == UserId).FirstOrDefault();
                if (requestDistribute != null)
                {
                    data = requestDistribute;
                    data.Document = document;
                    data.DocumentId = documentId;
                    var employee = ServiceFactory<EmployeeService>().GetById(data.PerformBy.Value);
                    data.EmployeePerformBy = employee;
                    var temp = ServiceFactory<DocumentRequestResultAttachmentService>().GetFileIds(requestDistribute.Id);
                    data.FileAttachs = new FileUploadBO()
                    {
                        Files = temp.ToList()
                    };
                }
                // Ngược lại cho thêm mới
                else
                {
                    data = new DocumentRequestBO()
                    {
                        Document = document,
                        DocumentId = documentId
                    };
                }
            }
            return data;
        }
        /// <summary>
        /// Tạo/cập nhật yêu sửa đổi
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DocumentRequestBO CreateAdjustRequest(Guid documentId, string id = "")
        {
            try
            {
                var checkPermission = ServiceFactory<DocumentProcessService>().CheckRoleTypeExits(iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument);
                if (!checkPermission)
                    throw new AccessDenyException();
                DocumentRequestBO data;
                DocumentBO document;
                if (documentId == Guid.Empty)
                    document = new DocumentBO();
                else document = ServiceFactory<DocumentService>().GetById(documentId); ;
                if (document != null)
                {
                    // Lấy tên và màu sắc mức độ bảo mật
                    if (document.CategoryId.HasValue && document.CategoryId != Guid.Empty)
                    {
                        var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
                        document.CategoryName = category.Name;
                        document.DepartmentId = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value).DepartmentId;
                        document.DepartmentName = ServiceFactory<DepartmentService>().GetById(document.DepartmentId.Value).Name;
                    }
                    // 
                    if (document.SecurityId.HasValue && document.SecurityId != Guid.Empty)
                    {
                        var sercurity = ServiceFactory<DocumentSecurityService>().GetById(document.SecurityId.Value);
                        document.Security = sercurity;
                    }
                    // Lấy tên người soạn thảo
                    if (document.CreateBy.HasValue && document.CreateBy != Guid.Empty)
                    {
                        var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(document.CreateBy.Value);
                        document.Writer = employee.Name;
                    }
                    var temp = ServiceFactory<DocumentAttachmentService>().GetByDocument(document.Id);
                    document.FileAttachs = new FileUploadBO()
                    {
                        Files = temp.ToList()
                    };
                }
                if (!String.IsNullOrEmpty(id))
                {
                    data = GetById(new Guid(id));
                    // Lấy người phê duyệt
                    if (data.PerformBy.HasValue && data.PerformBy != Guid.Empty)
                    {
                        var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy.Value);
                        data.EmployeePerformBy = employee;
                    }
                    data.Document = document;
                }
                else
                {
                    // Nếu tài liệu đã yêu cầu hủy thì cho chỉnh sửa
                    data = base.Get(p => p.DocumentId == documentId && p.TypeAdjust == true && p.CreateBy == UserId).FirstOrDefault();
                    if (data != null)
                    {
                        data.Document = document;
                        //var Request = base.Get(p => p.ParentId == data.Id).FirstOrDefault();
                        var employeePerformBy = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy == null ? Guid.Empty : data.PerformBy.Value);
                        if (data.RequestTo == (int)iDAS.Service.Common.Resource.RequestTo.Compilator)
                        {
                            data.notCheck = true;
                            data.Check = false;
                            data.EmployeePerformwrite = new DocumentEmployeeBO()
                            {
                                Id = employeePerformBy.Id,
                                Name = employeePerformBy.Name,
                                AvatarUrl = employeePerformBy.AvatarUrl,
                            };
                        }
                        else
                        {
                            data.Check = true;
                            data.notCheck = false;
                            data.EmployeePerform = new DocumentEmployeeBO()
                            {
                                Id = employeePerformBy.Id,
                                Name = employeePerformBy.Name,
                                AvatarUrl = employeePerformBy.AvatarUrl,
                            };

                        }

                        var temp = ServiceFactory<DocumentRequestResultAttachmentService>().GetFileIds(data.Id);
                        data.FileAttachs = new FileUploadBO()
                        {
                            Files = temp.ToList()
                        };
                    }
                    // Ngược lại cho thêm mới
                    else
                    {
                        data = new DocumentRequestBO()
                        {
                            Check = true,
                            Document = document,
                            DocumentId = documentId
                        };
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Danh sách các yêu cầu được tạo bởi người đang nhập
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public IEnumerable<DocumentRequestBO> GetRequestByCurrentUser(int pageIndex, int pageSize, out int count, bool typeWrite = false, Resource.RequestStatus status = Resource.RequestStatus.All)
        {
            var data = Enumerable.Empty<DocumentRequestBO>();
            try
            {
                data = Get(i => i.TypeWrite == typeWrite && i.CreateBy == UserId

                       && (status == Resource.RequestStatus.New ? (i.IsPerform != true && i.IsComplete != true && i.IsFinish != true)
                       : status == Resource.RequestStatus.Perform ? i.IsPerform == true
                       : status == Resource.RequestStatus.Complete ? i.IsComplete == true
                       : status == Resource.RequestStatus.Finish ? i.IsFinish == true
                       : true)
                  );
                count = data.Count();
                data = Page(data, pageIndex, pageSize).ToList();
                var employees = ServiceFactory<EmployeeService>().GetByIds(data.Select(i => i.CreateBy.Value).Distinct());
                var documents = ServiceFactory<DocumentService>().GetByIds(data.Select(i => i.DocumentId == null ? Guid.Empty : i.DocumentId.Value).Distinct());

                data = data.Select(i =>
                {
                    var employee = employees.Where(u => u.Id == i.CreateBy).FirstOrDefault();
                    var document = documents.Where(u => u.Id == i.DocumentId).FirstOrDefault();
                    i.Document = document;
                    if (i.TypeWrite == true)
                    {
                        //var review = ServiceFactory<DocumentReviewSuggestService>().GetByRequestId(i.Id);
                        //i.ReviewSuggestId = review == null ? (Guid?)null : review.Id;
                    }
                    return i;
                });
            }
            catch (Exception)
            {

                throw;
            }
            return data;

        }
        /// <summary>
        /// Thu hôi yêu cầu
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        public void UpdateRequest(DocumentRequestBO data, bool allowSave = true)
        {
            try
            {
                data.ProcessStatus = (int)iDAS.Service.Common.Resource.RequestProcessStatus.New;
                base.Update(data, false);
                SaveTransaction(allowSave);
            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// TẠO MỚI  Yêu cầu
        /// </summary>
        /// <param name="data"></param>
        public void InsertRequestDestroy(DocumentRequestBO data, bool allowSave = true)
        {
            try
            {
                var fileIds = Enumerable.Empty<Guid>();
                //1. Nếu Id = empty: thực hiện thêm mới tài liệu, thêm mới yêu cầu
                if (data != null && data.FileAttachs != null && data.FileAttachs.FileAttachments.Any())
                {
                    if (data != null && data.FileAttachs != null)
                    {
                        fileIds = ServiceFactory<FileService>().UploadRange(data.FileAttachs, false);
                    }
                }
                data.PerformBy = data.EmployeePerform.Id;
                data.ApproveBy = UserId;
                data.ApproveAt = data.CreateAt;
                data.EstimateAt = data.EndAt;
                data.Order = data.EmployeePerform.NextOrder;
                data.TypeRole = data.EmployeePerform.RoleType;
                data.TypeRole = (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyRequester;
                data.RequestFrom = (int)iDAS.Service.Common.Resource.RequestFrom.Requestor;
                if (data.Id == null || data.Id == Guid.Empty)
                {
                    data.Id = base.Insert(data, false);
                }
                else
                {
                    base.Update(data, false);
                    if (data.FileAttachs != null && data.FileAttachs.FileRemoves != null && data.FileAttachs.FileRemoves.Count > 0)
                        ServiceFactory<DocumentRequestResultAttachmentService>().DeleteRange(data.Id, data.FileAttachs.FileRemoves);
                }

                // Insert đính kèm tương ứng với bảng kết quả
                foreach (var fileId in fileIds)
                {
                    var attachment = new DocumentRequestResultAttachmentBO()
                    {
                        FileId = fileId,
                        ResultId = data.Id
                    };
                    ServiceFactory<DocumentRequestResultAttachmentService>().Insert(attachment, false);
                }
                SaveTransaction(allowSave);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Get all yêu cầu hủy
        /// </summary>
        /// <param name="data"></param>
        public IEnumerable<DocumentRequestBO> GetRequestDestroy(int pageIndex, int pageSize, out int count, Resource.RequestStatus status = Resource.RequestStatus.All)
        {
            var data = Enumerable.Empty<DocumentRequestBO>();
            try
            {
                data = base.Get(i => i.TypeDestroy == true && (i.CreateBy == UserId || i.PerformBy == UserId)
                     && (status == Resource.RequestStatus.New ? (i.IsSend != true && i.IsPerform != true && i.IsComplete != true && i.IsFinish != true)
                       : status == Resource.RequestStatus.Wait ? i.IsSend == true && i.IsPerform != true && i.IsComplete != true && i.IsFinish != true
                       : status == Resource.RequestStatus.Perform ? i.IsPerform == true && i.IsComplete != true && i.IsFinish != true
                       : status == Resource.RequestStatus.Complete ? i.IsComplete == true && i.IsFinish != true
                       : status == Resource.RequestStatus.Finish ? i.IsFinish == true
                       : true)

                    ).OrderByDescending(i => i.CreateAt);
                count = data.Count();
                data = Page(data, pageIndex, pageSize).ToList();
                var employees = ServiceFactory<EmployeeService>().GetByIds(data.Select(i => i.CreateBy.Value).Distinct());
                var documents = ServiceFactory<DocumentService>().GetByIds(data.Select(i => i.DocumentId == null ? Guid.Empty : i.DocumentId.Value).Distinct());
                data = data.Select(i =>
                {
                    var employee = employees.Where(u => u.Id == i.CreateBy).FirstOrDefault();
                    i.EmployeeCreateBy = employee;
                    var title = ServiceFactory<EmployeeTitleService>().Get(m => m.EmployeeId == employee.Id).FirstOrDefault();
                    if (title != null)
                    {
                        var result = ServiceFactory<DepartmentTitleService>().GetById(title.TitleId);
                        i.TitleEmploy = result.Name;
                    }
                    var document = documents.Where(u => u.Id == i.DocumentId).FirstOrDefault();
                    i.Document = document;
                    i.UserId = UserId;

                    return i;
                });
                //foreach (var item in data)
                //{
                //    if (item.CreateBy.HasValue && item.CreateBy.Value != Guid.Empty)
                //    {
                //        var employee = ServiceFactory<EmployeeService>().GetById(item.CreateBy.Value);
                //        item.EmployeeCreateBy = employee;
                //        var title = ServiceFactory<EmployeeTitleService>().Get(i => i.EmployeeId == employee.Id).FirstOrDefault();
                //        if (title != null)
                //        {
                //            var result = ServiceFactory<DepartmentTitleService>().GetById(title.TitleId);
                //            item.TitleEmploy = result.Name;
                //        }
                //    }
                //    if (item.DocumentId.HasValue)
                //    {
                //        var document = ServiceFactory<DocumentService>().GetById(item.DocumentId.Value);
                //        item.Document = document;
                //    }
                //    item.UserId = UserId;
                //}

            }
            catch (Exception)
            {
                throw;
            }
            return data;

        }
        /// <summary>
        /// Get all yêu cầu sửa đổi
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<DocumentRequestBO> GetRequestAdjust(int pageIndex, int pageSize, out int count, Resource.RequestProcessStatus status = Resource.RequestProcessStatus.None)
        {
            var data = Enumerable.Empty<DocumentRequestBO>();
            try
            {
                data = base.Get(i => i.TypeAdjust == true && i.CreateBy == UserId && i.ParentId == null
                       && i.DocumentSuggestId == null
                       && i.RequestSource == (int)Resource.RequestSource.RequestWrite
                       && i.RequestFrom == (int)Resource.RequestFrom.Requestor
                       && (status == Resource.RequestProcessStatus.New ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.New)
                       : status == Resource.RequestProcessStatus.WaitAssign ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.WaitAssign)
                       : status == Resource.RequestProcessStatus.WaitPerform ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.WaitPerform)
                       : status == Resource.RequestProcessStatus.Perform ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.Perform)
                       : status == Resource.RequestProcessStatus.WaitReview ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.WaitReview)
                       : status == Resource.RequestProcessStatus.ReviewReject ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.ReviewReject)
                       : status == Resource.RequestProcessStatus.WaitSendApprove ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.WaitSendApprove)
                       : status == Resource.RequestProcessStatus.WaitApprove ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.WaitApprove)
                       : status == Resource.RequestProcessStatus.Complete ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.Complete)
                       : status == Resource.RequestProcessStatus.Cancel ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.Cancel)
                       : status == Resource.RequestProcessStatus.OutOfDate ? (i.EstimateAt.HasValue && i.EstimateAt.Value.Date < DateTime.Now.Date)
                       : true)
                    ).OrderByDescending(i => i.CreateAt);
                count = data.Count();
                data = Page(data, pageIndex, pageSize).ToList();
                var employees = ServiceFactory<EmployeeService>().GetByIds(data.Select(i => i.CreateBy.Value).Distinct());
                var documents = ServiceFactory<DocumentService>().GetByIds(data.Select(i => i.DocumentId == null ? Guid.Empty : i.DocumentId.Value).Distinct());

                data = data.Select(i =>
                {
                    var employee = employees.Where(u => u.Id == i.CreateBy).FirstOrDefault();
                    i.EmployeeCreateBy = employee;
                    var title = ServiceFactory<EmployeeTitleService>().Get(m => m.EmployeeId == employee.Id).FirstOrDefault();
                    if (title != null)
                    {
                        var result = ServiceFactory<DepartmentTitleService>().GetById(title.TitleId);
                        i.TitleEmploy = result.Name;
                    }
                    var document = documents.Where(u => u.Id == i.DocumentId).FirstOrDefault();
                    i.Document = document;
                    i.UserId = UserId;

                    return i;
                });

            }
            catch (Exception)
            {

                throw;
            }
            return data.ToList();

        }
        /// <summary>
        /// Get all yêu cầu phân phối
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<DocumentRequestBO> GetRequestDistribute(int pageIndex, int pageSize, out int count, Resource.RequestStatus type = Resource.RequestStatus.All, Resource.DocumentEmployeeRole status = iDAS.Service.Common.Resource.DocumentEmployeeRole.All)
        {
            var data = Enumerable.Empty<DocumentRequestBO>();
            try
            {   // 1. lấy theo điều kiện
                data = base.Get(i => i.TypeRole == (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeRequester && i.CreateBy == UserId
                       && (type == Resource.RequestStatus.New ? (i.IsSend != true && i.IsPerform != true && i.IsComplete != true && i.IsFinish != true)
                       : type == Resource.RequestStatus.Wait ? i.IsSend == true && i.IsPerform != true && i.IsComplete != true && i.IsFinish != true
                       : type == Resource.RequestStatus.Perform ? i.IsPerform == true && i.IsComplete != true && i.IsFinish != true
                       : type == Resource.RequestStatus.Complete ? i.IsComplete == true && i.IsFinish != true
                       : type == Resource.RequestStatus.Finish ? i.IsFinish == true
                       : true)
                       && (status == Resource.DocumentEmployeeRole.Assigner ? i.PerformBy == UserId
                       : status == Resource.DocumentEmployeeRole.Approver ? i.ApproveBy == UserId
                       : true)
                    ).OrderByDescending(i => i.CreateAt);
                count = data.Count();
                data = Page(data, pageIndex, pageSize).ToList();
                // 2. lấy danh sách người yêu cầu
                var employees = ServiceFactory<EmployeeService>().GetByIds(data.Select(i => i.CreateBy.Value).Distinct());
                // 3. truy vấn sang bảng document 
                var documents = ServiceFactory<DocumentService>().GetByIds(data.Select(i => i.DocumentId != null ? i.DocumentId.Value : Guid.Empty).Distinct());

                // 4. lấy dữ liệu
                data = data.Select(i =>
                {
                    var employee = employees.Where(u => u.Id == i.CreateBy).FirstOrDefault();
                    if (employee != null)
                    {
                        i.EmployeeCreateBy = employee;
                        var title = ServiceFactory<EmployeeTitleService>().Get(m => m.EmployeeId == employee.Id).FirstOrDefault();
                        if (title != null)
                        {
                            var result = ServiceFactory<DepartmentTitleService>().GetById(title.TitleId);
                            i.TitleEmploy = result.Name;
                        }
                    }
                    var document = documents.Where(u => u.Id == i.DocumentId).FirstOrDefault();
                    i.Document = document;
                    i.UserId = UserId;
                    return i;
                });

            }
            catch (Exception)
            {

                throw;
            }
            return data;

        }
        /// <summary>
        /// Get all yêu cầu soạn thảo(viết mới)
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<DocumentRequestBO> GetRequestWriteNew(int pageIndex, int pageSize, out int count, Resource.RequestProcessStatus type = Resource.RequestProcessStatus.None, Resource.DocumentEmployeeRole status = iDAS.Service.Common.Resource.DocumentEmployeeRole.All)
        {
            var data = Enumerable.Empty<DocumentRequestBO>();
            try
            {   // 1. lấy theo điều kiện
                data = base.Get(i => i.TypeWrite == true && i.CreateBy == UserId && i.ParentId == null
                       && i.RequestSource == (int)Resource.RequestSource.RequestWrite
                       && i.RequestFrom == (int)Resource.RequestFrom.Requestor
                       && i.DocumentSuggestId == null
                       && (type == Resource.RequestProcessStatus.New ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.New)
                       : type == Resource.RequestProcessStatus.WaitAssign ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.WaitAssign)
                       : type == Resource.RequestProcessStatus.WaitPerform ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.WaitPerform)
                       : type == Resource.RequestProcessStatus.Perform ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.Perform)
                       : type == Resource.RequestProcessStatus.WaitReview ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.WaitReview)
                       : type == Resource.RequestProcessStatus.ReviewReject ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.ReviewReject)
                       : type == Resource.RequestProcessStatus.WaitSendApprove ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.WaitSendApprove)
                       : type == Resource.RequestProcessStatus.WaitApprove ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.WaitApprove)
                       : type == Resource.RequestProcessStatus.Complete ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.Complete)
                       : type == Resource.RequestProcessStatus.Cancel ? (i.ProcessStatus == (int)Resource.RequestProcessStatus.Cancel)
                       : type == Resource.RequestProcessStatus.OutOfDate ? (i.EstimateAt.HasValue && i.EstimateAt.Value.Date < DateTime.Now.Date)
                       : true)
                       && (status == Resource.DocumentEmployeeRole.Assigner ? i.PerformBy == UserId
                       : status == Resource.DocumentEmployeeRole.Approver ? i.ApproveBy == UserId
                       : true)
                    ).OrderByDescending(i => i.CreateAt);
                count = data.Count();
                data = Page(data, pageIndex, pageSize).ToList();
                // 2. lấy danh sách người yêu cầu
                var employees = ServiceFactory<EmployeeService>().GetByIds(data.Select(i => i.CreateBy.Value).Distinct());
                // 3. truy vấn sang bảng document 
                var documents = ServiceFactory<DocumentService>().GetByIds(data.Select(i => i.DocumentId != null ? i.DocumentId.Value : Guid.Empty).Distinct());

                // 4. lấy dữ liệu
                data = data.Select(i =>
                {
                    var employee = employees.Where(u => u.Id == i.CreateBy).FirstOrDefault();
                    if (employee != null)
                    {
                        i.EmployeeCreateBy = employee;
                        var title = ServiceFactory<EmployeeTitleService>().Get(m => m.EmployeeId == employee.Id).FirstOrDefault();
                        if (title != null)
                        {
                            var result = ServiceFactory<DepartmentTitleService>().GetById(title.TitleId);
                            i.TitleEmploy = result.Name;
                        }
                    }
                    var document = documents.Where(u => u.Id == i.DocumentId).FirstOrDefault();
                    i.Document = document;
                    i.UserId = UserId;
                    return i;
                });

            }
            catch (Exception)
            {

                throw;
            }
            return data;

        }
        /// <summary>
        /// Lưu yêu cầu lưu trữ
        /// </summary>
        /// <param name="data"></param>
        public void SaveRequestReceive(DocumentRequestBO data, bool allowSave = true)
        {

            try
            {
                var documents = ServiceFactory<DocumentService>().GetAll();
                var documentCode = documents.Where(p => p.Id == data.DocumentId.Value).FirstOrDefault().Code;
                var documentPublish = ServiceFactory<DocumentPublishService>().GetAll();
                if (documentPublish.Any(p => p.DocumentId == data.DocumentId) == false)
                {
                    throw new FormatException(Resource.ExceptionErrorMessage.DocumentPublish);
                }
                else
                {
                    if (data.Id == null || data.Id == Guid.Empty)
                    {
                        data.TypeArchive = true;
                        data.PerformBy = data.EmployeePerformBy.Id;
                        base.Insert(data, false);
                    }
                    else
                    {
                        data.TypeArchive = true;
                        data.PerformBy = data.EmployeePerformBy.Id;
                        base.Update(data, false);
                    }
                    SaveTransaction(allowSave);
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Lưu yêu cầu soạn thảo
        /// request Id = rỗng : thực hiện thêm mới
        /// else thực hiện update
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        public void SaveRequestWriteNew(DocumentRequestBO data, bool allowSave = true)
        {
            try
            {
                var fileIds = Enumerable.Empty<Guid>();
                //1. Nếu Id = empty: thực hiện thêm mới tài liệu, thêm mới yêu cầu
                if (data != null && data.FileAttachs != null && data.FileAttachs.FileAttachments.Any())
                {
                    fileIds = ServiceFactory<FileService>().UploadRange(data.FileAttachs, false);
                }
                data.ApproveBy = UserId;
                data.ApproveAt = data.CreateAt;
                data.EstimateAt = data.EndAt;
                data.TypeWrite = true;
                data.RequestSource = (int)iDAS.Service.Common.Resource.RequestSource.RequestWrite;
                data.RequestFrom = (int)iDAS.Service.Common.Resource.RequestFrom.Requestor;
                // Trường hợp người yêu cầu gửi thẳng đến người soạn thảo
                #region
                if (data.Check == true)
                {
                    data.RequestTo = (int)iDAS.Service.Common.Resource.RequestTo.Compilator;
                    data.PerformBy = data.EmployeePerformwrite != null ? data.EmployeePerformwrite.Id : (Guid?)null;
                    data.TypeCompile = true;
                    if (data.IsSend == true)
                    {
                        data.ProcessStatus = (int)iDAS.Service.Common.Resource.RequestProcessStatus.WaitPerform;
                    }
                    // Qua 1 bước phân công => người yêu cầu = người phân công
                    data.Order = data.EmployeePerformwrite.NextOrder;
                    data.TypeRole = data.EmployeePerformwrite.RoleType;
                    if (data.Id == Guid.Empty)
                    {
                        // Insert document
                        // 1. Create new document
                        var document = new DocumentBO()
                        {
                            DepartmentId = data.DepartmentId,
                            Name = data.Document.Name,
                            IsRequestWrite = true
                        };
                        var checkName = ServiceFactory<DocumentService>().Get(i => !string.IsNullOrEmpty(i.Name) && i.Name.Trim().ToUpper() == data.Document.Name.Trim().ToUpper()).Any();
                        if (checkName) { throw new DataHasBeenExistedException(); }
                        else
                        {
                            // 2. Insert document
                            document.Id = ServiceFactory<DocumentService>().Insert(document, false);
                            data.DocumentId = document.Id;
                            //3. Kết quả của yêu cầu gửi đến phân công là nguồn của yêu cầu gửi đến người soạn thảo
                            data.Id = base.Insert(data, false);
                        }
                    }
                    else
                    {
                        var checkName = ServiceFactory<DocumentService>().Get(i => !string.IsNullOrEmpty(i.Name) && i.Name.Trim().ToUpper() == data.Document.Name.Trim().ToUpper()).Any();
                        if (checkName) { throw new DataHasBeenExistedException(); }
                        {
                            base.Update(data, false);
                            if (data.FileAttachs != null && data.FileAttachs.FileRemoves != null && data.FileAttachs.FileRemoves.Count > 0)
                                ServiceFactory<DocumentRequestResultAttachmentService>().DeleteRange(data.Id, data.FileAttachs.FileRemoves);
                        }
                    }
                }
                #endregion
                else
                {
                    data.RequestTo = (int)iDAS.Service.Common.Resource.RequestTo.Assigner;
                    data.PerformBy = data.EmployeePerform != null ? data.EmployeePerform.Id : (Guid?)null;
                    data.Order = data.EmployeePerform.NextOrder;
                    data.TypeRole = data.EmployeePerform.RoleType;
                    data.TypeCompile = false;
                    if (data.Id == Guid.Empty || data.Id == null)
                    {
                        // Insert document
                        // 1. Create new document
                        var document = new DocumentBO()
                        {
                            Name = data.Document.Name,
                            IsRequestWrite = true,
                            DepartmentId = data.DepartmentId
                        };
                        // 2. Insert document
                        document.Id = ServiceFactory<DocumentService>().Insert(document, false);
                        data.DocumentId = document.Id;
                        data.Id = base.Insert(data, false);

                    }
                    else //2. Thực hiện cập nhật tên tài liệu, cập nhật thông tin yêu cầu 
                    {
                        base.Update(data, false);
                        if (data.FileAttachs != null && data.FileAttachs.FileRemoves != null && data.FileAttachs.FileRemoves.Count > 0)
                            ServiceFactory<DocumentRequestResultAttachmentService>().DeleteRange(data.Id, data.FileAttachs.FileRemoves);
                    }
                }

                // Insert đính kèm tương ứng với bảng kết quả
                foreach (var fileId in fileIds)
                {
                    var attachment = new DocumentRequestResultAttachmentBO()
                    {
                        FileId = fileId,
                        ResultId = data.Id
                    };
                    ServiceFactory<DocumentRequestResultAttachmentService>().Insert(attachment, false);
                }
                SaveTransaction(allowSave);
            }
            catch (Exception err)
            {
                throw err;
            }
        }
        /// <summary>
        /// Lưu yêu cầu sửa đổi
        /// request Id = rỗng : thực hiện thêm mới
        /// else thực hiện update
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        public void SaveRequestAdjust(DocumentRequestBO data, bool allowSave = true)
        {
            try
            {
                var fileIds = Enumerable.Empty<Guid>();
                //1. Nếu Id = empty: thực hiện thêm mới tài liệu, thêm mới yêu cầu
                if (data != null && data.FileAttachs != null && data.FileAttachs.FileAttachments.Any())
                {
                    if (data != null && data.FileAttachs != null)
                    {
                        fileIds = ServiceFactory<FileService>().UploadRange(data.FileAttachs, true);
                    }
                }
                data.ApproveBy = UserId;
                data.ApproveAt = data.CreateAt;
                data.EstimateAt = data.EndAt;
                data.TypeAdjust = true;
                data.RequestSource = (int)iDAS.Service.Common.Resource.RequestSource.RequestAdjust;
                data.RequestFrom = (int)iDAS.Service.Common.Resource.RequestFrom.Requestor;
                // Trường hợp người yêu cầu gửi thẳng đến người soạn thảo
                if (data.Check == true)
                {
                    data.RequestTo = (int)iDAS.Service.Common.Resource.RequestTo.Compilator;
                    data.PerformBy = data.EmployeePerformwrite != null ? data.EmployeePerformwrite.Id : (Guid?)null;
                    data.TypeCompile = true;
                    if (data.IsSend == true)
                    {
                        data.ProcessStatus = (int)iDAS.Service.Common.Resource.RequestProcessStatus.WaitPerform;
                    }
                    data.Order = data.EmployeePerformwrite.NextOrder;
                    data.TypeRole = data.EmployeePerformwrite.RoleType;
                    // Qua 1 bước phân công => người yêu cầu = người phân công
                    if (data.Id == Guid.Empty)
                    {
                        data.Id = base.Insert(data, true);
                    }
                    else
                    {
                        base.Update(data, true);
                        if (data.FileAttachs != null && data.FileAttachs.FileRemoves != null && data.FileAttachs.FileRemoves.Count > 0)
                            ServiceFactory<DocumentRequestResultAttachmentService>().DeleteRange(data.Id, data.FileAttachs.FileRemoves);
                    }
                }
                else
                {
                    data.TypeCompile = false;
                    data.Order = data.EmployeePerform.NextOrder;
                    data.TypeRole = data.EmployeePerform.RoleType;
                    data.RequestTo = (int)iDAS.Service.Common.Resource.RequestTo.Assigner;
                    data.PerformBy = data.EmployeePerform != null ? data.EmployeePerform.Id : (Guid?)null;
                    if (data.Id == null || data.Id == Guid.Empty)
                    {
                        data.Id = base.Insert(data, true);
                    }
                    else
                    {
                        base.Update(data, true);
                        if (data.FileAttachs != null && data.FileAttachs.FileRemoves != null && data.FileAttachs.FileRemoves.Count > 0)
                            ServiceFactory<DocumentRequestResultAttachmentService>().DeleteRange(data.Id, data.FileAttachs.FileRemoves);
                    }
                }

                // Insert đính kèm tương ứng với bảng kết quả
                foreach (var fileId in fileIds)
                {
                    var attachment = new DocumentRequestResultAttachmentBO()
                    {
                        FileId = fileId,
                        ResultId = data.Id
                    };
                    ServiceFactory<DocumentRequestResultAttachmentService>().Insert(attachment, true);
                }
            }
            catch (Exception err)
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        public void SendRequestAdjust(DocumentRequestBO data, bool allowSave = true)
        {
            data.IsSend = true;
            data.SendAt = DateTime.Now;
            data.ProcessStatus = (int)iDAS.Service.Common.Resource.RequestProcessStatus.WaitAssign;
            SaveRequestAdjust(data);
        }
        /// <summary>
        /// Lưu yêu cầu phân phối
        /// request Id = rỗng : thực hiện thêm mới
        /// else thực hiện update
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        public void SaveRequestDistribute(DocumentRequestBO data, bool allowSave = true)
        {
            try
            {
                var fileIds = Enumerable.Empty<Guid>();
                //1. Nếu Id = empty: thực hiện thêm mới tài liệu, thêm mới yêu cầu
                if (data != null && data.FileAttachs != null && data.FileAttachs.FileAttachments.Any())
                {
                    if (data != null && data.FileAttachs != null)
                    {
                        fileIds = ServiceFactory<FileService>().UploadRange(data.FileAttachs, false);
                    }
                }
                data.DocumentSuggestId = data.DocumentSuggestId;
                data.PerformBy = data.EmployeePerformBy.Id;
                data.ApproveBy = UserId;
                data.ApproveAt = data.CreateAt;
                data.EstimateAt = data.EndAt;
                // data.Order = data.EmployeePerform.NextOrder;
                // data.TypeRole = data.EmployeePerform.RoleType;
                data.TypeRole = (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeRequester;
                data.RequestFrom = (int)iDAS.Service.Common.Resource.RequestFrom.Requestor;
                if (data.Id == null || data.Id == Guid.Empty)
                {
                    data.Id = base.Insert(data, false);
                }
                else
                {
                    base.Update(data, false);
                    if (data.FileAttachs != null && data.FileAttachs.FileRemoves != null && data.FileAttachs.FileRemoves.Count > 0)
                        ServiceFactory<DocumentRequestResultAttachmentService>().DeleteRange(data.Id, data.FileAttachs.FileRemoves);
                }

                // Insert đính kèm tương ứng với bảng kết quả
                foreach (var fileId in fileIds)
                {
                    var attachment = new DocumentRequestResultAttachmentBO()
                    {
                        FileId = fileId,
                        ResultId = data.Id
                    };
                    ServiceFactory<DocumentRequestResultAttachmentService>().Insert(attachment, false);
                }
                SaveTransaction(allowSave);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Gửi phân công từ đề nghị
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        public Guid? SaveRequestFromSuggestWriteNew(DocumentRequestBO data)
        {
            try
            {
                if (data.Id == Guid.Empty)
                {
                    data.Id = base.Insert(data);
                    return data.Id;
                }
                return null;
            }
            catch (Exception e)
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        public void SendRequestWriteNew(DocumentRequestBO data, bool allowSave = true)
        {
            data.ProcessStatus = (int)iDAS.Service.Common.Resource.RequestProcessStatus.WaitAssign;
            data.IsSend = true;
            data.SendAt = DateTime.Now;
            SaveRequestWriteNew(data);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        public void SendRequestDistribute(DocumentRequestBO data, bool allowSave = true)
        {
            data.IsSend = true;
            data.SendAt = DateTime.Now;
            SaveRequestDistribute(data);
        }
        /// <summary>
        /// Xóa yêu cầu soạn thảo
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        public void DeleteRequestWriteNew(Guid RequestId, bool allowSave = true)
        {

            try
            {
                // 1. Get data RequestDocument
                var data = base.GetById(RequestId);
                // 2. Delete document
                if (data.DocumentId != null)
                {
                    var document = ServiceFactory<DocumentService>().GetAll();
                    if (document.Any(p => p.Id == data.DocumentId) == true)
                    {
                        ServiceFactory<DocumentService>().Delete(data.DocumentId.Value, false);
                    }
                }
                base.Delete(RequestId);
                SaveTransaction(allowSave);
            }
            catch (Exception err)
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }

        }
        /// <summary>
        /// Get chi tết yêu cầu soạn thảo
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public DocumentRequestBO GetDetailRequestWriteNew(Guid RequestId)
        {
            try
            {
                var data = base.GetById(RequestId);
                //var Request = base.Get(p => p.ParentId == data.Id).FirstOrDefault();
                var employeePerformBy = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy == null ? Guid.Empty : data.PerformBy.Value);
                if (data.RequestTo == (int)iDAS.Service.Common.Resource.RequestTo.Compilator)
                {
                    data.notCheck = true;
                    data.Check = false;
                    data.EmployeePerformwrite = new DocumentEmployeeBO()
                    {
                        Id = employeePerformBy.Id,
                        Name = employeePerformBy.Name,
                        AvatarUrl = employeePerformBy.AvatarUrl,
                        RoleNames = employeePerformBy.RoleNames
                    };
                }
                else
                {
                    data.Check = true;
                    data.notCheck = false;
                    data.EmployeePerform = new DocumentEmployeeBO()
                    {
                        Id = employeePerformBy.Id,
                        Name = employeePerformBy.Name,
                        AvatarUrl = employeePerformBy.AvatarUrl,
                        RoleNames = employeePerformBy.RoleNames

                    };
                }
                var document = ServiceFactory<DocumentService>().GetById(data.DocumentId.Value);
                var temp = ServiceFactory<DocumentRequestResultAttachmentService>().GetFileIds(data.Id);
                data.FileAttachs = new FileUploadBO()
                {
                    Files = temp.ToList()
                };
                data.Document = document;
                return data;

            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Get chi tết yêu cầu phân phối
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public DocumentRequestBO GetDetailRequestDistribute(Guid RequestId)
        {
            try
            {
                var data = base.GetById(RequestId);
                var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.ApproveBy.Value);
                var document = ServiceFactory<DocumentService>().GetById(data.DocumentId.Value);
                var temp = ServiceFactory<DocumentRequestResultAttachmentService>().GetFileIds(data.Id);
                data.FileAttachs = new FileUploadBO()
                {
                    Files = temp.ToList()
                };
                data.UserId = UserId;
                data.EmployeePerformBy = employee;
                data.Document = document;
                return data;

            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Lây yêu cầu lưu trữ tài liệu
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <param name="typeRole"></param>
        /// <returns></returns>
        public DocumentRequestBO GetDataRequestArchives(Guid? DocumentId, Resource.DocumentProcessRole typeRole)
        {

            try
            {
                var data = Get(i => i.TypeRole == (int)typeRole && i.DocumentId == DocumentId).FirstOrDefault();
                if (data != null)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy != null ? data.PerformBy.Value : Guid.Empty);
                    data.EmployeePerformBy = employee;
                    return data;
                }
                else
                {
                    var result = new DocumentRequestBO();
                    result.DocumentId = DocumentId;
                    return result;
                }

            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// Lay yeu cau huy
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <param name="typeRole"></param>
        /// <returns></returns>
        public DocumentRequestBO GetDataRequestDestroy(Guid? DocumentId)
        {

            try
            {
                var data = Get(i => i.TypeDestroy == true && i.DocumentId == DocumentId).FirstOrDefault();
                if (data != null)
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy != null ? data.PerformBy.Value : Guid.Empty);
                    data.EmployeePerformBy = employee;
                    return data;
                }
                else
                {
                    var result = new DocumentRequestBO();
                    result.DocumentId = DocumentId;
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="typeRole"></param>
        /// <returns></returns>
        public IEnumerable<DocumentRequestBO> GetRequestByTypeRole(int pageIndex, int pageSize, out int count, Resource.DocumentProcessRole typeRole)
        {
            var data = Enumerable.Empty<DocumentRequestBO>();
            try
            {
                data = Get(i => i.TypeRole == (int)typeRole && (i.CreateBy == UserId || i.PerformBy == UserId) && i.IsSend == true)
                    .OrderByDescending(t => t.CreateAt)
                      .Select(i =>
                      {
                          i.UserId = UserId;
                          return i;
                      });
                count = data.Count();
                data = Page(data, pageIndex, pageSize).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public void ChangePerform(DocumentRequestBO item)
        {
            var obj = base.GetById(item.Id);
            if (obj != null)
            {
                obj.PerformBy = item.EmployeePerformBy.Id;
                base.Update(obj);
            }
        }

        /// <summary>
        /// Danh sách yêu cầu ban hành tới tôi
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentRequestBO> GetRequestPromulgateCurrentUser()
        {
            var requests = Get(i => i.TypeRole == ((int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate))
                .Where(i => i.IsSend == true && i.PerformBy == UserId);
            return requests;
        }

        public void Recover(Guid id)
        {
            var obj = base.GetById(id);
            if (obj != null)
            {
                obj.IsSend = false;
                obj.SendAt = null;
                base.Update(obj);
            }
        }

        #region Yêu cầu phân công phát sinh từ đề nghị =======================================================

        /// <summary>
        /// Yêu cầu gửi đến phân công phát sinh từ nguồn đề nghị
        /// </summary>
        /// <param name="suggestId">Id của đề nghị</param>
        /// <param name="from">Người gửi: người đề nghị, người xem xét, người phê duyệt, người biên soạn</param>
        /// <param name="source">Nguồn phát sinh: yêu cầu sửa đổi, yêu cầu viết mới, đề nghị sửa đổi, đề nghị viết mới, tự người biên soạn tạo</param>
        /// <param name="documentId"></param>
        /// <param name="fromDate"></param>
        /// <param name="endDate"></param>
        /// <param name="EstimateDate">Ngày mong muốn</param>
        /// <param name="note">Nội dung đề nghị</param>
        /// <param name="performBy">Người nhận yêu cầu/ người phân công</param>
        /// <param name="departmentId">Quy trình của phòng ban</param>
        /// <param name="isSend"></param>
        /// <param name="order">thứ tự trong quy trình</param>
        /// <param name="approveBy">Phê duyệt bởi ai</param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid CreateFromSuggest(Guid suggestId,
            Resource.RequestFrom from,
            Resource.RequestSource source,
            Guid documentId,
            DateTime? fromDate, DateTime? endDate, DateTime? EstimateDate,
            string note,
            Guid performBy,
            Guid? departmentId,
            bool isSend,
            int? order,
            Guid? approveBy,
            bool allowSave = true)
        {
            return CreateFromSuggest(suggestId, from, Resource.RequestTo.Assigner, source, documentId, fromDate, endDate, EstimateDate, note, performBy, departmentId, isSend, order, approveBy, allowSave);
        }
        /// <summary>
        /// Yêu cầu phát sinh từ đề nghị gửi cho người soạn thảo hoặc người phân công
        /// </summary>
        /// <param name="suggestId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="source"></param>
        /// <param name="documentId"></param>
        /// <param name="fromDate"></param>
        /// <param name="endDate"></param>
        /// <param name="EstimateDate"></param>
        /// <param name="note"></param>
        /// <param name="performBy"></param>
        /// <param name="departmentId"></param>
        /// <param name="isSend"></param>
        /// <param name="order"></param>
        /// <param name="approveBy"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid CreateFromSuggest(Guid suggestId,
            Resource.RequestFrom from,
            Resource.RequestTo to,
            Resource.RequestSource source,
            Guid documentId,
            DateTime? fromDate, DateTime? endDate, DateTime? EstimateDate,
            string note,
            Guid performBy,
            Guid? departmentId,
            bool isSend,
            int? order,
            Guid? approveBy,
            bool allowSave = true)
        {
            //1. kiểm tra nguồn là sửa đổi hay viết mới
            var typeWrite = source == Resource.RequestSource.RequestWrite || source == Resource.RequestSource.SuggestWrite || source == Resource.RequestSource.Compilation;
            var typeAdjust = source == Resource.RequestSource.RequestAdjust || source == Resource.RequestSource.SuggestAdjust;
            var request = new DocumentRequestBO()
            {
                DepartmentId = departmentId,
                Order = order,
                DocumentId = documentId,
                DocumentSuggestId = suggestId,
                PerformBy = performBy,
                TypeWrite = typeWrite,
                TypeAdjust = typeAdjust,
                IsSend = isSend,
                Note = note,
                StartAt = fromDate,
                EndAt = endDate,
                EstimateAt = EstimateDate,
                ApproveBy = approveBy,
                SendAt = DateTime.Now,
                TypeRole = (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit,// Áp dụng quy trình => là người phân công
                ProcessStatus = (int)Resource.RequestProcessStatus.WaitAssign, // Trạng thái mặc định là chờ phân công
                RequestFrom = (int)from,
                RequestTo = (int)to, //người nhận là người phân công
                RequestSource = (int)source,
                TypeCompile = to == Resource.RequestTo.Compilator
            };
            request.Id = base.Insert(request, allowSave);
            return request.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DocumentRequestBO GetSuggestToAssignItem(Guid id)
        {
            var data = GetAssignInfoByRequestId(id);

            return data;
        }

        #endregion
        #region Dùng chung ===================================================================================
        /// <summary>
        /// Cập nhật trạng thái của yêu cầu được tạo bởi người yêu cầu
        /// 
        /// </summary>
        /// <param name="documentId">Id tài liệu cập nhật</param>
        /// <param name="status">Trạng thái tiếp theo được cập nhật</param>
        /// <param name="allowSave"></param>
        public void UpdateProcessStatus(Guid documentId, Resource.RequestProcessStatus status, bool allowSave = true)
        {
            DocumentRequestBO rootRequest;
            var document = ServiceFactory<DocumentService>().GetById(documentId);
            // trường hợp yêu cầu viết mới tài liệu
            // Cập nhật trạng thái yêu cầu viết mới của tài liệu đó
            if (!document.ParentId.HasValue || document.ParentId == Guid.Empty)
            {
                rootRequest = Get(i => !i.ParentId.HasValue
                    && i.ParentId != Guid.Empty
                    && i.TypeWrite == true
                    && i.DocumentId == documentId).FirstOrDefault();
            }
            // Trường hợp yêu cầu sửa đổi tài liệu
            else
            {
                // Trạng thái trước trạng thái thực hiện, tài liệu chưa được tạo ra => cập nhật yêu cầu sửa đổi đối với chính tài liệu đó
                if ((int)status < (int)Resource.RequestProcessStatus.Perform)
                {
                    rootRequest = Get(i => !i.ParentId.HasValue
                        && i.ParentId != Guid.Empty
                        && i.DocumentId == documentId
                        && i.TypeAdjust == true).FirstOrDefault();
                }
                // trạng thái từ đang thực hiện: tài liệu đã được tạo ra => cập nhật yêu cầu sửa đổi đối với tài liệu cha
                else
                {
                    rootRequest = Get(i => !i.ParentId.HasValue
                        && i.ParentId != Guid.Empty
                        && i.DocumentId == document.ParentId.Value
                        && i.TypeAdjust == true).FirstOrDefault();
                }
            }
            rootRequest.ProcessStatus = (int)status;
            Update(rootRequest, allowSave);
        }
        /// <summary>
        /// Tìm yêu cầu phân công
        /// </summary>
        /// <param name="currentRequestId"></param>
        /// <returns></returns>
        private Guid GetRootRequestId(Guid currentRequestId)
        {
            var request = base.GetById(currentRequestId);
            if (request.ParentId.HasValue && request.ParentId != Guid.Empty)
            {
                return request.Id;
            }
            else
            {
                return GetRootRequestId(request.ParentId.Value);
            }
        }
        #endregion
        #region Soạn thảo ====================================================================================
        /// <summary>
        /// Danh sách yêu cầu khung soạn thảo
        /// Actor người phân công : Thu hồi yêu cầu, hủy yêu cầu
        /// Actor người biên soạn : thực hiện phân công, gửi đề nghị kiểm tra
        /// Actor người soạn thảo : không thực hiện phân công
        /// </summary>
        /// <returns></returns>
        /// 
        public IEnumerable<DocumentRequestBO> GetDataWritenArea()
        {
            // Trường hợp người tạo ra là người yêu cầu gửi soạn thảo => người thấy là người tạo ra với vai trò là người yêu cầu (trạng thái chờ thực hiện, đang thực hiện) hoặc người thực hiện với vai trò là biên soạn(trạng thái là chờ thực hiện, đang thực hiện)
            // Trường hợp người tạo là người phân công => người thấy là người tạo với vai trò là người phân công hoặc người thực hiện với vai trò biên soạn
            // Trường hợp người tạo ra là người biên soạn => chỉ thấy bản ghi nó thực hiện biên soạn
            // Trường hợp người soạn thảo: vai trò thực hiện là người soạn thảo
            // Trường hợp người thực hiện là người biên soạn => chỉ thấy bản ghi nó thực hiện với vai trò biên soạn
            var requests = Get(i => i.TypeAdjust == true || i.TypeWrite == true)
                .Where(i => i.IsSend == true && i.IsComplete != true && i.IsDestroy != true)// Điều kiện trạng thái là đã gửi, chưa hoàn thành, chưa hủy
                .Where(i => (i.PerformBy == UserId && (i.RequestTo == (int)Resource.RequestTo.Writer || i.RequestTo == (int)Resource.RequestTo.Compilator) // người thực hiện là người soạn thảo hoặc biên soạn
                    || (i.CreateBy == UserId
                        && (i.RequestFrom == (int)Resource.RequestFrom.Requestor || i.RequestFrom == (int)Resource.RequestFrom.Assigner || i.PerformBy == UserId)
                        && i.TypeCompile == true
                        && i.RequestTo == (int)Resource.RequestTo.Compilator
                    )))
                .OrderByDescending(i => i.CreateAt)
                .Select(i =>
                {
                    i.UserId = UserId;
                    return i;
                });
            return requests;
        }

        public IEnumerable<DocumentRequestBO> GetDataWritenAreaTable(int pageIndex, int pageSize, out int count, Resource.DocumentEmployeeRole role = Resource.DocumentEmployeeRole.All, Resource.RequestStatus status = Resource.RequestStatus.All)
        {
            var requests = Get(i => (i.PerformBy == UserId || i.CreateBy == UserId)
                && (i.RequestTo == (int)Resource.RequestTo.Compilator || i.RequestTo == (int)Resource.RequestTo.Writer)
                && (i.TypeAdjust == true || i.TypeWrite == true))
                .Where(i => role == Resource.DocumentEmployeeRole.All ||
                        (role == Resource.DocumentEmployeeRole.Approver ? (i.RequestFrom == (int)Resource.RequestFrom.Requestor && i.CreateBy == UserId)
                        : role == Resource.DocumentEmployeeRole.Assigner ? (i.RequestFrom == (int)Resource.RequestFrom.Assigner && i.CreateBy == UserId)
                        : role == Resource.DocumentEmployeeRole.Compiler ? (i.IsSend == true && i.RequestTo == (int)Resource.RequestTo.Compilator && i.PerformBy == UserId)
                        : role == Resource.DocumentEmployeeRole.Writer ? (i.IsSend == true && i.RequestTo == (int)Resource.RequestTo.Writer && i.PerformBy == UserId)
                        : false
                        ))
                .Where(i => status == Resource.RequestStatus.All ||
                    (
                        status == Resource.RequestStatus.Destroy ? i.IsDestroy == true
                        : status == Resource.RequestStatus.Finish ? (i.IsFinish == true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.Complete ? (i.IsComplete == true && i.IsFinish != true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.Perform ? (i.IsPerform == true && i.IsComplete != true && i.IsFinish != true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.Wait ? (i.IsSend == true && i.IsPerform != true && i.IsComplete != true && i.IsFinish != true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.New ? (i.IsSend != true && i.IsPerform != true && i.IsComplete != true && i.IsFinish != true && i.IsDestroy != true)
                        : false
                    ))
                .OrderByDescending(i => i.CreateAt).AsEnumerable();
            count = requests.Count();
            requests = Page(requests, pageIndex, pageSize);
            // Ids người tạo
            var employeeIds = requests.Select(i => i.CreateBy.Value);
            // 
            var requesters = ServiceFactory<EmployeeService>().GetByIds(employeeIds);
            // 
            var requesterTitles = ServiceFactory<EmployeeTitleService>().GetRoleIdsByEmployeeIds(employeeIds);
            //
            var departmentTitles = ServiceFactory<DepartmentTitleService>().GetByIds(requesterTitles.Select(i => i.TitleId));

            var documentIds = requests.Where(u => u.DocumentId.HasValue).Select(u => u.DocumentId.Value);
            var documents = ServiceFactory<DocumentService>().GetByIds(documentIds);
            requests = requests.Select(i =>
            {
                i.UserId = UserId;
                var document = documents.FirstOrDefault(u => u.Id == i.DocumentId.Value);
                i.Document = document;
                var requester = requesters.FirstOrDefault(u => u.Id == i.CreateBy.Value);

                var titleIds = requesterTitles.Where(u => u.EmployeeId == i.CreateBy.Value).Select(u => u.TitleId);

                var tileNames = departmentTitles.Where(u => titleIds.Contains(u.Id)).Select(u => u.Name);
                requester.RoleNames = string.Join(",", tileNames);
                i.EmployeeCreateBy = requester;
                return i;
            });
            return requests;
        }

        public IEnumerable<DocumentRequestBO> GetDataCreateByCompilator(int pageIndex, int pageSize, out int count, Resource.RequestStatus status = Resource.RequestStatus.All)
        {
            var requests = Get(i => i.CreateBy == UserId && i.TypeCompile == true && i.RequestFrom == (int)Resource.RequestFrom.Compilator)
                .Where(i => status == Resource.RequestStatus.All ||
                    (
                        status == Resource.RequestStatus.Destroy ? i.IsDestroy == true
                        : status == Resource.RequestStatus.Finish ? (i.IsFinish == true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.Complete ? (i.IsComplete == true && i.IsFinish != true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.Perform ? (i.IsPerform == true && i.IsComplete != true && i.IsFinish != true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.Wait ? (i.IsSend == true && i.IsPerform != true && i.IsComplete != true && i.IsFinish != true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.New ? (i.IsSend != true && i.IsPerform != true && i.IsComplete != true && i.IsFinish != true && i.IsDestroy != true)
                        : false
                    ))
                .OrderByDescending(i => i.CreateAt).AsEnumerable();
            count = requests.Count();
            requests = Page(requests, pageIndex, pageSize);
            // Ids nhận
            var employeeIds = requests.Where(i => i.PerformBy.HasValue).Select(i => i.PerformBy.Value);
            // 
            var performers = ServiceFactory<EmployeeService>().GetByIds(employeeIds);
            // 
            var performerTitles = ServiceFactory<EmployeeTitleService>().GetRoleIdsByEmployeeIds(employeeIds);
            //
            var departmentTitles = ServiceFactory<DepartmentTitleService>().GetByIds(performerTitles.Select(i => i.TitleId));

            var documentIds = requests.Where(u => u.DocumentId.HasValue).Select(u => u.DocumentId.Value);
            var documents = ServiceFactory<DocumentService>().GetByIds(documentIds);
            requests = requests.Select(i =>
            {
                i.UserId = UserId;
                var document = documents.FirstOrDefault(u => u.Id == i.DocumentId.Value);
                i.Document = document;
                if (i.PerformBy.HasValue && i.PerformBy != Guid.Empty)
                {
                    var performer = performers.FirstOrDefault(u => u.Id == i.PerformBy.Value);
                    var titleIds = performerTitles.Where(u => u.EmployeeId == i.PerformBy.Value).Select(u => u.TitleId);
                    var tileNames = departmentTitles.Where(u => titleIds.Contains(u.Id)).Select(u => u.Name);
                    performer.RoleNames = string.Join(",", tileNames);
                    i.EmployeePerformBy = performer;
                }
                return i;
            });
            return requests;
        }

        /// <summary>
        /// Danh sách yêu cầu soạn thảo theo yêu cầu biên soạn
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<DocumentRequestBO> GetRequestWriteByCompilation(Guid id)
        {
            var data = Get(i => i.ParentId == id && i.PerformBy != null && i.PerformBy != Guid.Empty);
            var employeeIds = data.Select(i => i.PerformBy.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetByIds(employeeIds);
            var employeeTitles = ServiceFactory<EmployeeTitleService>().GetRoleIdsByEmployeeIds(employeeIds);
            var departmentTitles = ServiceFactory<DepartmentTitleService>().GetByIds(employeeTitles.Select(i => i.TitleId).Distinct());
            data = data.Select(i =>
            {
                var performer = employees.FirstOrDefault(u => u.Id == i.PerformBy);
                var titleIds = employeeTitles.Where(u => u.EmployeeId == i.PerformBy).Select(u => u.TitleId).Distinct();
                var titles = departmentTitles.Where(u => titleIds.Contains(u.Id)).Select(u => u.Name);
                performer.RoleNames = string.Join(", ", titles);
                i.EmployeePerformBy = performer;
                return i;
            });
            return data;
        }
        /// <summary>
        /// Thông tin yêu cầu soạn thảo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DocumentRequestBO GetWriteInfoByRequestId(Guid id)
        {
            var request = GetFullInfo(id);
            var requestResult = ServiceFactory<DocumentRequestResultService>().GetSingleByRequest(id);
            request.Result = requestResult == null ? new DocumentRequestResultBO() { DocumentRequestId = request.Id, PerformBy = request.PerformBy, FileAttachs = new FileUploadBO() } : requestResult;

            return request;
        }
        /// <summary>
        /// Lưu kết quả soạn thảo
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        public DocumentRequestBO SaveRequestWrite(DocumentRequestBO data, bool allowSave = true)
        {
            //. Lấy ra người thực hiện 
            if (data.EmployeePerform != null || data.EmployeePerformBy != null)
            {
                data.PerformBy = data.EmployeePerform != null ? data.EmployeePerform.Id : data.EmployeePerformBy.Id;
            }
            // Trường hợp người thực hiện lưu kết quả
            if (data.PerformBy == UserId)
                data.IsPerform = true;
            //1. Trường hợp tạo mới bản ghi
            if (data.Id == Guid.Empty)
                //3.1 Lấy bản ghi vừa tạo
                data.Id = base.Insert(data, true);
            else
                base.Update(data, true);
            //2. Trường hợp cập nhật từ yêu cầu sửa đổi, thời điểm lưu sẽ tạo ra tài liệu mới
            if (data.TypeAdjust == true)
            {
                //2.1. Chưa tồn tại tài liệu đã sửa đổi
                DocumentBO newDocument;
                if (!data.NewDocumentId.HasValue || data.NewDocumentId == Guid.Empty)
                {
                    newDocument = ServiceFactory<DocumentService>().GetChild(data.DocumentId.Value);
                }
                else
                {
                    newDocument = ServiceFactory<DocumentService>().GetById(data.NewDocumentId.Value);
                }
                data.NewDocument = newDocument;
            }

            //3. kết quả phần soạn thảo có dữ liệu
            if (data.EmployeePerform != null || data.EmployeePerformBy != null)
            {
                data.PerformBy = data.EmployeePerform != null ? data.EmployeePerform.Id : data.EmployeePerformBy.Id;
            }
            //4. Trong trường hợp người lưu là người biên soạn, lưu kết quả trực tiếp vào tài liệu
            if (data.TypeCompile == true)
            {
                //5. Nếu là sửa đổi sẽ cập nhật tài liệu sửa đổi ngược lại sẽ cập nhật tài liệu đó
                var document = data.TypeAdjust == true ? data.NewDocument :
                    data.RequestSource == (int)Resource.RequestSource.Compilation ? data.Document :
                    ServiceFactory<DocumentService>().GetById(data.DocumentId.Value);
                if (data.Result != null)
                {
                    document.ShortContent = data.Result.Content;
                    document.FileAttachs = data.Result.FileAttachs;
                }
                //6. Cập nhật kết quả soạn thảo vào tài liệu và đính kèm của tài liệu
                if (document.Id == Guid.Empty)
                {
                    document.DepartmentId = data.DepartmentId;
                    document.Id = ServiceFactory<DocumentService>().Insert(document);
                }
                else
                {
                    ServiceFactory<DocumentService>().Update(document, true);
                }
                ServiceFactory<DocumentAttachmentService>().Update(document, true);
                if (data.RequestSource != (int)Resource.RequestSource.Compilation)
                {
                    // Trường hợp hoàn thành yêu cầu biên soạn
                    if (data.IsComplete == true)
                    {
                        UpdateProcessStatus(data.DocumentId.Value, Resource.RequestProcessStatus.WaitReview);
                    }
                    // Trường hợp thực hiện soạn thảo
                    else
                    {
                        UpdateProcessStatus(data.DocumentId.Value, Resource.RequestProcessStatus.Perform);
                    }
                }
            }
            if (data.Result != null)
            {
                data.Result.DocumentRequestId = data.Id;
                // Lưu kết quả vào bảng kết quả
                ServiceFactory<DocumentRequestResultService>().SaveResult(data.Result, true);
            }
            return data;
        }
        /// <summary>
        /// Gửi yêu cầu soạn thảo
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        public void SendRequestWrite(DocumentRequestBO item, bool allowSave = true)
        {
            SendAssign(item, allowSave);
        }
        /// <summary>
        /// Thông tin yêu cầu soạn thảo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public DocumentRequestBO GetRequestWriteFormItem(string id, Guid parentId)
        {
            DocumentRequestBO data = null;
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    var parent = GetById(parentId);
                    data = new DocumentRequestBO()
                    {
                        ParentId = parentId,
                        Parent = parent,
                        DocumentId = parent.DocumentId,
                        TypeAdjust = parent.TypeAdjust,
                        TypeWrite = parent.TypeWrite
                    };
                }
                else
                {
                    data = base.GetById(new Guid(id));
                    var employee = data == null || !data.PerformBy.HasValue ? null : ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy.Value);
                    data.EmployeePerformBy = employee;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return data;
        }

        /// <summary>
        /// Hoàn thành việc soạn thảo
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public void CompleteWrite(DocumentRequestBO data, bool allowSave = true)
        {
            //1. Đánh dấu đã hoàn thành
            data.IsComplete = true;
            //2. Lưu kết quả, trạng thái yêu cầu
            SaveRequestWrite(data);
            SaveTransaction(allowSave);
        }

        /// <summary>
        /// Người biên soạn thực hiện gửi đề nghị ban hành(đề nghị kiểm tra)
        /// </summary>
        /// <param name="responsibility"></param>
        public void SendPublishSuggest(DocumentResponsibilityBO responsibility)
        {
            var request = base.GetById(responsibility.ExtendId.Value);
            ServiceFactory<DocumentResponsibilityService>().InsertResponsibility(responsibility, true);
            if (request != null)
            {
                request.IsComplete = true;
                base.Update(request, true);
            }
            var document = ServiceFactory<DocumentService>().GetById(responsibility.DocumentId.Value);
            document.IsWaitPublish = true;
            ServiceFactory<DocumentService>().Update(document, true);
        }

        /// <summary>
        /// Duyệt kết qủa soạn thảo từ yêu cầu soạn thảo
        /// </summary>
        /// <param name="id"></param>
        public void Approve(Guid id)
        {
            var data = GetById(id);
            data.IsFinish = true;
            data.ApproveAt = DateTime.Now;
            data.IsApprove = true;
            data.IsAccept = true;
            Update(data);
        }
        /// <summary>
        /// Duyệt không đạt kết quả soạn thảo từ yêu cầu soạn thảo
        /// </summary>
        /// <param name="id"></param>
        public void Disapprove(Guid id)
        {
            var data = GetById(id);
            data.ApproveAt = DateTime.Now;
            data.IsApprove = true;
            data.IsAccept = false;
            data.IsComplete = false;
            Update(data);
        }
        /// <summary>
        /// Gửi yêu cầu theo Id yêu cầu
        /// View biên soạn tài liệu
        /// </summary>
        /// <param name="id"></param>
        public void Send(Guid id)
        {
            var data = GetById(id);
            data.IsSend = true;
            data.SendAt = DateTime.Now;
            Update(data);
        }

        /// <summary>
        /// Người biên soạn hoàn thành
        /// Cập nhật trạng thái => hoàn thành
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        public void CompleteByCompile(DocumentRequestBO item, bool allowSave = true)
        {
            item.IsComplete = true;
            SaveRequestWrite(item);
        }
        #endregion
        #region Phân công ====================================================================================
        /// <summary>
        /// Thông tin yêu cầu 
        /// Khung phân công
        /// </summary>
        /// <param name="id"></param>
        /// <param name="allowDeleted"></param>
        /// <param name="allowDefaultIfNull"></param>
        /// <returns></returns>
        public DocumentRequestBO GetAssignInfoByRequestId(Guid id)
        {
            var data = GetFullInfo(id);
            var newRequest = Get(i => i.ParentId == id).FirstOrDefault();
            if (newRequest != null && newRequest.PerformBy.HasValue)
            {
                var performer = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(newRequest.PerformBy.Value);
                newRequest.EmployeePerformBy = performer;
                if (performer != null)
                {
                    newRequest.EmployeePerform = new DocumentEmployeeBO()
                    {
                        Name = performer.Name,
                        Id = performer.Id,
                        RoleNames = performer.RoleNames,
                        AvatarUrl = performer.AvatarUrl
                    };
                }
                data.NewRequest = newRequest;
            }
            else
            {
                data.NewRequest = new DocumentRequestBO()
                {
                    DocumentId = data.DocumentId,
                    Document = data.Document,
                    ParentId = data.Id,
                    RequestSource = data.RequestSource
                };
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DocumentRequestBO GetFullInfo(Guid id)
        {
            //1. Thông tin của yêu cầu
            var request = base.GetById(id);
            try
            {
                //2. Lấy thông tin của tài liệu
                var document = request.DocumentId.HasValue ? ServiceFactory<DocumentService>().GetById(request.DocumentId.Value) : new DocumentBO();
                //3. Thông tin người thực hiện
                var employeeApprove = request.ApproveBy.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(request.ApproveBy.Value) : null;
                //4. Thông tin đề nghị
                if (request.DocumentSuggestId.HasValue)
                {
                    if (request.TypeWrite == true)
                    {
                        var suggest = ServiceFactory<DocumentWriteSuggestService>().GetById(request.DocumentSuggestId.Value);
                        var responsibilityApprove = ServiceFactory<DocumentResponsibilityService>().GetByExtend(request.DocumentSuggestId.Value)
                                                        .FirstOrDefault(i => i.RoleType.HasValue && i.RoleType == (int)Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit);
                        suggest.ApproveAt = responsibilityApprove == null ? (DateTime?)null : responsibilityApprove.PerformAt;
                        suggest.ApproveNote = responsibilityApprove == null ? string.Empty : responsibilityApprove.ApproveNote;
                        //5. Thông tin người đề nghị
                        var suggester = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(suggest.CreateBy.Value);
                        suggest.EmployeeSuggestBy = new DocumentEmployeeBO() { Name = suggester.Name, Id = suggester.Id };
                        request.Suggest = suggest;
                    }
                    if (request.TypeAdjust == true)
                    {
                        var suggest = ServiceFactory<DocumentAdjustSuggestService>().GetById(request.DocumentSuggestId.Value);
                        var responsibilityApprove = ServiceFactory<DocumentResponsibilityService>().GetByExtend(request.DocumentSuggestId.Value)
                                                       .FirstOrDefault(i => i.RoleType.HasValue && i.RoleType == (int)Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit);
                        suggest.ApproveAt = responsibilityApprove == null ? (DateTime?)null : responsibilityApprove.PerformAt;
                        suggest.ApproveNote = responsibilityApprove == null ? string.Empty : responsibilityApprove.ApproveNote;
                        //5. Thông tin người đề nghị
                        var suggester = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(suggest.CreateBy.Value);
                        suggest.EmployeeSuggestBy = new DocumentEmployeeBO() { Name = suggester.Name, Id = suggester.Id };
                        request.SuggestAdjust = suggest;
                    }
                }
                // Thông tin tài liệu mới trong trường hợp sửa đổi
                if (request.TypeAdjust == true && document != null)
                {
                    var child = ServiceFactory<DocumentService>().GetChild(request.DocumentId.Value);
                    request.NewDocument = child;
                    request.NewDocumentId = child.Id;
                }
                if (request.DepartmentId.HasValue)
                {
                    var department = ServiceFactory<DepartmentService>().GetById(request.DepartmentId.Value);
                    document.DepartmentName = department.Name;
                }
                request.Document = document;
                request.EmployeeApproveBy = employeeApprove;
                var temp = ServiceFactory<DocumentRequestResultAttachmentService>().GetFileIds(request.Id);
                request.FileAttachs = new FileUploadBO()
                {
                    Files = temp.ToList()
                };
                return request;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Lưu yêu cầu biên soạn gửi từ người phân công => người biên soạn
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        /// 
        public Guid SaveAssign(DocumentRequestBO data, bool allowSave = true)
        {
            try
            {
                var fileIds = Enumerable.Empty<Guid>();
                Guid documentResultId;
                //1. Upload đính kèm
                if (data != null && data.FileAttachs != null)
                {
                    fileIds = ServiceFactory<FileService>().UploadRange(data.FileAttachs, true);
                }
                //2. Gán Id người thực hiện từ employee perform by
                data.PerformBy = data.EmployeePerformBy != null ? data.EmployeePerformBy.Id : data.EmployeePerform.Id;
                data.ApproveBy = UserId;
                data.RequestFrom = (int)Resource.RequestFrom.Assigner;
                data.RequestTo = (int)Resource.RequestTo.Compilator;
                if (data.EmployeePerform != null)
                {
                    data.Order = data.EmployeePerform.NextOrder;
                    data.TypeRole = data.EmployeePerform.RoleType;
                }
                //data.DepartmentId = data.DepartmentId;

                //3. Trường hợp tạo mới bản ghi
                if (data.Id == Guid.Empty)
                {
                    //3.1 Lấy bản ghi vừa tạo
                    data.Id = base.Insert(data, true);
                    //3.2 Tạo mới kết quả
                    var requestResult = new DocumentRequestResultBO()
                    {
                        DocumentRequestId = data.Id,
                        PerformBy = data.PerformBy,
                        StartAt = data.StartAt,
                        EndAt = data.EndAt
                    };
                    //3.3 Insert kết quả
                    documentResultId = ServiceFactory<DocumentRequestResultService>().Insert(requestResult, true);
                }
                else
                {
                    base.Update(data, true);
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
            return data.Id;
        }
        /// <summary>
        /// Người phân công Gửi yêu cầu soạn thảo tới người thực hiện(biên soạn)
        /// Khung người phân công
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid SendAssign(DocumentRequestBO item, bool allowSave = true)
        {
            try
            {
                //1. Cập nhật thông tin đã gửi đến người thực hiện
                item.IsSend = true;
                //2. Ngày gửi
                item.SendAt = DateTime.Now;
                //3. Get Id gửi yêu cầu
                item.Id = SaveAssign(item, true);

                if (item.ParentId.HasValue)
                {
                    //4. Đưa ra thông tin yêu cầu từ người phê duyệt cho người phân công
                    var parent = base.GetById(item.ParentId.Value);
                    //5. Cập nhật thông tin yêu cầu => đang thực hiện
                    parent.IsPerform = true;
                    //6. Cập nhật thông tin yêu cầu cha => chờ thực hiện
                    parent.ProcessStatus = (int)Resource.RequestProcessStatus.WaitPerform;
                    Update(parent, true);
                }
                return item.Id;
            }
            catch (Exception e)
            {
                throw;
            }

        }
        /// <summary>
        /// Danh sách yêu cầu khung phân công
        /// Người phê duyệt: người tạo ra yêu cầu
        /// Người phân công: người phân công tạo ra yêu cầu nhưng chưa gửi đi, người phân công nhận yêu cầu nhưng chưa gửi đi(trạng thái chờ thực hiện)
        /// 
        /// Người đề nghị, xem xét gửi đến phân công không thông qua phê duyệt
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentRequestBO> GetDataAssignArea()
        {
            // Người tạo ra: với vai trò người yêu cầu(phê duyệt) gửi đến khung phân công, trạng thái là chờ thực hiện hoặc đang thực hiện
            // Người tạo ra: với vai trò người đề nghị, người xem xét gửi đến khung phân công, trạng thái là chờ thực hiện hoặc đang thực hiện
            // Người tạo ra: với vai trò phân công tự tạo ra ở khung phân công nhưng chưa gửi đi 
            // Người thực hiện: với vai trò phân công, trạng thái chờ thực hiện

            var requests = Get(i => i.TypeAdjust == true || i.TypeWrite == true) // Sửa đổi hoặc tạo mới
                                                                                 //.Where(i =>
                                                                                 //    ((i.PerformBy == UserId || (i.CreateBy == UserId && i.RequestFrom == (int)Resource.RequestFrom.Requestor))
                                                                                 //            && i.RequestTo == (int)Resource.RequestTo.Assigner && i.IsPerform != true && i.IsSend == true) // Gửi cho người phân công, trạng thái là chờ thực hiện(chờ phân công)
                                                                                 //    || (i.CreateBy == UserId && i.RequestFrom == (int)Resource.RequestFrom.Assigner && i.IsSend != true && i.IsDestroy != true)
                                                                                 //    )// Người phân công tạo ra, trạng thái là chờ gửi(mới),

                .Where(i =>
                        (i.RequestTo == (int)Resource.RequestTo.Assigner  // Gửi đến khung phân công
                            && ((i.CreateBy == UserId || (i.PerformBy == UserId && i.IsPerform != true && i.IsSend == true)) && (i.RequestFrom == (int)Resource.RequestFrom.Requestor || i.RequestFrom == (int)Resource.RequestFrom.Suggestor || i.RequestFrom == (int)Resource.RequestFrom.Reviewer) && i.IsSend == true && i.IsComplete != true) // Người tạo ra là người yêu cầu/ người đề nghị, người soạn thảo với trạng thái là chờ thực hiện(isSend = true) hoặc đang thực hiện(isComplete == true)
                            )
                        || (i.CreateBy == UserId && i.RequestFrom == (int)Resource.RequestFrom.Assigner && i.RequestSource == (int)Resource.RequestSource.Assignation && i.IsSend != true)
                    )// người phân công tạo ra nhưng chưa gửi đi
                    .OrderByDescending(i => i.CreateAt)
                    .Select(i =>
                    {
                        i.UserId = UserId;
                        return i;
                    });
            return requests;
        }
        /// <summary>
        /// Thu hồi
        /// </summary>
        /// <param name="id"></param>
        public void RevertPerform(Guid id)
        {
            var data = base.GetById(id);
            data.IsSend = false;
            Update(data, false);
            SaveTransaction();
        }
        /// <summary>
        /// Thu hồi yêu cầu biên soạn của người phân công
        /// View người phân công
        /// </summary>
        /// <param name="id"></param>
        public void RevertAssign(Guid id)
        {
            // Cập nhật trạng thái gửi yêu cầu cho phân công
            var data = base.GetById(id);
            data.IsSend = false;
            if (data.ParentId.HasValue)
            {
                var parent = base.GetById(data.ParentId.Value);
                if (parent != null)
                {
                    parent.IsPerform = false;
                    Update(parent, false);
                }
            }
            data.PerformBy = Guid.Empty;
            // Cập nhật trạng thái yêu cầu phân công thành chờ giao
            UpdateProcessStatus(data.DocumentId.Value, Resource.RequestProcessStatus.WaitAssign);
            Update(data, false);
            SaveTransaction();
        }

        /// <summary>
        /// Thực hiện thu hồi từ suggest ID
        /// </summary>
        /// <param name="id"></param>
        public void RevertAssignBySuggestId(Guid suggestId)
        {
            var data = Get(i => i.DocumentSuggestId == suggestId).FirstOrDefault();
            base.Delete(data.Id, false);
        }

        /// <summary>
        /// Hủy
        /// </summary>
        /// <param name="id"></param>
        public void DestroyAssign(Guid id)
        {
            var data = base.GetById(id);
            data.IsDestroy = true;
            Update(data);
        }
        /// <summary>
        /// Chuyển phân công
        /// </summary>
        /// <param name="id"></param>
        public void TransferAssign(Guid id)
        {
            var data = base.GetById(id);
            data.IsSend = false;
            Update(data);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="role"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public IEnumerable<DocumentRequestBO> GetDataAssignAreaTable(int pageIndex, int pageSize, out int count, Resource.DocumentEmployeeRole role = Resource.DocumentEmployeeRole.All, Resource.RequestStatus status = Resource.RequestStatus.All)
        {
            var requests = Get(i => ((i.RequestFrom == (int)Resource.RequestFrom.Assigner && i.CreateBy == UserId)
                || (i.PerformBy == UserId && i.RequestTo == (int)Resource.RequestTo.Assigner)) //gửi đến khung yêu cầu hoặc người yêu cầu tạo ra và gửi đi
                && (i.TypeAdjust == true || i.TypeWrite == true)
                && i.DocumentId.HasValue)// Sửa đổi hoặc tạo mới, gửi đến khung phân công
                .Where(i => role == Resource.DocumentEmployeeRole.All ||
                    (role == Resource.DocumentEmployeeRole.Approver ? i.RequestFrom == (int)Resource.RequestFrom.Requestor
                    : role == Resource.DocumentEmployeeRole.Assigner ? ((i.RequestTo == (int)Resource.RequestTo.Assigner && i.IsSend == true) || i.RequestFrom == (int)Resource.RequestFrom.Assigner)
                    : role == Resource.DocumentEmployeeRole.Suggester ? i.RequestFrom == (int)Resource.RequestFrom.Suggestor
                    : role == Resource.DocumentEmployeeRole.Reviewer ? i.RequestFrom == (int)Resource.RequestFrom.Reviewer
                    : false))
                .Where(i => status == Resource.RequestStatus.All ||
                    (
                        status == Resource.RequestStatus.Destroy ? i.IsDestroy == true
                        : status == Resource.RequestStatus.Finish ? (i.IsFinish == true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.Complete ? (i.IsComplete == true && i.IsFinish != true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.Perform ? (i.IsPerform == true && i.IsComplete != true && i.IsFinish != true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.Wait ? (i.IsSend == true && i.IsPerform != true && i.IsComplete != true && i.IsFinish != true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.New ? (i.IsSend != true && i.IsPerform != true && i.IsComplete != true && i.IsFinish != true && i.IsDestroy != true)
                        : false
                    ))
                    .OrderByDescending(i => i.CreateAt).AsEnumerable();
            count = requests.Count();
            requests = Page(requests, pageIndex, pageSize);
            // Ids người tạo
            var employeeIds = requests.Select(i => i.CreateBy.Value);
            // 
            var requesters = ServiceFactory<EmployeeService>().GetByIds(employeeIds);
            // 
            var requesterTitles = ServiceFactory<EmployeeTitleService>().GetRoleIdsByEmployeeIds(employeeIds);
            //
            var departmentTitles = ServiceFactory<DepartmentTitleService>().GetByIds(requesterTitles.Select(i => i.TitleId));

            var documentIds = requests.Where(u => u.DocumentId.HasValue).Select(u => u.DocumentId.Value);
            var documents = ServiceFactory<DocumentService>().GetByIds(documentIds);
            requests = requests.Select(i =>
            {
                i.UserId = UserId;
                var document = documents.FirstOrDefault(u => u.Id == i.DocumentId.Value);
                i.Document = document;
                var requester = requesters.FirstOrDefault(u => u.Id == i.CreateBy.Value);
                var titleIds = requesterTitles.Where(u => u.EmployeeId == i.CreateBy.Value).Select(u => u.TitleId);
                var tileNames = departmentTitles.Where(u => titleIds.Contains(u.Id)).Select(u => u.Name);
                requester.RoleNames = string.Join(",", tileNames);
                i.EmployeeCreateBy = requester;
                return i;
            });
            return requests;
        }

        public IEnumerable<DocumentRequestBO> GetDataCreateByAssigner(int pageIndex, int pageSize, out int count, Resource.RequestStatus status = Resource.RequestStatus.All)
        {
            var requests = Get(i => (i.RequestFrom == (int)Resource.RequestFrom.Assigner && i.CreateBy == UserId) //gửi đến khung yêu cầu hoặc người yêu cầu tạo ra và gửi đi
                && (i.TypeAdjust == true || i.TypeWrite == true)
                && i.DocumentId.HasValue)
                .Where(i => status == Resource.RequestStatus.All ||
                    (
                        status == Resource.RequestStatus.Destroy ? i.IsDestroy == true
                        : status == Resource.RequestStatus.Finish ? (i.IsFinish == true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.Complete ? (i.IsComplete == true && i.IsFinish != true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.Perform ? (i.IsPerform == true && i.IsComplete != true && i.IsFinish != true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.Wait ? (i.IsSend == true && i.IsPerform != true && i.IsComplete != true && i.IsFinish != true && i.IsDestroy != true)
                        : status == Resource.RequestStatus.New ? (i.IsSend != true && i.IsPerform != true && i.IsComplete != true && i.IsFinish != true && i.IsDestroy != true)
                        : false
                    ))
                    .OrderByDescending(i => i.CreateAt).AsEnumerable();
            count = requests.Count();
            requests = Page(requests, pageIndex, pageSize);
            // Ids nhận
            var employeeIds = requests.Where(i => i.PerformBy.HasValue).Select(i => i.PerformBy.Value);
            // 
            var performers = ServiceFactory<EmployeeService>().GetByIds(employeeIds);
            // 
            var performerTitles = ServiceFactory<EmployeeTitleService>().GetRoleIdsByEmployeeIds(employeeIds);
            //
            var departmentTitles = ServiceFactory<DepartmentTitleService>().GetByIds(performerTitles.Select(i => i.TitleId));

            var documentIds = requests.Where(u => u.DocumentId.HasValue).Select(u => u.DocumentId.Value);
            var documents = ServiceFactory<DocumentService>().GetByIds(documentIds);
            requests = requests.Select(i =>
            {
                i.UserId = UserId;
                var document = documents.FirstOrDefault(u => u.Id == i.DocumentId.Value);
                i.Document = document;
                if (i.PerformBy.HasValue && i.PerformBy != Guid.Empty)
                {
                    var performer = performers.FirstOrDefault(u => u.Id == i.PerformBy.Value);
                    var titleIds = performerTitles.Where(u => u.EmployeeId == i.PerformBy.Value).Select(u => u.TitleId);
                    var tileNames = departmentTitles.Where(u => titleIds.Contains(u.Id)).Select(u => u.Name);
                    performer.RoleNames = string.Join(",", tileNames);
                    i.EmployeePerformBy = performer;
                }
                return i;
            });
            return requests;
        }
        #endregion
        #region Phân công thực hiện yêu cầu viết mới =========================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Guid SendRequestWriteByAssigner(DocumentRequestBO request)
        {
            request.IsSend = true;
            request.SendAt = DateTime.Now;
            SaveRequestWriteByAssigner(request);
            return request.Id;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Guid SaveRequestWriteByAssigner(DocumentRequestBO request)
        {
            var fileIds = Enumerable.Empty<Guid>();
            //1. Nếu Id = empty: thực hiện thêm mới tài liệu, thêm mới yêu cầu
            if (request != null && request.FileAttachs != null && request.FileAttachs.FileAttachments.Any())
            {
                fileIds = ServiceFactory<FileService>().UploadRange(request.FileAttachs, false);
            }
            request.PerformBy = request.EmployeePerform.Id;
            request.Order = request.EmployeePerform.NextOrder;
            request.TypeRole = request.EmployeePerform.RoleType;
            request.CheckerBy = request.EmployeeCheck.Id;
            request.ApproverBy = request.EmployeeApprove.Id;
            request.PromulgaterBy = request.EmployeePromulgate.Id;
            if (request.Id == Guid.Empty)
            {
                request.Document.DepartmentId = request.DepartmentId;
                request.DocumentId = ServiceFactory<DocumentService>().Insert(request.Document, true);
                request.Id = Insert(request, true);
            }
            else
            {
                Update(request, true);
            }

            foreach (var fileId in fileIds)
            {
                var attachment = new DocumentRequestResultAttachmentBO()
                {
                    FileId = fileId,
                    ResultId = request.Id
                };
                ServiceFactory<DocumentRequestResultAttachmentService>().Insert(attachment, false);
            }
            return request.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DocumentRequestBO GetFormRequestWriteByAssigner(string id)
        {
            DocumentRequestBO item;
            if (string.IsNullOrEmpty(id))
            {
                item = new DocumentRequestBO()
                {
                    RequestFrom = (int)Resource.RequestFrom.Assigner,
                    RequestTo = (int)Resource.RequestTo.Compilator,
                    RequestSource = (int)Resource.RequestSource.Assignation,
                    Document = new DocumentBO(),
                    ApproveBy = UserId
                };
            }
            else
            {
                item = GetById(new Guid(id));
                var document = ServiceFactory<DocumentService>().GetById(item.DocumentId.Value);
                if (item.PerformBy.HasValue)
                {
                    var employeePerform = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.PerformBy.Value);
                    item.EmployeePerform = new DocumentEmployeeBO()
                    {
                        Id = employeePerform.Id,
                        Name = employeePerform.Name,
                        AvatarUrl = employeePerform.AvatarUrl,
                        RoleNames = employeePerform.RoleNames
                    };
                }
                if (item.CheckerBy.HasValue)
                {
                    var employeePerform = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.CheckerBy.Value);
                    item.EmployeeCheck = new DocumentEmployeeBO()
                    {
                        Id = employeePerform.Id,
                        Name = employeePerform.Name,
                        AvatarUrl = employeePerform.AvatarUrl,
                        RoleNames = employeePerform.RoleNames
                    };
                }
                if (item.ApproverBy.HasValue)
                {
                    var employeePerform = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.ApproverBy.Value);
                    item.EmployeeApprove = new DocumentEmployeeBO()
                    {
                        Id = employeePerform.Id,
                        Name = employeePerform.Name,
                        AvatarUrl = employeePerform.AvatarUrl,
                        RoleNames = employeePerform.RoleNames
                    };
                }
                if (item.PromulgaterBy.HasValue)
                {
                    var employeePerform = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.PromulgaterBy.Value);
                    item.EmployeePromulgate = new DocumentEmployeeBO()
                    {
                        Id = employeePerform.Id,
                        Name = employeePerform.Name,
                        AvatarUrl = employeePerform.AvatarUrl,
                        RoleNames = employeePerform.RoleNames
                    };
                }
                var temp = ServiceFactory<DocumentRequestResultAttachmentService>().GetFileIds(item.Id);
                item.FileAttachs = new FileUploadBO()
                {
                    Files = temp.ToList()
                };
            }
            return item;
        }

        public void RevertRequestWriteByAssigner(Guid id)
        {
            var request = base.GetById(id);
            request.IsSend = false;
            base.Update(request);
        }
        public void CancelRequestWriteByAssigner(Guid id)
        {
            var request = base.GetById(id);
            request.IsDestroy = true;
        }
        public void DeleteRequestWriteByAssigner(Guid id)
        {
            base.Delete(id);
        }
        #endregion
        #region Tạo tài liệu khung soạn thảo =================================================================
        public DocumentRequestBO GetFormWriteByComiplator(string id)
        {
            DocumentRequestBO data;
            if (string.IsNullOrEmpty(id))
            {
                data = new DocumentRequestBO()
                {
                    PerformBy = UserId,
                    IsSend = true,
                    SendAt = DateTime.Now,
                    RequestSource = (int)Resource.RequestSource.Compilation,
                    RequestFrom = (int)Resource.RequestFrom.Compilator,
                    RequestTo = (int)Resource.RequestTo.Compilator,
                    TypeWrite = true,
                    TypeCompile = true,
                    TypeRole = (int)Resource.DocumentProcessRole.Write,
                    Document = new DocumentBO()
                };
            }
            else
            {
                data = base.GetById(new Guid(id));
                var document = ServiceFactory<DocumentService>().GetById(data.DocumentId.Value);
                data.Document = document;
            }
            return data;
        }
        public Guid SaveDocumentByCompilator(DocumentRequestBO request)
        {
            try
            {
                request.PerformBy = UserId;
                request.IsSend = true;
                request.IsPerform = true;
                request.RequestSource = (int)Resource.RequestSource.Compilation;
                request.RequestFrom = (int)Resource.RequestFrom.Compilator;
                request.RequestTo = (int)Resource.RequestTo.Compilator;
                request.TypeWrite = true;
                request.TypeCompile = true;
                request.TypeRole = (int)Resource.DocumentProcessRole.Write;

                if (request.Id == Guid.Empty)
                {
                    request.SendAt = DateTime.Now;
                    request.StartAt = DateTime.Now;
                    request.Document.DepartmentId = request.DepartmentId;
                    request.DocumentId = ServiceFactory<DocumentService>().Insert(request.Document, true);
                    ServiceFactory<DocumentAttachmentService>().Update(request.FileAttachs, request.DocumentId.Value, true);
                    request.Id = base.Insert(request, true);
                }
                else
                {
                    ServiceFactory<DocumentService>().Update(request.Document, true);
                    ServiceFactory<DocumentAttachmentService>().Update(request.Document, true);
                    base.Update(request, true);
                }
                return request.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DocumentRequestBO SendDocumentByCompilator(DocumentRequestBO request)
        {
            request.IsComplete = true;
            SaveDocumentByCompilator(request);
            return request;
        }

        public void CompleteDocumentByCompilator(DocumentRequestBO request)
        {
            try
            {
                request.IsComplete = true;
                request.IsFinish = true;
                SaveDocumentByCompilator(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Xóa yêu cầu và tài liệu bởi request id
        /// </summary>
        /// <param name="requestId"></param>
        public void DeleteDocumentByRequestId(Guid requestId)
        {
            try
            {
                var request = GetById(requestId);
                if (request != null && request.DocumentId.HasValue)
                {
                    ServiceFactory<DocumentService>().Delete(request.DocumentId.Value, false);
                }
                base.Delete(requestId, false);
                SaveTransaction();
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion
        public IEnumerable<DocumentRequestBO> GetDistributeRequest()
        {
            var data = Enumerable.Empty<DocumentRequestBO>();
            data = base.GetQuery().Where(i => i.TypeRole == (int)Common.Resource.DocumentProcessRole.DistributeRequester
                && (i.CreateBy == UserId || i.PerformBy == UserId)
                && i.IsSend == true)
                .OrderByDescending(t => t.CreateAt)
                .AsEnumerable()
                  .Select(i =>
                  {
                      i.UserId = UserId;
                      return i;
                  });
            return data;
        }

    }
}