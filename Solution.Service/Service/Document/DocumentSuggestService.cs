﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public class DocumentSuggestService : BaseService<DocumentSuggestDTO, DocumentSuggestBO>, IDocumentSuggestService
    {
        const int _minOrder = 0;
        public IEnumerable<DocumentSuggestBO> GetByCurrentUser(int pageIndex, int pageSize, out int totalCount, iDAS.Service.Common.Resource.DocumentProcessRole roleType, Resource.DocumentSuggestStatusType statusType, Resource.DocumentSuggestType suggestType)
        {
            var suggestDatas = Get(i => i.SuggestType == (int)suggestType
                                    && (statusType == Resource.DocumentSuggestStatusType.All || i.StatusType == (int)statusType)
                                  );
            if (roleType == iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate)
            {
                var dataRoleSuggest = suggestDatas.Where(i => i.SuggestBy == UserId)
                                .Select(i =>
                                {
                                    i.RoleType = (int)Resource.DocumentProcessRole.Promulgate;
                                    return i;
                                }
                                        );
                var suggestResponsibilitys = ServiceFactory<DocumentSuggestResponsibilityService>().GetBySuggests(suggestDatas.Select(i => i.Id))
                                           .Where(i => i.ApproveBy == UserId && !(i.IsNew == null && i.IsApprove == null));
                //var data = dataRoleSuggest.Union(suggestDatas.Where(i => suggestResponsibilitys.Select(r => r.DocumentSuggestId).Contains(i.Id)));
                var data = dataRoleSuggest.Union(suggestResponsibilitys.Join(suggestDatas, suggestResponsibility => suggestResponsibility.DocumentSuggestId, suggestData => suggestData.Id
                      , (suggestResponsibility, suggestdata) => new DocumentSuggestBO()
                      {
                          Id = (Guid)suggestResponsibility.DocumentSuggestId,
                          Order = suggestdata.Order,
                          SuggestAt = suggestdata.SuggestAt,
                          StatusType = suggestdata.StatusType,
                          SuggestType = suggestdata.SuggestType,
                          RoleType = suggestResponsibility.RoleType,
                          CreateAt = suggestdata.CreateAt,
                      }));
                totalCount = data.Count();
                data = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
                return data;
            }
            else if (roleType == Resource.DocumentProcessRole.SuggesterWriteAndEdit)
            {
                var data = suggestDatas.Where(i => i.SuggestBy == UserId)
                                .Select(i =>
                                {
                                    i.RoleType = (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit;
                                    return i;
                                }
                                        );
                totalCount = data.Count();
                data = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
                return data;
            }
            else
            {
                var suggestResponsibilitys = ServiceFactory<DocumentSuggestResponsibilityService>().GetBySuggests(suggestDatas.Select(i => i.Id))
                                          .Where(i => i.ApproveBy == UserId && i.RoleType == (int)roleType && !(i.IsNew == null && i.IsApprove == null));
                var data = suggestResponsibilitys.Join(suggestDatas, suggestResponsibility => suggestResponsibility.DocumentSuggestId, suggestData => suggestData.Id
                      , (suggestResponsibility, suggestdata) => new DocumentSuggestBO()
                      {
                          Id = (Guid)suggestResponsibility.DocumentSuggestId,
                          Order = suggestdata.Order,
                          SuggestAt = suggestdata.SuggestAt,
                          StatusType = suggestdata.StatusType,
                          SuggestType = suggestdata.SuggestType,
                          RoleType = suggestResponsibility.RoleType,
                          CreateAt = suggestdata.CreateAt,
                      });
                totalCount = data.Count();
                data = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
                return data;
            }
        }
        public DocumentSuggestBO GetDetail(Guid id, bool allowGetSuggest = true, bool allowGetDocument = true)
        {
            var data = GetById(id);
            if (data.SuggestBy != null && allowGetSuggest)
                data.EmployeeSuggest = ServiceFactory<EmployeeService>().GetById(data.SuggestBy.Value);
            return data;
        }
        public int? GetStatus(Guid id)
        {
            var data = GetById(id);
            if (data != null)
            {
                return data.StatusType;
            }
            return null;
        }
        public override Guid Insert(DocumentSuggestBO data, bool allowSave = true)
        {
            data.StatusType = (int)Resource.DocumentSuggestStatusType.New;
            data.SuggestBy = UserId;
            data.Id = base.Insert(data, false);
            SaveTransaction(allowSave);
            return data.Id;
        }
        public Guid InsertAndSaveRole(DocumentSuggestBO data, bool allowSave = true)
        {
            data.StatusType = (int)Resource.DocumentSuggestStatusType.New;
            data.SuggestBy = UserId;
            data.Id = base.Insert(data, false);
            // Lưu thông tin người gửi
            if (data.EmployeeSendTo != null)
            {
                var suggestSend = new DocumentSuggestResponsibilityBO()
                {
                    DocumentSuggestId = data.Id,
                    RoleType = (int)data.EmployeeSendTo.RoleType,
                    ApproveBy = data.EmployeeSendTo.Id,
                    Order = data.EmployeeSendTo.NextOrder,
                };
                ServiceFactory<DocumentSuggestResponsibilityService>().Insert(suggestSend, false);
            }
            SaveTransaction(allowSave);
            return data.Id;
        }
        public void UpdateAndSaveRole(DocumentSuggestBO data, bool allowSave = true)
        {
            data.SuggestBy = UserId;
            base.Update(data, false);
            if (data.EmployeeSendTo != null)
            {
                var performs = ServiceFactory<DocumentSuggestResponsibilityService>().GetBySuggest(data.Id);
                var suggestSend = performs.Where(i => i.RoleType == data.EmployeeSendTo.RoleType).FirstOrDefault();
                if (suggestSend != null && data.EmployeeSendTo != null)
                {  // Lưu thông tin người gửi
                    suggestSend.ApproveBy = data.EmployeeSendTo.Id;
                    suggestSend.RoleType = (int)data.EmployeeSendTo.RoleType;
                    suggestSend.Order = data.EmployeeSendTo.NextOrder;
                    ServiceFactory<DocumentSuggestResponsibilityService>().Update(suggestSend, false);
                }
                else
                {
                    suggestSend = new DocumentSuggestResponsibilityBO()
                   {
                       DocumentSuggestId = data.Id,
                       RoleType = (int)data.EmployeeSendTo.RoleType,
                       ApproveBy = data.EmployeeSendTo.Id,
                       Order = data.EmployeeSendTo.NextOrder,
                   };
                    ServiceFactory<DocumentSuggestResponsibilityService>().Insert(suggestSend, false);
                }
            }
            SaveTransaction(allowSave);
            return;
        }
        /// <summary>
        /// Cập nhật trạng thái
        /// </summary>
        /// <param name="id"></param>
        /// <param name="statusType"></param>
        public void UpdateStatus(Guid id, int? statusType, int? order, bool allowSave = true)
        {
            var data = base.GetById(id);
            data.StatusType = statusType;
            data.Order = order;
            base.Update(data, allowSave: allowSave);
        }
        public override void Delete(Guid id, bool allowSave = true)
        {
            base.Delete(id);
            var reponsibilies = ServiceFactory<DocumentSuggestResponsibilityService>().Get().Where(i => i.DocumentSuggestId == id).Select(i => i.Id);
            ServiceFactory<DocumentSuggestResponsibilityService>().DeleteRange(reponsibilies);
        }
        public void Cancel(Guid id)
        {
            var obj = base.GetById(id);
            if (obj != null)
                obj.StatusType = (int)Common.Resource.DocumentSuggestStatusType.Destroy;
            base.Update(obj);
        }
    }
}
