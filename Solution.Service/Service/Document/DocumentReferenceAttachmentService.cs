﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentReferenceAttachmentService : BaseService<DocumentReferenceAttachmentDTO, DocumentReferenceAttachmentBO>, IDocumentReferenceAttachmentService
    {
        public IEnumerable<Guid> InsertRange(FileUploadBO fileUpload, Guid referenceId, bool allowSave = true)
        {
            var fileIds = ServiceFactory<FileService>().UploadRange(fileUpload);
            var data = new List<DocumentReferenceAttachmentBO>();
            foreach (var item in fileIds)
            {
                data.Add(new DocumentReferenceAttachmentBO()
                {
                    FileId = item,
                    ReferenceId = referenceId
                });
            }
            base.InsertRange(data, allowSave);
            return fileIds;
        }

        public void DeleteRange(Guid ReferenceId, IEnumerable<Guid> fileIds)
        {
            var ids = Get(i => i.ReferenceId == ReferenceId && fileIds.Contains(i.FileId.Value)).Select(i => i.Id);
            base.DeleteRange(ids);
        }

        public IEnumerable<DocumentReferenceAttachmentBO> GetByReferenceId(Guid referenceId)
        {
            var data = Get(i => i.ReferenceId == referenceId);
            return data;
        }
        public IEnumerable<Guid> GetFile(Guid referenceId)
        {
            var data = Get(i => i.ReferenceId.Value == referenceId)
                .Select(
                i => i.FileId.Value
                );
            return data;
        }

        public IEnumerable<DocumentReferenceAttachmentBO> GetByReferenceIds(IEnumerable<Guid> referenceIds)
        {
            var data = Get(i => referenceIds.Contains(i.ReferenceId.Value));
            return data;
        }
    }
}
