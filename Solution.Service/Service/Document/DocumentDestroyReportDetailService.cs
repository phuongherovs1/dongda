﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentDestroyReportDetailService : BaseService<DocumentDestroyReportDetailDTO, DocumentDestroyReportDetailBO>, IDocumentDestroyReportDetailService
    {
        /// <summary>
        /// Danh sách chi tiết của biên bản
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentDestroyReportDetailBO> GetByReportId(int pageIndex, int pageSize, out int count, Guid reportId)
        {
            try
            {
                var data = Get(p => p.DocumentDestroyReportId == reportId);
                count = data.Count();
                data = Page(data, pageIndex, pageSize)
                    .Select(i =>
                    {
                        // Lấy ra thông tin người tham gia hủy
                        if (i.EmployeeId.HasValue)
                        {
                            var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.EmployeeId.Value);
                            i.EmployeeName = employee.Name;
                            i.EmployeeUrl = employee.AvatarUrl;
                            var title = ServiceFactory<EmployeeTitleService>().Get(p => p.EmployeeId == i.EmployeeId.Value).FirstOrDefault();
                            if (title != null)
                            {
                                var result = ServiceFactory<DepartmentTitleService>().GetById(title.TitleId);
                                i.Title = result == null ? string.Empty : result.Name;
                                if (result.DepartmentId.HasValue)
                                {
                                    var department = ServiceFactory<DepartmentService>().GetById(result.DepartmentId.Value);
                                    i.DepartmentName = department == null ? string.Empty : department.Name;
                                }
                            }
                        }
                        return i;
                    });
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Tạo chi tiết của biên bản
        /// </summary>
        /// <param name="reportId"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public IEnumerable<Guid> Create(Guid reportId, string jsonData = "")
        {
            try
            {
                var result = Enumerable.Empty<Guid>();
                var reportDetails = JsonConvert.DeserializeObject<List<DocumentDestroyReportDetailBO>>(jsonData);
                if (reportDetails != null && reportDetails.Count() > 0)
                {
                    var ids = ServiceFactory<DocumentDestroyReportDetailService>().Get(p => p.DocumentDestroyReportId == reportId).Select(p => p.Id);
                    if (ids != null && ids.Count() > 0)
                        base.DeleteRange(ids, true);
                    var details = new List<DocumentDestroyReportDetailBO>();
                    foreach (var item in reportDetails)
                    {
                        item.DocumentDestroyReportId = reportId;
                        details.Add(item);
                    }
                    result = InsertRange(details, true);
                }
                return result;
            }
            catch
            {
                throw;
            }
        }
    }
}
