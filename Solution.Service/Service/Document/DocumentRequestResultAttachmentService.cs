﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentRequestResultAttachmentService : BaseService<DocumentRequestResultAttachmentDTO, DocumentRequestResultAttachmentBO>, IDocumentRequestResultAttachmentService
    {
        public void DeleteRange(Guid requestResultId, IEnumerable<Guid> fileIds)
        {
            var ids = Get(i => i.ResultId == requestResultId && fileIds.Contains(i.FileId.Value)).Select(i => i.Id);
            base.DeleteRange(ids);
        }

        public IEnumerable<Guid> GetFileIds(Guid documentRequestResultId)
        {
            return base.Get(t => t.ResultId == documentRequestResultId).Select(t => t.FileId.HasValue ? t.FileId.Value : Guid.Empty);
        }

    }
}
