﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentPublishSuggestService : BaseService<DocumentPublishSuggestDTO, DocumentPublishSuggestBO>, IDocumentPublishSuggestService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="suggest"></param>
        public void Approve(DocumentPublishSuggestBO suggest)
        {
            suggest.IsApprove = true;
            base.Update(suggest, false);
            this.SaveTransaction();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="suggest"></param>
        /// <param name="destinations"></param>
        public Guid Insert(DocumentPublishSuggestBO suggest, string destinationString, bool allowSave = true)
        {
            suggest.Id = Insert(suggest, false);
            ServiceFactory<DocumentDestinationService>().InsertRange(destinationString, suggest.Id, allowSave);
            SaveTransaction(allowSave);
            return suggest.Id;
        }

        /// <summary>
        /// Lấy giá trị đang chọn
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public DocumentPublishSuggestBO CreateDefault(Guid documentId, string id = "")
        {
            var document = ServiceFactory<DocumentService>().GetById(documentId);
            // Lấy tên và màu sắc mức độ bảo mật
            if (document.SecurityId.HasValue && document.SecurityId != Guid.Empty)
            {
                var sercurity = ServiceFactory<DocumentSecurityService>().GetById(document.SecurityId.Value);
                document.Security = sercurity;
            }
            // Lấy tên danh mục
            if (document.CategoryId.HasValue && document.CategoryId != Guid.Empty)
            {
                var category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId.Value);
                document.CategoryName = category.Name;
            }
            DocumentPublishSuggestBO data;
            if (!string.IsNullOrEmpty(id))
            {
                data = GetById(new Guid(id));
                // Hiển thị tên người phê duyệt
                if (data.ApproveBy.HasValue && data.ApproveBy != Guid.Empty)
                {
                    var employeeId = data.ApproveBy;
                    data.EmployeeApproveBy = base.GetEmployee(employeeId.Value);
                }
                data.Document = document;
            }
            else
            {
                data = new DocumentPublishSuggestBO()
                {
                    Document = document,
                    DocumentId = documentId
                };
            }
            // Lấy file đính kèm
            var temp = ServiceFactory<DocumentAttachmentService>().GetByDocument(document.Id);
            document.FileAttachs = new FileUploadBO()
            {
                Files = temp.ToList()
            };
            return data;
        }

        /// <summary>
        /// Gửi duyệt ban hành
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Guid Send(DocumentPublishSuggestBO item, string jsonData = "", bool allowSave = true)
        {
            try
            {
                item.IsSend = true;
                Save(item, jsonData, allowSave);
            }
            catch (Exception)
            {
                throw;
            }
            return item.Id;
        }

        /// <summary>
        /// Danh sách đề nghị ban hành tạo bởi người đăng nhập hệ thống
        /// Danh sách này dùng chung cho 2 view:
        /// 1. Khi isSuggest = true: Lấy danh sách đề nghị được tạo bởi User đăng nhập
        /// 2. Khi isSuggest = false: Lấy danh sách User đăng nhập có quyền phê duyệt
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="approveStatus">Lọc theo trạng thái</param>
        /// <param name="isSuggest">isSuggest = true: Lấy danh sách đề nghị được tạo bởi User đăng nhập, isSuggest = false: Lấy danh sách User đăng nhập có quyền phê duyệt</param>
        /// <returns></returns>
        public IEnumerable<DocumentPublishSuggestBO> GetCreateByCurrentUser(int pageIndex, int pageSize, out int count, Resource.ApproveStatus approveStatus = Resource.ApproveStatus.All, bool isSuggest = true)
        {
            var data = Enumerable.Empty<DocumentPublishSuggestBO>();
            if (isSuggest == true)// danh sách đề nghị được tạo bởi User đăng nhập
            {
                data = Get(i => i.CreateBy == UserId);
            }
            else// danh sách User đăng nhập có quyền phê duyệt
            {
                data = Get(i => i.ApproveBy == UserId)
                       .Where(i => approveStatus == Resource.ApproveStatus.Wait ? (i.IsSend == true && i.IsApprove != true)
                            : approveStatus == Resource.ApproveStatus.Approve ? (i.IsApprove == true && i.IsAccept == true)
                            : approveStatus == Resource.ApproveStatus.Reject ? (i.IsApprove == true && i.IsAccept != true)
                            : true);
            }
            count = data.Count();
            data = Page(data, pageIndex, pageSize).Select(
                i =>
                {
                    // Người phê duyệt
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.ApproveBy.Value);
                    i.EmployeeApproveBy = employee;
                    // Người đề nghị
                    var suggestor = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.CreateBy.Value);
                    i.EmployeeSuggestBy = suggestor;
                    i.SuggestName = suggestor.Name;
                    i.SuggestAvatarUrl = suggestor.AvatarUrl;
                    var document = ServiceFactory<DocumentService>().GetById(i.DocumentId.Value);
                    i.Document = document;
                    // Trách nhiệm
                    i.TextRole = "Ban hành";
                    // Kết quả ban hành
                    i.ResultPublish = i.IsApprove == true && i.IsAccept == true ? "Đạt" : i.IsApprove == true && i.IsAccept == false ? "Không đạt" : string.Empty;
                    return i;
                }
                ).ToList();

            return data;
        }

        public IEnumerable<DocumentPublishSuggestBO> GetApproveByCurrentUser(int pageIndex, int pageSize, out int count, Resource.ApproveStatus approveStatus = Resource.ApproveStatus.All)
        {
            var data = Enumerable.Empty<DocumentPublishSuggestBO>();

            data = Get(i => i.ApproveBy == UserId)
                   .Where(i => approveStatus == Resource.ApproveStatus.Wait ? (i.IsSend == true && i.IsApprove != true)
                        : approveStatus == Resource.ApproveStatus.Approve ? (i.IsApprove == true && i.IsAccept == true)
                        : approveStatus == Resource.ApproveStatus.Reject ? (i.IsApprove == true && i.IsAccept != true)
                        : true);
            count = data.Count();
            data = Page(data, pageIndex, pageSize).Select(
                i =>
                {
                    // Người phê duyệt
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.ApproveBy.Value);
                    i.EmployeeApproveBy = employee;
                    // Người đề nghị
                    var suggestor = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.CreateBy.Value);
                    i.EmployeeSuggestBy = suggestor;
                    var suggestName = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.CreateBy.Value);
                    i.SuggestName = suggestName.Name;
                    var document = ServiceFactory<DocumentService>().GetById(i.DocumentId.Value);
                    i.Document = document;
                    return i;
                }
                ).ToList();

            return data;
        }

        /// <summary>
        /// Lưu thông tin đề nghị ban hành
        /// </summary>
        /// <param name="data"></param>
        /// <param name="jsonData"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid Save(DocumentPublishSuggestBO data, string jsonData = "", bool allowSave = true)
        {
            try
            {
                if (data.Document.FileAttachs != null)
                {
                    //upload file
                    var fileIds = ServiceFactory<FileService>().UploadRange(data.Document.FileAttachs, false);
                    //Insert file upload
                    foreach (var file in fileIds)
                    {
                        var obj = new DocumentAttachmentBO();
                        obj.DocumentId = data.Document.Id;
                        obj.FileId = file;
                        ServiceFactory<DocumentAttachmentService>().Insert(obj, false);
                    }
                }
                data.DocumentId = data.Document.Id;
                data.ApproveBy = data.EmployeeApproveBy.Id;
                if (data.Id == Guid.Empty)
                {
                    data.Id = base.Insert(data, false);
                }
                else
                {
                    data.IsSend = true;
                    base.Update(data, false);
                }
                ServiceFactory<DocumentReferenceService>().InsertRange(data.Document.Id, jsonData, false);
                SaveTransaction(allowSave);
            }
            catch
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }

            return data.Id;
        }

        /// <summary>
        /// Duyệt đề nghị ban hành
        /// </summary>
        /// <param name="publishSuggest"></param>
        /// <param name="jsonData"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid Approve(DocumentPublishSuggestBO publishSuggest, string jsonData = "", bool allowSave = true)
        {
            try
            {
                publishSuggest.IsApprove = true;
                publishSuggest.ApproveAt = DateTime.Now;
                publishSuggest.ApproveBy = UserId;
                base.Update(publishSuggest, false);
                ServiceFactory<DocumentDestinationService>().InsertRange(jsonData, publishSuggest.DocumentId.Value);
                SaveTransaction(allowSave);
            }
            catch
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
            return publishSuggest.Id;
        }

        #region Đề nghị ban hành view danh sách tài liệu 

        /// <summary>
        /// Form đề nghị ban hành
        /// </summary>
        /// <param name="id"></param>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public DocumentPublishSuggestBO GetSuggestForm(string id, Guid documentId)
        {
            DocumentPublishSuggestBO data;
            if (string.IsNullOrEmpty(id))
            {
                var document = ServiceFactory<DocumentService>().GetById(documentId);
                data = new DocumentPublishSuggestBO()
                {
                    Document = document,
                    DocumentId = documentId
                };
            }
            else
            {
                data = base.GetById(new Guid(id));
                var document = ServiceFactory<DocumentService>().GetById(data.DocumentId.Value);
                data.Document = document;
            }
            return data;
        }

        public void SendPublishSuggest(DocumentPublishSuggestBO item)
        {
            var receiver = new DocumentEmployeeBO()
            {
                Id = item.EmployeeApprove.Id,
                RoleType = (int)Resource.DocumentProcessRole.Promulgate,
                DepartmentId = item.Document.DepartmentId
            };
            var response = new DocumentResponsibilityBO()
            {
                EmployeeRecive = receiver,
                DepartmentId = item.Document.DepartmentId,
                ContentSuggest = item.Note,
                DocumentId = item.DocumentId,
                SendTo = UserId,
                PerformBy = item.EmployeeApprove.Id,
                PublishDate = item.SuggestAt,
                ApplyDate = item.ApplyAt,
                RoleType = (int)Resource.DocumentProcessRole.Promulgate,
                IsNew = true
            };

            ServiceFactory<DocumentResponsibilityService>().InsertRole(response);

        }
        #endregion
    }
}
