﻿using iDAS.DataAccess;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentDestroyService : BaseService<DocumentDestroyDTO, DocumentDestroyBO>, IDocumentDestroyService
    {
        /// <summary>
        /// Yêu cầu hủy
        /// View người ban hành
        /// </summary>
        /// <returns></returns>
        public int CountRequest()
        {
            var summary = Get(i => i.CreateBy == UserId).Count();
            return summary;
        }
    }
}
