﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentCategoryService : BaseService<DocumentCategoryDTO, DocumentCategoryBO>, IDocumentCategoryService
    {

        public override Guid Insert(DocumentCategoryBO data, bool allowSave = true)
        {
            var checkItem = Get(i =>
                i.ParentId == data.ParentId
                && i.DepartmentId == data.DepartmentId
                && i.Name.ToUpper() == data.Name.ToUpper()
                && i.IsExternal == data.IsExternal
                && i.IsInternal == data.IsInternal
                && i.IsDistribute == data.IsDistribute
                ).Any();
            if (checkItem)
                throw new DataHasBeenExistedException();
            return base.Insert(data, allowSave);
        }
        public override void Update(DocumentCategoryBO data, bool allowSave = true)
        {
            var checkItem = Get(i => i.Id != data.Id
                && i.ParentId == data.ParentId
                && i.DepartmentId == data.DepartmentId
                && i.Name.ToUpper() == data.Name.ToUpper()
                && i.IsExternal == data.IsExternal
                && i.IsInternal == data.IsInternal
                && i.IsDistribute == data.IsDistribute
                ).Any();
            if (checkItem)
                throw new DataHasBeenExistedException();
            base.Update(data, allowSave);
        }
        /// <summary>
        /// Danh sách danh mục tài liệu nội bộ
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentCategoryBO> GetTreeDocumentCategoryInternal(Guid? id, Guid departmentId)
        {
            try
            {
                var data = Get(i => i.DepartmentId == departmentId && i.IsExternal != true);
                return GetTreeDocumentCategory(data, id, departmentId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Danh sách tài liệu bên ngoài
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentCategoryBO> GetTreeDocumentCategoryExternal(Guid? id, Guid departmentId)
        {
            try
            {
                var data = Get(i => i.DepartmentId == departmentId && i.IsExternal == true);
                return GetTreeDocumentCategory(data, id, departmentId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="parentId"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        private IEnumerable<DocumentCategoryBO> GetTreeDocumentCategory(IEnumerable<DocumentCategoryBO> data, Guid? parentId, Guid departmentId)
        {
            //1. Danh sách danh mục theo phòng ban cha
            var filterByParent = data.Where(i => parentId.HasValue ? i.ParentId == parentId.Value : !i.ParentId.HasValue);
            //2. Danh sách danh mục tài liệu còn lại
            var remainCategory = data.Except(filterByParent);
            //3. 
            var documents = ServiceFactory<DocumentService>().Get(i => (i.CategoryId.HasValue && filterByParent.Select(u => u.Id).Contains(i.CategoryId.Value)) && filterByParent.Select(u => u.Id).Contains(i.CategoryId.Value));
            var result = filterByParent
                .Select(i =>
                {
                    i.IsParent = remainCategory.Any(t => t.ParentId == i.Id);
                    var countDocument = documents.Count(u => u.CategoryId == i.Id);
                    i.CountDocument = countDocument;
                    return i;
                });
            return result;
        }

        public IEnumerable<DocumentCategoryBO> GetTreeDocumentCategory(Guid? parentId, Guid departmentId, Common.Resource.DocumentCategoryType type)
        {
            //1. Danh sách tài liệu theo phòng ban
            var dataByDepartment = Get(i => i.DepartmentId == departmentId)
                .Where(i => type == Resource.DocumentCategoryType.Internal ? i.IsInternal == true
                    : type == Resource.DocumentCategoryType.External ? i.IsExternal == true
                    : i.IsDistribute == true);

            //2. Danh sách danh mục theo danh mục cha
            var data = dataByDepartment.Where(i => parentId.HasValue ? i.ParentId == parentId.Value : !i.ParentId.HasValue);
            var dataIds = data.Select(u => u.Id);
            //3. Danh sách danh mục tài liệu còn lại, dùng để đếm số lượng danh mục con
            var remainCategory = dataByDepartment.Except(data);
            //4. Danh sách tài liệu theo danh mục
            //var documents = ServiceFactory<DocumentService>()
            //    .Get(i => (i.CategoryId.HasValue && dataIds.Contains(i.CategoryId.Value)) || ( !string.IsNullOrEmpty( i.DistributeCategoryIds);
            var result = data
                .Select(i =>
                {
                    i.IsParent = remainCategory.Any(t => t.ParentId == i.Id);
                    var countDocument = i.IsDistribute != true ? ServiceFactory<DocumentService>().CountByCategory(i.Id) : ServiceFactory<DocumentService>().CountByDistributeCategory(i.Id);
                    i.CountDocument = countDocument;
                    return i;
                });
            return result;
        }
        /// <summary>
        /// Lấy danh sách category con theo documentcategoryId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<DocumentCategoryBO> GetChildrenByDocumentCategory(Guid? id)
        {
            try
            {
                var categories = new List<DocumentCategoryBO>();
                //1. Lấy category con theo documentcategoryId
                var children = base.Get(u => u.ParentId == id);
                //2. Nếu category khác null thì lấy danh sách các category con theo documentcategoryId đã lấy ở bước 1 ngược lại không làm gì
                if (children != null && children.Count() > 0)
                {
                    foreach (var child in children)
                    {
                        categories.AddRange(GetChildrenByDocumentCategory(child.Id));
                    }
                    categories.AddRange(children);
                }
                //3. trả ra danh sách category đã lấy ở bước 2
                return categories.Distinct();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public IEnumerable<DocumentCategoryBO> GetChildren(Guid? parentId)
        {
            var data = base.Get(i => i.ParentId == parentId);
            return data;
        }
        /// <summary>
        /// Danh sách danh mục được ở phòng ban khác mà có tài liệu được phân tới tôi
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentCategoryBO> GetForeignDistribute(Guid? department)
        {
            var data = Enumerable.Empty<DocumentCategoryBO>();
            try
            {
                //1. Danh sách phòng ban đang ở
                var departments = ServiceFactory<DepartmentService>().GetDepartmentsByCurrentUser();
                //2. Danh sách danh mục ở phòng ban khác
                //var foreignCategories = Get(i => !departments.Select(u => u.Id).Distinct().Contains(i.DepartmentId.Value));
                //3. Danh sách Id tài liệu được phân phối tới tôi
                var distributeDocumentIds = ServiceFactory<DocumentDistributeService>().GetDocumentIdsDistributeByEmployee(department);
                //4. Danh sách danh mục chứa tài liệu trên
                var categories = ServiceFactory<DocumentService>().GetByIds(distributeDocumentIds).Select(i => i.CategoryId.Value).Distinct()
                    //.Where(i => foreignCategories.Select(u => u.Id).Contains(i))
                    ;
                data = GetByIds(categories);
            }
            catch (Exception)
            {
                throw;
            }
            return data;
        }

        public IEnumerable<DocumentCategoryBO> GetByDepartment(Guid? departmentId, Common.Resource.DocumentCategoryType type)
        {
            var data = Get(i => i.DepartmentId == departmentId
                && type == Resource.DocumentCategoryType.Internal ? i.IsInternal == true
                : type == Resource.DocumentCategoryType.External ? i.IsExternal == true
                : i.IsDistribute == true)
                .OrderBy(p => p.Name);
            return data;
        }

        public IEnumerable<EmployeeBO> GetReviewEmployee(Guid id)
        {
            try
            {
                var documentCategory = GetById(id);
                //var roleReview = documentCategory.ReviewRole;
                //if (roleReview != null)
                //{
                //    var data = ServiceFactory<EmployeeService>().GetByRoleIDs(new List<Guid> { id });
                //    return data;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }
        public IEnumerable<EmployeeBO> GetApproveEmployee(Guid id)
        {
            try
            {
                var documentCategory = GetById(id);
                //var roleApprove = documentCategory.ApproveRole;
                //if (roleApprove != null)
                //{
                //    var data = ServiceFactory<EmployeeService>().GetByRoleIDs(new List<Guid> { roleApprove.Value });
                //    return data;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }


        /// <summary>
        /// lẤY TẤT CẢ DANH MỤC NỘI BỘ TRONG PHÒNG BAN CỦA NÓ
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentCategoryBO> GetCategorybyDepartment()
        {
            var data = new List<DocumentCategoryBO>();
            var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(UserId);
            var title = ServiceFactory<EmployeeTitleService>().Get(m => m.EmployeeId == employee.Id);
            var departmentTitle = ServiceFactory<DepartmentTitleService>().GetByIds(title.Select(i => i.TitleId).Distinct());
            foreach (var item in departmentTitle)
            {
                var categorys = Get(p => p.DepartmentId == item.DepartmentId && p.IsInternal == true);
                foreach (var cate in categorys)
                {
                    data.Add(cate);
                }
            }
            return data;
        }
        /// <summary>
        /// lẤY TẤT CẢ DANH MỤC BÊN NGOÀI TRONG PHÒNG BAN CỦA NÓ
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentCategoryBO> GetCategorybyDepartmentExternal()
        {
            var data = new List<DocumentCategoryBO>();
            var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(UserId);
            var title = ServiceFactory<EmployeeTitleService>().Get(m => m.EmployeeId == employee.Id);
            var departmentTitle = ServiceFactory<DepartmentTitleService>().GetByIds(title.Select(i => i.TitleId).Distinct());
            foreach (var item in departmentTitle)
            {
                var categorys = Get(p => p.DepartmentId == item.DepartmentId && p.IsExternal == true);
                foreach (var cate in categorys)
                {
                    data.Add(cate);
                }
            }
            return data;
        }
        /// <summary>
        /// Lấy danh mục 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentCategoryBO> GetCategorybyDepartmentDistribute(Guid Id)
        {
            var data = base.Get(i => i.DepartmentId == Id && i.IsDistribute == true);
            return data;
        }
        /// <summary>
        /// Lấy DANH MUC NOI BO
        /// </summary>
        /// <returns></returns>
        public DocumentCategoryBO GetByCategory(Guid Id)
        {
            var document = ServiceFactory<DocumentService>().GetById(Id);
            var Category = ServiceFactory<DocumentCategoryService>().GetById(document.CategoryId == null ? Guid.Empty : document.CategoryId.Value);
            Category.NoteCategory = document.NoteCategory;
            Category.DocumentCode = document.Code;
            Category.IsExternal = document.IsExternal;
            return Category;
        }

        /// <summary>
        /// lAY DANH MUC PHAN PHOI
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public DocumentCategoryBO GetByCategoryDistribute(Guid Id)
        {
            var data = new DocumentCategoryBO();
            var Category = base.GetAll();
            var distribute = ServiceFactory<DocumentDistributeService>().Get(i => i.DocumentId == Id && i.DepartmentId != null).Select(p => p.DepartmentId).Distinct();
            var result = Category.Where(p => distribute.Contains(p.DepartmentId) && p.IsDistribute == true);

            var document = ServiceFactory<DocumentService>().GetById(Id);
            if (document.DistributeCategoryIds != null)
            {
                if (document.DistributeCategoryIds.ToString().Trim().Contains(','))
                {
                    string[] CategoryIds = document.DistributeCategoryIds.Trim().Split(',');
                    for (int i = 0; i < CategoryIds.Length; i++)
                    {
                        foreach (var item in result)
                        {
                            if (CategoryIds[i].ToString().Trim().Equals(item.Id.ToString().Trim()))
                            {
                                data = item;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    foreach (var item in result)
                    {
                        if (document.DistributeCategoryIds.ToString().Trim().Equals(item.Id.ToString().Trim()))
                        {
                            data = item;
                            break;
                        }
                    }
                }
            }
            data.NoteCategory = document.NoteCategory;
            data.DistributeCategoryIds = data.Id;
            data.DocumentId = Id;
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="departmentId"></param>
        /// <param name="isExternal"></param>
        /// <returns></returns>
        public int CountCategoryByDepartment(Guid departmentId, Common.Resource.DocumentCategoryType type)
        {
            var result = Get(i => i.DepartmentId == departmentId && !i.ParentId.HasValue)
                .Count(i => type == Resource.DocumentCategoryType.Internal ? i.IsInternal == true : type == Resource.DocumentCategoryType.External ? i.IsExternal == true : i.IsDistribute == true);
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="allowSave"></param>
        public override void Delete(Guid id, bool allowSave = true)
        {
            if (CheckHasAnyDocumentTree(id))
                throw new CategoryHasDocumentException();
            base.Delete(id, allowSave);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool CheckHasAnyDocumentTree(Guid id)
        {
            //1. Kiểm tra xem có tài liệu nào có ở trong chính danh mục đó không
            var checkExistDocument = ServiceFactory<DocumentService>().Get(i => i.CategoryId == id).Any();
            if (checkExistDocument)
                return true;
            else
            {
                // Lấy ra danh sách các danh mục con
                var childrenCategory = Get(i => i.ParentId == id).ToList();
                // nếu không có danh mục con thì báo không có bất cứ tài liệu nào nằm trong danh mục hay danh danh mục con
                if (childrenCategory.Count == 0)
                    return false;
                else
                {
                    foreach (var child in childrenCategory)
                    {
                        // Đệ quy để kiểm tra tồn tại tài liệu trong danh mục con
                        if (CheckHasAnyDocumentTree(child.Id))
                            return true;
                    }
                }
            }
            return false;
        }
    }
}
