﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentAdjustHistoryService : BaseService<DocumentAdjustHistoryDTO, DocumentAdjustHistoryBO>, IDocumentAdjustHistoryService
    {
        public IEnumerable<DocumentAdjustHistoryBO> GetByDocumentId(Guid documentId)
        {
            var data = Get(i => i.DocumentId == documentId).Select(i =>
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.AdjustBy.Value);
                    i.EmployeeAdjustBy = employee;
                    return i;
                });
            return data;
        }
    }
}
