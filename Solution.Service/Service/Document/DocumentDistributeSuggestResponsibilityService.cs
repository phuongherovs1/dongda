﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentDistributeSuggestResponsibilityService : BaseService<DocumentDistributeSuggestResponsibilityDTO, DocumentDistributeSuggestResponsibilityBO>, IDocumentDistributeSuggestResponsibilityService
    {
        public IEnumerable<DocumentWriteSuggestResponsibilityBO> GetBySuggest(Guid suggestId)
        {
            var resposibily = Get(i => i.DocumentDistributeSuggestId == suggestId)
                                   .Select(item => new DocumentWriteSuggestResponsibilityBO
                                   {
                                       Id = item.Id,
                                       RoleType = item.RoleType,
                                       IsApprove = item.IsApprove,
                                       IsAccept = item.IsAccept,
                                       IsComplete = item.IsComplete,
                                       EmployeeSend = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo((Guid)item.PerformBy),
                                       ApproveNote = item.ApproveNote,
                                       ContentSuggest = item.ContentSuggest,
                                       CreateAt = item.CreateAt,
                                   }
                            ).OrderBy(i => i.CreateAt);
            return resposibily;

        }
        public void InsertResponsibility(DocumentDistributeSuggestResponsibilityBO item)
        {
            var id = InsertRole(item, true);
            if (item.Id != null && item.Id != Guid.Empty)
                UpdateCompleteAndLock(item.Id, false);
            base.SaveTransaction();
        }
        public System.Guid InsertRole(DocumentDistributeSuggestResponsibilityBO data, bool allowSave = true)
        {
            var item = new DocumentDistributeSuggestResponsibilityBO();
            item.DocumentDistributeSuggestId = data.DocumentDistributeSuggestId;
            item.PerformBy = data.EmployeeRecive.Id;
            item.RoleType = data.EmployeeRecive.RoleType;
            item.DepartmentId = data.EmployeeRecive.DepartmentId;
            item.ContentSuggest = data.ContentSuggest;
            item.Order = data.EmployeeRecive.NextOrder;
            item.IsNew = true;
            item.PerformAt = DateTime.Now;
            return base.Insert(item, allowSave);
        }
        public void UpdateCompleteAndLock(Guid id, bool allowSave = true)
        {
            var obj = base.GetById(id);
            if (obj != null)
            {
                obj.IsComplete = true;
                obj.IsLock = true;
                base.Update(obj, allowSave);
            }
        }
        public IEnumerable<DocumentDistributeSuggestResponsibilityBO> GetResponsibilityForDasboard()
        {
            var userId = UserId;
            var data = base.GetQuery().Where(i => i.PerformBy.HasValue && i.PerformBy.Value == userId && i.IsDelete == false && i.IsComplete != true)
                       // .Select(
                       // item => new DocumentDistributeSuggestResponsibilityBO()
                       // {
                       //     Id = item.Id,
                       //     IsNew = item.IsNew,
                       //     IsComplete = item.IsComplete,
                       //     IsApprove = item.IsApprove,
                       //     IsAccept = item.IsAccept,
                       //     IsLock = item.IsLock,
                       //     CreateAt = item.CreateAt,
                       //     RoleType = item.RoleType,
                       //     // EmployeeSuggest = ServiceFactory<EmployeeService>().GetById(item.CreateBy.Value)
                       // }
                       //)
                       .ToList();
            return data;
        }
        public DocumentDistributeSuggestResponsibilityBO GetDetailPerform(Guid id)
        {
            var data = GetById(id);
            if (data.DocumentDistributeSuggestId.HasValue)
            {
                var suggest = ServiceFactory<DocumentDistributeSuggestService>().GetDetailPerform(data.DocumentDistributeSuggestId.Value);
                data.Document = suggest.Document;
                data.EmployeeSuggest = suggest.EmployeeSuggest;
                data.DepartmentId = suggest.DepartmentId;
                data.ApplyAt = suggest.ApplyAt;
                data.Quantity = suggest.Quantity;
                data.ReasonOfSuggest = suggest.ReasonOfSuggest;
            }
            return data;
        }
        public void ApproveSave(DocumentDistributeSuggestResponsibilityBO data)
        {
            var update = GetById(data.Id);
            update.IsApprove = true;
            update.IsAccept = data.IsAccept;
            update.ApproveNote = data.ApproveNote;
            update.ApplyAt = DateTime.Now;
            if (update.RoleType == (int)Resource.DocumentProcessRole.ApproveDocument && data.IsAccept == true)
            {
                update.IsComplete = true;
            }
            Update(update);
        }
    }
}
