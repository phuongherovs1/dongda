﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DocumentProcessService : BaseService<DocumentProcessDTO, DocumentProcessBO>, IDocumentProcessService
    {
        public override DocumentProcessBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var obj = base.GetById(id, allowDeleted, allowDefaultIfNull);
            if (obj.DepartmentTitleId.HasValue)
                obj.ObjectName = ServiceFactory<DepartmentTitleService>().GetById(obj.DepartmentTitleId.Value).Name;
            else
                obj.ObjectName = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(obj.EmployeeId.Value).Name;
            return obj;
        }
        public IEnumerable<DocumentProcessBO> GetAll(int pageIndex, int pageSize, out int count, Guid departmentId, int process)
        {
            var result = base.Get(p => p.DepartmentId == departmentId && p.TypeProcess == process);
            count = result.Count();
            var data = Page(result.OrderBy(i => i.Order), pageIndex, pageSize).ToList();
            foreach (var item in data)
            {
                if (item.DepartmentTitleId.HasValue && item.DepartmentTitleId != Guid.Empty)
                {
                    item.DepartmentTitle = ServiceFactory<DepartmentTitleService>().GetById(item.DepartmentTitleId.Value);
                }
                else
                {
                    item.Employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.EmployeeId.Value);
                }
            }
            return data;
        }
        public int GetMaxOrder(Guid departmentId, int process, int roleType)
        {
            int max = 0;
            var obj = base.Get(p => p.DepartmentId == departmentId && p.TypeProcess == process && p.TypeRole == roleType);

            if (obj.Count() > 0)
                max = obj.Max(c => c.Order).Value;
            else
            {
                var last = base.Get(p => p.DepartmentId == departmentId && p.TypeProcess == process && p.TypeRole < roleType);
                if (last.Count() > 0)
                    max = last.Max(c => c.Order.Value);
            }
            return max;
        }
        public bool UpdateOrder(bool up, Guid roleId)
        {
            try
            {
                var obj = base.GetById(roleId);
                int orderLastOrFirt = 0;
                if (up)
                {
                    orderLastOrFirt = obj.Order.HasValue ? obj.Order.Value - 1 : 0;
                }
                else
                {
                    orderLastOrFirt = obj.Order.HasValue ? obj.Order.Value + 1 : 1;
                }
                var objUpOrDown = base
                    .Get()
                    .Where(c => c.DepartmentId == obj.DepartmentId)
                    .Where(c => c.TypeProcess == obj.TypeProcess)
                    .Where(c => c.Order == orderLastOrFirt)
                    .FirstOrDefault();
                if (objUpOrDown.TypeRole != obj.TypeRole)
                    return false;
                else
                {

                    if (objUpOrDown != null)
                    {
                        objUpOrDown.Order = obj.Order;
                        base.Update(objUpOrDown, false);
                        obj.Order = up ? obj.Order - 1 : obj.Order + 1;
                        base.Update(obj, false);
                        base.SaveTransaction();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private IEnumerable<Guid> getTitleIds(int process, int roletype, Guid departmentId)
        {
            var titleIds = new List<Guid>();
            titleIds.AddRange(base.Get(t => t.DepartmentId == departmentId
                        && t.TypeProcess == process && t.TypeRole == roletype
                        && t.DepartmentTitleId.HasValue && t.DepartmentTitleId != Guid.Empty)
                        .Select(t => t.DepartmentTitleId.Value)
                        );
            return titleIds;
        }
        private IEnumerable<Guid> getEmployeeIds(int process, int roletype, Guid departmentId)
        {
            var employeeIds = new List<Guid>();
            employeeIds.AddRange(base.Get(t => t.DepartmentId == departmentId
                        && t.TypeProcess == process && t.TypeRole == roletype
                        && t.EmployeeId.HasValue && t.DepartmentTitleId != Guid.Empty)
                        .Select(t => t.EmployeeId.Value)
                        );
            return employeeIds;
        }

        public IEnumerable<Guid> GetTitleIds(int process, int roletype)
        {
            var titleIds = new List<Guid>();
            var departmentIds = ServiceFactory<UserService>().GetDepartmentByCurrentUser();
            foreach (var departmentId in departmentIds)
            {
                if (CheckExitProcess(departmentId, process))
                {
                    titleIds.AddRange(getTitleIds(process, roletype, departmentId));
                }
                else
                {
                    var groupIds = new List<Guid>();
                    ServiceFactory<DepartmentService>().GetParentByDepartment(departmentId, ref groupIds);
                    foreach (var item in groupIds)
                    {
                        if (CheckExitProcess(item, process))
                        {
                            titleIds.AddRange(getTitleIds(process, roletype, item));
                            break;
                        }
                    }
                }
            }
            return titleIds;
        }
        public IEnumerable<Guid> GetEmployeeIds(int process, int roletype)
        {
            var employeeIds = new List<Guid>();
            var departmentIds = ServiceFactory<UserService>().GetDepartmentByCurrentUser();
            foreach (var departmentId in departmentIds)
            {
                if (CheckExitProcess(departmentId, process))
                {
                    employeeIds.AddRange(getEmployeeIds(process, roletype, departmentId));
                }
                else
                {
                    var groupIds = new List<Guid>();
                    ServiceFactory<DepartmentService>().GetParentByDepartment(departmentId, ref groupIds);
                    foreach (var item in groupIds)
                    {
                        if (CheckExitProcess(item, process))
                        {
                            employeeIds.AddRange(getEmployeeIds(process, roletype, item));
                            break;
                        }
                    }
                }
            }
            return employeeIds;
        }
        public IEnumerable<Guid> GetEmployeeByDepartment(Guid departmentId, int process, int roletype)
        {
            var employeeIds = new List<Guid>();
            if (CheckExitProcess(departmentId, process))
            {
                employeeIds.AddRange(getEmployeeIds(process, roletype, departmentId));
            }
            else
            {
                var groupIds = new List<Guid>();
                ServiceFactory<DepartmentService>().GetParentByDepartment(departmentId, ref groupIds);
                foreach (var item in groupIds)
                {
                    if (CheckExitProcess(item, process))
                    {
                        employeeIds.AddRange(getEmployeeIds(process, roletype, item));
                        break;
                    }
                }
            }
            return employeeIds;
        }
        public IEnumerable<Guid> GetTitleByDepartment(Guid departmentId, int process, int roletype)
        {
            var titleIds = new List<Guid>();
            if (CheckExitProcess(departmentId, process))
            {
                titleIds.AddRange(getTitleIds(process, roletype, departmentId));
            }
            else
            {
                var groupIds = new List<Guid>();
                ServiceFactory<DepartmentService>().GetParentByDepartment(departmentId, ref groupIds);
                foreach (var item in groupIds)
                {
                    if (CheckExitProcess(item, process))
                    {
                        titleIds.AddRange(getTitleIds(process, roletype, item));
                        break;
                    }
                }
            }
            return titleIds;
        }
        public bool CheckExitProcess(Guid? departmentId, int process)
        {
            var typeProcess = process;
            if (process == (int)Resource.DocumentTypeProcess.DistributeSuggest || process == (int)Resource.DocumentTypeProcess.CancelSuggest)
                typeProcess = (int)Resource.DocumentTypeProcess.WriteAndRelation;
            return base.Get().Any(t => t.DepartmentId == departmentId && t.TypeProcess == typeProcess && t.IsDelete == false);
        }
        public bool CheckExitRoleInProcess(Guid? departmentId, int process, int roletype)
        {
            return base.Get(t => t.DepartmentId == departmentId && t.TypeProcess == process && t.TypeRole == roletype).Count() > 0;
        }

        /// <summary>
        /// Kiểm tra người đăng nhập có quy trình hay không
        /// </summary>
        /// <param name="departmentId"></param>
        /// <param name="employeeId"></param>
        /// <param name="process"></param>
        /// <param name="roletype"></param>
        /// <returns></returns>
        public bool CheckExitEmployeeInProcess(Guid? departmentId, Guid? employeeId, int process, int roletype)
        {
            return base.Get(t => t.DepartmentId == departmentId && t.EmployeeId == employeeId && t.TypeProcess == process && t.TypeRole == roletype).Count() > 0;
        }
        public DocumentProcessBO GetByOrder(Guid departmentId, int process, int order)
        {
            var typeProcess = process;
            if (process == (int)Resource.DocumentTypeProcess.DistributeSuggest)
            {
                typeProcess = (int)Resource.DocumentTypeProcess.WriteAndRelation;
            }
            var data = base.Get(t => t.DepartmentId == departmentId && t.TypeProcess == typeProcess && t.Order == order && t.IsDelete == false)
                .FirstOrDefault();
            if (data != null)
                return data;
            return new DocumentProcessBO();
        }
        private DocumentProcessBO getProcessOrderCurrent(Guid departmentId, int process, int order)
        {
            var processItem = base.Get(t => t.DepartmentId == departmentId && t.TypeProcess == process && t.Order == order)
                .FirstOrDefault();
            return processItem;
        }
        private int getMaxOrder(Guid departmentId, int process)
        {
            return base.Get(t => t.DepartmentId == departmentId && t.TypeProcess == process).Max(t => t.Order.HasValue ? t.Order.Value : 0);
        }
        private DocumentProcessBO getByOrder(Guid departmentId, int process, int order)
        {
            return base.Get(t => t.DepartmentId == departmentId && t.TypeProcess == process && t.Order == order).FirstOrDefault();
        }
        private void NextProcess(int process, ref int processNext)
        {
            processNext = process++;

            return;
        }

        private bool CheckUserExist(Guid departmentId, int process, int roleType, ref int order)
        {
            var titleIds = ServiceFactory<EmployeeTitleService>().GetTitleIdsByCurrentUser();
            var record = base.Get(c => c.DepartmentId == departmentId && c.TypeProcess == process && c.TypeRole == roleType && (c.EmployeeId == UserId
                || (c.DepartmentTitleId.HasValue && titleIds.Contains(c.DepartmentTitleId.Value))))
                .OrderByDescending(c => c.Order)
                .FirstOrDefault();

            if (record != null)
            {
                order = record.Order.Value;
                return true;
            }
            else
                return false;
        }
        public DocumentProcessBO GetDistributeRoleNext(Guid departmentId, int order = 0)
        {
            var roleDistributeNext = Get(i => i.DepartmentId == departmentId && i.TypeProcess == (int)Resource.DocumentTypeProcess.WriteAndRelation
                                         && (i.TypeRole == (int)Resource.DocumentProcessRole.Check || i.TypeRole == (int)Resource.DocumentProcessRole.ApproveDocument))
                                     .Where(i => i.Order > order).OrderBy(i => i.Order)
                                     .Select(i => new DocumentProcessBO()
                                     {
                                         Id = i.Id,
                                         Order = i.Order,
                                         TypeRole = i.TypeRole,
                                     }
                                            )
                                     .FirstOrDefault();
            return roleDistributeNext;
        }
        public void GetRoleNext(Guid departmentId, int process, int order, ref DocumentProcessBO roleNext, int? startRole = null)
        {
            var max = getMaxOrder(departmentId, process);
            if (order > max)
                return;
            else
            {
                //Trường hợp quy trình đề nghị viết mới, sửa đổi tài liệu chỉ thiết lập người đề nghị thì chuyển trực tiếp đến người kiểm tra thuộc quy trình soạn thảo ban hành
                if (max == 1 && process == 0 && getByOrder(departmentId, process, max).TypeRole == (int)Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit)
                {
                    GetRoleNext(departmentId, process + 1, 0, ref roleNext, (int)Common.Resource.DocumentProcessRole.Write);
                    return;
                }
                else
                {
                    var processItem = getProcessOrderCurrent(departmentId, process, order);
                    //Trường hợp user tồn tại trong trách nhiệm xem xét đề nghị viết mới và sửa đổi ở bước cuối thì không phải gửi xem xét nữa
                    if (processItem != null && (processItem.TypeRole == (int)Common.Resource.DocumentProcessRole.ReviewWriteAndEdit || processItem.TypeRole == (int)Common.Resource.DocumentProcessRole.DestroyReviewer || processItem.TypeRole == (int)Common.Resource.DocumentProcessRole.DistributeSuggestReviewer))
                    {
                        int orderx = 0;
                        if (CheckUserExist(departmentId, process, processItem.TypeRole.Value, ref orderx))
                        {
                            processItem = getProcessOrderCurrent(departmentId, process, orderx + 1);
                        }
                    }
                    if (processItem != null && (startRole == null || (startRole != null && processItem.TypeRole > startRole)))
                    {
                        roleNext = processItem;
                        return;
                    }
                    else
                    {
                        order++;
                        GetRoleNext(departmentId, process, order, ref roleNext, startRole);
                    }
                }
            }
        }
        public DocumentProcessPerformBO GetProcess(Guid departmentId, iDAS.Service.Common.Resource.DocumentTypeProcess process)
        {
            var obj = new DocumentProcessPerformBO();
            if (base.Get().Any(p => p.DepartmentId == departmentId && p.TypeProcess == (int)process))
            {
                obj.IsExits = true;
                var data = base.Get(p => p.DepartmentId == departmentId && p.TypeProcess == (int)process);
                foreach (var item in data)
                {
                    obj.Items.Add(new DocumentProcessItemBO
                    {
                        IsTitle = item.DepartmentTitleId.HasValue ? true : false,
                        ObjectId = item.DepartmentTitleId.HasValue ? item.DepartmentTitleId.Value : item.EmployeeId.Value,
                        Order = item.Order.HasValue ? item.Order.Value : 0,
                        Name = item.DepartmentTitleId.HasValue ? ServiceFactory<DepartmentTitleService>().GetById(item.DepartmentTitleId.Value).Name : ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.EmployeeId.Value).Name,
                        RoleType = item.TypeRole.Value
                    });
                }
            }
            return obj;
        }
        public bool CheckRoleTypeExits(Resource.DocumentProcessRole roleType)
        {
            var titleIds = ServiceFactory<EmployeeTitleService>().GetTitleIdsByCurrentUser();
            return base.Get(t => t.TypeRole == (int)roleType && titleIds.Contains(t.DepartmentTitleId.HasValue ? t.DepartmentTitleId.Value : Guid.Empty) || UserId == t.EmployeeId).Any();
        }
        public IEnumerable<DepartmentBO> GetDepartmentAllowRequest(Resource.DocumentProcessRole roleType)
        {
            var titleIds = ServiceFactory<EmployeeTitleService>().GetTitleIdsByCurrentUser();
            var departmentIds = base.Get(t => t.TypeRole == (int)roleType && titleIds.Contains(t.DepartmentTitleId.HasValue ? t.DepartmentTitleId.Value : Guid.Empty) || UserId == t.EmployeeId).Select(t => t.DepartmentId.HasValue ? t.DepartmentId.Value : Guid.Empty).Distinct();
            return ServiceFactory<DepartmentService>().GetByIds(departmentIds);
        }
        private void updateOrder(int order, Guid departmentId, int process)
        {
            var data = base.Get(c => c.DepartmentId == departmentId && c.TypeProcess == process && c.Order >= order);
            foreach (var item in data)
            {
                item.Order = item.Order + 1;
                base.Update(item, true);
            }
        }
        private void updateDownOrder(int order, Guid departmentId, int process)
        {
            var data = base.Get(c => c.DepartmentId == departmentId && c.TypeProcess == process && c.Order > order);
            foreach (var item in data)
            {
                item.Order = item.Order - 1;
                base.Update(item, false);
            }
        }
        public void InsertProcess(DocumentProcessBO item)
        {
            var max = GetMaxOrder(item.DepartmentId.Value, (int)item.TypeProcess.Value, (int)item.TypeRole.Value);
            item.Order = max + 1;
            base.Insert(item, true);
            updateOrder(item.Order.Value, item.DepartmentId.Value, item.TypeProcess.Value);
        }
        public void DeleteProcess(Guid id)
        {
            var obj = base.GetById(id);
            updateDownOrder(obj.Order.Value, obj.DepartmentId.Value, obj.TypeProcess.Value);
            base.Delete(id, false);
            base.SaveTransaction();
        }
        public DocumentProcessBO GetRecord(Guid departmentId, int process, int roletype)
        {
            var obj = new DocumentProcessBO();
            obj = base.Get(t => t.DepartmentId == departmentId && t.TypeProcess == process && t.TypeRole == roletype).FirstOrDefault();
            return obj;
        }


        public void UpdateProcess(DocumentProcessBO item)
        {
            var obj = base.GetById(item.Id);
            obj.DepartmentTitleId = (item.DepartmentTitleId.HasValue && item.DepartmentTitleId != Guid.Empty) ? item.DepartmentTitleId.Value : Guid.Empty;
            obj.EmployeeId = (item.EmployeeId.HasValue && item.EmployeeId != Guid.Empty) ? item.EmployeeId.Value : Guid.Empty;
            obj.Order = item.Order;
            obj.Note = item.Note;
            obj.TypeRole = item.TypeRole;
            obj.TypeProcess = item.TypeProcess;
            obj.DepartmentId = item.DepartmentId;
            base.Update(obj);
        }


        public void Copy(Guid departmentId, Guid departmentCopyId, int process)
        {
            var temp = new List<DocumentProcessBO>();
            var data = base.Get(c => c.DepartmentId == departmentId && c.TypeProcess == process).Select(t => t.Id);
            base.DeleteRange(data, false);
            var dataCoppy = base.Get(c => c.DepartmentId == departmentCopyId && c.TypeProcess == process);
            foreach (var item in dataCoppy)
            {
                temp.Add(new DocumentProcessBO
                {
                    DepartmentTitleId = item.DepartmentTitleId,
                    Note = item.Note,
                    Order = item.Order,
                    TypeProcess = item.TypeProcess,
                    TypeRole = item.TypeRole,
                    EmployeeId = item.EmployeeId,
                    DepartmentId = departmentId
                });
            }
            base.InsertRange(temp, false);
            base.SaveTransaction();
        }


        public IEnumerable<EmployeeBO> GetEmployeeByRole(int pageIndex, int pageSize, out int count, Guid departmentId, int process, int roleType)
        {

            var employeeIds = new List<Guid>();
            var data = Enumerable.Empty<EmployeeBO>();
            employeeIds.AddRange(getEmployeeIds(process, roleType, departmentId));
            var titleIds = getTitleIds(process, roleType, departmentId);
            employeeIds.AddRange(ServiceFactory<EmployeeTitleService>().GetEmployeeIDsByRoleIDs(titleIds));
            data = ServiceFactory<EmployeeService>().GetByIds(employeeIds);
            count = data.Count();
            var employees = ServiceFactory<EmployeeService>().Page(data, pageIndex, pageSize);
            employees = employees.Select(i =>
            {
                i.RoleNames = string.Join(",", ServiceFactory<EmployeeService>().GetRoleNames(i.Id));
                return i;
            });
            return employees;

        }

        public IEnumerable<DepartmentBO> GetDeparmentByRoleType(int roleType)
        {
            var titleIds = ServiceFactory<EmployeeTitleService>().GetTitleIdsByCurrentUser();
            var departmentIds = base.Get(c => c.TypeRole == roleType && (c.EmployeeId == UserId
                 || (c.DepartmentTitleId.HasValue && titleIds.Contains(c.DepartmentTitleId.Value)))).Select(t => t.DepartmentId.Value);
            var department = ServiceFactory<DepartmentService>().GetByIds(departmentIds);
            return department;
        }

        /// <summary>
        /// Danh sách phòng ban với vai trò phân công của người đăng nhập hệ thống
        /// - Lấy ra danh sách phòng ban của nó nếu như không có quy trình
        /// - Danh sách phòng phan của nó với vai trò nó là người phân công
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DepartmentBO> GetDepartmentByAssignRole()
        {
            var data = Enumerable.Empty<DepartmentBO>();
            var dataIds = Enumerable.Empty<Guid>();
            // Danh sách chức danh hiện tại của người đăng nhập hệ thống
            var titleIds = ServiceFactory<EmployeeTitleService>().GetTitleIdsByCurrentUser();
            // Danh sách phòng ban theo của người hiện tại theo chức danh
            var titles = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds);
            // Danh sách Ids phòng ban của người đăng nhập hệ thống
            var departmentIds = titles.Where(i => i.DepartmentId.HasValue).Select(i => i.DepartmentId.Value);
            // Lấy ra danh sách phòng ban của nó có quy trình
            var currentDeparmentHasNotRole = Get(i => i.DepartmentTitleId.HasValue && departmentIds.Contains(i.DepartmentTitleId.Value)).Select(i => i.DepartmentId.Value);
            // Lấy ra danh sách phòng ban của nó không có quy trình = danh sách phòng ban - danh sách phòng ban có quy trình
            dataIds = departmentIds.Except(currentDeparmentHasNotRole);
            // Lấy ra danh sách phòng ban có quy trình với vai trò của nó là phân công
            var deparmentAssignRoleIds = Get(i => i.EmployeeId == UserId || (i.DepartmentTitleId.HasValue && titleIds.Contains(i.DepartmentTitleId.Value))).Select(i => i.DepartmentId.Value);
            dataIds = dataIds.Concat(deparmentAssignRoleIds);

            data = ServiceFactory<DepartmentService>().GetByIds(dataIds);
            return data;

        }
        private List<DocumentProcessComboBO> GetNextRoleForSuggesterWriteAndEdit(Guid departmentId, int typeProcess = 0, int? roleType = 0, int order = 0)
        {

            var data = new List<DocumentProcessComboBO>();
            var exitProcess = CheckExitProcess(departmentId, typeProcess);
            if (exitProcess)
            {
                var roleNext = Get(i => i.DepartmentId == departmentId && i.TypeProcess == typeProcess)
                          .Where(i => i.Order > order && i.IsDelete == false)
                          .OrderBy(i => i.Order).FirstOrDefault();
                if (roleNext == null) return null;
                if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit)
                {
                    #region Bỏ qua các bước xem xét mà nó đóng vai trò
                    //var currentUserId = UserId;

                    //var currentTitleIds = ServiceFactory<EmployeeTitleService>().GetRoleIdsByEmployeeId(currentUserId).ToList();
                    //// Lấy bước xem xét cao nhất mà nó tồn tại
                    //var proceesItemContainCurrentUser = Get(i => i.DepartmentId == departmentId
                    //                                            && i.TypeProcess == typeProcess
                    //                                            && i.TypeRole == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit
                    //                                            && i.IsDelete == false
                    //                                        )
                    //                                    .Where(i => (i.EmployeeId.HasValue && i.EmployeeId == currentUserId)
                    //                                            || (i.DepartmentTitleId.HasValue && currentTitleIds.Contains(i.DepartmentTitleId.Value))
                    //                                        )
                    //                                    .OrderByDescending(i => i.Order).FirstOrDefault();
                    //if (proceesItemContainCurrentUser != null)
                    //{
                    //    // Nhảy đến bước tiếp theo------------------------------------------------
                    //    var roleNextSuggest = Get(i => i.DepartmentId == departmentId && i.TypeProcess == typeProcess)
                    //                .Where(i => i.Order > proceesItemContainCurrentUser.Order && i.IsDelete == false)
                    //                .OrderBy(i => i.Order).FirstOrDefault();
                    //    if (roleNextSuggest.TypeRole == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit)
                    //    {
                    //        data.Add(new DocumentProcessComboBO
                    //        {
                    //            ID = (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit,
                    //            Name = Resource.DocumentProcessRoleText.ApprovalWriteAndEdit,
                    //            ProcessType = typeProcess,
                    //            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                    //        });
                    //    }
                    //    else if (roleNextSuggest.TypeRole == (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit)
                    //    {
                    //        data.Add(new DocumentProcessComboBO
                    //        {
                    //            ID = (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit,
                    //            Name = Resource.DocumentProcessRoleText.AssignmentWriteAndEdit,
                    //            ProcessType = typeProcess,
                    //            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                    //        });
                    //    }
                    //    //------------------------------------------------------------------------
                    //}
                    //else
                    //{
                    #endregion
                    data.Add(new DocumentProcessComboBO
                    {
                        ID = (int)Resource.DocumentProcessRole.ReviewWriteAndEdit,
                        Name = Resource.DocumentProcessRoleText.ReviewWriteAndEdit,
                        ProcessType = typeProcess,
                        Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                    });
                    //}
                }
                if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit)
                {
                    data.Add(new DocumentProcessComboBO
                    {
                        ID = (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit,
                        Name = Resource.DocumentProcessRoleText.ApprovalWriteAndEdit,
                        ProcessType = typeProcess,
                        Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                    });
                }
                if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit)
                {
                    data.Add(new DocumentProcessComboBO
                    {
                        ID = (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit,
                        Name = Resource.DocumentProcessRoleText.AssignmentWriteAndEdit,
                        ProcessType = typeProcess,
                        Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                    });
                }
            }
            else
            {
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.ReviewWriteAndEdit,
                    Name = Resource.DocumentProcessRoleText.ReviewWriteAndEdit,
                    ProcessType = typeProcess,
                });
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit,
                    Name = Resource.DocumentProcessRoleText.ApprovalWriteAndEdit,
                    ProcessType = typeProcess,
                });
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit,
                    Name = Resource.DocumentProcessRoleText.AssignmentWriteAndEdit,
                    ProcessType = typeProcess,
                });
            }
            return data;
        }
        private List<DocumentProcessComboBO> GetNextRoleForReviewWriteAndEdit(Guid departmentId, int typeProcess = 0, int? roleType = 0, int order = 0)
        {
            var data = new List<DocumentProcessComboBO>();
            var exitProcess = CheckExitProcess(departmentId, typeProcess);
            if (exitProcess)
            {
                var roleNext = Get(i => i.DepartmentId == departmentId && i.TypeProcess == typeProcess)
                              .Where(i => i.Order > order && i.IsDelete == false)
                              .OrderBy(i => i.Order).FirstOrDefault();
                if (roleNext == null)
                {
                    data.Add(new DocumentProcessComboBO
                    {
                        ID = (int)Resource.DocumentProcessRole.Write,
                        Name = Resource.DocumentProcessRoleText.Write,
                        ProcessType = typeProcess,
                    });
                }
                else
                {
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.ReviewWriteAndEdit,
                            Name = Resource.DocumentProcessRoleText.ReviewWriteAndEdit,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit,
                            Name = Resource.DocumentProcessRoleText.ApprovalWriteAndEdit,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit,
                            Name = Resource.DocumentProcessRoleText.AssignmentWriteAndEdit,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                }
            }
            else
            {
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.Write,
                    Name = Resource.DocumentProcessRoleText.Write,
                    ProcessType = typeProcess,
                });
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.ReviewWriteAndEdit,
                    Name = Resource.DocumentProcessRoleText.ReviewWriteAndEdit,
                    ProcessType = typeProcess,
                });
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit,
                    Name = Resource.DocumentProcessRoleText.ApprovalWriteAndEdit,
                    ProcessType = typeProcess,
                });
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit,
                    Name = Resource.DocumentProcessRoleText.AssignmentWriteAndEdit,
                    ProcessType = typeProcess,
                });
            }
            return data;
        }
        private List<DocumentProcessComboBO> GetNextRoleForApprovalWriteAndEdit(Guid departmentId, int typeProcess = 0, int? roleType = 0, int order = 0)
        {
            var data = new List<DocumentProcessComboBO>();
            var exitProcess = CheckExitProcess(departmentId, typeProcess);
            if (exitProcess)
            {
                var roleNext = Get(i => i.DepartmentId == departmentId && i.TypeProcess == typeProcess)
                        .Where(i => i.TypeRole == (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit)
                        .Where(i => i.Order > order && i.IsDelete == false)
                        .OrderBy(i => i.Order).FirstOrDefault();
                if (roleNext == null)
                {
                    data.Add(new DocumentProcessComboBO
                    {
                        ID = (int)Resource.DocumentProcessRole.Write,
                        Name = Resource.DocumentProcessRoleText.Write,
                        ProcessType = typeProcess,
                    });
                }
                else
                {
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit,
                            Name = Resource.DocumentProcessRoleText.AssignmentWriteAndEdit,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                }
            }
            else
            {
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit,
                    Name = Resource.DocumentProcessRoleText.AssignmentWriteAndEdit,
                    ProcessType = typeProcess,
                });
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.Write,
                    Name = Resource.DocumentProcessRoleText.Write,
                    ProcessType = typeProcess,
                });
            }
            return data;
        }
        private List<DocumentProcessComboBO> GetNextRoleForAssignmentWriteAndEdit(Guid departmentId, int typeProcess = 0, int? roleType = 0, int order = 0)
        {
            var data = new List<DocumentProcessComboBO>();
            data.Add(new DocumentProcessComboBO
            {
                ID = (int)Resource.DocumentProcessRole.Write,
                Name = Resource.DocumentProcessRoleText.Write,
                ProcessType = typeProcess,
                Order = 0
            });
            return data;

        }
        private List<DocumentProcessComboBO> GetNextRoleForWrite(Guid departmentId, int typeProcess = 0, int? roleType = 0, int order = 0)
        {
            var data = new List<DocumentProcessComboBO>();
            var exitProcess = CheckExitProcess(departmentId, typeProcess);
            if (exitProcess)
            {
                var roleNext = Get(i => i.DepartmentId == departmentId && i.TypeProcess == typeProcess)
                          .Where(i => i.Order > order && i.IsDelete == false)
                          .OrderBy(i => i.Order).FirstOrDefault();
                if (roleNext == null) return null;
                else
                {
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.Check)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.Check,
                            Name = Resource.DocumentProcessRoleText.Check,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.ApproveDocument)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.ApproveDocument,
                            Name = Resource.DocumentProcessRoleText.ApproveDocument,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.Promulgate)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.Promulgate,
                            Name = Resource.DocumentProcessRoleText.Promulgate,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                }
            }
            else
            {
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.Check,
                    Name = Resource.DocumentProcessRoleText.Check,
                    ProcessType = typeProcess,
                });
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.ApproveDocument,
                    Name = Resource.DocumentProcessRoleText.ApproveDocument,
                    ProcessType = typeProcess,
                });
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.Promulgate,
                    Name = Resource.DocumentProcessRoleText.Promulgate,
                    ProcessType = typeProcess,
                });
            }
            return data;
        }
        private List<DocumentProcessComboBO> GetNextRoleForCheck(Guid departmentId, int typeProcess = 0, int? roleType = 0, int order = 0)
        {
            var data = new List<DocumentProcessComboBO>();
            var exitProcess = CheckExitProcess(departmentId, typeProcess);
            if (exitProcess)
            {
                var typeProcessDatabase = typeProcess;
                if (typeProcess == (int)Resource.DocumentTypeProcess.DistributeSuggest || typeProcess == (int)Resource.DocumentTypeProcess.CancelSuggest)
                {
                    typeProcessDatabase = (int)Resource.DocumentTypeProcess.WriteAndRelation;
                }
                var roleNext = Get(i => i.DepartmentId == departmentId && i.TypeProcess == typeProcessDatabase)
                              .Where(i => i.Order > order && i.IsDelete == false)
                              .OrderBy(i => i.Order).FirstOrDefault();
                if (roleNext == null) return null;
                else
                {
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.Check)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.Check,
                            Name = typeProcess == (int)Resource.DocumentTypeProcess.CancelSuggest ? Resource.DocumentProcessRoleText.ReviewWriteAndEdit : Resource.DocumentProcessRoleText.Check,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.ApproveDocument)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.ApproveDocument,
                            Name = Resource.DocumentProcessRoleText.ApproveDocument,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.Promulgate)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.Promulgate,
                            Name = Resource.DocumentProcessRoleText.Promulgate,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                }
            }
            else
            {
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.Check,
                    Name = typeProcess == (int)Resource.DocumentTypeProcess.CancelSuggest ? Resource.DocumentProcessRoleText.ReviewWriteAndEdit : Resource.DocumentProcessRoleText.Check,
                    ProcessType = typeProcess,
                });
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.ApproveDocument,
                    Name = Resource.DocumentProcessRoleText.ApproveDocument,
                    ProcessType = typeProcess,
                });
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.Promulgate,
                    Name = Resource.DocumentProcessRoleText.Promulgate,
                    ProcessType = typeProcess,
                });
            }
            return data;
        }
        private List<DocumentProcessComboBO> GetNextRoleForApproveDocument(Guid departmentId, int typeProcess = 0, int? roleType = 0, int order = 0)
        {
            var data = new List<DocumentProcessComboBO>();
            var exitProcess = CheckExitProcess(departmentId, typeProcess);
            if (exitProcess)
            {
                var typeProcessDatabase = typeProcess;
                if (typeProcess == (int)Resource.DocumentTypeProcess.DistributeSuggest || typeProcess == (int)Resource.DocumentTypeProcess.CancelSuggest)
                {
                    typeProcessDatabase = (int)Resource.DocumentTypeProcess.WriteAndRelation;
                }
                var roleNext = Get(i => i.DepartmentId == departmentId && i.TypeProcess == typeProcessDatabase)
                              .Where(i => i.Order > order && i.IsDelete == false)
                              .Where(i => i.TypeRole == (int)Resource.DocumentProcessRole.Promulgate)
                              .OrderBy(i => i.Order).FirstOrDefault();
                if (roleNext == null) return null;
                else
                {
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.Promulgate)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.Promulgate,
                            Name = typeProcess == (int)Resource.DocumentTypeProcess.CancelSuggest ? Resource.DocumentProcessRoleText.Destroy : Resource.DocumentProcessRoleText.Promulgate,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                }
            }
            else
            {
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.ApproveDocument,
                    Name = Resource.DocumentProcessRoleText.ApproveDocument,
                    ProcessType = typeProcess,
                });
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.Promulgate,
                    Name = typeProcess == (int)Resource.DocumentTypeProcess.CancelSuggest ? Resource.DocumentProcessRoleText.Destroy : Resource.DocumentProcessRoleText.Promulgate,
                    ProcessType = typeProcess,
                });
            }
            return data;
        }
        private List<DocumentProcessComboBO> GetNextRoleForPromulgate(Guid departmentId, int typeProcess = 0, int? roleType = 0, int order = 0)
        {
            var data = new List<DocumentProcessComboBO>();
            var exitProcess = CheckExitProcess(departmentId, typeProcess);
            if (exitProcess)
            {
                var typeProcessDatabase = typeProcess;
                // Nếu là quy trình hủy sẽ lấy dữ liệu từ quy trình viết mới
                if (typeProcess == (int)Resource.DocumentTypeProcess.DistributeSuggest)
                {
                    typeProcessDatabase = (int)Resource.DocumentTypeProcess.WriteAndRelation;
                }
                var roleNext = Get(i => i.DepartmentId == departmentId && i.TypeProcess == typeProcessDatabase)
                              .Where(i => i.Order > order && i.IsDelete == false)
                              .OrderBy(i => i.Order).FirstOrDefault();
                if (roleNext == null) return null;
                else
                {
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.Check)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.Check,
                            Name = Resource.DocumentProcessRoleText.Check,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.ApproveDocument)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.ApproveDocument,
                            Name = Resource.DocumentProcessRoleText.ApproveDocument,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                }
            }
            else
            {
                if (typeProcess == (int)Resource.DocumentTypeProcess.DistributeSuggest)
                {
                    data.Add(new DocumentProcessComboBO
                    {
                        ID = (int)Resource.DocumentProcessRole.Check,
                        Name = Resource.DocumentProcessRoleText.Check,
                        ProcessType = typeProcess,
                    });
                    data.Add(new DocumentProcessComboBO
                    {
                        ID = (int)Resource.DocumentProcessRole.ApproveDocument,
                        Name = Resource.DocumentProcessRoleText.ApproveDocument,
                        ProcessType = typeProcess,
                    });
                }
            }
            return data;
        }
        private List<DocumentProcessComboBO> GetNextRoleForArchiver(Guid departmentId, int typeProcess = 0, int? roleType = 0, int order = 0)
        {
            var data = new List<DocumentProcessComboBO>();
            data.Add(new DocumentProcessComboBO
            {
                ID = (int)Resource.DocumentProcessRole.Archiver,
                Name = Resource.DocumentProcessRoleText.Archiver,
                ProcessType = typeProcess,
                Order = order
            });
            return data;
        }
        private List<DocumentProcessComboBO> GetNextRoleForDestroyCreater(Guid departmentId, int typeProcess = 0, int? roleType = 0, int order = 0)
        {
            var data = new List<DocumentProcessComboBO>();
            var exitProcess = CheckExitProcess(departmentId, typeProcess);
            if (exitProcess)
            {
                var typeProcessDatabase = typeProcess;
                // Nếu là quy trình hủy sẽ lấy dữ liệu từ quy trình viết mới
                if (typeProcess == (int)Resource.DocumentTypeProcess.CancelSuggest)
                {
                    typeProcessDatabase = (int)Resource.DocumentTypeProcess.WriteAndRelation;
                }

                var roleNext = Get(i => i.DepartmentId == departmentId && i.TypeProcess == typeProcessDatabase)
                              .Where(i => i.Order > order && i.IsDelete == false)
                              .OrderBy(i => i.Order).FirstOrDefault();
                if (roleNext == null) return null;
                else
                {
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.Check)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.Check,
                            Name = Resource.DocumentProcessRoleText.ReviewWriteAndEdit,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                    if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.ApproveDocument)
                    {
                        data.Add(new DocumentProcessComboBO
                        {
                            ID = (int)Resource.DocumentProcessRole.ApproveDocument,
                            Name = Resource.DocumentProcessRoleText.ApproveDocument,
                            ProcessType = typeProcess,
                            Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                        });
                    }
                    //if (roleNext.TypeRole == (int)Resource.DocumentProcessRole.Promulgate)
                    //{
                    //    data.Add(new DocumentProcessComboBO
                    //    {
                    //        ID = (int)Resource.DocumentProcessRole.Promulgate,
                    //        Name = Resource.DocumentProcessRoleText.Destroy,
                    //        ProcessType = typeProcess,
                    //        Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                    //    });
                    //}
                }
            }
            else
            {
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.Check,
                    Name = Resource.DocumentProcessRoleText.ReviewWriteAndEdit,
                    ProcessType = typeProcess,
                });
                data.Add(new DocumentProcessComboBO
                {
                    ID = (int)Resource.DocumentProcessRole.ApproveDocument,
                    Name = Resource.DocumentProcessRoleText.ApproveDocument,
                    ProcessType = typeProcess,
                });
                //data.Add(new DocumentProcessComboBO
                //{
                //    ID = (int)Resource.DocumentProcessRole.Promulgate,
                //    Name = Resource.DocumentProcessRoleText.Promulgate,
                //    ProcessType = typeProcess,
                //});
            }
            return data;
        }
        // Lấy danh sách trách nhiệm lựa chọn tiếp theo
        public List<DocumentProcessComboBO> GetNextRoleType(Guid departmentId, int typeProcess = 0, int? roleType = 0, int order = 0)
        {
            var data = new List<DocumentProcessComboBO>();
            if (roleType == 0 || roleType == null)
            {
                var roleCurrent = Get(i => i.DepartmentId == departmentId && i.TypeProcess == typeProcess)
                          .Where(i => i.Order == order).FirstOrDefault();
                if (roleCurrent != null && roleCurrent.Order.HasValue) roleType = roleCurrent.Order.Value;
            }
            switch (roleType)
            {
                // Người thực hiện là người có trách nhiệm đề nghị
                case (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit:
                    data = GetNextRoleForSuggesterWriteAndEdit(departmentId, typeProcess, roleType, order);
                    break;
                // Người thực hiện là người có trách nhiệm xem xét
                case (int)Resource.DocumentProcessRole.ReviewWriteAndEdit:
                    data = GetNextRoleForReviewWriteAndEdit(departmentId, typeProcess, roleType, order);
                    break;
                // Người thực hiện là người có trách nhiệm phê duyệt tài liệu viết mới và sửa đổi
                case (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit:
                    data = GetNextRoleForApprovalWriteAndEdit(departmentId, typeProcess, roleType, order);
                    break;
                // Người thực hiện là người có trách nhiệm phân công
                case (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit:
                    data = GetNextRoleForAssignmentWriteAndEdit(departmentId, typeProcess, roleType, order);
                    break;
                // Người thực hiện là người có trách nhiệm soạn thảo
                case (int)Resource.DocumentProcessRole.Write:
                    data = GetNextRoleForWrite(departmentId, typeProcess, roleType, order);
                    break;
                // Người thực hiện là người có trách nhiệm kiểm tra
                case (int)Resource.DocumentProcessRole.Check:
                    data = GetNextRoleForCheck(departmentId, typeProcess, roleType, order);
                    break;
                // Người thực hiện là người có trách nhiệm phê duyệt
                case (int)Resource.DocumentProcessRole.ApproveDocument:
                    data = GetNextRoleForApproveDocument(departmentId, typeProcess, roleType, order);
                    break;
                // Người thực hiện là người có trách nhiệm ban hành
                case (int)Resource.DocumentProcessRole.Promulgate:
                    data = GetNextRoleForPromulgate(departmentId, typeProcess, roleType, order);
                    break;
                // Người thực hiện là người có trách nhiệm lưu trữ
                case (int)Resource.DocumentProcessRole.Archiver:
                    data = GetNextRoleForArchiver(departmentId, typeProcess, roleType, order);
                    break;
                case (int)Resource.DocumentProcessRole.DestroyCreater:
                    data = GetNextRoleForDestroyCreater(departmentId, typeProcess, roleType, order);
                    break;
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="departmentId"></param>
        /// <param name="typeProcess"></param>
        /// <param name="roleType"></param>
        /// <returns></returns>
        public bool CheckCurrentUserRole(Guid departmentId, int typeProcess, int roleType)
        {
            return base.Get(t => t.DepartmentId == departmentId && t.TypeProcess == typeProcess && t.TypeRole == roleType).Any();
        }
    }
}
