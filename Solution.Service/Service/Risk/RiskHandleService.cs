﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class RiskHandleService : BaseService<RiskHandleDTO, RiskHandleBO>, IRiskHandleService
    {
        public override RiskHandleBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id);
            var riskAssetAudit = ServiceFactory<RiskAssetAuditService>().GetById(data.RiskAssetAuditId.HasValue ? data.RiskAssetAuditId.Value : Guid.Empty);
            var department = ServiceFactory<DepartmentService>().GetById(data.DepartmentId.HasValue ? data.DepartmentId.Value : Guid.Empty);
            var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.EmployeeId.HasValue ? data.EmployeeId.Value : Guid.Empty);
            var employeeCheck = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.EmployeeCheckId.HasValue ? data.EmployeeCheckId.Value : Guid.Empty);
            var resourceRelate = ServiceFactory<AssetResourceRelateService>().GetById(riskAssetAudit.AssetResourceRelateId.HasValue ? riskAssetAudit.AssetResourceRelateId.Value : new Guid());
            var asset = resourceRelate != null ? ServiceFactory<AssetService>().GetById(resourceRelate.AssetId.HasValue ? resourceRelate.AssetId.Value : Guid.Empty) : null;
            var plan = ServiceFactory<RiskPlanAuditService>().GetById(riskAssetAudit.RiskPlanAuditId.HasValue ? riskAssetAudit.RiskPlanAuditId.Value : Guid.Empty);
            data.Title = plan != null ? plan.Name : string.Empty;
            data.AssetName = asset != null ? asset.Name : string.Empty;
            data.MethodHandlerName = iDAS.Service.Common.Resource.ConvertMethodHandleRisk(data.MethodHandle.HasValue ? (int)data.MethodHandle.Value : 0);
            data.AcceptCriteriaName = iDAS.Service.Common.Resource.ConvertAcceptCriteriaRisk(data.AcceptCriteria.HasValue ? (int)data.AcceptCriteria.Value : 0);
            data.EmployeeName = employee != null ? employee.Name : string.Empty;
            data.DepartmentName = department != null ? department.Name : string.Empty;
            data.EmployeeCheckName = employeeCheck != null ? employeeCheck.Name : string.Empty;
            data.Hazard = riskAssetAudit != null ? riskAssetAudit.Hazard : string.Empty;
            data.Weakness = riskAssetAudit != null ? riskAssetAudit.Weakness : string.Empty;
            data.ProcessExpediency = resourceRelate != null ? resourceRelate.ProcessExpediency : string.Empty;
            return data;
        }
        public IEnumerable<RiskHandleBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            var result = base.GetQuery();
            count = result.Count();
            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            foreach (var item in data)
            {
                var riskAssetAudit = ServiceFactory<RiskAssetAuditService>().GetById(item.RiskAssetAuditId.HasValue ? item.RiskAssetAuditId.Value : Guid.Empty);
                var department = ServiceFactory<DepartmentService>().GetById(item.DepartmentId.HasValue ? item.DepartmentId.Value : Guid.Empty);
                var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.EmployeeId.HasValue ? item.EmployeeId.Value : Guid.Empty);
                var employeeCheck = ServiceFactory<EmployeeService>().GetById(item.EmployeeCheckId.HasValue ? item.EmployeeCheckId.Value : Guid.Empty);
                var resourceRelate = ServiceFactory<AssetResourceRelateService>().GetById(riskAssetAudit.AssetResourceRelateId.HasValue ? riskAssetAudit.AssetResourceRelateId.Value : new Guid());
                var asset = resourceRelate != null ? ServiceFactory<AssetService>().GetById(resourceRelate.AssetId.HasValue ? resourceRelate.AssetId.Value : Guid.Empty) : null;
                var plan = ServiceFactory<RiskPlanAuditService>().GetById(riskAssetAudit.RiskPlanAuditId.HasValue ? riskAssetAudit.RiskPlanAuditId.Value : Guid.Empty);

                item.Title = plan != null ? plan.Name : string.Empty;
                item.AssetName = asset != null ? asset.Name : string.Empty;
                item.MethodHandlerName = iDAS.Service.Common.Resource.ConvertMethodHandleRisk(item.MethodHandle.HasValue ? (int)item.MethodHandle.Value : 0);
                item.AcceptCriteriaName = iDAS.Service.Common.Resource.ConvertAcceptCriteriaRisk(item.AcceptCriteria.HasValue ? (int)item.AcceptCriteria.Value : 0);
                item.EmployeeName = employee != null ? employee.Name : string.Empty;
                item.EmployeeCheckName = employeeCheck != null ? employeeCheck.Name : string.Empty;
                item.DepartmentName = department != null ? department.Name : string.Empty;
                item.Hazard = riskAssetAudit != null ? riskAssetAudit.Hazard : string.Empty;
                item.Weakness = riskAssetAudit != null ? riskAssetAudit.Weakness : string.Empty;
                item.ProcessExpediency = resourceRelate != null ? resourceRelate.ProcessExpediency : string.Empty;

            }
            return data;
        }
    }
}
