﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class RiskAssetAuditService : BaseService<RiskAssetAuditDTO, RiskAssetAuditBO>, IRiskAssetAuditService
    {
        public override RiskAssetAuditBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id);
            var assetResourceRelate = ServiceFactory<AssetResourceRelateService>().GetById(data.AssetResourceRelateId.HasValue ? data.AssetResourceRelateId.Value : Guid.Empty);
            var lastReview = ServiceFactory<AssetReviewService>().GetLastByAssetResource(data.AssetResourceRelateId.HasValue ? data.AssetResourceRelateId.Value : Guid.Empty);
            var asset = ServiceFactory<AssetService>().GetById(assetResourceRelate != null ? assetResourceRelate.AssetId.Value : Guid.Empty);
            data.AssetName = asset != null ? asset.Name : string.Empty;
            data.AssetValue = data.AssetValue.HasValue ? data.AssetValue : (lastReview != null ? lastReview.Value : 0);
            data.ProcessExpediency = assetResourceRelate != null ? assetResourceRelate.ProcessExpediency : string.Empty;
            return data;
        }
        public IEnumerable<Guid> InsertRange(Guid riskPlanAuditId, string jsonData, bool allowSave = true)
        {
            var result = Enumerable.Empty<Guid>();
            try
            {
                if (!string.IsNullOrEmpty(jsonData))
                {
                    var data = JsonConvert.DeserializeObject<List<RiskAssetAuditBO>>(jsonData);
                    if (data != null && data.Count > 0)
                    {
                        foreach (var item in data)
                        {
                            item.RiskPlanAuditId = riskPlanAuditId;
                        }
                    }
                    result = InsertRange(data, allowSave);
                }
            }
            catch (Exception)
            {
            }
            return result;
        }
        public IEnumerable<RiskAssetAuditBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            var result = base.GetQuery();
            count = result.Count();
            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            foreach (var item in data)
            {
                var plan = ServiceFactory<RiskPlanAuditService>().GetById(item.RiskPlanAuditId.HasValue ? item.RiskPlanAuditId.Value : Guid.Empty);
                var department = ServiceFactory<DepartmentService>().GetById(plan != null ? plan.DepartmentId.Value : Guid.Empty);
                var resourceRelate = ServiceFactory<AssetResourceRelateService>().GetById(item.AssetResourceRelateId.HasValue ? item.AssetResourceRelateId.Value : new Guid());
                var asset = resourceRelate != null ? ServiceFactory<AssetService>().GetById(resourceRelate.AssetId.HasValue ? resourceRelate.AssetId.Value : Guid.Empty) : null;
                item.Title = plan != null ? plan.Name : string.Empty;
                item.AssetName = asset != null ? asset.Name : string.Empty;
                item.CreateAt = plan != null ? plan.CreateAt : item.CreateAt;
                item.ProcessExpediency = resourceRelate != null ? resourceRelate.ProcessExpediency : string.Empty;
                item.AcceptCriteriaName = iDAS.Service.Common.Resource.ConvertAcceptCriteriaRisk(item.AcceptCriteria.HasValue ? (int)item.AcceptCriteria.Value : 0);
            }
            return data;
        }
        public override void Update(RiskAssetAuditBO data, bool allowSave = true)
        {
            var obj = ServiceFactory<RiskHandleService>().Get(t => t.RiskAssetAuditId == data.Id).FirstOrDefault();
            if (data.AcceptCriteria == (int)Common.Resource.AcceptCriteriaRisk.Handler)
            {
                if (obj == null)
                {
                    obj = new RiskHandleBO();
                }
                obj.AssetValue = data.AssetValue;
                obj.RiskAssetAuditId = data.Id;
                ServiceFactory<RiskHandleService>().Insert(obj, allowSave);
            }
            else
            {
                if (obj != null)
                {
                    ServiceFactory<RiskHandleService>().Delete(obj.Id, allowSave);
                }
            }
            base.Update(data, allowSave);

        }
    }
}
