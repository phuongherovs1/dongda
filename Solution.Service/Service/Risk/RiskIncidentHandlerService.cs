﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class RiskIncidentHandlerService : BaseService<RiskIncidentHandlerDTO, RiskIncidentHandlerBO>, IRiskIncidentHandlerService
    {
        public IEnumerable<RiskIncidentHandlerBO> GetAll(int pageIndex, int pageSize, out int count, string incidentId = default(string))
        {
            var result = base.Get()
                .Where(t => t.RiskIncidentId.Value == new Guid(incidentId));
            count = result.Count();
            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            foreach (var item in data)
            {
                var department = ServiceFactory<DepartmentService>().GetById(item.DepartmentId.HasValue ? item.DepartmentId.Value : Guid.Empty);
                item.DepartmentName = department != null ? department.Name : string.Empty;
            }
            return data;
        }
    }
}
