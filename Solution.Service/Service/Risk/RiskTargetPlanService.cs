﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class RiskTargetPlanService : BaseService<RiskTargetPlanDTO, RiskTargetPlanBO>, IRiskTargetPlanService
    {

        public IEnumerable<RiskTargetPlanBO> GetDataRiskTargetPlan(int pageIndex, int pageSize, out int count, Guid? RiskTargetId, string filterName = default(string))
        {
            var dataQuery = base.GetQuery()
                .Where(i => i.RiskTargetId == RiskTargetId)
                .Where(i => string.IsNullOrEmpty(filterName) || (!string.IsNullOrEmpty(filterName) && (!string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(filterName.ToLower()))
               ))
                .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            return results;
        }
    }
}
