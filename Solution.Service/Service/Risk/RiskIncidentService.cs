﻿using iDAS.DataAccess;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class RiskIncidentService : BaseService<RiskIncidentDTO, RiskIncidentBO>, IRiskIncidentService
    {
        public IEnumerable<RiskIncidentBO> GetAll(int pageIndex, int pageSize, out int count, string departmentId = default(string))
        {
            departmentId = departmentId.Replace("Department_", string.Empty);
            var guidDepartmentId = Common.Utilities.ConvertToGuid(departmentId);
            var result = base.GetQuery()
                .Where(t => t.DepartmentId.Value == guidDepartmentId || departmentId == string.Empty || departmentId == default(string));
            count = result.Count();
            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            var departmentIds = data.Where(c => c.DepartmentId.HasValue).Select(c => c.DepartmentId.Value).ToList();
            var departments = ServiceFactory<DepartmentService>().GetByIds(departmentIds);
            foreach (var item in data)
            {
                if (item.DepartmentId.HasValue)
                {
                    var department = departments.FirstOrDefault(c => c.Id == item.DepartmentId);
                    item.DepartmentName = department != null ? department.Name : string.Empty;
                }
                var handler = ServiceFactory<RiskIncidentHandlerService>().GetQuery().Where(t => t.RiskIncidentId.Value == item.Id);
                item.IsDoing = (handler.Count() > 0 && handler.Where(t => t.Rate < 100).Count() > 0);
                item.IsFinish = (handler.Count() > 0 && handler.Where(t => t.Rate < 100).Count() == 0);
            }
            return data;
        }


        public int Import(List<RiskIncidentBO> riskIncidentsImport)
        {
            var numberInportSuccess = 0;
            var numberInportFail = 0;
            foreach (var item in riskIncidentsImport)
            {
                if (!base.GetQuery().Any(t => t.CodeIncident.Trim().ToUpper() == item.CodeIncident.Trim().ToUpper()))
                {
                    var procedureobject = new RiskIncidentBO()
                    {
                        DepartmentId = item.DepartmentId,
                        Conclude = item.Conclude,
                        CodeIncident = item.CodeIncident,
                        Date = item.Date,
                        Reason = item.Reason,
                        Description = item.Description,
                        AnotherIdea = item.AnotherIdea
                    };
                    base.Insert(procedureobject);
                    numberInportSuccess++;
                }
            }
            numberInportFail = riskIncidentsImport.Count - numberInportSuccess;
            return numberInportFail;
        }
    }
}
