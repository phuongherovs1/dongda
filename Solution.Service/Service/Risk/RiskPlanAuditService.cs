﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
namespace iDAS.Service
{
    public class RiskPlanAuditService : BaseService<RiskPlanAuditDTO, RiskPlanAuditBO>, IRiskPlanAuditService
    {

        public void Save(RiskPlanAuditBO data, string jsonData = "", bool allowSave = true)
        {
            try
            {
                // Insert plan
                // 1. Create new plan
                data.DateAudit = DateTime.Now;
                var planId = ServiceFactory<RiskPlanAuditService>().Insert(data, allowSave);
                //2 insert Asset audit
                ServiceFactory<RiskAssetAuditService>().InsertRange(planId, jsonData, allowSave);
            }
            catch
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }

    }
}
