﻿using iDAS.DataAccess;
using System;
using System.Linq;

namespace iDAS.Service
{
    public class RiskTargetTaskService : BaseService<RiskTargetTaskDTO, RiskTargetTaskBO>, IRiskTargetTaskService
    {
        public void DeleteTask(Guid id)
        {
            try
            {
                ServiceFactory<TaskService>().Delete(id, false);
                var RiskTargetTaskIds = base.GetQuery().Where(p => p.TaskId == id && p.IsDelete != true).Select(p => p.Id);
                base.DeleteRange(RiskTargetTaskIds, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
