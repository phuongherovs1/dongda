﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class RiskTargetService : BaseService<RiskTargetDTO, RiskTargetBO>, IRiskTargetService
    {


        public IEnumerable<RiskTargetBO> GetPolicy(bool allowDeleted = false)
        {
            var data = base.Get(t => t.IsPolicy.HasValue && t.IsPolicy.Value);
            return data;
        }


        public RiskTargetBO GetdatabyId(Guid Id)
        {
            var data = base.GetById(Id);
            try
            {
                if (data.DepartmentId != null)
                {
                    var Department = ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value);
                    data.DepartmentName = Department.Name;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return data;
        }

        public RiskTargetBO CreateDefault(Guid departmentId)
        {
            var data = new RiskTargetBO() { };
            try
            {
                var Department = ServiceFactory<DepartmentService>().GetById(departmentId);
                data.DepartmentId = departmentId;
                data.DepartmentName = Department.Name;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return data;
        }


        public IEnumerable<RiskTargetBO> GetScene(Guid departmentId, int Year, bool allowDeleted = false)
        {
            var data = base.Get(t => t.IsScene.HasValue && t.IsScene.Value && t.DepartmentId == departmentId && t.CreateAt.HasValue && t.CreateAt.Value.Year == Year);
            return data;
        }

        public List<ListYearBO> GetYear(Guid departmentId)
        {
            var year = new List<ListYearBO>();
            var data = base.Get(t => t.DepartmentId == departmentId).Select(t => t.CreateAt.Value.Year).Distinct().ToList();
            foreach (var item in data)
            {
                var listyear = new ListYearBO();
                listyear.year = item;
                year.Add(listyear);
            }
            return year;
        }


        public IEnumerable<RiskTargetBO> GetTarget(Guid departmentId, int Year, bool allowDeleted = false)
        {
            var data = base.GetQuery().Where(t => t.IsTarget.HasValue && t.IsTarget.Value && t.DepartmentId == departmentId
                       && t.CreateAt.HasValue && t.CreateAt.Value.Year == Year);
            return data;
        }
    }
}
