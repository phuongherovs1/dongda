﻿using iDAS.DataAccess;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class RiskEventLogService : BaseService<RiskEventLogDTO, RiskEventLogBO>, IRiskEventLogService
    {
        public IEnumerable<RiskEventLogBO> GetAll(int pageIndex, int pageSize, out int count, string departmentId = default(string))
        {
            departmentId = departmentId.Replace("Department_", string.Empty);
            var guidDepartmentId = Common.Utilities.ConvertToGuid(departmentId);
            var result = base.GetQuery().Where(t => t.DepartmentId.Value == guidDepartmentId || departmentId == string.Empty || departmentId == default(string));
            count = result.Count();
            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }
        public override System.Guid Insert(iDAS.Service.RiskEventLogBO data, bool allowSave = true)
        {
            if (data.IsIncident == true)
            {
                var obj = new RiskIncidentBO();
                obj.DepartmentId = data.DepartmentId;
                obj.Description = data.SummaryEvent;
                obj.CodeIncident = data.CodeIncident;
                obj.Date = data.Date;
                obj.Reason = data.Reason;
                ServiceFactory<RiskIncidentService>().Insert(obj);
            }
            return base.Insert(data, allowSave);
        }
        public override void Update(RiskEventLogBO data, bool allowSave = true)
        {
            if (data.IsIncident == true)
            {
                var obj = new RiskIncidentBO();
                obj.DepartmentId = data.DepartmentId;
                obj.Description = data.SummaryEvent;
                obj.CodeIncident = data.CodeIncident;
                obj.Date = data.Date;
                obj.Reason = data.Reason;
                ServiceFactory<RiskIncidentService>().Insert(obj);
            }
            base.Update(data, allowSave);
        }


        public int Import(List<RiskEventLogBO> eventLogsImport)
        {
            var numberInportSuccess = 0;
            var numberInportFail = 0;
            foreach (var item in eventLogsImport)
            {
                if (!base.GetQuery().Any(t => t.Code.Trim().ToUpper() == item.Code.Trim().ToUpper()))
                {
                    var procedureobject = new RiskEventLogBO()
                    {
                        DepartmentId = item.DepartmentId,
                        SummaryEvent = item.SummaryEvent,
                        CodeIncident = item.CodeIncident,
                        Date = item.Date,
                        Reason = item.Reason,
                        ProcessHandler = item.ProcessHandler,
                        IsIncident = item.IsIncident,
                        Info = item.Info,
                        Code = item.Code,
                    };
                    base.Insert(procedureobject);
                    numberInportSuccess++;
                }
            }
            numberInportFail = eventLogsImport.Count - numberInportSuccess;
            return numberInportFail;
        }
    }
}
