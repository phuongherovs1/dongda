﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ConfigTableService : BaseService<ConfigTableDTO, ConfigTableBO>, IConfigTableService
    {
        #region Override
        public Guid Insert(ConfigTableBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Configkey.Trim().ToUpper() == data.Configkey.Trim().ToUpper() && i.Id != data.Id && i.IsDelete == false).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            data.Id = Insert(data, allowSave);
            if (!string.IsNullOrEmpty(dataPropertySettings))
            {
                var dataProperties = JsonConvert.DeserializeObject<List<AssetPropertySettingBO>>(dataPropertySettings);
                ServiceFactory<AssetPropertySettingService>().InsertRange(data.Id, dataProperties, allowSave);
            }
            if (!string.IsNullOrEmpty(dataResourceRelates))
            {
                var resoureRelates = JsonConvert.DeserializeObject<List<AssetResourceRelateBO>>(dataResourceRelates);
                ServiceFactory<AssetResourceRelateService>().InsertRange(data.Id, resoureRelates, allowSave);
            }
            return data.Id;
        }

        public void Update(ConfigTableBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Configkey.Trim().ToUpper() == data.Configkey.Trim().ToUpper() && i.Id != data.Id && i.IsDelete == false).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            Update(data, allowSave);
            if (!string.IsNullOrEmpty(dataPropertySettings))
            {
                var dataProperties = JsonConvert.DeserializeObject<List<AssetPropertySettingBO>>(dataPropertySettings);
                ServiceFactory<AssetPropertySettingService>().Update(data.Id, dataProperties, allowSave);
            }
            if (!string.IsNullOrEmpty(dataResourceRelates))
            {
                var resoureRelates = JsonConvert.DeserializeObject<List<AssetResourceRelateBO>>(dataResourceRelates);
                ServiceFactory<AssetResourceRelateService>().Update(data.Id, resoureRelates, allowSave);
            }
        }

        public override IEnumerable<ConfigTableBO> GetAll(bool allowDeleted = false)
        {
            var data = base.GetAll(allowDeleted);
            return data;
        }

        public IEnumerable<ConfigTableBO> GetAll(int pageIndex, int pageSize, out int count, bool allowDeleted = false)
        {
            count = GetAll(allowDeleted).Count();
            var data = Page(GetAll(allowDeleted).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }
        public IEnumerable<ConfigTableBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count, bool allowDeleted = false)
        {
            count = GetAll(allowDeleted).Count();
            var data = Page(GetAll(allowDeleted).Where(x => x.Configkey.Contains(filterName) || x.ConfigValue.Contains(filterName)).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }
        public IEnumerable<ConfigTableBO> GetByConfigByType(string type, bool allowDeleted = false)
        {
            var data = Get(x => x.ConfigType.ToLower() == type.ToLower());
            return data;
        }

        #endregion
        #region Overload
        public override ConfigTableBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id, allowDeleted);
            return data;
        }
        public override ConfigTableBO GetJustById(Guid id, bool allowDefaultIfNull = true)
        {
            var data = base.GetJustById(id);
            return data;
        }
        public string GetByConfigKey(string key, bool allowDeleted = false)
        {
            var data = Get(x => x.Configkey == key && allowDeleted == false).ToList();
            if (data != null && data.Count > 0)
                return data[0].ConfigValue;
            return string.Empty;
        }
        #endregion
        #region Rule
        public ConfigTableBO GetFormItem()
        {
            try
            {
                var data = new ConfigTableBO()
                {
                    //Id = Guid.NewGuid(),
                    //Name = string.Empty,
                    //Description = string.Empty
                };
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
