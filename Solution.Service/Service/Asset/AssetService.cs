﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace iDAS.Service
{
    public class AssetService : BaseService<AssetDTO, AssetBO>, IAssetService
    {
        #region Override
        public Guid Insert(AssetBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Code.Trim().ToUpper() == data.Code.Trim().ToUpper() && i.Id != data.Id).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            data.Id = Insert(data, true);
            if (!string.IsNullOrEmpty(dataPropertySettings))
            {
                var dataProperties = JsonConvert.DeserializeObject<List<AssetPropertySettingBO>>(dataPropertySettings);
                ServiceFactory<AssetPropertySettingService>().InsertRange(data.Id, dataProperties);
            }
            if (!string.IsNullOrEmpty(dataResourceRelates))
            {
                var resoureRelates = JsonConvert.DeserializeObject<List<AssetResourceRelateBO>>(dataResourceRelates);
                ServiceFactory<AssetResourceRelateService>().InsertRange(data.Id, resoureRelates);
            }
            return data.Id;
        }

        public void Update(AssetBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Code.Trim().ToUpper() == data.Code.Trim().ToUpper() && i.Id != data.Id).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            Update(data, false);
            if (!string.IsNullOrEmpty(dataPropertySettings))
            {
                var dataProperties = JsonConvert.DeserializeObject<List<AssetPropertySettingBO>>(dataPropertySettings);
                ServiceFactory<AssetPropertySettingService>().Update(data.Id, dataProperties, false);
            }
            if (!string.IsNullOrEmpty(dataResourceRelates))
            {
                var resoureRelates = JsonConvert.DeserializeObject<List<AssetResourceRelateBO>>(dataResourceRelates);
                ServiceFactory<AssetResourceRelateService>().Update(data.Id, resoureRelates, false);
            }
            SaveTransaction(allowSave);
        }

        public override IEnumerable<AssetBO> GetAll(bool allowDeleted = false)
        {
            var data = base.GetAll(allowDeleted).Select(i =>
            {
                var group = ServiceFactory<AssetGroupService>().GetById(i.AssetGroupID.Value);
                i.AssetGroupName = group == null ? string.Empty : group.Name;
                return i;
            });
            return data;
        }

        public IEnumerable<AssetBO> GetAll(string connection, int pageIndex, int pageSize, out int count)
        {
            var types = new List<AssetTypeBO>();
            //count = GetAll().Count();
            //var data = Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            List<AssetBO> data = new List<AssetBO>();
            string query = "SELECT * FROM Assets WHERE IsDelete = 0";
            if (pageSize != -1)
                query += " order by CreateAt DESC OFFSET " + (((int)pageIndex - 1) * (int)pageSize) + " ROWS FETCH NEXT " + (int)pageSize + " ROWS ONLY";
            count = (int)GetTableByQuery("SELECT count(Id) FROM Assets", connection).Rows[0][0];
            DataTable dataTable_AssetResourceRelateBO = GetTableByQuery(query, connection);
            for (int i = 0; i < dataTable_AssetResourceRelateBO.Rows.Count; i++)
            {
                try
                {
                    AssetBO item = new AssetBO()
                    {
                        Id = new Guid(dataTable_AssetResourceRelateBO.Rows[i][0].ToString()),
                        Code = dataTable_AssetResourceRelateBO.Rows[i][1].ToString(),
                        Name = dataTable_AssetResourceRelateBO.Rows[i][2].ToString(),
                        AssetGroupID = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][3].ToString()) ? new Guid(dataTable_AssetResourceRelateBO.Rows[i][3].ToString()) : Guid.Empty,
                        TypeTitleId = dataTable_AssetResourceRelateBO.Rows[i][4].ToString(),
                        TitleId = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][5].ToString()) ? new Guid(dataTable_AssetResourceRelateBO.Rows[i][5].ToString()) : Guid.Empty,
                        Unit = dataTable_AssetResourceRelateBO.Rows[i][6].ToString(),
                        Quantity = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][7].ToString()) ? (double)dataTable_AssetResourceRelateBO.Rows[i][7] : 0,
                        IsDelete = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][8].ToString()) ? (bool)dataTable_AssetResourceRelateBO.Rows[i][8] : false,
                        CreateAt = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][9].ToString()) ? Convert.ToDateTime(dataTable_AssetResourceRelateBO.Rows[i][9].ToString()) : DateTime.MinValue,
                        CreateBy = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][10].ToString()) ? new Guid(dataTable_AssetResourceRelateBO.Rows[i][10].ToString()) : Guid.Empty,
                        UpdateAt = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][11].ToString()) ? Convert.ToDateTime(dataTable_AssetResourceRelateBO.Rows[i][11].ToString()) : DateTime.MinValue,
                        UpdateBy = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][12].ToString()) ? new Guid(dataTable_AssetResourceRelateBO.Rows[i][12].ToString()) : Guid.Empty,
                    };
                    data.Add(item);
                }
                catch (Exception e) { }
            }
            foreach (var item in data)
            {
                //// Get property by asset 
                //var propertyIds = ServiceFactory<AssetPropertySettingService>().GetPropertyIdsByAssetID(item.Id);
                //// check type has exist into types
                //var checkType = types.FirstOrDefault(i => i.PropetyIds.SequenceEqual(propertyIds));
                //// if not exist: get type by property and add to types
                //if (checkType == null)
                //{
                //    checkType = ServiceFactory<AssetTypeService>().GetByProperty(propertyIds);
                //    if (checkType != null)
                //    {
                //        checkType.PropetyIds = propertyIds;
                //        types.Add(checkType);
                //        item.AssetTypeName = checkType.Name;
                //    }
                //    else
                //    {
                //        item.AssetTypeName = Resource.CommonLabel.Undefine;
                //    }
                //}
                //else
                //{
                //    item.AssetTypeName = checkType.Name;
                //}
                var temp = ServiceFactory<AssetTempService>().GetTempByAssetId(item.Id);
                item.Temp = new FileUploadBO()
                {
                    Files = temp.ToList()
                };
                var tempitem = ServiceFactory<AssetTempService>().Get(x => x.AssetID == item.Id);
                item.LabelTemp = tempitem != null && tempitem.ToList().Count > 0 ? string.Join(",", tempitem.ToList().Where(y => !string.IsNullOrEmpty(y.LabelTemp)).Select(x => x.LabelTemp)) : "";
            }
            return data;
        }

        #endregion
        #region Overload
        public override AssetBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id);

            var group = ServiceFactory<AssetGroupService>().GetById(data.AssetGroupID.HasValue ? data.AssetGroupID.Value : Guid.Empty);
            data.AssetGroupName = group != null ? group.Name : string.Empty;
            var temp = ServiceFactory<AssetTempService>().GetTempByAssetId(id);
            data.Temp = new FileUploadBO()
            {
                Files = temp.ToList()
            };

            return data;
        }
        #endregion
        #region Rule
        public AssetBO GetFormItem()
        {
            try
            {
                var data = new AssetBO()
                {
                    Id = Guid.NewGuid(),
                    Code = GenerateCode()
                };
                return data;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private string GenerateCode()
        {
            var code = string.Format("TS{0}01", DateTime.Now.ToString("yyMMdd"));
            try
            {
                var lastItem = Get(i => i.CreateAt.Value.Date == DateTime.Now.Date).OrderByDescending(i => i.CreateAt).FirstOrDefault();
                if (lastItem != null)
                {
                    var numberCode = lastItem.Code.Substring(8);
                    var newNumber = Convert.ToInt32(numberCode);
                    var newNumberCode = ++newNumber < 10 ? "0" + newNumber : "" + newNumber;
                    code = string.Format("TS{0}{1}", DateTime.Now.ToString("yyMMdd"), newNumberCode);
                }

            }
            catch (Exception)
            {
            }
            return code;
        }
        #endregion
        public int Import(List<AssetBO> objects)
        {
            var numberInportSuccess = 0;
            var numberInportFail = 0;
            foreach (var item in objects)
            {
                if (!base.GetQuery().Any(t => t.Code.Trim().ToUpper() == item.Code.Trim().ToUpper()))
                {
                    var procedureobject = new AssetBO()
                    {
                        AssetGroupID = item.AssetGroupID,
                        Name = item.Name,
                        Code = item.Code,
                        Quantity = item.Quantity,
                        Unit = item.Unit
                    };
                    base.Insert(procedureobject);
                    numberInportSuccess++;
                }
            }
            numberInportFail = objects.Count - numberInportSuccess;
            return numberInportFail;
        }
    }
}
