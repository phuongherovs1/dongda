﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class AssetPropertyService : BaseService<AssetPropertyDTO, AssetPropertyBO>, IAssetPropertyService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        public IEnumerable<AssetPropertyBO> GetByAsset(Guid assetId)
        {
            //1. Get property ids from Asset propery setting
            var propertyIds = ServiceFactory<AssetPropertySettingService>().GetPropertyIdsByAssetID(assetId);
            //2. Get asset by ids
            return GetByIds(propertyIds);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="assetTypeId"></param>
        /// <returns></returns>
        public IEnumerable<AssetPropertyBO> GetByAssetType(Guid assetTypeId)
        {
            //1. Get property ids from asset type rule ids
            var propertyIds = ServiceFactory<AssetTypePropertyService>().GetPropertyIdsByAssetTypeID(assetTypeId);
            //2. get asset by ids
            return GetByIds(propertyIds);
        }
        public IEnumerable<AssetPropertyBO> GetContainName(string key)
        {
            var data = Get(i => i.Name.ToUpper().Contains(key.ToUpper())).OrderByDescending(i => i.CreateAt);
            return data;
        }
        public override Guid Insert(AssetPropertyBO data, bool allowSave = true)
        {
            var checkProperty = Get(i => i.Name.Trim().ToUpper() == data.Name.Trim().ToUpper()).Any();
            if (checkProperty) throw new DataHasBeenExistedException();
            else
                return base.Insert(data, allowSave);
        }
        public IEnumerable<AssetPropertyBO> GetQuery(int pageIndex = 1, int pageSize = 20, string query = "")
        {
            var data = Get(i => i.Name.ToUpper().Contains(query.ToUpper())).OrderByDescending(i => i.CreateAt);
            return Page(data, pageIndex, pageSize);
        }
        public IEnumerable<AssetPropertyBO> QueryFilterAssetType(Guid assetTypeId, Guid currentProperty, int pageIndex = 1, int pageSize = 20, string query = "")
        {
            var propertyByAsset = ServiceFactory<AssetTypePropertyService>().GetPropertyIdsByAssetTypeID(assetTypeId);
            if (currentProperty != Guid.Empty)
                propertyByAsset = propertyByAsset.Except(new[] { currentProperty });
            var data = GetNotInIds(propertyByAsset);
            data = data.Where(i => i.Name.ToUpper().Contains(query.ToUpper())).OrderByDescending(i => i.CreateAt);
            return data;// Page(data, pageIndex, pageSize);
        }
        public IEnumerable<AssetPropertyBO> GetPropertyFilterType(IEnumerable<Guid> propertyIds, string query = "")
        {
            IEnumerable<AssetPropertyBO> data = Enumerable.Empty<AssetPropertyBO>();
            //1. if propertyIds is empty, return all properties
            if (propertyIds == null || propertyIds.Count() == 0)
            {
                data = Get();
            }
            //2. do step by step: 
            else
            {
                var ids = ServiceFactory<AssetTypePropertyService>().GetSelectPropertyType(propertyIds);
                //3. Except properties where have id in propertyIds
                ids = ids.Except(propertyIds);
                data = GetByIds(ids);
            }
            data = string.IsNullOrEmpty(query) ? data : data.Where(i => i.Name.ToUpper().Contains(query.Trim().ToUpper()));
            return data;
        }
    }
}
