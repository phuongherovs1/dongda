﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace iDAS.Service
{
    public class AssetTempService : BaseService<AssetTempDTO, AssetTempBO>, IAssetTempService
    {
        public override IEnumerable<AssetTempBO> GetAll(bool allowDeleted = false)
        {
            return base.GetAll(allowDeleted);
        }

        public IEnumerable<AssetTempBO> GetByAssetId(Guid assetId)
        {

            var data = Get(i => i.AssetID == assetId).OrderByDescending(i => i.CreateAt)
                .Select(
                i =>
                {
                    var employee = ServiceFactory<EmployeeService>().GetById(i.Paster.Value);
                    var template = ServiceFactory<AssetTempSettingService>().GetById(i.TempSettingID.Value);
                    i.TemplateName = template.Name;
                    i.AvartarUrl = employee.AvatarUrl;
                    i.EmployeeName = employee.Name;
                    i.Template = template.TemplateFile;
                    i.Temp = new FileUploadBO()
                    {
                        Files = i.TempFile.HasValue ? new List<Guid> { i.TempFile.Value } : new List<Guid>()
                    };
                    return i;
                }
                ).ToList();

            return data;

        }

        public IEnumerable<Guid> GetTempByAssetId(Guid assetId)
        {
            try
            {
                var data = Get(i => i.AssetID == assetId).Where(x => x.TempFile.HasValue)
                    .Select(
                    i => i.TempFile.Value
                    );
                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public override Guid Insert(AssetTempBO data, bool allowSave = true)
        {
            data.Paster = data.Creater.Id;
            if (data.Template.FileAttachments.Any() && data.Template.FileAttachments[0] != null)
                data.TempFile = ServiceFactory<FileService>().Upload(data.Template);
            return base.Insert(data, allowSave);
        }
        public override void Update(AssetTempBO data, bool allowSave = true)
        {
            data.Paster = data.Creater.Id;
            if (data.Template.FileRemoves.Any())
            {
                foreach (Guid item in data.Template.FileRemoves)
                {
                    var result = ServiceFactory<FileService>().GetById(item);
                    File.Delete(HttpContext.Current.Server.MapPath("/Upload/" + item.ToString() + "." + result.Extension));
                }
                ServiceFactory<FileService>().DeleteRange(data.Template.FileRemoves);
            }
            if (data.Template.FileAttachments.Any() && data.Template.FileAttachments[0] != null)
                data.TempFile = ServiceFactory<FileService>().Upload(data.Template);
            base.Update(data, allowSave);
        }
    }
}
