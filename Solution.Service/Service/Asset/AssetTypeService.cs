﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class AssetTypeService : BaseService<AssetTypeDTO, AssetTypeBO>, IAssetTypeService
    {
        public override void Update(AssetTypeBO data, bool allowSave = true)
        {
            if (data.Id == Guid.Empty)
                Insert(data, allowSave);
            else
                base.Update(data, allowSave);
        }

        public override void Delete(Guid id, bool allowSave = true)
        {
            ServiceFactory<AssetTypePropertyService>().DeleteByAssetType(id, false);
            base.Delete(id, allowSave);
        }

        public override void DeleteRange(IEnumerable<Guid> ids, bool allowSave = true)
        {
            base.DeleteRange(ids, allowSave);
        }

        public Guid Insert(AssetTypeBO data, string typeProperties = "", bool allowSave = true)
        {
            var existName = Get(i => i.Name.Trim().ToUpper() == data.Name.Trim().ToUpper() && i.Id != data.Id).Any();
            if (existName)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            var typeID = Insert(data, true);
            if (!string.IsNullOrEmpty(typeProperties))
            {
                var properties = JsonConvert.DeserializeObject<List<AssetTypePropertyBO>>(typeProperties);
                ServiceFactory<AssetTypePropertyService>().InsertRange(typeID, properties, false);
            }
            SaveTransaction(allowSave);
            return typeID;
        }

        public void Update(AssetTypeBO data, string typeProperties = "", bool allowSave = true)
        {
            var existName = Get(i => i.Name.Trim().ToUpper() == data.Name.Trim().ToUpper() && i.Id != data.Id).Any();
            if (existName)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            Update(data, true);
            if (!string.IsNullOrEmpty(typeProperties))
            {
                var properties = JsonConvert.DeserializeObject<List<AssetTypePropertyBO>>(typeProperties);
                ServiceFactory<AssetTypePropertyService>().UpdateRange(data.Id, properties, true, true);
            }
        }

        public AssetTypeBO GetByAsset(Guid assetId)
        {
            var propertyIds = ServiceFactory<AssetPropertySettingService>().GetIdsByAsset(assetId).Select(i => i.PropertyId.Value).ToList();
            var result = GetByProperty(propertyIds);
            return result;
        }

        public AssetTypeBO GetByProperty(IEnumerable<Guid> propertyIds)
        {
            var typeId = ServiceFactory<AssetTypePropertyService>().GetType(propertyIds);

            if (typeId == Guid.Empty)
                return null;
            else
            {
                return GetById(typeId);
            }
        }
    }
}