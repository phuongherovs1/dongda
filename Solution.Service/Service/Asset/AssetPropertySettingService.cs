﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class AssetPropertySettingService : BaseService<AssetPropertySettingDTO, AssetPropertySettingBO>, IAssetPropertySettingService
    {
        public IEnumerable<Guid> GetPropertyIdsByAssetID(Guid assetId)
        {
            return this.Get(u => u.AssetId == assetId).OrderBy(i => i.PropertyId.Value).Select(i => i.PropertyId.Value);
        }

        public IEnumerable<Guid> InsertRange(Guid assetID, IEnumerable<AssetPropertySettingBO> properties, bool allowSave = true)
        {
            var itemInsert = properties.Where(u => u.Id == null || u.Id == Guid.Empty);
            var itemUpdate = properties.Where(u => u.Id != null && u.Id != Guid.Empty);
            var insertIds = base.InsertRange(itemInsert, false);
            Update(itemUpdate, allowSave);
            SaveTransaction(allowSave);
            var result = insertIds.Concat(itemUpdate.Select(i => i.Id));
            return result;
        }

        public void Update(Guid assetID, IEnumerable<AssetPropertySettingBO> properties, bool allowSave = true)
        {
            var itemDelete = Get(i => i.AssetId == assetID && !properties.Select(u => u.PropertyId.Value).Contains(i.PropertyId.Value)).Select(i => i.Id);
            DeleteRange(itemDelete, false);
            var itemInsert = properties.Where(u => u.Id == null || u.Id == Guid.Empty);
            var itemUpdate = properties.Where(u => u.Id != null && u.Id != Guid.Empty);
            var insertIds = base.InsertRange(itemInsert, false);
            Update(itemUpdate, allowSave);
            SaveTransaction(allowSave);
            var result = insertIds.Concat(itemUpdate.Select(i => i.Id));

        }

        public IEnumerable<AssetPropertySettingBO> GetByAsset(Guid assetId)
        {
            var data = Get(i => i.AssetId == assetId)
                .Select(i =>
                {
                    var property = ServiceFactory<AssetPropertyService>().GetById(i.PropertyId.Value);
                    i.PropertyName = property.Name;
                    return i;
                });
            return data;
        }

        public IEnumerable<AssetPropertySettingBO> GetIdsByAsset(Guid assetId)
        {
            var data = Get(i => i.AssetId == assetId);//.ToList()
            return data;
        }
        public void Update(IEnumerable<AssetPropertySettingBO> properties, bool allowSave = true)
        {
            foreach (var item in properties)
            {
                base.Update(item, false);
            }
        }
    }
}
