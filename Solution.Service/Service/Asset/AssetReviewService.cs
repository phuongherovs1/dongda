﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class AssetReviewService : BaseService<AssetReviewDTO, AssetReviewBO>, IAssetReviewService
    {
        public override Guid Insert(AssetReviewBO data, bool allowSave = true)
        {
            data.EmployeeId = string.IsNullOrEmpty(data.Other) ? data.Reviewer.Id : (Guid?)null;
            return base.Insert(data, allowSave);
        }
        public AssetReviewBO GetLastByAssetResource(Guid assetResourceID)
        {
            var data = Get(i => i.AssetResourceRelateId == assetResourceID).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            if (data != null)
            {
                var importantResult = ServiceFactory<AssetReviewSettingService>().GetByValue(data.ValueOfImportant, Common.Resource.QualityReview.IsImportant);
                var inviblateResult = ServiceFactory<AssetReviewSettingService>().GetByValue(data.ValueOfInviblate.Value, Common.Resource.QualityReview.IsInviblate);
                var readyResult = ServiceFactory<AssetReviewSettingService>().GetByValue(data.ValueOfReady.Value, Common.Resource.QualityReview.IsReady);
                var securityResult = ServiceFactory<AssetReviewSettingService>().GetByValue(data.ValueOfSecurity.Value, Common.Resource.QualityReview.IsSecurity);
                data.ImportantName = importantResult.Name;
                data.InviblateName = inviblateResult.Name;
                data.ReadyName = readyResult.Name;
                data.SecurityName = securityResult.Name;
                data.ImportantColor = importantResult.Color;
            }
            return data;
        }

        public IEnumerable<AssetReviewBO> GetByAssetResource(Guid assetResourceID)
        {
            var data = Get(i => i.AssetResourceRelateId == assetResourceID).OrderByDescending(i => i.CreateAt)
                .Select(
                i =>
                {
                    if (i.EmployeeId.HasValue)
                    {
                        var employeeReviewer = ServiceFactory<EmployeeService>().GetById(i.EmployeeId.Value);
                        i.ReviewerAvatarUrl = employeeReviewer.AvatarUrl;
                    }
                    var importantResult = ServiceFactory<AssetReviewSettingService>().GetByValue(i.ValueOfImportant, Common.Resource.QualityReview.IsImportant);
                    var inviblateResult = ServiceFactory<AssetReviewSettingService>().GetByValue(i.ValueOfInviblate.Value, Common.Resource.QualityReview.IsInviblate);
                    var readyResult = ServiceFactory<AssetReviewSettingService>().GetByValue(i.ValueOfReady.Value, Common.Resource.QualityReview.IsReady);
                    var securityResult = ServiceFactory<AssetReviewSettingService>().GetByValue(i.ValueOfSecurity.Value, Common.Resource.QualityReview.IsSecurity);
                    // var tempSetting = ServiceFactory<AssetTempSettingService>().GetById(importantResult)
                    i.ImportantName = importantResult.Name;
                    i.InviblateName = inviblateResult.Name;
                    i.ReadyName = readyResult.Name;
                    i.SecurityName = securityResult.Name;
                    i.ImportantColor = importantResult.Color;
                    return i;
                }
                );
            return data;
        }

        public IEnumerable<AssetReviewBO> GetByAsset(Guid assetId)
        {
            var assetResourceIds = ServiceFactory<AssetResourceRelateService>().GetByAssetId(assetId).Select(i => i.Id);
            var data = Get(i => assetResourceIds.Contains(i.AssetResourceRelateId.Value)).OrderByDescending(i => i.CreateAt)
                .Select(
                i =>
                {
                    var importantResult = ServiceFactory<AssetReviewSettingService>().GetByValue(i.ValueOfImportant, Common.Resource.QualityReview.IsImportant);
                    if (i.EmployeeId.HasValue)
                    {
                        var employeeReviewer = ServiceFactory<EmployeeService>().GetById(i.EmployeeId.Value);
                        i.ReviewerAvatarUrl = employeeReviewer.AvatarUrl;
                    }
                    i.ImportantName = importantResult.Name;
                    i.ImportantColor = importantResult.Color;
                    return i;
                }
                );
            return data;
        }
    }
}
