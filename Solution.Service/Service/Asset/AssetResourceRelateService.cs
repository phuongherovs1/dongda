﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace iDAS.Service
{
    public class AssetResourceRelateService : BaseService<AssetResourceRelateDTO, AssetResourceRelateBO>, IAssetResourceRelateService
    {
        public IEnumerable<AssetResourceRelateBO> GetSelectObject(Resource.ObjectRelative selectObject, string query = "")
        {
            var result = Enumerable.Empty<AssetResourceRelateBO>();
            switch (selectObject)
            {
                case Resource.ObjectRelative.Department:
                    {
                        var data = ServiceFactory<DepartmentService>().Get(i => string.IsNullOrEmpty(query) ? true : i.Name.ToUpper().Contains(query.ToUpper()));
                        result = (from i in data
                                  select new AssetResourceRelateBO
                                  {
                                      DepartmentId = i.Id,
                                      ObjectId = i.Id,
                                      ObjectName = i.Name,
                                      ObjectRelate = selectObject,
                                      ObjectImage = "/Content/images/underfind.jpg"
                                  });
                    }
                    break;
                case Resource.ObjectRelative.Role:
                    {
                        var data = ServiceFactory<DepartmentTitleService>().Get(i => string.IsNullOrEmpty(query) ? true : i.Name.ToUpper().Contains(query.ToUpper()));
                        result = (from i in data
                                  select new AssetResourceRelateBO
                                  {
                                      RoleId = i.Id,
                                      ObjectId = i.Id,
                                      ObjectName = i.Name,
                                      ObjectRelate = selectObject,
                                      ObjectImage = "/Content/images/underfind.jpg"
                                  });
                    }
                    break;
                case Resource.ObjectRelative.Employee:
                    {
                        var data = ServiceFactory<EmployeeService>().Get(i => string.IsNullOrEmpty(query) ? true : i.Name.ToUpper().Contains(query.ToUpper()));
                        result = (from i in data
                                  select new AssetResourceRelateBO
                                  {
                                      EmployeeId = i.Id,
                                      ObjectId = i.Id,
                                      ObjectName = i.Name,
                                      ObjectRelate = selectObject,
                                      ObjectImage = i.AvatarUrl
                                  });
                    }
                    break;
                case Resource.ObjectRelative.Other:
                    result = new List<AssetResourceRelateBO>(){
                        new AssetResourceRelateBO(){ ObjectId = Guid.Empty, ObjectName = string.Empty, ObjectRelate = selectObject}
                    };
                    break;
                default:
                    result = new List<AssetResourceRelateBO>(){
                        new AssetResourceRelateBO(){ ObjectId = Guid.Empty, ObjectName = string.Empty, ObjectRelate = selectObject}
                    };
                    break;
            }

            return result;
        }
        public IEnumerable<AssetResourceRelateBO> GetByAssetId(Guid assetId)
        {
            var data = Get(i => i.AssetId == assetId)
                .Select(
                i =>
                {
                    i.ObjectName = i.DepartmentId.HasValue ? ServiceFactory<DepartmentService>().GetById(i.DepartmentId.Value).Name
                        : i.EmployeeId.HasValue ? ServiceFactory<EmployeeService>().GetById(i.EmployeeId.Value).Name
                        : i.RoleId.HasValue ? ServiceFactory<DepartmentTitleService>().GetById(i.RoleId.Value).Name
                        : i.Other;
                    i.ObjectImage = i.EmployeeId.HasValue ? ServiceFactory<EmployeeService>().GetById(i.EmployeeId.Value).AvatarUrl
                        : i.Other;
                    return i;
                });
            return data;
        }
        public IEnumerable<Guid> InsertRange(Guid assetID, IEnumerable<AssetResourceRelateBO> data, bool allowSave = true)
        {
            var itemInsert = data.Where(u => u.Id == null || u.Id == Guid.Empty);
            var itemUpdate = data.Where(u => u.Id != null && u.Id != Guid.Empty);
            var insertIds = base.InsertRange(itemInsert, false);
            Update(itemUpdate, allowSave);
            SaveTransaction(allowSave);
            var result = insertIds.Concat(itemUpdate.Select(i => i.Id));
            return result;
        }
        public void Update(IEnumerable<AssetResourceRelateBO> properties, bool allowSave = true)
        {
            foreach (var item in properties)
            {
                base.Update(item, false);
            }
        }
        public void Update(Guid assetID, IEnumerable<AssetResourceRelateBO> resoureRelates, bool allowSave = true)
        {
            var objectIds = resoureRelates.Where(i => i.ObjectId.HasValue).Select(u => u.ObjectId.Value);
            var itemDelete = Get(i => i.AssetId == assetID
                && ((i.RoleId.HasValue && !objectIds.Contains(i.RoleId.Value))
                || (i.DepartmentId.HasValue && !objectIds.Contains(i.DepartmentId.Value))
                || (i.EmployeeId.HasValue && !objectIds.Contains(i.EmployeeId.Value))
                || (!string.IsNullOrEmpty(i.Other) && !resoureRelates.Select(u => u.ObjectName).Contains(i.Other))
                )).Select(i => i.Id);
            DeleteRange(itemDelete, false);
            var itemInsert = resoureRelates.Where(u => u.Id == Guid.Empty);
            var itemUpdate = resoureRelates.Where(u => u.Id != Guid.Empty);
            var insertIds = base.InsertRange(itemInsert, false);
            Update(itemUpdate, false);
            SaveTransaction(allowSave);
        }
        public IEnumerable<AssetBO> GetAssetInfo(int pageIndex, int pageSize, out int count)
        {
            var data = Enumerable.Empty<AssetBO>();
            //Get loại tài sản cho phép đánh giá
            var typeAssetHaspermission = ServiceFactory<AssetTypeService>().Get(x => x.IsEvaluate == true);
            //Get lst ID thuộc tính của loại tài sản trên
            if (typeAssetHaspermission != null && typeAssetHaspermission.ToList().Count > 0)
            {
                var AssetTypeProperties = ServiceFactory<AssetTypePropertyService>().Get(x => x.AssetTypeId.HasValue && typeAssetHaspermission.ToList().Select(u => u.Id).Contains(x.AssetTypeId.Value));
                if (AssetTypeProperties != null && AssetTypeProperties.ToList().Count > 0)
                {
                    var AssetPropertySettings = ServiceFactory<AssetPropertySettingService>().Get(x => x.PropertyId.HasValue && AssetTypeProperties.ToList().Select(u => u.PropertyId.Value).Contains(x.PropertyId.Value));
                    if (AssetPropertySettings != null && AssetPropertySettings.ToList().Count > 0)
                    {
                        data = ServiceFactory<AssetService>().GetByIds(AssetPropertySettings.ToList().Select(y => y.AssetId.Value));
                    }
                }
            }
            count = data.Count();
            data = ServiceFactory<AssetService>().Page(data, pageIndex, pageSize).ToList();
            var types = new List<AssetTypeBO>();
            foreach (var item in data)
            {
                var group = ServiceFactory<AssetGroupService>().GetById(item.AssetGroupID.Value);
                item.AssetGroupName = group == null ? string.Empty : group.Name;
                // Get property by asset 
                var propertyIds = ServiceFactory<AssetPropertySettingService>().GetPropertyIdsByAssetID(item.Id);
                // check type has exist into types
                var checkType = types.FirstOrDefault(i => i.PropetyIds.SequenceEqual(propertyIds));
                // if not exist: get type by property and add to types
                if (checkType == null)
                {
                    checkType = ServiceFactory<AssetTypeService>().GetByProperty(propertyIds);
                    if (checkType != null)
                    {
                        checkType.PropetyIds = propertyIds;
                        types.Add(checkType);
                        item.AssetTypeName = checkType.Name;
                    }
                    else
                    {
                        item.AssetTypeName = Resource.CommonLabel.Undefine;
                    }
                }
                else
                {
                    item.AssetTypeName = checkType.Name;
                }
                var temp = ServiceFactory<AssetTempService>().GetTempByAssetId(item.Id);
                item.Temp = new FileUploadBO()
                {
                    Files = temp.ToList()
                };
                var tempitem = ServiceFactory<AssetTempService>().Get(x => x.AssetID == item.Id);
                item.LabelTemp = tempitem != null && tempitem.ToList().Count > 0 ? string.Join(",", tempitem.ToList().Where(y => !string.IsNullOrEmpty(y.LabelTemp)).Select(x => x.LabelTemp)) : "";
            }
            return data;
        }
        public IEnumerable<AssetBO> GetReview(int pageIndex, int pageSize, out int count, Guid? assetId = null)
        {
            var data = Enumerable.Empty<AssetBO>();
            //Get loại tài sản cho phép đánh giá
            var typeAssetHaspermission = ServiceFactory<AssetTypeService>().Get(x => x.IsEvaluate == true);
            //Get lst ID thuộc tính của loại tài sản trên
            if (typeAssetHaspermission != null && typeAssetHaspermission.ToList().Count > 0)
            {
                var AssetTypeProperties = ServiceFactory<AssetTypePropertyService>().Get(x => x.AssetTypeId.HasValue && typeAssetHaspermission.ToList().Select(u => u.Id).Contains(x.AssetTypeId.Value));
                if (AssetTypeProperties != null && AssetTypeProperties.ToList().Count > 0)
                {
                    var AssetPropertySettings = ServiceFactory<AssetPropertySettingService>().Get(x => x.PropertyId.HasValue && AssetTypeProperties.ToList().Select(u => u.PropertyId.Value).Contains(x.PropertyId.Value));
                    if (AssetPropertySettings != null && AssetPropertySettings.ToList().Count > 0)
                    {
                        data = ServiceFactory<AssetService>().GetByIds(AssetPropertySettings.ToList().Select(y => y.AssetId.Value));
                    }
                }
            }
            count = data.Count();
            data = ServiceFactory<AssetService>().Page(data, pageIndex, pageSize).ToList();
            // Use for storage temp setting affter query from database
            data = data.Select(i =>
             {
                 var lastReview = ServiceFactory<AssetReviewService>().GetLastByAssetResource(i.Id);
                 if (lastReview != null)
                 {
                     i.ValueOfInviblate = lastReview.ValueOfInviblate;
                     i.ValueOfReady = lastReview.ValueOfReady;
                     i.ValueOfSecurity = lastReview.ValueOfSecurity;
                     i.InviblateName = lastReview.InviblateName;
                     i.SecurityName = lastReview.SecurityName;
                     i.ReadyName = lastReview.ReadyName;
                     i.Value = lastReview.Value;
                     i.ImportantName = lastReview.ImportantName;
                 }
                 var temp = ServiceFactory<AssetTempService>().GetTempByAssetId(i.Id);
                 i.Temp = new FileUploadBO()
                 {
                     Files = temp.ToList()
                 };
                 var tempitem = ServiceFactory<AssetTempService>().Get(x => x.AssetID == i.Id);
                 i.LabelTemp = tempitem != null && tempitem.ToList().Count > 0 ? string.Join(",", tempitem.ToList().Where(y => !string.IsNullOrEmpty(y.LabelTemp)).Select(x => x.LabelTemp)) : "";
                 return i;
             }).OrderByDescending(x => x.CreateAt);
            return data;
        }
        public IEnumerable<AssetResourceRelateBO> GetRoleRelateTo(string connection, int pageIndex, int pageSize, out int count, string filterName = "")
        {
            #region Get AssetResourceRelateBO
            List<AssetResourceRelateBO> data = new List<AssetResourceRelateBO>();
            string query = "SELECT TB1.Id, AssetId, TB1.DepartmentId, IsUsing, RoleId, IsUse, IsManage, IsOwner, Other, ProcessExpediency, TB1.IsDelete, TB1.CreateAt, TB1.CreateBy, TB1.UpdateAt, TB1.UpdateBy, EmployeeId, TB2.Name, TB2.Code, TB3.Name, TB4.Name, TB5.Name FROM AssetResourceRelates as TB1 LEFT JOIN Assets as TB2 ON TB1.AssetId = TB2.Id LEFT JOIN Departments as TB3 ON TB1.DepartmentId = TB3.Id LEFT JOIN Employees as TB4 ON TB1.EmployeeId = TB4.Id LEFT JOIN DepartmentTitles as TB5 ON TB1.DepartmentId = TB5.Id WHERE IsUse = 1 AND TB1.IsDelete = 0";
            if (!string.IsNullOrEmpty(filterName))
                query = "SELECT TB1.Id, AssetId, TB1.DepartmentId, IsUsing, RoleId, IsUse, IsManage, IsOwner, Other, ProcessExpediency, TB1.IsDelete, TB1.CreateAt, TB1.CreateBy, TB1.UpdateAt, TB1.UpdateBy, EmployeeId, TB2.Name, TB2.Code, TB3.Name, TB4.Name, TB5.Name FROM AssetResourceRelates as TB1 LEFT JOIN Assets as TB2 ON TB1.AssetId = TB2.Id LEFT JOIN Departments as TB3 ON TB1.DepartmentId = TB3.Id LEFT JOIN Employees as TB4 ON TB1.EmployeeId = TB4.Id LEFT JOIN DepartmentTitles as TB5 ON TB1.DepartmentId = TB5.Id WHERE IsUse = 1 AND TB1.IsDelete = 0 AND (UPPER(TB3.Name) like '%" + filterName.ToUpper() + "%' OR UPPER(TB4.Name) like '%" + filterName.ToUpper() + "%' OR UPPER(TB4.Name) like '%" + filterName.ToUpper() + "%')";
            if (pageSize != -1)
                query += " order by TB1.CreateAt DESC OFFSET " + (((int)pageIndex - 1) * (int)pageSize) + " ROWS FETCH NEXT " + (int)pageSize + " ROWS ONLY";
            DataTable dataTable_AssetResourceRelateBO = GetTableByQuery(query, connection);
            for (int i = 0; i < dataTable_AssetResourceRelateBO.Rows.Count; i++)
            {
                try
                {
                    AssetResourceRelateBO item = new AssetResourceRelateBO()
                    {
                        Id = new Guid(dataTable_AssetResourceRelateBO.Rows[i][0].ToString()),
                        AssetId = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][1].ToString()) ? new Guid(dataTable_AssetResourceRelateBO.Rows[i][1].ToString()) : Guid.Empty,
                        DepartmentId = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][2].ToString()) ? new Guid(dataTable_AssetResourceRelateBO.Rows[i][2].ToString()) : Guid.Empty,
                        IsUsing = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][3].ToString()) ? (bool)dataTable_AssetResourceRelateBO.Rows[i][3] : false,
                        RoleId = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][4].ToString()) ? new Guid(dataTable_AssetResourceRelateBO.Rows[i][4].ToString()) : Guid.Empty,
                        IsUse = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][5].ToString()) ? (bool)dataTable_AssetResourceRelateBO.Rows[i][5] : false,
                        IsManage = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][6].ToString()) ? (bool)dataTable_AssetResourceRelateBO.Rows[i][6] : false,
                        IsOwner = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][7].ToString()) ? (bool)dataTable_AssetResourceRelateBO.Rows[i][7] : false,
                        Other = dataTable_AssetResourceRelateBO.Rows[i][8].ToString(),
                        ProcessExpediency = dataTable_AssetResourceRelateBO.Rows[i][9].ToString(),
                        IsDelete = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][10].ToString()) ? (bool)dataTable_AssetResourceRelateBO.Rows[i][10] : false,
                        CreateAt = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][13].ToString()) ? Convert.ToDateTime(dataTable_AssetResourceRelateBO.Rows[i][11].ToString()) : DateTime.MinValue,
                        CreateBy = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][12].ToString()) ? new Guid(dataTable_AssetResourceRelateBO.Rows[i][12].ToString()) : Guid.Empty,
                        UpdateAt = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][13].ToString()) ? Convert.ToDateTime(dataTable_AssetResourceRelateBO.Rows[i][13].ToString()) : DateTime.MinValue,
                        UpdateBy = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][14].ToString()) ? new Guid(dataTable_AssetResourceRelateBO.Rows[i][14].ToString()) : Guid.Empty,
                        EmployeeId = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][15].ToString()) ? new Guid(dataTable_AssetResourceRelateBO.Rows[i][15].ToString()) : Guid.Empty,
                        AssetName = dataTable_AssetResourceRelateBO.Rows[i][16].ToString(),
                        AssetCode = dataTable_AssetResourceRelateBO.Rows[i][17].ToString(),
                        User = !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][18].ToString()) ? dataTable_AssetResourceRelateBO.Rows[i][18].ToString() : !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][19].ToString()) ? dataTable_AssetResourceRelateBO.Rows[i][19].ToString() : !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][20].ToString()) ? dataTable_AssetResourceRelateBO.Rows[i][20].ToString() : dataTable_AssetResourceRelateBO.Rows[i][8].ToString(),
                        Owner = (!string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][7].ToString()) ? (bool)dataTable_AssetResourceRelateBO.Rows[i][7] : false) == true ? !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][18].ToString()) ? dataTable_AssetResourceRelateBO.Rows[i][18].ToString() : !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][19].ToString()) ? dataTable_AssetResourceRelateBO.Rows[i][19].ToString() : !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][20].ToString()) ? dataTable_AssetResourceRelateBO.Rows[i][20].ToString() : dataTable_AssetResourceRelateBO.Rows[i][8].ToString() : GetOwnerByAssetId(connection, !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[i][1].ToString()) ? new Guid(dataTable_AssetResourceRelateBO.Rows[i][1].ToString()) : Guid.Empty)
                    };
                    data.Add(item);
                }
                catch (Exception e)
                { }
            }
            #endregion
            //data = Get(i => i.IsUse == true && i.IsDelete == false).OrderBy(i => i.AssetId).AsEnumerable();
            //var assetIds = data.Select(i => i.AssetId.Value);
            //var assets = ServiceFactory<AssetService>().GetByIds(assetIds).ToList();
            // Use for storage temp setting affter query from database
            //var result = data.AsEnumerable();
            //.Select(i =>
            //{
            //var asset = assets.FirstOrDefault(u => u.Id == i.AssetId.Value);
            //var lastReview = ServiceFactory<AssetReviewService>().GetLastByAssetResource(i.Id);
            //i.AssetName = asset == null ? string.Empty : asset.Name;
            //i.AssetCode = asset == null ? string.Empty : asset.Code;
            // Object Name
            //var objectName = i.DepartmentId.HasValue ? ServiceFactory<DepartmentService>().GetById(i.DepartmentId.Value).Name
            //     : i.EmployeeId.HasValue ? ServiceFactory<EmployeeService>().GetById(i.EmployeeId.Value).Name
            //     : i.RoleId.HasValue ? ServiceFactory<DepartmentTitleService>().GetById(i.RoleId.Value).Name
            //     : i.Other;
            //i.User = objectName;
            //    i.Owner = i.IsOwner == true ? objectName : GetOwnerByAssetId(i.AssetId.Value);
            //    return i;
            //});
            //if (!string.IsNullOrEmpty(filterName))
            //    data = data.Where(i => i.User.Contains(filterName)).ToList();
            count = (int)GetTableByQuery(!string.IsNullOrEmpty(filterName) ? "SELECT count(TB1.Id) FROM AssetResourceRelates as TB1 LEFT JOIN Assets as TB2 ON TB1.AssetId = TB2.Id LEFT JOIN Departments as TB3 ON TB1.DepartmentId = TB3.Id LEFT JOIN Employees as TB4 ON TB1.EmployeeId = TB4.Id LEFT JOIN DepartmentTitles as TB5 ON TB1.DepartmentId = TB5.Id WHERE IsUse = 1 AND TB1.IsDelete = 0 AND (UPPER(TB3.Name) like '%" + filterName.ToUpper() + "%' OR UPPER(TB4.Name) like '%" + filterName.ToUpper() + "%' OR UPPER(TB4.Name) like '%" + filterName.ToUpper() + "%')" : "SELECT count(TB1.Id) FROM AssetResourceRelates as TB1 LEFT JOIN Assets as TB2 ON TB1.AssetId = TB2.Id LEFT JOIN Departments as TB3 ON TB1.DepartmentId = TB3.Id LEFT JOIN Employees as TB4 ON TB1.EmployeeId = TB4.Id LEFT JOIN DepartmentTitles as TB5 ON TB1.DepartmentId = TB5.Id WHERE IsUse = 1 AND TB1.IsDelete = 0", connection).Rows[0][0];
            //data = Page(data, pageIndex, pageSize).ToList();
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetOwnerByAssetId(string connection, Guid assetID)
        {
            DataTable dataTable_AssetResourceRelateBO = GetTableByQuery("SELECT TOP(1) TB3.Name, TB4.Name, TB5.Name FROM AssetResourceRelates as TB1 LEFT JOIN Assets as TB2 ON TB1.AssetId = TB2.Id LEFT JOIN Departments as TB3 ON TB1.DepartmentId = TB3.Id LEFT JOIN Employees as TB4 ON TB1.EmployeeId = TB4.Id LEFT JOIN DepartmentTitles as TB5 ON TB1.DepartmentId = TB5.Id WHERE TB1.AssetId = '" + assetID + "' AND TB1.IsOwner = 1 order by TB1.CreateAt", connection);
            string Owner = dataTable_AssetResourceRelateBO.Rows.Count > 0 ? (!string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[0][0].ToString()) ? dataTable_AssetResourceRelateBO.Rows[0][0].ToString() : !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[0][1].ToString()) ? dataTable_AssetResourceRelateBO.Rows[0][1].ToString() : !string.IsNullOrEmpty(dataTable_AssetResourceRelateBO.Rows[0][2].ToString()) ? dataTable_AssetResourceRelateBO.Rows[0][2].ToString() : "") : "";
            //var data = Get(i => i.AssetId == assetID && i.IsOwner == true)
            //    .Select(
            //    i =>
            //    {
            //        i.ObjectName = i.DepartmentId.HasValue ? ServiceFactory<DepartmentService>().GetById(i.DepartmentId.Value).Name
            //            : i.EmployeeId.HasValue ? ServiceFactory<EmployeeService>().GetById(i.EmployeeId.Value).Name
            //            : i.RoleId.HasValue ? ServiceFactory<DepartmentTitleService>().GetById(i.RoleId.Value).Name
            //            : i.Other;
            //        return i;
            //    }).FirstOrDefault();
            return !string.IsNullOrEmpty(Owner) ? string.Empty : Owner;
        }
        public string GetManagerByAssetId(Guid assetID)
        {
            var data = Get(i => i.AssetId == assetID && i.IsManage == true)
                .Select(
                i =>
                {
                    i.ObjectName = i.DepartmentId.HasValue ? ServiceFactory<DepartmentService>().GetById(i.DepartmentId.Value).Name
                        : i.EmployeeId.HasValue ? ServiceFactory<EmployeeService>().GetById(i.EmployeeId.Value).Name
                        : i.RoleId.HasValue ? ServiceFactory<DepartmentTitleService>().GetById(i.RoleId.Value).Name
                        : i.Other;
                    return i;
                }).FirstOrDefault();
            return data == null ? string.Empty : data.ObjectName;
        }

        public IEnumerable<AssetResourceRelateBO> GetAssetReview(string connection, int pageIndex, int pageSize, out int count, Guid? cateId = null, string stroperator = default(string), decimal? assetValue = null)
        {
            var data = Enumerable.Empty<AssetResourceRelateBO>();
            if (cateId == null || cateId == Guid.Empty)
            {
                data = Get(i => i.IsUse == true)
                    .AsEnumerable();
            }
            else
            {
                data = Get()
                    .Where(c => ServiceFactory<AssetService>()
                    .GetById(c.AssetId.Value).AssetGroupID == cateId)
                    .Where(c => c.IsUse == true);
            }
            switch (stroperator)
            {
                case "=":
                    data = data.Where(t => t.Value == assetValue || assetValue == null);
                    break;
                case ">=":
                    data = data.Where(t => t.Value >= assetValue || assetValue == null);
                    break;
                case "<=":
                    data = data.Where(t => t.Value <= assetValue || assetValue == null);
                    break;
                case ">":
                    data = data.Where(t => t.Value > assetValue || assetValue == null);
                    break;
                case "<":
                    data = data.Where(t => t.Value < assetValue || assetValue == null);
                    break;
                default:
                    break;
            }
            count = data.Count();
            data = Page(data, pageIndex, pageSize).ToList();
            var assetIds = data.Select(i => i.AssetId.Value);
            var assets = ServiceFactory<AssetService>().GetByIds(assetIds).ToList();
            // Use for storage temp setting affter query from database
            var tempSettings = new List<AssetTempSettingBO>();
            data = data.Select(i =>
            {
                var asset = assets.FirstOrDefault(u => u.Id == i.AssetId.Value);
                var lastReview = ServiceFactory<AssetReviewService>().GetLastByAssetResource(i.Id);
                i.AssetName = asset == null ? string.Empty : asset.Name;
                i.AssetCode = asset == null ? string.Empty : asset.Code;
                // Object Name
                var objectName = i.DepartmentId.HasValue ? ServiceFactory<DepartmentService>().GetById(i.DepartmentId.Value).Name
                     : i.EmployeeId.HasValue ? ServiceFactory<EmployeeService>().GetById(i.EmployeeId.Value).Name
                     : i.RoleId.HasValue ? ServiceFactory<DepartmentTitleService>().GetById(i.RoleId.Value).Name
                     : i.Other;

                i.User = objectName;
                // if isOwner is true, use object name to Owner else get Owner by asset ID
                i.Owner = i.IsOwner == true ? objectName : GetOwnerByAssetId(connection, i.AssetId.Value);
                if (lastReview != null)
                {
                    if (lastReview.EmployeeId.HasValue)
                    {
                        var reviewer = ServiceFactory<EmployeeService>().GetById(lastReview.EmployeeId.Value);
                        i.ReviewerAvatarUrl = reviewer.AvatarUrl;
                        i.ReviewerName = reviewer.Name;
                    }
                    else
                    {
                        i.ReviewerName = lastReview.Other;
                    }

                    i.ValueOfInviblate = lastReview.ValueOfInviblate;
                    i.ValueOfReady = lastReview.ValueOfReady;
                    i.ValueOfSecurity = lastReview.ValueOfSecurity;
                    i.Value = lastReview.Value;
                    i.InviblateName = lastReview.InviblateName;
                    i.SecurityName = lastReview.SecurityName;
                    i.ReadyName = lastReview.ReadyName;
                    i.ImportantName = lastReview.ImportantName;
                    // Get Temp by Value of Important
                    // Get temp setting by Value
                    var tempSetting = tempSettings.FirstOrDefault(u => u.From <= i.ValueOfImportant.Value && i.ValueOfImportant.Value <= u.To);
                    if (tempSetting == null)
                    {
                        tempSetting = ServiceFactory<AssetTempSettingService>().GetByImportantValue(i.ValueOfImportant.Value);
                        if (tempSetting != null)
                        {
                            i.Temp = tempSetting.TemplateFile;
                            tempSettings.Add(tempSetting);
                        }
                    }
                    else
                    {
                        i.Temp = tempSetting.TemplateFile;
                    }
                }
                return i;
            });

            return data;
        }
    }
}
