﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class AssetTypePropertyService : BaseService<AssetTypePropertyDTO, AssetTypePropertyBO>, IAssetTypePropertyService
    {

        #region Overload
        public AssetTypePropertyBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true, bool includeAssetProperty = false)
        {
            var data = base.GetById(id);
            if (includeAssetProperty)
            {
                var properties = ServiceFactory<AssetPropertyService>().GetById(data.PropertyId.Value);
                data.PropertyName = properties == null ? string.Empty : properties.Name;
                data.PropertyCode = properties == null ? string.Empty : properties.Code;
            }
            return data;
        }
        public IEnumerable<Guid> InsertRange(Guid typeId, IEnumerable<AssetTypePropertyBO> data, bool allowSave = true)
        {
            // if (CheckConflictProperties(data, typeId))
            if (GetType(data.Select(i => i.PropertyId.Value)) != Guid.Empty)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            return base.InsertRange(data, allowSave);
        }
        public void UpdateRange(Guid typeId, IEnumerable<AssetTypePropertyBO> data, bool allowSave = true, bool isCheckConflict = false)
        {
            // if be conflict then throw excetion
            // if (CheckConflictProperties(data, typeId))
            var getTypeId = GetType(data.Select(i => i.PropertyId.Value));
            if (getTypeId != Guid.Empty && getTypeId != typeId)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            //1. Get data for delete: Exist into database but not exist into data
            var dataPropertyIds = data.Select(i => i.PropertyId.Value);
            var dataDelete = Get(i => i.AssetTypeId == typeId && !dataPropertyIds.Contains(i.PropertyId.Value)).Select(i => i.Id);
            DeleteRange(dataDelete, false);
            // Get data for insert: Exist into data but not exist into database
            var dataInsert = data.Where(i => !Get(u => u.AssetTypeId == typeId).Select(u => u.PropertyId.Value).Contains(i.PropertyId.Value));
            InsertRange(dataInsert, false);
            var propertyIDByAssetType = Get(i => i.AssetTypeId == typeId).Select(i => i.PropertyId.Value).Distinct();
            // Filter Only update 
            var propertyIdUpdate = propertyIDByAssetType.Except(dataInsert.Select(i => i.PropertyId.Value)).ToList();
            // Get for update data
            var dataUpdate = Get(i => propertyIdUpdate.Contains(i.PropertyId.Value) && i.AssetTypeId == typeId && !dataDelete.Contains(i.Id));
            foreach (var item in dataUpdate)
            {
                //var itemUpdate = Get(i => i.PropertyId == item.PropertyId && i.AssetTypeId == typeId).FirstOrDefault();
                // item.Id = itemUpdate.Id;
                var newDataUpdate = data.FirstOrDefault(i => i.PropertyId == item.PropertyId);
                item.Description = newDataUpdate.Description;
                Update(item, false);
            }
            SaveTransaction(allowSave);
        }
        #endregion
        #region Business
        public IEnumerable<Guid> GetPropertyIdsByAssetTypeID(Guid assetTypeId)
        {
            return Get(u => u.AssetTypeId == assetTypeId).Select(i => i.PropertyId.Value);
        }
        public IEnumerable<Guid> GetPropertyIdsByAssetTypeIds(IEnumerable<Guid> assetTypeIds)
        {
            return Get(u => assetTypeIds.Contains(u.AssetTypeId.Value)).Select(i => i.PropertyId.Value);
        }
        public IEnumerable<Guid> GetAssetTypeIdsByPropertyID(Guid propertyID)
        {
            return Get(u => u.PropertyId == propertyID).Select(i => i.AssetTypeId.Value).Distinct();
        }
        public IEnumerable<Guid> GetAssetTypeIdsByPropertyIds(IEnumerable<Guid> propertyIds)
        {
            return Get(u => propertyIds.Contains(u.PropertyId.Value)).Select(i => i.AssetTypeId.Value).Distinct();
        }
        public IEnumerable<Guid> GetSelectPropertyType(IEnumerable<Guid> propertyIds)
        {
            var typeIds = GetTypeByPropertyType(propertyIds);
            var result = GetPropertyIdsByAssetTypeIds(typeIds);
            return result;
        }
        public IEnumerable<Guid> GetTypeByPropertyType(IEnumerable<Guid> propertyIds)
        {
            //1. Get all property type have property id in propertyIds
            var data = Get(i => propertyIds.Contains(i.PropertyId.Value));
            //2. Get distinct type
            var typeIds = data.Select(i => i.AssetTypeId.Value).Distinct();
            var notExpect = new List<Guid>();
            foreach (var typeId in typeIds)
            {
                // Get all properties by type
                var properties = data.Where(i => i.AssetTypeId == typeId).Select(i => i.PropertyId.Value);
                // Check if any property in properties is not into propertyIds
                var checkNotExist = propertyIds.Any(i => !properties.Contains(i));
                if (checkNotExist)
                    // Insert type into not expect 
                    notExpect.Add(typeId);
            }
            typeIds = typeIds.Except(notExpect);
            return typeIds;
        }

        public Guid GetType(IEnumerable<Guid> propertyIds)
        {
            var assetTypeIds = GetAssetTypeIdsByPropertyIds(propertyIds);
            var typeIds = Get(i =>
                assetTypeIds.Contains(i.AssetTypeId.Value)
                && GetPropertyIdsByAssetTypeID(i.AssetTypeId.Value).Count() == propertyIds.Count())
                .Select(i => i.AssetTypeId.Value);
            foreach (var typeId in typeIds)
            {
                //
                if (Get(i => i.AssetTypeId == typeId).Select(i => i.PropertyId.Value).Where(i => propertyIds.Contains(i)).Count() == propertyIds.Count())
                {
                    return typeId;
                }
            }
            return Guid.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="assetTypeId"></param>
        /// <param name="propertyId"></param>
        /// <param name="allowSave"></param>
        public void UpdateSelectProperty(Guid assetTypeId, Guid propertyId, bool allowSave = true)
        {
            var data = new AssetTypePropertyBO()
            {
                PropertyId = propertyId,
                AssetTypeId = assetTypeId
            };
            Insert(data, allowSave);
        }
        /// <summary>
        /// Get properties by asset type Id
        /// </summary>
        /// <param name="assetTypeId"></param>
        /// <returns></returns>
        public IEnumerable<AssetTypePropertyBO> GetByAssetType(Guid assetTypeId)
        {
            ///
            var items = Get(i => i.AssetTypeId == assetTypeId);
            var properties = ServiceFactory<AssetPropertyService>().GetByIds(items.Select(i => i.PropertyId.Value));
            foreach (var item in items)
            {
                var property = properties.FirstOrDefault(i => i.Id == item.PropertyId);
                item.PropertyName = property.Name;
                item.PropertyCode = property.Code;
            }
            return items;
        }
        public void DeleteByAssetType(Guid assetTypeID, bool allowSave = true)
        {
            var dataDelete = Get(i => i.AssetTypeId == assetTypeID).Select(i => i.Id);
            DeleteRange(dataDelete, allowSave);
        }
        public void DeleteByAssetType(IEnumerable<Guid> assetTypeIds, bool allowSave = true)
        {
            var dataDelete = Get(i => assetTypeIds.Contains(i.AssetTypeId.Value)).Select(i => i.Id);
            DeleteRange(dataDelete, allowSave);
        }
        public void InsertOverride(Guid assetTypeID, IEnumerable<Guid> typeConfictIds, IEnumerable<AssetTypePropertyBO> data)
        {
            ServiceFactory<AssetTypeService>().DeleteRange(typeConfictIds, false);
        }
        #endregion

    }
}