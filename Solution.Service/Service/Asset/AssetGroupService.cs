﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Linq;
namespace iDAS.Service
{
    public class AssetGroupService : BaseService<AssetGroupDTO, AssetGroupBO>, IAssetGroupService
    {
        public override Guid Insert(AssetGroupBO data, bool allowSave = true)
        {
            var existName = Get(i => i.Name.Trim().ToUpper() == data.Name.Trim().ToUpper()).Any();
            if (existName)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            return base.Insert(data, allowSave);
        }

        public override void Update(AssetGroupBO data, bool allowSave = true)
        {
            var existName = Get(i => i.Name.Trim().ToUpper() == data.Name.Trim().ToUpper() && i.Id != data.Id).Any();
            if (existName)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            base.Update(data, allowSave);
        }

    }
}
