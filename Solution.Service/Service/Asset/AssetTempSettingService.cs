﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class AssetTempSettingService : BaseService<AssetTempSettingDTO, AssetTempSettingBO>, IAssetTempSettingService
    {
        public override Guid Insert(iDAS.Service.AssetTempSettingBO data, bool allowSave = true)
        {
            var existName = Get(i => i.Name.Trim().ToUpper() == data.Name.Trim().ToUpper()).Any();
            if (existName)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            if (data.TemplateFile.Files != null)
                data.Template = ServiceFactory<FileService>().Upload(data.TemplateFile);
            return base.Insert(data, allowSave);
        }
        public override void Update(AssetTempSettingBO data, bool allowSave = true)
        {
            var existName = Get(i => i.Name.Trim().ToUpper() == data.Name.Trim().ToUpper() && i.Id != data.Id).Any();
            if (existName)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            data.IsUse = data.IsUse ?? false;
            if (data.IsChangeFile)
                data.Template = ServiceFactory<FileService>().Upload(data.TemplateFile);
            base.Update(data, allowSave);
        }
        public override AssetTempSettingBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id);
            data.TemplateFile = new FileUploadBO()
            {
                Files = data.Template.HasValue ? new List<Guid> { data.Template.Value } : new List<Guid>()
            };
            return data;
        }
        public AssetTempSettingBO GetByImportantValue(int value)
        {
            AssetTempSettingBO result = null;
            var reviewSetting = ServiceFactory<AssetReviewSettingService>().GetByValue(value, Common.Resource.QualityReview.IsImportant);
            if (reviewSetting != null)
            {
                result = GetById(reviewSetting.AssetTempSettingId.Value);
                result.From = reviewSetting.From.Value;
                result.To = reviewSetting.To.Value;
            }
            return result;
        }

    }
}
