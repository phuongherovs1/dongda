﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class AssetReviewSettingService : BaseService<AssetReviewSettingDTO, AssetReviewSettingBO>, IAssetReviewSettingService
    {
        #region override

        public override Guid Insert(AssetReviewSettingBO data, bool allowSave = true)
        {
            // do check conflict value
            data.IsSecurity = data.QualityReview == (int)Resource.QualityReview.IsSecurity;
            data.IsImportant = data.QualityReview == (int)Resource.QualityReview.IsImportant;
            data.IsInviblate = data.QualityReview == (int)Resource.QualityReview.IsInviblate;
            data.IsReady = data.QualityReview == (int)Resource.QualityReview.IsReady;
            if (CheckExistName(data.Name, data.QualityReview, Guid.Empty))
            {
                throw new DataHasBeenExistedException();
            }
            if (data.From > data.To)
                throw new FormatException(Resource.ExceptionErrorMessage.LessEqualValue);
            if (!IsConflictValue(data.Id, data.From.Value, data.To.Value, (Resource.QualityReview)data.QualityReview))
            {
                return base.Insert(data, allowSave);
            }
            else
            {
                throw new InvalidOperationException() { };
            }
        }

        public override void Update(AssetReviewSettingBO data, bool allowSave = true)
        {
            // do check conflict value
            try
            {
                data.IsSecurity = data.QualityReview == (int)Resource.QualityReview.IsSecurity;
                data.IsImportant = data.QualityReview == (int)Resource.QualityReview.IsImportant;
                data.IsInviblate = data.QualityReview == (int)Resource.QualityReview.IsInviblate;
                data.IsReady = data.QualityReview == (int)Resource.QualityReview.IsReady;
                if (CheckExistName(data.Name, data.QualityReview, data.Id))
                {
                    throw new DataHasBeenExistedException();
                }
                if (data.From > data.To)
                    throw new FormatException(Resource.ExceptionErrorMessage.LessEqualValue);
                if (!IsConflictValue(data.Id, data.From.Value, data.To.Value, (Resource.QualityReview)data.QualityReview))
                {
                    base.Update(data, allowSave);
                }
                else
                {
                    throw new InvalidOperationException("Khoảng giá trị thiết lập đã tồn tại!");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion
        public IEnumerable<AssetReviewSettingBO> SelectByReview(out int count, int pageIndex, int pageSize, Resource.QualityReview selectReview)
        {

            var data = Get(i =>
                selectReview == Resource.QualityReview.IsImportant ? i.IsImportant == true
                : selectReview == Resource.QualityReview.IsInviblate ? i.IsInviblate == true
                : selectReview == Resource.QualityReview.IsReady ? i.IsReady == true
                : selectReview == Resource.QualityReview.IsSecurity ? i.IsSecurity == true
                : i.Id != Guid.Empty
                ).Select(
                i =>
                {
                    if (i.IsImportant == true)
                    {
                        var limitValue = GetLimitImportantValue();
                        i.IsOutOfValue = i.From < limitValue.ImportantMinValue
                           || i.To > limitValue.ImportantMaxValue;
                    }
                    return i;
                }
                );
            count = data.Count();
            return Page(data.OrderBy(i => i.From), pageIndex, pageSize);
        }

        public bool IsConflictValue(Guid id, int from, int to, Resource.QualityReview selectReview)
        {
            var checkExist = Get(i => i.Id != id, allowDeleted: false).Where(i => selectReview == Resource.QualityReview.IsSecurity ? i.IsSecurity == true
                                : selectReview == Resource.QualityReview.IsReady ? i.IsReady == true
                                : selectReview == Resource.QualityReview.IsInviblate ? i.IsInviblate == true
                                : i.IsImportant == true

                                ).Where(i =>
                                    (i.From.Value <= from && from <= i.To.Value
                                    || i.From.Value <= to && to <= i.To.Value
                                    || from <= i.From.Value && i.To.Value <= to
                                    )
                                );
            return checkExist.Count() > 0;
        }

        public bool AllowReviewImportant()
        {
            var isReady = Get(u => u.IsReady == true).Any();
            var isSecurity = Get(u => u.IsSecurity == true).Any();
            var isInviblate = Get(u => u.IsInviblate == true).Any();
            return isReady || isSecurity || isInviblate;
        }

        public AssetReviewSettingBO GetByValue(int value, Resource.QualityReview selectReview)
        {
            var result = Get(i => selectReview == Resource.QualityReview.IsSecurity ? i.IsSecurity == true
                : selectReview == Resource.QualityReview.IsReady ? i.IsReady == true
                : selectReview == Resource.QualityReview.IsInviblate ? i.IsInviblate == true
                : i.IsImportant == true
                )
                .Where(i => i.From <= value && value <= i.To).FirstOrDefault();
            return result;
        }

        public Dictionary<int, string> GetLogicReview()
        {
            var dict = new Dictionary<int, string>();
            dict.Add((int)Resource.QualityReview.IsSecurity, Resource.QualityReviewText.IsSecurity);
            dict.Add((int)Resource.QualityReview.IsInviblate, Resource.QualityReviewText.IsInviblate);
            dict.Add((int)Resource.QualityReview.IsReady, Resource.QualityReviewText.IsReady);
            if (AllowReviewImportant())
                dict.Add((int)Resource.QualityReview.IsImportant, Resource.QualityReviewText.IsImportant);
            return dict;
        }

        public Resource.ReviewLimitValue GetLimitValue()
        {
            var minSecurity = Get(i => i.IsSecurity == true).Select(i => i.From.Value).DefaultIfEmpty().Min();
            var maxSecurity = Get(i => i.IsSecurity == true).Select(i => i.To.Value).DefaultIfEmpty().Max();
            var minInviblate = Get(i => i.IsInviblate == true).Select(i => i.From.Value).DefaultIfEmpty().Min();
            var maxInviblate = Get(i => i.IsInviblate == true).Select(i => i.To.Value).DefaultIfEmpty().Max();
            var minReady = Get(i => i.IsReady == true).Select(i => i.From.Value).DefaultIfEmpty().Min();
            var maxReady = Get(i => i.IsReady == true).Select(i => i.To.Value).DefaultIfEmpty().Max();
            var minImportant = Get(i => i.IsImportant == true).Select(i => i.From.Value).DefaultIfEmpty().Min();
            var maxImportant = Get(i => i.IsImportant == true).Select(i => i.To.Value).DefaultIfEmpty().Max();
            var result = new Resource.ReviewLimitValue
            {
                SecurityMinValue = minSecurity,
                SecurityMaxValue = maxSecurity,
                InviblateMinValue = minInviblate,
                InviblateMaxValue = maxInviblate,
                ReadyMinValue = minReady,
                ReadyMaxValue = maxReady,
                ImportantMinValue = minImportant,
                ImportantMaxValue = maxImportant
            };
            return result;
        }

        public Resource.ReviewLimitValue GetLimitImportantValue()
        {
            var data = Get();
            var minSecurity = data.Where(i => i.IsSecurity == true).Count() == 0 ? 0 : data.Where(i => i.IsSecurity == true).Min(i => i.From.Value);
            var maxSecurity = data.Where(i => i.IsSecurity == true).Count() == 0 ? 0 : data.Where(i => i.IsSecurity == true).Max(i => i.To.Value);
            var minReady = data.Where(i => i.IsReady == true).Count() == 0 ? 0 : data.Where(i => i.IsReady == true).Min(i => i.From.Value);
            var maxReady = data.Where(i => i.IsReady == true).Count() == 0 ? 0 : data.Where(i => i.IsReady == true).Max(i => i.To.Value);
            var minInviblate = data.Where(i => i.IsInviblate == true).Count() == 0 ? 0 : data.Where(i => i.IsInviblate == true).Min(i => i.From.Value);
            var maxInviblate = data.Where(i => i.IsInviblate == true).Count() == 0 ? 0 : data.Where(i => i.IsInviblate == true).Max(i => i.To.Value);
            var result = new Resource.ReviewLimitValue
            {
                ImportantMinValue = minSecurity + minReady + minInviblate,
                ImportantMaxValue = (Int64)maxSecurity + (Int64)maxReady + (Int64)maxInviblate
            };
            return result;
        }

        private bool CheckExistName(string name, int selectReview, Guid id)
        {
            var result = Get(i => selectReview == (int)Resource.QualityReview.IsSecurity ? i.IsSecurity == true
              : selectReview == (int)Resource.QualityReview.IsReady ? i.IsReady == true
              : selectReview == (int)Resource.QualityReview.IsInviblate ? i.IsInviblate == true
              : i.IsImportant == true
              ).Any(i => i.Name.Trim().ToUpper() == name.Trim().ToUpper() && i.Id != id);
            return result;
        }
    }
}