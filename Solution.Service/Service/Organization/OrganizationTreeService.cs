﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class OrganizationTreeService : IOrganizationTreeService
    {
        private DepartmentService _departmentService;
        private DepartmentService departmentService
        {
            get
            {
                return _departmentService = _departmentService ?? new DepartmentService();
            }
        }

        private DepartmentTitleService _departmentTitleService;
        private DepartmentTitleService departmentTitleService
        {
            get
            {
                return _departmentTitleService = _departmentTitleService ?? new DepartmentTitleService();
            }
        }

        private EmployeeService _employeeService;
        private EmployeeService employeeService
        {
            get
            {
                return _employeeService = _employeeService ?? new EmployeeService();
            }
        }

        /// <summary>
        /// Get node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public IEnumerable<OrganizationTreeBO> GetByNode(string node, bool showTitle = false, bool showEmployee = false, string onlyTitle = default(string))
        {
            var data = new List<OrganizationTreeBO>();
            try
            {
                if (string.IsNullOrEmpty(node) || node == "root")
                {
                    var children = departmentService.GetTreeDepartment(null);
                    foreach (var child in children)
                    {
                        var treeNode = new OrganizationTreeBO()
                        {
                            Id = string.Format("{0}_{1}", Common.Resource.OrganizationTreeType.Department, child.Id),
                            Name = child.Name,
                            AvatarUrl = string.Empty,
                            Type = Common.Resource.OrganizationTreeType.Department,
                            IsLeaf = false
                        };
                        data.Add(treeNode);
                    }
                }
                else
                {
                    var nodeData = node.Split('_');
                    if (nodeData != null && nodeData.Length >= 2)
                    {
                        Guid nodeId = Common.Utilities.ConvertToGuid(nodeData[1]);
                        if (nodeData[0] == "Department")
                        {
                            var children = string.IsNullOrEmpty(node) ? departmentService.GetTreeDepartment(null) : departmentService.GetTreeDepartment(nodeId).ToList();
                            foreach (var child in children)
                            {
                                var treeNode = new OrganizationTreeBO()
                                {
                                    Id = string.Format("{0}_{1}", Common.Resource.OrganizationTreeType.Department, child.Id),
                                    Name = child.Name,
                                    AvatarUrl = string.Empty,
                                    Type = Common.Resource.OrganizationTreeType.Department,
                                    IsLeaf = false
                                };
                                data.Add(treeNode);
                            }
                            if (showTitle)
                            {
                                var filterTitle = new List<Guid>();
                                if (onlyTitle != default(string))
                                {
                                    var filterTitleArr = onlyTitle.Trim(',').Split(',').ToList();
                                    if (filterTitleArr != null && filterTitleArr.Count > 0)
                                        foreach (var item in filterTitleArr)
                                        {
                                            filterTitle.Add(new Guid(item));
                                        }
                                }
                                var titles = departmentTitleService.GetRolesByDepartment(nodeId).Where(i => filterTitle != null && filterTitle.Count > 0 ? filterTitle.Contains(i.Id) : true);
                                foreach (var title in titles)
                                {
                                    var treeNode = new OrganizationTreeBO()
                                    {
                                        Id = string.Format("{0}_{1}", Common.Resource.OrganizationTreeType.Title, title.Id),
                                        Name = title.Name,
                                        AvatarUrl = string.Empty,
                                        Type = Common.Resource.OrganizationTreeType.Title,
                                        IsLeaf = false
                                    };
                                    data.Add(treeNode);
                                }
                            }
                        }
                        if (nodeData[0] == "Title" && showEmployee)
                        {
                            var employees = employeeService.GetByRoleIDs(new[] { nodeId });
                            foreach (var employee in employees)
                            {
                                var treeNode = new OrganizationTreeBO()
                                {
                                    Id = string.Format("{0}_{1}", Common.Resource.OrganizationTreeType.Employee, employee.Id),
                                    Name = employee.Name,
                                    AvatarUrl = employee.AvatarUrl,
                                    Type = Common.Resource.OrganizationTreeType.Employee,
                                    IsLeaf = true
                                };
                                data.Add(treeNode);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            return data;
        }
    }
}