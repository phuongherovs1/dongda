﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class OrganizationSummaryService : BaseService<EmployeeDTO, EmployeeBO>, IOrganizationSummaryService
    {

        public List<PieChartBO> HumanAnalyticByDepartment()
        {
            var summaryData = new List<PieChartBO>();
            var totalEmployees = ServiceFactory<EmployeeTitleService>().GetQuery().Select(c => c.EmployeeId).Distinct().Count();
            var departments = ServiceFactory<DepartmentService>().Get(x => x.IsDelete == false).ToList();
            foreach (var item in departments)
            {
                var titleIds = ServiceFactory<DepartmentTitleService>().GetQuery().Where(c => c.DepartmentId == item.Id).Select(c => c.Id).ToList();
                var value = ServiceFactory<EmployeeTitleService>().GetQuery().Where(c => titleIds.Any(i => i == c.TitleId)).Count();
                summaryData.Add(new PieChartBO()
                {
                    Name = item.Name,
                    Value = value,
                    Rate = Math.Round((decimal)value / totalEmployees * 100)
                });
            }
            return summaryData.ToList();
        }
        public List<PieChartBO> HumanSexAnalytic()
        {
            var summaryData = new List<PieChartBO>();
            var Value_Man = ServiceFactory<EmployeeService>().GetQuery().Where(c => c.Sex == (int)iDAS.Service.Common.Resource.Sex.Man).Count();
            summaryData.Add(new PieChartBO()
            {
                Name = iDAS.Service.Common.Resource.SexText.Man + " : " + Value_Man,
                Value = Value_Man
            });
            var Value_Feman = ServiceFactory<EmployeeService>().GetQuery().Where(c => c.Sex == (int)iDAS.Service.Common.Resource.Sex.Feman).Count();
            summaryData.Add(new PieChartBO()
            {
                Name = iDAS.Service.Common.Resource.SexText.Feman + " : " + Value_Feman,
                Value = Value_Feman
            });
            var Value_Other = ServiceFactory<EmployeeService>().GetQuery().Where(c => c.Sex == (int)iDAS.Service.Common.Resource.Sex.Other).Count();
            summaryData.Add(new PieChartBO()
            {
                Name = iDAS.Service.Common.Resource.SexText.Other + " : " + Value_Other,
                Value = Value_Other
            });
            return summaryData;
        }
    }
}
