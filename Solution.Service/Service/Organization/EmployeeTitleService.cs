﻿using iDAS.ADO;
using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
namespace iDAS.Service
{
    public class EmployeeTitleService : BaseService<EmployeeTitleDTO, EmployeeTitleBO>, IEmployeeTitleService
    {

        public bool CheckEmployeeRoleExits(Guid employeeId, Guid roleId)
        {
            try
            {
                //1. Lấy ra bản ghi có EmployeeID = employeeId và RoleID==roleId
                var record = base.Get(c => c.EmployeeId == employeeId && c.TitleId == roleId).FirstOrDefault();
                //2. Kiểm tra nếu tồn tại trả ra true ngược lại trả ra false
                return record != null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetStringDepartmentRole(Guid empId)
        {
            DataTable dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employee_GetRoleNameByEmpId",
                    parameter: new
                    {
                        empId = empId,
                    }
                );
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                String outstring = dataTable.Rows[0][0].ToString();
                if (outstring.Contains("@"))
                {
                    List<string> lstS = outstring.Split('@').ToList();
                    return String.Join(" <br/> ", lstS);
                }
                else
                    return outstring;
            }
            else
                return "";
        }
        public override Guid Insert(EmployeeTitleBO item, bool allowSave = true)
        {
            try
            {
                //kiểm tra nếu nhân sự chưa có chức danh thì thêm chức danh ngược lại không làm gì
                if (!CheckEmployeeRoleExits(item.EmployeeId, item.TitleId))
                {
                    item.Id = base.Insert(item, allowSave);
                }
                return item.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(Guid Id)
        {
            try
            {
                base.Delete(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IQueryable<EmployeeTitleBO> GetRolesByEmployeeID(Guid employeeId, bool allowGetRoleName = true)
        {
            try
            {
                //1. Lấy ra danh sách chức danh của nhân sự theo employeeId
                var employeeRoles = base.GetQuery().Where(t => t.EmployeeId == employeeId);
                //2. Duyệt danh sách gán giá trị cho trường RoleName
                if (allowGetRoleName)
                {

                    return employeeRoles.AsEnumerable().Select(
                    item =>
                    {
                        item.RoleName = ServiceFactory<DepartmentTitleService>().GetById(item.TitleId).Name;
                        item.DepartmentRoleName = ServiceFactory<DepartmentTitleService>().GetById(item.TitleId).DepartmentName;
                        item.ProfileWorkExperienceId = item.ProfileWorkExperienceId;
                        return item;
                    }).AsQueryable();
                }
                //3. Trả ra danh sách chức danh của nhân sự đã lấy
                return employeeRoles;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Danh sách chức danh theo danh sách employee
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeTitleBO> GetRoleIdsByEmployeeIds(IEnumerable<Guid> employeeIds)
        {
            try
            {
                var data = base.Get(t => employeeIds.Contains(t.EmployeeId));
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<Guid> GetRoleIdsByEmployeeId(Guid employeeId)
        {
            try
            {
                var data = base.GetQuery().Where(t => t.EmployeeId == employeeId)
                               .Select(i => i.TitleId);
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<Guid> GetEmployeeIDsByRoleIDs(IEnumerable<Guid> titleIds)
        {
            //1. Lấy ra danh sách EmployeeID theo roleIds 
            var data = base.GetQuery().Where(u => titleIds.Any(c => c == u.TitleId))
                .Select(t => t.EmployeeId)
                .Distinct();
            return data;
        }

        public string GetRoleNamesByEmployeeIDAndDepartmentID(Guid employeeId, Guid departmentId)
        {
            try
            {
                //1. Lấy danh sách id chức danh theo id nhân sự
                //var roleIds = GetRolesByEmployeeID(employeeId)
                //        .Select(t => t.TitleId)
                //        .ToArray();
                //if (departmentId != Guid.Empty)
                //{
                //    //2. Lấy danh sách chức danh theo danh sách chức danh đã lấy ở bước 1
                //    var rolebydepartment = ServiceFactory<DepartmentTitleService>()
                //            .GetRolesByDepartment(departmentId)
                //            .Where(c => roleIds.Any(r => r == c.Id))
                //            .Select(c => c.Name)
                //            .ToArray();
                //    return String.Join(", ", rolebydepartment);
                //}
                //else
                //{
                //    var roleAll = ServiceFactory<DepartmentTitleService>()
                //            .Get()
                //           .Where(c => roleIds.Any(r => r == c.Id))
                //           .Select(c => c.Name)
                //           .ToArray();
                //    return String.Join(", ", roleAll);
                //}
                string StringOut = "";
                DataTable dataTable =  Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_DepartmentTitle_GetTitleInfo", parameter: new { employeeId = employeeId, departmentId = departmentId });
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    List<string> lstName = new List<string>();
                    foreach (DataRow item in dataTable.Rows)
                        lstName.Add(item["Name"].ToString());
                    StringOut = String.Join(", ", lstName);
                }
                return StringOut;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<EmployeeTitleBO> GetByTitleId(Guid titleId)
        {
            // var employeeByRole = employeeRoleSV.GetEmployeeIDbyRole(roleId).ToList();
            var data = Get(u => u.TitleId == titleId)
                .AsEnumerable()
                .Select(u =>
                {
                    var employee = ServiceFactory<EmployeeService>().GetById(u.EmployeeId);
                    return u;
                });
            return data;
        }
        public bool CheckEmployeeIDExits(Guid employeeId)
        {
            //Kiểm tra xem id nhân sự đã tồn tại trong bảng EmployeeRole hay chưa nếu tồn tại trả ra :true ngược lại trả ra: false
            return Get().Any(i => i.EmployeeId == employeeId);
        }
        /// <summary>
        /// Đưa ra danh sách chức danh của người đang đăng nhập hệ thống
        /// </summary>
        /// <returns></returns>
        public IQueryable<Guid> GetTitleIdsByCurrentUser()
        {
            var data = GetQuery().Where(u => u.EmployeeId == UserId)
                .Select(i => i.TitleId).Distinct();
            return data;
        }

    }
}