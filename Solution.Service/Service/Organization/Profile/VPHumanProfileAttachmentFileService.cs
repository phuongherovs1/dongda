﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class VPHumanProfileAttachmentFileService : BaseService<VPHumanProfileAttachmentFileDTO, VPHumanProfileAttachmentFileBO>, IVPHumanProfileAttachmentFileService
    {
        public void DeleteRange(Guid PHumanProfileId, IEnumerable<Guid> fileIds)
        {
            var ids = Get(i => i.HumanProfileAttachmentId == PHumanProfileId && fileIds.Contains(i.AttachmentFileId.Value)).Select(i => i.Id);
            base.DeleteRange(ids);
        }

        public IEnumerable<Guid> GetByFile(Guid Id)
        {
            var data = Get(i => i.HumanProfileAttachmentId.Value == Id)
                .Select(
                i => i.AttachmentFileId.Value
                );
            return data;
        }
        public IEnumerable<Guid> GetFileByProfileId(Guid Id)
        {
            try
            {
                var data = Get(i => i.HumanProfileAttachmentId == Id).Where(x => x.AttachmentFileId.HasValue)
                    .Select(
                    i => i.AttachmentFileId.Value
                    );
                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
