﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanProfileWorkTrialResultService : BaseService<VPHumanProfileWorkTrialResultDTO, VPHumanProfileWorkTrialResultBO>, IVPHumanProfileWorkTrialResultService
    {
        /// <summary>
        /// Khởi tạo giá trị mặc định
        /// </summary>
        /// <param name="humanProfileWorkTrialId"></param>
        /// <returns></returns>
        public VPHumanProfileWorkTrialResultBO CreateDefault(Guid humanProfileWorkTrialId)
        {
            try
            {
                var data = base.GetQuery().Where(p => p.HumanProfileWorkTrialId == humanProfileWorkTrialId && p.IsDelete != true).FirstOrDefault();
                var humanProfileWorkTrial = ServiceFactory<VPHumanProfileWorkTrialService>().GetById(humanProfileWorkTrialId);
                if (data == null)
                {
                    data = new VPHumanProfileWorkTrialResultBO()
                    {
                        Id = Guid.Empty,
                        HumanProfileWorkTrialId = humanProfileWorkTrialId,
                        EmployeeTrialName = humanProfileWorkTrial.HumanEmployeeId.HasValue ? ServiceFactory<EmployeeService>().GetById(humanProfileWorkTrial.HumanEmployeeId.Value).Name : string.Empty
                    };
                }
                else
                {
                    data.EmployeeTrialName = humanProfileWorkTrial.HumanEmployeeId.HasValue ? ServiceFactory<EmployeeService>().GetById(humanProfileWorkTrial.HumanEmployeeId.Value).Name : string.Empty;
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Thêm mới/sửa yêu cầu tự đánh giá của nhân viên thử việc
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid Create(VPHumanProfileWorkTrialResultBO data)
        {
            try
            {
                var humanProfileWorkTrial = ServiceFactory<VPHumanProfileWorkTrialService>().GetById(data.HumanProfileWorkTrialId.Value);
                if (humanProfileWorkTrial != null)
                {
                    humanProfileWorkTrial.Status = true;
                    ServiceFactory<VPHumanProfileWorkTrialService>().Update(humanProfileWorkTrial, true);
                }
                if (data.Id == null || data.Id == Guid.Empty)
                {
                    data.Id = base.Insert(data, true);
                }
                else
                    base.Update(data, true);
                return data.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Danh sách kết quả tự đánh giá
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="humanProfileWorkTrialId"></param>
        /// <returns></returns>
        public IEnumerable<VPHumanProfileWorkTrialResultBO> GetByHumanProfileWorkTrialId(int pageIndex, int pageSize, out int count, Guid humanProfileWorkTrialId)
        {
            try
            {
                var data = base.GetQuery().Where(p => p.HumanProfileWorkTrialId == humanProfileWorkTrialId && p.IsDelete != true);
                count = data.Count();
                var result = Page(data.OrderBy(p => p.CreateAt), pageIndex, pageSize);
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update danh sách tự đánh giá
        /// </summary>
        /// <param name="jsonData"></param>
        public void UpdateProfileWorkTrial(string jsonData, Guid humanProfileWorkTrialId)
        {
            try
            {
                if (!string.IsNullOrEmpty(jsonData))
                {
                    var profileWorkTrial = ServiceFactory<VPHumanProfileWorkTrialService>().GetQuery().Where(p => p.IsDelete != true);
                    var lstPointItem = JsonConvert.DeserializeObject<List<VPHumanProfileWorkTrialResultBO>>(jsonData);
                    if (lstPointItem != null && lstPointItem.Count > 0)
                    {
                        foreach (var item in lstPointItem)
                        {
                            var data = new VPHumanProfileWorkTrialResultBO()
                            {
                                Id = item.Id,
                                HumanProfileWorkTrialId = humanProfileWorkTrialId,
                                CriteriaName = item.CriteriaName,
                                EmployeePoint = item.EmployeePoint,
                                ManagerPoint = item.ManagerPoint,
                                Note = item.Note
                            };
                            if (data.Id == null || data.Id == Guid.Empty)
                                base.Insert(data, true);
                            else
                                base.Update(data, true);
                            var profile = profileWorkTrial.FirstOrDefault(p => p.Id == humanProfileWorkTrialId);
                            if (profile != null)
                            {
                                profile.IsEdit = true;
                                ServiceFactory<VPHumanProfileWorkTrialService>().Update(profile, true);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}