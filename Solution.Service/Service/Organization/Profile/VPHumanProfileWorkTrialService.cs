﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class VPHumanProfileWorkTrialService : BaseService<VPHumanProfileWorkTrialDTO, VPHumanProfileWorkTrialBO>, IVPHumanProfileWorkTrialService
    {
        /// <summary>
        /// Nhân sự tự đánh giá
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<VPHumanProfileWorkTrialBO> GetByEmployee(int pageIndex, int pageSize, out int count)
        {
            // Danh sách kết quả đánh giá
            var profileResults = ServiceFactory<VPHumanProfileWorkTrialResultService>().GetQuery();
            var data = base.GetQuery().Where(i => i.HumanEmployeeId == UserId || i.CreateBy == UserId || i.ManagerId == UserId);
            count = data.Count();
            data = Page(data.OrderBy(p => p.CreateAt), pageIndex, pageSize);
            var employeeIds = data.Where(i => i.HumanEmployeeId.HasValue).Select(i => i.HumanEmployeeId.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();

            var titleIds = data.Where(i => i.HumanRoleId.HasValue).Select(i => i.HumanRoleId.Value).Distinct();
            var titles = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds);

            var result = data.AsEnumerable().Select(i =>
            {
                var employee = employees.FirstOrDefault(p => p.Id == i.HumanEmployeeId.Value);
                i.EmployeeTrialName = employee == null ? string.Empty : employee.Name;
                var title = titles.FirstOrDefault(p => p.Id == i.HumanRoleId.Value);
                i.TitleName = title == null ? string.Empty : title.Name;
                var profileResult = profileResults.Where(p => p.HumanProfileWorkTrialId == i.Id);
                if (profileResult != null && profileResult.Count() > 0)
                {
                    foreach (var item in profileResult)
                    {
                        if (item.ManagerPoint.HasValue)
                            i.AuditStatus = false;
                        else
                            i.AuditStatus = true;
                    }
                }
                return i;
            });

            return result;
        }

        /// <summary>
        /// Danh sách nhân sự thử việc
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<VPHumanProfileWorkTrialBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            try
            {
                var data = base.GetQuery().Where(p => p.IsDelete != true);
                count = data.Count();
                data = Page(data.OrderBy(p => p.CreateAt), pageIndex, pageSize);
                var employeeIds = data.Where(i => i.HumanEmployeeId.HasValue).Select(i => i.HumanEmployeeId.Value).Distinct();
                var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();

                var titleIds = data.Where(i => i.HumanRoleId.HasValue).Select(i => i.HumanRoleId.Value).Distinct();
                var titles = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds);

                var result = data.AsEnumerable().Select(i =>
                {
                    var employee = employees.FirstOrDefault(p => p.Id == i.HumanEmployeeId.Value);
                    i.EmployeeTrialName = employee == null ? string.Empty : employee.Name;
                    var title = titles.FirstOrDefault(p => p.Id == i.HumanRoleId.Value);
                    i.TitleName = title == null ? string.Empty : title.Name;
                    if (title != null)
                    {
                        var department = title.DepartmentId.HasValue ? ServiceFactory<DepartmentService>().GetQuery().FirstOrDefault(p => p.Id == title.DepartmentId.Value) : null;
                        i.DepartmentName = department != null ? department.Name : "Không có phòng ban";
                    }
                    var maneger = i.ManagerId.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.ManagerId.Value) : null;
                    i.EmployeeManegeName = maneger == null ? string.Empty : maneger.Name;
                    return i;
                });
                return result;
            }
            catch (Exception e) { throw; }
        }

        /// <summary>
        /// Thêm mới/sửa nhân sự
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid Create(VPHumanProfileWorkTrialBO data, string manegeId, string roleId)
        {
            try
            {
                data.HumanEmployeeId = data.EmployeeId.Id;
                data.Status = false;
                if (!string.IsNullOrEmpty(manegeId))
                {
                    if (manegeId.Contains('_'))
                    {
                        var maneger = manegeId.Split('_').ToList();
                        if (maneger != null && maneger.Count > 0)
                            data.ManagerId = new Guid(maneger[1]);
                    }
                    else
                        data.ManagerId = new Guid(manegeId);
                }
                if (!string.IsNullOrEmpty(roleId))
                {
                    if (roleId.Contains('_'))
                    {
                        var role = roleId.Split('_').ToList();
                        if (role != null && role.Count > 0)
                            data.HumanRoleId = new Guid(role[1]);
                    }
                    else
                        data.HumanRoleId = new Guid(roleId);
                }
                var employeeExist = base.GetQuery().Where(p => p.HumanEmployeeId == data.HumanEmployeeId && p.HumanRoleId == data.HumanRoleId && p.Id != data.Id);
                if (employeeExist != null && employeeExist.Count() > 0)
                    throw new DataHasBeenExistedException();
                if (data.Id == null || data.Id == Guid.Empty)
                {
                    data.Id = base.Insert(data, true);
                }
                else
                    base.Update(data, true);
                SaveTransaction(true);
                return data.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Thông tin chi tiết nhân sự thử việc
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public VPHumanProfileWorkTrialBO GetDetail(string id)
        {
            try
            {
                var data = new VPHumanProfileWorkTrialBO();
                if (!string.IsNullOrEmpty(id))
                {
                    data = base.GetById(new Guid(id));
                    var employee = data.HumanEmployeeId.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.HumanEmployeeId.Value) : null;
                    data.EmployeeId = employee == null ? new EmployeeBO() : employee;
                    data.EmployeeTrialName = employee == null ? string.Empty : employee.Name;
                    var maneger = data.ManagerId.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.ManagerId.Value) : null;
                    data.EmployeeManege = maneger == null ? new EmployeeBO() : maneger;
                    data.EmployeeManegeId = "Employee_" + data.ManagerId;
                    data.EmployeeManegeName = maneger == null ? string.Empty : maneger.Name;
                    var employeeApproval = data.ApprovalBy.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.ApprovalBy.Value) : null;
                    data.EmployeeApprover = employeeApproval == null ? new EmployeeBO() : employeeApproval;
                    var role = data.HumanRoleId.HasValue ? ServiceFactory<DepartmentTitleService>().GetById(data.HumanRoleId.Value) : null;
                    data.RoleId = role == null ? string.Empty : "Title_" + role.Id;
                    data.RoleName = role == null ? string.Empty : role.Name;
                }
                else
                {
                    data = new VPHumanProfileWorkTrialBO()
                    {
                        Id = Guid.Empty
                    };
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Danh sách nhân sự thử việc theo người quản lý
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<VPHumanProfileWorkTrialBO> GetByManege(int pageIndex, int pageSize, out int count)
        {
            var data = base.GetQuery().Where(i => i.ManagerId == UserId);
            count = data.Count();
            data = Page(data.OrderBy(p => p.CreateAt), pageIndex, pageSize);
            var employeeIds = data.Where(i => i.HumanEmployeeId.HasValue).Select(i => i.HumanEmployeeId.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();

            var titleIds = data.Where(i => i.HumanRoleId.HasValue).Select(i => i.HumanRoleId.Value).Distinct();
            var titles = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds);

            var result = data.AsEnumerable().Select(i =>
            {
                var employee = employees.FirstOrDefault(p => p.Id == i.HumanEmployeeId.Value);
                i.EmployeeTrialName = employee == null ? string.Empty : employee.Name;
                var title = titles.FirstOrDefault(p => p.Id == i.HumanRoleId.Value);
                i.TitleName = title == null ? string.Empty : title.Name;
                return i;
            });

            return result;
        }

        /// <summary>
        /// Danh sách nhân sự thử việc chờ duyệt
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<VPHumanProfileWorkTrialBO> GetDataApproval(int pageIndex, int pageSize, out int count)
        {
            try
            {
                var data = base.GetQuery().Where(p => p.IsDelete != true && p.ApprovalBy == UserId);
                count = data.Count();
                data = Page(data.OrderBy(p => p.CreateAt), pageIndex, pageSize);
                var employeeIds = data.Where(i => i.HumanEmployeeId.HasValue).Select(i => i.HumanEmployeeId.Value).Distinct();
                var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();

                var titleIds = data.Where(i => i.HumanRoleId.HasValue).Select(i => i.HumanRoleId.Value).Distinct();
                var titles = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds);

                var result = data.AsEnumerable().Select(i =>
                {
                    var employee = employees.FirstOrDefault(p => p.Id == i.HumanEmployeeId.Value);
                    i.EmployeeTrialName = employee == null ? string.Empty : employee.Name;
                    if (titles != null && titles.ToList().Count > 0)
                    {
                        var title = titles.FirstOrDefault(p => p.Id == i.HumanRoleId.Value);
                        i.TitleName = title == null ? string.Empty : title.Name;
                        i.DepartmentName = title.DepartmentId.HasValue ? ServiceFactory<DepartmentService>().GetById(title.DepartmentId.Value).Name : string.Empty;
                    }
                    return i;
                });

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Phê duyệt nhân sự thử việc
        /// </summary>
        /// <param name="data"></param>
        public void Approval(VPHumanProfileWorkTrialBO data)
        {
            try
            {
                data.IsApproval = true;
                if (data.IsAccept == true)
                    data.IsEdit = false;
                else
                    data.IsEdit = true;
                base.Update(data);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public IEnumerable<VPHumanProfileWorkTrialBO> GetByEmployee(int pageIndex, int pageSize, out int count, Guid employeeId)
        {
            var data = Get(i => i.HumanEmployeeId == employeeId);
            count = data.Count();
            return data;
        }

        public VPHumanProfileWorkTrialBO GetNewEmployeeTrialForm(string id, Guid profileId)
        {
            VPHumanProfileWorkTrialBO data;
            if (string.IsNullOrEmpty(id))
            {
                data = new VPHumanProfileWorkTrialBO()
                {
                    ProfileID = profileId
                };
            }
            else
            {
                data = GetById(new Guid(id));
            }
            return data;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public VPHumanProfileWorkTrialBO GetNewEmployeeTrialForm(Guid profileId, Guid recruitmentResultId)
        {
            var profile = ServiceFactory<VPHumanRecruitmentProfileService>().GetById(profileId);

            var data = new VPHumanProfileWorkTrialBO()
            {
                ProfileID = profileId,
                ProfileName = profile.Name,
                RecruitmentResultId = recruitmentResultId
            };
            if (profile.EmployeeId.HasValue)
            {
                var user = ServiceFactory<UserService>().GetById(profile.EmployeeId.Value);
                data.User = user;
            }
            return data;
        }

        public void CreateEmployeeTrial(VPHumanProfileWorkTrialBO profileWorkTrial)
        {
            var recruitmentProfileResult = ServiceFactory<VPHumanRecruitmentProfileResultService>().GetById(profileWorkTrial.RecruitmentResultId.Value);
            recruitmentProfileResult.IsTrial = true;
            ServiceFactory<VPHumanRecruitmentProfileResultService>().Update(recruitmentProfileResult, true);
            profileWorkTrial.HumanEmployeeId = profileWorkTrial.User.Id;
            Insert(profileWorkTrial, true);
            var profile = ServiceFactory<VPHumanRecruitmentProfileService>().GetById(profileWorkTrial.ProfileID.Value);
            profile.EmployeeId = profileWorkTrial.User.Id;
            ServiceFactory<VPHumanRecruitmentProfileService>().Update(profile, true);
        }

        public void CreateEmployee(VPHumanProfileWorkTrialBO profileWorkTrial)
        {
            var recruitmentProfileResult = ServiceFactory<VPHumanRecruitmentProfileResultService>().GetById(profileWorkTrial.RecruitmentResultId.Value);
            recruitmentProfileResult.IsEmployee = true;
            ServiceFactory<VPHumanRecruitmentProfileResultService>().Update(recruitmentProfileResult, true);
            var profile = ServiceFactory<VPHumanRecruitmentProfileService>().GetById(profileWorkTrial.ProfileID.Value);
            profile.EmployeeId = profileWorkTrial.User.Id;
            ServiceFactory<VPHumanRecruitmentProfileService>().Update(profile, true);
        }


        public IEnumerable<VPHumanProfileWorkTrialBO> GetByEmployeeNotPaging(Guid employeeId)
        {
            var data = Get(i => i.HumanEmployeeId == employeeId);
            return data;
        }
    }
}