﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanProfileAssessService : BaseService<VPHumanProfileAssessDTO, VPHumanProfileAssessBO>, IVPHumanProfileAssessService
    {

        public IEnumerable<VPHumanProfileAssessBO> GetAllByEmployeeId(Guid employeeId, int pageIndex, int pageSize, out int count)
        {
            var data = Get(i => i.HumanEmployeeId == employeeId);
            count = data.Count();
            return Page(data, pageIndex, pageSize);
        }

        public IEnumerable<VPHumanProfileAssessBO> GetAll()
        {
            var data = base.GetAll();
            return data;
        }

        public IEnumerable<VPHumanProfileAssessBO> GetAllByEmployeeIdNotPaging(Guid employeeId)
        {
            var data = Get(i => i.HumanEmployeeId == employeeId);
            return data;
        }
    }
}