﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanProfileTrainingService : BaseService<VPHumanProfileTrainingDTO, VPHumanProfileTrainingBO>, IVPHumanProfileTrainingService
    {

        public IEnumerable<VPHumanProfileTrainingBO> GetAll()
        {
            var data = base.GetAll();
            foreach (VPHumanProfileTrainingBO item in data)
            {
                var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(item.Id);
                item.FileAttachs = new FileUploadBO()
                {
                    Files = files.ToList()
                };
            }
            return data;
        }
        public IEnumerable<VPHumanProfileTrainingBO> GetAllByEmployeeId(Guid employeeId, int pageIndex, int pageSize, out int count)
        {
            var data = Get(i => i.HumanEmployeeId == employeeId);
            foreach (VPHumanProfileTrainingBO item in data)
            {
                var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(item.Id);
                item.FileAttachs = new FileUploadBO()
                {
                    Files = files.ToList()
                };
            }
            count = data.Count();
            return Page(data, pageIndex, pageSize);
        }

        public VPHumanProfileTrainingBO GetdataById(Guid Id)
        {
            var data = base.GetById(Id);
            var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(data.Id);
            data.FileAttachs = new FileUploadBO()
            {
                Files = files.ToList()
            };
            return data;
        }

        public void HumanProfileTraining(VPHumanProfileTrainingBO data, bool allowSave = true)
        {
            try
            {
                var fileIds = Enumerable.Empty<Guid>();
                if (data != null && data.FileAttachs != null && data.FileAttachs.FileAttachments.Any())
                {
                    if (data != null && data.FileAttachs != null)
                    {
                        fileIds = ServiceFactory<FileService>().UploadRange(data.FileAttachs, true);
                    }
                }

                if (data.Id == null || data.Id == Guid.Empty)
                {
                    data.Id = base.Insert(data, true);
                }
                else
                {
                    base.Update(data, true);
                    if (data.FileAttachs != null && data.FileAttachs.FileRemoves != null && data.FileAttachs.FileRemoves.Count > 0)
                        ServiceFactory<VPHumanProfileAttachmentFileService>().DeleteRange(data.Id, data.FileAttachs.FileRemoves);
                }

                // Insert đính kèm tương ứng với bảng kết quả
                foreach (var fileId in fileIds)
                {
                    var attachment = new VPHumanProfileAttachmentFileBO()
                    {
                        AttachmentFileId = fileId,
                        HumanProfileAttachmentId = data.Id
                    };
                    ServiceFactory<VPHumanProfileAttachmentFileService>().Insert(attachment, true);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        public IEnumerable<VPHumanProfileTrainingBO> GetAllByEmployeeIdNotPaging(Guid employeeId)
        {
            var data = Get(i => i.HumanEmployeeId == employeeId);
            foreach (VPHumanProfileTrainingBO item in data)
            {
                var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(item.Id);
                item.FileAttachs = new FileUploadBO()
                {
                    Files = files.ToList()
                };
            }
            return data;
        }
    }
}