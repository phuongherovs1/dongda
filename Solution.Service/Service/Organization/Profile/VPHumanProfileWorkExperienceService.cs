﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanProfileWorkExperienceService : BaseService<VPHumanProfileWorkExperienceDTO, VPHumanProfileWorkExperienceBO>, IVPHumanProfileWorkExperienceService
    {
        public IEnumerable<VPHumanProfileWorkExperienceBO> GetAll(int p1, int p2, out int totalCount)
        {
            var data = base.GetAll();
            foreach (VPHumanProfileWorkExperienceBO item in data)
            {
                var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(item.Id);
                item.FileAttachs = new FileUploadBO()
                {
                    Files = files.ToList()
                };
            }
            totalCount = data.Count();
            return Page(data, p1, p2);
        }
        public VPHumanProfileWorkExperienceBO GetFormItem(string id, Guid employeeId)
        {
            VPHumanProfileWorkExperienceBO data;
            if (string.IsNullOrEmpty(id))
            {
                data = new VPHumanProfileWorkExperienceBO()
                {
                    HumanEmployeeId = employeeId
                };
            }
            else
            {
                data = base.GetById(new Guid(id));
                var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(data.Id);
                data.FileAttachs = new FileUploadBO()
                {
                    Files = files.ToList()
                };
            }
            return data;
        }

        public VPHumanProfileWorkExperienceBO GetdataById(Guid Id)
        {
            var data = base.GetById(Id);
            var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(data.Id);
            data.FileAttachs = new FileUploadBO()
            {
                Files = files.ToList()
            };
            return data;
        }

        public void HumanProfileExperience(VPHumanProfileWorkExperienceBO data, bool allowSave = true)
        {
            try
            {
                var fileIds = Enumerable.Empty<Guid>();
                if (data != null && data.FileAttachs != null && data.FileAttachs.FileAttachments.Any())
                {
                    if (data != null && data.FileAttachs != null)
                    {
                        fileIds = ServiceFactory<FileService>().UploadRange(data.FileAttachs, false);
                    }
                }

                if (data.Id == null || data.Id == Guid.Empty)
                {
                    data.Id = base.Insert(data, true);
                }
                else
                {
                    base.Update(data, true);
                    if (data.FileAttachs != null && data.FileAttachs.FileRemoves != null && data.FileAttachs.FileRemoves.Count > 0)
                        ServiceFactory<VPHumanProfileAttachmentFileService>().DeleteRange(data.Id, data.FileAttachs.FileRemoves);
                }

                // Insert đính kèm tương ứng với bảng kết quả
                foreach (var fileId in fileIds)
                {
                    var attachment = new VPHumanProfileAttachmentFileBO()
                    {
                        AttachmentFileId = fileId,
                        HumanProfileAttachmentId = data.Id
                    };
                    ServiceFactory<VPHumanProfileAttachmentFileService>().Insert(attachment, true);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        
        public IEnumerable<VPHumanProfileWorkExperienceBO> GetByEmployeeId(Guid employeeId, int pageIndex, int pageSize, out int count)
        {
            var data = Get(i => i.HumanEmployeeId == employeeId);
            foreach (VPHumanProfileWorkExperienceBO item in data)
            {
                var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(item.Id);
                item.FileAttachs = new FileUploadBO()
                {
                    Files = files.ToList()
                };
            }
            count = data.Count();
            data = base.Page(data, pageIndex, pageSize);
            return data;
        }


        public IEnumerable<VPHumanProfileWorkExperienceBO> GetByEmployeeIdNotPaging(Guid employeeId)
        {
            var data = Get(i => i.HumanEmployeeId == employeeId);
            foreach (VPHumanProfileWorkExperienceBO item in data)
            {
                var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(item.Id);
                item.FileAttachs = new FileUploadBO()
                {
                    Files = files.ToList()
                };
            }
            return data;
        }
    }
}