﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanProfileContractService : BaseService<VPHumanProfileContractDTO, VPHumanProfileContractBO>, IVPHumanProfileContractService
    {
        public IEnumerable<VPHumanProfileContractBO> GetAllByEmployeeId(int p1, int p2, out int totalCount, Guid EmployeeID)
        {
            var data = Get(i => i.HumanEmployeeId == EmployeeID);
            foreach (VPHumanProfileContractBO item in data)
            {
                var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(item.Id);
                item.FileAttachs = new FileUploadBO()
                {
                    Files = files.ToList()
                };
            }
            totalCount = data.Count();
            return Page(data, p1, p2);
        }
        public VPHumanProfileContractBO GetdataById(Guid Id)
        {
            var data = base.GetById(Id);
            var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(data.Id);
            data.FileAttachs = new FileUploadBO()
            {
                Files = files.ToList()
            };
            return data;
        }

        public IEnumerable<VPHumanProfileContractBO> GetAll()
        {
            var data = base.GetAll();
            foreach (VPHumanProfileContractBO item in data)
            {
                var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(item.Id);
                item.FileAttachs = new FileUploadBO()
                {
                    Files = files.ToList()
                };
            }
            return data;
        }

        public void HumanProfileContract(VPHumanProfileContractBO data, bool allowSave = true)
        {
            try
            {
                var fileIds = Enumerable.Empty<Guid>();
                if (data != null && data.FileAttachs != null && data.FileAttachs.FileAttachments.Any())
                {
                    if (data != null && data.FileAttachs != null)
                    {
                        fileIds = ServiceFactory<FileService>().UploadRange(data.FileAttachs, allowSave);
                    }
                }

                if (data.Id == null || data.Id == Guid.Empty)
                {
                    data.Id = base.Insert(data, allowSave);
                }
                else
                {
                    base.Update(data, allowSave);
                    if (data.FileAttachs != null && data.FileAttachs.FileRemoves != null && data.FileAttachs.FileRemoves.Count > 0)
                        ServiceFactory<VPHumanProfileAttachmentFileService>().DeleteRange(data.Id, data.FileAttachs.FileRemoves);
                }

                // Insert đính kèm tương ứng với bảng kết quả
                foreach (var fileId in fileIds)
                {
                    var attachment = new VPHumanProfileAttachmentFileBO()
                    {
                        AttachmentFileId = fileId,
                        HumanProfileAttachmentId = data.Id
                    };
                    ServiceFactory<VPHumanProfileAttachmentFileService>().Insert(attachment, allowSave);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        public IEnumerable<VPHumanProfileContractBO> GetAllByEmployeeIdNotPaging(Guid EmployeeID)
        {
            var data = Get(i => i.HumanEmployeeId == EmployeeID);
            foreach (VPHumanProfileContractBO item in data)
            {
                var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(item.Id);
                item.FileAttachs = new FileUploadBO()
                {
                    Files = files.ToList()
                };
            }
            return data;
        }
    }
}