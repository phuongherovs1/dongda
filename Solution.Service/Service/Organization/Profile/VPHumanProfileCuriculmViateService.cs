﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
namespace iDAS.Service
{
    public class VPHumanProfileCuriculmViateService : BaseService<VPHumanProfileCuriculmViateDTO, VPHumanProfileCuriculmViateBO>, IVPHumanProfileCuriculmViateService
    {
        public IEnumerable<VPHumanProfileCuriculmViateBO> GetAll()
        {
            var data = base.GetAll();
            foreach (VPHumanProfileCuriculmViateBO item in data)
            {
                var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(item.Id);
                item.FileAttachs = new FileUploadBO()
                {
                    Files = files.ToList()
                };
            }
            return data;
        }
        public VPHumanProfileCuriculmViateBO GetdataById(Guid Id)
        {
            var data = base.GetById(Id);
            var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(data.Id);
            data.FileAttachs = new FileUploadBO()
            {
                Files = files.ToList()
            };
            return data;
        }
        public void InsertHumanProfileCuriculmViate(VPHumanProfileCuriculmViateBO data, bool allowSave = true)
        {
            try
            {
                var fileIds = Enumerable.Empty<Guid>();
                if (data != null && data.FileAttachs != null && data.FileAttachs.FileRemoves.Any())
                {
                    foreach (Guid guiditem in data.FileAttachs.FileRemoves)
                    {
                        FileBO result = ServiceFactory<FileService>().GetById(guiditem);
                        File.Delete(HttpContext.Current.Server.MapPath("/Upload/" + guiditem.ToString() + "." + result.Extension));
                        ServiceFactory<FileService>().Delete(guiditem);
                    }
                }
                if (data != null && data.FileAttachs != null && data.FileAttachs.FileAttachments.Any())
                {
                    if (data != null && data.FileAttachs != null)
                    {
                        fileIds = ServiceFactory<FileService>().UploadRange(data.FileAttachs, allowSave);
                    }
                }

                if (data.Id == null || data.Id == Guid.Empty)
                {
                    data.Id = base.Insert(data, allowSave);
                }
                else
                {
                    base.Update(data, allowSave);
                    if (data.FileAttachs != null && data.FileAttachs.FileRemoves != null && data.FileAttachs.FileRemoves.Count > 0)
                        ServiceFactory<VPHumanProfileAttachmentFileService>().DeleteRange(data.Id, data.FileAttachs.FileRemoves);
                }

                // Insert đính kèm tương ứng với bảng kết quả
                foreach (var fileId in fileIds)
                {
                    var attachment = new VPHumanProfileAttachmentFileBO()
                    {
                        AttachmentFileId = fileId,
                        HumanProfileAttachmentId = data.Id
                    };
                    ServiceFactory<VPHumanProfileAttachmentFileService>().Insert(attachment, allowSave);
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public VPHumanProfileCuriculmViateBO GetByEmployeeId(Guid EmployeeId)
        {
            var data = base.GetQuery().Where(i => i.HumanEmployeeId == EmployeeId).FirstOrDefault();
            if (data != null)
            {
                var files = ServiceFactory<VPHumanProfileAttachmentFileService>().GetByFile(data.Id);
                data.FileAttachs = new FileUploadBO()
                {
                    Files = files.ToList()
                };
            }
            return data;
        }

    }
}