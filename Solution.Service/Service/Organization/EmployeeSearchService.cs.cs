﻿using iDAS.ADO;
using iDAS.Service.API.Organization;
using System.Data;

namespace iDAS.Service.Service.Organization
{
    public class EmployeeSearchService : IEmployeeSearchService
    {
        public DataTable GetData(string religion, string identityCard, string people, string placeOfWork, string position)
        {
            return Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employees_Search", parameter: new { Religion = religion, IdentityCard = identityCard, People = people, PlaceOfWork = placeOfWork, Position = position });
        }

        public DataTable GetDataByCertificate(string diplomasName, string faculty, string major, string certificatesName, string placeOfTraining)
        {
            return Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employees_SearchByDiplomas", parameter: new { DiplomasName = diplomasName, Faculty = faculty, Major = major, CertificatesName = certificatesName, PlaceOfTraining = placeOfTraining });
        }

        public DataTable GetDataByDecision(string numberOfDecisionReward, string reasonReward, string formReward, string numberOfDecisionDiscipline, string reasonDiscipline, string formDiscipline)
        {
            return Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employees_SearchByDecision", parameter: new
            {
                NumberOfDecisionReward = numberOfDecisionReward,
                ReasonReward = reasonReward,
                FormReward = formReward,
                NumberOfDecisionDiscipline = numberOfDecisionDiscipline,
                ReasonDiscipline = reasonDiscipline,
                FormDiscipline = formDiscipline
            });
        }

        public DataTable GetDataByContract(string numberOfContracts, string typeOfContracts, string numberOfInsurances, string typeOfInsurances)
        {
            return Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employees_SearchByContract", parameter: new { NumberOfContracts = numberOfContracts, TypeOfContracts = typeOfContracts, NumberOfInsurances = numberOfInsurances, TypeOfInsurances = typeOfInsurances });
        }

        public DataTable GetStatistic(string statisticType)
        {
            //Tổng hợp dữ liệu
            int i = Sql_ADO.idasAdoService.ExecuteNoquery("sp_Employees_Statistic", parameter: new { StatisticType = statisticType });

            //Lấy dữ liệu
            return Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employees_GetStatistic", parameter: new { StatisticType = statisticType });
        }
    }
}
