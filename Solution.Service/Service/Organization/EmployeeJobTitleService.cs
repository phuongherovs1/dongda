﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class EmployeeJobTitleService : BaseService<EmployeeJobTitleDTO, EmployeeJobTitleBO>, IEmployeeJobTitleService
    {
        public IEnumerable<EmployeeJobTitleBO> GetByEmployeeId(Guid id)
        {
            var data = base.GetAll().Where(e => e.HumanEmployeeId == id);

            return data;
        }

        public void Update(EmployeeJobTitleBO jobtitle)
        {
            throw new NotImplementedException();
        }
    }
}