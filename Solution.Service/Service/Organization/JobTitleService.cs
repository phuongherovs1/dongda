﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace iDAS.Service
{
    public class JobTitleService : BaseService<JobTitleDTO, JobTitleBO>, IJobTitleService
    {

        public IEnumerable<JobTitleBO> Get(Expression<Func<JobTitleBO, bool>> filter = null, Func<IQueryable<JobTitleBO>, IQueryable<JobTitleBO>> include = null, Func<IQueryable<JobTitleBO>, IOrderedQueryable<JobTitleBO>> orderBy = null, Func<IQueryable<JobTitleBO>, IOrderedQueryable<JobTitleBO>> orderByDescending = null, bool allowDeleted = false)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<JobTitleBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<JobTitleBO> GetAllNotPaging()
        {
            var data = base.GetAll();
            return data;
        }

        public JobTitleBO GetSingle(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Guid> InsertRange(IEnumerable<JobTitleBO> data, bool allowSave = true)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<JobTitleBO> Page(IEnumerable<JobTitleBO> data, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public IQueryable<JobTitleBO> Page(IQueryable<JobTitleBO> data, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }


        IEnumerable<JobTitleBO> IService<JobTitleBO>.GetAll(bool allowDeleted)
        {
            throw new NotImplementedException();
        }

        IEnumerable<JobTitleBO> IService<JobTitleBO>.GetByIds(IEnumerable<Guid> ids, bool allowDeleted)
        {
            throw new NotImplementedException();
        }

        IQueryable<JobTitleBO> IService<JobTitleBO>.GetByIds(IQueryable<Guid> ids, bool allowDeleted)
        {
            throw new NotImplementedException();
        }

        JobTitleBO IService<JobTitleBO>.GetJustById(Guid id, bool allowDefaultIfNull)
        {
            throw new NotImplementedException();
        }

        IEnumerable<JobTitleBO> IService<JobTitleBO>.GetNotInIds(IEnumerable<Guid> ids, bool allowDeleted)
        {
            throw new NotImplementedException();
        }

        IQueryable<JobTitleBO> IService<JobTitleBO>.GetNotInIds(IQueryable<Guid> ids, bool allowDeleted)
        {
            throw new NotImplementedException();
        }

        IQueryable<JobTitleBO> IService<JobTitleBO>.GetQuery(bool allowDeleted)
        {
            throw new NotImplementedException();
        }

        IQueryable<JobTitleBO> IService<JobTitleBO>.GetQueryById(Guid id, bool allowDeleted)
        {
            throw new NotImplementedException();
        }

        IEnumerable<JobTitleBO> IService<JobTitleBO>.Page(out int count, int pageIndex, int pageSize, bool allowDeleted)
        {
            throw new NotImplementedException();
        }
    }
}