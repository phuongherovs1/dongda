﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanRecruitmentPlanInterviewService : BaseService<VPHumanRecruitmentPlanInterviewDTO, VPHumanRecruitmentPlanInterviewBO>, IVPHumanRecruitmentPlanInterviewService
    {

        public IEnumerable<VPHumanRecruitmentPlanInterviewBO> GetByPlanId(Guid planId, int pageIndex, int pageSize, out int count)
        {
            var data = Get(i => i.HumanRecruitmentPlanId == planId);
            count = data.Count();
            return Page(data, pageIndex, pageSize);
        }

        public IEnumerable<VPHumanRecruitmentPlanInterviewBO> GetByProfieInterviewId(Guid planId, Guid profileInterviewId, int pageIndex, int pageSize, out int count)
        {
            var data = Get(i => i.HumanRecruitmentPlanId == planId);
            count = data.Count();
            data = Page(data, pageIndex, pageSize);
            data = data.Select(i =>
            {
                var interviewResult = ServiceFactory<VPHumanRecruitmentInterviewService>().Get(u => u.HumanRecruitmentProfileInterviewId == profileInterviewId && u.HumanRecruitmentPlanInterviewId == i.Id).FirstOrDefault();
                if (interviewResult != null)
                {
                    i.Result = interviewResult == null ? null : interviewResult.Result;
                    i.Note = interviewResult == null ? null : interviewResult.Note;
                    i.Time = interviewResult == null ? null : interviewResult.Time;
                    // i.InterviewResult.Id = interviewResult == null ? Guid.Empty : interviewResult.Id;

                }
                i.HumanRecruitmentPlanInterviewId = i.Id;
                i.HumanRecruitmentProfileInterviewId = profileInterviewId;

                //i.InterviewResult.PlanInterviewName = i.Name;
                //i.InterviewResult.StartTime = i.StartTime;
                //i.InterviewResult.EndTime = i.EndTime;
                //i.InterviewResult.InterviewContent = i.Content;


                return i;
            });
            return data;
        }
    }
}