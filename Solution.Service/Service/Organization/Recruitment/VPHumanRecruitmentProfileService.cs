﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace iDAS.Service
{
    public class VPHumanRecruitmentProfileService : BaseService<VPHumanRecruitmentProfileDTO, VPHumanRecruitmentProfileBO>, IVPHumanRecruitmentProfileService
    {
        public IEnumerable<VPHumanRecruitmentProfileBO> GetAll(int pageIndex, int pageSize, out int totalCount)
        {
            var data = base.GetQuery().OrderByDescending(i => i.CreateAt); ;
            totalCount = data.Count();
            var result = base.Page(data, pageIndex, pageSize).ToList();
            return result.ToList();
        }

        public IEnumerable<VPHumanRecruitmentProfileBO> GetAllProfileInterview(int pageIndex, int pageSize, out int totalCount, Guid PlanId, string filterName = default(string))
        {
            List<Guid> lstIDlistDataTrungTuyen = new List<Guid>();
            List<Guid> lst = new List<Guid>();
            var listDataTrungTuyen = ServiceFactory<VPHumanRecruitmentProfileResultService>().Get(x => x.IsPass == true && x.IsDelete == false);
            if (listDataTrungTuyen != null)
            {
                lstIDlistDataTrungTuyen = listDataTrungTuyen.Select(x => x.HumanRecruitmentProfileInterviewId.Value).ToList();
                var tProfileInterview = ServiceFactory<VPHumanRecruitmentProfileInterviewService>().GetByIds(lstIDlistDataTrungTuyen);
                if (tProfileInterview != null)
                    lst = tProfileInterview.Select(x => x.HumanRecruitmentProfileId.Value).ToList();
            }
            List<Guid> lstIDlistDataThuViec = new List<Guid>();
            var listDataThuViec = ServiceFactory<VPHumanProfileWorkTrialResultService>().Get(x => x.IsDelete == false);
            if (listDataThuViec != null)
                lstIDlistDataThuViec = listDataThuViec.Select(x => x.HumanProfileWorkTrialId.Value).ToList();
            lstIDlistDataThuViec.AddRange(lst);
            lstIDlistDataThuViec = lstIDlistDataThuViec.Distinct().ToList();
            //var ProfileInterview = ServiceFactory<VPHumanRecruitmentProfileInterviewService>().Get(m => m.HumanRecruitmentPlanId == PlanId);
            if (lstIDlistDataTrungTuyen.Count > 0)
            {
                if (!string.IsNullOrEmpty(filterName))
                {
                    var data = base.Get(x => !lstIDlistDataThuViec.Contains(x.Id) && x.Name.Trim().ToUpper().Contains(filterName.Trim().ToUpper())).OrderByDescending(i => i.CreateAt);
                    totalCount = data.Count();
                    var result = base.Page(data, pageIndex, pageSize).ToList();
                    return result.ToList();
                }
                else
                {
                    var data = base.Get(x => !lstIDlistDataThuViec.Contains(x.Id)).OrderByDescending(i => i.CreateAt);
                    totalCount = data.Count();
                    var result = base.Page(data, pageIndex, pageSize).ToList();
                    return result.ToList();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(filterName))
                {
                    var data = base.Get(x => x.Name.Trim().ToUpper().Contains(filterName.Trim().ToUpper()));
                    totalCount = data.Count();
                    var result = base.Page(data, pageIndex, pageSize).ToList();
                    return result.ToList();
                }
                else
                {
                    var data = base.GetAll();
                    totalCount = data.Count();
                    var result = base.Page(data, pageIndex, pageSize).ToList();
                    return result.ToList();
                }
            }
        }

        public IEnumerable<VPHumanRecruitmentProfileBO> LoadProfileInterviewByPlan(int pageIndex, int pageSize, out int totalCount, Guid PlanId)
        {
            var ProfileIds = ServiceFactory<VPHumanRecruitmentProfileInterviewService>().Get(m => m.HumanRecruitmentPlanId == PlanId).Select(i => i.HumanRecruitmentProfileId);
            var data = base.GetQuery().Where(i => ProfileIds.Contains(i.Id)).OrderByDescending(i => i.CreateAt);
            totalCount = data.Count();
            var result = base.Page(data, pageIndex, pageSize).AsEnumerable();
            result = result.Select(i =>
            {
                var ProfileInterView = ServiceFactory<VPHumanRecruitmentProfileInterviewService>().Get(m => m.HumanRecruitmentPlanId == PlanId && m.HumanRecruitmentProfileId.Value == i.Id).FirstOrDefault();
                i.ProfileInterViewId = ProfileInterView.Id;
                i.IsEdit = ProfileInterView.IsEdit;
                i.RequirementID = ProfileInterView.HumanRecruitmentRequirementId;
                if (i.RequirementID.HasValue)
                {
                    VPHumanRecruitmentRequirementBO VPHumanRecruitmentRequirement = ServiceFactory<VPHumanRecruitmentRequirementService>().GetById(i.RequirementID.Value);
                    if (VPHumanRecruitmentRequirement != null)
                        i.RequirementName = VPHumanRecruitmentRequirement.Name;
                }
                var ProfileResultS = ServiceFactory<VPHumanRecruitmentProfileResultService>().Get(k => k.HumanRecruitmentProfileInterviewId == ProfileInterView.Id).FirstOrDefault();
                if (ProfileResultS != null)
                {
                    i.IsApproval = ProfileResultS.IsApproval;
                    i.IsPass = ProfileResultS.IsPass;
                }
                return i;

            });
            return result;
        }
        public VPHumanRecruitmentProfileBO GetById(Guid Id)
        {
            return base.GetById(Id);
        }
        public void Update(VPHumanRecruitmentProfileBO item)
        {
            if (item != null && item.FileAttachs != null && item.FileAttachs.FileRemoves.Any())
            {
                foreach (Guid guiditem in item.FileAttachs.FileRemoves)
                {
                    FileBO filebo = ServiceFactory<FileService>().GetById(guiditem);
                    File.Delete(HttpContext.Current.Server.MapPath("/Upload/" + guiditem.ToString() + "." + filebo.Extension));
                    ServiceFactory<FileService>().Delete(guiditem);
                    item.FileId = Guid.Empty;
                }
            }
            if (item != null && item.FileAttachs != null && item.FileAttachs.FileAttachments.Any())
            {
                item.FileId = ServiceFactory<FileService>().Upload(item.FileAttachs, true);
            }
            base.Update(item);
        }
        public void Insert(VPHumanRecruitmentProfileBO item)
        {
            if (item != null && item.FileAttachs != null && item.FileAttachs.FileRemoves.Any())
            {
                foreach (Guid guiditem in item.FileAttachs.FileRemoves)
                {
                    FileBO filebo = ServiceFactory<FileService>().GetById(guiditem);
                    File.Delete(HttpContext.Current.Server.MapPath("/Upload/" + guiditem.ToString() + "." + filebo.Extension));
                    ServiceFactory<FileService>().Delete(guiditem);
                    item.FileId = Guid.Empty;
                }
            }
            if (item != null && item.FileAttachs != null && item.FileAttachs.FileAttachments.Count > 0 && item.FileAttachs.FileAttachments[0] != null)
            {
                item.FileId = ServiceFactory<FileService>().Upload(item.FileAttachs, true);
            }
            base.Insert(item);
        }
        public void Delete(Guid id)
        {
            base.Delete(id);
        }
    }
}