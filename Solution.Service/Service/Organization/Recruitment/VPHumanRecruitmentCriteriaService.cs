﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanRecruitmentCriteriaService : BaseService<VPHumanRecruitmentCriteriaDTO, VPHumanRecruitmentCriteriaBO>, IVPHumanRecruitmentCriteriaService
    {
        public IEnumerable<VPHumanRecruitmentCriteriaBO> GetByDepartment(int pageIndex, int pageSize, out int totalCount, Guid departmentID)
        {
            var titleIds = ServiceFactory<DepartmentTitleService>().GetQuery().Where(i => i.DepartmentId == departmentID)
                                .Select(i => i.Id);
            var data = base.GetQuery().Where(i => i.HumanRoleId.HasValue && titleIds.Contains(i.HumanRoleId.Value)).OrderByDescending(i => i.CreateAt).ToList();
            var dataNot = base.GetQuery().Where(i => !i.HumanRoleId.HasValue).OrderByDescending(i => i.CreateAt).ToList();
            data.AddRange(dataNot);
            totalCount = data.Count;
            var results = base.Page(data, pageIndex, pageSize).ToList();
            results = results.Select(i =>
           {
               var titleIName = ServiceFactory<DepartmentTitleService>().Get(n => n.Id == i.HumanRoleId).FirstOrDefault();
               if (titleIName != null)
                   i.RoleName = titleIName.Name;
               else
                   i.RoleName = "Khác";
               return i;
           }).ToList();
            return results;
        }
        public void Active(Guid id, bool IsActive)
        {
            var rc = GetById(id);
            rc.IsActive = IsActive;
            Update(rc);
        }
        public void IsDelete(Guid id)
        {
            var rc = GetById(id);
            rc.IsDelete = true;
            rc.UpdateAt = DateTime.Now;
            Update(rc);
        }

        public IEnumerable<VPHumanRecruitmentCriteriaBO> GetCriteria(int pageIndex, int pageSize, out int count, Guid profileInterviewID, Guid RequirementID)
        {
            var roleId = ServiceFactory<VPHumanRecruitmentRequirementService>().GetById(RequirementID).HumanRoleId;

            var profileId = ServiceFactory<VPHumanRecruitmentProfileInterviewService>().GetById(profileInterviewID).HumanRecruitmentProfileId;

            var data = Get(i => i.HumanRoleId == roleId);
            count = data.Count();
            data = Page(data, pageIndex, pageSize);

            var criteriaIds = data.Select(i => i.Id);

            var reviews = ServiceFactory<VPHumanRecruitmentReviewService>().Get(i => i.HumanRecruitmentProfileId == profileId && i.HumanRecruitmentCriteriaId.HasValue && criteriaIds.Contains(i.HumanRecruitmentCriteriaId.Value));

            //  var humanRecruitmentCriteriasIds = Get(i => i.HumanRoleId == roleId).Select(i => i.Id);

            //   var data = Get(i => i.HumanRecruitmentProfileId == profileId && humanRecruitmentCriteriasIds.Contains(i.HumanRecruitmentCriteriaId.Value));

            data = data.Select(
                i =>
                {
                    //var criteria = humanRecruitmentCriterias.FirstOrDefault(u => i.HumanRecruitmentCriteriaId == u.Id);
                    //i.HumanRecruitmentCriteriaId = criteria.Id;
                    //i.HumanRecruitmentProfileId = profileId;
                    //i.CriteriaName = criteria.Name;

                    var review = reviews.FirstOrDefault(u => u.HumanRecruitmentCriteriaId == i.Id);
                    if (review != null)
                    {
                        i.Note = review.Note;
                        i.Point = review.Point;
                        i.Time = review.Time;
                    }
                    i.HumanRecruitmentCriteriaId = i.Id;
                    i.HumanRecruitmentProfileId = profileId;
                    return i;
                });
            count = data.Count();
            return data;

        }

        //public VPHumanRecruitmentCriteriaBO getDetail(Guid Id)
        //{
        //    var item = base.GetById(Id);
        //    var titleIName = ServiceFactory<DepartmentTitleService>().GetById(item.HumanRoleId.Value);
        //    item.RoleName = titleIName.Name;
        //    return item;
        //}
        /// <summary>
        /// Update: Thịnh
        /// Date:   10/09/2019
        /// </summary>
        public VPHumanRecruitmentCriteriaBO getDetail(Guid Id)
        {
            var item = base.GetById(Id);
            if (item.HumanRoleId != null && item.HumanRoleId.Value != Guid.Empty)
            {
                var titleIName = ServiceFactory<DepartmentTitleService>().GetById(item.HumanRoleId.Value);
                item.RoleName = titleIName.Name;
            }
            else
                item.RoleName = "";
            return item;
        }

    }
}