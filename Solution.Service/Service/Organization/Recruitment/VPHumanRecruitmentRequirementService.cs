﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanRecruitmentRequirementService : BaseService<VPHumanRecruitmentRequirementDTO, VPHumanRecruitmentRequirementBO>, IVPHumanRecruitmentRequirementService
    {
        public List<Guid> GetDepartmentsListParent(Guid ID, List<Guid> LstOut)
        {
            if (LstOut == null || LstOut.Count == 0)
                LstOut = new List<Guid>();
            if (LstOut.IndexOf(ID) == -1)
                LstOut.Add(ID);
            List<DepartmentBO> department = ServiceFactory<DepartmentService>().Get(x => x.ParentId.HasValue && x.ParentId.Value.ToString() == ID.ToString()).ToList();
            if (department.Count > 0)
            {
                foreach (DepartmentBO item in department)
                    GetDepartmentsListParent(item.Id, LstOut);
            }
            return LstOut;
        }
        public IEnumerable<VPHumanRecruitmentRequirementBO> GetAllByDeptID(int pageIndex, int pageSize, out int totalCount, Guid DepartmentID)
        {
            List<Guid> lstDepartment = new List<Guid>();
            lstDepartment = GetDepartmentsListParent(DepartmentID, lstDepartment);
            var titleIds = ServiceFactory<DepartmentTitleService>().GetQuery().Where(i => i.DepartmentId.HasValue && lstDepartment.Contains(i.DepartmentId.Value))
                               .Select(i => i.Id);
            var data = base.GetQuery().Where(i => i.HumanRoleId.HasValue && titleIds.Contains(i.HumanRoleId.Value)).OrderByDescending(i => i.CreateAt);
            totalCount = data.Count();
            var results = base.Page(data, pageIndex, pageSize).ToList();
            results = results.Select(i =>
            {
                var titleIName = ServiceFactory<DepartmentTitleService>().Get(n => n.Id == i.HumanRoleId).FirstOrDefault();
                i.RoleName = titleIName.Name;
                if (i.ApprovalBy != null && i.ApprovalBy != Guid.Empty)
                    i.NameEmployeeApproveBy = ServiceFactory<EmployeeService>().GetById(i.ApprovalBy.Value).Name;
                i.UserId = UserId;
                if (i.HumanRoleId.HasValue)
                {
                    var Title = ServiceFactory<DepartmentTitleService>().GetById(i.HumanRoleId.Value);
                    if (Title != null && Title.DepartmentId.HasValue)
                    {
                        var Department = ServiceFactory<DepartmentService>().GetById(Title.DepartmentId.Value);
                        i.DepartmentName = Department != null ? Department.Name : "";
                    }
                }
                if (!i.ApproveNumber.HasValue)
                    i.ApproveNumber = 0;
                return i;
            }).ToList();
            return results;

        }
        public IEnumerable<VPHumanRecruitmentRequirementBO> GetAll(int pageIndex, int pageSize, out int totalCount)
        {
            var titleIds = ServiceFactory<DepartmentTitleService>().GetQuery().Select(i => i.Id);
            var data = base.GetQuery().Where(i => i.HumanRoleId.HasValue && titleIds.Contains(i.HumanRoleId.Value)).OrderByDescending(i => i.CreateAt);
            totalCount = data.Count();
            var results = base.Page(data, pageIndex, pageSize).ToList();
            results = results.Select(i =>
            {
                var titleIName = ServiceFactory<DepartmentTitleService>().Get(n => n.Id == i.HumanRoleId).FirstOrDefault();
                i.RoleName = titleIName.Name;
                if (i.ApprovalBy != null && i.ApprovalBy != Guid.Empty)
                    i.NameEmployeeApproveBy = ServiceFactory<EmployeeService>().GetById(i.ApprovalBy.Value).Name;
                i.UserId = UserId;
                return i;
            }).ToList();
            return results;

        }

        public void Approve(VPHumanRecruitmentRequirementBO updateData)
        {
            updateData.ApprovalAt = DateTime.Now;
            updateData.IsApproval = true;
            base.Update(updateData);
        }

        public void DeleteRequirAndApprov(Guid id)
        {

        }

        public VPHumanRecruitmentRequirementBO updateRecruitment(Guid Id)
        {
            var item = base.GetById(Id);
            var titleIName = ServiceFactory<DepartmentTitleService>().GetById(item.HumanRoleId.Value);
            item.RoleName = titleIName.Name;
            var employees = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.ApprovalBy == null ? Guid.Empty : item.ApprovalBy.Value);
            item.EmployeeApproveBy = employees;
            return item;
        }

        public IEnumerable<VPHumanRecruitmentRequirementBO> GetRequirementApproved(string planId, int pageIndex, int pageSize, out int count)
        {
            var data = Get(i => i.IsApproval == true && i.IsAccept == true && i.IsDelete == false);
            count = data.Count();
            data = Page(data, pageIndex, pageSize);
            if (!string.IsNullOrEmpty(planId))
            {
                var requirementIds = data.Select(i => i.Id);
                var recruitmentPlanRequirementIds = ServiceFactory<VPHumanRecruitmentPlanRequirementService>()
                    .GetQuery().Where(i => i.HumanRecruitmentPlanId == new Guid(planId) && i.HumanRecruitmentRequirementId.HasValue && requirementIds.Contains(i.HumanRecruitmentRequirementId.Value))
                    .Select(i => i.HumanRecruitmentRequirementId.Value);

                data = data.Select(i =>
                {
                    var checkSelected = recruitmentPlanRequirementIds.Contains(i.Id);
                    i.IsSelected = checkSelected;
                    return i;
                });
            }
            return data;
        }


        public IEnumerable<VPHumanRecruitmentRequirementBO> GetRequirementByplanId(Guid planId)
        {
            var RequimentIds = ServiceFactory<VPHumanRecruitmentPlanRequirementService>().Get(i => i.HumanRecruitmentPlanId == planId).Select(k => k.HumanRecruitmentRequirementId);
            var result = base.Get(i => RequimentIds.Contains(i.Id));
            return result;
        }
    }
}