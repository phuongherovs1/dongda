﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanRecruitmentPlanRequirementService : BaseService<VPHumanRecruitmentPlanRequirementDTO, VPHumanRecruitmentPlanRequirementBO>, IVPHumanRecruitmentPlanRequirementService
    {
        public IEnumerable<VPHumanRecruitmentPlanRequirementBO> GetByPlanId(Guid planId, int pageIndex, int pageSize, out int count)
        {
            var data = Get(i => i.HumanRecruitmentPlanId == planId);
            count = data.Count();
            data = Page(data, pageIndex, pageSize);
            data = data.Select(i =>
            {
                var requirement = ServiceFactory<VPHumanRecruitmentRequirementService>().GetById(i.HumanRecruitmentRequirementId.Value);
                i.Name = requirement.Name;
                i.Reason = requirement.Reason;
                return i;
            });
            return data;
        }


        public Guid Insert(Guid planId, Guid requirementId)
        {
            var data = new VPHumanRecruitmentPlanRequirementBO()
            {
                HumanRecruitmentRequirementId = requirementId,
                HumanRecruitmentPlanId = planId
            };
            data.Id = base.Insert(data);
            return data.Id;

        }

        public void Delete(Guid planId, Guid requirementId)
        {
            var ids = Get(i => i.HumanRecruitmentPlanId == planId && i.HumanRecruitmentRequirementId == requirementId).Select(i => i.Id);
            DeleteRange(ids);

        }
    }
}