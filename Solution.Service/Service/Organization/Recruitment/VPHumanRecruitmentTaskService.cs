﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanRecruitmentTaskService : BaseService<VPHumanRecruitmentTaskDTO, VPHumanRecruitmentTaskBO>, IVPHumanRecruitmentTaskService
    {
        public List<VPHumanRecruitmentTaskBO> GetAll(int pageIndex, int pageSize, out int totalCount)
        {
            var data = base.GetQuery();
            totalCount = data.Count();
            data = base.Page(data, pageIndex, pageSize);
            return data.ToList();
        }
        public VPHumanRecruitmentTaskBO GetById(Guid Id)
        {
            return base.GetById(Id);
        }
        public void Update(VPHumanRecruitmentTaskBO item, int userID)
        {
            base.Update(item);
        }
        public void Insert(VPHumanRecruitmentTaskBO item, int userID)
        {
            base.Insert(item);
        }
        public void Delete(Guid id)
        {
            base.Delete(id);
        }
    }
}