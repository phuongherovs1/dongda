﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanRecruitmentProfileInterviewService : BaseService<VPHumanRecruitmentProfileInterviewDTO, VPHumanRecruitmentProfileInterviewBO>, IVPHumanRecruitmentProfileInterviewService
    {

        public IEnumerable<VPHumanRecruitmentProfileInterviewBO> GetByPlanId(Guid planId, int pageIndex, int pageSize, out int count)
        {
            var data = Get(i => i.HumanRecruitmentPlanId == planId);
            count = data.Count();
            return Page(data, pageIndex, pageSize);
        }

        public void DleteProfileInterview(Guid planId, Guid ProfileId)
        {
            var data = Get(i => i.HumanRecruitmentPlanId == planId && i.HumanRecruitmentProfileId == ProfileId).FirstOrDefault();
            base.Delete(data.Id);
        }

        public void InsertProfileResult(VPHumanRecruitmentProfileResultBO item)
        {
            ServiceFactory<VPHumanRecruitmentProfileResultService>().Insert(item, true);
            var data = base.GetById(item.HumanRecruitmentProfileInterviewId.Value);
            data.IsEdit = false;
            base.Update(data, true);
        }

        public VPHumanRecruitmentProfileResultBO GetResultByProfileInterViewID(Guid ProfileInterviewID)
        {
            var item = base.GetById(ProfileInterviewID);
            var data = ServiceFactory<VPHumanRecruitmentProfileResultService>().Get(i => i.HumanRecruitmentProfileInterviewId == ProfileInterviewID).FirstOrDefault();
            if (data != null)
            {
                data.HumanRecruitmentProfileInterviewId = ProfileInterviewID;
                var employ = ServiceFactory<EmployeeService>().GetById(data.ApprovalBy.Value);
                data.EmployeeApproveBy = employ;
                data.ProfileName = ServiceFactory<VPHumanRecruitmentProfileService>().GetById(item.HumanRecruitmentProfileId.Value).Name;
                data.PlanName = ServiceFactory<VPHumanRecruitmentPlanService>().GetById(item.HumanRecruitmentPlanId.Value).Name;
                var Requiremen = ServiceFactory<VPHumanRecruitmentRequirementService>().GetById(item.HumanRecruitmentRequirementId.Value);
                data.RoleName = ServiceFactory<DepartmentTitleService>().GetById(Requiremen.HumanRoleId.Value).Name;
                var Review = ServiceFactory<VPHumanRecruitmentReviewService>().Get(i => i.HumanRecruitmentProfileId == item.HumanRecruitmentProfileId);
                data.CriteriaMinPoint = Review.Min(p => p.Point);
                data.CriteriaMaxPoint = Review.Max(p => p.Point);
                data.TotalPoint = Review.Sum(p => p.Point);
                return data;
            }
            else
            {
                data = new VPHumanRecruitmentProfileResultBO() { };
                data.HumanRecruitmentProfileInterviewId = ProfileInterviewID;
                data.ProfileName = ServiceFactory<VPHumanRecruitmentProfileService>().GetById(item.HumanRecruitmentProfileId.Value).Name;
                data.PlanName = ServiceFactory<VPHumanRecruitmentPlanService>().GetById(item.HumanRecruitmentPlanId.Value).Name;
                var Requiremen = ServiceFactory<VPHumanRecruitmentRequirementService>().GetById(item.HumanRecruitmentRequirementId.Value);
                data.RoleName = ServiceFactory<DepartmentTitleService>().GetById(Requiremen.HumanRoleId.Value).Name;
                var Review = ServiceFactory<VPHumanRecruitmentReviewService>().Get(i => i.HumanRecruitmentProfileId == item.HumanRecruitmentProfileId);
                data.CriteriaMinPoint = Review.Min(p => p.Point);
                data.CriteriaMaxPoint = Review.Max(p => p.Point);
                data.TotalPoint = Review.Sum(p => p.Point);
                return data;
            }


        }

    }
}