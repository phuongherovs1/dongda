﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanRecruitmentPlanService : BaseService<VPHumanRecruitmentPlanDTO, VPHumanRecruitmentPlanBO>, IVPHumanRecruitmentPlanService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public IEnumerable<VPHumanRecruitmentPlanBO> GetData(int pageIndex, int pageSize, out int count)
        {
            var data = Page(out count, pageIndex, pageSize);
            data = data.Select(i =>
            {
                i.UserId = UserId;
                if (i.ApprovalBy != null && i.ApprovalBy != Guid.Empty)
                    i.NameEmployeeApproveBy = ServiceFactory<EmployeeService>().GetById(i.ApprovalBy.Value).Name;
                return i;
            }).ToList();
            return data;
        }
        public VPHumanRecruitmentPlanBO GetById(Guid? id)
        {
            var data = base.GetById(id.Value, false);
            if (data != null)
                data.UserId = UserId;
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="requirementIds"></param>
        /// <returns></returns>
        public Guid Save(VPHumanRecruitmentPlanBO data, string jsonIds)
        {
            try
            {
                data.IsEdit = true;
                var planId = base.Insert(data, false);
                var requirementIds = JsonConvert.DeserializeObject<List<Guid>>(jsonIds);
                if (requirementIds != null && requirementIds.Count > 0)
                {
                    var requirements = new List<VPHumanRecruitmentPlanRequirementBO>();
                    foreach (var requrementId in requirementIds)
                    {
                        var requirement = new VPHumanRecruitmentPlanRequirementBO()
                        {
                            HumanRecruitmentPlanId = planId,
                            HumanRecruitmentRequirementId = requrementId
                        };
                        requirements.Add(requirement);
                    }

                    ServiceFactory<VPHumanRecruitmentPlanRequirementService>().InsertRange(requirements, false);
                }
                SaveTransaction();
                return planId;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public void Update(VPHumanRecruitmentPlanBO data, string jsonIds)
        {
            try
            {
                base.Update(data, false);
                var requirementIds = JsonConvert.DeserializeObject<List<Guid>>(jsonIds);

                //// remove
                //var removeIds = ServiceFactory<VPHumanRecruitmentPlanRequirementService>()
                //    .Get(i => (requirementIds == null || !requirementIds.Any() || requirementIds.Contains(i.HumanRecruitmentRequirementId.Value))
                //        && i.HumanRecruitmentPlanId == data.Id).Select(i => i.Id);
                //ServiceFactory<VPHumanRecruitmentPlanRequirementService>().RemoveRange(removeIds);
                // insert
                var existRequirements = ServiceFactory<VPHumanRecruitmentPlanRequirementService>().Get(i => i.HumanRecruitmentPlanId == data.Id);
                var existRequirementIds = existRequirements.Select(i => i.Id);
                var removeRequirementIds = existRequirementIds.Except(requirementIds);

                var removeIds = existRequirements.Where(i => removeRequirementIds.Contains(i.HumanRecruitmentRequirementId.Value)).Select(i => i.Id);

                var insertRequirementIds = requirementIds.Except(existRequirementIds);

                var insertRequirements = from i in insertRequirementIds
                                         select new VPHumanRecruitmentPlanRequirementBO
                                         {
                                             HumanRecruitmentPlanId = data.Id,
                                             HumanRecruitmentRequirementId = i
                                         };

                ServiceFactory<VPHumanRecruitmentPlanRequirementService>().DeleteRange(removeIds, false);
                ServiceFactory<VPHumanRecruitmentPlanRequirementService>().InsertRange(insertRequirements, false);
                SaveTransaction();
                // var insertIds = 
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void SendApprove(VPHumanRecruitmentPlanBO data)
        {
            data.ApprovalBy = data.EmployeeApproveBy.Id;
            data.IsEdit = false;
            base.Update(data);
            SaveTransaction();
        }

        public IEnumerable<VPHumanRecruitmentPlanBO> GetAllAprovel(int pageIndex, int pageSize, out int totalCount)
        {
            var data = base.GetQuery().Where(i => i.IsAccept == true && i.IsApproval == true).OrderByDescending(i => i.CreateAt); ;
            totalCount = data.Count();
            var result = base.Page(data, pageIndex, pageSize);
            return result;
        }

        public void Approve(VPHumanRecruitmentPlanBO data)
        {
            base.Update(data);
        }
    }
}