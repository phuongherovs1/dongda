﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanRecruitmentProfileResultService : BaseService<VPHumanRecruitmentProfileResultDTO, VPHumanRecruitmentProfileResultBO>, IVPHumanRecruitmentProfileResultService
    {
        public List<VPHumanRecruitmentProfileResultBO> GetAll(int pageIndex, int pageSize, out int totalCount)
        {
            var data = base.GetQuery();
            totalCount = data.Count();
            data = base.Page(data, pageIndex, pageSize);
            return data.ToList();
        }


        /// <summary>
        /// Danh sách kết quả trúng tuyển theo kế hoạch tuyển dụng
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="planId"></param>
        /// <returns></returns>
        public IEnumerable<VPHumanRecruitmentProfileResultBO> GetProfileResultByPlan(int pageIndex, int pageSize, out int count, Guid planId)
        {
            try
            {
                var interviews = ServiceFactory<VPHumanRecruitmentProfileInterviewService>().GetQuery().Where(p => p.HumanRecruitmentPlanId == planId);

                var profileInterviewIds = interviews.Select(p => p.Id);
                var data = base.GetQuery().Where(p => profileInterviewIds.Contains(p.HumanRecruitmentProfileInterviewId.Value));


                count = data.Count();
                var result = Page(data.AsEnumerable().OrderBy(p => p.CreateAt), pageIndex, pageSize);

                result = result.Select(i =>
                {
                    var interview = interviews.FirstOrDefault(u => i.HumanRecruitmentProfileInterviewId == u.Id);
                    if (interview != null && interview.HumanRecruitmentProfileId.HasValue)
                    {
                        var recruitmentProfile = ServiceFactory<VPHumanRecruitmentProfileService>().GetById(interview.HumanRecruitmentProfileId.Value);
                        i.HumanName = recruitmentProfile.Name;
                        i.ProfileId = recruitmentProfile.Id;

                    }
                    return i;
                });
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public VPHumanRecruitmentProfileResultBO GetDetailResultFormItem(Guid id)
        {
            try
            {
                var data = GetById(id);
                var recruitmentProfileInterview = ServiceFactory<VPHumanRecruitmentProfileInterviewService>().GetById(data.HumanRecruitmentProfileInterviewId.Value);
                var profile = ServiceFactory<VPHumanRecruitmentProfileService>().GetById(recruitmentProfileInterview.HumanRecruitmentProfileId.Value);
                var plan = ServiceFactory<VPHumanRecruitmentPlanService>().GetById(recruitmentProfileInterview.HumanRecruitmentPlanId.Value);
                data.PlanID = recruitmentProfileInterview.HumanRecruitmentPlanId.Value;

                data.ProfileName = profile.Name;
                data.PlanName = plan.Name;

                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        public void Approve(VPHumanRecruitmentProfileResultBO data)
        {
            data.IsApproval = true;
            Update(data);
        }
    }
}