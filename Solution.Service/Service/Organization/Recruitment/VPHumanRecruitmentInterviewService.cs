﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanRecruitmentInterviewService : BaseService<VPHumanRecruitmentInterviewDTO, VPHumanRecruitmentInterviewBO>, IVPHumanRecruitmentInterviewService
    {
        public IEnumerable<VPHumanRecruitmentInterviewBO> GetByProfieInterviewID(int p1, int p2, out int totalCount, Guid ProfileInterviewID, Guid PlanID)
        {
            //var data = Get(i => i.HumanRecruitmentPlanInterviewId == PlanID);
            //data = Page(data, p1 , p2);
            //data = data.Select(i => new VPHumanRecruitmentInterviewBO
            //    {
            //          Result = i.HumanRecruitmentInterviews.FirstOrDefault(i => i.HumanRecruitmentProfileInterviewID == ProfileInterviewID).Result,
            //        Note = item.HumanRecruitmentInterviews.FirstOrDefault(i => i.HumanRecruitmentProfileInterviewID == ProfileInterviewID).Note,
            //        Time = item.HumanRecruitmentInterviews.FirstOrDefault(i => i.HumanRecruitmentProfileInterviewID == ProfileInterviewID).Time
            //        return i;
            //    });

            //return data;

            var data = ServiceFactory<VPHumanRecruitmentPlanInterviewService>().Get(i => i.HumanRecruitmentPlanId == PlanID);
            totalCount = data.Count();
            var result = from d in data
                         select new VPHumanRecruitmentInterviewBO
                         {
                             Id = base.Get(i => i.HumanRecruitmentProfileInterviewId == ProfileInterviewID).FirstOrDefault().Id,
                             HumanRecruitmentPlanInterviewId = d.Id,
                             PlanInterviewName = d.Name,
                             StartTime = d.StartTime,
                             EndTime = d.EndTime,
                             InterviewContent = d.Content,
                             HumanRecruitmentProfileInterviewId = ProfileInterviewID,
                             Result = base.Get(i => i.HumanRecruitmentProfileInterviewId == ProfileInterviewID).FirstOrDefault().Result,
                             Note = base.Get(i => i.HumanRecruitmentProfileInterviewId == ProfileInterviewID).FirstOrDefault().Note,
                             Time = base.Get(i => i.HumanRecruitmentProfileInterviewId == ProfileInterviewID).FirstOrDefault().Time
                         };
            return result;

        }

        public void UpdateOrInsert(VPHumanRecruitmentInterviewBO item)
        {
            var Exits = base.Get(i => i.HumanRecruitmentProfileInterviewId == item.HumanRecruitmentProfileInterviewId && i.HumanRecruitmentPlanInterviewId == item.HumanRecruitmentPlanInterviewId).FirstOrDefault();
            if (Exits != null)
            {
                item.Id = Exits.Id;
                base.Update(item, true);
            }
            else
            {
                var data = new VPHumanRecruitmentInterviewBO()
                {
                    HumanRecruitmentPlanInterviewId = item.HumanRecruitmentPlanInterviewId,
                    HumanRecruitmentProfileInterviewId = item.HumanRecruitmentProfileInterviewId,
                    Note = item.Note,
                    Result = item.Result,
                    Time = item.Time
                };
                base.Insert(data, true);
            }

        }
    }
}