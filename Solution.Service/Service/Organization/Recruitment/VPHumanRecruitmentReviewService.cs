﻿using iDAS.DataAccess;
using System.Linq;
namespace iDAS.Service
{
    public class VPHumanRecruitmentReviewService : BaseService<VPHumanRecruitmentReviewDTO, VPHumanRecruitmentReviewBO>, IVPHumanRecruitmentReviewService
    {
        public void UpdateOrInsert(VPHumanRecruitmentReviewBO item)
        {
            var Exits = base.Get(i => i.HumanRecruitmentProfileId == item.HumanRecruitmentProfileId && i.HumanRecruitmentCriteriaId == item.HumanRecruitmentCriteriaId).FirstOrDefault();
            if (Exits != null)
            {
                item.Id = Exits.Id;
                base.Update(item, true);
            }
            else
            {
                var data = new VPHumanRecruitmentReviewBO()
                {
                    HumanRecruitmentProfileId = item.HumanRecruitmentProfileId,
                    HumanRecruitmentCriteriaId = item.HumanRecruitmentCriteriaId,
                    Note = item.Note,
                    Point = item.Point,
                    Time = item.Time
                };
                base.Insert(data, true);
            }

        }

    }
}