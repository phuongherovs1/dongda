﻿using iDAS.ADO;
using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace iDAS.Service
{
    public class EmployeeService : BaseService<EmployeeDTO, EmployeeBO>, IEmployeeService
    {
        // Lấy nhân sự đăng nhập
        public EmployeeBO GetCurrentUser()
        {
            return GetById(UserId);
        }
        // lấy thông tin nhân sự theo chuỗi id
        public IEnumerable<EmployeeBO> GetInfoByIds(IEnumerable<Guid> ids, bool allowGetRoleName = false, bool allowDeleted = false)
        {
            return base.GetByIds(ids, allowDeleted)
                .Select(i =>
                {
                    i.RoleNames = allowGetRoleName ? string.Join(",", GetRoleNames(i.Id)) : "";
                    return i;
                });
        }
        // Lấy danh sách nhân sự chưa được chọn
        public IEnumerable<EmployeeBO> GetInfoNotInIds(IQueryable<Guid> employeeIds, string query = "", bool allowGetRoleName = false, bool allowDeleted = false)
        {
            var data = Enumerable.Empty<EmployeeBO>();
            data = base.GetNotInIds(employeeIds, allowDeleted);
            return data.Where(i => string.IsNullOrEmpty(query) || (!string.IsNullOrEmpty(i.Name) && i.Name.ToUpper().Contains(query.ToUpper())))
                            .Select(i =>
                            {
                                i.RoleNames = allowGetRoleName ? string.Join(",", GetRoleNames(i.Id)) : "";
                                return i;
                            });
        }
        //lấy toàn bộ thông tin nhân sự
        public IEnumerable<EmployeeBO> GetInfo(int pageIndex, int pageSize, out int totalCount, string query = "", bool allowGetRoleName = false, bool allowDeleted = false)
        {
            var data = Enumerable.Empty<EmployeeBO>();
            data = base.GetAll(allowDeleted);
            totalCount = data.Count();
            data = data.Where(i => string.IsNullOrEmpty(query) || (!string.IsNullOrEmpty(i.Name) && i.Name.ToUpper().Contains(query.ToUpper())))
                            .Select(i =>
                            {
                                i.RoleNames = allowGetRoleName ? string.Join(",", GetRoleNames(i.Id)) : "";
                                return i;
                            });
            data = base.Page(data, pageIndex, pageSize);
            return data;
        }
        public EmployeeBO GetDetail(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id, allowDeleted, allowDefaultIfNull);
            var user = ServiceFactory<UserService>().GetQuery().Where(i => i.Id == id).FirstOrDefault();
            if (user != null)
            {
                data.UserName = user.UserName;
                data.Password = "        ";
            }
            data.RoleNames = string.Join(",", GetRoleNames(id));
            data.FileAvatar.Data = data.Avatar;
            return data;
        }
        // Lấy thông tin chung cho nhân sự
        public EmployeeBO GetEmployeeGeneralInfo(Guid id)
        {
            var employee = base.GetById(id);
            employee.RoleNames = string.Join(",", GetRoleNamesQuery(id));
            return employee;
        }
        public IEnumerable<EmployeeBO> GetEmployeesGeneralInfo(IEnumerable<Guid> ids)
        {
            var employee = base.GetByIds(ids);
            var empTitleIds = ServiceFactory<EmployeeTitleService>().GetQuery().Where(i => ids.Contains(i.EmployeeId)).Select(i => i.TitleId);
            return employee.AsEnumerable().Select(
              i =>
              {
                  i.RoleNames = string.Join(",", GetRoleNamesQuery(i.Id));
                  return i;
              });

        }
        /// <summary>
        /// Lấy danh sách nhân sự
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EmployeeBO> GetAll(int pageIndex, int pageSize, out int totalCount, bool allowGetRoleName = false, bool allowDeleted = false)
        {
            var data = base.GetQuery(allowDeleted: allowDeleted);
            totalCount = data.Count();
            data = base.Page(data, pageIndex, pageSize);
            return data.AsEnumerable().Select(i =>
            {
                i.RoleNames = allowGetRoleName ? string.Join(",", GetRoleNames(i.Id)) : "";
                return i;
            });
        }
        public IEnumerable<EmployeeBO> GetAllHasRole(int pageIndex, int pageSize, out int totalCount, string query = "", bool allowGetRoleName = false)
        {
            try
            {
                //1. Lấy danh sách id nhân sự đã có chức danh
                var employeeIds = ServiceFactory<EmployeeTitleService>().GetQuery().Select(t => t.EmployeeId).Distinct();
                //2. Lấy danh sách nhân sự theo không chứa trong danh sách id vừa lấy ở bước 1 với điều kiện Name hoặc Email hoặc PhoneNumber chứa trong chuổi query
                var data = GetQuery().Where(u => employeeIds.Contains(u.Id));
                if (!string.IsNullOrEmpty(query))
                {
                    data = data.Where(i => !string.IsNullOrEmpty(i.Name) && i.Name.Contains(query));
                }
                totalCount = data.Count();
                var employees = base.Page(data.OrderBy(i => i.Name), pageIndex, pageSize).ToList();
                return employees.Select(i =>
                {
                    i.RoleNames = allowGetRoleName ? string.Join(",", GetRoleNames(i.Id)) : "";
                    return i;
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Lấy nhân sự chưa có chức danh
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeBO> GetAllRoleEmpty(string query = "")
        {
            try
            {
                //1. Lấy danh sách id nhân sự đã có chức danh
                var employeeIds = ServiceFactory<EmployeeTitleService>().GetQuery().Select(t => t.EmployeeId).Distinct();
                //2. Lấy danh sách nhân sự theo không chứa trong danh sách id vừa lấy ở bước 1 với điều kiện Name hoặc Email hoặc PhoneNumber chứa trong chuổi query
                var data = GetQuery().Where(u => !employeeIds.Contains(u.Id));
                if (!string.IsNullOrEmpty(query))
                {
                    data = data.Where(i => !string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(query.ToLower())).Distinct();
                }
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Lấy danh sách nhân sự theo cức danh
        public IEnumerable<EmployeeBO> GetByRoleIDs(IEnumerable<Guid> roleIds)
        {
            var employeeIds = ServiceFactory<EmployeeTitleService>().GetEmployeeIDsByRoleIDs(roleIds);
            var data = base.GetQuery().Where(u => employeeIds.Contains(u.Id)).AsEnumerable();
            return data;
        }
        public IEnumerable<EmployeeBO> GetByDepartmentIndex(Guid departmentId, string query = "", bool allowGetRoleName = false, List<string> lstEmployeeTitles = null)
        {
            try
            {
                var tittles = ServiceFactory<DepartmentTitleService>().GetQuery().Where(i => i.DepartmentId == departmentId).Select(i => i.Id);
                var employeeTitles = ServiceFactory<EmployeeTitleService>().GetQuery().Where(i => tittles.Contains(i.TitleId));
                if (lstEmployeeTitles != null && lstEmployeeTitles.Count > 0)
                    employeeTitles = employeeTitles.Where(i => lstEmployeeTitles.Contains(i.TitleId.ToString())).Distinct();
                var employeeIds = employeeTitles.Select(e => e.EmployeeId);
                var data =
                   base.GetQuery()
                   .Where(i => employeeIds.Contains(i.Id))
                      .Where(t => (string.IsNullOrEmpty(query))
                      || (!string.IsNullOrEmpty(t.Name) && t.Name.ToLower().Contains(query.ToLower()))
                      || (!string.IsNullOrEmpty(t.Email) && t.Email.ToLower().Contains(query.ToLower()))
                      || (!string.IsNullOrEmpty(t.PhoneNumber) && t.PhoneNumber.Contains(query))).Distinct();
                return data.ToList().Select(i =>
                {
                    i.RoleNames = allowGetRoleName ? string.Join(",", GetRoleNames(i.Id)) : "";
                    return i;
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<EmployeeBO> GetAllByDepartment(int pageIndex, int pageSize, out int totalCount, Guid departmentId, string query = "", bool allowGetRoleName = false)
        {
            try
            {
                var tittles = ServiceFactory<DepartmentTitleService>().GetQuery().Where(i => i.DepartmentId == departmentId).Select(i => i.Id);
                var employeeTitles = ServiceFactory<EmployeeTitleService>().GetQuery().Where(i => tittles.Contains(i.TitleId));
                var employeeIds = employeeTitles.Select(e => e.EmployeeId);
                List<EmployeeBO> data =
                   base.GetQuery()
                   .Where(i => employeeIds.Contains(i.Id))
                      .Where(t => (string.IsNullOrEmpty(query))
                      || (!string.IsNullOrEmpty(t.Name) && t.Name.ToLower().Contains(query.ToLower()))
                      || (!string.IsNullOrEmpty(t.Email) && t.Email.ToLower().Contains(query.ToLower()))
                      || (!string.IsNullOrEmpty(t.PhoneNumber) && t.PhoneNumber.Contains(query))).ToList();
                totalCount = data.Count();


                //1. Lấy danh sách id nhân sự đã có chức danh
                var employeeIdEmtryRole = ServiceFactory<EmployeeTitleService>().GetQuery().Select(t => t.EmployeeId).Distinct();
                //2. Lấy danh sách nhân sự theo không chứa trong danh sách id vừa lấy ở bước 1 với điều kiện Name hoặc Email hoặc PhoneNumber chứa trong chuổi query
                List<EmployeeBO> dataEmtryRole = GetQuery().Where(u => !employeeIdEmtryRole.Contains(u.Id)).ToList();
                if (!string.IsNullOrEmpty(query))
                {
                    dataEmtryRole = dataEmtryRole.Where(i => !string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(query.ToLower())).ToList();
                }
                totalCount += dataEmtryRole.Count();

                data.AddRange(dataEmtryRole);
                var result = base.Page(data.OrderBy(i => i.Name), pageIndex, pageSize).ToList();
                return result.Select(i =>
                {
                    i.RoleNames = allowGetRoleName ? string.Join(",", GetRoleNames(i.Id)) : "";
                    return i;
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<EmployeeBO> GetByDepartment(int pageIndex, int pageSize, out int totalCount, Guid departmentId, string query = "", bool allowGetRoleName = false)
        {
            try
            {
                var department = ServiceFactory<DepartmentService>().GetById(departmentId);
                var tittles = ServiceFactory<DepartmentTitleService>().GetQuery()
                    .Where(i => i.DepartmentId == departmentId
                        || (!department.ParentId.HasValue
                        || department.ParentId == Guid.Empty))
                        .Select(i => i.Id).ToList();
                var employeeTitles = ServiceFactory<EmployeeTitleService>()
                    .GetQuery()
                    .Where(i => tittles.Any(c => c == i.TitleId))
                    .ToList();
                var employeeIds = employeeTitles.Select(e => e.EmployeeId);
                var data = base.GetQuery()
                        .Where(i => employeeIds.Any(c => c == i.Id))
                        .Where(t => (string.IsNullOrEmpty(query))
                      || (!string.IsNullOrEmpty(t.Name) && t.Name.Contains(query))
                      || (!string.IsNullOrEmpty(t.Email) && t.Email.Contains(query))
                      || (!string.IsNullOrEmpty(t.PhoneNumber) && t.PhoneNumber.Contains(query))
                      );
                totalCount = data.Count();
                var result = base.Page(data.OrderBy(i => i.Name), pageIndex, pageSize).ToList();
                return result.Select(i =>
                {
                    i.RoleNames = allowGetRoleName ? string.Join(",", GetRoleNames(i.Id)) : "";
                    return i;
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // Danh sách nhân sự theo phòng ban
        public IEnumerable<EmployeeBO> GetByDepartment(Guid departmentId)
        {
            try
            {
                DataTable data = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_DepartmentTitle_GetEmployeeInfo", parameter: new { departmentId = departmentId });
                List<EmployeeBO> lstEmployee = new List<EmployeeBO>();
                if(data != null && data.Rows.Count > 0)
                {
                    foreach (DataRow item in data.Rows)
                    {
                        EmployeeBO ebo = new EmployeeBO();
                        ebo.Name = item["Name"].ToString();
                        ebo.Email = item["Email"].ToString();
                        ebo.PhoneNumber = item["PhoneNumber"].ToString();
                        ebo.Id = Guid.Parse(item["Id"].ToString());
                        ebo.FileAvatar.Data = Encoding.ASCII.GetBytes(item["Name"].ToString());
                        lstEmployee.Add(ebo);
                    }
                }
                ////bước 1. lấy danh sách id chức danh của phòng ban
                //var roleIds = ServiceFactory<DepartmentTitleService>().GetRoleIdsByDepartment(departmentId);
                ////bước 2. lấy danh sách id nhân sự theo danh sách id chức danh đã lấy ở bước 1
                //var employeeIds = ServiceFactory<EmployeeTitleService>().GetEmployeeIDsByRoleIDs(roleIds);
                ////bước 3. lấy danh sách nhân sự theo danh sách id nhân sự đã lấy ở bước 2
                //var data = base.Get(p => employeeIds.Contains(p.Id));
                return lstEmployee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IEnumerable<EmployeeBO> GetByUsers(IEnumerable<UserBO> users)
        {
            var employees = base.Get().Where(i => users.Any(z => z.Id == i.Id));
            return employees;
        }
        /// <summary>
        /// Người phê duyệt ban hành
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeBO> GetByPublishApprove(Guid categoryId)
        {
            var data = Enumerable.Empty<EmployeeBO>();

            //1. Đưa ra chức danh có quyền phê duyệt ban hành tài liệu
            var category = ServiceFactory<DocumentCategoryService>().GetById(categoryId);
            if (category == null)
                return data;
            //var titleId = category.ApproveRole.Value;
            //2. Lấy ra danh sách nhân sự theo quyền phê duyệt ban hành tài liệu
            // data = GetByRoleIDs(new Guid[] { titleId });
            data = GetByRoleIDs(new Guid[] { });
            return data;
        }

        /// <summary>
        /// Get employee (resource) by task resourceID
        /// </summary>
        /// <returns></returns>
        public EmployeeBO GetByTaskResourceID(Guid taskResourceID)
        {
            var taskResource = ServiceFactory<TaskResourceService>().GetById(taskResourceID);
            return GetById(taskResource.EmployeeId.Value);
        }
        /// <summary>
        /// danh sách nhân sự có quyền kiểm duyệt tài liệu
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeBO> GetByReviewApprove(Guid categoryId)
        {
            var data = Enumerable.Empty<EmployeeBO>();

            //1. Đưa ra chức danh có quyền phê duyệt ban hành tài liệu
            var category = ServiceFactory<DocumentCategoryService>().GetById(categoryId);
            if (category == null)
                return data;
            // var titleId = category.ReviewRole.Value;
            //2. Lấy ra danh sách nhân sự theo quyền phê duyệt ban hành tài liệu
            //data = GetByRoleIDs(new Guid[] { titleId }
            //    );
            data = GetByRoleIDs(new Guid[] { }
                );
            return data;
        }
        /// <summary>
        /// Danh sách nhân sự có vai trò soạn thảo
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeBO> GetByPerformWrite(Guid categoryId)
        {
            var data = Enumerable.Empty<EmployeeBO>();

            //1. Đưa ra chức danh có quyền phê duyệt ban hành tài liệu
            var category = ServiceFactory<DocumentCategoryService>().GetById(categoryId);
            if (category == null)
                return data;
            // var titleId = category.WriteRole.Value;
            //2. Lấy ra danh sách nhân sự theo quyền phê duyệt ban hành tài liệu
            data = GetByRoleIDs(new Guid[] { });
            //data = GetByRoleIDs(new Guid[] { titleId });
            return data;
        }
        /// <summary>
        /// Đưa ra danh sách nhân sự có vai trò lưu trữ tài liệu
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeBO> GetByArchiveRole(Guid categoryId)
        {
            var data = Enumerable.Empty<EmployeeBO>();

            //1. Đưa ra chức danh có quyền phê duyệt ban hành tài liệu
            var category = ServiceFactory<DocumentCategoryService>().GetById(categoryId);
            if (category == null)
                return data;
            // var titleId = category.ArchiveRole.Value;
            //2. Lấy ra danh sách nhân sự theo quyền phê duyệt ban hành tài liệu
            data = GetByRoleIDs(new Guid[] { });
            //  data = GetByRoleIDs(new Guid[] { titleId });
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="query"></param>
        /// <param name="departmentId"></param>
        /// <param name="process"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeBO> GetEmployeeInProcessDocument(int pageIndex, int pageSize, out int totalCount, string query, string departmentId = default(string), int process = 0, int order = 0, int? roleType = 0)
        {
            var employeeIds = new List<Guid>();
            var data = Enumerable.Empty<EmployeeBO>();
            if (ServiceFactory<DocumentProcessService>().CheckExitProcess(new Guid(departmentId), (int)process))
            {
                if (roleType.HasValue && roleType.Value == (int)iDAS.Service.Common.Resource.DocumentProcessRole.Write)
                {
                    data = base.Get(i => string.IsNullOrEmpty(query) || (!string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(query.ToLower())));
                }
                else
                {
                    if (process == (int)iDAS.Service.Common.Resource.DocumentTypeProcess.CancelSuggest
                        || process == (int)iDAS.Service.Common.Resource.DocumentTypeProcess.DistributeSuggest)
                    {
                        process = (int)iDAS.Service.Common.Resource.DocumentTypeProcess.WriteAndRelation;
                    }
                    var processItem = ServiceFactory<DocumentProcessService>().GetByOrder(new Guid(departmentId), (int)process, order);
                    if (processItem.EmployeeId.HasValue && processItem.EmployeeId != Guid.Empty)
                        employeeIds.Add(processItem.EmployeeId.Value);
                    else if (processItem.DepartmentTitleId.HasValue && processItem.DepartmentTitleId != Guid.Empty)
                        employeeIds.AddRange(ServiceFactory<EmployeeTitleService>().Get(t => t.TitleId == processItem.DepartmentTitleId.Value).Select(t => t.EmployeeId).Distinct());
                    data = base.GetByIds(employeeIds).Where(i => string.IsNullOrEmpty(query) || (!string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(query.ToLower())));
                }
            }
            else
            {
                data = base.Get(i => string.IsNullOrEmpty(query) || (!string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(query.ToLower())));
            }
            totalCount = data.Count();
            var employees = base.Page(data, pageIndex, pageSize);
            var empIds = employees.Select(i => i.Id);
            var empTitles = ServiceFactory<EmployeeTitleService>().GetRoleIdsByEmployeeIds(empIds);
            var users = ServiceFactory<UserService>().GetByIds(employees.Select(i => i.Id));
            employees = employees.Select(i =>
            {
                var titleIds = empTitles.Where(u => u.EmployeeId == i.Id).Select(u => u.TitleId);
                var titleNames = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds).Select(u => u.Name);
                var user = users.FirstOrDefault(u => u.Id == i.Id);
                i.UserName = user == null ? string.Empty : user.UserName;
                i.RoleNames = string.Join(",", titleNames);
                return i;
            });
            return employees;
        }
        public IEnumerable<EmployeeBO> GetEmployeeByRoleType(int pageIndex, int pageSize, out int totalCount, string query, int roleType = 0)
        {
            try
            {
                var employeeIds = new List<Guid>();
                var data = Enumerable.Empty<EmployeeBO>();
                var processItems = ServiceFactory<DocumentProcessService>().GetQuery().Where(t => t.TypeRole == roleType);
                foreach (var processItem in processItems)
                {
                    if (processItem.EmployeeId.HasValue && processItem.EmployeeId != Guid.Empty)
                        employeeIds.Add(processItem.EmployeeId.Value);
                    else if (processItem.DepartmentTitleId.HasValue && processItem.DepartmentTitleId != Guid.Empty)
                        employeeIds.AddRange(ServiceFactory<EmployeeTitleService>().GetQuery().Where(t => t.TitleId == processItem.DepartmentTitleId.Value).Select(t => t.EmployeeId).Distinct());
                }
                data = base.GetByIds(employeeIds);
                if (!string.IsNullOrEmpty(query))
                {
                    data = data.Where(i => !string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(query.ToLower()));
                }
                totalCount = data.Count();
                var employees = base.Page(data, pageIndex, pageSize);
                var empIds = employees.Select(i => i.Id);
                var empTitles = ServiceFactory<EmployeeTitleService>().GetRoleIdsByEmployeeIds(empIds);
                var titleIds = empTitles.Select(i => i.TitleId);
                var titles = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds);
                var users = ServiceFactory<UserService>().GetByIds(employees.Select(i => i.Id));
                employees = employees.Select(i =>
                {
                    var user = users.FirstOrDefault(u => u.Id == i.Id);
                    i.UserName = user == null ? string.Empty : user.UserName;
                    var employeeTitleIds = empTitles.Where(u => u.EmployeeId == i.Id).Select(u => u.TitleId);
                    var titlesNames = titles.Where(u => employeeTitleIds.Contains(u.Id)).Select(u => u.Name);
                    i.RoleNames = string.Join(",", titlesNames);
                    return i;
                });
                return employees;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Chuyển nhân sự khác thiết lập theo chức danh trong quy trình
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="query"></param>
        /// <param name="roleType"></param>
        /// <param name="typeProcess"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeBO> GetEmployeeByProcess(int pageIndex, int pageSize, out int totalCount, string query, int roleType = 0, int typeProcess = 0, string departmentId = "")
        {
            try
            {
                var employeeIds = new List<Guid>();
                var data = Enumerable.Empty<EmployeeBO>();
                var employees = Enumerable.Empty<EmployeeBO>();
                if (!String.IsNullOrEmpty(departmentId))
                {
                    // Lấy chức danh của người đăng nhập hệ thống
                    var title = ServiceFactory<EmployeeTitleService>().GetTitleIdsByCurrentUser();
                    var processItems = ServiceFactory<DocumentProcessService>().Get(t => t.TypeRole == roleType && t.TypeProcess == typeProcess
                                        && t.DepartmentId == new Guid(departmentId) && title.Contains(t.DepartmentTitleId.Value));
                    if (processItems != null && processItems.Count() > 0)
                    {
                        foreach (var processItem in processItems)
                        {
                            if (processItem.DepartmentTitleId.HasValue && processItem.DepartmentTitleId != Guid.Empty)
                                employeeIds.AddRange(ServiceFactory<EmployeeTitleService>().Get(t => t.TitleId == processItem.DepartmentTitleId.Value).Select(t => t.EmployeeId).Distinct());
                        }
                        data = base.GetByIds(employeeIds);
                        if (!string.IsNullOrEmpty(query))
                        {
                            data = data.Where(i => !string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(query.ToLower()));
                        }
                        //totalCount = data.Count();
                        employees = base.Page(data, pageIndex, pageSize);
                        var empIds = employees.Select(i => i.Id);
                        var empTitles = ServiceFactory<EmployeeTitleService>().GetRoleIdsByEmployeeIds(empIds);
                        var titleIds = empTitles.Select(i => i.TitleId);
                        var titles = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds);
                        var users = ServiceFactory<UserService>().GetByIds(employees.Select(i => i.Id));
                        employees = employees.Select(i =>
                        {
                            var user = users.FirstOrDefault(u => u.Id == i.Id);
                            i.UserName = user == null ? string.Empty : user.UserName;
                            var employeeTitleIds = empTitles.Where(u => u.EmployeeId == i.Id).Select(u => u.TitleId);
                            var titlesNames = titles.Where(u => employeeTitleIds.Contains(u.Id)).Select(u => u.Name);
                            i.RoleNames = string.Join(",", titlesNames);
                            return i;
                        });
                    }
                    else
                    {
                        data = base.Get(i => string.IsNullOrEmpty(query) || (!string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(query.ToLower())));
                    }
                }
                totalCount = data.Count();
                return employees;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Lấy ra danh sách nhân sự có vai trò lưu trữ theo phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeBO> GetArchiveRoleByDepartment(Guid departmentId, string name)
        {
            //1 Kiểm tra xem phòng ban đó có thiết lập hay không?
            // nếu không có thiết lập thì lấy tất cả danh sách đó
            // nếu có thiết lập thì lấy ra danh sách nhân sự trong thiết lập
            var departmentPermissionData = ServiceFactory<DocumentDepartmentPermissionService>().GetQuery()
                .Where(i => i.DepartmentId.HasValue && i.DepartmentId == departmentId && i.IsArchive == true);
            if (!departmentPermissionData.Any())
            {
                var data = GetByDepartment(departmentId);
                return data;
            }
            else
            {
                // Đưa ra danh sách chức danh đc chọn trong thiết lập
                var titleIds = departmentPermissionData.Where(i => i.DepartmentTitleId.HasValue).Select(i => i.DepartmentTitleId.Value);
                // 
                var employeeByTitles = ServiceFactory<EmployeeTitleService>().GetEmployeeIDsByRoleIDs(titleIds);
                var employeeIds = departmentPermissionData.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value).AsEnumerable();
                // Danh sách id nhân sự 
                employeeIds = employeeIds.Concat(employeeByTitles).Distinct().ToList();
                var titles = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds);
                var employeeTitles = ServiceFactory<EmployeeTitleService>().GetRoleIdsByEmployeeIds(employeeIds);
                var users = ServiceFactory<UserService>().GetByIds(employeeIds);
                var data = GetByIds(employeeIds).Where(i => string.IsNullOrEmpty(name) || (!string.IsNullOrEmpty(i.Name) && i.Name.Contains(name)));
                data = data.Select(i =>
                {
                    var employeeTitleIds = employeeTitles.Where(u => u.EmployeeId == i.Id).Select(u => u.TitleId);
                    var titlesNames = ServiceFactory<DepartmentTitleService>().GetByIds(employeeTitleIds).Select(u => u.Name);
                    i.RoleNames = string.Join(",", titlesNames);
                    return i;
                });
                return data;
            }

        }
        // Lấy chức danh cho nhân sự, có thể mặc định những chức danh để truy vấn
        public IEnumerable<string> GetRoleNames(Guid id)
        {
            var titleIds = ServiceFactory<EmployeeTitleService>().GetQuery().Where(u => u.EmployeeId == id).Select(u => u.TitleId);
            var titlesNames = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds).Select(u => u.Name);
            return titlesNames;
        }
        public IEnumerable<string> GetDeparmentRoleNames(Guid id)
        {
            List<string> listDeparmentRoleNames = new List<string>();
            var titleIds = ServiceFactory<EmployeeTitleService>().GetQuery().Where(u => u.EmployeeId == id).Select(u => u.TitleId);
            var titlesNames = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds);
            foreach(TitleBO item in titlesNames)
            {
                listDeparmentRoleNames.Add(ServiceFactory<DepartmentService>().GetById(item.DepartmentId.Value).Name + " - " + item.Name);
            };
            return listDeparmentRoleNames;
        }
        // Lấy truy vấn chức danh cho nhân sự, có thể mặc định những chức danh để truy vấn
        public IQueryable<string> GetRoleNamesQuery(Guid id)
        {
            var titleIds = ServiceFactory<EmployeeTitleService>().GetQuery().Where(u => u.EmployeeId == id).Select(u => u.TitleId);
            var titlesNames = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds).Select(u => u.Name);
            return titlesNames;
        }

        public string GetRoleNames1(Guid employeeId)
        {
            try
            {
                var titleIds = ServiceFactory<EmployeeTitleService>().GetQuery().Where(u => u.EmployeeId == employeeId).Select(u => u.TitleId);
                var departmentTitles = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds);

                if (departmentTitles == null || departmentTitles.Count() == 0)
                    return string.Empty;

                string result = string.Empty;
                foreach (var departmentTitle in departmentTitles)
                {
                    Guid departmentId = (Guid)departmentTitle.DepartmentId;
                    string departmentName = ServiceFactory<DepartmentService>().GetById(departmentId).Name;
                    if (string.IsNullOrEmpty(result))
                        result = departmentName + " - " + departmentTitle.Name;
                    else
                        result += "<br/>" + departmentName + " - " + departmentTitle.Name;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeBO> GetUserChatList(int start, int limit, out int count, string name = default(string))
        {
            var userIds = ServiceFactory<UserService>().GetQuery().Select(c => c.Id).Except(new[] { UserId });
            var data = base.GetByIds(userIds)
                   .Where(i => string.IsNullOrEmpty(name) || name == default(string) || (!string.IsNullOrEmpty(i.Name) && i.Name.ToUpper().Contains(name.ToUpper())))
                   .OrderBy(c => c.Name);
            count = data.Count();
            var result = data.Skip(start).Take(limit).ToList();
            var users = ServiceFactory<UserService>().GetByIds(result.Select(i => i.Id)).ToList();
            return result.AsEnumerable().Select(i =>
            {
                var user = users.FirstOrDefault(u => u.Id == i.Id);
                i.UserName = user == null ? string.Empty : user.UserName;
                return i;
            });
        }

        public EmployeeBO GetWithLoginInfo(Guid id)
        {
            var employee = base.GetById(id);
            var user = ServiceFactory<UserService>().GetById(id);
            if (user != null)
            {
                employee.UserName = user.UserName;
            }
            return employee;
        }
        public EmployeeBO GetWithLoginInfo()
        {
            return GetWithLoginInfo(UserId);
        }

        public IEnumerable<EmployeeBO> GetData(int pageIndex, int pageSize, out int totalCount, string query = "", bool allowGetRoleName = false)
        {
            try
            {
                var tittles = ServiceFactory<DepartmentTitleService>().GetQuery().Select(i => i.Id);
                var employeeTitles = ServiceFactory<EmployeeTitleService>().GetQuery().Where(i => tittles.Contains(i.TitleId));
                var employeeIds = employeeTitles.Select(e => e.EmployeeId);
                var data =
                   base.GetQuery()
                   .Where(i => employeeIds.Contains(i.Id))
                      .Where(t => (string.IsNullOrEmpty(query))
                      || (!string.IsNullOrEmpty(t.Name) && t.Name.ToLower().Contains(query.ToLower()))
                      || (!string.IsNullOrEmpty(t.Email) && t.Email.ToLower().Contains(query.ToLower()))
                      || (!string.IsNullOrEmpty(t.PhoneNumber) && t.PhoneNumber.Contains(query)))
                      ;

                totalCount = data.Count();
                var result = base.Page(data.OrderBy(i => i.Name), pageIndex, pageSize).ToList();
               // var result = data.OrderBy(x => x.Name).Skip(pageIndex).Take(pageSize == 0 ? 0 : pageSize).ToList();
                //var result = data.OrderBy(x => x.Name).Skip(pageIndex).ToList();
                return result.Select(i =>
                {
                    //i.RoleNames = allowGetRoleName ? string.Join(",", GetRoleNames(i.Id)) : "";
                    i.RoleNames = allowGetRoleName ? GetRoleNames1(i.Id) : "";
                    return i;
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Boolean GetByDepartmentEmployee(Guid departmentId)
        {
            Boolean result = false;
            try
            {
                var tittles = ServiceFactory<DepartmentTitleService>().GetQuery().Where(i => i.DepartmentId == departmentId).Select(i => i.Id);
                var employeeTitles = ServiceFactory<EmployeeTitleService>().GetQuery().Where(i => tittles.Contains(i.TitleId));
                var employeeIds = employeeTitles.Select(e => e.EmployeeId);
                var data =
                   base.GetQuery()
                   .Where(i => employeeIds.Contains(i.Id));

                if (data.Count() > 0) result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}