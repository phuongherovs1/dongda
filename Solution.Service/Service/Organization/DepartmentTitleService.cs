﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class DepartmentTitleService : BaseService<DepartmentTitleDTO, TitleBO>, IDepartmentTitleService
    {
        public override TitleBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id, allowDeleted, allowDefaultIfNull);
            if (data.DepartmentId.HasValue)
            {
                var department = ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value);
                data.DepartmentName = department.Name;
            }
            return data;
        }
        public override Guid Insert(TitleBO dto, bool allowSave = true)
        {
            try
            {
                dto.Order = GetMaxOrderRole(dto.DepartmentId.Value) + 1;
                dto.Id = base.Insert(dto, allowSave);
                return dto.Id;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public int GetMaxOrderRole(Guid departmentId)
        {
            try
            {
                var maxOrder = base.Get(c => c.DepartmentId == departmentId)
                    .OrderByDescending(c => c.Order)
                    .Select(t => t.Order)
                    .FirstOrDefault();
                return maxOrder == null ? 0 : int.Parse(maxOrder.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<TitleBO> GetRolesByDepartment(Guid departmentId)
        {
            try
            {
                var roles = base.GetQuery().Where(c => c.DepartmentId == departmentId && c.IsDelete == false)
                    .OrderBy(t => t.Order);
                return roles;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void UpdateOrder(bool up, Guid roleId)
        {
            try
            {
                var obj = base.GetById(roleId);
                int orderLastOrFirt = 0;
                if (up)
                {
                    orderLastOrFirt = obj.Order.HasValue ? obj.Order.Value - 1 : 0;
                }
                else
                {
                    orderLastOrFirt = obj.Order.HasValue ? obj.Order.Value + 1 : 1;
                }
                var objUpOrDown = base
                    .Get()
                    .Where(c => c.Order == orderLastOrFirt && c.DepartmentId == obj.DepartmentId)
                    .FirstOrDefault();
                if (objUpOrDown != null)
                {
                    objUpOrDown.Order = obj.Order;
                    base.Update(objUpOrDown, false);
                    obj.Order = up ? obj.Order - 1 : obj.Order + 1;
                    base.Update(obj, false);
                    base.SaveTransaction();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public IEnumerable<Guid> GetRoleIds()
        {
            try
            {
                return base.Get()
                    .Select(t => t.Id).AsEnumerable();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public IEnumerable<Guid> GetRoleIdsByDepartment(Guid departmentId)
        {
            try
            {
                return base.Get(c => c.DepartmentId == departmentId)
                    .Select(t => t.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public Guid GetDepartmentByRole(Guid roleId)
        {
            try
            {
                return base.Get(t => t.Id == roleId)
                    .Select(t => t.DepartmentId.HasValue ? t.DepartmentId.Value : new Guid())
                    .FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public IQueryable<Guid> GetDepartmentByRoleIDs(IQueryable<Guid> roleIds)
        {

            try
            {
                return base.GetQuery().Where(i => roleIds.Any(r => r == i.Id))
                .Select(i => i.DepartmentId.HasValue ? i.DepartmentId.Value : Guid.Empty)
                .Distinct();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void DeleteByOrder(Guid Id)
        {
            int Order = base.GetById(Id).Order.Value;
            base.Delete(Id);
            var lst = base.Get(x => x.Order > Order);
            foreach (TitleBO item in lst)
            {
                item.Order -= 1;
                base.Update(item);
            }
        }
    }
}