﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class VPHumanTrainingPractionerService : BaseService<VPHumanTrainingPractionerDTO, VPHumanTrainingPractionerBO>, IVPHumanTrainingPractionerService
    {
        public override VPHumanTrainingPractionerBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var obj = base.GetById(id, allowDeleted, allowDefaultIfNull);
            if (obj.EmployeeId.HasValue)
                obj.Practioner = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(obj.EmployeeId.Value);
            return obj;
        }
        public override System.Guid Insert(iDAS.Service.VPHumanTrainingPractionerBO data, bool allowSave = true)
        {
            data.TimeRegister = DateTime.Now;
            data.EmployeeId = data.Practioner.Id;
            return base.Insert(data, allowSave);
        }
        public override void Update(VPHumanTrainingPractionerBO data, bool allowSave = true)
        {
            data.EmployeeId = data.Practioner.Id;
            base.Update(data, allowSave);
        }
        public IEnumerable<VPHumanTrainingPractionerBO> GetAll(int pageIndex, int pageSize, out int count, Guid detailId)
        {
            var data = base.GetQuery()
                .Where(c => c.VPHumanTrainingPlanDetailId == detailId);
            count = data.Count();
            var results = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            var employeeIds = results.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results
                      .Select(i =>
                      {
                          i.Practioner = employees.FirstOrDefault(u => u.Id == i.EmployeeId);
                          return i;
                      }).ToList();
            return results;
        }
        public bool CheckInvalidEmployees(Guid? employeesId, Guid? detailId)
        {
            var obj = base.GetQuery().Where(t => t.EmployeeId == employeesId
                && t.VPHumanTrainingPlanDetailId == detailId
                ).FirstOrDefault();
            if (obj != null)
                return true;
            return false;
        }
        public bool CheckOverNumber(Guid? detailId, int? number)
        {
            var obj = base.GetQuery().Where(
                t => t.VPHumanTrainingPlanDetailId == detailId
                ).ToList();
            if (obj.Count >= number)
                return true;
            return false;
        }
        public void InsertObject(string stringId, Guid detailId)
        {
            var ids = stringId.Split(',').Select(n => Guid.Parse(n)).ToList();
            var idsExit = base.GetQuery()
                .Where(i => i.VPHumanTrainingPlanDetailId == detailId)
                .Select(item => item.EmployeeId)
                .ToArray();
            List<VPHumanTrainingPractionerBO> objectPractioners = new List<VPHumanTrainingPractionerBO>();
            foreach (var id in ids)
            {
                if (!idsExit.Contains(id))
                {
                    objectPractioners.Add(new VPHumanTrainingPractionerBO
                    {
                        EmployeeId = id,
                        VPHumanTrainingPlanDetailId = detailId,
                        TimeRegister = DateTime.Now,
                        IsRegister = false,
                    });
                };
            }
            base.InsertRange(objectPractioners);
        }
        public void InsertToProfile(Guid id)
        {
            var obj = base.GetById(id);
            var detail = ServiceFactory<VPHumanTrainingPlanDetailService>().GetById(obj.VPHumanTrainingPlanDetailId.Value);
            VPHumanProfileTrainingBO item = new VPHumanProfileTrainingBO();
            item.Certificate = detail.Certificate;
            item.Content = detail.Contents;
            item.HumanEmployeeId = obj.EmployeeId;
            item.EndDate = detail.ToDate;
            item.StartDate = detail.FromDate;
            item.Result = obj.Rank == 1 ? "Giỏi" : obj.Rank == 2 ? "Khá" : obj.Rank == 3 ? "Trung bình" : obj.Rank == 4 ? "Yếu" : obj.Rank == 5 ? "Kém" : string.Empty;
            item.Name = detail.Contents;
            item.Form = detail.Type == true ? "Nội bộ" : "Bên ngoài";
            item.Reviews = obj.CommentOfTeacher;
            ServiceFactory<VPHumanProfileTrainingService>().Insert(item, false);
            obj.IsInProfile = true;
            base.Update(obj, false);
            base.SaveTransaction();
        }
    }
}
