﻿using iDAS.DataAccess;
using System;
using System.Linq;

namespace iDAS.Service
{
    public class VPHumanTrainingPlanRequirementService : BaseService<VPHumanTrainingPlanRequirementDTO, VPHumanTrainingPlanRequirementBO>, IVPHumanTrainingPlanRequirementService
    {

        public void UpdateSelected(Guid id, bool isSelected, Guid planId)
        {
            var obj = base.GetQuery(allowDeleted: false)
                .Where(c => c.VBHumanTrainingPlanId == planId && c.VBHumanTrainingRequirementId == id)
                .FirstOrDefault();
            if (obj != null)
            {
                if (!isSelected)
                    base.Delete(obj.Id);
                else
                {
                    obj.IsDelete = false;
                    base.Update(obj);
                }
            }
            else if (isSelected)
            {
                var trainingPlanRequirement = new VPHumanTrainingPlanRequirementBO();
                trainingPlanRequirement.VBHumanTrainingRequirementId = id;
                trainingPlanRequirement.VBHumanTrainingPlanId = planId;
                base.Insert(trainingPlanRequirement);
            }
        }
    }
}
