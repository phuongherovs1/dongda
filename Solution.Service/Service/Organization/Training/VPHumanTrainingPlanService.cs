﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class VPHumanTrainingPlanService : BaseService<VPHumanTrainingPlanDTO, VPHumanTrainingPlanBO>, IVPHumanTrainingPlanService
    {
        public IEnumerable<VPHumanTrainingPlanBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            var data = base.GetQuery();
            count = data.Count();
            var results = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            var employeeIds = results.Where(i => i.ApprovalBy.HasValue).Select(i => i.ApprovalBy.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results
                      .Select(i =>
                      {
                          i.EmployeeApproval = employees.FirstOrDefault(u => u.Id == i.ApprovalBy);
                          return i;
                      }).ToList();
            return results;
        }
        public override VPHumanTrainingPlanBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var obj = base.GetById(id, allowDeleted, allowDefaultIfNull);
            if (obj.ApprovalBy.HasValue)
                obj.EmployeeApproval = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(obj.ApprovalBy.Value);
            return obj;
        }
        public void SendApproval(VPHumanTrainingPlanBO data)
        {
            data.ApprovalBy = data.EmployeeApproval.Id;
            base.Update(data);
        }


        public void Approval(VPHumanTrainingPlanBO data)
        {
            data.IsAccept = data.IsAccept;
            data.IsApproval = true;
            data.Note = data.Note;
            data.ApprovalAt = DateTime.Now;
            base.Update(data);
        }


        public IEnumerable<VPHumanTrainingPlanBO> GetPlanIsAccept(int pageIndex, int pageSize, out int count)
        {
            var data = base.GetQuery().Where(c => c.IsAccept == true);
            count = data.Count();
            var results = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            var employeeIds = results.Where(i => i.ApprovalBy.HasValue).Select(i => i.ApprovalBy.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results
                      .Select(i =>
                      {
                          i.EmployeeApproval = employees.FirstOrDefault(u => u.Id == i.ApprovalBy);
                          return i;
                      }).ToList();
            return results;
        }
    }
}
