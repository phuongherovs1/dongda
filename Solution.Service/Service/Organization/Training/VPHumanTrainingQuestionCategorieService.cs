﻿using iDAS.DataAccess;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class VPHumanTrainingQuestionCategorieService : BaseService<VPHumanTrainingQuestionCategoryDTO, VPHumanTrainingQuestionCategorieBO>, IVPHumanTrainingQuestionCategorieService
    {

        public IEnumerable<VPHumanTrainingQuestionCategorieBO> GetQuestionCategory(int pageIndex, int pageSize, out int totalCount)
        {

            var data = base.GetQuery().OrderByDescending(i => i.CreateAt);
            totalCount = data.Count();
            var results = base.Page(data, pageIndex, pageSize).ToList();
            return results;
        }
    }
}
