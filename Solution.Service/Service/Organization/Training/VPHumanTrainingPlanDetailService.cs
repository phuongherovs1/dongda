﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class VPHumanTrainingPlanDetailService : BaseService<VPHumanTrainingPlanDetailDTO, VPHumanTrainingPlanDetailBO>, IVPHumanTrainingPlanDetailService
    {

        public IEnumerable<VPHumanTrainingPlanDetailBO> GetAll(int pageIndex, int pageSize, out int count, Guid planId)
        {
            var data = base.GetQuery().Where(c => c.VPHumanTrainingPlanId == planId);
            count = data.Count();
            var results = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            results = results.AsEnumerable().Select(i =>
            {
                var NumberAssign = ServiceFactory<VPHumanTrainingPractionerService>().Get(c => c.VPHumanTrainingPlanDetailId == i.Id);
                if (NumberAssign != null)
                    i.NumberAssign = NumberAssign.ToList().Count;
                return i;
            }).ToList();
            return results;
        }


        public IEnumerable<VPHumanTrainingPlanDetailBO> GetResultTraining(int pageIndex, int pageSize, out int count, Guid planId)
        {
            var data = base.GetQuery().Where(c => c.VPHumanTrainingPlanId == planId && c.IsCancel != true);
            count = data.Count();
            var results = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return results;
        }
    }
}
