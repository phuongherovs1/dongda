﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class VPHumanTrainingQuestionService : BaseService<VPHumanTrainingQuestionDTO, VPHumanTrainingQuestionBO>, IVPHumanTrainingQuestionService
    {

        public VPHumanTrainingQuestionBO CreateDefaut(Guid Id)
        {
            var data = ServiceFactory<VPHumanTrainingQuestionCategorieService>().GetById(Id);
            var item = new VPHumanTrainingQuestionBO()
            {
                NameQuestionCategory = data.Contents,
                VPHumanTrainingQuestionId = Id
            };
            return item;
        }
        public VPHumanTrainingQuestionBO ViewDetalQuest(Guid Id)
        {
            var data = base.GetById(Id);
            var item = ServiceFactory<VPHumanTrainingQuestionCategorieService>().GetById(data.VPHumanTrainingQuestionId.Value);
            data.NameQuestionCategory = item.Contents;
            return data;
        }


        public IEnumerable<VPHumanTrainingQuestionBO> GetQuestion(int pageIndex, int pageSize, out int totalCount, Guid QuestionCategorId)
        {

            var data = base.GetQuery().Where(i => i.VPHumanTrainingQuestionId == QuestionCategorId).OrderByDescending(i => i.CreateAt);
            totalCount = data.Count();
            var results = base.Page(data, pageIndex, pageSize).ToList();
            return results;
        }
    }
}
