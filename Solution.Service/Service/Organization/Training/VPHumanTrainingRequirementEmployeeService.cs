﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class VPHumanTrainingRequirementEmployeeService : BaseService<VPHumanTrainingRequirementEmployeeDTO, VPHumanTrainingRequirementEmployeeBO>, IVPHumanTrainingRequirementEmployeeService
    {
        public override VPHumanTrainingRequirementEmployeeBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var obj = base.GetById(id, allowDeleted, allowDefaultIfNull);
            if (obj.EmployeeId.HasValue)
                obj.Member = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(obj.EmployeeId.Value);
            return obj;
        }
        public override System.Guid Insert(iDAS.Service.VPHumanTrainingRequirementEmployeeBO data, bool allowSave = true)
        {
            data.EmployeeId = data.Member.Id;
            return base.Insert(data, allowSave);
        }
        public override void Update(VPHumanTrainingRequirementEmployeeBO data, bool allowSave = true)
        {
            data.EmployeeId = data.Member.Id;
            base.Update(data, allowSave);
        }
        public IEnumerable<VPHumanTrainingRequirementEmployeeBO> GetAll(int pageIndex, int pageSize, out int count, Guid requirementId)
        {
            var data = base.GetQuery().Where(c => c.VPHumanTrainingRequirementId == requirementId);
            count = data.Count();
            var results = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            var employeeIds = results.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results
                      .Select(i =>
                      {
                          i.Member = employees.FirstOrDefault(u => u.Id == i.EmployeeId);
                          return i;
                      }).ToList();
            return results;
        }
    }
}
