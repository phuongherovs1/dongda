﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class VPHumanTrainingRequirementService : BaseService<VPHumanTrainingRequirementDTO, VPHumanTrainingRequirementBO>, IVPHumanTrainingRequirementService
    {
        public override VPHumanTrainingRequirementBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var obj = base.GetById(id, allowDeleted, allowDefaultIfNull);
            if (obj.EmployeeId.HasValue)
                obj.RequireBy = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(obj.EmployeeId.Value);
            if (obj.ApprovalBy.HasValue)
                obj.EmployeeApproval = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(obj.ApprovalBy.Value);
            return obj;
        }
        public override Guid Insert(VPHumanTrainingRequirementBO data, bool allowSave = true)
        {
            data.EmployeeId = data.RequireBy.Id;
            return base.Insert(data, allowSave);
        }
        public override void Update(VPHumanTrainingRequirementBO data, bool allowSave = true)
        {
            if (data.RequireBy != null)
                data.EmployeeId = data.RequireBy.Id;
            base.Update(data, allowSave);
        }
        public IEnumerable<VPHumanTrainingRequirementBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            var data = base.GetQuery();
            count = data.Count();
            var results = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            var employeeIds = results.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results
                      .Select(i =>
                      {
                          i.RequireBy = employees.FirstOrDefault(u => u.Id == i.EmployeeId);
                          return i;
                      }).ToList();
            return results;
        }

        public void SendApproval(VPHumanTrainingRequirementBO data)
        {
            data.ApprovalBy = data.EmployeeApproval.Id;
            base.Update(data);
        }
        public void Approval(VPHumanTrainingRequirementBO data)
        {
            data.IsAccept = data.IsAccept;
            data.IsApproval = true;
            data.Note = data.Note;
            data.ApprovalAt = DateTime.Now;
            base.Update(data);
        }


        public IEnumerable<VPHumanTrainingRequirementBO> GetByPlan(int pageIndex, int pageSize, out int count, Guid planId)
        {

            var data = base.GetQuery()
                .Where(c => c.IsAccept == true);
            count = data.Count();
            var results = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            var employeeIds = results.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results
                      .Select(i =>
                      {
                          i.RequireBy = employees.FirstOrDefault(u => u.Id == i.EmployeeId);
                          i.IsSelected = ServiceFactory<VPHumanTrainingPlanRequirementService>().GetQuery().Any(c => c.VBHumanTrainingPlanId == planId && c.VBHumanTrainingRequirementId == i.Id);
                          return i;
                      }).ToList();
            return results;
        }


        public IEnumerable<VPHumanTrainingRequirementBO> GetRequirementByPlan(int pageIndex, int pageSize, out int count, Guid planId)
        {
            var reuirementIds = ServiceFactory<VPHumanTrainingPlanRequirementService>().GetQuery().Where(c => c.VBHumanTrainingPlanId == planId && c.VBHumanTrainingRequirementId.HasValue).Select(c => c.VBHumanTrainingRequirementId).ToList();
            var data = base.GetQuery().Where(c => reuirementIds.Any(i => i == c.Id))
               .Where(c => c.IsAccept == true);
            count = data.Count();
            var results = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            var employeeIds = results.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results
                      .Select(i =>
                      {
                          i.RequireBy = employees.FirstOrDefault(u => u.Id == i.EmployeeId);
                          return i;
                      }).ToList();
            return results;
        }
    }
}
