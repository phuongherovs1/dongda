﻿using iDAS.ADO;
using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
namespace iDAS.Service
{
    public class DepartmentService : BaseService<DepartmentDTO, DepartmentBO>, IDepartmentService
    {
        public DepartmentBO GetDepartmentDetail(Guid id, bool checkParent = false)
        {
            var department = base.GetById(id);
            if (checkParent)
                department.IsParent = GetQuery().Any(t => t.ParentId == id);
            return department;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<DepartmentBO> GetTreeDepartment(Guid? id)
        {
            try
            {
                //1. Lấy và trả ra danh sách phòng ban có parent Id bằng departmentId
                return GetQuery().Where(i => i.ParentId == id)
                    .AsEnumerable()
                    .Select(i =>
                    {
                        i.IsParent = Get().Any(t => t.ParentId == i.Id);
                        return i;
                    }).OrderBy(x => x.Name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<DepartmentBO> GetTreeDepartmentChecked(Guid? id, Guid? programId)
        {
            try
            {
                return GetQuery()
                    .Where(i => i.ParentId == id)
                    .AsEnumerable()
                    .Select(i =>
                    {
                        i.IsParent = Get().Any(t => t.ParentId == i.Id);
                        i.Checked = ServiceFactory<QualityAuditProgramDepartmentService>().GetQuery().Any(c => c.QualityAuditProgramId == programId && c.DepartmentId == i.Id);
                        return i;
                    });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// get phong ban phan phoi
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<DepartmentBO> GetDepartmentDistribute(Guid Id)
        {
            try
            {
                var distribute = ServiceFactory<DocumentDistributeService>().Get(i => i.DocumentId == Id && i.DepartmentId != null).Select(p => p.DepartmentId).Distinct();
                var Department = base.GetAll();
                var data = Department.Where(p => distribute.Contains(p.Id));
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<Guid> GetDepartmentIdsByEmployeeID(Guid employeeId)
        {
            try
            {
                //bước 1. Lấy danh sách chức danh của nhân sự: employeeId
                var roleIds = ServiceFactory<EmployeeTitleService>().GetRolesByEmployeeID(employeeId).Select(t => t.TitleId);

                //bước 2. Lấy danh sách phòng ban theo danh sách chức danh đã lấy ở bước 1
                var departmentIds = ServiceFactory<DepartmentTitleService>().GetDepartmentByRoleIDs(roleIds);
                return departmentIds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public IEnumerable<DepartmentBO> GetDepartmentsByEmployeeID(Guid employeeId)
        {
            try
            {
                ////bước 1. Lấy danh sách chức danh của nhân sự: employeeId
                //var roleIds = ServiceFactory<EmployeeTitleService>().GetRolesByEmployeeID(employeeId, allowGetRoleName: false).Select(t => t.TitleId);

                ////bước 2. Lấy danh sách phòng ban theo danh sách chức danh đã lấy ở bước 1
                //var departmentIds = ServiceFactory<DepartmentTitleService>().GetDepartmentByRoleIDs(roleIds);
                DataTable DataID = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Department_GetParentId");
                List<Guid> lstGuidDepartment = new List<Guid>();
                if (DataID != null && DataID.Rows.Count > 0)
                {
                    foreach (DataRow item in DataID.Rows)
                    {
                        lstGuidDepartment.Add(Guid.Parse(item["ParentId"].ToString()));
                    }
                }

                DataTable dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_DepartmentTitle_GetDepartmentInfo", parameter: new { employeeId = employeeId });
                List<Guid> lstGuid = new List<Guid>();
                if(dataTable != null && dataTable.Rows.Count > 0)
                {
                    foreach (DataRow item in dataTable.Rows)
                    {
                        lstGuid.Add(Guid.Parse(item["DepartmentId"].ToString()));
                    }
                }
                //bước 3. Lấy danh sách phòng ban theo danh sách id phòng ban đã lấy ở bước 2
                var departments = GetByIds(lstGuid).AsEnumerable()
                    .Select(i =>
                    {
                        i.IsParent = lstGuidDepartment.Any(t => t == i.Id);
                        return i;
                    });

                //bước 4. Trả ra danh sách phòng ban đã lấy ở bước 3
                return departments;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<DepartmentBO> GetDepartmentsByTitleID(Guid titleId)
        {
            try
            {
                ////bước 1. Lấy danh sách chức danh của nhân sự: employeeId
                //var roleIds = ServiceFactory<EmployeeTitleService>().GetRolesByEmployeeID(employeeId, allowGetRoleName: false).Select(t => t.TitleId);

                ////bước 2. Lấy danh sách phòng ban theo danh sách chức danh đã lấy ở bước 1
                //var departmentIds = ServiceFactory<DepartmentTitleService>().GetDepartmentByRoleIDs(roleIds);
                DataTable DataID = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Department_GetParentId");
                List<Guid> lstGuidDepartment = new List<Guid>();
                if (DataID != null && DataID.Rows.Count > 0)
                {
                    foreach (DataRow item in DataID.Rows)
                    {
                        lstGuidDepartment.Add(Guid.Parse(item["ParentId"].ToString()));
                    }
                }

                DataTable dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_DepartmentTitle_GetDepartmentInfo_ByTitleID", parameter: new { id = titleId });
                List<Guid> lstGuid = new List<Guid>();
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    foreach (DataRow item in dataTable.Rows)
                    {
                        lstGuid.Add(Guid.Parse(item["DepartmentId"].ToString()));
                    }
                }
                //bước 3. Lấy danh sách phòng ban theo danh sách id phòng ban đã lấy ở bước 2
                var departments = GetByIds(lstGuid).AsEnumerable()
                    .Select(i =>
                    {
                        i.IsParent = lstGuidDepartment.Any(t => t == i.Id);
                        return i;
                    });

                //bước 4. Trả ra danh sách phòng ban đã lấy ở bước 3
                return departments;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<DepartmentBO> GetFullDepartmentsByID(Guid Departmentid)
        {
            try
            {
                DataTable DataID = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Department_GetParentId");
                List<Guid> lstGuidDepartment = new List<Guid>();
                if (DataID != null && DataID.Rows.Count > 0)
                {
                    foreach (DataRow item in DataID.Rows)
                    {
                        lstGuidDepartment.Add(Guid.Parse(item["ParentId"].ToString()));
                    }
                }
                List<Guid> lstGuid = new List<Guid>();
                lstGuid.Add(Departmentid);
                //bước 3. Lấy danh sách phòng ban theo danh sách id phòng ban đã lấy ở bước 2
                var departments = GetByIds(lstGuid).AsEnumerable()
                    .Select(i =>
                    {
                        i.IsParent = lstGuidDepartment.Any(t => t == i.Id);
                        return i;
                    });

                //bước 4. Trả ra danh sách phòng ban đã lấy ở bước 3
                return departments;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DepartmentBO> GetDepartmentsByCurrentUser()
        {
            return GetDepartmentsByEmployeeID(UserId);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<DepartmentBO> GetChildrenByDepartment(Guid id)
        {
            try
            {
                var departments = new List<DepartmentBO>();
                //1. Lấy phòng ban theo id
                var children = base.Get(u => u.ParentId == id);
                //2. Nếu phòng ban khác null thì lấy danh sách các phòng ban con theo phòng ban đã lấy ở bước 1 ngược lại không làm gì
                if (children != null && children.Count() > 0)
                {
                    foreach (var child in children)
                    {
                        departments.AddRange(GetChildrenByDepartment(child.Id));
                    }
                    departments.AddRange(children);
                }

                //3. trả ra danh sách phòng ban đã lấy ở bước 2
                return departments.Distinct();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public override Guid Insert(DepartmentBO data, bool allowSave = true)
        {
            data.Level = GetLevel(data.ParentId);
            return base.Insert(data, allowSave);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        public override void Update(DepartmentBO data, bool allowSave = true)
        {
            data.Level = GetLevel(data.ParentId);
            base.Update(data, allowSave);
        }

        /// <summary>
        /// Get level by parent ID
        /// </summary>
        /// <param name="parentID"></param>
        /// <returns></returns>
        private int GetLevel(Guid? parentID)
        {
            var level = 0;
            if (parentID != null)
            {
                var parent = this.GetById(parentID.Value);
                level = parent.Level.Value + 1;
            }
            return level;
        }
        public void GetParentByDepartment(Guid id, ref List<Guid> departmentIds)
        {
            var departments = getParentByDepartment(id);
            if (departments == null || !departments.ParentId.HasValue) return;
            departmentIds.Add(departments.ParentId.Value);
            GetParentByDepartment(departments.ParentId.Value, ref departmentIds);
        }
        private DepartmentBO getParentByDepartment(Guid departmentId)
        {
            var groups = base.GetById(departmentId);
            return groups;
        }
        public DepartmentBO GetFormItem(string id, string parentId)
        {
            DepartmentBO department;
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    department = new DepartmentBO()
                    {
                        ParentId = string.IsNullOrEmpty(parentId) ? (Guid?)null : new Guid(parentId)
                    };
                }
                else
                {
                    department = GetById(new Guid(id));
                }
                return department;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public bool CheckHasRoot()
        {
            return Get(i => !i.ParentId.HasValue || i.ParentId == Guid.Empty).Any();
        }
        public IEnumerable<OrganizationTreeBO> GetByNode(string node, bool showTitle = false, bool showEmployee = false, string onlyTitle = default(string))
        {
            var data = new List<OrganizationTreeBO>();
            try
            {
                if (string.IsNullOrEmpty(node) || node == "root")
                {
                    var children = GetTreeDepartment(null);
                    foreach (var child in children)
                    {
                        var treeNode = new OrganizationTreeBO()
                        {
                            Id = string.Format("{0}_{1}", Common.Resource.OrganizationTreeType.Department, child.Id),
                            Name = child.Name,
                            AvatarUrl = string.Empty,
                            Type = Common.Resource.OrganizationTreeType.Department,
                            IsLeaf = !(base.GetQuery().Any(c => c.ParentId == child.Id) || (showTitle && ServiceFactory<DepartmentTitleService>().GetQuery().Any(c => c.DepartmentId == child.Id)))
                        };
                        data.Add(treeNode);
                    }
                }
                else
                {
                    var nodeData = node.Split('_');
                    if (nodeData != null && nodeData.Length >= 2)
                    {
                        Guid nodeId = Common.Utilities.ConvertToGuid(nodeData[1]);
                        if (nodeData[0] == "Department")
                        {
                            var children = string.IsNullOrEmpty(node) ? GetTreeDepartment(null) : GetTreeDepartment(nodeId).ToList();
                            foreach (var child in children)
                            {
                                var treeNode = new OrganizationTreeBO()
                                {
                                    Id = string.Format("{0}_{1}", Common.Resource.OrganizationTreeType.Department, child.Id),
                                    Name = child.Name,
                                    AvatarUrl = string.Empty,
                                    Type = Common.Resource.OrganizationTreeType.Department,
                                    IsLeaf = !(base.GetQuery().Any(c => c.ParentId == child.Id) || (showTitle && ServiceFactory<DepartmentTitleService>().GetQuery().Any(c => c.DepartmentId == child.Id)))
                                };
                                data.Add(treeNode);
                            }
                            if (showTitle)
                            {
                                var filterTitle = new List<Guid>();
                                if (onlyTitle != default(string))
                                {
                                    var filterTitleArr = onlyTitle.Trim(',').Split(',').ToList();
                                    if (filterTitleArr != null && filterTitleArr.Count > 0)
                                        foreach (var item in filterTitleArr)
                                        {
                                            filterTitle.Add(new Guid(item));
                                        }
                                }
                                var titles = ServiceFactory<DepartmentTitleService>().GetRolesByDepartment(nodeId)
                                    .Where(i => filterTitle != null && filterTitle.Count > 0 ? filterTitle.Contains(i.Id) : true)
                                    .ToList();
                                foreach (var title in titles)
                                {
                                    var treeNode = new OrganizationTreeBO()
                                    {
                                        Id = string.Format("{0}_{1}", Common.Resource.OrganizationTreeType.Title, title.Id),
                                        Name = title.Name,
                                        AvatarUrl = string.Empty,
                                        Type = Common.Resource.OrganizationTreeType.Title,
                                        IsLeaf = !(showEmployee && ServiceFactory<EmployeeTitleService>().GetQuery().Any(c => c.TitleId == title.Id))
                                    };
                                    data.Add(treeNode);
                                }
                            }
                        }
                        if (nodeData[0] == "Title" && showEmployee)
                        {
                            var employees = ServiceFactory<EmployeeService>().GetByRoleIDs(new[] { nodeId }).ToList();
                            foreach (var employee in employees)
                            {
                                var treeNode = new OrganizationTreeBO()
                                {
                                    Id = string.Format("{0}_{1}", Common.Resource.OrganizationTreeType.Employee, employee.Id),
                                    Name = employee.Name,
                                    AvatarUrl = employee.AvatarUrl,
                                    Type = Common.Resource.OrganizationTreeType.Employee,
                                    IsLeaf = true
                                };
                                data.Add(treeNode);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            return data;
        }

        /// <summary>
        /// Danh sách phòng ban mà người đăng nhập hệ thống có thể cập nhật tài liệu
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DepartmentBO> GetDepartmentCanUpdateDocument()
        {
            var currentUserTitleIds = ServiceFactory<EmployeeTitleService>().GetTitleIdsByCurrentUser();
            // Danh sách chức danh của người đăng nhập hệ thống
            var currentUserTitles = ServiceFactory<DepartmentTitleService>().GetByIds(currentUserTitleIds);
            //
            var currentDepartmentIds = currentUserTitles.Where(i => i.DepartmentId.HasValue).Select(i => i.DepartmentId.Value);

            var departmentCanUpdate = ServiceFactory<DocumentDepartmentPermissionService>()
                .Get(i => (i.DepartmentTitleId.HasValue && currentUserTitleIds.Contains(i.DepartmentTitleId.Value)) || i.EmployeeId == UserId).Select(i => i.DepartmentId.Value);
            var departmentIds = currentDepartmentIds.Concat(departmentCanUpdate).Distinct();
            return GetByIds(departmentIds);

        }

        /// <summary>
        /// Lấy tất cả phòng ban và phòng ban con của phòng ban người đăng nhập
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public IEnumerable<DepartmentBO> GetDepartmentsAndChildrenByEmployeeID(Guid employeeId)
        {
            try
            {
                var departments = new List<DepartmentBO>();
                //bước 1. Lấy danh sách chức danh của nhân sự: employeeId
                var roleIds = ServiceFactory<EmployeeTitleService>().GetRolesByEmployeeID(employeeId, allowGetRoleName: false).Select(t => t.TitleId);

                //bước 2. Lấy danh sách phòng ban theo danh sách chức danh đã lấy ở bước 1
                var departmentIds = ServiceFactory<DepartmentTitleService>().GetDepartmentByRoleIDs(roleIds);

                //bước 3. Lấy danh sách phòng ban theo danh sách id phòng ban đã lấy ở bước 2
                departments.AddRange(GetByIds(departmentIds).AsEnumerable());
                //bước 4. Lấy phòng ban con theo id danh sách phòng ban ở bước 3
                var departmentChilds = new List<DepartmentBO>();
                if (departments != null && departments.Count > 0)
                {
                    foreach (var i in departments)
                    {
                        var children = base.Get(u => u.ParentId == i.Id);
                        //bước 5. Nếu phòng ban con khác null thì lấy danh sách các phòng ban con theo phòng ban đã lấy ở bước 5 ngược lại không làm gì
                        if (children != null && children.Count() > 0)
                        {
                            foreach (var child in children)
                            {
                                departmentChilds.AddRange(GetChildrenByDepartment(child.Id));
                            }
                            departmentChilds.AddRange(children);
                        }
                    }
                }
                departments.AddRange(departmentChilds);
                //bước 6. trả ra danh sách phòng ban đã lấy ở bước 5
                return departments.Distinct();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}