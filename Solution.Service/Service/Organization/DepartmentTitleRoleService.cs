﻿using iDAS.ADO;
using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace iDAS.Service
{
    public class DepartmentTitleRoleService : BaseService<DepartmentTitleRoleDTO, DepartmentTitleRoleBO>, IDepartmentTitleRoleService
    {

        public IEnumerable<DepartmentTitleRoleBO> GetRolesByTitle(Guid titleId)
        {
            try
            {
                var roles = base.GetAll();
                if (roles != null)
                    return roles.Where(c => c != null && c.TitleId == titleId);
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DepartmentTitleRoleBO GetRole(Guid titleId, Guid departmentId)
        {
            try
            {
                var roles = base.GetAll();
                if (roles != null)
                    return roles.FirstOrDefault(c => c != null && c.TitleId == titleId && c.DepartmentId == departmentId);
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckTitleRole_View(Guid userId)
        {
            EmployeeTitleService ets = new EmployeeTitleService();
            DepartmentService ds = new DepartmentService();
            DepartmentTitleRoleService service = new DepartmentTitleRoleService();
            // danh sách chức danh của user đăng nhập
            var userLogin_TitleIds = ets.GetTitleIdsByCurrentUser().ToList();
            // phòng ban của user có hồ sơ cần xem
            var departments = ds.GetDepartmentsByEmployeeID(userId);
            // kiểm tra quyền theo chức danh
            foreach (var titleId in userLogin_TitleIds)
            {
                foreach (var department in departments)
                {
                    if (service.GetAll().Any(i => i.TitleId == titleId && (i.DepartmentId == department.Id || i.DepartmentId == department.ParentId) && (i.ViewAll || i.UpdateAll)))
                        return true;
                }
            }
            return false;
        }

        public bool CheckTitleRole_Edit(Guid userId)
        {
            EmployeeTitleService ets = new EmployeeTitleService();
            DepartmentService ds = new DepartmentService();
            DepartmentTitleRoleService service = new DepartmentTitleRoleService();
            // danh sách chức danh của user đăng nhập
            var userLogin_TitleIds = ets.GetTitleIdsByCurrentUser().ToList();
            // phòng ban của user có hồ sơ cần xem
            var departments = ds.GetDepartmentsByEmployeeID(userId);
            // kiểm tra quyền theo chức danh
            foreach (var titleId in userLogin_TitleIds)
            {
                foreach (var department in departments)
                {
                    if (service.GetAll().Any(i => i.TitleId == titleId && (i.DepartmentId == department.Id || i.DepartmentId == department.ParentId) && i.UpdateAll))
                        return true;
                }
            }
            return false;
        }
        public bool CheckRoleForTitles_View(Guid userId)
        {
            EmployeeTitleService ets = new EmployeeTitleService();
            DepartmentService ds = new DepartmentService();
            DepartmentTitleRoleService service = new DepartmentTitleRoleService();
            // danh sách chức danh của user đăng nhập
            var userLogin_TitleIds = ets.GetTitleIdsByCurrentUser().ToList();
            // phòng ban của chức danh có hồ sơ cần xem
            var departments = ds.GetDepartmentsByTitleID(userId);
            // kiểm tra quyền theo chức danh
            foreach (var titleId in userLogin_TitleIds)
            {
                foreach (var department in departments)
                {
                    if (service.GetAll().Any(i => i.TitleId == titleId && (i.DepartmentId == department.Id || i.DepartmentId == department.ParentId) && (i.ViewAll || i.UpdateAll)))
                        return true;
                }
            }
            return false;
        }

        public bool CheckRoleForTitles_Edit(Guid userId, Guid? Departmentid = null)
        {
            EmployeeTitleService ets = new EmployeeTitleService();
            DepartmentService ds = new DepartmentService();
            DepartmentTitleRoleService service = new DepartmentTitleRoleService();
            // danh sách chức danh của user đăng nhập
            var userLogin_TitleIds = ets.GetTitleIdsByCurrentUser().ToList();
            // phòng ban của chức danh có hồ sơ cần xem
            var departments = ds.GetDepartmentsByTitleID(userId);
            // kiểm tra quyền theo chức danh
            foreach (var titleId in userLogin_TitleIds)
            {
                foreach (var department in departments)
                {
                    if (service.GetAll().Any(i => i.TitleId == titleId && (i.DepartmentId == department.Id || i.DepartmentId == department.ParentId) && i.UpdateAll))
                        return true;
                }
            }
            return false;
        }
        public bool CheckCreateForTitles(Guid Departmentid)
        {
            EmployeeTitleService ets = new EmployeeTitleService();
            DepartmentService ds = new DepartmentService();
            DepartmentTitleRoleService service = new DepartmentTitleRoleService();
            // danh sách chức danh của user đăng nhập
            var userLogin_TitleIds = ets.GetTitleIdsByCurrentUser().ToList();
            // phòng ban của chức danh có hồ sơ cần xem
            var departments = ds.GetFullDepartmentsByID(Departmentid);
            // kiểm tra quyền theo chức danh
            foreach (var titleId in userLogin_TitleIds)
            {
                foreach (var department in departments)
                {
                    if (service.GetAll().Any(i => i.TitleId == titleId && (i.DepartmentId == department.Id || i.DepartmentId == department.ParentId) && i.UpdateAll))
                        return true;
                }
            }
            return false;
        }
        public Dictionary<Guid, string> GetListDeparment_HasRoleEdit(Guid userId)
        {
            Dictionary<Guid, string> result = new Dictionary<Guid, string>();
            var Department = ServiceFactory<DepartmentService>().GetAll();
            foreach (DepartmentBO item in Department)
            {
                result.Add(item.Id, item.Name);
            }
            //var isAdmin = ServiceFactory<UserRoleService>().RoleExits("Administrator");
            //if (isAdmin)
            //{
            //    var Department = ServiceFactory<DepartmentService>().GetAll();
            //    foreach (DepartmentBO item in Department)
            //    {
            //        result.Add(item.Id, item.Name);
            //    }
            //}
            //else
            //{
            //    List<DictionaryObject> lstObj = new List<DictionaryObject>();
            //    EmployeeTitleService ets = new EmployeeTitleService();
            //    DepartmentService ds = new DepartmentService();
            //    DepartmentTitleRoleService service = new DepartmentTitleRoleService();
            //    // danh sách chức danh của user đăng nhập
            //    var userLogin_TitleIds = ets.GetTitleIdsByCurrentUser().ToList();
            //    // phòng ban của user có hồ sơ cần xem
            //    foreach (var titleId in userLogin_TitleIds)
            //    {
            //        DataTable dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryTable("GetListDeparment_HasRoleEdit",
            //            parameter: new
            //            {
            //                titleId = titleId
            //            }
            //        );
            //        if (dataTable != null && dataTable.Rows.Count > 0)
            //        {
            //            foreach (DataRow item in dataTable.Rows)
            //            {
            //                DictionaryObject objitem = new DictionaryObject();
            //                objitem.id = Guid.Parse(item["id"].ToString());
            //                objitem.name = item["Name"].ToString();
            //                lstObj.Add(objitem);
            //            }
            //        }
            //    }
            //    var lstDepartmentID = ServiceFactory<DepartmentTitleService>().GetByIds(userLogin_TitleIds).Select(x => x.DepartmentId.Value);
            //    var Department = ServiceFactory<DepartmentService>().GetByIds(lstDepartmentID);
            //    foreach (DepartmentBO item in Department)
            //    {
            //        DictionaryObject objitem = new DictionaryObject();
            //        objitem.id = item.Id;
            //        objitem.name = item.Name;
            //        lstObj.Add(objitem);
            //    }
            //    lstObj = lstObj.GroupBy(elem => elem.id).Select(group => group.First()).ToList();
            //    foreach (DictionaryObject item in lstObj)
            //    {
            //        result.Add(item.id, item.name);
            //    }
            //}
            return result;
        }
        public bool CheckRoleForDeparment_Edit(Guid deparment)
        {
            EmployeeTitleService ets = new EmployeeTitleService();
            DepartmentService ds = new DepartmentService();
            DepartmentTitleRoleService service = new DepartmentTitleRoleService();
            // danh sách chức danh của user đăng nhập
            var userLogin_TitleIds = ets.GetTitleIdsByCurrentUser().ToList();
            // phòng ban của user có hồ sơ cần xem
            foreach (var titleId in userLogin_TitleIds)
            {
                DataTable dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryTable("GetListDeparment_HasRoleEdit",
                    parameter: new
                    {
                        titleId = titleId
                    }
                );
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    foreach (DataRow item in dataTable.Rows)
                    {
                        Guid id = Guid.Parse(item["id"].ToString());
                        if (deparment == id)
                            return true;
                    }
                }
            }
            return false;
        }
        public void Update(DepartmentTitleRoleBO de)
        {
            try
            {
                base.Update(de);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Guid Insert(DepartmentTitleRoleBO de)
        {
            try
            {
                return base.Insert(de);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Delete(DepartmentTitleRoleBO de)
        {
            try
            {
                base.Delete(de.Id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
    public class DictionaryObject
    {
        public Guid id { get; set; }
        public string name { get; set; }
    }
}