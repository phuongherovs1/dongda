﻿using iDAS.ADO;
using iDAS.Service.API.TimeKeeping;
using System.Data;

namespace iDAS.Service.Service.TimeKeeping
{
    public class PermissionService : IPermissionService
    {
        public bool Delete(string id)
        {
            return Sql_ADO.idasAdoService.ExecuteNoquery("sp_Timekeeping_Permission_Delete", parameter: new { id = id }) > 0;
        }

        public bool Insert(string id)
        {
            return Sql_ADO.idasAdoService.ExecuteNoquery("sp_Timekeeping_Permission_Insert", parameter: new { id = id }) > 0;
        }

        public DataTable Permission_GetData(int type, string name = "", string Id = "")
        {
            return Sql_ADO.idasAdoService.ExecuteNoqueryTable("Timekeeping_Permission_GetData", parameter: new { Name = name, Type = type, Id = Id });
        }

        public bool Update(string Code, string tMonth, string tDate, string TimeIn, string TimeOut)
        {
            return Sql_ADO.idasAdoService.ExecuteNoquery("TimeKeeping_UPDATE_BATCH", parameter: new { Code = Code, tMonth = tMonth, tDate = tDate, TimeIn = TimeIn, TimeOut = TimeOut }) > 0;
        }
    }
}
