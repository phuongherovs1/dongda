﻿using iDAS.Service.API.TimeKeeping;
using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace iDAS.Service.Service.TimeKeeping
{
    public class FormApproveStatusService : BaseService<FormApproveStatusDTO, FormApproveStatusBO>, IFormApproveStatusService
    {
        public IEnumerable<FormApproveStatusBO> GetByFormId(Guid id)
        {
            return base.GetAll().Where(u => u.FormId == id);
        }

        public IEnumerable<FormApproveStatusBO> GetByType(string tableName)
        {
            return base.GetAll().Where(u => u.TableName == tableName);
        }

    }
}
