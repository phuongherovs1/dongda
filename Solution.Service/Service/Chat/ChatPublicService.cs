﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class ChatPublicService : BaseService<ChatPublicDTO, ChatPublicBO>, IChatPublicService
    {
        public override IEnumerable<ChatPublicBO> GetAll(bool allowDeleted = false)
        {
            var data = base.GetAll(allowDeleted).OrderBy(i => i.CreateAt).AsEnumerable();
            data = data.Select(i =>
            {
                var emp = ServiceFactory<EmployeeService>().GetById(i.From.Value);
                i.FromName = emp.Name;
                i.FromAvatarUrl = emp.AvatarUrl;
                i.UserId = UserId;
                return i;
            });
            return data;
        }

        public Guid NewMessage(string content)
        {
            var data = new ChatPublicBO { Content = content, From = UserId };
            data.Id = base.Insert(data);
            return data.Id;
        }
    }
}
