﻿using iDAS.DataAccess;
using System;
using System.Linq;
namespace iDAS.Service
{
    public class ChatUserOnlineService : BaseService<ChatUserOnlineDTO, ChatUserOnlineBO>, IChatUserOnlineService
    {
        public void OnConnected(string username)
        {
            try
            {
                var userIds = base.GetQuery().Where(i => !string.IsNullOrEmpty(i.UserName) && i.UserName == username).Select(i => i.Id);
                DeleteRange(userIds, false);
                var user = ServiceFactory<UserService>().GetQuery().FirstOrDefault(i => i.UserName == username);

                var userOnline = new ChatUserOnlineBO()
                {
                    UserName = username
                };
                if (user != null)
                    userOnline.UserOnlineId = user.Id;
                base.Insert(userOnline, false);
                SaveTransaction();
            }
            catch (Exception)
            {

            }

        }

        public void OnDisconnected(string username)
        {
            try
            {
                var userIds = base.GetQuery().Where(i => !string.IsNullOrEmpty(i.UserName) && i.UserName == username).Select(i => i.Id);
                DeleteRange(userIds);
            }
            catch (Exception)
            {

            }

        }
    }
}
