﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class ChatMessengerService : BaseService<ChatMessengerDTO, ChatMessengerBO>, IChatMessengerService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="toUserId"></param>
        /// <returns></returns>
        public IEnumerable<ChatMessengerBO> GetDataChatToUser(Guid toUserId)
        {
            var end = Guid.Empty;
            var data = base.GetQuery()
                .Where(i => (i.CreateBy == UserId && i.To == toUserId) || (i.CreateBy == toUserId && i.To == UserId))
                .OrderBy(i => i.CreateAt)
                .AsEnumerable()
                .Select(i =>
                {
                    var to = i.To;
                    i.ShowAvartar = to != end;
                    i.UserId = UserId;
                    end = to.Value;
                    return i;
                });
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="toUserId"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public Guid NewMessage(Guid toUserId, string content)
        {
            var msg = new ChatMessengerBO()
            {
                Content = content,
                To = toUserId
            };
            return base.Insert(msg);
        }
        public IEnumerable<ChatMessengerBO> GetMoreByCurrentUser(int start, int limit, out int count)
        {
            var data = base.GetQuery()
                .Where(c => c.To == UserId && c.CreateBy.HasValue)
                .GroupBy(c => c.CreateBy)
                .Select(r => r.Where(c => c.CreateAt.Value == r.Max(t => t.CreateAt.Value)).FirstOrDefault());
            count = data.Count();
            var results = data.AsEnumerable().Skip(start).Take(limit).ToList();
            var createIds = results.Where(t => t.CreateBy.HasValue).Select(t => t.CreateBy.Value);
            var createQuery = ServiceFactory<EmployeeService>().GetByIds(createIds).ToList();
            var users = ServiceFactory<UserService>().GetByIds(createIds).ToList();
            results = results.Select(i =>
            {
                if (i.CreateBy.HasValue)
                {
                    i.Creater = createQuery.FirstOrDefault(c => c.Id == i.CreateBy.Value);
                    i.Creater.UserName = users.FirstOrDefault(c => c.Id == i.CreateBy.Value).UserName;
                }
                return i;
            }).ToList();
            return results;
        }
        public int GetTotalNotRead()
        {
            return (base.GetQuery()
                .Where(c => c.To == UserId && c.CreateBy.HasValue)
                .GroupBy(c => c.CreateBy)
                .Select(r => r.Where(c => c.CreateAt.Value == r.Max(t => t.CreateAt.Value)).FirstOrDefault()))
                .Where(c => c.IsRead != true)
                .Count();
        }
        public int UpdateRead(Guid id, bool isRead = true)
        {
            var obj = base.GetById(id);
            if (obj != null)
            {
                obj.IsRead = isRead;
                if (isRead)
                    obj.ReadAt = DateTime.Now;
                base.Update(obj);
            }
            return GetTotalNotRead();
        }
        public void Revert(Guid id)
        {
            var obj = base.GetById(id, true);
            if (obj != null)
            {
                obj.IsDelete = false;
                base.Update(obj);
            }
        }


        public int GetCount()
        {
            var data = base.GetQuery()
                .Where(c => c.To == UserId && c.CreateBy.HasValue)
                .GroupBy(c => c.CreateBy)
                .Select(r => r.Where(c => c.CreateAt.Value == r.Max(t => t.CreateAt.Value)).FirstOrDefault());
            return data.Count();
        }


        public IEnumerable<ChatMessengerBO> GetTopMessenger(int top)
        {
            var data = base.GetQuery()
                .Where(c => c.To == UserId && c.CreateBy.HasValue)
                .GroupBy(c => c.CreateBy)
                .Select(r => r.Where(c => c.CreateAt.Value == r.Max(t => t.CreateAt.Value)).FirstOrDefault());

            var results = data.AsEnumerable().Take(top).ToList();
            var createIds = results.Where(t => t.CreateBy.HasValue).Select(t => t.CreateBy.Value);
            var createQuery = ServiceFactory<EmployeeService>().GetByIds(createIds).ToList();
            results = results.Select(i =>
            {
                if (i.CreateBy.HasValue)
                    i.Creater = createQuery.FirstOrDefault(c => c.Id == i.CreateBy.Value);
                return i;
            }).ToList();
            return results;
        }
    }
}
