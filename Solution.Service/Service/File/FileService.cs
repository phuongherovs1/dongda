﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using iDAS.Service.FileServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iDAS.Service
{
    public class FileService : BaseService<FileDTO, FileBO>, IFileService
    {
        private FileServiceReference.IFileService serviceFile = new FileServiceReference.FileServiceClient();
        private const string SystemCode = "iDAS";
        private const string UserCode = "File";
        public Guid Upload(FileUploadBO data, bool allowSave = true)
        {
            var file = data.FileAttachments.FirstOrDefault();
            if (file == null)
                throw new ArgumentException(Resource.File.Exception.FileNullOrEmpty);
            #region Save file to disk
            string filename = Guid.NewGuid().ToString();
            file.SaveAs(HttpContext.Current.Server.MapPath("/Upload/" + filename + "." + System.IO.Path.GetExtension(file.FileName).Trim('.')));
            #endregion
            var item = new FileBO() { File = file };
            item.Id = new Guid(filename);
            return base.Insert(item, allowSave);
        }
        /// <summary>
        /// upload file
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public IEnumerable<Guid> UploadRange(FileUploadBO data, bool allowSave = true)
        {
            var source = new List<FileBO>();
            foreach (var file in data.FileAttachments)
            {
                if (file == null)
                    continue;
                #region Save file to disk
                string filename = Guid.NewGuid().ToString();
                file.SaveAs(HttpContext.Current.Server.MapPath("/Upload/" + filename + "." + System.IO.Path.GetExtension(file.FileName).Trim('.')));
                #endregion
                var item = new FileBO() { File = file };
                item.Id = new Guid(filename);
                source.Add(item);
            };
            return base.InsertRange(source, allowSave);
        }
        /// <summary>
        /// view file
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public FileBO View(Guid fileId)
        {
            RequestFileInfo fileInfo = new RequestFileInfo();
            ResponseFileInfo fileResult = new ResponseFileInfo();
            fileInfo.SystemCode = SystemCode;
            fileInfo.Code = UserCode;
            fileInfo.Id = fileId.ToString();
            fileResult = serviceFile.ViewFile(fileInfo);
            if (fileResult.Error != FileError.Success)
                throw new ArgumentException(Resource.File.GetException(fileResult.Error));
            return new FileBO() { Stream = fileResult.Stream, Name = fileResult.Name, Type = fileResult.Type };
        }
        /// <summary>
        /// download file
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public FileBO Download(Guid fileId)
        {
            RequestFileInfo fileInfo = new RequestFileInfo();
            ResponseFileInfo fileResult = new ResponseFileInfo();
            fileInfo.SystemCode = SystemCode;
            fileInfo.Code = UserCode;
            fileInfo.Id = fileId.ToString();
            fileResult = serviceFile.DownloadFile(fileInfo);
            if (fileResult.Error != FileError.Success)
                throw new ArgumentException(Resource.File.GetException(fileResult.Error));
            return new FileBO() { Stream = fileResult.Stream, Name = fileResult.Name, Type = fileResult.Type };
        }
        /// <summary>
        /// delete file
        /// </summary>
        /// <param name="fileId"></param>
        public override void Remove(Guid fileId, bool allowSave = true)
        {
            RequestFileInfo fileInfo = new RequestFileInfo();
            ResponseFileInfo fileResult = new ResponseFileInfo();
            fileInfo.SystemCode = SystemCode;
            fileInfo.Code = UserCode;
            fileInfo.Id = fileId.ToString();
            fileResult = serviceFile.DeleteFile(fileInfo);
            if (fileResult.Error != FileError.Success)
                throw new ArgumentException(Resource.File.GetException(fileResult.Error));
            base.Remove(fileId, allowSave);
        }
        /// <summary>
        /// remove file
        /// </summary>
        /// <param name="fileIds"></param>
        public override void RemoveRange(IEnumerable<Guid> fileIds, bool allowSave = true)
        {
            RequestFileInfo fileInfo = new RequestFileInfo();
            ResponseFileInfo fileResult = new ResponseFileInfo();
            foreach (var fileId in fileIds)
            {
                fileInfo.SystemCode = SystemCode;
                fileInfo.Code = UserCode;
                fileInfo.Id = fileId.ToString();
                fileResult = serviceFile.DeleteFile(fileInfo);
                if (fileResult.Error != FileError.Success)
                    throw new ArgumentException(Resource.File.GetException(fileResult.Error));
            }
            base.RemoveRange(fileIds, allowSave);
        }

        //public ResponseFileInfo Update(HttpPostedFileBase file, string fileId)
        //{
        //    UploadFileInfo fileInfo = new UploadFileInfo();
        //    ResponseFileInfo fileResult = new ResponseFileInfo();
        //    fileInfo.SystemCode = SystemCode;
        //    fileInfo.Code = UserCode;
        //    fileInfo.Name = file.FileName;
        //    fileInfo.Size = file.ContentLength;
        //    fileInfo.Type = file.ContentType;
        //    fileInfo.Stream = file.InputStream;
        //    fileInfo.Id = fileId;
        //    fileInfo.IsReplace = true;
        //    fileResult = serviceFile.UploadFile(fileInfo);
        //    if (fileResult.Error == FileError.Success)
        //    {
        //        // var item = fileInfoDA.GetById(new Guid(fileId));
        //        //item.Name = fileResult.Name;
        //        //fileInfoDA.Update(item);
        //        //fileInfoDA.Save();
        //    }
        //    return fileResult;
        //}
        //public ResponseFileInfo ViewExport(string fileId)
        //{
        //    RequestFileInfo fileInfo = new RequestFileInfo();
        //    ResponseFileInfo fileResult = new ResponseFileInfo();
        //    fileInfo.SystemCode = SystemCode;
        //    fileInfo.Code = UserCode;
        //    fileInfo.Id = fileId;
        //    fileResult = serviceFile.ViewExportFile(fileInfo);
        //    return fileResult;
        //}
        //public ResponseFileInfo DownloadExport(string fileId)
        //{
        //    RequestFileInfo fileInfo = new RequestFileInfo();
        //    ResponseFileInfo fileResult = new ResponseFileInfo();
        //    fileInfo.SystemCode = SystemCode;
        //    fileInfo.Code = UserCode;
        //    fileInfo.Id = fileId;
        //    fileResult = serviceFile.DownloadExportFile(fileInfo);
        //    return fileResult;
        //}
    }
}
