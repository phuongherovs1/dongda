﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    class SmsFormContentService : BaseService<SmsFormContentDTO, SmsFormContentBO>, ISmsFormContentService
    {
        public IEnumerable<SmsFormContentBO> GetAll(int pageIndex, int pageSize, out int count, bool allowDeleted = false)
        {
            var data = base.GetAll(allowDeleted);
            count = data.ToList().Count;
            data = base.Page(data, pageIndex, pageSize);
            return data;
        }

        public IEnumerable<SmsFormContentBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count, bool allowDeleted = false)
        {
            count = GetAll().Count();
            var data = Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            if (!string.IsNullOrEmpty(filterName))
                data = Page(GetAll().Where(x => x.FormContent.ToUpper().Contains(filterName.ToUpper())).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }

        public SmsFormContentBO GetFormItem()
        {
            try
            {
                var data = new SmsFormContentBO();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Guid Insert(SmsFormContentBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.FormContent.Trim().ToUpper() == data.FormContent.Trim().ToUpper() && i.Id != data.Id).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            data.Id = Insert(data, true);
            return data.Id;
        }

        public void Update(SmsFormContentBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.FormContent.Trim().ToUpper() == data.FormContent.Trim().ToUpper() && i.Id != data.Id).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            Update(data, true);
        }
    }
}
