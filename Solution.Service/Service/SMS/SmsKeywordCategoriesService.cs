﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    class SmsKeywordCategoriesService : BaseService<SmsKeywordCategoriesDTO, SmsKeywordCategoriesBO>, ISmsKeywordCategoriesService
    {
        #region Override
        public Guid Insert(SmsKeywordCategoriesBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Name.Trim().ToUpper() == data.Name.Trim().ToUpper() && i.Id != data.Id).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            data.Id = Insert(data, true);
            return data.Id;
        }

        public void Update(SmsKeywordCategoriesBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Name.Trim().ToUpper() == data.Name.Trim().ToUpper() && i.Id != data.Id).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            Update(data, true);
        }

        public override IEnumerable<SmsKeywordCategoriesBO> GetAll(bool allowDeleted = false)
        {
            var data = base.GetAll(allowDeleted);
            return data;
        }

        public IEnumerable<SmsKeywordCategoriesBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            count = GetAll().Count();
            var data = Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }
        public IEnumerable<SmsKeywordCategoriesBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count)
        {
            count = GetAll().Count();
            var data = Page(GetAll().Where(x => x.Name.Contains(filterName) || (!string.IsNullOrEmpty(x.Description) && x.Description.Contains(filterName))).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }

        #endregion
        #region Overload
        public override SmsKeywordCategoriesBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id);
            return data;
        }
        #endregion
        #region Rule
        public SmsKeywordCategoriesBO GetFormItem()
        {
            try
            {
                var data = new SmsKeywordCategoriesBO()
                {
                    Id = Guid.NewGuid(),
                    Name = string.Empty,
                    Description = string.Empty
                };
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
        public override IEnumerable<SmsKeywordCategoriesBO> GetDataByStringQuery(string query)
        {
            var data = base.GetDataByStringQuery(query);
            return data;
        }
        public int Import(List<SmsKeywordCategoriesBO> objects)
        {
            var numberInportSuccess = 0;
            var numberInportFail = 0;
            foreach (var item in objects)
            {
                if (!base.GetQuery().Any(t => t.Name.Trim().ToUpper() == item.Name.Trim().ToUpper()))
                {
                    var procedureobject = new SmsKeywordCategoriesBO()
                    {
                        Name = item.Name,
                        Description = item.Description
                    };
                    base.Insert(procedureobject);
                    numberInportSuccess++;
                }
            }
            numberInportFail = objects.Count - numberInportSuccess;
            return numberInportFail;
        }
        public IEnumerable<OrganizationTreeBO> GetByNode()
        {
            var data = new List<OrganizationTreeBO>();
            try
            {
                var children = GetAll(false).OrderBy(x => x.Name);
                foreach (var child in children)
                {
                    var treeNode = new OrganizationTreeBO()
                    {
                        Id = string.Format("{0}", child.Id),
                        Name = child.Name,
                        AvatarUrl = string.Empty,
                        Type = Common.Resource.OrganizationTreeType.Title,
                        IsLeaf = false
                    };
                    data.Add(treeNode);
                }
            }
            catch (Exception e) { }
            return data;
        }
    }
}
