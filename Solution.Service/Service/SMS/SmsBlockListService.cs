﻿using iDAS.ADO;
using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class SmsBlockListService : BaseService<SmsBlockListDTO, SmsBlockListBO>, ISmsBlockListService
    {
        #region Override
        public Guid Insert(SmsBlockListBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.SendNumber.Trim().ToUpper() == data.SendNumber.Trim().ToUpper() && i.SendDate == data.SendDate && i.Id != data.Id && i.IsDelete == false).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            data.Id = Insert(data, true);
            return data.Id;
        }

        public void Update(SmsBlockListBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.SendNumber.Trim().ToUpper() == data.SendNumber.Trim().ToUpper() && i.Id != data.Id && i.IsDelete == false).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            Update(data, true);
        }

        public override IEnumerable<SmsBlockListBO> GetAll(bool allowDeleted = false)
        {
            var data = base.GetAll(allowDeleted);
            return data;
        }

        public IEnumerable<SmsBlockListBO> GetAll(int pageIndex, int pageSize, out int count, bool allowDeleted = false)
        {
            count = GetAll(allowDeleted).Count();
            var data = Page(GetAll(allowDeleted).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }
        public IEnumerable<SmsBlockListBO> GetByfilterName(int pageIndex, int pageSize, string filterName, DateTime DateFrom, DateTime DateTo, out int count, bool allowDeleted = false)
        {
            int IntCount = 0;
            var data = GetAll(allowDeleted);
            List<SmsBlockListBO> result = new List<SmsBlockListBO>();
            foreach (SmsBlockListBO item in data)
            {
                var SmsManagersSynchronized = ServiceFactory<SmsManagersSynchronizedService>().GetById(item.IdSms.Value);
                item.Sendreflect = SmsManagersSynchronized.SendNumber;
                item.MessageContentSynchronized = SmsManagersSynchronized.MessageContent;
                result.Add(item);
            }
            if (!string.IsNullOrEmpty(filterName))
            {
                result = Page(result.Where(x => x.SendNumber.Contains(filterName) || x.MessageContent.Contains(filterName)).OrderByDescending(i => i.SendDate), pageIndex, pageSize).ToList();
                IntCount = result.Where(x => x.SendNumber.Contains(filterName) || x.MessageContent.Contains(filterName)).ToList().Count;
            }
            if (DateFrom != null)
            {
                result = Page(result.Where(x => x.SendDate.Value.Subtract(DateFrom).Hours >= 0).OrderByDescending(i => i.SendDate), pageIndex, pageSize).ToList();
                IntCount = data.Where(x => x.SendDate.Value.Subtract(DateFrom).Hours >= 0).ToList().Count;
            }
            if (DateTo != null)
            {
                result = Page(result.Where(x => DateTo != null && x.SendDate.Value.Subtract(DateTo).Hours < 0).OrderByDescending(i => i.SendDate), pageIndex, pageSize).ToList();
                IntCount = result.Where(x => DateTo != null && x.SendDate.Value.Subtract(DateTo).Hours < 0).ToList().Count;
            }
            count = IntCount;
            return result;
        }

        #endregion
        #region Overload
        public override SmsBlockListBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id, allowDeleted);
            return data;
        }
        public override SmsBlockListBO GetJustById(Guid id, bool allowDefaultIfNull = true)
        {
            var data = base.GetJustById(id);
            return data;
        }
        #endregion
        #region Rule
        public SmsBlockListBO GetFormItem()
        {
            try
            {
                var data = new SmsBlockListBO()
                {
                    //Id = Guid.NewGuid(),
                    //Name = string.Empty,
                    //Description = string.Empty
                };
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        public int Import(List<SmsBlockListBO> objects)
        {
            var numberInportSuccess = 0;
            var numberInportFail = 0;
            foreach (var item in objects)
            {
                //if (!base.GetQuery().Any(t => t.Name.Trim().ToUpper() == item.Name.Trim().ToUpper()))
                //{
                //    var procedureobject = new SmsBlockListBO()
                //    {
                //        Name = item.Name,
                //        Description = item.Description
                //    };
                //    base.Insert(procedureobject);
                //    numberInportSuccess++;
                //}
            }
            numberInportFail = objects.Count - numberInportSuccess;
            return numberInportFail;
        }

        public List<SmsManagersSynchronizedBO> GetDataSMSWarning(int pageIndex, int pageSize, string filterName, out int count)
        {
            count = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_GetCount_SmsManagersSynchronized_SMSWarning", parameter: new { filterName = filterName, key = "Vi phạm nghiêm trọng" }).Rows.Count;
            return Sql_ADO.idasAdoService.ToList<SmsManagersSynchronizedBO>(Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Getdata_SmsManagersSynchronized_SMSWarning", parameter: new { pageIndex = pageIndex, pageSize = pageSize, filterName = filterName, key = "Vi phạm nghiêm trọng" }));
        }
    }
}
