﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    class SmsKeywordsService : BaseService<SmsKeywordsDTO, SmsKeywordsBO>, ISmsKeywordsService
    {
        #region Override
        public Guid Insert(SmsKeywordsBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.KeywordValue.Trim().ToUpper() == data.KeywordValue.Trim().ToUpper() && i.Id != data.Id).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            data.Id = Insert(data, true);
            return data.Id;
        }

        public void Update(SmsKeywordsBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.KeywordValue.Trim().ToUpper() == data.KeywordValue.Trim().ToUpper() && i.Id != data.Id).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            Update(data, true);
            SaveTransaction(allowSave);
        }

        public override IEnumerable<SmsKeywordsBO> GetAll(bool allowDeleted = false)
        {
            var data = base.GetAll(allowDeleted);
            return data;
        }

        public IEnumerable<SmsKeywordsBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            count = GetAll().Count();
            var data = Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }
        public IEnumerable<SmsKeywordsBO> GetByfilterName(int pageIndex, int pageSize, string filterName, Guid keywordCategoryId, out int count)
        {
            count = GetAll().Count();
            var data = Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            if (!string.IsNullOrEmpty(filterName) && keywordCategoryId == Guid.Empty)
                data = Page(GetAll().Where(x => x.KeywordValue.Contains(filterName) || (!string.IsNullOrEmpty(x.Description) && x.Description.Contains(filterName))).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            else if (keywordCategoryId != Guid.Empty && string.IsNullOrEmpty(filterName))
                data = Page(GetAll().Where(x => x.KeywordCategoryId == keywordCategoryId).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            else if (!string.IsNullOrEmpty(filterName) && keywordCategoryId != Guid.Empty)
                data = Page(GetAll().Where(x => (x.KeywordValue.Contains(filterName) || x.Description.Contains(filterName)) && (x.KeywordCategoryId == keywordCategoryId)).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }

        #endregion
        #region Overload
        public override SmsKeywordsBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id);
            return data;
        }
        #endregion
        #region Rule
        public SmsKeywordsBO GetFormItem()
        {
            try
            {
                var data = new SmsKeywordsBO()
                {
                    Id = Guid.NewGuid(),
                    KeywordValue = string.Empty,
                    KeywordCategoryId = Guid.NewGuid(),
                    Description = string.Empty
                };
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
        public int Import(List<SmsKeywordsBO> objects)
        {
            var numberInportSuccess = 0;
            var numberInportFail = 0;
            foreach (var item in objects)
            {
                if (!base.GetQuery().Any(t => t.KeywordValue.Trim().ToUpper() == item.KeywordValue.Trim().ToUpper()))
                {
                    var procedureobject = new SmsKeywordsBO()
                    {
                        KeywordValue = item.KeywordValue,
                        KeywordCategoryId = item.KeywordCategoryId,
                        Description = item.Description
                    };
                    base.Insert(procedureobject);
                    numberInportSuccess++;
                }
            }
            numberInportFail = objects.Count - numberInportSuccess;
            return numberInportFail;
        }
    }
}
