﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class SmsManagersSynchronizedService : BaseService<SmsManagersSynchronizedDTO, SmsManagersSynchronizedBO>, ISmsManagersSynchronizedService
    {
        #region Override
        public Guid Insert(SmsManagersSynchronizedBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.SendNumber.Trim().ToUpper() == data.SendNumber.Trim().ToUpper() && i.RecieveNumber.Trim().ToUpper() == data.RecieveNumber.Trim().ToUpper() && i.SendDate == data.SendDate && i.Id != data.Id).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            data.Id = Insert(data, true);
            return data.Id;
        }

        public void Update(SmsManagersSynchronizedBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Id != data.Id).Any();
            if (!existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            Update(data, true);
        }

        public override IEnumerable<SmsManagersSynchronizedBO> GetAll(bool allowDeleted = false)
        {
            var data = base.GetAll(allowDeleted);
            return data;
        }
        public IEnumerable<SmsManagersSynchronizedBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            count = GetAll().Count();
            var data = Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }
        public IEnumerable<SmsManagersSynchronizedBO> GetDataByStringQuery(string query, int pageIndex, int pageSize, out int count)
        {
            count = GetDataByStringQuery(query).Count();
            var data = Page(GetDataByStringQuery(query).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }
        public IEnumerable<SmsManagersSynchronizedBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count)
        {
            count = GetAll().Count();
            var data = Page(GetAll().Where(x => x.SendNumber.Contains(filterName) || x.MessageContent.Contains(filterName) || x.NetworkProvider.Contains(filterName) || x.RecieveNumber.Contains(filterName)).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }

        #endregion
        #region Overload
        public override SmsManagersSynchronizedBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id);
            return data;
        }
        #endregion
        #region Rule
        public SmsManagersSynchronizedBO GetFormItem()
        {
            try
            {
                var data = new SmsManagersSynchronizedBO()
                {
                    //Id = Guid.NewGuid(),
                    //Name = string.Empty,
                    //Description = string.Empty
                };
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        public void Import(List<SmsManagersSynchronizedBO> objects)
        {
            foreach (var item in objects)
            {
                try
                {
                    base.Insert(item);
                }
                catch (Exception e) { }
            }
        }
    }
}
