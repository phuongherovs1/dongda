﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class SmsTypeService : BaseService<SmsTypeDTO, SmsTypeBO>, ISmsTypeService
    {
        public Guid Insert(SmsTypeBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            data.Id = Insert(data, false);
            if (!string.IsNullOrEmpty(dataPropertySettings))
            {
                var dataProperties = JsonConvert.DeserializeObject<List<AssetPropertySettingBO>>(dataPropertySettings);
                ServiceFactory<AssetPropertySettingService>().InsertRange(data.Id, dataProperties, false);
            }
            if (!string.IsNullOrEmpty(dataResourceRelates))
            {
                var resoureRelates = JsonConvert.DeserializeObject<List<AssetResourceRelateBO>>(dataResourceRelates);
                ServiceFactory<AssetResourceRelateService>().InsertRange(data.Id, resoureRelates, false);
            }
            SaveTransaction(allowSave);
            return data.Id;
        }

        public void Update(SmsTypeBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            Update(data, false);
            if (!string.IsNullOrEmpty(dataPropertySettings))
            {
                var dataProperties = JsonConvert.DeserializeObject<List<AssetPropertySettingBO>>(dataPropertySettings);
                ServiceFactory<AssetPropertySettingService>().Update(data.Id, dataProperties, false);
            }
            if (!string.IsNullOrEmpty(dataResourceRelates))
            {
                var resoureRelates = JsonConvert.DeserializeObject<List<AssetResourceRelateBO>>(dataResourceRelates);
                ServiceFactory<AssetResourceRelateService>().Update(data.Id, resoureRelates, false);
            }
            SaveTransaction(allowSave);
        }

        public override IEnumerable<SmsTypeBO> GetAll(bool allowDeleted = false)
        {
            var data = base.GetAll(allowDeleted);
            return data;
        }
        public override IEnumerable<SmsTypeBO> GetDataByStringQuery(string query)
        {
            var data = base.GetDataByStringQuery(query);
            return data;
        }

        public IEnumerable<SmsTypeBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            count = GetAll().Count();
            var data = Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }
    }
}
