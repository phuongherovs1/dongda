﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class SmsManagersService : BaseService<SmsManagerDTO, SmsManagersBO>, ISmsManagersService
    {
        #region Override
        public Guid Insert(SmsManagersBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            //var existCode = Get(i => i.RecieveNumber == data.RecieveNumber && i.SendNumber == data.SendNumber && i.SendDate.Value == data.SendDate.Value && i.Id != data.Id).Any();
            //if (existCode)
            //{
            //    var ex = new DataHasBeenExistedException();
            //    throw ex;
            //}
            data.Id = Insert(data, true);
            return data.Id;
        }

        public void Update(SmsManagersBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Id == data.Id).Any();
            if (!existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            Update(data, true);
        }

        public override IEnumerable<SmsManagersBO> GetAll(bool allowDeleted = false)
        {
            var data = base.GetAll(allowDeleted);
            return data;
        }

        public IEnumerable<SmsManagersBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            count = GetAll().Count();
            var data = Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }
        public IEnumerable<SmsManagersBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count)
        {
            count = GetAll().Count();
            var data = Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            if (!string.IsNullOrEmpty(filterName))
                data = Page(GetAll().Where(x => x.RecieveNumber.Contains(filterName) || x.SendNumber.Contains(filterName) || x.MessageContent.Contains(filterName)).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            //if (!string.IsNullOrEmpty(filterName) && keywordCategoryId == Guid.Empty)
            //    data = Page(GetAll().Where(x => x.KeywordValue.Contains(filterName) || x.Description.Contains(filterName)).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            //else if (keywordCategoryId != Guid.Empty && string.IsNullOrEmpty(filterName))
            //    data = Page(GetAll().Where(x => x.KeywordCategoryId == keywordCategoryId).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            //else if (!string.IsNullOrEmpty(filterName) && keywordCategoryId != Guid.Empty)
            //    data = Page(GetAll().Where(x => (x.KeywordValue.Contains(filterName) || x.Description.Contains(filterName)) && (x.KeywordCategoryId == keywordCategoryId)).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }

        #endregion
        #region Overload
        public override SmsManagersBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id);
            return data;
        }
        #endregion
        #region Rule
        public SmsManagersBO GetFormItem()
        {
            try
            {
                var data = new SmsManagersBO()
                {
                    Id = Guid.NewGuid(),
                    RecieveNumber = string.Empty,
                    SendNumber = string.Empty,
                    SendDate = DateTime.MinValue,
                    MessageContent = string.Empty,
                    NetworkProvider = string.Empty
                };
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
        public int Import(List<SmsManagersBO> objects)
        {
            var numberInportSuccess = 0;
            var numberInportFail = 0;
            foreach (var item in objects)
            {
                if (!base.GetQuery().Any(i => i.RecieveNumber == item.RecieveNumber && i.SendNumber == item.SendNumber && i.SendDate.Value == item.SendDate.Value))
                {
                    try
                    {
                        base.Insert(item);
                        numberInportSuccess++;
                    }
                    catch (Exception e) { }
                }
            }
            numberInportFail = objects.Count - numberInportSuccess;
            return numberInportFail;
        }
    }
}
