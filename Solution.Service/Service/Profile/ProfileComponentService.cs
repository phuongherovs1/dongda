﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileComponentService : BaseService<ProfileComponentDTO, ProfileComponentBO>, IProfileComponentService
    {
        public void UpdateProfileComponentPrinciple(Guid profileId)
        {
            var profile = ServiceFactory<ProfileService>().GetById(profileId);
            if (profile != null)
            {
                if (profile.CategoryId.HasValue)
                {
                    var profilePrincipleIds = ServiceFactory<ProfilePrincipleService>().Get(i => i.CategoryId == profile.CategoryId.Value && i.IsUse == true).Select(i => i.Id).ToList();
                    if (profilePrincipleIds.Count > 0)
                    {
                        var insertedProfilePrincipleIds = Get(i => i.ProfilePrincipleId.HasValue && profilePrincipleIds.Contains(i.ProfilePrincipleId.Value) && i.ProfileId == profileId).Select(i => i.ProfilePrincipleId).ToList();
                        var profilePrincipleInsertIds = profilePrincipleIds.Where(i => !insertedProfilePrincipleIds.Contains(i)).ToList();
                        if (profilePrincipleInsertIds.Count > 0)
                        {
                            var profileComponentInsert = ServiceFactory<ProfilePrincipleService>().GetByIds(profilePrincipleInsertIds)
                                                                                .Select(i => new ProfileComponentBO()
                                                                                {
                                                                                    ProfileId = profileId,
                                                                                    ProfilePrincipleId = i.Id,
                                                                                    Name = i.Name,
                                                                                    ArchiveType = i.ArchiveType,
                                                                                    IsUpdated = false,
                                                                                    IsCancel = false,
                                                                                    IsProfilePrinciple = true
                                                                                }
                                                                                     );
                            base.InsertRange(profileComponentInsert);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Lấy danh sách thông tin thành phần hồ sơ hồ sơ theo id của hồ sơ
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="categoryId"></param>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public IEnumerable<ProfileComponentBO> GetByProfile(int pageIndex, int pageSize, out int count, Guid profileId, Common.Resource.ProfileComponentStatus status = 0, string query = default(string))
        {
            var va = status == Resource.ProfileComponentStatus.WaitUpdate;
            var components = Get(i => i.ProfileId == profileId
                                    && ((status == Resource.ProfileComponentStatus.All
                                        || status == Resource.ProfileComponentStatus.WaitUpdate && i.IsCancel != true && i.IsUpdated != true)
                                        || (status == Resource.ProfileComponentStatus.Updating && i.IsCancel != true && i.IsUpdated == true)
                                        || (status == Resource.ProfileComponentStatus.Eliminate && (i.IsCancel == true))
                                    )
                                )
                             .Where(i => string.IsNullOrEmpty(query) || (!string.IsNullOrEmpty(i.Name) && i.Name.Contains(query)))
                            ;
            count = components.Count();
            var result = Page(components, pageIndex, pageSize);
            result = result
                        .Select(i =>
                            {
                                i.Note = String.IsNullOrEmpty(i.Note) ? "Chưa cập nhật" : i.Note;
                                i.DocumentName = i.ProfilePrincipleId.HasValue ? ServiceFactory<ProfilePrincipleService>().GetDocumentName(i.ProfilePrincipleId.Value) : "";
                                i.EmployeePerformBy = i.EmployeeId.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.EmployeeId.Value) : new EmployeeBO();
                                return i;
                            }
                        );
            return result;
        }
        /// <summary>
        /// Lấy chi tiết thông tin thành phần hồ sơ
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProfileComponentBO GetDetail(Guid id)
        {
            var data = base.GetById(id);
            var files = ServiceFactory<ProfileComponentAttachmentService>().GetByComponent(data.Id);
            data.FileAttachments = new FileUploadBO()
            {
                Files = files.ToList()
            };
            if (data.ProfileId.HasValue)
            {
                var profile = ServiceFactory<ProfileService>().GetProfileInfo(data.ProfileId.Value);
                data.ProfileName = profile.Name;
                data.ProfileCategoryName = profile.CategoryName;
            }
            return data;
        }
        /// <summary>
        /// Thêm mới thông tin thành phần hồ sơ
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid InsertProfilecomponent(ProfileComponentBO data)
        {
            if (Get(i => !string.IsNullOrEmpty(i.Name) && i.Name.Trim().ToLower() == data.Name.Trim().ToLower() && i.Id != data.Id).Any())
                throw new ProfileNameHasBeenExistedException();
            data.IsUpdated = true;
            data.EmployeeId = UserId;
            data.Id = base.Insert(data);
            // Cập nhật đính kèm
            ServiceFactory<ProfileComponentAttachmentService>().Update(data.FileAttachments, data.Id, false);
            return data.Id;
        }
        /// <summary>
        /// Cập nhật thông tin thành phần hồ sơ
        /// </summary>
        /// <param name="data"></param>
        public void UpdateProfilecomponent(ProfileComponentBO data)
        {
            if (Get(i => !string.IsNullOrEmpty(i.Name) && i.Name.Trim().ToLower() == data.Name.Trim().ToLower() && i.Id != data.Id).Any())
                throw new ProfileNameHasBeenExistedException();
            data.IsUpdated = true;
            data.EmployeeId = UserId;
            base.Update(data, false);
            ServiceFactory<ProfileComponentAttachmentService>().Update(data.FileAttachments, data.Id, false);
            SaveTransaction(true);
        }
        // Sử dụng thành phần hồ sơ
        public void UseComponent(Guid id)
        {
            var component = GetById(id);
            component.IsCancel = false;
            base.Update(component);
        }
        // Loại bỏ thành phần hồ sơ
        public void CancelComponent(Guid id)
        {
            var component = GetById(id);
            component.IsCancel = true;
            base.Update(component);
        }

        /// <summary>
        /// Kiểm tra người đăng nhập co phải người tạo hồ sơ không:
        /// - Nếu người đăng nhập là người tạo hồ sơ và người được giao phân công thì hiển thị chức năng Thêm và Xóa, ngược lại thì ẩn
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public bool CheckRoleAddOrDelete(Guid profileId)
        {
            try
            {
                return ServiceFactory<ProfileService>().GetQuery().Where(p => p.Id == profileId && p.CreateBy == UserId).Any();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Nếu người đăng nhập là người tạo hồ sơ, người được phân công, người kiểm tra hoặc người phê duyệt thì mới thực hiện được chức năng Cập nhật
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public bool CheckRoleUpdate(Guid profileId)
        {
            try
            {
                var roleCreate = ServiceFactory<ProfileService>().GetQuery().Where(p => p.Id == profileId && p.CreateBy == UserId).Any();
                // Nếu người đăng nhập không phải người tạo hồ sơ thì kiểm tra xem có được phân công cập nhật không
                if (!roleCreate)
                {
                    var roleAssign = ServiceFactory<ProfileAssignService>().GetQuery().Where(p => p.ProfileId.HasValue && p.ProfileId.Value == profileId && p.EndAt >= DateTime.Now &&
                                                                                       p.PerformBy.HasValue && p.PerformBy.Value == UserId).Any();
                    // Nếu người đăng nhập không phải người được giao phân công thì kiểm tra xem có phải là người kiểm tra hoặc phê duyệt không
                    if (!roleAssign)
                    {
                        var responsibility = ServiceFactory<ProfileResponsibilityService>().GetQuery().Where(p => p.ProfileId == profileId && p.RoleType.HasValue &&
                                             (p.RoleType.Value == (int)Resource.ProfileResponsibilityRole.Checker || p.RoleType.Value == (int)Resource.ProfileResponsibilityRole.Approval));
                        if (responsibility.Any(p => p.EmployeeId == UserId))
                            return true;
                        else
                            return false;
                    }
                    return roleAssign;
                }
                return roleCreate;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Nếu người đăng nhập là người tạo hồ sơ hoặc người được phân công thì mới thực hiện được chức năng Lưu
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public bool CheckRoleSaveProfileComponent(Guid profileId)
        {
            try
            {
                var roleCreate = ServiceFactory<ProfileService>().GetQuery().Where(p => p.Id == profileId && p.CreateBy == UserId).Any();
                // Nếu người đăng nhập không phải người tạo hồ sơ thì kiểm tra xem có được phân công cập nhật không
                if (!roleCreate)
                    return ServiceFactory<ProfileAssignService>().GetQuery().Where(p => p.ProfileId.HasValue && p.ProfileId.Value == profileId && p.EndAt >= DateTime.Now &&
                                                                                       p.PerformBy.HasValue && p.PerformBy.Value == UserId).Any();
                return roleCreate;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Thông tin hồ sơ
        /// Chỉ người tạo hồ sơ hoặc người được giao cập nhật mới được thêm mới thành phần hồ sơ
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ProfileBO GetProfileInfo(Guid Id)
        {
            var profile = ServiceFactory<ProfileService>().GetById(Id);
            var assign = ServiceFactory<ProfileAssignService>().GetQuery().Where(p => p.ProfileId == Id);
            // Kiểm tra người đăng nhập có phải là người tạo hồ sơ không
            if (profile.CreateBy != UserId)
            {
                // Kiểm tra người đăng nhập có được giao phân công cập nhật không
                if (!assign.Any(p => p.PerformBy == UserId))
                    throw new AccessDenyException();
            }
            if (profile.DepartmentId.HasValue)
            {
                profile.DepartmentName = ServiceFactory<DepartmentService>().GetById(profile.DepartmentId.Value).Name;
            }
            if (profile.CategoryId.HasValue)
            {
                profile.CategoryName = ServiceFactory<ProfileCategoryService>().GetById(profile.CategoryId.Value).Name;
            }
            return profile;
        }
    }
}
