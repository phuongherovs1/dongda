﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileSuggestBorrowService : BaseService<ProfileSuggestBorrowDTO, ProfileSuggestBorrowBO>, IProfileSuggestBorrowService
    {
        public override System.Guid Insert(iDAS.Service.ProfileSuggestBorrowBO data, bool allowSave = true)
        {
            data.ApproveBy = data.Approval.Id;
            data.SendAt = DateTime.Now;
            return base.Insert(data, allowSave);
        }
        public IEnumerable<ProfileSuggestBorrowBO> GetByCurrentUser()
        {
            // Không lấy những hồ sơ đã bị xóa
            var profile = ServiceFactory<ProfileService>().GetQuery().Select(n => n.Id);
            var data = base.GetQuery().Where(c => c.CreateBy == UserId || c.ApproveBy == UserId)
                .Where(c => c.IsApprove != true && c.IsSend == true && profile.AsEnumerable().Contains(c.ProfileId.Value));
            return data;
        }
        public ProfileSuggestBorrowBO GetDetail(Guid id)
        {
            var data = base.GetById(id);
            if (data.ProfileId.HasValue)
                data.Profile = ServiceFactory<ProfileService>().GetDetailDataProfile(data.ProfileId.Value);
            if (data.ApproveBy.HasValue)
                data.Approval = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.ApproveBy.Value);
            if (data.CreateBy.HasValue)
                data.Creater = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.CreateBy.Value);
            return data;
        }
        public void ApprovalSuggest(ProfileSuggestBorrowBO item)
        {
            item.IsApprove = true;
            item.ApprovalAt = DateTime.Now;
            base.Update(item, false);
            if (item.IsAccept == true)
            {
                var borrow = new ProfileBorrowBO();
                borrow.ProfileId = item.ProfileId;
                borrow.BorrowDate = item.BorrowDate;
                borrow.ReturnEstimateDate = item.ReturnEstimateDate;
                borrow.Status = (int)iDAS.Service.Common.Resource.ProfileBorrowStatus.WaitAssign;
                borrow.BorrowEmployeeId = item.Creater.Id;
                borrow.ProfileSuggestBorrowId = item.Id;
                ServiceFactory<ProfileBorrowService>().Insert(borrow, false);
                var borrowFist = ServiceFactory<ProfileBorrowService>().GetQuery().Where(c => c.ProfileId == item.ProfileId && c.IsClose != true);
                foreach (var borrowItem in borrowFist)
                {
                    borrowItem.IsClose = true;
                    ServiceFactory<ProfileBorrowService>().Update(borrowItem, false);
                }
            }
            base.SaveTransaction();
        }
    }
}
