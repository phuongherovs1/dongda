﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileDepartmentArchiveRoleService : BaseService<ProfileDepartmentArchiveRoleDTO, ProfileDepartmentArchiveRoleBO>, IProfileDepartmentArchiveRoleService
    {
        /// <summary>
        /// lấy danh sách quyền lưu trữ theo phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public IEnumerable<ProfileDepartmentArchiveRoleBO> GetByDepartment(Guid departmentId)
        {
            var data = Get(i => i.DepartmentId == departmentId);
            var employeeIds = data.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value);
            var titleIds = data.Where(i => i.TitleId.HasValue).Select(i => i.TitleId.Value);
            var employees = ServiceFactory<EmployeeService>().GetByIds(employeeIds);
            var titles = ServiceFactory<DepartmentTitleService>().GetByIds(titleIds);
            data = data.Select(
                i =>
                {
                    if (i.EmployeeId.HasValue && i.EmployeeId != Guid.Empty)
                    {
                        var employee = employees.FirstOrDefault(u => i.EmployeeId == u.Id);
                        i.ObjectName = employee == null ? string.Empty : employee.Name;
                        i.ObjectImage = employee == null ? string.Empty : employee.AvatarUrl;
                    }
                    else
                    {
                        var title = titles.FirstOrDefault(u => i.TitleId == u.Id);
                        i.ObjectName = title == null ? string.Empty : title.Name;
                    }
                    return i;
                });
            return data;
        }

        /// <summary>
        /// Lưu thông tin người lữu trữ
        /// </summary>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public IEnumerable<Guid> Save(string jsonData)
        {

            try
            {
                var result = Enumerable.Empty<Guid>();
                var data = JsonConvert.DeserializeObject<List<ProfileDepartmentArchiveRoleBO>>(jsonData);
                if (data.Count > 0)
                {
                    result = InsertRange(data);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CheckAllowByCurentUser(Guid departmentId)
        {
            try
            {
                var userId = UserId;
                var titleIds = ServiceFactory<EmployeeTitleService>().GetTitleIdsByCurrentUser();
                var data = GetQuery().Where(i => i.DepartmentId == departmentId)
                                      .Where(i => (i.EmployeeId.HasValue && i.EmployeeId == UserId) || (i.TitleId.HasValue && titleIds.Contains(i.TitleId.Value)))
                                      .Any();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
