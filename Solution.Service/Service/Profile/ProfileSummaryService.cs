﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileSummaryService : BaseService<ProfileDTO, ProfileBO>, IProfileSummaryService
    {
        /// <summary>
        /// Lấy danh sách công việc phân công cập nhật hồ sơ 
        /// Là những công việc hiện tại cần xử lý
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProfileDasboardBO> GetDasboardAssignUpdate()
        {
            var result = Enumerable.Empty<ProfileDasboardBO>();
            // Danh sách phân công được gửi đến người đăng nhập
            var listProfileAssign = ServiceFactory<ProfileAssignService>().GetSendAssign()
                                    .Select(p => new ProfileDasboardBO
                                    {
                                        Id = p.Id,
                                        ProfileId = p.ProfileId,
                                        Time = p.CreateAt.HasValue ? p.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                                        Name = "Phân công cập nhật hồ sơ",
                                        RoleType = (int)Resource.ProfileRoleType.ProfileAssign,
                                        StatusAssign = p.StatusType,
                                        IsAssign = true,
                                        CreateAt = p.CreateAt
                                    });
            if (listProfileAssign.Count() > 0) { result = result.Concat(listProfileAssign); }
            return result.OrderByDescending(p => p.CreateAt);

        }
        /// <summary>
        /// Lấy danh sách đề nghị kiểm tra
        /// Là những công việc hiện tại cần xử lý
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProfileDasboardBO> GetDasboardSuggestCheck()
        {
            var result = Enumerable.Empty<ProfileDasboardBO>();
            // Danh sách vai trò kiểm tra được gửi đến người đăng nhập
            var listProfileCheck = ServiceFactory<ProfileResponsibilityService>().GetResponsibilityReview()
                                    .Select(p => new ProfileDasboardBO
                                    {
                                        Id = p.Id,
                                        ProfileId = p.ProfileId,
                                        Time = p.CreateAt.HasValue ? p.SendAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                                        Name = "Kiểm tra thành phần hồ sơ",
                                        RoleType = (int)Resource.ProfileRoleType.ProfileChecker,
                                        StatusResponsibility = p.StatusType,
                                        CreateAt = p.CreateAt
                                    });
            if (listProfileCheck.Count() > 0) { result = result.Concat(listProfileCheck); }
            return result.OrderByDescending(p => p.CreateAt);
        }
        /// <summary>
        /// Lấy danh sách phê duyệt của đề nghị hủy, đề nghị mượn trả
        /// Là những công việc hiện tại cần xử lý
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProfileDasboardBO> GetDasboardApproval()
        {
            var result = Enumerable.Empty<ProfileDasboardBO>();
            // Danh sách đề nghị hủy chờ phê duyệt
            var listProfileDestroySuggestSend = ServiceFactory<ProfileDestroySuggestService>().GetDestroySuggestSend()
                                                .Select(item => new ProfileDasboardBO
                                                {
                                                    Id = item.Id,
                                                    Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                                                    Name = "Đề nghị hủy hồ sơ",
                                                    RoleType = (int)Resource.ProfileRoleType.DestroyApproval,
                                                    IsDestroy = true,
                                                    IsAccept = item.IsAccept,
                                                    IsApproval = item.IsApprove,
                                                    CreateAt = item.CreateAt
                                                });
            if (listProfileDestroySuggestSend.Count() > 0) { result = result.Concat(listProfileDestroySuggestSend); }
            var profileSuggestBorrows = ServiceFactory<ProfileSuggestBorrowService>().GetByCurrentUser()
                                        .Select(item => new ProfileDasboardBO
                                        {
                                            Id = item.Id,
                                            Time = item.CreateAt.HasValue ? item.CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                                            Name = "Đề nghị mượn hồ sơ",
                                            RoleType = item.ApproveBy == UserId ? (int)Resource.ProfileRoleType.BorrowApproval : (int)Resource.ProfileRoleType.BorrowCreater,
                                            IsAccept = item.IsAccept,
                                            IsApproval = item.IsApprove,
                                            CreateAt = item.CreateAt
                                        });
            if (profileSuggestBorrows.Count() > 0) { result = result.Concat(profileSuggestBorrows); }
            // Danh sách vai trò phê duyệt được gửi đến người đăng nhập
            var listProfileApproval = ServiceFactory<ProfileResponsibilityService>().GetResponsibilityApproval()
                                    .Select(p => new ProfileDasboardBO
                                    {
                                        Id = p.Id,
                                        ProfileId = p.ProfileId,
                                        Time = p.CreateAt.HasValue ? p.SendAt.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                                        Name = "Phê duyệt thành phần hồ sơ",
                                        RoleType = (int)Resource.ProfileRoleType.ProfileApprover,
                                        StatusResponsibility = p.StatusType,
                                        IsProfileApproval = true,
                                        CreateAt = p.CreateAt
                                    });
            if (listProfileApproval.Count() > 0) { result = result.Concat(listProfileApproval); }
            return result.OrderByDescending(p => p.CreateAt);

        }
        /// <summary>
        /// Lấy danh sách hồ sơ cần thực hiện cho việc lưu trữ
        /// Là những công việc hiện tại cần xử lý
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProfileDasboardBO> GetDasboardArchire()
        {
            var result = Enumerable.Empty<ProfileDasboardBO>();
            var listProfileArchive = ServiceFactory<ProfileTransferService>().GetProfileSentArchive()
                                    .Select(i => new ProfileDasboardBO
                                    {
                                        Id = i.Id,
                                        ProfileId = i.ProfileId,
                                        Time = i.DateSend.HasValue ? i.DateSend.Value.ToString("dd-MM-yyyy HH:mm") : DateTime.Now.ToString("dd-MM-yyyy HH:mm"),
                                        Name = "Lưu trữ hồ sơ",
                                        RoleType = (int)Resource.ProfileRoleType.ProfileArchiver,
                                        StatusArchive = i.SendResult,
                                        CreateAt = i.DateSend
                                    });
            if (listProfileArchive.Count() > 0) { result = result.Concat(listProfileArchive); }
            return result;
        }

        public List<PieChartBO> ProfileStatusAnalytic(Guid departmentId)
        {
            var summaryData = new List<PieChartBO>();
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.ProfileStatusText.New,
                Value = ServiceFactory<ProfileService>().GetQuery().Where(c => c.Status == (int)Resource.ProfileStatus.New && c.DepartmentArchiveId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.ProfileStatusText.Update,
                Value = ServiceFactory<ProfileService>().GetQuery().Where(c => c.Status == (int)Resource.ProfileStatus.Update && c.DepartmentArchiveId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.ProfileStatusText.Close,
                Value = ServiceFactory<ProfileService>().GetQuery().Where(c => c.Status == (int)Resource.ProfileStatus.Close && c.DepartmentArchiveId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.ProfileStatusText.Archive,
                Value = ServiceFactory<ProfileService>().GetQuery().Where(c => c.Status == (int)Resource.ProfileStatus.Archive && c.DepartmentArchiveId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.ProfileStatusText.Destroy,
                Value = ServiceFactory<ProfileService>().GetQuery().Where(c => c.Status == (int)Resource.ProfileStatus.Destroy && c.DepartmentArchiveId == departmentId).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.ProfileStatusText.WaitDestroy,
                Value = ServiceFactory<ProfileService>().GetQuery().Where(c => c.Status == (int)Resource.ProfileStatus.WaitDestroy && c.DepartmentArchiveId == departmentId).Count()
            });
            return summaryData;
        }

        public List<PieChartBO> ProfileBorrowAnalytic(Guid departmentId)
        {
            var profileIds = ServiceFactory<ProfileService>().GetQuery().Where(c => c.DepartmentArchiveId == departmentId).Select(c => c.Id).ToList();
            var summaryData = new List<PieChartBO>();
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.ProfileBorrowStatusText.WaitAssign,
                Value = ServiceFactory<ProfileBorrowService>().GetQuery().Where(c => c.Status == (int)Resource.ProfileBorrowStatus.WaitAssign && profileIds.Any(p => p == c.ProfileId)).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.ProfileBorrowStatusText.Borrowing,
                Value = ServiceFactory<ProfileBorrowService>().GetQuery().Where(c => c.Status == (int)Resource.ProfileBorrowStatus.Borrowing && profileIds.Any(p => p == c.ProfileId)).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.ProfileBorrowStatusText.Extend,
                Value = ServiceFactory<ProfileBorrowService>().GetQuery().Where(c => c.Status == (int)Resource.ProfileBorrowStatus.Extend && profileIds.Any(p => p == c.ProfileId)).Count()
            });
            summaryData.Add(new PieChartBO()
            {
                Name = Resource.ProfileBorrowStatusText.OverTimeReturn,
                Value = ServiceFactory<ProfileBorrowService>().GetQuery().Where(c => c.Status == (int)Resource.ProfileBorrowStatus.OverTimeReturn && profileIds.Any(p => p == c.ProfileId)).Count()
            });
            return summaryData;
        }
    }
}
