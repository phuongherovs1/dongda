﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileDestroyReportService : BaseService<ProfileDestroyReportDTO, ProfileDestroyReportBO>, IProfileDestroyReportService
    {
        /// <summary>
        /// Tạo biên bản mặc định
        /// </summary>
        /// <returns></returns>
        public ProfileDestroyReportBO CreateDefault(string departmentId, string ProfileDestroySuggest)
        {
            try
            {
                // Kiểm tra người đăng nhập có quyền thao tác không
                if (departmentId.Contains('_'))
                {
                    var id = departmentId.Split('_').ToList();
                    if (id != null && id.Count == 2)
                    {
                        if (!ServiceFactory<ProfileDepartmentArchiveRoleService>().CheckAllowByCurentUser(new Guid(id[1])))
                        {
                            throw new AccessDenyException();
                        }
                    }
                }
                else
                {
                    if (!ServiceFactory<ProfileDepartmentArchiveRoleService>().CheckAllowByCurentUser(new Guid(departmentId)))
                    {
                        throw new AccessDenyException();
                    }
                }
                // Lấy đề nghị hủy đã duyệt được chọn
                //var destroySuggest = ServiceFactory<ProfileDestroySuggestService>().Get(p => p.IsApprove == true && p.IsAccept == true && p.Status != (int)Resource.ProfileDestroyStatus.BillCreated).OrderByDescending(p => p.CreateAt).FirstOrDefault();
                var destroySuggest = ServiceFactory<ProfileDestroySuggestService>().Get(p => p.Id.Equals(Guid.Parse(ProfileDestroySuggest)) && p.IsApprove == true && p.IsAccept == true && p.Status != (int)Resource.ProfileDestroyStatus.BillCreated).OrderByDescending(p => p.CreateAt).FirstOrDefault();
                if (destroySuggest == null)
                    throw new NotExistDestroySuggest();
                var data = base.GetQuery().Where(p => p.ProfileDestroySuggestId == destroySuggest.Id).FirstOrDefault();
                // Nếu đề nghị chưa được tạo biên bản thì tạo mới biên bản
                if (data == null)
                {
                    data = new ProfileDestroyReportBO()
                    {
                        Id = Guid.Empty,
                        ReportAt = DateTime.Now,
                        DestroyAt = DateTime.Now,
                        ProfileDestroy = destroySuggest,
                        ProfileDestroySuggestId = destroySuggest.Id,
                        EmployeeCreateBy = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(UserId),
                        EmployeeIds = string.Empty,
                        EmployeeName = string.Empty
                    };
                }
                else
                {
                    data.ProfileDestroy = destroySuggest;
                    data.EmployeeCreateBy = data.CreateBy.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.CreateBy.Value) : null;
                    // Lấy thành phần nhân sự tham gia
                    var reportDetail = ServiceFactory<ProfileDestroyReportDetailService>().GetQuery().Where(p => p.ProfileDestroyReportId == data.Id).AsEnumerable();
                    if (reportDetail != null && reportDetail.Count() > 0)
                    {
                        foreach (var item in reportDetail)
                        {
                            if (item.EmployeeId.HasValue)
                            {
                                data.EmployeeIds += "Employee_" + item.EmployeeId + ",";
                                var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.EmployeeId.Value);
                                data.EmployeeName += employee.Name + ",";
                            }
                        }
                        data.EmployeeIds = data.EmployeeIds.Remove(data.EmployeeIds.Length - 1, 1);
                        data.EmployeeName = data.EmployeeName.Remove(data.EmployeeName.Length - 1, 1);
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Lưu biên bản hủy
        /// </summary>
        /// <param name="data"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public Guid Save(ProfileDestroyReportBO data, string employeeId)
        {
            try
            {
                data.ReportAt = DateTime.Now;
                data.Id = base.Insert(data);
                // Insert thành phần tham gia
                ServiceFactory<ProfileDestroyReportDetailService>().Save(data.Id, employeeId, true);
                // Cập nhật hồ sơ của đề nghị về trạng thái đã hủy
                var profileDestroy = ServiceFactory<ProfileDestroyDetailService>().Get(p => p.ProfileDestroySuggestId == data.ProfileDestroySuggestId).AsEnumerable();
                if (profileDestroy != null && profileDestroy.Count() > 0)
                {
                    foreach (var item in profileDestroy)
                    {
                        item.Status = (int)Resource.ProfileStatus.Destroy;
                        ServiceFactory<ProfileDestroyDetailService>().Update(item);
                        // Cập nhật hồ sơ sang trạng thái Hủy
                        var profile = ServiceFactory<ProfileService>().GetById(item.ProfileId.Value);
                        if (profile != null)
                        {
                            profile.Status = (int)Resource.ProfileStatus.Destroy;
                            ServiceFactory<ProfileService>().Update(profile);
                        }
                    }
                }
                // Cập nhật đề nghị hủy sang trạng thái đã lập biên bản
                var destroySuggest = ServiceFactory<ProfileDestroySuggestService>().GetById(data.ProfileDestroySuggestId.Value);
                if (destroySuggest != null)
                {
                    destroySuggest.Status = (int)Resource.ProfileDestroyStatus.BillCreated;
                    ServiceFactory<ProfileDestroySuggestService>().Update(destroySuggest);
                }
                return data.Id;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
