﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileDestroySuggestService : BaseService<ProfileDestroySuggestDTO, ProfileDestroySuggestBO>, IProfileDestroySuggestService
    {
        /// <summary>
        /// Danh sách đề nghị hủy hồ sơ
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<ProfileDestroySuggestBO> GetData(int pageIndex, int pageSize, out int count, Resource.ProfileDestroyStatus status)
        {
            try
            {
                var data = base.GetQuery().Where(p => p.CreateBy == UserId && p.IsDelete == false).AsEnumerable();
                count = data.Count();
                data = Page(data, pageIndex, pageSize);
                // Lấy hết danh sách hồ sơ của tất cả đề nghị hủy
                var profileDetail = ServiceFactory<ProfileDestroyDetailService>().GetQuery().Where(p => p.IsDelete == false);
                data = data.Select(i =>
                {
                    // Lấy những hồ sơ của đề nghị
                    var destroyProfile = profileDetail.Where(p => p.ProfileDestroySuggestId == i.Id);
                    // Đếm số hồ sơ
                    i.CountProfile = destroyProfile.Count();
                    // Lấy danh sách ProfileId của đề nghị hủy
                    var details = destroyProfile.Select(p => p.ProfileId.Value);
                    if (details != null && details.Count() > 0)
                    {
                        i.ProfileIds = details.ToList();
                    }
                    return i;
                });
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Lưu thông tin đề nghị hủy
        /// </summary>
        /// <param name="data"></param>
        /// <param name="profiles"></param>
        /// <param name="departmentId"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid Save(ProfileDestroySuggestBO data, string profiles, string departmentId, bool allowSave = true)
        {
            try
            {
                if (!String.IsNullOrEmpty(departmentId))
                {
                    if (departmentId.Contains('_'))
                    {
                        var items = departmentId.Split('_').ToList();
                        if (items != null && items.Count == 2)
                            data.DepartmentId = new Guid(items[1]);
                    }
                    else
                        data.DepartmentId = new Guid(departmentId);
                }
                data.ApproveBy = data.EmployeeApproveBy.Id;
                data.Status = data.Status.HasValue ? data.Status : (int)Resource.ProfileDestroyStatus.New;
                if (data.Id == null || data.Id == Guid.Empty)
                    data.Id = base.Insert(data);
                else
                    base.Update(data);
                // Thêm hồ sơ của đề nghị hủy
                ServiceFactory<ProfileDestroyDetailService>().InsertRange(profiles, data.Id);
                //cap nhật trạng thái của profile
                if (!String.IsNullOrEmpty(profiles))
                {
                    var listdata = new List<ProfileBO>();
                    var profiles_id = profiles.Split(',').ToList();
                    if (profiles_id != null && profiles_id.Count > 0)
                    {
                        foreach (var item in profiles_id)
                        {
                            // lấy thông tin
                            var Profileinfo = ServiceFactory<ProfileService>().GetById(Guid.Parse(item));
                            Profileinfo.Status = (int)Resource.ProfileStatus.WaitDestroy;
                            listdata.Add(Profileinfo);
                        }
                        ServiceFactory<ProfileService>().UpdateProfileMulti(listdata);
                    }
                }
                return data.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Gửi đề nghị hủy
        /// </summary>
        /// <param name="data"></param>
        /// <param name="profiles"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid Send(ProfileDestroySuggestBO data, string profiles, string departmentId, bool allowSave = true)
        {
            try
            {
                data.IsSend = true;
                data.Status = (int)Resource.ProfileDestroyStatus.WaitApprove;
                data.Id = Save(data, profiles, departmentId);
                return data.Id;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Xóa đề nghị hủy và hồ sơ của đề nghị hủy
        /// </summary>
        /// <param name="id"></param>
        public void DeleteProfileDestroy(Guid id)
        {
            try
            {
                // Xóa đề nghị hủy
                base.Delete(id);
                // Xóa hồ sơ của đề nghị hủy
                var detail = ServiceFactory<ProfileDestroyDetailService>().GetIdByProfileDestroySuggestId(id);
                ServiceFactory<ProfileDestroyDetailService>().DeleteRange(detail);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Lấy những đề nghị hủy chưa duyệt đề cập nhật, nếu chưa có thì tạo mới
        /// Kiểm tra người đăng nhập có quyền tạo đề nghị hủy trong phòng ban này không
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public ProfileDestroySuggestBO CreateDefault(string departmentId)
        {
            try
            {
                var data = base.Get(p => p.IsApprove != true && p.CreateBy == UserId).OrderByDescending(p => p.CreateAt).FirstOrDefault();
                if (!String.IsNullOrEmpty(departmentId))
                {
                    if (departmentId.Contains('_'))
                    {
                        var id = departmentId.Split('_').ToList();
                        if (id != null && id.Count == 2)
                        {
                            if (!ServiceFactory<ProfileDepartmentArchiveRoleService>().CheckAllowByCurentUser(new Guid(id[1])))
                            {
                                throw new AccessDenyException();
                            }
                        }
                    }
                    else
                    {
                        if (!ServiceFactory<ProfileDepartmentArchiveRoleService>().CheckAllowByCurentUser(new Guid(departmentId)))
                        {
                            throw new AccessDenyException();
                        }
                    }
                    if (data == null)
                    {
                        data = new ProfileDestroySuggestBO()
                        {
                            Id = Guid.Empty,
                            IsShow = false,
                            SuggestAt = DateTime.Now
                        };
                    }
                    else
                    {
                        data.ProfileIds = ServiceFactory<ProfileDestroyDetailService>().GetIdByProfileDestroySuggestId(data.Id);
                        // Người phê duyệt
                        data.EmployeeApproveBy = data.ApproveBy.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.ApproveBy.Value) : null;
                        // Người đề nghị
                        //var suggester = data.CreateBy.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.CreateBy.Value) : null;
                        //data.EmployeeSuggestBy = suggester == null ? null : suggester;
                        // Lấy danh sách ProfileId của đề nghị hủy
                        var profileDetail = ServiceFactory<ProfileDestroyDetailService>().GetQuery().Where(p => p.IsDelete == false);
                        var destroyProfile = profileDetail.Where(p => p.ProfileDestroySuggestId == data.Id);
                        var details = destroyProfile.Select(p => p.ProfileId.Value);
                        foreach (var item in details)
                        {
                            data.strProfile += item + ",";
                        }
                    }
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Lấy bản ghi để thực hiện phê duyệt
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProfileDestroySuggestBO GetForApproval(string id)
        {
            try
            {
                var data = new ProfileDestroySuggestBO();
                if (!String.IsNullOrEmpty(id))
                {
                    data = base.GetById(new Guid(id));
                    if (data == null)
                    {
                        data = new ProfileDestroySuggestBO()
                        {
                            Id = Guid.Empty
                        };
                    }
                    else
                    {
                        data.ProfileIds = ServiceFactory<ProfileDestroyDetailService>().GetIdByProfileDestroySuggestId(data.Id);
                        // Người đề nghị
                        var suggester = data.CreateBy.HasValue ? ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.CreateBy.Value) : null;
                        data.EmployeeSuggestBy = suggester == null ? null : suggester;
                        // Lấy danh sách ProfileId của đề nghị hủy
                        var profileDetail = ServiceFactory<ProfileDestroyDetailService>().GetQuery().Where(p => p.IsDelete == false);
                        var destroyProfile = profileDetail.Where(p => p.ProfileDestroySuggestId == data.Id);
                        var details = destroyProfile.Select(p => p.ProfileId.Value);
                        foreach (var item in details)
                        {
                            data.strProfile += item + ",";
                        }
                    }
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Danh sách đề nghị hủy được gửi đi đến khung phê duyệt
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProfileDestroySuggestBO> GetDestroySuggestSend()
        {
            var profile = ServiceFactory<ProfileService>().GetQuery().Select(p => p.Id);
            var data = base.GetQuery().Where(p => p.IsSend == true && p.IsDelete == false && p.IsApprove != true &&
                                                  p.IsAccept != true && p.ApproveBy == UserId).OrderByDescending(p => p.CreateAt);
            return data;
        }

        /// <summary>
        /// Phê duyệt đề nghị hủy
        /// </summary>
        /// <param name="data"></param>
        public void Approve(ProfileDestroySuggestBO data)
        {
            try
            {
                data.ApproveAt = DateTime.Now;
                data.IsApprove = true;
                if (data.IsAccept == true)
                    data.Status = (int)Resource.ProfileDestroyStatus.WaitPerform;
                else
                    data.Status = (int)Resource.ProfileDestroyStatus.Reject;
                base.Update(data);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
