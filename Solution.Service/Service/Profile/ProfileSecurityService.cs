﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileSecurityService : BaseService<ProfileSecurityDTO, ProfileSecurityBO>, IProfileSecurityService
    {
        public IEnumerable<ProfileSecurityBO> GetContain(string name)
        {
            throw new NotImplementedException();
        }

        public bool CheckExist(string name)
        {
            var check = Get(i => !string.IsNullOrEmpty(name) && i.Name.ToUpper() == name.Trim().ToUpper()).Any();
            return check;

        }
    }
}
