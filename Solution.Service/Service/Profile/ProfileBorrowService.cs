﻿using iDAS.DataAccess;
using System;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileBorrowService : BaseService<ProfileBorrowDTO, ProfileBorrowBO>, IProfileBorrowService
    {
        public ProfileBorrowBO GetByProfile(Guid profileId)
        {
            var data = base.GetQuery().Where(c => c.ProfileId == profileId && c.IsClose != true).FirstOrDefault();
            if (data != null)
            {
                if (data.BorrowEmployeeId.HasValue)
                    data.BorrowEmployee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.BorrowEmployeeId.Value);
                data.Profile = ServiceFactory<ProfileService>().GetDetailDataProfile(profileId);
                if (data.AssignEmployeeId.HasValue)
                    data.AssignEmployee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.AssignEmployeeId.Value);
            }
            return data;
        }

        public ProfileBorrowBO GetBorrowById(Guid id)
        {
            var data = base.GetById(id);
            if (data.BorrowEmployeeId.HasValue)
                data.BorrowEmployee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.BorrowEmployeeId.Value);
            if (data.ProfileId.HasValue)
                data.Profile = ServiceFactory<ProfileService>().GetDetailDataProfile(data.ProfileId.Value);
            return data;
        }


        public void Borrow(ProfileBorrowBO item)
        {
            item.BorrowEmployeeId = item.BorrowEmployee.Id;
            base.Insert(item);
        }


        public bool ExitBorrow(Guid profileId)
        {
            return base.GetQuery().Any(c => c.IsClose != true && c.ProfileId == profileId);
        }


        public void AssignProfile(ProfileBorrowBO item)
        {
            item.AssignEmployeeId = item.AssignEmployee.Id;
            item.Status = (int)iDAS.Service.Common.Resource.ProfileBorrowStatus.Borrowing;
            base.Update(item);
        }
        public void ReturnProfile(ProfileBorrowBO item)
        {
            item.Status = (int)iDAS.Service.Common.Resource.ProfileBorrowStatus.Return;
            item.IsClose = true;
            base.Update(item);
        }
    }
}
