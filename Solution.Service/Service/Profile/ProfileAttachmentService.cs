﻿using iDAS.DataAccess;
using System;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileAttachmentService : BaseService<ProfileAttachmentDTO, ProfileAttachmentBO>, IProfileAttachmentService
    {
        /// <summary>
        /// Cập nhật file đính kèm
        /// </summary>
        /// <param name="fileUpload"></param>
        /// <param name="profileId"></param>
        /// <param name="allowSave"></param>
        public void Update(FileUploadBO fileUpload, Guid profileId, bool allowSave = true)
        {
            if (fileUpload != null)
            {
                var files = GetQuery().Where(p => p.ProfileId == profileId).Select(p => p.Id);
                if (files != null && files.Count() > 0)
                    DeleteRange(files);
                //RemoveRange(fileUpload.FileRemoves, false);
                var fileIds = ServiceFactory<FileService>().UploadRange(fileUpload, false);
                var file = fileIds.Select(i => new ProfileAttachmentBO
                {
                    ProfileId = profileId,
                    FileId = i
                });
                InsertRange(file, false);
            }
            SaveTransaction(allowSave);
        }
    }
}
