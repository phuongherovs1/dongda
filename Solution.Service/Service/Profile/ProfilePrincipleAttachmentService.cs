﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfilePrincipleAttachmentService : BaseService<ProfilePrincipleAttachmentDTO, ProfilePrincipleAttachmentBO>, IProfilePrincipleAttachmentService
    {
        /// <summary>
        /// Danh sách file đính kèm theo principleId
        /// </summary>
        /// <param name="principleId"></param>
        /// <returns></returns>
        public IEnumerable<Guid> GetByPrinciple(Guid principleId)
        {
            var data = Get(i => i.PrincipleId.Value == principleId)
                .Select(i => i.FileId.Value);
            return data;
        }

        /// <summary>
        /// Lấy danh sách file theo danh sách principle
        /// </summary>
        /// <param name="principleIds"></param>
        /// <returns></returns>
        public IEnumerable<ProfilePrincipleAttachmentBO> GetByPrinciples(IEnumerable<Guid> principleIds)
        {
            var data = Get(i => i.PrincipleId.HasValue && principleIds.Contains(i.PrincipleId.Value));
            return data;
        }

        /// <summary>
        /// Cập nhật file đính kèm theo principleId
        /// </summary>
        /// <param name="fileUpload"></param>
        /// <param name="principleId"></param>
        /// <param name="allowSave"></param>
        public void Update(FileUploadBO fileUpload, Guid principleId, bool allowSave = true)
        {
            if (fileUpload != null)
            {
                RemoveRange(fileUpload.FileRemoves, false);
                var fileIds = ServiceFactory<FileService>().UploadRange(fileUpload, false);
                var data = fileIds.Select(i => new ProfilePrincipleAttachmentBO
                {
                    PrincipleId = principleId,
                    FileId = i
                });
                InsertRange(data, false);
                SaveTransaction(allowSave);
            }
        }
    }
}
