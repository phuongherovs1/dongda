﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileTransferService : BaseService<ProfileTransferDTO, ProfileTransferBO>, IProfileTransferService
    {
        public void TransferProfile(ProfileTransferBO item)
        {
            item.DateSend = DateTime.Now;
            var profileTranferId = base.Insert(item, false);
            var detail = new ProfileTransferDetailBO
            {
                ProfileTransferId = profileTranferId,
                ProfileId = item.ProfileId
            };
            var profile = ServiceFactory<ProfileService>().GetById(item.ProfileId);
            profile.Status = (int)Common.Resource.ProfileTransferStatus.WaitArchive;
            ServiceFactory<ProfileService>().Update(profile, false);
            ServiceFactory<ProfileTransferDetailService>().Insert(detail, false);
            base.SaveTransaction();
        }
        public ProfileTransferBO GetByProfile(Guid profileId)
        {
            var profileTransferId = ServiceFactory<ProfileTransferDetailService>().GetQuery()
                .Where(c => c.ProfileId == profileId)
                .OrderByDescending(c => c.CreateAt)
                .Select(c => c.ProfileTransferId)
                .FirstOrDefault();
            if (profileTransferId.HasValue)
            {
                var data = base.GetById(profileTransferId.Value);
                data.SendEmployee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.SendEmployeeId.Value);
                data.Profile = ServiceFactory<ProfileService>().GetById(profileId);
                data.Profile.TransferToDepartment = ServiceFactory<DepartmentService>().GetById(data.DepartmentArchiveId.Value);
                data.Profile.DepartmentArchive = ServiceFactory<DepartmentService>().GetById(data.SendDepartmentId.Value);
                return data;
            }
            else
            {
                var profile = ServiceFactory<ProfileService>().GetById(profileId);
                profile.DepartmentArchive = ServiceFactory<DepartmentService>().GetById(profile.DepartmentArchiveId.Value);
                return new ProfileTransferBO()
                {
                    Profile = profile,
                    SendEmployee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(UserId),
                    DateSend = DateTime.Now,
                    DateReceive = DateTime.Now,
                    SendDepartmentId = profile.DepartmentArchiveId
                };
            }
        }
        public void ReceiveProfile(ProfileTransferBO item)
        {
            item.EmployeeArchiveId = item.ArchiveEmployee.Id;
            item.SendResult = (int)iDAS.Service.Common.Resource.ESendResult.Archived;
            base.Update(item, false);
            var profile = ServiceFactory<ProfileService>().GetById(item.ProfileId);
            profile.Status = (int)Common.Resource.ProfileTransferStatus.Archive;
            ServiceFactory<ProfileService>().Update(profile, false);
            SaveTransaction(true);
        }

        /// <summary>
        /// Danh sách hồ sơ lưu trữ gửi đến người lưu trữ
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProfileTransferBO> GetProfileSentArchive()
        {
            var detail = ServiceFactory<ProfileTransferDetailService>().GetQuery();
            var data = base.GetQuery().Where(p => p.EmployeeArchiveId == UserId && p.SendResult == (int)iDAS.Service.Common.Resource.ESendResult.WaitArchive).AsEnumerable();
            data = data.Select(i =>
                        {
                            var profileDetail = detail.Where(n => n.ProfileTransferId == i.Id).FirstOrDefault();
                            if (profileDetail != null)
                            {
                                var profile = ServiceFactory<ProfileService>().GetById(profileDetail.ProfileId.Value);
                                if (profile != null)
                                    i.ProfileId = profile.Id;
                            }
                            return i;
                        }).Where(p => p.ProfileId != null && p.ProfileId != Guid.Empty);
            return data;
        }
    }
}
