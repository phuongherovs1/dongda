﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfilePrincipleService : BaseService<ProfilePrincipleDTO, ProfilePrincipleBO>, IProfilePrincipleService
    {
        /// <summary>
        /// Danh sách hồ sơ nguyên tắc theo danh mục
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="categoryId"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<ProfilePrincipleBO> GetByCategory(int pageIndex, int pageSize, out int count, Guid categoryId, string query = default(string))
        {
            try
            {
                // Lấy danh sách hồ sơ nguyên tắc theo danh mục và theo người tạo
                var data = base.GetQuery().Where(p => p.CategoryId == categoryId && p.CreateBy == UserId).AsEnumerable();
                if (!String.IsNullOrEmpty(query))
                {
                    data = data.Where(p => p.Name.ToLower().Contains(query.ToLower()));
                }
                count = data.Count();
                data = Page(data, pageIndex, pageSize);
                var documentIds = data.Where(i => i.DocumentPublishId.HasValue).Select(i => i.DocumentPublishId.Value);
                var documents = ServiceFactory<DocumentService>().GetByIds(documentIds);
                data = data.Select(i =>
                {
                    if (i.DocumentPublishId.HasValue)
                    {
                        var document = documents.FirstOrDefault(u => i.DocumentPublishId == u.Id);
                        i.DocumentName = document.Name;
                    }
                    var temp = ServiceFactory<ProfilePrincipleAttachmentService>().GetByPrinciple(i.Id);
                    i.FileAttachments = new FileUploadBO()
                    {
                        Files = temp.ToList()
                    };
                    return i;
                }).OrderBy(p => p.Order);
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Lấy thông tin chi tiết
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public ProfilePrincipleBO GetDetail(string id, string departmentId, string categoryId)
        {
            try
            {
                var data = new ProfilePrincipleBO();
                if (!String.IsNullOrEmpty(id))
                {
                    data = GetById(new Guid(id));
                    var departmentName = data.DepartmentId.HasValue ? ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value) : null;
                    data.DepartmentName = departmentName == null ? string.Empty : departmentName.Name;
                    var files = ServiceFactory<ProfilePrincipleAttachmentService>().GetByPrinciple(data.Id);
                    data.FileAttachments = new FileUploadBO()
                    {
                        Files = files.ToList()
                    };
                }
                else
                {
                    var department = new DepartmentBO();
                    var cateId = string.Empty;
                    if (!String.IsNullOrEmpty(departmentId))
                    {
                        if (departmentId.Contains("_"))
                        {
                            var items = departmentId.Split('_').ToList();
                            if (items != null && items.Count == 2)
                            {
                                department = ServiceFactory<DepartmentService>().GetById(new Guid(items[1]));
                            }
                        }
                        else
                        {
                            department = ServiceFactory<DepartmentService>().GetById(new Guid(departmentId));
                        }
                    }
                    if (!String.IsNullOrEmpty(categoryId))
                    {
                        cateId = categoryId;
                    }
                    data = new ProfilePrincipleBO()
                    {
                        Id = Guid.Empty,
                        CategoryId = cateId == string.Empty ? (Guid?)null : new Guid(cateId),
                        DepartmentId = department == null ? (Guid?)null : department.Id,
                        DepartmentName = department == null ? string.Empty : department.Name
                    };
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Thêm mới hồ sơ nguyên tắc
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid InsertProfile(ProfilePrincipleBO data)
        {
            try
            {
                // Mặc định sử dụng
                data.IsUse = true;
                var order = base.GetQuery().Where(p => p.IsDelete == false).OrderByDescending(p => p.Order).FirstOrDefault();
                data.Order = order == null ? 1 : (order.Order + 1);
                data.Id = base.Insert(data, false);
                // Cập nhật đính kèm
                ServiceFactory<ProfilePrincipleAttachmentService>().Update(data.FileAttachments, data.Id, false);
                SaveTransaction(true);
                return data.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Cập nhật 
        /// </summary>
        /// <param name="data"></param>
        public void UpdateProfile(ProfilePrincipleBO data)
        {
            try
            {
                base.Update(data, false);
                ServiceFactory<ProfilePrincipleAttachmentService>().Update(data.FileAttachments, data.Id, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Xóa hồ sơ nguyên tắc và xóa cả file đính kèm
        /// </summary>
        /// <param name="id"></param>
        public void DeleteProfile(Guid id)
        {
            try
            {
                // Xóa hồ sơ nguyên tắc
                base.Delete(id, false);
                // Lấy file đính kèm của hồ sơ nguyên tắc
                var files = ServiceFactory<ProfilePrincipleAttachmentService>().Get(p => p.PrincipleId == id).Select(p => p.Id);
                // Xóa những file đã lấy được ở trên
                ServiceFactory<ProfilePrincipleAttachmentService>().DeleteRange(files, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Lấy danh sách tài liệu ban hành
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DocumentPublishBO> GetDocumentPublish()
        {
            try
            {
                var data = ServiceFactory<DocumentPublishService>().GetQuery().Where(p => p.IsPublish == true && p.IsObsolete != true && p.IsDelete == false).AsEnumerable();
                var documentIds = data.Where(i => i.DocumentId.HasValue).Select(i => i.DocumentId.Value);
                var documents = ServiceFactory<DocumentService>().GetByIds(documentIds);
                data = data.Select(i =>
                {
                    if (i.DocumentId.HasValue)
                    {
                        var document = documents.FirstOrDefault(u => i.DocumentId == u.Id);
                        if (document != null)
                        {
                            i.DocumentName = document.Name;
                            i.DocumentId = document.Id;
                        }
                    }
                    return i;
                }).OrderBy(p => p.DocumentName);
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Sắp xếp tăng
        /// </summary>
        /// <param name="id"></param>
        public void SortASC(Guid id)
        {
            try
            {
                var data = base.GetById(id);
                var order = data.Order;
                if (data != null)
                {
                    var preOrder = base.GetQuery().Where(p => p.Order == data.Order - 1).FirstOrDefault();
                    if (preOrder != null)
                    {
                        data.Order = preOrder.Order;
                        base.Update(data, false);
                        preOrder.Order = order;
                        base.Update(preOrder, false);
                        SaveTransaction(true);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Sắp xếp giảm
        /// </summary>
        /// <param name="id"></param>
        public void SortDESC(Guid id)
        {
            try
            {
                var data = base.GetById(id);
                var order = data.Order;
                if (data != null)
                {
                    var nextOrder = base.GetQuery().Where(p => p.Order == data.Order + 1).FirstOrDefault();
                    if (nextOrder != null)
                    {
                        data.Order = nextOrder.Order;
                        base.Update(data, false);
                        nextOrder.Order = order;
                        base.Update(nextOrder, false);
                        SaveTransaction(true);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Sử dụng
        /// </summary>
        /// <param name="id"></param>
        public void Using(Guid id)
        {
            try
            {
                var data = base.GetById(id);
                if (data != null)
                {
                    data.IsUse = true;
                    base.Update(data);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Không sử dụng
        /// </summary>
        /// <param name="id"></param>
        public void NotUse(Guid id)
        {
            try
            {
                var data = base.GetById(id);
                if (data != null)
                {
                    data.IsUse = false;
                    base.Update(data);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Kiểm tra người đăng nhập có phải là người tạo danh mục đang chọn hay không
        /// Nếu đúng thì hiện button thêm hồ sơ nguyên tắc, ngược lại ẩn button
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public bool ShowOrHideButton(string categoryId)
        {
            try
            {
                if (!String.IsNullOrEmpty(categoryId))
                {
                    var category = ServiceFactory<ProfileCategoryService>().GetById(new Guid(categoryId));
                    if (category.CreateBy == UserId)
                        return false;
                    else
                        return true;
                }
                else
                    return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        // Lấy tên tài liệu theo mã  quy tắc
        public string GetDocumentName(Guid id)
        {
            var result = "";
            var documentId = GetById(id).DocumentPublishId;
            if (documentId.HasValue)
            {
                result = ServiceFactory<DocumentService>().GetById(documentId.Value).Name;
            }
            return result;
        }
    }
}
