﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileComponentAttachmentService : BaseService<ProfileComponentAttachmentDTO, ProfileComponentAttachmentBO>, IProfileComponentAttachmentService
    {
        /// <summary>
        /// Danh sách file đính kèm theo ComponentId
        /// </summary>
        /// <param name="ComponentId"></param>
        /// <returns></returns>
        public IEnumerable<Guid> GetByComponent(Guid ComponentId)
        {
            var data = Get(i => i.ProfileComponentId.Value == ComponentId)
                .Select(i => i.FileId.Value);
            return data;
        }

        /// <summary>
        /// Lấy danh sách file theo danh sách Component
        /// </summary>
        /// <param name="ComponentIds"></param>
        /// <returns></returns>
        public IEnumerable<ProfileComponentAttachmentBO> GetByComponents(IEnumerable<Guid> ComponentIds)
        {
            var data = Get(i => i.ProfileComponentId.HasValue && ComponentIds.Contains(i.ProfileComponentId.Value));
            return data;
        }

        /// <summary>
        /// Cập nhật file đính kèm theo ComponentId
        /// </summary>
        /// <param name="fileUpload"></param>
        /// <param name="componentId"></param>
        /// <param name="allowSave"></param>
        public void Update(FileUploadBO fileUpload, Guid componentId, bool allowSave = true)
        {
            if (fileUpload != null)
            {
                RemoveRange(fileUpload.FileRemoves, false);
                var fileIds = ServiceFactory<FileService>().UploadRange(fileUpload, false);
                var data = fileIds.Select(i => new ProfileComponentAttachmentBO
                {
                    ProfileComponentId = componentId,
                    FileId = i
                });
                InsertRange(data, false);
                SaveTransaction(allowSave);
            }
        }
    }
}
