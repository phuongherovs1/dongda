﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileAssignService : BaseService<ProfileAssignDTO, ProfileAssignBO>, IProfileAssignService
    {
        /// <summary>
        /// Lấy danh sách thông tin nhân sự cập nhật hồ sơ theo id của hồ sơ
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="categoryId"></param>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public IEnumerable<ProfileAssignBO> GetByProfile(int pageIndex, int pageSize, out int count, Guid profileId, Common.Resource.ProfileAssignStatus status, string query = default(string))
        {
            var profiles = Get(i => i.ProfileId == profileId && (status == Resource.ProfileAssignStatus.All ? true : i.StatusType == (int)status));
            var profileComponent = ServiceFactory<ProfileComponentService>().GetQuery().Where(p => p.ProfileId == profileId);
            var employees = ServiceFactory<EmployeeService>().Get(i => string.IsNullOrEmpty(query) || (!string.IsNullOrEmpty(i.Name) && query.Contains(i.Name)));
            var result = profiles.Join(employees, p => p.PerformBy, e => e.Id,
                                        (p, e) => new ProfileAssignBO()
                                        {
                                            Id = p.Id,
                                            AssignAt = p.AssignAt,
                                            DateUpdateProfile = p.DateUpdateProfile,
                                            StartAt = p.StartAt,
                                            StatusType = p.StatusType,
                                            PerformBy = p.PerformBy,
                                            CountComponent = profileComponent.Where(i => i.EmployeeId == p.PerformBy).Count() > 0 ?
                                                                     profileComponent.Where(i => i.EmployeeId == p.PerformBy).Count() + " thành phần" : string.Empty,
                                            EmployeePerformBy = new EmployeeBO()
                                            {
                                                Id = e.Id,
                                                Avatar = e.Avatar,
                                                Name = e.Name,
                                                RoleNames = e.RoleNames
                                            }
                                        }
                                     );
            result = Page(result, pageIndex, pageSize);
            count = result.Count();
            var data = Enumerable.Empty<ProfileAssignBO>();
            if (count > 0)
                data = result.Select(
                       i =>
                       {
                           i.EmployeePerformBy.RoleNames = i.PerformBy.HasValue ? string.Join(",", ServiceFactory<EmployeeService>().GetRoleNames(i.PerformBy.Value)) : "";
                           return i;
                       }
                    );
            return data;
        }
        /// <summary>
        /// Lấy chi tiết thông tin phân công
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProfileAssignBO GetDetail(Guid id)
        {
            // Lấy danh sách nhân sự có trách nhiệm
            var responsibilities = ServiceFactory<ProfileResponsibilityService>().GetQuery();
            // Lấy danh sách nhân sự được phân công
            var asign = ServiceFactory<ProfileAssignService>().GetQuery();
            // Thông tin nhân sự được phân công
            var data = base.GetById(id);
            if (data.ProfileId.HasValue)
            {
                var profile = ServiceFactory<ProfileService>().GetById(data.ProfileId.Value);
                // Hiển thị chức năng chi tiết nếu người đăng nhập là: Người tạo hồ sơ, Người lập danh mục, Người được phân công, Người có trách nhiệm liên quan
                // Kiểm tra người đăng nhập có phải là người tạo hồ sơ không
                if (profile.CreateBy != UserId)
                {
                    var category = profile.CategoryId.HasValue ? ServiceFactory<ProfileCategoryService>().GetById(profile.CategoryId.Value) : null;
                    if (category != null)
                    {
                        // Nếu người đăng nhập không phải là người tạo hồ sơ thì kiểm tra người đăng nhập có phải người tạo danh mục không
                        if (category.CreateBy != UserId)
                        {
                            var employeeAsign = asign.Where(p => p.ProfileId == profile.Id);
                            // Nếu người đăng nhập không phải là người tạo danh mục thì kiểm tra người đăng nhập có phải người được phân công không
                            if (!employeeAsign.Any(p => p.PerformBy == UserId))
                            {
                                var responsibility = responsibilities.Where(p => p.ProfileId == profile.Id);
                                // Nếu người đăng nhập không phải là người được phân công thì kiểm tra người đăng nhập có phải người có trách nhiệm liên quan không
                                if (!responsibility.Any(p => p.EmployeeId == UserId))
                                    throw new AccessDenyException();
                            }
                        }
                    }
                    data.IsHideButtonSave = true;
                }
                else
                    data.IsHideButtonSave = false;
                data.ProfileName = profile.Name;
                if (profile.DepartmentId.HasValue)
                {
                    data.Department = new DepartmentBO()
                    {
                        Id = profile.DepartmentId.Value,
                        Name = ServiceFactory<DepartmentService>().GetById(profile.DepartmentId.Value).Name
                    };
                    data.DepartmentId = data.Department.Id;
                }
            }
            if (data.PerformBy.HasValue)
                data.EmployeePerformBy = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.PerformBy.Value);
            return data;
        }
        /// <summary>
        /// Thêm mới thông tin phân công
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid InsertProfile(ProfileAssignBO data)
        {
            data.StatusType = (int)Resource.ProfileAssignStatus.WaitSend;
            data.PerformBy = data.EmployeePerformBy.Id;
            return base.Insert(data);
        }
        /// <summary>
        /// Cập nhật thông tin phân công
        /// </summary>
        /// <param name="data"></param>
        public void UpdateProfile(ProfileAssignBO data)
        {
            data.PerformBy = data.EmployeePerformBy.Id;
            base.Update(data);
        }
        // gửi phân công, thực hiện lưu dữ liệu phân công và cập nhật trạng thái
        public void SendProfileAssign(ProfileAssignBO data)
        {
            if (data.Id == Guid.Empty)
            {
                data.PerformBy = data.EmployeePerformBy.Id;
                data.StatusType = (int)Resource.ProfileAssignStatus.WaitUpdate;
                data.AssignAt = DateTime.Now;
                base.Insert(data);
            }
            else
            {
                data.PerformBy = data.EmployeePerformBy.Id;
                data.StatusType = (int)Resource.ProfileAssignStatus.WaitUpdate;
                data.AssignAt = DateTime.Now;
                base.Update(data);
            }
        }
        // gửi phân công theo Id bản ghi. thực hiện cập nhật trạng thái cho phân công
        public void SendProfileAssignById(Guid id)
        {
            var data = base.GetById(id);
            data.StatusType = (int)Resource.ProfileAssignStatus.WaitUpdate;
            data.AssignAt = DateTime.Now;
            base.Update(data);
        }
        // Thu hồi phân công. --> Trạng thái hồ sơ trở về chờ phân công
        public void RevertAssign(Guid id)
        {
            var data = base.GetById(id);
            data.DateUpdateProfile = (DateTime?)null;
            data.StatusType = (int)Resource.ProfileAssignStatus.WaitSend;
            base.Update(data);
        }
        // Hủy phân công
        public void DestroyAssign(Guid id)
        {
            var data = base.GetById(id);
            data.StatusType = (int)Resource.ProfileAssignStatus.Destroy;
            base.Update(data);
        }

        /// <summary>
        /// Lấy thông tin hồ sơ
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ProfileBO GetProfileInfo(Guid Id)
        {
            var profile = ServiceFactory<ProfileService>().GetById(Id);
            if (profile.CreateBy != UserId)
            {
                throw new AccessDenyException();
            }
            if (profile.DepartmentId.HasValue)
            {
                profile.DepartmentName = ServiceFactory<DepartmentService>().GetById(profile.DepartmentId.Value).Name;
            }
            if (profile.CategoryId.HasValue)
            {
                profile.CategoryName = ServiceFactory<ProfileCategoryService>().GetById(profile.CategoryId.Value).Name;
            }
            return profile;
        }

        /// <summary>
        /// Gửi phân công cập nhật hồ sơ đến từng người
        /// Hồ sơ nào ở trạng thái đóng thì kết thúc việc cập nhật
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProfileAssignBO> GetSendAssign()
        {
            try
            {
                var profiles = ServiceFactory<ProfileService>().GetQuery();
                var data = base.GetQuery().Where(p => p.StatusType == (int)Resource.ProfileAssignStatus.WaitUpdate && p.PerformBy == UserId).OrderByDescending(p => p.CreateAt).AsEnumerable();
                foreach (var item in data)
                {
                    var profile = profiles.Where(i => i.Id == item.ProfileId.Value).FirstOrDefault();
                    if (profile != null)
                    {
                        if (profile.Status == (int)Resource.ProfileStatus.Close)
                        {
                            var assign = data.Where(p => p.ProfileId == profile.Id).FirstOrDefault();
                            data = assign != null ? data.Where(p => p.Id != assign.Id) : data;
                        }
                    }
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Xóa nhân sự được giao cập nhật
        /// </summary>
        /// <param name="id"></param>
        public void DeleteAssign(Guid id)
        {
            try
            {
                var data = base.GetById(id);
                if (data.CreateBy != UserId)
                    throw new AccessDenyException();
                else
                    base.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
