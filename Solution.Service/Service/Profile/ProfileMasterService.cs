﻿using iDAS.ADO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileMasterService : IProfileMasterService
    {

        private string GetTableName(string dataType)
        {
            var tableName = "";
            switch (dataType)
            {
                case "nation": tableName = "MstNationality"; break;
                case "religion": tableName = "MstReligion"; break;
                case "ethnic": tableName = "MstEthnic"; break;
                case "bloodType": tableName = "MstBloodType"; break;
                case "positionGroup": tableName = "MstYouthPosition"; break;
                case "positionParty": tableName = "MstPositionParty"; break;
                case "rankArmy": tableName = "MstPositionMilitary"; break;
                case "family": tableName = "MstFamilyRelationship"; break;
                case "contractType": tableName = "MstContractType"; break;
                case "contractStatus": tableName = "MstContractStatus"; break;
                case "discipline": tableName = "MstDisciplineCategory"; break;
                case "award": tableName = "MstAwardCategory"; break;
                case "certificateType": tableName = "MstCertificateType"; break;
                case "certificateLevel": tableName = "MstCertificateLevel"; break;
                case "educationType": tableName = "MstEducationType"; break;
                case "educationResult": tableName = "MstEducationResult"; break;
                case "educationOrg": tableName = "MstEducationOrg"; break;
                case "educationField": tableName = "MstEducationField"; break;
                case "educationLevel": tableName = "MstEducationLevel"; break;
                case "city": tableName = "MstCity"; break;
                case "district": tableName = "MstDistrict"; break;
                case "commune": tableName = "MstCommune"; break;
                case "political": tableName = "MstPoliticalTheory"; break;
                case "management": tableName = "MstGovermentManagement"; break;
                case "jobTitle": tableName = "MstJobTitle"; break;
                default: break;
            }
            return tableName;
        }



        public DataTable GetMasterDataList(string type, string superId = "", string query = "")
        {
            var tableName = GetTableName(type);
            var tb = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_MasterData_GetAll", parameter: new { table = tableName });
            var tb1 = new DataTable();

            if (superId != "")
            {
                var tr = new List<DataRow>();
                foreach (DataRow r in tb.Rows)
                {
                    var id = r.ItemArray[2].ToString();
                    if (id == superId)
                    {
                        tr.Add(r);
                    }
                }
                if (tr.Count > 0) tb1 = tr.CopyToDataTable();
                    return tb1;
            }
            return tb;
        }

        public IEnumerable<ProfileMasterBO> ToEnumerable(string type, string superId = "")
        {
            var data = GetMasterDataList(type, superId);
            var result = new List<ProfileMasterBO>();
            foreach (DataRow r in data.Rows)
            {
                var bo = new ProfileMasterBO();
                if (r.Table.Columns.Contains("Rank"))
                    bo = new ProfileMasterBO() { Id = Guid.Parse(r["Id"].ToString()), Name = r["Name"].ToString(), Rank = int.TryParse(r["Rank"].ToString(), out int i) ? i : 0, DataType = type };
                else
                    bo = new ProfileMasterBO() { Id = Guid.Parse(r["Id"].ToString()), Name = r["Name"].ToString(), Rank = 0, DataType = type };
                result.Add(bo);
            }
            return result.AsEnumerable();
        }

        public ProfileMasterBO GetSingleMasterData(string type, string id)
        {
            var tableName = GetTableName(type);
            var bo = new ProfileMasterBO();
            if (id == null || id == "")
                id = Guid.Empty.ToString();

            var dt = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_MasterData_GetSingle", parameter: new { table = tableName, id = id });
            if (dt.Rows.Count > 0)
            {
                DataRow r = dt.Rows[0];
                bo.Id = Guid.Parse(r["Id"].ToString());
                bo.Name = r["Name"].ToString();
            }
            else bo.Name = "";
            bo.DataType = type;
            return bo;
        }

        public int Insert(string id, string name, string type, string userId, int rank = default, string superId = "")
        {

            var tableName = GetTableName(type);
            return Sql_ADO.idasAdoService.ExecuteNoquery("sp_MasterData_Insert", parameter: new { table = tableName, id = id, createBy = userId, name = name, rank = rank, superId = superId });
        }

        public int Update(string id, string name, string type, string userId, int rank = default)
        {
            var tableName = GetTableName(type);
            return Sql_ADO.idasAdoService.ExecuteNoquery("sp_MasterData_Update", parameter: new { table = tableName, id = id, updateBy = userId, name = name, rank = rank });
        }
        public int Delete(string id, string type, string userId)
        {
            var tableName = GetTableName(type);
            return Sql_ADO.idasAdoService.ExecuteNoquery("sp_MasterData_Delete", parameter: new { table = tableName, id = id, updateBy = userId });
        }
    }
}
