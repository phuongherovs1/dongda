﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileDestroyReportDetailService : BaseService<ProfileDestroyReportDetailDTO, ProfileDestroyReportDetailBO>, IProfileDestroyReportDetailService
    {
        /// <summary>
        /// Thêm mới nhân sự tham gia hủy
        /// </summary>
        /// <param name="employeeIds"></param>
        /// <returns></returns>
        public IEnumerable<Guid> Save(Guid reportId, string employeeIds, bool allowSave = true)
        {
            try
            {
                var result = Enumerable.Empty<Guid>();
                if (!String.IsNullOrEmpty(employeeIds))
                {
                    var employee = employeeIds.Split(',').ToList();
                    if (employee != null && employee.Count > 0)
                    {
                        var data = new List<ProfileDestroyReportDetailBO>();
                        // Xóa thành phần nhân sự tham gia
                        var details = base.GetQuery().Where(p => p.ProfileDestroyReportId == reportId).AsEnumerable().Select(p => p.Id);
                        base.DeleteRange(details, true);
                        foreach (var item in employee)
                        {
                            if (item.Contains('_'))
                            {
                                var employeeId = item.Split('_').ToList();
                                if (employeeId != null && employeeId.Count == 2)
                                {
                                    var reportDetail = new ProfileDestroyReportDetailBO();
                                    reportDetail.ProfileDestroyReportId = reportId;
                                    reportDetail.EmployeeId = new Guid(employeeId[1]);
                                    data.Add(reportDetail);
                                }
                            }
                        }
                        result = InsertRange(data, true);
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
