﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileDestroyDetailService : BaseService<ProfileDestroyDetailDTO, ProfileDestroyDetailBO>, IProfileDestroyDetailService
    {
        /// <summary>
        /// Danh sách hồ sơ của đề nghị hủy
        /// </summary>
        /// <param name="profileDestroySuggestId"></param>
        /// <returns></returns>
        public IEnumerable<ProfileDestroyDetailBO> GetByProfileDestroySuggestId(Guid profileDestroySuggestId, string query, iDAS.Service.Common.Resource.ProfileStatus status)
        {
            try
            {
                var profiles = ServiceFactory<ProfileService>().GetAll();
                var data = base.GetQuery().Where(i => i.ProfileDestroySuggestId.Value == profileDestroySuggestId).AsEnumerable();
                data = data.Select(i =>
                {
                    if (i.ProfileId.HasValue)
                    {
                        var profile = profiles.FirstOrDefault(p => p.Id == i.ProfileId.Value);
                        var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(profile.EmployeeArchiveId.Value);
                        profile.EmployeeArchive = employee;
                        //var title = ServiceFactory<EmployeeTitleService>().Get(m => m.EmployeeId == employee.Id).FirstOrDefault();
                        //if (title != null)
                        //{
                        //    var result = ServiceFactory<DepartmentTitleService>().GetById(title.TitleId);
                        //    //profile.TitleEmploy = result == null ? string.Empty : result.Name;
                        //}
                        i.Profile = profile;
                    }
                    return i;
                });
                if (!String.IsNullOrEmpty(query))
                {
                    data = data.Where(p => p.ProfileName.ToLower().Contains(query.ToLower()) || p.ProfileCode.ToLower().Contains(query.ToLower()));
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Thêm hồ sơ của đề nghị hủy
        /// </summary>
        /// <param name="profileIds"></param>
        /// <param name="destroySuggestId"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public IEnumerable<Guid> InsertRange(string profileIds, Guid destroySuggestId, bool allowSave = true)
        {
            try
            {
                var result = Enumerable.Empty<Guid>();
                if (!String.IsNullOrEmpty(profileIds))
                {
                    var data = new List<ProfileDestroyDetailBO>();
                    var profiles = profileIds.Split(',').ToList();
                    if (profiles != null && profiles.Count > 0)
                    {
                        // Xóa những hồ sơ
                        var ids = base.Get(p => p.ProfileDestroySuggestId == destroySuggestId).Select(p => p.Id);
                        base.DeleteRange(ids);
                        foreach (var item in profiles)
                        {
                            if (!String.IsNullOrEmpty(item))
                            {
                                var detail = new ProfileDestroyDetailBO();
                                detail.ProfileId = new Guid(item);
                                detail.ProfileDestroySuggestId = destroySuggestId;
                                detail.Status = (int)Resource.ProfileStatus.WaitReview;
                                data.Add(detail);
                            }
                        }
                        result = InsertRange(data);
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Danh sách Id theo đề nghị hủy
        /// </summary>
        /// <param name="profileDestroySuggestId"></param>
        /// <returns></returns>
        public IEnumerable<Guid> GetIdByProfileDestroySuggestId(Guid profileDestroySuggestId)
        {
            try
            {
                var Ids = base.GetQuery().Where(p => p.ProfileDestroySuggestId == profileDestroySuggestId).AsEnumerable().Select(p => p.Id);
                return Ids;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Hiển thị thông tin hồ sơ để xem xét
        /// </summary>
        /// <param name="id"></param>
        public ProfileDestroyDetailBO Review(Guid id)
        {
            try
            {
                var data = base.GetById(id);
                var profile = ServiceFactory<ProfileService>().GetById(data.ProfileId.Value);
                if (profile != null)
                {
                    var department = profile.DepartmentId.HasValue ? ServiceFactory<DepartmentService>().GetById(profile.DepartmentId.Value) : null;
                    profile.DepartmentName = department == null ? string.Empty : department.Name;
                    var category = profile.CategoryId.HasValue ? ServiceFactory<ProfileCategoryService>().GetById(profile.CategoryId.Value) : null;
                    profile.CategoryName = category == null ? string.Empty : category.Name;
                    data.Profile = profile;
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Cập nhật hồ sơ của đề nghị hủy
        /// </summary>
        /// <param name="data"></param>
        public void UpdateDetroyDetail(ProfileDestroyDetailBO data)
        {
            try
            {
                // Đồng ý thì chuyển trạng thái hồ sơ thành chờ hủy
                if (data.IsAccept == true)
                {
                    data.Status = (int)Resource.ProfileStatus.WaitDestroy;
                    var profile = ServiceFactory<ProfileService>().GetById(data.ProfileId.Value);
                    if (profile != null)
                        profile.Status = (int)Resource.ProfileStatus.WaitDestroy;
                    ServiceFactory<ProfileService>().Update(profile, false);
                }
                else // Ngược lại thì chuyển trạng thái hồ sơ thành Lưu trữ
                    data.Status = (int)Resource.ProfileStatus.Archive;
                base.Update(data, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Danh sách hồ sơ hủy trong biên bản hủy
        /// </summary>
        /// <param name="profileDestroySuggestId"></param>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public IEnumerable<ProfileDestroyDetailBO> GetProfileDestroy(Guid profileDestroySuggestId, string query, Resource.ProfileDestroyDetail status)
        {
            try
            {
                var profiles = ServiceFactory<ProfileService>().GetAll();
                var data = base.GetQuery().Where(i => i.ProfileDestroySuggestId.Value == profileDestroySuggestId
                                                && (i.Status.HasValue && i.Status.Value == (int)Resource.ProfileStatus.WaitDestroy)).AsEnumerable();
                data = data.Where(p => status == Resource.ProfileDestroyDetail.Destroy ? p.IsDestroy == true :
                                       status == Resource.ProfileDestroyDetail.NotDestroy ? p.IsDestroy == false : true);
                data = data.Select(i =>
                {
                    if (i.ProfileId.HasValue)
                    {
                        var profile = profiles.FirstOrDefault(p => p.Id == i.ProfileId.Value);
                        var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(profile.EmployeeArchiveId.Value);
                        profile.EmployeeArchive = employee;
                        var title = ServiceFactory<EmployeeTitleService>().Get(m => m.EmployeeId == employee.Id).FirstOrDefault();
                        if (title != null)
                        {
                            var result = ServiceFactory<DepartmentTitleService>().GetById(title.TitleId);
                            //profile.TitleEmploy = result == null ? string.Empty : result.Name;
                        }
                        i.Profile = profile;
                    }
                    return i;
                });
                if (!String.IsNullOrEmpty(query))
                {
                    data = data.Where(p => p.ProfileName.ToLower().Contains(query.ToLower()) || p.ProfileCode.ToLower().Contains(query.ToLower()));
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Cập nhật kết quả hủy
        /// </summary>
        /// <param name="data"></param>
        public void UpdateDestroyResult(ProfileDestroyDetailBO data)
        {
            try
            {
                // Đồng ý hủy thì chuyển trạng thái hồ sơ thành hủy
                if (data.IsDestroy == true)
                {
                    data.Status = (int)Resource.ProfileStatus.Destroy;
                    var profile = ServiceFactory<ProfileService>().GetById(data.ProfileId.Value);
                    if (profile != null)
                        profile.Status = (int)Resource.ProfileStatus.Destroy;
                    ServiceFactory<ProfileService>().Update(profile, false);
                }
                base.Update(data, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
