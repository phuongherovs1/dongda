﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileService : BaseService<ProfileDTO, ProfileBO>, IProfileService
    {
        /// <summary>
        /// Lấy danh sách hồ sơ theo danh mục hồ sơ
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="categoryId"></param>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public IEnumerable<ProfileBO> GetByCategory(int pageIndex, int pageSize, out int count, Guid? categoryId, string query = default(string), Common.Resource.ProfileStatus status = 0)
        {
            try
            {
                // 1. lấy theo điều kiện
                var dataQuery = base.GetQuery()
                     .Where(i => i.CategoryId == categoryId
                        // Lọc theo trạng thái-------------------------
                        && (status == Resource.ProfileStatus.All ? true : (i.Status.Value == (int)status))
                     //----------------------------------------------------------
                     )
                     // lọc theo ô tìm kiếm
                     .Where(i => string.IsNullOrEmpty(query) || (!string.IsNullOrEmpty(query) && (!string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(query.ToLower())) ||
                            (!string.IsNullOrEmpty(i.Code) && i.Code.ToLower().Contains(query.ToLower()))))
                     //------------------
                     // Lọc theo quyền

                     //------------------
                     .OrderByDescending(i => i.CreateAt);
                count = dataQuery.Count();

                var results = Page(dataQuery, pageIndex, pageSize).ToList();
                var employeeIds = results.Where(i => i.EmployeeArchiveId.HasValue).Select(i => i.EmployeeArchiveId.Value).Distinct();
                var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();

                var securityIds = results.Where(i => i.SecurityId.HasValue).Select(i => i.SecurityId.Value).Distinct();
                var securities = ServiceFactory<ProfileSecurityService>().GetByIds(securityIds).ToList();

                var categoryIds = results.Where(i => i.CategoryId.HasValue).Select(i => i.CategoryId.Value);
                var categories = ServiceFactory<ProfileCategoryService>().GetByIds(categoryIds);

                // Lấy danh sách nhân sự có trách nhiệm
                var responsibilities = ServiceFactory<ProfileResponsibilityService>().GetQuery();
                // Lấy danh sách nhân sự được phân công
                var asign = ServiceFactory<ProfileAssignService>().GetQuery();
                results = results.Select(i =>
                        {
                            if (i.EmployeeArchiveId.HasValue)
                                i.EmployeeArchive = employees.FirstOrDefault(u => u.Id == i.EmployeeArchiveId);
                            i.Security = securities.FirstOrDefault(u => i.SecurityId == u.Id);
                            // Hiển thị chức năng chi tiết nếu người đăng nhập là: Người tạo hồ sơ, Người lập danh mục, Người được phân công, Người có trách nhiệm liên quan
                            if (i.CreateBy == UserId)
                                i.IsHiddenButton = false;
                            else
                            {
                                var category = categories.FirstOrDefault(u => u.Id == i.CategoryId.Value);
                                // Nếu người đăng nhập là người tạo danh mục thì có quyền xem chức năng Chi tiết
                                if (category.CreateBy == UserId)
                                    i.IsHiddenButton = false;
                                // Nếu không phải kiểm tra xem người đăng nhập có trách nhiệm đối với hồ sơ không
                                else
                                {
                                    var responsibility = responsibilities.Where(p => p.ProfileId == i.Id);
                                    if (responsibility.Any(p => p.EmployeeId == UserId))
                                        i.IsHiddenButton = false;
                                    // Nếu người đăng nhập không có trách nhiệm thì kiểm tra xem người đăng nhập có được phân công
                                    else
                                    {
                                        var employeeAsign = asign.Where(p => p.ProfileId == i.Id);
                                        i.IsHiddenButton = employeeAsign.Any(p => p.PerformBy == UserId) ? false : true;
                                    }
                                }
                            }
                            if (i.CreateBy == UserId)
                                i.HideContextMenuDelete = false;
                            else
                                i.HideContextMenuDelete = true;
                            return i;
                        }
                    ).ToList();

                return results;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Đếm số hồ sơ của danh mục hồ sơ
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public int CountProfilesByCategory(Guid categoryId)
        {
            return 0;
        }
        // Xem chi tiết hồ sơ
        public ProfileBO GetDetailDataProfile(Guid Id)
        {
            try
            {   // 1. lấy theo điều kiện
                var data = base.GetById(Id, allowDeleted: true);
                if (data != null)
                {
                    // mức độ mật
                    if (data.SecurityId.HasValue)
                        data.Security = ServiceFactory<ProfileSecurityService>().GetById(data.SecurityId.Value);
                    if (data.EmployeeArchiveId.HasValue)
                        data.EmployeeArchive = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.EmployeeArchiveId.Value);
                    // Danh mục hồ sơ
                    if (data.CategoryId.HasValue)
                    {
                        var cate = ServiceFactory<ProfileCategoryService>().GetById(data.CategoryId.Value);
                        data.CategoryName = cate.Name;
                        // Phòng ban chuyển lưu trữ
                        if (cate.TransferDepartmentId.HasValue && cate.IsTransferArchive == true)
                            data.TransferToDepartment = ServiceFactory<DepartmentService>().GetById(cate.TransferDepartmentId.Value);
                    }
                    // Lấy phòng ban của hồ sơ
                    data.DepartmentName = data.DepartmentId.HasValue ? ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value).Name : string.Empty;
                    if (data.DepartmentArchiveId.HasValue)
                    {
                        data.DepartmentArchive = ServiceFactory<DepartmentService>().GetById(data.DepartmentArchiveId.Value);
                        data.DepartmentArchiveName = data.DepartmentArchive.Name;
                    }
                    if (data.CreateBy == UserId)
                        data.IsHiddenButton = false;
                    else
                        data.IsHiddenButton = true;
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
        // Lấy thông tin rút của hồ sơ
        public ProfileBO GetProfileInfo(Guid id)
        {
            var profile = ServiceFactory<ProfileService>().GetById(id);
            if (profile.DepartmentId.HasValue)
            {
                profile.DepartmentName = ServiceFactory<DepartmentService>().GetById(profile.DepartmentId.Value).Name;
            }
            if (profile.CategoryId.HasValue)
            {
                profile.CategoryName = ServiceFactory<ProfileCategoryService>().GetById(profile.CategoryId.Value).Name;
            }
            return profile;
        }
        public ProfileBO CreateDefault(Guid CategoryId, Guid departmentId)
        {
            var data = new ProfileBO() { };
            try
            {
                var category = ServiceFactory<ProfileCategoryService>().GetById(CategoryId);
                data.CategoryId = category.Id;
                data.CategoryName = category.Name;
                var Department = ServiceFactory<DepartmentService>().GetById(departmentId);
                data.DepartmentId = departmentId;
                data.DepartmentName = Department.Name;
                data.ProfileCreateAt = DateTime.Now;
                data.TimeOpen = DateTime.Now;
                data.TimeClose = DateTime.Now;
                data.ArchiveDate = DateTime.Now;
                data.ArchiveDeadline = DateTime.Now;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return data;
        }

        public ProfileBO CreateDefaultForHuman(Guid departmentId, string curiculmviateId)
        {
            var data = new ProfileBO() { };
            try
            {
                if (!string.IsNullOrEmpty(curiculmviateId))
                {
                    var curiculmviate = ServiceFactory<VPHumanProfileCuriculmViateService>().GetQuery().FirstOrDefault(p => p.Id == new Guid(curiculmviateId));
                    if (curiculmviate == null)
                        throw new NotExistProfileCuriculmViate();
                    if (curiculmviate.ProfileId.HasValue)
                    {
                        data = base.GetById(curiculmviate.ProfileId.Value);
                        if (data != null)
                        {
                            data.ProfileCuriculmviateId = new Guid(curiculmviateId);
                            var departmentArchive = ServiceFactory<DepartmentService>().GetQuery().FirstOrDefault(p => p.Id == data.DepartmentArchiveId.Value);
                            data.DepartmentArchiveName = departmentArchive == null ? string.Empty : departmentArchive.Name;
                            data.strDepartmentArchiveId = departmentArchive == null ? string.Empty : "Department_" + departmentArchive.Id;
                            var Department = ServiceFactory<DepartmentService>().GetQuery().FirstOrDefault(p => p.Id == data.DepartmentId.Value);
                            data.DepartmentName = Department == null ? string.Empty : Department.Name;
                            var fileAttach = ServiceFactory<ProfileAttachmentService>().GetQuery().Where(p => p.ProfileId == data.Id).Select(p => p.FileId.Value);
                            data.FileAttachment = new FileUploadBO()
                            {
                                Files = fileAttach.ToList()
                            };
                        }
                        else
                        {
                            var Department = ServiceFactory<DepartmentService>().GetById(departmentId);
                            data.DepartmentId = departmentId;
                            data.DepartmentName = Department.Name;
                            data.ProfileCuriculmviateId = new Guid(curiculmviateId);
                            data.ProfileCreateAt = DateTime.Now;
                            data.TimeOpen = DateTime.Now;
                            data.TimeClose = DateTime.Now;
                            data.ArchiveDate = DateTime.Now;
                            data.ArchiveDeadline = DateTime.Now;
                        }
                    }
                    else
                    {
                        var Department = ServiceFactory<DepartmentService>().GetById(departmentId);
                        data.DepartmentId = departmentId;
                        data.DepartmentName = Department.Name;
                        data.ProfileCuriculmviateId = new Guid(curiculmviateId);
                        data.ProfileCreateAt = DateTime.Now;
                        data.TimeOpen = DateTime.Now;
                        data.TimeClose = DateTime.Now;
                        data.ArchiveDate = DateTime.Now;
                        data.ArchiveDeadline = DateTime.Now;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return data;
        }

        /// <summary>
        /// Lấy chi tiết thông tin hồ sơ
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProfileBO GetDetail(Guid id)
        {
            return GetById(id);
        }
        public void UpdateProfileMulti(List<ProfileBO> data)
        {
            if (data.Count > 0)
            {
                foreach (var item in data)
                {
                    base.Update(item);
                }
            }
        }
        /// <summary>
        /// Thêm mới thông tin hồ sơ
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid InsertProfile(ProfileBO data)
        {
            if (Get(i => !string.IsNullOrEmpty(i.Name) && i.Name.Trim().ToLower() == data.Name.Trim().ToLower() && i.Id != data.Id).Any())
                throw new ProfileNameHasBeenExistedException();
            data.Status = (int)Resource.ProfileStatus.New;
            data.Id = base.Insert(data, true);
            // Cập nhật đính kèm
            ServiceFactory<ProfileAttachmentService>().Update(data.FileAttachment, data.Id, true);
            // Thêm vai trò người lập hồ sơ
            var responsibility = new ProfileResponsibilityBO()
            {
                ProfileId = data.Id,
                EmployeeId = UserId,
                RoleType = (int)Resource.ProfileResponsibilityRole.Creater,
                ResponsibiltityAt = DateTime.Now,
                SendAt = DateTime.Now,
                StatusType = (int)Resource.ProfileResponsibilityStatus.Sent
            };
            ServiceFactory<ProfileResponsibilityService>().Insert(responsibility, true);
            return data.Id;
        }
        /// <summary>
        /// Cập nhật thông tin hồ sơ
        /// </summary>
        /// <param name="data"></param>
        public void UpdateProfile(ProfileBO data)
        {
            base.Update(data);
        }
        public IEnumerable<ProfileBO> GetProfileArchives(int pageIndex, int pageSize, out int count, string filterName = default(string), Resource.ProfileArchiveTypes archiveType = 0, Resource.ProfileStatus status = 0, string objectId = default(string))
        {
            var objectSplitId = Guid.Empty;
            if (objectId.Contains('_'))
                objectSplitId = new Guid(objectId.Split('_')[1]);
            else
                if (!string.IsNullOrEmpty(objectId))
                objectSplitId = new Guid(objectId);
            // Chỉ lấy những hồ sơ có trạng thái Lưu trữ
            var data = base.GetQuery().Where(p => p.Status.HasValue && (p.Status.Value == (int)Resource.ProfileStatus.Archive));
            // Lấy danh sách hồ sơ đã nộp lưu trong bảng ProfileTranferDetail
            var tranferDetail = ServiceFactory<ProfileTransferDetailService>().GetQuery();
            // filter
            data = data
                .Where(p => string.IsNullOrEmpty(filterName) || p.Name.Contains(filterName) || p.Code.Contains(filterName))
                .Where(c => (c.Status.HasValue && c.Status.Value == (int)status) || status == 0)
                .Where(c => c.ArchiveDate.HasValue)
                .Where(c => c.EmployeeArchiveId == objectSplitId || c.CreateBy == objectSplitId || c.DepartmentArchiveId == objectSplitId);
            if (archiveType == Resource.ProfileArchiveTypes.HasTime)
                data = data.Where(c => c.TypeDate != (int)Resource.ProfileArchiveTime.Permanent);
            if (archiveType == Resource.ProfileArchiveTypes.NoLimit)
                data = data.Where(c => c.TypeDate == (int)Resource.ProfileArchiveTime.Permanent);
            if (archiveType == Resource.ProfileArchiveTypes.OverTime)
                data = data.Where(c => (c.TypeDate == (int)Resource.ProfileArchiveTime.Day && DateTime.Now >= c.ArchiveDate.Value.AddDays(c.ArchiveTime.Value))
                    || (c.TypeDate == (int)Resource.ProfileArchiveTime.Month && DateTime.Now >= c.ArchiveDate.Value.AddMonths(c.ArchiveTime.Value))
                    || (c.TypeDate == (int)Resource.ProfileArchiveTime.Year && DateTime.Now >= c.ArchiveDate.Value.AddYears(c.ArchiveTime.Value)));
            count = data.Count();
            var results = Page(data.OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();

            var categoryIds = results.Where(i => i.CategoryId.HasValue).Select(i => i.CategoryId.Value);
            var categories = ServiceFactory<ProfileCategoryService>().GetByIds(categoryIds);

            var employeeCreateIds = results.Where(i => i.CreateBy.HasValue).Select(i => i.CreateBy.Value);
            var employeesCreate = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeCreateIds).ToList();

            var employeeIds = results.Where(i => i.EmployeeArchiveId.HasValue).Select(i => i.EmployeeArchiveId.Value);
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();

            var departmentIds = results.Where(i => i.DepartmentArchiveId.HasValue).Select(i => i.DepartmentArchiveId.Value);
            var departments = ServiceFactory<DepartmentService>().GetByIds(departmentIds).ToList();

            // Lấy danh sách nhân sự có trách nhiệm
            var responsibilities = ServiceFactory<ProfileResponsibilityService>().GetQuery();
            // Lấy danh sách nhân sự được phân công
            var asign = ServiceFactory<ProfileAssignService>().GetQuery();
            results = results
                    .Select(i =>
                    {
                        var category = i.CategoryId.HasValue ? categories.FirstOrDefault(u => i.CategoryId == u.Id) : null;
                        // Hồ sơ đã nộp lưu trữ thì thời gian lưu trữ là thời gian lưu trữ tại bộ phận trong danh mục
                        // Nếu hồ sơ đã nộp lưu trữ thì người lưu trữ là người nhận lưu trữ
                        if (tranferDetail.Any(p => p.ProfileId == i.Id))
                        {
                            if (category != null && category.ArchiveTime2 != null)
                            {
                                switch (category.ArchiveTime2.Value)
                                {
                                    case (int)Resource.ProfileArchiveTime.Day:
                                        i.ProfileTimeArchive = category.Times2.HasValue ? category.Times2 + " " + Resource.ArchiveTimeText.Day : string.Empty;
                                        break;
                                    case (int)Resource.ProfileArchiveTime.Month:
                                        i.ProfileTimeArchive = category.Times2.HasValue ? category.Times2 + " " + Resource.ArchiveTimeText.Month : string.Empty;
                                        break;
                                    case (int)Resource.ProfileArchiveTime.Year:
                                        i.ProfileTimeArchive = category.Times2.HasValue ? category.Times2 + " " + Resource.ArchiveTimeText.Year : string.Empty;
                                        break;
                                    case (int)Resource.ProfileArchiveTime.Permanent:
                                        i.ProfileTimeArchive = Resource.ArchiveTimeText.Permanent;
                                        break;
                                }
                            }
                            if (i.EmployeeArchiveId.HasValue)
                                i.ArchiveEmployee = employees.FirstOrDefault(u => i.EmployeeArchiveId == u.Id);
                            var tranferId = tranferDetail.FirstOrDefault(p => p.ProfileId == i.Id);
                            if (tranferId != null)
                            {
                                var tranfer = ServiceFactory<ProfileTransferService>().GetById(tranferId.ProfileTransferId.Value);
                                // Nếu hồ sơ đã nộp lưu rồi thì ngày lưu trữ là ngày tiếp nhận trong biên bản bàn giao
                                if (tranfer != null)
                                    i.ProfileArchiveDate = tranfer.DateReceive;
                            }
                        }
                        // Hồ sơ chưa nộp lưu trữ thì thời gian lưu trữ là thời gian lưu trữ tại cá nhân trong danh mục
                        // Nếu chưa nộp lưu trữ thì người lưu trữ là người tạo
                        else
                        {
                            if (category != null && category.ArchiveTime1 != null)
                            {
                                switch (category.ArchiveTime1.Value)
                                {
                                    case (int)Resource.ProfileArchiveTime.Day:
                                        i.ProfileTimeArchive = category.Times1.HasValue ? category.Times1 + " " + Resource.ArchiveTimeText.Day : string.Empty;
                                        break;
                                    case (int)Resource.ProfileArchiveTime.Month:
                                        i.ProfileTimeArchive = category.Times1.HasValue ? category.Times1 + " " + Resource.ArchiveTimeText.Month : string.Empty;
                                        break;
                                    case (int)Resource.ProfileArchiveTime.Year:
                                        i.ProfileTimeArchive = category.Times1.HasValue ? category.Times1 + " " + Resource.ArchiveTimeText.Year : string.Empty;
                                        break;
                                    case (int)Resource.ProfileArchiveTime.Permanent:
                                        i.ProfileTimeArchive = Resource.ArchiveTimeText.Permanent;
                                        break;
                                }
                            }
                            i.ArchiveEmployee = employeesCreate.FirstOrDefault(u => i.CreateBy == u.Id);
                            // Nếu hồ sơ chưa nộp lưu thì lấy ngày mở hồ sơ
                            i.ProfileArchiveDate = i.TimeOpen;
                        }
                        // Thời hạn lưu trữ là thời gian lưu trữ tại bộ phận lấy trong danh mục
                        if (category != null && category.ArchiveTime2 != null)
                        {
                            switch (category.ArchiveTime2.Value)
                            {
                                case (int)Resource.ProfileArchiveTime.Day:
                                    i.DeadlineArchiveText = category.Times2.HasValue ? category.Times2 + " " + Resource.ArchiveTimeText.Day : string.Empty;
                                    break;
                                case (int)Resource.ProfileArchiveTime.Month:
                                    i.DeadlineArchiveText = category.Times2.HasValue ? category.Times2 + " " + Resource.ArchiveTimeText.Month : string.Empty;
                                    break;
                                case (int)Resource.ProfileArchiveTime.Year:
                                    i.DeadlineArchiveText = category.Times2.HasValue ? category.Times2 + " " + Resource.ArchiveTimeText.Year : string.Empty;
                                    break;
                                case (int)Resource.ProfileArchiveTime.Permanent:
                                    i.DeadlineArchiveText = Resource.ArchiveTimeText.Permanent;
                                    break;
                            }
                        }
                        if (i.DepartmentArchiveId.HasValue)
                            i.DepartmentArchiveName = departments.FirstOrDefault(u => i.DepartmentArchiveId == u.Id).Name;
                        if (i.CreateBy == UserId)
                            i.HideContextMenuDelete = false;
                        else
                            i.HideContextMenuDelete = true;

                        // Hiển thị chức năng chi tiết nếu người đăng nhập là: Người tạo hồ sơ, Người lập danh mục, Người được phân công, Người có trách nhiệm liên quan
                        if (i.CreateBy == UserId)
                            i.IsHiddenButton = false;
                        else
                        {
                            if (category != null)
                            {
                                // Nếu người đăng nhập là người tạo danh mục thì có quyền xem chức năng Chi tiết
                                if (category.CreateBy == UserId)
                                    i.IsHiddenButton = false;
                                // Nếu không phải kiểm tra xem người đăng nhập có trách nhiệm đối với hồ sơ không
                                else
                                {
                                    var responsibility = responsibilities.Where(p => p.ProfileId == i.Id);
                                    if (responsibility.Any(p => p.EmployeeId == UserId))
                                        i.IsHiddenButton = false;
                                    // Nếu người đăng nhập không có trách nhiệm thì kiểm tra xem người đăng nhập có được phân công
                                    else
                                    {
                                        var employeeAsign = asign.Where(p => p.ProfileId == i.Id);
                                        i.IsHiddenButton = employeeAsign.Any(p => p.PerformBy == UserId) ? false : true;
                                    }
                                }
                            }
                        }
                        return i;
                    }).ToList();

            return results.Distinct();
        }
        public IEnumerable<ProfileBO> GetProfileBorrows(int pageIndex, int pageSize, out int count, string filterName = default(string), Resource.ProfileBorrowStatus status = 0, string objectId = default(string))
        {
            var objectSplitId = Guid.Empty;
            if (objectId.Contains('_'))
                objectSplitId = new Guid(objectId.Split('_')[1]);
            else
                if (!string.IsNullOrEmpty(objectId))
                objectSplitId = new Guid(objectId);

            var result = Enumerable.Empty<ProfileBO>();

            var profileIds = ServiceFactory<ProfileBorrowService>().GetQuery().Where(c => c.IsClose != true)
                .Where(c => (c.Status.HasValue && c.Status.Value == (int)status) || status == 0)
                .Select(c => c.ProfileId)
                .Distinct();

            result = base.GetQuery()
                .Where(c => profileIds.Any(a => a == c.Id))
                .Where(p => string.IsNullOrEmpty(filterName) || p.Name.Contains(filterName) || p.Code.Contains(filterName))
                .Where(c => c.EmployeeArchiveId == objectSplitId || c.DepartmentArchiveId == objectSplitId);
            count = result.Count();

            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            data = data.Select(i =>
            {
                i.ProfileBorrow = ServiceFactory<ProfileBorrowService>().GetByProfile(i.Id);
                return i;
            });
            return data;
        }
        public IEnumerable<ProfileBO> GetProfileDestroys(int pageIndex, int pageSize, out int count, string filterName = default(string), string objectId = default(string))
        {
            var objectSplitId = Guid.Empty;
            if (objectId.Contains('_'))
                objectSplitId = new Guid(objectId.Split('_')[1]);
            else
                if (!string.IsNullOrEmpty(objectId))
                objectSplitId = new Guid(objectId);

            var result = Enumerable.Empty<ProfileBO>();

            var profileIds = ServiceFactory<ProfileDestroyDetailService>().GetQuery()
                .Where(c => c.Status == (int)Resource.ProfileStatus.Destroy)
                .Select(c => c.ProfileId).Distinct();

            result = base.GetQuery()
                .Where(c => profileIds.Any(a => a == c.Id))
                .Where(p => string.IsNullOrEmpty(filterName) || p.Name.Contains(filterName) || p.Code.Contains(filterName))
                .Where(c => c.EmployeeArchiveId.Value.CompareTo(objectSplitId) > 0 || c.DepartmentArchiveId.Value.CompareTo(objectSplitId) > 0);
            count = result.Count();

            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            var departmentIds = data.Where(i => i.DepartmentArchiveId.HasValue).Select(i => i.DepartmentArchiveId.Value);
            var departments = ServiceFactory<DepartmentService>().GetByIds(departmentIds);
            data = data.Select(i =>
            {
                if (i.DepartmentArchiveId.HasValue)
                    i.DepartmentArchiveName = departments.FirstOrDefault(u => i.DepartmentArchiveId == u.Id).Name;
                var profileDestroySuggestId = ServiceFactory<ProfileDestroyDetailService>().GetQuery().Where(c => c.ProfileId == i.Id).Select(c => c.ProfileDestroySuggestId).FirstOrDefault();
                if (profileDestroySuggestId.HasValue)
                {
                    var profileDestroySuggest = ServiceFactory<ProfileDestroySuggestService>().GetById(profileDestroySuggestId.Value);
                    var report = ServiceFactory<ProfileDestroyReportService>().GetQuery().Where(c => c.ProfileDestroySuggestId == profileDestroySuggestId).FirstOrDefault();
                    var detail = ServiceFactory<ProfileDestroyDetailService>().GetQuery().Where(p => p.ProfileDestroySuggestId == profileDestroySuggest.Id).AsEnumerable();
                    if (report != null)
                    {
                        i.DestroyAt = report.DestroyAt;
                        i.DestroyName = profileDestroySuggest.ReasonOfSuggest + " ( " + detail.Count() + " hồ sơ)" + " - ngày hủy: " + String.Format("{0:dd/MM/yyyy HH:mm}", i.DestroyAt.Value);
                    }
                }
                return i;
            });
            return data;
        }


        /// <summary>
        /// Danh sách hồ sơ chưa được tạo đề nghị hủy để thêm vào đề nghị hủy
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<ProfileBO> GetProfileWaitDestroyForAddDestroySuggest(int pageIndex, int pageSize, out int count)
        {
            try
            {
                // Danh sách hồ sơ đã tạo đề nghị hủy
                var profileDestroy = ServiceFactory<ProfileDestroyDetailService>().GetAll().Select(p => p.ProfileId.Value);
                // Lấy những hồ sơ có trạng thái là chờ hủy và chưa tạo đề nghị hủy
                var data = base.GetQuery().Where(p => p.Status.Value == (int)Resource.ProfileStatus.Archive && p.IsDelete == false
                                                       && !profileDestroy.Contains(p.Id)).AsEnumerable();
                count = data.Count();
                data = Page(data, pageIndex, pageSize);
                var employees = ServiceFactory<EmployeeService>().GetByIds(data.Select(i => i.EmployeeArchiveId.Value).Distinct());
                var sercuritys = ServiceFactory<ProfileSecurityService>().GetByIds(data.Select(i => i.SecurityId.Value).Distinct());
                // 4. lấy dữ liệu
                data = data.Select(i =>
                {
                    var employee = employees.Where(u => u.Id == i.EmployeeArchiveId).FirstOrDefault();
                    var sercurity = sercuritys.Where(u => u.Id == i.SecurityId).FirstOrDefault();
                    if (employee != null)
                    {
                        i.Security = sercurity;
                    }
                    if (employee != null)
                    {
                        i.EmployeeArchive = employee;
                        var title = ServiceFactory<EmployeeTitleService>().Get(m => m.EmployeeId == employee.Id).FirstOrDefault();
                        if (title != null)
                        {
                            var result = ServiceFactory<DepartmentTitleService>().GetById(title.TitleId);
                            //   i.TitleEmploy = result.Name;
                        }
                    }
                    i.UserId = UserId;
                    return i;
                });
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// xóa hồ sơ
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        public void DeleteProfile(Guid Id, bool allowSave = true)
        {
            try
            {
                base.Delete(Id, false);
                SaveTransaction(allowSave);
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Danh sách hồ sơ lưu chuyển
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filterName"></param>
        /// <param name="status"></param>
        /// <param name="objectId"></param>
        /// <returns></returns>
        public IEnumerable<ProfileBO> GetProfileTransfers(int pageIndex, int pageSize, out int count, string filterName = default(string), Resource.ProfileTransferStatus status = 0, string objectId = default(string))
        {
            var objectSplitId = Guid.Empty;
            if (objectId.Contains('_'))
                objectSplitId = new Guid(objectId.Split('_')[1]);
            else
                if (!string.IsNullOrEmpty(objectId))
                objectSplitId = new Guid(objectId);

            var cateIds = ServiceFactory<ProfileCategoryService>().GetQuery().Where(c => c.IsTransferArchive == true).Select(c => c.Id);
            var result = Enumerable.Empty<ProfileBO>();
            result = base.GetQuery()
                .Where(p => string.IsNullOrEmpty(filterName) || p.Name.Contains(filterName) || p.Code.Contains(filterName))
                .Where(c => c.EmployeeArchiveId == objectSplitId || c.DepartmentArchiveId == objectSplitId)
                .Where(c => cateIds.Any(x => x == c.CategoryId))
                .Where(c => c.TypeDate.HasValue && c.TypeDate.Value != (int)Resource.ProfileArchiveTime.Permanent);

            if ((int)status != 0)
                result = result.Where(c => c.Status.HasValue && c.Status.Value == (int)status);
            // hết thời gian lưu trữ thì mới cho phép lưu chuyển sang phần khác
            //result = result.Where(c => (c.TypeDate == (int)Resource.ProfileArchiveTime.Day && DateTime.Now >= c.ArchiveDate.Value.AddDays(c.ArchiveTime.Value))
            //        || (c.TypeDate == (int)Resource.ProfileArchiveTime.Month && DateTime.Now >= c.ArchiveDate.Value.AddMonths(c.ArchiveTime.Value))
            //        || (c.TypeDate == (int)Resource.ProfileArchiveTime.Year && DateTime.Now >= c.ArchiveDate.Value.AddYears(c.ArchiveTime.Value))
            //        );

            count = result.Count();
            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            var departmentArchiveIds = data.Where(c => c.DepartmentArchiveId.HasValue).Select(c => c.DepartmentArchiveId.Value);
            var departmentArchives = ServiceFactory<DepartmentService>().GetByIds(departmentArchiveIds);
            data = data.Select(i =>
            {
                if (i.DepartmentArchiveId.HasValue)
                    i.DepartmentArchiveName = departmentArchives.FirstOrDefault(u => u.Id == i.DepartmentArchiveId.Value).Name;
                i.ProfileTransfer = ServiceFactory<ProfileTransferService>().GetByProfile(i.Id);
                return i;
            });
            return data;
        }
        /// <summary>
        /// Đếm số hồ sơ trong danh mục
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public int CountByCategory(Guid categoryId)
        {
            var result = Get(i => i.CategoryId == categoryId).Count();
            return result;
        }
        /// <summary>
        /// Danh sách hồ sơ chờ hủy
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filterName"></param>
        /// <param name="archiveType"></param>
        /// <param name="status"></param>
        /// <param name="objectId"></param>
        /// <returns></returns>
        public IEnumerable<ProfileBO> GetProfileWaitDestroy(int pageIndex, int pageSize, out int count, string filterName = default(string), Resource.ProfileArchiveTypes archiveType = 0, Resource.ProfileStatus status = 0, string objectId = default(string))
        {
            // Lấy tất cả danh sách hồ sơ đã tạo đề nghị hủy
            var profileDestroy = ServiceFactory<ProfileDestroyDetailService>().GetQuery().AsEnumerable();
            // Kiểm tra đề nghị hủy gần nhất đã phê duyệt chưa
            //var destroySuggest = ServiceFactory<ProfileDestroySuggestService>().GetQuery().Where(p => p.IsApprove == true && p.IsDelete == false).OrderByDescending(p => p.CreateAt).AsEnumerable().FirstOrDefault();
            var objectSplitId = Guid.Empty;
            if (objectId.Contains('_'))
                objectSplitId = new Guid(objectId.Split('_')[1]);
            else
                if (!string.IsNullOrEmpty(objectId))
                objectSplitId = new Guid(objectId);

            var result = Enumerable.Empty<ProfileBO>();
            // Lấy những hồ sơ có trạng thái Chờ hủy
            result = base.GetQuery()
                .Where(p => p.Status.HasValue && (p.Status.Value == (int)Resource.ProfileStatus.WaitDestroy));

            ////Nếu đề nghị hủy gần nhất chưa duyệt thì không lấy những hồ sơ đã tạo đề nghị hủy của đề nghị hủy đấy
            //if (destroySuggest == null)
            //{
            //    //profileDestroy = profileDestroy.Where(p => p.ProfileDestroySuggestId == destroySuggest.Id).AsEnumerable();
            //    result = result.Where(p => !profileDestroy.Select(i => i.ProfileId.Value).Contains(p.Id));
            //}
            //// Nếu đề nghị hủy gần nhất đã được phê duyệt thì không lấy những hồ sơ có trạng thái là chờ hủy hoặc hủy
            //else
            //{
            //    profileDestroy = profileDestroy.Where(p => p.Status.HasValue && (p.Status.Value == (int)Resource.ProfileStatus.WaitDestroy ||
            //                                          p.Status.Value == (int)Resource.ProfileStatus.Destroy)).AsEnumerable();
            //    result = result.Where(p => !profileDestroy.Select(i => i.ProfileId.Value).Contains(p.Id));
            //}
            if (!String.IsNullOrEmpty(filterName))
                result = result.Where(p => string.IsNullOrEmpty(filterName) || p.Name.Contains(filterName) || p.Code.Contains(filterName));
            result = result.Where(c => (c.Status.HasValue && status == 0 ? true : c.Status.Value == (int)status))
                           .Where(c => c.ArchiveDate.HasValue);
            count = result.Count();
            result = result.Where(c => (c.Status.HasValue && status == 0 ? true : c.Status.Value == (int)status))
                           .Where(c => c.ArchiveDate.HasValue)
                           .Where(c => c.EmployeeArchiveId.Value.CompareTo(objectSplitId) > 0 || c.DepartmentArchiveId.Value.CompareTo(objectSplitId) > 0);
            if (archiveType == Resource.ProfileArchiveTypes.HasTime)
                result = result.Where(c => c.TypeDate != (int)Resource.ProfileArchiveTime.Permanent);
            if (archiveType == Resource.ProfileArchiveTypes.NoLimit)
                result = result.Where(c => c.TypeDate == (int)Resource.ProfileArchiveTime.Permanent);
            if (archiveType == Resource.ProfileArchiveTypes.OverTime)
                result = result.Where(c => (c.TypeDate == (int)Resource.ProfileArchiveTime.Day && DateTime.Now >= c.ArchiveDate.Value.AddDays(c.ArchiveTime.Value))
                    || (c.TypeDate == (int)Resource.ProfileArchiveTime.Month && DateTime.Now >= c.ArchiveDate.Value.AddMonths(c.ArchiveTime.Value))
                    || (c.TypeDate == (int)Resource.ProfileArchiveTime.Year && DateTime.Now >= c.ArchiveDate.Value.AddYears(c.ArchiveTime.Value))
                    );

            count = result.Count();
            var data = Page(result.OrderByDescending(i => i.CreateAt), pageIndex, pageSize);

            var employeeIds = data.Where(i => i.EmployeeArchiveId.HasValue).Select(i => i.EmployeeArchiveId.Value);
            var employees = ServiceFactory<EmployeeService>().GetByIds(employeeIds);
            var departmentIds = data.Where(i => i.DepartmentArchiveId.HasValue).Select(i => i.DepartmentArchiveId.Value);
            var departments = ServiceFactory<DepartmentService>().GetByIds(departmentIds);
            data = data.Select(i =>
            {
                var tranferDetail = ServiceFactory<ProfileTransferDetailService>().GetQuery().Where(p => p.ProfileId == i.Id)
                                    .OrderByDescending(c => c.CreateAt)
                                    .Select(c => c.ProfileTransferId.Value).FirstOrDefault();
                if (tranferDetail != null)
                {
                    var tranfer = ServiceFactory<ProfileTransferService>().GetById(tranferDetail);
                    i.DayOfStorage = tranfer == null ? i.TimeOpen : tranfer.DateReceive;
                }
                if (i.DepartmentArchiveId.HasValue)
                    i.DepartmentArchiveName = departments.FirstOrDefault(u => i.DepartmentArchiveId == u.Id).Name;
                if (i.EmployeeArchiveId.HasValue)
                {
                    i.ArchiveEmployee = employees.FirstOrDefault(u => i.EmployeeArchiveId == u.Id);
                    i.ArchiveEmployee.RoleNames = string.Join(",", ServiceFactory<EmployeeService>().GetRoleNamesQuery(i.EmployeeArchiveId.Value));
                }
                // nhóm theo đề nghị hủy
                var profileDestroySuggestId = ServiceFactory<ProfileDestroyDetailService>().GetQuery().Where(c => c.ProfileId == i.Id).Select(c => c.ProfileDestroySuggestId).FirstOrDefault();
                if (profileDestroySuggestId.HasValue)
                {
                    var profileDestroySuggest = ServiceFactory<ProfileDestroySuggestService>().GetById(profileDestroySuggestId.Value);
                    if (profileDestroySuggest != null)
                    {
                        i.profileDestroySuggeststatus = profileDestroySuggest.IsApprove;
                        i.profileDestroySuggestId = profileDestroySuggest.Id;
                        i.GroupSummaryName = profileDestroySuggest.GroupSummaryName;
                    }
                }
                return i;
            });
            var rs = data.ToList();
            return data;
        }


        public Guid InsertProfileEmployee(ProfileBO data)
        {
            if (Get(i => !string.IsNullOrEmpty(i.Name) && i.Name.Trim().ToLower() == data.Name.Trim().ToLower() && i.Id != data.Id).Any())
                throw new ProfileNameHasBeenExistedException();
            data.Status = data.Status == null ? (int)Resource.ProfileStatus.New : data.Status;
            data.Id = base.Insert(data, false);
            // Cập nhật đính kèm
            ServiceFactory<ProfileAttachmentService>().Update(data.FileAttachment, data.Id, false);
            // Thêm vai trò người lập hồ sơ
            var responsibility = new ProfileResponsibilityBO()
            {
                ProfileId = data.Id,
                EmployeeId = UserId,
                RoleType = (int)Resource.ProfileResponsibilityRole.Creater,
                ResponsibiltityAt = DateTime.Now,
                SendAt = DateTime.Now,
                StatusType = (int)Resource.ProfileResponsibilityStatus.Sent
            };
            ServiceFactory<ProfileResponsibilityService>().Insert(responsibility, false);
            // Cập nhật ProfileId vào bảng VPProfileCuriculmViate
            if (data.ProfileCuriculmviateId.HasValue)
            {
                var profileCuriculmviate = ServiceFactory<VPHumanProfileCuriculmViateService>().GetById(data.ProfileCuriculmviateId.Value);
                if (profileCuriculmviate != null)
                {
                    profileCuriculmviate.ProfileId = data.Id;
                    ServiceFactory<VPHumanProfileCuriculmViateService>().Update(profileCuriculmviate, false);
                }
            }
            SaveTransaction(true);
            return data.Id;
        }

        /// <summary>
        /// Cập nhật hồ sơ nhân sự
        /// </summary>
        /// <param name="data"></param>
        public void UpdateProfileEmployee(ProfileBO data)
        {
            if (Get(i => !string.IsNullOrEmpty(i.Name) && i.Name.Trim().ToLower() == data.Name.Trim().ToLower() && i.Id != data.Id).Any())
                throw new ProfileNameHasBeenExistedException();
            base.Update(data, false);
            // Cập nhật đính kèm
            ServiceFactory<ProfileAttachmentService>().Update(data.FileAttachment, data.Id, false);
            SaveTransaction(true);
        }
    }
}
