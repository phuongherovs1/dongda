﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ProfileResponsibilityService : BaseService<ProfileResponsibilityDTO, ProfileResponsibilityBO>, IProfileResponsibilityService
    {
        /// <summary>
        /// Lấy danh sách thông tin nhân sự cập nhật hồ sơ theo id của hồ sơ
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="categoryId"></param>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public IEnumerable<ProfileResponsibilityBO> GetByProfile(int pageIndex, int pageSize, out int count, Guid profileId, Common.Resource.ProfileResponsibilityStatus status, string query = default(string))
        {
            var profiles = Get(i => i.ProfileId == profileId && (status == Resource.ProfileResponsibilityStatus.All ? true : i.StatusType == (int)status));
            var employees = ServiceFactory<EmployeeService>().Get(i => string.IsNullOrEmpty(query) || (!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(query.ToLower())));
            var result = profiles.Join(employees, p => p.EmployeeId, e => e.Id,
                                        (p, e) => new ProfileResponsibilityBO()
                                        {
                                            Id = p.Id,
                                            ResponsibiltityAt = p.ResponsibiltityAt,
                                            SendAt = p.SendAt,
                                            ApprovalAt = p.ApprovalAt,
                                            RoleType = p.RoleType,
                                            StatusType = p.StatusType,
                                            EmployeeId = p.EmployeeId,
                                            EmployeePerformBy = new EmployeeBO()
                                            {
                                                Id = e.Id,
                                                Avatar = e.Avatar,
                                                Name = e.Name,
                                                RoleNames = e.RoleNames
                                            }
                                        }
                                     );
            result = Page(result, pageIndex, pageSize);
            count = result.Count();
            var data = Enumerable.Empty<ProfileResponsibilityBO>();
            if (count > 0)
                data = result.Select(
                       i =>
                       {
                           i.EmployeePerformBy.RoleNames = string.Join(",", ServiceFactory<DepartmentTitleService>()
                               .GetByIds(ServiceFactory<EmployeeTitleService>().Get(c => c.EmployeeId == i.EmployeeId).Select(t => t.TitleId).ToList()).Select(u => u.Name).ToList());
                           return i;
                       }
                    );
            return data;
        }
        /// <summary>
        /// Lấy chi tiết thông tin trách nhiệm liên quan
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProfileResponsibilityBO GetDetail(Guid id)
        {
            var data = base.GetById(id);
            var responsibilities = data.ProfileId.HasValue ? base.GetQuery().Where(p => p.ProfileId == data.ProfileId && p.IsDelete != true).AsEnumerable() : null;
            if (responsibilities != null && responsibilities.Count() > 0 && !responsibilities.Select(p => p.EmployeeId).Contains(UserId))
                throw new AccessDenyException();
            data.ApprovalAt = DateTime.Now;
            var profile = data.ProfileId.HasValue ? ServiceFactory<ProfileService>().GetById(data.ProfileId.Value) : null;
            data.ProfileName = profile == null ? string.Empty : profile.Name;
            if (data.EmployeeId.HasValue)
                data.EmployeePerformBy = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.EmployeeId.Value);
            if (data.CreateBy.HasValue)
                data.EmployeeCreateBy = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(data.CreateBy.Value);
            return data;
        }
        /// <summary>
        /// Thêm mới thông tin trách nhiệm liên quan
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid InsertProfile(ProfileResponsibilityBO data)
        {
            data.StatusType = (int)Resource.ProfileResponsibilityStatus.WaitSend;
            data.EmployeeId = data.EmployeePerformBy.Id;
            data.ResponsibiltityAt = DateTime.Now;
            if (data.RoleType == (int)Resource.ProfileResponsibilityRole.Creater)
            {
                data.StatusType = (int)Resource.ProfileResponsibilityStatus.WaitSend;
            }
            if (data.RoleType == (int)Resource.ProfileResponsibilityRole.Checker)
            {
                if (data.ResultType == (int)Resource.ProfileResponsibilityResult.Accept) data.StatusType = (int)Resource.ProfileResponsibilityStatus.CheckAccept;
                if (data.ResultType == (int)Resource.ProfileResponsibilityResult.Fail) data.StatusType = (int)Resource.ProfileResponsibilityStatus.CheckFail;
            }
            if (data.RoleType == (int)Resource.ProfileResponsibilityRole.Approval)
            {
                if (data.ResultType == (int)Resource.ProfileResponsibilityResult.Accept) data.StatusType = (int)Resource.ProfileResponsibilityStatus.ApproveAccept;
                if (data.ResultType == (int)Resource.ProfileResponsibilityResult.Fail) data.StatusType = (int)Resource.ProfileResponsibilityStatus.ApproveFail;
            }
            return base.Insert(data);
        }
        /// <summary>
        /// Cập nhật thông tin trách nhiệm liên quan
        /// </summary>
        /// <param name="data"></param>
        public void UpdateProfile(ProfileResponsibilityBO data)
        {
            if (data.EmployeePerformBy != null)
                data.EmployeeId = data.EmployeePerformBy.Id;
            data.ResponsibiltityAt = DateTime.Now;
            if (data.RoleType == (int)Resource.ProfileResponsibilityRole.Creater)
            {
                data.StatusType = (int)Resource.ProfileResponsibilityStatus.WaitSend;
            }
            if (data.RoleType == (int)Resource.ProfileResponsibilityRole.Checker)
            {
                if (data.ResultType == (int)Resource.ProfileResponsibilityResult.Accept) data.StatusType = (int)Resource.ProfileResponsibilityStatus.CheckAccept;
                if (data.ResultType == (int)Resource.ProfileResponsibilityResult.Fail) data.StatusType = (int)Resource.ProfileResponsibilityStatus.CheckFail;
            }
            if (data.RoleType == (int)Resource.ProfileResponsibilityRole.Approval)
            {
                // Nếu phê duyệt đạt thì cập nhật hồ sơ về trạng thái đóng
                if (data.ResultType == (int)Resource.ProfileResponsibilityResult.Accept)
                {
                    data.StatusType = (int)Resource.ProfileResponsibilityStatus.ApproveAccept;
                    var profile = ServiceFactory<ProfileService>().GetById(data.ProfileId.Value);
                    profile.Status = (int)iDAS.Service.Common.Resource.ProfileStatus.Close;
                    ServiceFactory<ProfileService>().Update(profile, false);
                }
                if (data.ResultType == (int)Resource.ProfileResponsibilityResult.Fail) data.StatusType = (int)Resource.ProfileResponsibilityStatus.ApproveFail;
            }
            base.Update(data, false);
            SaveTransaction(true);
        }
        // Gửi thông tin trách nhiệm
        public void SendProfileResponsibilityById(Guid id)
        {
            var data = base.GetById(id);
            var profile = ServiceFactory<ProfileService>().GetById(data.ProfileId.Value);
            if (profile != null)
            {
                if (profile.CreateBy != UserId)
                    throw new AccessDenyException();
            }
            if (data.RoleType == (int)Resource.ProfileResponsibilityRole.Creater)
            {
                data.StatusType = (int)Resource.ProfileResponsibilityStatus.Sent;
            }
            if (data.RoleType == (int)Resource.ProfileResponsibilityRole.Checker)
            {
                data.StatusType = (int)Resource.ProfileResponsibilityStatus.WaitCheck;
            }
            if (data.RoleType == (int)Resource.ProfileResponsibilityRole.Approval)
            {
                data.StatusType = (int)Resource.ProfileResponsibilityStatus.WaitApprove;
            }
            data.SendAt = DateTime.Now;
            base.Update(data);
        }
        /// <summary>
        /// Thu hồi trách nhiệm. --> Trạng thái hồ sơ trở về chờ trách nhiệm
        /// Chỉ người tạo hồ sơ mới thực hiện được chức năng này
        /// </summary>
        /// <param name="id"></param>
        public void RevertResponsibility(Guid id)
        {
            var data = base.GetById(id);
            var profile = ServiceFactory<ProfileService>().GetById(data.ProfileId.Value);
            if (profile != null)
            {
                if (profile.CreateBy != UserId)
                    throw new AccessDenyException();
            }
            data.StatusType = (int)Resource.ProfileResponsibilityStatus.WaitSend;
            base.Update(data);
        }


        /// <summary>
        /// Danh sách trách nhiệm kiểm tra
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProfileResponsibilityBO> GetResponsibilityReview()
        {
            try
            {
                var data = base.GetQuery().Where(p => p.RoleType == (int)Resource.ProfileResponsibilityRole.Checker && p.EmployeeId == UserId && p.StatusType.HasValue &&
                                           (p.StatusType == (int)Resource.ProfileResponsibilityStatus.CheckFail || p.StatusType == (int)Resource.ProfileResponsibilityStatus.WaitCheck))
                                          .OrderByDescending(p => p.CreateAt);
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Danh sách trách nhiệm phê duyệt
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProfileResponsibilityBO> GetResponsibilityApproval()
        {
            try
            {
                var data = base.GetQuery().Where(p => p.RoleType == (int)Resource.ProfileResponsibilityRole.Approval && p.EmployeeId == UserId && p.StatusType.HasValue &&
                                           (p.StatusType == (int)Resource.ProfileResponsibilityStatus.ApproveFail || p.StatusType == (int)Resource.ProfileResponsibilityStatus.WaitApprove))
                                          .OrderByDescending(p => p.CreateAt);
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// XÓa trách nhiệm
        /// </summary>
        /// <param name="id"></param>
        public void DeleteResponsibility(Guid id)
        {
            var data = base.GetById(id);
            var profile = ServiceFactory<ProfileService>().GetById(data.ProfileId.Value);
            if (profile != null)
            {
                if (profile.CreateBy != UserId)
                    throw new AccessDenyException();
            }
            base.Delete(id);
        }

        /// <summary>
        /// Khởi tạo giá trị mặc định khi thêm mới trách nhiệm
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public ProfileResponsibilityBO CreateDefault(string profileId)
        {
            try
            {
                var data = new ProfileResponsibilityBO();
                if (!String.IsNullOrEmpty(profileId))
                {
                    var profile = ServiceFactory<ProfileService>().GetProfileInfo(new Guid(profileId));
                    if (profile.CreateBy != UserId)
                        throw new AccessDenyException();
                    data.ProfileId = profile.Id;
                    data.ProfileName = profile.Name;
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
