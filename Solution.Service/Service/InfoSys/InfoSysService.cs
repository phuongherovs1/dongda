﻿using iDAS.ADO;
using System.Data;

namespace iDAS.Service
{
    public class InfoSysService : IInfoSysService
    {

        public bool Insert(InfoSysBO infoSysBO)
        {
            return Sql_ADO.idasAdoService.ExecuteNoquery("sp_InfoSystems_Insert", parameter: new { infoSysBO.InfoSystemCode, infoSysBO.Name, infoSysBO.Purpose, infoSysBO.InternalAccess, infoSysBO.RemoteAccess, infoSysBO.PartnerAccess, infoSysBO.IsActive }) > 0;
        }

        public bool Update(InfoSysBO infoSysBO)
        {
            return Sql_ADO.idasAdoService.ExecuteNoquery("sp_InfoSystems_Update", parameter: new { infoSysBO.Id, infoSysBO.InfoSystemCode, infoSysBO.Name, infoSysBO.Purpose, infoSysBO.InternalAccess, infoSysBO.RemoteAccess, infoSysBO.PartnerAccess, infoSysBO.IsActive }) > 0;
        }

        public bool Disable(int infoSysID)
        {
            return true;
        }

        public DataTable GetAllInfoSystems()
        {
            return Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_InfoSystems_GetAll");
        }

        public DataTable GetInfoSystems(int pageIndex, int pageSize, out int count)
        {
            count = 0;
            return new DataTable();
        }


        public InfoSysBO GetInfoSysDetail(int infoSysID)
        {
            return Sql_ADO.idasAdoService.ToList<InfoSysBO>(Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_InfoSystems_GetDetail", parameter: new { Id = infoSysID }))[0];
        }
    }
}
