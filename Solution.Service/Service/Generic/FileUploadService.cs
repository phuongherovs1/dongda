﻿using iDAS.Service.FileServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iDAS.Service
{
    public class FileUploadService : IFileUploadService
    {
        private FileServiceReference.IFileService serviceFile = new FileServiceReference.FileServiceClient();
        private const string SystemCode = "iDAS";
        private const string UserCode = "File";
        private FileService _fileService;
        public FileService fileSV
        {
            get
            {
                return _fileService = _fileService ?? new FileService();
            }
        }
        //Upload file dùng cho single file
        public Guid Upload(FileBO file)
        {
            if (file != null)
            {
                var fileInfo = new FileServiceReference.UploadFileInfo();
                fileInfo.SystemCode = SystemCode;
                fileInfo.Code = UserCode;
                fileInfo.Name = file.Name;
                fileInfo.Size = file.Size.Value;
                fileInfo.Type = file.Type;
                fileInfo.Stream = file.File.InputStream;
                var fileResult = serviceFile.UploadFile(fileInfo);
                //file.UploadSuccess = fileResult.Error == FileServiceReference.FileError.Success;
            }
            if (true)
            {
                file.Id = fileSV.Insert(file);
            }
            return file.Id;
        }
        //Upload file dùng cho multi file
        public IEnumerable<Guid> Upload(FileUploadBO data)
        {
            //Xóa những file đã xóa
            fileSV.DeleteRange(data.FileRemoves);
            //Up load file mới
            var source = new List<FileBO>();
            foreach (var file in data.FileAttachments)
            {
                if (file != null)
                {
                    var fileInfo = new FileServiceReference.UploadFileInfo();
                    fileInfo.SystemCode = SystemCode;
                    fileInfo.Code = UserCode;
                    fileInfo.Name = file.FileName;
                    fileInfo.Size = file.ContentLength;
                    fileInfo.Type = file.ContentType;
                    fileInfo.Stream = file.InputStream;
                    var fileResult = serviceFile.UploadFile(fileInfo);
                    var item = new FileBO() { File = file };
                    ///item.UploadSuccess = fileResult.Error == FileServiceReference.FileError.Success;
                    item.Id = new Guid(fileResult.Id);
                    source.Add(item);
                }
            };
            foreach (var item in source)
            {
                if (true)
                {
                    item.Id = fileSV.Insert(item);
                }
            };
            return source.Select(i => i.Id);
        }
        public ResponseFileInfo View(string fileId)
        {
            RequestFileInfo fileInfo = new RequestFileInfo();
            ResponseFileInfo fileResult = new ResponseFileInfo();
            fileInfo.SystemCode = SystemCode;
            fileInfo.Code = UserCode;
            fileInfo.Id = fileId;
            fileResult = serviceFile.ViewFile(fileInfo);
            return fileResult;
        }
        public ResponseFileInfo Delete(string fileId)
        {
            RequestFileInfo fileInfo = new RequestFileInfo();
            ResponseFileInfo fileResult = new ResponseFileInfo();
            fileInfo.SystemCode = SystemCode;
            fileInfo.Code = UserCode;
            fileInfo.Id = fileId;
            fileResult = serviceFile.DeleteFile(fileInfo);
            new FileService().Delete(new Guid(fileId));
            return fileResult;
        }
        public List<ResponseFileInfo> Delete(List<Guid> fileIds)
        {
            RequestFileInfo fileInfo = new RequestFileInfo();
            ResponseFileInfo fileResult = new ResponseFileInfo();
            List<ResponseFileInfo> result = new List<ResponseFileInfo>();
            foreach (var fileId in fileIds)
            {
                fileInfo.SystemCode = SystemCode;
                fileInfo.Code = UserCode;
                fileInfo.Id = fileId.ToString();
                fileResult = serviceFile.DeleteFile(fileInfo);
                if (fileResult.Error == FileError.NotFound)
                    fileResult.Id = fileId.ToString();
                result.Add(fileResult);
            }
            return result;
        }
        public ResponseFileInfo Download(string fileId)
        {
            RequestFileInfo fileInfo = new RequestFileInfo();
            ResponseFileInfo fileResult = new ResponseFileInfo();
            fileInfo.SystemCode = SystemCode;
            fileInfo.Code = UserCode;
            fileInfo.Id = fileId;
            fileResult = serviceFile.DownloadFile(fileInfo);
            return fileResult;
        }
        public ResponseFileInfo Update(HttpPostedFileBase file, string fileId)
        {
            UploadFileInfo fileInfo = new UploadFileInfo();
            ResponseFileInfo fileResult = new ResponseFileInfo();
            fileInfo.SystemCode = SystemCode;
            fileInfo.Code = UserCode;
            fileInfo.Name = file.FileName;
            fileInfo.Size = file.ContentLength;
            fileInfo.Type = file.ContentType;
            fileInfo.Stream = file.InputStream;
            fileInfo.Id = fileId;
            fileInfo.IsReplace = true;
            fileResult = serviceFile.UploadFile(fileInfo);
            if (fileResult.Error == FileError.Success)
            {
                // var item = fileInfoDA.GetById(new Guid(fileId));
                //item.Name = fileResult.Name;
                //fileInfoDA.Update(item);
                //fileInfoDA.Save();
            }
            return fileResult;
        }


        //public ResponseFileInfo ViewExport(string fileId)
        //{
        //    RequestFileInfo fileInfo = new RequestFileInfo();
        //    ResponseFileInfo fileResult = new ResponseFileInfo();
        //    fileInfo.SystemCode = SystemCode;
        //    fileInfo.Code = UserCode;
        //    fileInfo.Id = fileId;
        //    fileResult = serviceFile.ViewExportFile(fileInfo);
        //    return fileResult;
        //}
        //public ResponseFileInfo DownloadExport(string fileId)
        //{
        //    RequestFileInfo fileInfo = new RequestFileInfo();
        //    ResponseFileInfo fileResult = new ResponseFileInfo();
        //    fileInfo.SystemCode = SystemCode;
        //    fileInfo.Code = UserCode;
        //    fileInfo.Id = fileId;
        //    fileResult = serviceFile.DownloadExportFile(fileInfo);
        //    return fileResult;
        //}
        public string GetMessageError(List<ResponseFileInfo> files)
        {
            var message = string.Empty;
            foreach (var file in files)
            {
                if (file.Error != FileError.Success)
                {
                    message += file.Name + ": " + GetMessageError(file.Error) + "<br />";
                }
            }
            return message;
        }
        public string GetMessageError(FileError error)
        {
            var message = string.Empty;
            switch (error)
            {
                case FileError.NotFound: message = "Tệp tin không tồn tại trên hệ thống"; break;
                case FileError.Exist: message = "Tệp tin đã tồn tại trên hệ thống"; break;
                case FileError.OverSize: message = "Tệp tin đã vượt qua kích cỡ cho phép (2Gb)"; break;
                case FileError.FileNullOrEmpty: message = "Tệp tin không có dung lượng (0 Byte)"; break;
                case FileError.CodeNullOrEmpty: message = "Mã khách hàng hoặc mã hệ thống không tồn tại"; break;
                case FileError.NotView: message = "Không thể xem được tệp đã chọn"; break;
                case FileError.Fail: message = "Thực hiện xử lý tệp phát sinh lỗi"; break;
                case FileError.Success: message = "Thực hiện xử lý tệp thành công"; break;
            }
            return message;
        }
    }
}
