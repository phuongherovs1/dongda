﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class KPIForTaskService : BaseService<KPIForTaskDTO, KPIForTaskBO>, IKPIForTaskService
    {
        public Guid Insert(KPIForTaskBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            data.Id = base.Insert(data, true);
            return data.Id;
        }
        public IEnumerable<TaskPerformBO> GetDataToSendPointAdmin(bool Isparent, string filter = default(string), string TaskIdSearch = default(string))
        {
            //Load taskId theo user trên table TaskResources có IsRoleMark = true
            //Load TaskResourcesId (là id của taskperform) có taskId thuộc list taskId trên và IsRolePerform = true
            //Load taskperform theo list TaskResourcesI ở trên
            var lstTask = ServiceFactory<TaskResourceService>().Get(i => i.EmployeeId.Value == UserId && i.IsRolePerform == true);
            if (Isparent)
                lstTask = ServiceFactory<TaskResourceService>().Get(i => i.EmployeeId.Value == UserId && (i.IsRoleMark == true || i.IsRoleAssign == true));
            var lstTaskResources = ServiceFactory<TaskResourceService>().Get(i => lstTask.Select(x => x.TaskId.Value).ToList().Contains(i.TaskId.Value) && i.IsRolePerform == true);
            var tasks = ServiceFactory<TaskService>().GetByIds(lstTaskResources.Select(i => i.TaskId.Value).ToList().Distinct());
            var employees = ServiceFactory<EmployeeService>().GetByIds(lstTaskResources.Select(i => i.EmployeeId.Value).ToList());
            var KPIForTasks = GetAll();
            var lstTaskPerform = ServiceFactory<TaskPerformService>().GetByIds(lstTaskResources.Select(x => x.Id).ToList()).ToList();
            foreach (TaskPerformBO item in lstTaskPerform)
            {
                item.Employee = employees.FirstOrDefault(i => lstTaskResources.Any(z => z.Id == item.Id && i.Id == z.EmployeeId));
                if (lstTaskResources.Any(z => z.Id == item.Id))
                {
                    var taskitem = tasks.FirstOrDefault(i => i.Id == lstTaskResources.First(z => z.Id == item.Id).TaskId);
                    if (taskitem != null)
                    {
                        item.TaskId = taskitem.Id;
                        item.TaskStatus = taskitem.Status;
                        item.TaskName = taskitem.Name;
                        item.CompleteRateTask = taskitem.CompleteRate;
                        //Load thông thin điểm đã đc cho trước đó
                        var KPIForTaskitem = KPIForTasks.FirstOrDefault(i => i.TaskId.Value == taskitem.Id && i.EmployeeId.Value == item.EmployeeId);
                        if (KPIForTaskitem != null)
                        {
                            item.Score = KPIForTaskitem.Score;
                            item.Score_Value = KPIForTaskitem.Score_Value;
                            item.Score_Time = KPIForTaskitem.Score_Time;
                            item.Score_Plus = KPIForTaskitem.Score_Plus;
                            item.Score_Minus = KPIForTaskitem.Score_Minus;
                            item.Attitude1 = KPIForTaskitem.Attitude1;
                            item.Attitude2 = KPIForTaskitem.Attitude2;
                            item.Attitude3 = KPIForTaskitem.Attitude3;
                            item.Attitude4 = KPIForTaskitem.Attitude4;
                            item.AttitudePlus = KPIForTaskitem.AttitudePlus;
                            item.AttitudeMinus = KPIForTaskitem.AttitudeMinus;

                            item.Manager = KPIForTaskitem.Manager;
                            item.ManagerMinus = KPIForTaskitem.ManagerMinus;
                            item.ManagerPlus = KPIForTaskitem.ManagerPlus;

                            item.Note = string.IsNullOrEmpty(KPIForTaskitem.Note) ? "" : KPIForTaskitem.Note;
                        }
                        else
                            item.Note = "";
                    }
                }
            }
            if (!string.IsNullOrEmpty(filter))
                lstTaskPerform = lstTaskPerform.Where(x => x.EmployeeName.ToUpper().Contains(filter.ToUpper())).ToList();
            if (!string.IsNullOrEmpty(TaskIdSearch))
                lstTaskPerform = lstTaskPerform.Where(x => x.TaskId.HasValue && x.TaskId.Value.ToString() == TaskIdSearch).ToList();
            return lstTaskPerform;
        }
        public IEnumerable<KPIForTaskBO> GetDataScoreOfEmployee(DateTime DateFrom, DateTime DateTo, Guid? DepartmentId, int pageIndex, int pageSize, out int count, string filterName = default(string))
        {
            var listKPIForTask = Get(x => x.Id != Guid.Empty && x.CreateAt.Value.Date.Subtract(DateFrom.Date).Days >= 0 && x.CreateAt.Value.Date.Subtract(DateTo.Date).Days <= 0);
            count = listKPIForTask.ToList().Count;
            List<KPIForTaskBO> result = new List<KPIForTaskBO>();
            foreach (KPIForTaskBO item in listKPIForTask)
            {
                var employee = ServiceFactory<EmployeeService>().GetById(item.EmployeeId.Value);
                if (employee != null)
                {
                    item.EmployeeName = employee.Name;
                    item.EmployeeImageUrl = employee.AvatarUrl;
                    var employeeTitle = ServiceFactory<EmployeeTitleService>().Get(x => x.EmployeeId == employee.Id).Select(x => x.TitleId).ToList();
                    var departmentTitles = ServiceFactory<DepartmentTitleService>().GetByIds(employeeTitle).Select(x => x.DepartmentId.Value).ToList();
                    var department = ServiceFactory<DepartmentService>().GetByIds(departmentTitles).ToList();
                    if (department.Count > 0)
                    {
                        item.DepartmentNames = string.Join(", ", department.Select(x => x.Name));
                        item.DepartmentIds = "," + string.Join(", ", department.Select(x => x.Name)) + ",";
                    }
                }
                if (result.Count == 0)
                {
                    item.SumScore = item.Score * item.Weightscore;
                    result.Add(item);
                }
                else
                {
                    int index = result.FindIndex(i => i.EmployeeId.Value.ToString() == item.EmployeeId.Value.ToString());
                    if (index > -1)
                    {
                        result[index].SumScore += item.Score * item.Weightscore;
                    }
                    else
                    {
                        item.SumScore = item.Score * item.Weightscore;
                        result.Add(item);
                    }
                }
            }
            if (!string.IsNullOrEmpty(filterName))
                result = result.Where(x => x.EmployeeName.ToUpper().Contains(filterName.ToUpper())).ToList();
            if (DepartmentId.HasValue && DepartmentId != Guid.Empty)
                result = result.Where(x => x.DepartmentIds.Contains("," + DepartmentId.Value + ",")).ToList();
            var data = base.Page(result, pageIndex, pageSize);
            return data;
        }
        public IEnumerable<KPIForTaskBO> GetDataScoreOfEmployeeMark(DateTime DateFrom, DateTime DateTo, Guid? EmployeeId)
        {
            var listKPIForTask = Get(x => x.Id != Guid.Empty && x.IsDelete == false && x.EmployeeId.Value.ToString() == EmployeeId.Value.ToString() && x.CreateAt.Value.Date.Subtract(DateFrom.Date).Days >= 0 && x.CreateAt.Value.Date.Subtract(DateTo.Date).Days <= 0);
            List<KPIForTaskBO> result = new List<KPIForTaskBO>();
            foreach (KPIForTaskBO item in listKPIForTask)
            {
                var employee = ServiceFactory<EmployeeService>().GetById(item.CreateBy.Value);
                if (employee != null)
                {
                    item.EmployeeNameMark = employee.Name;
                    item.EmployeeImageUrlMark = employee.AvatarUrl;
                    var lstTaskPerformId = ServiceFactory<TaskResourceService>().Get(x => x.TaskId == item.TaskId && x.IsRolePerform == true && x.EmployeeId == item.EmployeeId).Select(x => x.Id).ToList();
                    if (lstTaskPerformId.Count > 0)
                    {
                        var Task = ServiceFactory<TaskService>().GetById(item.TaskId.Value);
                        item.TaskName = Task.Name;

                        int Weightscore = 0;
                        if (item.Manager.HasValue)
                            Weightscore += item.Manager.Value;
                        if (item.ManagerPlus.HasValue)
                            Weightscore += item.ManagerPlus.Value;
                        if (item.ManagerMinus.HasValue)
                            Weightscore -= item.ManagerMinus.Value;
                        item.Weightscore = Weightscore;

                        int SumScore = 0;
                        if (item.Score_Value.HasValue)
                            SumScore += item.Score_Value.Value;
                        if (item.Score_Time.HasValue)
                            SumScore += item.Score_Time.Value;
                        if (item.Score_Plus.HasValue)
                            SumScore += item.Score_Plus.Value;
                        if (item.Score_Minus.HasValue)
                            SumScore -= item.Score_Minus.Value;
                        item.SumScore = SumScore;

                        item.MaxScore = Weightscore + item.Score_Attitude.Value + SumScore;

                        var TaskPerform = ServiceFactory<TaskPerformService>().GetById(lstTaskPerformId[0]);
                        item.StatusProcessTask = TaskPerform.UnitText;
                    }

                }
            }
            return listKPIForTask;
        }
    }
}