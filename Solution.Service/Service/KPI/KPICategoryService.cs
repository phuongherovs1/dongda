﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class KPICategoryService : BaseService<KPICategoryDTO, KPICategoryBO>, IKPICategoryService
    {
        #region Override
        public Guid Insert(KPICategoryBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Name.Trim().ToUpper() == data.Name.Trim().ToUpper() && i.Id != data.Id && i.IsDelete == false).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            if (data.ParrentId.HasValue)
            {
                var item = GetById(data.ParrentId.Value);
                data.Level = item.Level + 1;
            }
            else
            {
                data.Level = 0;
            }
            data.Id = Insert(data, true);
            return data.Id;
        }

        public void Update(KPICategoryBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true)
        {
            var existCode = Get(i => i.Name.Trim().ToUpper() == data.Name.Trim().ToUpper() && i.Id != data.Id && i.IsDelete == false).Any();
            if (existCode)
            {
                var ex = new DataHasBeenExistedException();
                throw ex;
            }
            Update(data, true);
        }

        public override IEnumerable<KPICategoryBO> GetAll(bool allowDeleted = false)
        {
            var data = base.GetAll(allowDeleted);
            return data;
        }

        public IEnumerable<KPICategoryBO> GetAll(int pageIndex, int pageSize, out int count, bool allowDeleted = false)
        {
            count = GetAll(allowDeleted).Count();
            var data = Page(GetAll(allowDeleted).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            return data;
        }
        public IEnumerable<KPICategoryBO> GetByfilterName(int pageIndex, int pageSize, int level, Guid? id, string filterName, out int count, bool allowDeleted = false)
        {
            count = Get(x => x.IsDelete == allowDeleted && x.Level == level).ToList().Count;
            var data = Page(Get(x => x.IsDelete == allowDeleted && x.Level == level).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
            if (id.HasValue)
                data = data.Where(x => x.ParrentId.Value.ToString() == id.Value.ToString()).ToList();
            if (!string.IsNullOrEmpty(filterName))
            {
                count = data.Where(x => x.Name.Contains(filterName)).ToList().Count;
                data = Page(data.Where(x => x.Name.Contains(filterName)).OrderByDescending(i => i.CreateAt), pageIndex, pageSize).ToList();
                data = GetParentByChild(data).ToList();
            }
            return data;
        }
        private IEnumerable<KPICategoryBO> GetParentByChild(IEnumerable<KPICategoryBO> data)
        {
            foreach (KPICategoryBO item in data)
            {
                if (item.ParrentId.HasValue)
                {
                    var lstParent = Get(x => x.Id.ToString() == item.ParrentId.Value.ToString());
                    GetParentByChild(lstParent);
                    data.ToList().AddRange(lstParent);
                }
            }
            return data.Distinct();
        }
        public IEnumerable<KPICategoryBO> GetTreeDepartment(Guid? id, string filterName = default(string))
        {
            try
            {
                if (!string.IsNullOrEmpty(filterName))
                {
                    var data = GetQuery().Where(i => i.ParrentId == id && (i.Name == filterName || (i.Maxscore.HasValue && i.Maxscore.Value.ToString() == filterName))).AsEnumerable().AsEnumerable()
                    .Select(i =>
                    {
                        i.IsParent = Get().Any(t => t.ParrentId == i.Id);
                        return i;
                    }).OrderBy(x => x.Name);
                    return data;
                }
                else
                {
                    var data = GetQuery().Where(i => i.ParrentId == id).AsEnumerable().AsEnumerable()
                   .Select(i =>
                       {
                           i.IsParent = Get().Any(t => t.ParrentId == i.Id);
                           return i;
                       }).OrderBy(x => x.Name);
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Overload
        public override KPICategoryBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = base.GetById(id, allowDeleted);
            return data;
        }
        public override KPICategoryBO GetJustById(Guid id, bool allowDefaultIfNull = true)
        {
            var data = base.GetJustById(id);
            return data;
        }
        #endregion
        #region Rule
        public KPICategoryBO GetFormItem()
        {
            try
            {
                var data = new KPICategoryBO() { };
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}