﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public abstract partial class BaseService<TDTO, TPOCO> : IService<TPOCO>
        where TDTO : IDataTransfer
        where TPOCO : BaseBO
    {
        private IUnitOfWork<TPOCO> unitOfWork
        {
            get
            {
                return _unitOfWork.Value;
            }
        }
        private Dictionary<string, IService> cache
        {
            get
            {
                return _cache.Value;
            }
        }
        private Lazy<Dictionary<string, IService>> _cache;
        private Lazy<UnitOfWork<TDTO, TPOCO>> _unitOfWork;
        public BaseService()
        {
            _unitOfWork = new Lazy<UnitOfWork<TDTO, TPOCO>>();
            _cache = new Lazy<Dictionary<string, IService>>();
        }
        public virtual IEnumerable<TPOCO> GetAll(bool allowDeleted = false)
        {
            return unitOfWork.Repository.GetAll(allowDeleted);
        }
        public virtual IEnumerable<TPOCO> Get(Expression<Func<TPOCO, bool>> filter = null,
            Func<IQueryable<TPOCO>, IQueryable<TPOCO>> include = null,
            Func<IQueryable<TPOCO>, IOrderedQueryable<TPOCO>> orderBy = null,
            Func<IQueryable<TPOCO>, IOrderedQueryable<TPOCO>> orderByDescending = null, bool allowDeleted = false)
        {
            return unitOfWork.Repository.Get(filter, include, orderBy, orderByDescending, allowDeleted);
        }
        public virtual IQueryable<TPOCO> GetQuery(bool allowDeleted = false)
        {
            return unitOfWork.Repository.Get(allowDeleted);
        }
        public virtual IEnumerable<TPOCO> GetDataByStringQuery(string query)
        {
            return unitOfWork.Repository.GetDataByStringQuery(query);
        }
        public virtual TPOCO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            var data = unitOfWork.Repository.GetById(id, allowDeleted);
            if (allowDefaultIfNull && data == null)
                data = Activator.CreateInstance<TPOCO>();
            if (data != null)
                data.UserId = UserId;
            return data;
        }
        public virtual TPOCO GetJustById(Guid id, bool allowDefaultIfNull = true)
        {
            var data = unitOfWork.Repository.GetJustById(id);
            if (allowDefaultIfNull && data == null)
                data = Activator.CreateInstance<TPOCO>();
            if (data != null)
                data.UserId = UserId;
            return data;
        }
        public virtual IQueryable<TPOCO> GetQueryById(Guid id, bool allowDeleted = false)
        {
            return unitOfWork.Repository.Get(allowDeleted).Where(i => i.Id == id);
        }
        public virtual IEnumerable<TPOCO> GetByIds(IEnumerable<Guid> ids, bool allowDeleted = false)
        {
            return unitOfWork.Repository.GetByIds(ids, allowDeleted);
        }
        public virtual IQueryable<TPOCO> GetByIds(IQueryable<Guid> ids, bool allowDeleted = false)
        {
            return unitOfWork.Repository.GetByIds(ids, allowDeleted);
        }
        public virtual IEnumerable<TPOCO> GetNotInIds(IEnumerable<Guid> ids, bool allowDeleted = false)
        {
            return unitOfWork.Repository.GetNotInIds(ids, allowDeleted);
        }
        public virtual IQueryable<TPOCO> GetNotInIds(IQueryable<Guid> ids, bool allowDeleted = false)
        {
            return unitOfWork.Repository.GetNotInIds(ids, allowDeleted);
        }
        public virtual Guid Insert(TPOCO data, bool allowSave = true)
        {
            var id = unitOfWork.Repository.Insert(data);
            SaveTransaction(allowSave);
            unitOfWork.Repository.SaveChangesAsync();
            return id;
        }
        public virtual IEnumerable<Guid> InsertRange(IEnumerable<TPOCO> data, bool allowSave = true)
        {
            var ids = unitOfWork.Repository.InsertRange(data);
            SaveTransaction(allowSave);
            return ids;
        }
        public virtual void Update(TPOCO data, bool allowSave = true)
        {
            unitOfWork.Repository.Update(data);
            SaveTransaction(allowSave);
        }
        public virtual void Delete(Guid id, bool allowSave = true)
        {
            unitOfWork.Repository.Delete(id);
            SaveTransaction(allowSave);
        }
        public virtual void DeleteRange(IEnumerable<Guid> ids, bool allowSave = true)
        {
            unitOfWork.Repository.DeleteRange(ids);
            SaveTransaction(allowSave);
        }
        public virtual void Remove(Guid id, bool allowSave = true)
        {
            unitOfWork.Repository.Remove(id);
            SaveTransaction(allowSave);
        }
        public virtual void RemoveRange(IEnumerable<Guid> ids, bool allowSave = true)
        {
            unitOfWork.Repository.RemoveRange(ids);
            SaveTransaction(allowSave);
        }
        public virtual void Revert(Guid id, bool allowSave = true)
        {
            unitOfWork.Repository.Revert(id);
            SaveTransaction(allowSave);
        }
        public virtual void RevertRange(IEnumerable<Guid> ids, bool allowSave = true)
        {
            unitOfWork.Repository.RevertRange(ids);
            SaveTransaction(allowSave);
        }
        public virtual void SaveTransaction(bool allowSave = true)
        {
            if (allowSave) unitOfWork.Repository.SaveChanges();
        }
        public virtual Task<int> SaveTransactionAsync(bool allowSave = true)
        {
            if (allowSave)
                return unitOfWork.Repository.SaveChangesAsync();
            else
                return Task.FromResult(0);
        }
        public IEnumerable<TPOCO> Page(out int count, int pageIndex, int pageSize, bool allowDeleted = false)
        {

            var query = GetQuery(allowDeleted);
            count = query.Count();
            var data = Page(query.OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            return data;
        }

        public IEnumerable<TPOCO> Page(IEnumerable<TPOCO> data, int pageIndex, int pageSize)
        {
            if (pageSize <= 0)
                return data;
            else
                return unitOfWork.Repository.Page(data, pageIndex, pageSize);
        }
        public IQueryable<TPOCO> Page(IQueryable<TPOCO> data, int pageIndex, int pageSize)
        {
            if (pageSize <= 0)
                return data;
            else
                return unitOfWork.Repository.Page(data, pageIndex, pageSize);
        }
        public Guid UserId
        {
            get
            {
                return unitOfWork.Repository.GetUserId();
            }
        }
        public EmployeeBO GetUserCurrent()
        {
            return ServiceFactory<EmployeeService>().GetById(UserId);
        }
        public EmployeeBO GetEmployee(Guid id)
        {
            return ServiceFactory<EmployeeService>().GetById(id);
        }
        public IQueryable<EmployeeBO> GetEmployees(IQueryable<Guid> ids)
        {
            return ServiceFactory<EmployeeService>().GetByIds(ids);
        }
        public void SetDbContext(IUnitOfWork unitOfWork)
        {
            this.unitOfWork.SetDbContext(unitOfWork);
        }
        protected T ServiceFactory<T>() where T : IService
        {
            var key = typeof(T).ToString();
            if (!cache.ContainsKey(key))
            {
                T service = Activator.CreateInstance<T>();
                service.SetDbContext(unitOfWork);
                cache[key] = service;
            }
            return (T)cache[key];
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public static DataTable GetTableByQuery(string query, string connection)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connection);
                conn.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = conn;
                sqlCommand.CommandText = query;
                SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
                DataTable dataTable = new DataTable();
                da.Fill(dataTable);
                conn.Close();
                conn.Dispose();
                return dataTable;
            }
            catch (Exception e) { return new DataTable(); }
        }
    }
}