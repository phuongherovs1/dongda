﻿using iDAS.ADO;
using iDAS.Service.API.Timekeeper;
using iDAS.Service.POCO.Timekeeper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using zkemkeeper;

namespace iDAS.Service.Service.Timekeeper
{
    public class ManagerTimkeeperService : IManagerTimkeeperService
    {
        public bool CreateTimkeeper(TimekeeperBO timkeeperBO)
        {
            return Sql_ADO.idasAdoService.ExecuteNoquery("sp_Timekeeper_create", parameter: timkeeperBO) > 0;
        }
        public bool DeleteTimkeeper(int _IDTimekeeper)
        {
            return Sql_ADO.idasAdoService.ExecuteNoquery("sp_Timekeeper_delete", parameter: new { IDTimekeeper = _IDTimekeeper }) > 0;
        }
        public DataTable SelectTimkeeper()
        {
            return Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Timekeeper_select");
        }
        public bool UpdateTimkeeper(TimekeeperBO timkeeperBO)
        {
            return Sql_ADO.idasAdoService.ExecuteNoquery("sp_Timekeeper_update", parameter: timkeeperBO) > 0;
        }

        public TimekeeperBO SelectTimkeeper(int idTimkeeper)
        {
            return Sql_ADO.idasAdoService.ToList<TimekeeperBO>(Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Timekeeper_select", parameter: new { IDTimekeeper = idTimkeeper }))[0];
        }
        public bool InputTimekeeping(int idTimkeeper, string IP, int Post, out string error)
        {
            try
            {
                var TimekeeperhistoryOBs = Getlogdata(idTimkeeper, IP, Post);
                DataTable data = new DataTable();
                data.Columns.Add("IDTimeKeeper", typeof(int));
                data.Columns.Add("Code", typeof(int));
                data.Columns.Add("dateTimekeeping", typeof(DateTime));
                foreach (var item in TimekeeperhistoryOBs)
                {
                    data.Rows.Add(item.IDTimeKeeper, item.Code, item.dateTimekeeping);
                }
                if (data.Rows.Count > 0)
                {
                    // kiểm tra dữ liệu và thêm dữ liệu vào bảng Timekeepinghistory
                    SummaryTimekeepinghistory(data);
                }
                //thêm dữ liệu vào bảng timekeeping
                SummaryTimekeeping(DateTime.Now);
                error = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return false;
            }

        }
        //thêm dữ liệu vào trong bảng timekeeping
        protected static void SummaryTimekeeping(DateTime date)
        {
            Sql_ADO.idasAdoService.ExecuteNoquery("sp_Timekeeping_Input", parameter: new { date = date });
        }
        // kiểm tra và thêm dữ liệu vào Timekeepinghistory
        protected static void SummaryTimekeepinghistory(DataTable data)
        {
            Sql_ADO.idasAdoService.ExecuteNoquery("sp_Timekeepinghistory_create_check", parameter: new { TimekeepinghistoryTable = data });
        }
        // Lấy dữ liệu từ máy chấm công 
        protected List<TimekeeperhistoryOB> Getlogdata(int IDTimekeeper, string IP, int Port)
        {
            try
            {
                List<TimekeeperhistoryOB> timekeeperhistoryOBs = new List<TimekeeperhistoryOB>();
                string dwEnrollNumber1 = "";
                int dwVerifyMode;
                int dwInOutMode;
                int dwYear;
                int dwMonth;
                int dwDay;
                int dwHour;
                int dwMinute;
                int dwSecond;
                int dwWorkCode = 0;
                var MCC = new CZKEM();
                if (MCC.Connect_Net(IP, Port))
                {
                    MCC.ReadAllGLogData(IDTimekeeper);
                    while (MCC.SSR_GetGeneralLogData(IDTimekeeper, out dwEnrollNumber1, out dwVerifyMode, out dwInOutMode, out dwYear, out dwMonth, out dwDay, out dwHour, out dwMinute, out dwSecond, ref dwWorkCode))
                    {
                        DateTime date = new DateTime(dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond);
                        TimekeeperhistoryOB timekeeperhistoryOB = new TimekeeperhistoryOB();
                        timekeeperhistoryOB.Code = dwEnrollNumber1;
                        timekeeperhistoryOB.IDTimeKeeper = IDTimekeeper;
                        timekeeperhistoryOB.dateTimekeeping = date;
                        timekeeperhistoryOBs.Add(timekeeperhistoryOB);
                    }
                }
                return timekeeperhistoryOBs;
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Lỗi đọc logdata: {0}", e.Message));
            }
        }

        public string GetdataExpost(DateTime fromdate, DateTime todate)
        {
            StringBuilder stringBuilder = new StringBuilder(@"");
            int day = (todate.Subtract(fromdate).Days + 1);
            int totalCol = day + 6;
            stringBuilder.Append(string.Format(@"<table id='viewdatagriview' style='width:100%' cellpadding='0' cellspacing='0'>
                                                           <tr>
                                                            <th colspan ='{2}'>
                                                            <table style='width:100%' cellpadding='0' cellspacing='0'>
                                                            <tr>
                                                            <td colspan ='{3}'><b>Đơn vị:</b>Trung tâm ứng cứu khẩn cấp máy tính Việt Nam</td> <td colspan ='{4}' style='text-align: center;'>Mẫu số CO1a-HĐ</td>
                                                            </tr>
                                                            <tr>
                                                            <td colspan ='{3}'><b>Bộ phận:</b></td> <td colspan ='{4}' style='text-align: center;'>(Ban hành theo QĐ số19/2006/QĐ-BTC</td>
                                                            </tr>
                                                            <tr>
                                                            <td colspan ='{3}'><b>Mã đơn vị SDNS:</b> 1032173</td> <td colspan ='{4}' style='text-align:center;'>Ngày 30/04/2006 của Bộ trưởng Bộ Tài chính)</td>
                                                            </tr>
                                                            </table>  
                                                            </th>
                                                           </tr>
                                                    <tr>
                                                    <th colspan ='{0}' style='border:none;padding-bottom: 20px;text-align:center;'><h2>BẢNG CHẤM CÔNG</h2></br>
                                                    Ngày tháng
                                                    </th>
                                                    </tr>
                                                    <tr><th colspan ='{0}' style='text-align:right;'>Số:…………….</th></tr>
                                                    <tr>
                                                    <th style='border:1px solid;' rowspan='2'>STT</th>
                                                    <th style='border:1px solid;' rowspan='2'>Họ và tên</th>
                                                    <th style='border:1px solid;' rowspan='2'>NB lương hoặc cấp bậc CV</th>
                                                    <th style='border:1px solid;' colspan='{1}'>Ngày trong tháng</th>
                                                    <th style='border:1px solid;' colspan='3'>Quy ra công</th>
                                                    </tr>", totalCol, day, totalCol, day + 3, 3));
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder.Append("<tr>");
            for (int i = 0; i < day; i++)
            {
                stringBuilder.Append(string.Format("<th style='border:1px solid;'>{0}</th>", fromdate.AddDays(i).Day));
                stringBuilder1.Append(string.Format("<th style='border:1px solid;'>{0}</th>", (i + 1)));
            }
            stringBuilder.Append(@"<th style='border:1px solid;'>số công hưởng lương thời gian</th>
                                                    <th style='border:1px solid;'>số  công nghỉ không lương </th>
                                                    <th style='border:1px solid;'>số công hưởng BHXH</th>");
            stringBuilder.Append("</tr>");
            stringBuilder.Append("<tr>");
            stringBuilder.Append("<th style='border:1px solid;'>A</th><th style='border:1px solid;'>B</th><th style='border:1px solid;'>C</th>");
            stringBuilder1.Append(string.Format("<th style='border:1px solid;'>{0}</th>", day + 1));
            stringBuilder1.Append(string.Format("<th style='border:1px solid;'>{0}</th>", day + 2));
            stringBuilder1.Append(string.Format("<th style='border:1px solid;'>{0}</th>", day + 3));
            stringBuilder.Append(stringBuilder1.ToString());
            stringBuilder.Append("</tr>");
            DataSet data = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_Timekeeping_getdata", parameter: new { fromdate = fromdate, todate = todate });
            int so = 0;
            foreach (DataRow item in data.Tables[0].Rows)
            {
                so++;
                stringBuilder.Append("<tr><td style='border:1px solid;'>" + so + "</td><td style='border:1px solid;'>" + item["Name"] + "</td><td style='border:1px solid;'></td>");
                var datadays = data.Tables[1].Select("Code='" + item["Code"] + "'").CopyToDataTable();
                for (int i = 0; i < day; i++)
                {
                    DataRow[] datarow = datadays.Select("datetimekeeping='" + fromdate.AddDays(i).ToString("yyyy-MM-dd") + "'");
                    if (datarow.Length > 0)
                    {
                        stringBuilder.Append("<td style='border:1px solid;'>X</td>");
                    }
                    else
                    {
                        stringBuilder.Append("<td style='border:1px solid;'></td>");
                    }
                }
                stringBuilder.Append("<td style='border:1px solid;'></td><td style='border:1px solid;'></td><td style = 'border:1px solid;' ></td>");
                stringBuilder.Append("</tr>");
            };
            stringBuilder.Append("<tr>");
            stringBuilder.Append("<td style='border:1px solid;'></td><td style='border:1px solid;'></td><td style='border:1px solid;'><b>Cộng</b></td>");
            for (int i = 0; i < day + 3; i++)
            {
                stringBuilder.Append("<td style='border:1px solid;'></td>");
            }
            stringBuilder.Append("</tr>");
            stringBuilder.Append(string.Format("<tr><td colspan ='{0}' style='text-align:right;'>Ngày.......Tháng.......Năm.......</td></tr>", totalCol));
            int index1 = (totalCol / 3);
            int index3 = (totalCol - (index1 * 2));
            stringBuilder.Append(string.Format("<tr><td colspan ='{0}' style='text-align:center;'><b>Người chấm công</b></td><td colspan ='{1}' style='text-align:center;'><b>Lãnh đạo phòng</b></td><td colspan ='{2}' style='text-align:center;'><b>Giám đốc</b></td></tr>", index1, index1, index3));
            var col = (totalCol / 3);
            stringBuilder.Append("<tr><td></td><td></td><td colspan ='3'><u>Ký hiệu chấm công</u></td><td colspan ='" + col + "'><td colspan ='3'><u>Ký hiệu chấm công</u></td></tr>");
            stringBuilder.Append("<tr><td></td><td></td><td></td><td colspan ='4'>  -Lương thời gian</td><td>+</td><td colspan ='" + col + "'><td colspan ='4'>  - Hội nghị, học tập </td><td>H</td></tr>");
            stringBuilder.Append("<tr><td></td><td></td><td></td><td colspan ='4'>  -Ốm, điều dưỡng</td><td>Ô</td><td colspan ='" + col + "'><td colspan ='4'>  - Nghỉ bù </td><td>Nb</td></tr>");
            stringBuilder.Append("<tr><td></td><td></td><td></td><td colspan ='4'>  -Con ốm</td><td>Cô</td><td colspan ='" + col + "'><td colspan ='4'>  - Nghỉ không lương </td><td>No</td></tr>");
            stringBuilder.Append("<tr><td></td><td></td><td></td><td colspan ='4'>  -Thai sản</td><td>Ts</td><td colspan ='" + col + "'><td colspan ='4'>  - Nghừng việc </td><td>N</td></tr>");
            stringBuilder.Append("<tr><td></td><td></td><td></td><td colspan ='4'>  -Tai nạn</td><td>T</td><td colspan ='" + col + "'><td colspan ='4'>  -Lao động nghĩa vụ </td><td>Lđ</td></tr>");
            stringBuilder.Append("<tr><td></td><td></td><td></td><td colspan ='4'>  -Nghỉ phép</td><td>P</td><td colspan ='" + col + "'><td colspan ='4'>  - Nghỉ lễ tết</td><td>L</td></tr>");
            stringBuilder.Append(string.Format("<tr><td colspan ='{0}' style='text-align:center;'><b>Nguyễn Thị Thảo</b></td><td colspan ='{1}' style='text-align:center;'><b>Phan Liên Hương</b></td><td colspan ='{2}' style='text-align:center;'><b>Nguyễn Trọng Đường</b></td></tr>", index1, index1, index3));
            stringBuilder.Append("</table>");
            return stringBuilder.ToString();
        }
    }
}
