﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class CalendarDayOverrideService : BaseService<CalendarDayOverrideDTO, CalendarDayOverrideBO>, ICalendarDayOverrideService
    {
        public CalendarDayOverrideBO InsertUpdate(CalendarDayOverrideBO data, bool allowSave = true)
        {
            if (data.Id == Guid.Empty)
            {
                data.Id = base.Insert(data);
                if (data.IsWorkingDay == true)
                    ServiceFactory<CalendarDayOverrideShiftService>().InsertRange(data.Id, data.CalendarId, data.DayType);
            }
            else
                base.Update(data);
            return data;
        }
        public CalendarDayOverrideBO GetByCalendarAndDate(Guid calendarId, DateTime date)
        {
            try
            {
                return base.Get(c => c.CalendarId == calendarId && c.Date == date.Date)
                    .FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<CalendarDayOverrideBO> GetByCalendar(Guid calendarId)
        {
            try
            {
                return base.Get(t => t.CalendarId == calendarId);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool CheckDayOverrideHasOverrided(Guid calendarId, DateTime date)
        {
            try
            {
                return base.Get(t => t.CalendarId == calendarId)
                    .Any(t => t.Date.Value.Date == date.Date);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// after copy calendar, data from calendarday override being copy too
        /// </summary>
        /// <param name="fromCalendarId"></param>
        /// <param name="toCalendarId"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public IEnumerable<Guid> CopyCalendarDayOverride(Guid fromCalendarId, Guid toCalendarId, bool allowSave = true)
        {
            var fromCalendarOverride = Get(i => i.CalendarId == fromCalendarId);
            var toCalendarOverride = Enumerable.Empty<CalendarDayOverrideBO>();
            foreach (var item in fromCalendarOverride)
            {
                var copyItem = new CalendarDayOverrideBO()
                {
                    CopyFrom = item.Id,
                    Id = Guid.NewGuid(),
                    IsHoliday = item.IsHoliday,
                    IsWorkingDay = item.IsWorkingDay,
                    Date = item.Date,
                    CalendarId = toCalendarId,
                    IsOverride = false
                };
                toCalendarOverride = toCalendarOverride.Concat(new[] { copyItem });
                ServiceFactory<CalendarDayOverrideShiftService>().CopyCalendarShiftOverride(item.Id, copyItem.Id, false);
            }
            var ids = InsertRange(toCalendarOverride, false);
            SaveTransaction(allowSave);
            return ids;
        }

        public IEnumerable<CalendarDayOverrideBO> GetByDayTypeBeetweenDate(Guid calendarId, DateTime startDate, DateTime endDate, IEnumerable<int> dayTypes = null)
        {
            if (dayTypes == null)
            {
                var calendarWeekShifts = ServiceFactory<CalendarWeekShiftService>().GetByCalendar(calendarId);
                dayTypes = ServiceFactory<CalendarWeekShiftService>().GetDayTypeOff(calendarWeekShifts);
            }
            return Get(i => i.CalendarId == calendarId
                && dayTypes.Contains((int)i.Date.Value.DayOfWeek)
                && startDate <= i.Date && i.Date <= endDate
                );
        }

        public IEnumerable<CalendarDayOverrideBO> GetOverrideToWorking(Guid calendarId, DateTime startDate, DateTime endDate, IEnumerable<int> dayTypeOff = null)
        {
            var data = GetByDayTypeBeetweenDate(calendarId, startDate, endDate, dayTypeOff).Where(i => i.IsWorkingDay == true);
            return data;
        }

        public IEnumerable<CalendarDayOverrideBO> GetOverrideToDayOff(Guid calendarId, DateTime startDate, DateTime endDate, IEnumerable<int> dayTypeOff = null)
        {
            //1. get working day type 
            var workingDayType = Common.Utilities.DayOfWeek.GetDayOfWeek().Where(i => !dayTypeOff.Contains(i));
            var data = GetByDayTypeBeetweenDate(calendarId, startDate, endDate, dayTypeOff).Where(i => i.IsWorkingDay != true || i.IsHoliday == true);
            return data;
        }
    }
}