﻿
using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class CalendarService : BaseService<CalendarDTO, CalendarBO>, ICalendarService
    {
        public TimeSpan GetWorkingDay(Guid? calendarID, DateTime startDate, DateTime endDate)
        {
            TimeSpan result = new TimeSpan(0, 0, 0);
            //1. If calendarId == null  
            if (calendarID == null || calendarID == Guid.Empty)
                result = endDate - startDate;
            // else 
            //2. If date of startDate = date of endDate
            //3. Get calendar shift by 
            else
            {
                if (startDate.Date == endDate.Date)
                {
                    var dayType = (int)startDate.DayOfWeek;
                    //3.1. Get calendar shift by date 
                    var calendarShifts = ServiceFactory<CalendarDayOverrideShiftService>().GetByDate(calendarID.Value, startDate);
                    //3.1.1 If has not any shift calendar
                    if (calendarShifts == null || calendarShifts.Count() == 0)
                    {
                        result = endDate - startDate;
                    }
                    //3.1.2 
                    else
                    {
                        //3.1.2.1 If startDate and endDate is not in any calendar shift: has not woking day, hour
                        if (ServiceFactory<CalendarDayOverrideShiftService>().CheckTimeConflict(calendarShifts, startDate.TimeOfDay, endDate.TimeOfDay))
                        {
                            if (startDate.TimeOfDay <= calendarShifts.Min(i => i.StartTime.Value) && calendarShifts.Max(i => i.EndTime.Value) <= endDate.TimeOfDay)
                                result = new TimeSpan(1, 0, 0, 0);
                            else
                            {
                                //3.1.2.2. Get all shift when have  startDate <=  shift start time  and shift end time <= endDate => sum all time of shift
                                var shiftBeetweens = calendarShifts.Where(i => startDate.TimeOfDay <= i.StartTime && i.EndTime <= endDate.TimeOfDay);
                                // Loop
                                foreach (var shift in shiftBeetweens)
                                {
                                    var durationTime = shift.EndTime.Value - shift.StartTime.Value;
                                    result = result.Add(durationTime);
                                }
                                //3.1.2.3 Get all shift have shift start time < startDate < shift end time
                                var startShift = calendarShifts.Where(i => i.StartTime < startDate.TimeOfDay && startDate.TimeOfDay < i.EndTime).FirstOrDefault();
                                if (startShift != null)
                                {
                                    var durationTime = startShift.EndTime.Value - startDate.TimeOfDay;
                                    result = result.Add(durationTime);
                                }
                                //3.1.2.4 Get all shift have shift start time < endDate  < shift end time
                                var endShift = calendarShifts.Where(i => i.StartTime < endDate.TimeOfDay && endDate.TimeOfDay < i.EndTime).FirstOrDefault();
                                if (endShift != null)
                                {
                                    var durationTime = endDate.TimeOfDay - endShift.StartTime.Value;
                                    result = result.Add(durationTime);
                                }
                            }
                        }
                    }
                }
                else
                {
                    // Calendar week shift
                    var calendarWeekShifts = ServiceFactory<CalendarWeekShiftService>().GetByCalendar(calendarID.Value);
                    //3.6. Get day of week when its hasn't any shift => return day of week array
                    var dayTypeOff = ServiceFactory<CalendarWeekShiftService>().GetDayTypeOff(calendarWeekShifts);
                    //3.5. Get day diff from startDate to endDate: count all date beetween startDate and endDate
                    var amongDays = (endDate - startDate).Days - 1;
                    if (amongDays > 0)
                    {
                        // Count day has not shift and beetween startDate and endDate
                        var countDayOff = Utilities.DayOfWeek.CountByDayTypeBetweenDate(dayTypeOff, startDate.AddDays(1), endDate.AddDays(-1));
                        // Count day has not 
                        var countOverrideToWorking = ServiceFactory<CalendarDayOverrideService>().GetOverrideToWorking(calendarID.Value, startDate.AddDays(1), endDate.AddDays(-1), dayTypeOff).Count();
                        var countOverrideToDayOff = ServiceFactory<CalendarDayOverrideService>().GetOverrideToDayOff(calendarID.Value, startDate.AddDays(1), endDate.AddDays(-1), dayTypeOff).Count();
                        amongDays = amongDays - countDayOff + countOverrideToWorking - countOverrideToDayOff;
                    }

                    result = result.Add(new TimeSpan(amongDays, 0, 0, 0));
                    var fistDayShifts = ServiceFactory<CalendarDayOverrideShiftService>().GetByDate(calendarID.Value, startDate);
                    if (startDate.TimeOfDay <= fistDayShifts.Min(i => i.StartTime))
                    {
                        result = result.Add(new TimeSpan(1, 0, 0, 0));
                    }
                    else
                    {
                        var durationTime = new TimeSpan();
                        var startShift = fistDayShifts.Where(i => i.StartTime < startDate.TimeOfDay && startDate.TimeOfDay < i.EndTime).FirstOrDefault();
                        if (startShift != null)
                        {
                            durationTime = durationTime.Add(startShift.EndTime.Value - startDate.TimeOfDay);
                        }
                        var shiftAfter = fistDayShifts.Where(i => startDate.TimeOfDay <= i.StartTime);
                        // Loop
                        foreach (var shift in shiftAfter)
                        {
                            durationTime = durationTime.Add(shift.EndTime.Value - shift.StartTime.Value);
                        }
                        result = result.Add(durationTime);
                    }
                    var lastDayShifts = ServiceFactory<CalendarDayOverrideShiftService>().GetByDate(calendarID.Value, endDate);
                    if (endDate.TimeOfDay >= lastDayShifts.Max(i => i.EndTime))
                    {
                        result = result.Add(new TimeSpan(1, 0, 0, 0));
                    }
                    else
                    {
                        var durationTime = new TimeSpan();
                        var endShift = lastDayShifts.Where(i => i.StartTime < endDate.TimeOfDay && endDate.TimeOfDay < i.EndTime).FirstOrDefault();
                        if (endShift != null)
                        {
                            durationTime = durationTime.Add(endDate.TimeOfDay - endShift.StartTime.Value);
                        }
                        var shiftBefore = lastDayShifts.Where(i => endDate.TimeOfDay >= i.EndTime);
                        // Loop
                        foreach (var shift in shiftBefore)
                        {
                            durationTime = durationTime.Add(shift.EndTime.Value - shift.StartTime.Value);
                        }
                        result = result.Add(durationTime);
                    }

                }
            }
            return result;

        }


        public override Guid Insert(CalendarBO data, bool allowSave = true)
        {
            data.Id = base.Insert(data, allowSave);
            // when calendar
            if (data.ParentId != null)
            {
                ServiceFactory<CalendarWeekShiftService>().CopyCalendarShift(data.ParentId.Value, data.Id);
                ServiceFactory<CalendarDayOverrideService>().CopyCalendarDayOverride(data.ParentId.Value, data.Id);
            }
            return data.Id;
        }

        public IEnumerable<CalendarBO> GetActive()
        {
            var data = Get(i => i.IsActive == true);
            return data;
        }
    }
}
