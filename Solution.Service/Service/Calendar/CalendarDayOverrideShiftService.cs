﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class CalendarDayOverrideShiftService : BaseService<CalendarDayOverrideShiftDTO, CalendarDayOverrideShiftBO>, ICalendarDayOverrideShiftService
    {
        public IEnumerable<CalendarDayOverrideShiftBO> GetByCalendarDayOverride(Guid calendarDayOverrideId)
        {
            return base.Get(t => t.CalendarDayOverrideId == calendarDayOverrideId);
        }
        public IEnumerable<CalendarDayOverrideShiftBO> GetByCalendar(Guid calendarId)
        {
            try
            {
                return base.Get(t => t.CalendarDayOverrideId == calendarId)
                    .Select(item => new CalendarDayOverrideShiftBO()
                    {
                        IsOverTime = item.IsOverTime
                    })
                    .Distinct();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<CalendarDayOverrideShiftBO> GetByDate(Guid calendarId, DateTime date)
        {
            var data = ServiceFactory<CalendarWeekShiftService>().GetByCalendarAndDayType(calendarId, (int)date.DayOfWeek)
                   .Select(t => new CalendarDayOverrideShiftBO()
                   {
                       Id = t.Id,
                       StartTime = t.StartTime,
                       EndTime = t.EndTime
                   });
            var calendarDayOveride = ServiceFactory<CalendarDayOverrideService>().GetByCalendarAndDate(calendarId, date);
            if (calendarDayOveride != null)
            {
                data = GetByCalendarDayOverride(calendarDayOveride.Id);
            }
            return data;
        }
        /// <summary>
        /// Copy all calendar 
        /// Source : calendar day override shift
        /// Destination : Calendar day shift overrride
        /// </summary>
        /// <param name="fromCalendarOverride"></param>
        /// <param name="toCalendarOverride"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public IEnumerable<Guid> CopyCalendarShiftOverride(Guid fromCalendarOverride, Guid toCalendarOverride, bool allowSave = false)
        {
            //1. Get data copies : have calendar day overrride Id = from Calendar override 
            var dataCopies = Get(i => i.CalendarDayOverrideId == fromCalendarOverride)
                //2. create new data copy save data source to new destination
                .Select(i =>
                {
                    i.CopyFrom = i.Id;
                    i.IsOverride = false;
                    i.Id = Guid.NewGuid();
                    i.CalendarDayOverrideId = toCalendarOverride;
                    return i;
                });
            var ids = InsertRange(dataCopies, false);
            SaveTransaction(allowSave);
            return ids;
        }
        /// <summary>
        /// Copy shift to override 
        /// Source: calendar day shift
        /// Destination: calendar day shift override
        /// </summary>
        /// <param name="calendarOverrideId"></param>
        /// <param name="calendarShift"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public IEnumerable<Guid> InsertRange(Guid calendarOverrideId, Guid calendarId, int dayType, bool allowSave = false)
        {
            var dataCopies = ServiceFactory<CalendarWeekShiftService>().GetByCalendarAndDayType(calendarId, dayType)
                .Select(
                i => new CalendarDayOverrideShiftBO
                {
                    Id = Guid.NewGuid(),
                    IsOverride = false,
                    StartTime = i.StartTime,
                    EndTime = i.EndTime,
                    CalendarDayOverrideId = calendarOverrideId
                });
            var ids = base.InsertRange(dataCopies, allowSave);
            return ids;

        }
        /// <summary>
        /// return any shift has been conflict
        /// </summary>
        /// <param name="data"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public bool CheckTimeConflict(IEnumerable<CalendarDayOverrideShiftBO> data, TimeSpan startTime, TimeSpan endTime)
        {
            return data.Any(t => (startTime <= t.StartTime && t.StartTime < endTime)
                            || (startTime < t.EndTime && t.EndTime <= endTime)
                            || (startTime <= t.StartTime && t.EndTime <= endTime));
        }
    }
}