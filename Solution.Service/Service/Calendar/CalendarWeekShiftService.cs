﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
namespace iDAS.Service
{
    public class CalendarWeekShiftService : BaseService<CalendarWeekShiftDTO, CalendarWeekShiftBO>, ICalendarWeekShiftService
    {
        private string GetDayType(int? daytype)
        {
            switch (daytype)
            {
                case 0: return DayOfWeek.Sunday.ToString();
                case 1: return DayOfWeek.Monday.ToString();
                case 2: return DayOfWeek.Tuesday.ToString();
                case 3: return DayOfWeek.Wednesday.ToString();
                case 4: return DayOfWeek.Thursday.ToString();
                case 5: return DayOfWeek.Friday.ToString();
                default: return DayOfWeek.Saturday.ToString();
            }
        }
        public IEnumerable<object> GetWeekShiftCalendar(Guid calendarId)
        {
            try
            {
                var data = base.Get(c => c.CalendarId == calendarId)
                      .OrderBy(t => t.CreateAt)
                      .ToList();
                var calendarShift = new List<object>();
                foreach (var item in data.GroupBy(t => t.RowId))
                {
                    var row = item.ToList();
                    var obj = new ExpandoObject() as IDictionary<string, object>;
                    foreach (var i in row)
                    {
                        obj.Add(GetDayType(i.DayType), i.StartTime.ToString().Remove(i.StartTime.ToString().Length - 3) + " - " + i.EndTime.ToString().Remove(i.StartTime.ToString().Length - 3));
                    }
                    obj.Add("RowID", row[0].RowId);
                    calendarShift.Add(obj);
                }
                return calendarShift;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public CalendarWeekShiftBO GetByRowIDAndDayType(Guid rowId, int dayType)
        {
            try
            {
                return base
                  .Get(c => c.RowId == rowId && c.DayType == dayType)
                  .FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public Guid GetCalendarByRowID(Guid rowId)
        {
            try
            {
                return base
                    .Get(t => t.RowId == rowId)
                    .Select(t => t.CalendarId)
                    .FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// Get all shift by day of week
        /// </summary>
        /// <param name="calendarId"></param>
        /// <param name="dayType"></param>
        /// <returns></returns>
        public IEnumerable<CalendarWeekShiftBO> GetByCalendarAndDayType(Guid calendarId, int dayType)
        {
            try
            {
                return base.Get(t => t.CalendarId == calendarId && t.DayType == dayType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Check time is conflict 
        /// </summary>
        /// <param name="calendarId"></param>
        /// <param name="dayType"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public bool CheckTimeShiftExits(Guid calendarId, int dayType, TimeSpan startTime, TimeSpan endTime)
        {
            try
            {
                return base
                        .Get(t => t.CalendarId == calendarId && t.DayType == dayType)
                        .Any(t =>
                             (startTime <= t.StartTime && t.StartTime < endTime)
                            || (startTime < t.EndTime && t.EndTime <= endTime)
                            || (startTime <= t.StartTime && t.EndTime <= endTime));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CheckTimeShiftExits(Guid calendarId, int dayType, TimeSpan startTime, TimeSpan endTime, bool checkOverride)
        {
            try
            {
                return base
                        .Get(t => t.CalendarId == calendarId && t.DayType == dayType)
                        .Any(t =>
                             (startTime <= t.StartTime && t.StartTime < endTime)
                            || (startTime < t.EndTime && t.EndTime <= endTime)
                            || (startTime <= t.StartTime && t.EndTime <= endTime));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CheckTimeShiftExitsEdit(Guid id, Guid calendarId, int dayType, TimeSpan startTime, TimeSpan endTime)
        {
            try
            {
                return base
                        .Get(t => t.CalendarId == calendarId && t.DayType == dayType)
                        .Where(t => t.Id != id)
                        .Any(t =>
                             (startTime <= t.StartTime && t.StartTime < endTime)
                            || (startTime < t.EndTime && t.EndTime <= endTime)
                            || (startTime <= t.StartTime && t.EndTime <= endTime));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public IEnumerable<Guid> CopyCalendarShift(Guid fromCalendarId, Guid toCalendarId, bool allowSave = true)
        {
            var fromCalendar = Get(i => i.CalendarId == fromCalendarId);
            var toCalendar = fromCalendar
                .Select(i =>
                {
                    i.CopyFrom = i.Id;
                    i.Id = Guid.Empty;
                    i.IsOverride = false;
                    i.CalendarId = toCalendarId;
                    return i;
                });
            var ids = InsertRange(toCalendar, false);
            SaveTransaction(allowSave);
            return ids;
        }
        public IEnumerable<CalendarWeekShiftBO> GetByCalendar(Guid calendarID)
        {
            return Get(i => i.CalendarId == calendarID);
        }
        public IEnumerable<int> GetDayTypeOff(Guid calendarID)
        {
            var dayOfWeek = Common.Utilities.DayOfWeek.GetDayOfWeek();
            var result = dayOfWeek.Where(u => !Get(i => i.CalendarId == calendarID).Select(i => i.DayType.Value).Distinct().Contains(u));
            return result;
        }

        public IEnumerable<int> GetDayTypeOff(IEnumerable<CalendarWeekShiftBO> data)
        {
            var dayOfWeek = Common.Utilities.DayOfWeek.GetDayOfWeek();
            var result = dayOfWeek.Where(u => !data.Select(i => i.DayType.Value).Distinct().Contains(u));
            return result;
        }
    }
}