﻿using iDAS.DataAccess;

namespace iDAS.Service
{
    public class CoreService : ICoreService
    {
        public bool UpdateDatabase(DatabaseBO item)
        {
            string connectionString = "Server={0};Database={1};User Id={2};Password={3};";
            connectionString = string.Format(connectionString, item.Server, item.Database, item.UserId, item.Password);
            UnitOfWork unitOfWork = new UnitOfWork();
            return unitOfWork.MigrationDbContext(connectionString);
        }
    }
}
