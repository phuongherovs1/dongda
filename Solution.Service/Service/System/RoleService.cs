﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public class RoleService : BaseService<SystemRoleDTO, RoleBO>, IRoleService
    {
        #region IRoleStore<RoleBO, Guid> Members
        public Task CreateAsync(RoleBO role)
        {
            if (role == null)
                throw new ArgumentNullException("role");
            base.Insert(role);
            return SaveTransactionAsync();
        }
        public Task UpdateAsync(RoleBO role)
        {
            if (role == null)
                throw new ArgumentNullException("role");
            base.Update(role);
            return SaveTransactionAsync();
        }
        public Task DeleteAsync(RoleBO role)
        {
            if (role == null)
                throw new ArgumentNullException("role");
            base.Delete(role.Id);
            return SaveTransactionAsync();
        }
        public Task<RoleBO> FindByIdAsync(Guid roleId)
        {
            var role = base.GetById(roleId);
            return Task.FromResult<RoleBO>(role);
        }
        public Task<RoleBO> FindByNameAsync(string roleName)
        {
            var role = base.Get(i => i.Name == roleName).FirstOrDefault();
            return Task.FromResult<RoleBO>(role);
        }
        #endregion

        #region IQueryableRoleStore<RoleBO, Guid> Members
        public IQueryable<RoleBO> Roles
        {
            get
            {
                return base.GetAll().AsQueryable();
            }
        }
        #endregion

        public IEnumerable<RoleBO> FindByUserId(Guid userId)
        {
            var roleIds = new List<Guid>();
            //b1: Lấy danh sách quyền của nhân sự đăng nhập
            //var roleIdsOfUser = ServiceFactory<UserRoleService>().GetRoleIds(userId).ToList();
            //if (roleIdsOfUser.Count > 0) roleIds.AddRange(roleIdsOfUser);
            // bước 2 lấy danh sách quyền của chức danh nhân sự đăng nhập
            var roleIdsOfTitle = ServiceFactory<TitleRoleService>().GetRoleIds(userId).ToList();
            if (roleIdsOfTitle.Count > 0) roleIds.AddRange(roleIdsOfTitle);

            return base.GetByIds(roleIds);
        }
        public RoleBO FindByCode(string code)
        {
            return base.Get(i => i.Code == code, allowDeleted: false).FirstOrDefault();
        }
        public void InsertOrUpdate(IEnumerable<RoleBO> roles)
        {
            try
            {
                base.DeleteRange(base.Get(allowDeleted: true).Select(i => i.Id), false);
                foreach (var role in roles)
                {
                    var item = FindByCode(role.Code);
                    if (item != null)
                    {
                        role.Id = item.Id;
                        Update(role, false);
                    }
                    else
                    {
                        Insert(role, false);
                    }
                }
                SaveTransaction();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public override IEnumerable<RoleBO> GetAll(bool allowDelete = false)
        {
            var roles = base.GetAll(allowDelete);
            var roleIds = ServiceFactory<UserRoleService>().GetRoleIds();
            foreach (var role in roles)
            {
                role.IsPermission = roleIds.Contains(role.Id);
            }
            return roles;
        }
        public IEnumerable<RoleBO> GetAll(Guid userId, bool allowDeleted = false)
        {
            var roles = base.GetAll(allowDeleted);
            var roleIds = ServiceFactory<UserRoleService>().GetRoleIds(userId);
            foreach (var role in roles)
            {
                role.IsPermission = roleIds.Contains(role.Id);
            }
            return roles;
        }
        public IEnumerable<RoleBO> GetAll(Guid userId, string module, bool allowDeleted = false)
        {
            var roles = base.Get(allowDeleted: allowDeleted).Where(i => i.Module == module);
            var roleIds = ServiceFactory<UserRoleService>().GetRoleIds(userId);
            foreach (var role in roles)
            {
                role.IsPermission = roleIds.Contains(role.Id);
            }
            return roles;
        }
        public IEnumerable<string> GetModules()
        {
            return base.Get().Select(i => i.Module).Distinct();
        }
        public IEnumerable<RoleBO> GetAll(Guid userId, int pageIdex, int pageSize, out int totalCount, bool allowDeleted = false)
        {
            var query = base.GetAll(allowDeleted);
            totalCount = query.Count();
            var roleIds = ServiceFactory<UserRoleService>().GetRoleIds(userId);
            foreach (var item in query)
            {
                item.IsPermission = roleIds.Contains(item.Id);
            }
            var results = base.Page(query, pageIdex, pageSize);
            return results;
        }
        public IEnumerable<RoleBO> GetAll(Guid userId, string module, int pageIdex, int pageSize, out int totalCount, bool allowDeleted = false)
        {
            var query = base.Get(allowDeleted: allowDeleted).Where(i => i.Module == module);
            totalCount = query.Count();
            base.Page(query, pageIdex, pageSize);
            var roleIds = ServiceFactory<UserRoleService>().GetRoleIds(userId);
            foreach (var item in query)
            {
                item.IsPermission = roleIds.Contains(item.Id);
            }
            return query;
        }
        public IEnumerable<RoleBO> GetAllByTittle(Guid titleId, int pageIdex, int pageSize, out int totalCount, bool allowDeleted = false)
        {
            var query = base.GetAll(allowDeleted);
            totalCount = query.Count();
            base.Page(query, pageIdex, pageSize);
            var roleIds = ServiceFactory<TitleRoleService>().GetRoleIdsByTitleId(titleId);
            foreach (var item in query)
            {
                item.IsPermission = roleIds.Contains(item.Id);
            }
            return query;
        }
        public IEnumerable<RoleBO> GetAllByTittle(Guid titleId, string module, int pageIdex, int pageSize, out int totalCount, bool allowDeleted = false)
        {
            var query = base.Get(allowDeleted: allowDeleted).Where(i => i.Module == module);
            totalCount = query.Count();
            base.Page(query, pageIdex, pageSize);
            var roleIds = ServiceFactory<TitleRoleService>().GetRoleIdsByTitleId(titleId);
            foreach (var item in query)
            {
                item.IsPermission = roleIds.Contains(item.Id);
            }
            return query;
        }
    }
}
