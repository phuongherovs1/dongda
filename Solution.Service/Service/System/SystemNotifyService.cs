﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class SystemNotifyService : BaseService<SystemNotifyDTO, SystemNotifyBO>, ISystemNotifyService
    {

        public IEnumerable<SystemNotifyBO> GetByUser(Guid userId)
        {
            var data = base.GetQuery().Where(c => c.EmployeeId == userId).Take(70)
                .OrderByDescending(c => c.CreateAt);
            return data;
        }
        public IEnumerable<SystemNotifyBO> GetByCurrentUser()
        {
            return GetByUser(UserId);
        }
        public int GetTotalNotRead()
        {
            return base.GetQuery().Where(c => c.EmployeeId == UserId && c.IsRead != true).Count();
        }
        public int UpdateRead(Guid id, bool isRead = true)
        {
            var obj = base.GetById(id);
            if (obj != null)
            {
                obj.IsRead = isRead;
                if (isRead)
                    obj.ReadTime = DateTime.Now;
                base.Update(obj);
            }
            return base.GetQuery().Where(c => c.IsRead != true && c.EmployeeId == UserId).Count();
        }
        public IEnumerable<string> Insert(string title, string content, IEnumerable<Guid> employeeIDs, string url, object param)
        {
            foreach (var employeeID in employeeIDs)
            {
                var notify = new SystemNotifyBO()
                {
                    EmployeeId = employeeID,
                    Title = title,
                    Contents = content,
                    IsRead = false,
                    Url = url,
                    Params = JsonConvert.SerializeObject(param),
                    DateSend = DateTime.Now
                };
                base.Insert(notify, false);
            }
            /// Danh sách user nhận thông báo
            var result = ServiceFactory<UserService>().GetByIds(employeeIDs).Where(i => !string.IsNullOrEmpty(i.UserName)).Select(i => i.UserName);

            base.SaveTransaction();
            return result;
        }
        public string Insert(string title, string content, Guid employeeID, string url, object param, int type = 0)
        {
            var notify = new SystemNotifyBO()
            {
                EmployeeId = employeeID,
                Title = title,
                Contents = content,
                IsRead = false,
                Url = url,
                Params = JsonConvert.SerializeObject(param),
                DateSend = DateTime.Now,
                Type = type
            };

            var result = ServiceFactory<UserService>().GetById(employeeID).UserName;

            base.Insert(notify);
            return result;
        }
        public IEnumerable<SystemNotifyBO> GetMoreByCurrentUser(int start, int limit, out int count)
        {
            var data = base.GetQuery()
                .Where(c => c.EmployeeId == UserId)
                .OrderByDescending(c => c.CreateAt);
            count = data.Count();
            var results = data.Skip(start).Take(limit).ToList();
            var createIds = results.Where(t => t.CreateBy.HasValue).Select(t => t.CreateBy.Value).Distinct();
            var creatersQuery = ServiceFactory<EmployeeService>().GetByIds(createIds).ToList();
            results = results.Select(i =>
            {
                if (i.CreateBy.HasValue)
                {
                    var creater = creatersQuery.FirstOrDefault(c => c.Id == i.CreateBy.Value);
                    i.Creater = creater != null ? creater.Name: "";
                }
                return i;
            }).ToList();
            return results;
        }
        public int GetCountByCurrentUser()
        {
            return base.GetQuery().Where(c => c.EmployeeId == UserId).Count();
        }


        public void Revert(Guid id)
        {
            var obj = base.GetById(id, true);
            if (obj != null)
            {
                obj.IsDelete = false;
                base.Update(obj);
            }
        }
    }
}
