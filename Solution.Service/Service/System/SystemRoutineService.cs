﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class SystemRoutineService : BaseService<SystemRoutineDTO, SystemRoutineBO>, ISystemRoutineService
    {
        public void Perform(DateTime dateTime)
        {
            var notPerforms = GetNotPerforms(dateTime);
            if (notPerforms.Count > 0)
            {
                foreach (var notPerform in notPerforms)
                {
                    if (notPerform.Type.HasValue)
                    {
                        switch (notPerform.Type.Value)
                        {
                            case (int)iDAS.Service.Common.Resource.RoutineType.QualityMeetingPlan:
                                // Tạo định kỳ
                                ServiceFactory<QualityMeetingPlanDetailService>().CreateByRoutine(new Guid(notPerform.Params));
                                // Cập nhật thành công
                                UpdateSuccess(notPerform.Id);
                                break;
                        }
                    }
                }
            }

        }
        public List<SystemRoutineBO> GetNotPerforms(DateTime dateTime)
        {
            var compareDate = dateTime.AddHours(1);
            var notPerforms = GetQuery().Where(i => i.IsComplete == null || (i.IsComplete.HasValue && i.IsComplete == false))
                                         .Where(i => i.DatePerform.HasValue && i.DatePerform <= compareDate).ToList();
            return notPerforms;
        }
        public override Guid Insert(SystemRoutineBO data, bool allowSave = true)
        {
            data.IsComplete = false;
            return base.Insert(data, allowSave);
        }
        public Guid InsertRoutine(DateTime? datePerform, iDAS.Service.Common.Resource.RoutineType type, string param)
        {
            return Insert(new SystemRoutineBO()
            {
                DatePerform = datePerform,
                Type = (int)type,
                Params = param
            });
        }
        private void UpdateSuccess(Guid id)
        {
            var item = GetById(id);
            item.IsComplete = true;
            Update(item);
        }
    }
}
