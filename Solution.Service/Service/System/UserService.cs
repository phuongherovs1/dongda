﻿using iDAS.DataAccess;
using iDAS.Service.Service.System;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public class UserService : BaseService<SystemUserDTO, UserBO>, IUserService
    {
        #region IUserStore<UserBO, Guid>

        public Task CreateAsync(UserBO user)
        {
            try
            {
                Insert(user, true);
                return SaveTransactionAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Task UpdateAsync(UserBO user)
        {
            try
            {
                Update(user, false);
                return SaveTransactionAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public UserBO GetCurrentUser()
        {
            return base.GetById(UserId);
        }

        public Task DeleteAsync(UserBO user)
        {
            try
            {
                Delete(user.Id, false);
                return SaveTransactionAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Task<UserBO> FindByIdAsync(Guid userId)
        {
            var user = GetById(userId);
            if (user != null)
                mapUserFromEmployee(user);
            return Task.FromResult<UserBO>(user);
        }
        public Task<UserBO> FindByNameAsync(string userName)
        {
            var user = Get(i => i.UserName == userName).FirstOrDefault();
            if (user != null)
                mapUserFromEmployee(user);
            return Task.FromResult<UserBO>(user);
        }
        #endregion

        //#region IUserClaimStore<UserBO, Guid>
        //public Task AddClaimAsync(UserBO user, Claim claim)
        //{
        //    if (user == null)
        //        throw new ArgumentNullException("user");
        //    if (claim == null)
        //        throw new ArgumentNullException("claim");

        //    var u = GetByID(user.Id);
        //    if (u == null)
        //        throw new ArgumentException("UserBO does not correspond to a User entity.", "user");

        //    var item = new ClaimBO
        //    {
        //        ClaimType = claim.Type,
        //        ClaimValue = claim.Value,
        //        UserId = user.Id,
        //    };
        //    new ClaimService().Insert(item);

        //    Update(u);
        //    return SaveTransactionAsync();
        //}

        //public Task<IList<Claim>> GetClaimsAsync(UserBO user)
        //{
        //    if (user == null)
        //        throw new ArgumentNullException("user");

        //    var u = GetByID(user.Id);
        //    if (u == null)
        //        throw new ArgumentException("UserBO does not correspond to a User entity.", "user");
        //    var claims = new ClaimService().GetAll();

        //    return Task.FromResult<IList<Claim>>(claims.Select(x => new Claim(x.ClaimType, x.ClaimValue)).ToList());
        //}

        //public Task RemoveClaimAsync(UserBO user, Claim claim)
        //{
        //    if (user == null)
        //        throw new ArgumentNullException("user");
        //    if (claim == null)
        //        throw new ArgumentNullException("claim");

        //    var u = GetByID(user.Id);
        //    if (u == null)
        //        throw new ArgumentException("UserBO does not correspond to a User entity.", "user");

        //    //var c = u.Claims.FirstOrDefault(x => x.ClaimType == claim.Type && x.ClaimValue == claim.Value);
        //    //u.Claims.Remove(c);

        //    Update(u);
        //    return SaveTransactionAsync();
        //}
        //#endregion

        #region IUserLoginStore<UserBO, Guid>
        public Task AddLoginAsync(UserBO user, UserLoginInfo login)
        {
            try
            {
                var userLogin = new UserLoginBO()
                {
                    SystemUserId = user.Id,
                    LoginProvider = login.LoginProvider,
                    ProviderKey = login.ProviderKey,
                };
                ServiceFactory<UserLoginService>().Insert(userLogin, false);
                return SaveTransactionAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Task<UserBO> FindAsync(UserLoginInfo login)
        {


            var UserBO = default(UserBO);

            //var l = _unitOfWork.ExternalLoginRepository.GetByProviderAndKey(login.LoginProvider, login.ProviderKey);
            //if (l != null)
            //    UserBO = getUserBO(l.User);

            return Task.FromResult<UserBO>(UserBO);
        }
        public Task<IList<UserLoginInfo>> GetLoginsAsync(UserBO user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            var u = GetById(user.Id);
            if (u == null)
                throw new ArgumentException("UserBO does not correspond to a User entity.", "user");

            var logins = ServiceFactory<UserLoginService>().GetAll();
            return Task.FromResult<IList<UserLoginInfo>>(logins.Select(x => new UserLoginInfo(x.LoginProvider, x.ProviderKey)).ToList());
        }
        public Task RemoveLoginAsync(UserBO user, UserLoginInfo login)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            if (login == null)
                throw new ArgumentNullException("login");

            var u = GetById(user.Id);
            if (u == null)
                throw new ArgumentException("UserBO does not correspond to a User entity.", "user");

            //var l = u.Logins.FirstOrDefault(x => x.LoginProvider == login.LoginProvider && x.ProviderKey == login.ProviderKey);
            //u.Logins.Remove(l);

            Update(u);
            return SaveTransactionAsync();
        }
        #endregion

        #region IUserRoleStore<UserBO, Guid>
        public Task AddToRoleAsync(UserBO user, string roleName)
        {
            try
            {
                var role = ServiceFactory<RoleService>().FindByNameAsync(roleName);
                var userRole = new UserRoleBO()
                {
                    SystemUserId = user.Id,
                    SystemRoleId = role.Result.Id,
                };
                ServiceFactory<UserRoleService>().Insert(userRole, false);
                return SaveTransactionAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Task<IList<string>> GetRolesAsync(UserBO user)
        {
            //var roles = ServiceFactory<RoleService>().FindByUserId(user.Id);
            //return Task.FromResult<IList<string>>(roles.Select(i => i.Name).ToList());
            //////var roles = ServiceFactory<PermissionService>().GetAuthorizeUser(user.Id); 
            PermissionService permissionService = new PermissionService();
            return Task.FromResult<IList<string>>(permissionService.GetAuthorizeUser(user.Id));
        }
        public Task<bool> IsInRoleAsync(UserBO user, string roleName)
        {
            var roles = ServiceFactory<RoleService>().FindByUserId(user.Id);
            return Task.FromResult<bool>(roles.Any(i => i.Name == roleName));
        }
        public Task RemoveFromRoleAsync(UserBO user, string roleName)
        {
            try
            {
                var role = ServiceFactory<RoleService>().FindByNameAsync(roleName);
                ServiceFactory<UserRoleService>().Remove(user.Id, role.Result.Id, false);
                return SaveTransactionAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region IUserPasswordStore<UserBO, Guid>
        public Task<string> GetPasswordHashAsync(UserBO user)
        {
            return Task.FromResult<string>(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(UserBO user)
        {
            return Task.FromResult<bool>(!string.IsNullOrWhiteSpace(user.PasswordHash));
        }
        public Task SetPasswordHashAsync(UserBO user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        #endregion

        #region IUserSecurityStampStore<UserBO, Guid>
        public Task<string> GetSecurityStampAsync(UserBO user)
        {
            return Task.FromResult<string>(user.SecurityStamp);
        }
        public Task SetSecurityStampAsync(UserBO user, string stamp)
        {
            user.SecurityStamp = stamp;
            return Task.FromResult(0);
        }
        #endregion

        private void mapUserFromEmployee(UserBO user, EmployeeBO employee = null)
        {
            if (employee == null)
                employee = ServiceFactory<EmployeeService>().GetById(user.Id);
            if (employee == null)
                throw new ArgumentNullException("employee");
            user.FullName = employee.Name;
            user.Avatar.Data = employee.Avatar;
            user.Email = employee.Email;
            user.PhoneNumber = employee.PhoneNumber;
            user.Password =  "Password";
        }
        private void mapMultiUserFromEmployee(IEnumerable<UserBO> users)
        {
            var employees = ServiceFactory<EmployeeService>().GetByUsers(users);
            foreach (var user in users)
            {
                var employee = employees.FirstOrDefault(i => i.Id == user.Id);
                mapUserFromEmployee(user, employee);
            }
        }
        private EmployeeBO getEmployeeFromUser(UserBO user)
        {
            var employee = new EmployeeBO()
            {
                Id = user.Id,
                Name = user.FullName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Avatar = user.Avatar.Data
            };
            return employee;
        }
        public IEnumerable<UserBO> GetAll(int pageIndex, int pageSize, out int totalCount, bool allowDeleted = false)
        {
            var query = base.GetAll(allowDeleted).OrderBy(i => i.FullName);
            totalCount = query.Count();
            var users = base.Page(query, pageIndex, pageSize);
            mapMultiUserFromEmployee(users);
            return users;
        }
        public IEnumerable<UserBO> GetByQuery(int pageIndex, int pageSize, out int totalCount, bool allowDeleted = false, string strFilter = "")
        {
            CultureInfo culture = new CultureInfo("vi-VN");
            var query = base.GetQuery().OrderBy(i => i.UserName);
            totalCount = query.Count();
            var employeeIds = query.Select(c => c.Id).ToList();
            var employees = ServiceFactory<EmployeeService>().GetByIds(employeeIds).ToList();
            var users = query.ToList().Select(i =>
             {
                 var employee = employees.FirstOrDefault(c => c.Id == i.Id);
                 if (employee != null)
                 {
                     i.FullName = employee.Name;
                     i.Avatar.Data = employee.Avatar;
                     i.Email = employee.Email;
                     i.PhoneNumber = employee.PhoneNumber;
                 }
                 i.LastName = i.FullName.Split(' ').Last();
                 return i;
             }).OrderBy(x => x.LastName, StringComparer.Create(culture, false)).ToList();
            users = users.Where(i => string.IsNullOrEmpty(strFilter) || i.FullName.Contains(strFilter)).ToList();
            users = base.Page(users, pageIndex, pageSize).ToList();
            return users;
        }

        public override UserBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)

        {
            var user = base.GetById(id, allowDeleted, allowDefaultIfNull);
            if (user != null)
                mapUserFromEmployee(user);
            return user;
        }
        public override Guid Insert(UserBO user, bool allowSave = true)
        {
            try
            {
                if (user.Id == null || user.Id == Guid.Empty)
                    user.Id = Guid.NewGuid();
                base.Insert(user, allowSave);
                if (!user.IsExistEmployee)
                {
                    var employee = getEmployeeFromUser(user);
                    employee.Name = user.Name;
                    ServiceFactory<EmployeeService>().Insert(employee, allowSave);
                }
                return user.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public override void Update(UserBO user, bool allowSave = true)
        {
            base.Update(user);
            var employee = getEmployeeFromUser(user);
            ServiceFactory<EmployeeService>().Update(employee);
        }
        public IEnumerable<Guid> GetDepartmentByCurrentUser()
        {
            //Bước 1: lấy danh sách chức danh của người đăng nhập
            var titleIds = ServiceFactory<EmployeeTitleService>().GetTitleIdsByCurrentUser();
            //Bước 2: lấy danh sách phòng ban theo danh sách chức danh đã lấy ở bước 1
            var departmentIds = ServiceFactory<DepartmentTitleService>().GetDepartmentByRoleIDs(titleIds);
            //Trả về danh sách phòng ban đã lấy ở bước 2
            return departmentIds;
        }
    }
}
