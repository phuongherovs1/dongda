﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class UserRoleService : BaseService<SystemUserRoleDTO, UserRoleBO>, IUserRoleService
    {
        public void Remove(Guid userId, Guid roleId, bool allowSave = true)
        {
            try
            {
                var userRoleId = GetId(userId, roleId);
                base.Remove(userRoleId, allowSave);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<Guid> GetRoleIds()
        {
            return base.Get(i => i.SystemUserId == UserId).Select(i => i.SystemRoleId.Value);
        }
        public IEnumerable<Guid> GetRoleIds(Guid userId)
        {
            return base.Get(i => i.SystemUserId == userId).Select(i => i.SystemRoleId.Value);
        }
        public Guid GetId(Guid userId, Guid roleId)
        {
            //return base.Get(i => i.SystemUserId == userId && i.SystemRoleId == roleId).Select(i => i.Id).FirstOrDefault();
            var result = base.GetQuery(true).Where(i => i.SystemUserId == userId && i.SystemRoleId == roleId).FirstOrDefault();
            var result1 = base.GetQuery(false).Where(i => i.SystemUserId == userId && i.SystemRoleId == roleId).FirstOrDefault();
            if (result != null)
                return result.Id;
            else if (result1 != null)
                return result1.Id;
            else
                return Guid.Empty;
        }
        public override void Update(UserRoleBO userRole, bool allowSave = true)
        {
            var id = GetId(userRole.UserId, userRole.RoleId);
            userRole.Id = id;
            if (id == Guid.Empty)
                base.Insert(userRole, allowSave);
            else
                base.Update(userRole, allowSave);
        }

        public bool RoleExits(string code)
        {
            var role = ServiceFactory<RoleService>().FindByCode(code);
            if (role != null)
                return base.Get().Any(t => t.SystemRoleId == role.Id && t.SystemUserId == UserId);
            else
                return false;

        }


        public bool CurrentUser(Guid userid)
        {
            return userid == UserId;
        }
        public List<string> CurrentUserRole(Guid userid)
        {
            var result = base.GetQuery().Where(i => i.SystemUserId == userid).ToList();
            List<string> listrole = new List<string>();
            if (result != null)
                foreach (var item in result)
                {
                    var role = ServiceFactory<RoleService>().GetById((Guid)item.SystemRoleId);
                    listrole.Add(role.Module);
                }
            return listrole.Distinct().ToList();
        }
    }
}
