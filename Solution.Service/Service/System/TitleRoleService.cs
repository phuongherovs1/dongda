﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class TitleRoleService : BaseService<SystemTitleRoleDTO, TitleRoleBO>, ITitleRoleService
    {
        public IEnumerable<Guid> GetRoleIdsByTitleId(Guid titleId)
        {
            return base.Get(i => i.DepartmentTitleId != null && titleId == i.DepartmentTitleId.Value).Select(i => i.SystemRoleId.Value);
        }
        public IEnumerable<Guid> GetRoleIds(Guid userId)
        {
            var titleIds = ServiceFactory<EmployeeTitleService>().GetRoleIdsByEmployeeId(userId).ToList();
            return base.Get(i => i.DepartmentTitleId != null && titleIds.Contains(i.DepartmentTitleId.Value)).Select(i => i.SystemRoleId.Value);
        }
        public bool RoleExits(string code, Guid titleId)
        {
            var role = ServiceFactory<RoleService>().FindByCode(code);
            if (role != null)
                return base.Get().Any(t => t.SystemRoleId == role.Id && t.DepartmentTitleId == titleId);
            else
                return false;
        }
        public Guid GetId(Guid titleId, Guid roleId, bool allowDeleted = true)
        {
            return base.Get(i => i.DepartmentTitleId == titleId && i.SystemRoleId == roleId, allowDeleted: allowDeleted).Select(i => i.Id).FirstOrDefault();
        }
        public override void Update(TitleRoleBO titleRole, bool allowSave = true)
        {
            var id = GetId(titleRole.UserId, titleRole.RoleId);
            titleRole.Id = id;
            if (id == Guid.Empty)
                base.Insert(titleRole, allowSave);
            else
                base.Update(titleRole, allowSave);
        }
        public List<string> GetUserRole(Guid userid)
        {
            var titleIds = ServiceFactory<EmployeeTitleService>().GetRoleIdsByEmployeeId(userid).ToList();
            var result = base.Get(i => i.DepartmentTitleId != null && titleIds.Contains(i.DepartmentTitleId.Value)).Select(i => i.SystemRoleId.Value);
            List<string> listrole = new List<string>();
            if (result != null)
                foreach (var item in result)
                {
                    var role = ServiceFactory<RoleService>().GetById((Guid)item);
                    listrole.Add(role.Module);
                }
            return listrole.Distinct().ToList();
        }

    }
}
