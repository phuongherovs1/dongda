﻿using iDAS.ADO;
using iDAS.Service.API.System;
using System;
using System.Collections.Generic;
using System.Data;

namespace iDAS.Service.Service.System
{
    public class PermissionService : IPermissionService
    {
        public DataTable GetListRolePermission(Guid IdUser, int RoleID)
        {
            DataTable data = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_SystemRoles_Permission", parameter: new { IdUser = IdUser, RoleID = RoleID });
            return data;
            // return data.Select(string.Format("(RoleID={0} or SupervisorID={0}) and (Supervisor =0 or Supervisor is null)", RoleID)).CopyToDataTable();
        }

        public DataTable GetListRoleTitlePermission(Guid DepartmentTitleId, int RoleID)
        {
            return Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_SystemRolesTitle_Permission", parameter: new { DepartmentTitleId = DepartmentTitleId, RoleID = RoleID });
        }

        public DataTable GetListTreeRole(int ID)
        {
            return Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_SystemRoles_TreeRole", parameter: new { ID = ID });
        }
        public List<string> GetAuthorizeUser(Guid IdUser)
        {
            var data = ViewDataMenu(IdUser);
            List<string> Rolename = new List<string>();
            foreach (DataRow item in data.Rows)
            {
                if (item["Supervisor"] == DBNull.Value || !Convert.ToBoolean(item["Supervisor"]))
                {
                    Rolename.Add(item["RoleName"].ToString());
                }

            }
            return Rolename;
        }
        public bool UpdateUserRole(Guid IdUser, int RoleID, string Ispermission, string IsRoleEdit)
        {
            if(!string.IsNullOrEmpty(Ispermission) && string.IsNullOrEmpty(IsRoleEdit))
                return Sql_ADO.idasAdoService.ExecuteNoquery("sp_SystemRolesTest_UpdateRoleUser", parameter: new { IdUser = IdUser, RoleID = RoleID, IsRoleEdit = "", Ispermission = Ispermission}) > 0;
            else if (string.IsNullOrEmpty(Ispermission) && !string.IsNullOrEmpty(IsRoleEdit))
                return Sql_ADO.idasAdoService.ExecuteNoquery("sp_SystemRolesTest_UpdateRoleUser", parameter: new { IdUser = IdUser, RoleID = RoleID, Ispermission = "", IsRoleEdit = IsRoleEdit }) > 0;
            else
                return Sql_ADO.idasAdoService.ExecuteNoquery("sp_SystemRolesTest_UpdateRoleUser", parameter: new { IdUser = IdUser, RoleID = RoleID, Ispermission = Ispermission, IsRoleEdit = IsRoleEdit }) > 0;
        }

        public bool UpdateUserRoleTitle(Guid DepartmentTitleId, int RoleID, string Ispermission, string IsRoleEdit)
        {
            //return Sql_ADO.idasAdoService.ExecuteNoquery("sp_SystemRolesTestTitle_UpdateRoleUser", parameter: new { DepartmentTitleId = DepartmentTitleId, RoleID = RoleID, Ispermission = Ispermission }) > 0;
            if (!string.IsNullOrEmpty(Ispermission) && string.IsNullOrEmpty(IsRoleEdit))
                return Sql_ADO.idasAdoService.ExecuteNoquery("sp_SystemRolesTestTitle_UpdateRoleUser", parameter: new { DepartmentTitleId = DepartmentTitleId, RoleID = RoleID, IsRoleEdit = "", Ispermission = Ispermission }) > 0;
            else if (string.IsNullOrEmpty(Ispermission) && !string.IsNullOrEmpty(IsRoleEdit))
                return Sql_ADO.idasAdoService.ExecuteNoquery("sp_SystemRolesTestTitle_UpdateRoleUser", parameter: new { DepartmentTitleId = DepartmentTitleId, RoleID = RoleID, Ispermission = "", IsRoleEdit = IsRoleEdit }) > 0;
            else
                return Sql_ADO.idasAdoService.ExecuteNoquery("sp_SystemRolesTestTitle_UpdateRoleUser", parameter: new { DepartmentTitleId = DepartmentTitleId, RoleID = RoleID, Ispermission = Ispermission, IsRoleEdit = IsRoleEdit }) > 0;

        }

        public DataTable ViewDataMenu(Guid IdUser)
        {
            return Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_SystemRoles_ViewDataMenu", parameter: new { IdUser = IdUser });
        }
    }
}
