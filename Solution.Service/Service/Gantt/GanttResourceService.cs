﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class GanttResourceService : IGanttResourceService
    {
        public TaskResourceService _taskResourceService;
        public TaskResourceService taskResourceService
        {
            get
            {
                return _taskResourceService = _taskResourceService ?? new TaskResourceService();
            }
        }

        public IEnumerable<GanttResourceBO> GetByTask(Guid taskID)
        {
            var data = taskResourceService.GetByTask(taskID)
                .Select(i => new GanttResourceBO
                {
                    Id = i.Id,
                    Name = i.TaskName,
                    PercentDone = i.CompleteRate ?? 0,
                    ResourceImage = i.Employee.AvatarUrl,
                    ResourceName = i.Employee.Name,
                    Status = i.TaskStatusText,
                    BaselineStartDate = i.StartAt,
                    BaselineEndDate = i.EndAt,
                    StartDate = i.PerformAt,
                    EndDate = i.CompletePlanAt
                });
            return data;
        }
    }
}
