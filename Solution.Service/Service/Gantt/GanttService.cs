﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class GanttService : IGanttService
    {
        public TaskService _taskService;
        public TaskService taskService
        {
            get
            {
                return _taskService = _taskService ?? new TaskService();
            }
        }
        public TaskAssociateService _taskAssociateService;
        public TaskAssociateService taskAssociateService
        {
            get
            {
                return _taskAssociateService = _taskAssociateService ?? new TaskAssociateService();
            }
        }
        //public TaskStageService _taskStageService;
        //public TaskStageService taskStageService
        //{
        //    get
        //    {
        //        return _taskStageService = _taskStageService ?? new TaskStageService();
        //    }
        //}
        public GanttBO GetByTaskID(Guid taskID)
        {
            /// Tạo object result 
            var gantt = new GanttBO();
            /// get task by id
            var task = new TaskService().GetById(taskID);

            /// Map task với 
            var ganttTask = MapGanttTask(task);

            var ganttTaskID = new List<Guid>();

            ganttTask.children = AddChildrenGantt(ganttTask.ID, ref ganttTaskID);
            gantt.tasks = new GanttTask() { rows = new List<GanttTaskBO>() { ganttTask } };
            var taskAssociate = taskAssociateService.GetByTaskIDs(ganttTaskID).ToList();
            var ganttDependency = MapGanttAssociate(taskAssociate);
            gantt.dependencies = new GanttDependency() { rows = ganttDependency };

            return gantt;
        }
        private List<GanttTaskBO> AddChildrenGantt(Guid ID, ref List<Guid> childrenID)
        {
            var children = new List<GanttTaskBO>();
            if (ID != null)
            {
                var taskChildren = taskService.Get(u => u.ParentId == ID).ToList();
                if (taskChildren != null && taskChildren.Count > 0)
                {
                    foreach (var taskChild in taskChildren)
                    {
                        var child = MapGanttTask(taskChild);
                        if (taskService.Get(u => u.ParentId == taskChild.Id).Count() > 0)
                        {
                            child.children = AddChildrenGantt(child.ID, ref childrenID);
                        }
                        childrenID.Add(child.ID);
                        children.Add(child);
                    }
                }
            }
            return children;
        }

        private GanttDependencyBO MapGanttAssociate(TaskAssociateBO associate)
        {
            var dependencyBO = new GanttDependencyBO();
            dependencyBO.From = associate.TaskId;
            dependencyBO.To = associate.TaskAssociateId;
            dependencyBO.Id = associate.Id;
            dependencyBO.Type = associate.Type;
            return dependencyBO;
        }
        private List<GanttDependencyBO> MapGanttAssociate(List<TaskAssociateBO> associate)
        {
            var dependencies = new List<GanttDependencyBO>();
            foreach (var item in associate)
            {
                var denpendency = MapGanttAssociate(item);
                dependencies.Add(denpendency);
            }
            return dependencies;
        }

        private GanttTaskBO MapGanttTask(TaskBO taskBO)
        {
            return new GanttTaskBO()
            {
                ID = taskBO.Id,
                Name = taskBO.Name,
                HumanEmployeeID = taskBO.EmployeeId,
                IsComplete = taskBO.IsFinish,
                IsPass = taskBO.IsCancel,
                IsPause = taskBO.IsPause,
                PercentDone = taskBO.CompleteRate,
                ParentID = taskBO.ParentId,
                BaselineStartDate = taskBO.StartAt,
                BaselineEndDate = taskBO.EndAt,
                StartDate = taskBO.StartAt,
                EndDate = taskBO.EndAt,
                //Segments = MapGanttTaskSegments(new TaskStageService().ListByTaskID(taskBO.Id).ToList())
            };
        }
        private List<GanttTaskBO> MapGanttTask(List<TaskBO> taskBO)
        {
            var data = new List<GanttTaskBO>();
            foreach (var item in taskBO)
            {
                data.Add(MapGanttTask(item));
            }
            return data;
        }

        public GanttBO GetAll()
        {
            throw new NotImplementedException();
        }

        public GanttBO GetByUserID(Guid userID)
        {
            throw new NotImplementedException();
        }
    }
}
