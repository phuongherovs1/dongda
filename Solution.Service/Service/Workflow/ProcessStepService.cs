﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class ISOProcessStepService : BaseService<ISOProcessStepDTO, ISOProcessStepBO>, IISOProcessStepService
    {
        public void Save(ISOProcessBO data, bool allowSave = true)
        {
            // Xóa ProcessStep theo ProcessId
            var processStep = ServiceFactory<ISOProcessStepService>().GetQuery().Where(p => p.ProcessId == data.Id).Select(p => p.Id);
            if (processStep != null && processStep.Count() > 0)
            {
                // Xóa Relationship theo ProcessStepId
                foreach (var step in processStep)
                {
                    var relationship = ServiceFactory<ISOProcessRelationshipService>().GetQuery().Where(p => p.StepId == step);
                    var ids = relationship.Select(p => p.Id);
                    if (relationship != null && relationship.Count() > 0)
                        ServiceFactory<ISOProcessRelationshipService>().RemoveRange(ids, false);
                }
                ServiceFactory<ISOProcessStepService>().RemoveRange(processStep, false);
            }
            // Xóa ISOProcessStepDependencies theo ProcessId
            var stepDepandency = ServiceFactory<ISOProcessStepDependencyService>().GetQuery().Where(p => p.ProcessId == data.Id);
            if (stepDepandency != null && stepDepandency.Count() > 0)
                ServiceFactory<ISOProcessStepDependencyService>().RemoveRange(stepDepandency.Select(p => p.Id), false);

            var processGraphic = new ISOProcessGraphicBO()
            {
                Id = new Guid(),
                ProcessId = data.Id,
                GraphData = data.GraphData
            };
            ServiceFactory<ISOProcessGraphicService>().Insert(processGraphic, false);
            var list = data.Cells;
            var processStepId = Guid.Empty;
            List<ProcessStepDetail> lstDetail = new List<ProcessStepDetail>();
            foreach (var item in list)
            {
                if (item.Type == "org.Member")
                {
                    var ProcessSteps = new ISOProcessStepBO()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        ProcessId = data.Id
                    };
                    processStepId = ServiceFactory<ISOProcessStepService>().Insert(ProcessSteps, false);
                    // Lưu nhân sự hoặc chức danh theo từng bước
                    var relationship = new ISOProcessRelationshipBO();
                    relationship.StepId = processStepId;
                    if (item.Responsibilities != null && item.Responsibilities.Count() > 0)
                    {
                        foreach (var responsibility in item.Responsibilities)
                        {
                            if (responsibility.ObjectId.Contains('_'))
                            {
                                var arr = responsibility.ObjectId.Split('_').ToList();
                                if (arr != null && arr.Count >= 2)
                                {
                                    if (arr[0] == "Title")
                                    {
                                        relationship.TitleId = arr[1].ToGuid();
                                        relationship.EmployeeId = null;
                                    }
                                    if (arr[0] == "Employee")
                                    {
                                        relationship.EmployeeId = arr[1].ToGuid();
                                        relationship.TitleId = null;
                                    }
                                }
                            }
                            ServiceFactory<ISOProcessRelationshipService>().Insert(relationship, false);
                        }
                    }

                    ProcessStepDetail detail = new ProcessStepDetail()
                    {
                        ProcessStepId = processStepId,
                        ImageId = item.Id
                    };
                    lstDetail.Add(detail);
                }
                else if (item.Type == "app.Link")
                {
                    if (item.Source != null && item.Target != null)
                    {
                        //var source = lstDetail.Where(p => p.ImageId == item.Source).FirstOrDefault();
                        //var target = lstDetail.Where(p => p.ImageId == item.Target).FirstOrDefault();
                        var processStepDependency = new ISOProcessStepDependencyBO()
                        {
                            ProcessId = data.Id,
                            Name = item.Name,
                            Source = item.Source,
                            Target = item.Target
                        };
                        ServiceFactory<ISOProcessStepDependencyService>().Insert(processStepDependency, false);
                    }
                }
            }
            SaveTransaction(allowSave);
        }

    }
}
