﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class ISOProcessService : BaseService<ISOProcessDTO, ISOProcessBO>, IISOProcessService
    {
        public IEnumerable<ISOProcessBO> GetByDepartment(Guid? departmentId)
        {
            var data = Get(i => !departmentId.HasValue || i.DepartmentId == departmentId);
            return data;
        }

        public ISOProcessBO GetFormItem(string id)
        {
            ISOProcessBO item;
            item = string.IsNullOrEmpty(id) ? new ISOProcessBO() : GetById(id.ToGuid());
            return item;
        }


        public ISOProcessBO GetGraphData(Guid id)
        {
            var data = GetById(id);
            data.Relationship = new List<ISOProcessRelationshipBO>();
            var GetGraphData = ServiceFactory<ISOProcessGraphicService>().Get(i => i.ProcessId == id).OrderByDescending(i => i.CreateAt).FirstOrDefault();
            if (GetGraphData != null)
            {
                data.GraphData = GetGraphData.GraphData;
            }
            var StepIds = ServiceFactory<ISOProcessStepService>().Get(i => i.ProcessId == id).Select(i => i.Id);
            var GetRelation = ServiceFactory<ISOProcessRelationshipService>().GetQuery().Where(i => StepIds.Contains(i.StepId.Value)).ToList();
            GetRelation = GetRelation.Select(i =>
            {
                var employee = ServiceFactory<EmployeeService>().GetById(i.EmployeeId.HasValue ? i.EmployeeId.Value : Guid.Empty);
                var Title = ServiceFactory<DepartmentTitleService>().GetById(i.TitleId.HasValue ? i.TitleId.Value : Guid.Empty);
                if (i.TitleId.HasValue)
                {
                    i.ObjectName = Title.Name;
                    i.ObjectId = Title.Id;
                }
                if (i.EmployeeId.HasValue)
                {
                    i.ObjectName = employee.Name;
                    i.ObjectId = employee.Id;
                }
                return i;
            }).ToList();
            foreach (var step in StepIds)
            {
                if (step != null && step != Guid.Empty)
                {
                    data.lstStepId = new List<Guid>();
                    data.lstStepId.Add(step);
                }
            }
            foreach (var item in GetRelation)
            {
                var Relationship = new ISOProcessRelationshipBO();
                Relationship.Id = item.ObjectId.Value;
                Relationship.ObjectName = item.ObjectName;
                Relationship.StepId = item.StepId;
                Relationship.ObjectId = item.ObjectId;
                data.Relationship.Add(Relationship);
            }
            return data;

        }
    }
}
