﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class ISOProcessRelationshipService : BaseService<ISOProcessRelationshipDTO, ISOProcessRelationshipBO>, IISOProcessRelationshipService
    {
        public IEnumerable<ISOProcessRelationshipBO> GetDataRelationship(int pageIndex, int pageSize, out int count, Guid StepId)
        {
            var dataQuery = base.GetQuery()
              .Where(i => i.StepId == StepId)
              .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            results = results.Select(i =>
            {
                var employee = ServiceFactory<EmployeeService>().GetById(i.EmployeeId.HasValue ? i.EmployeeId.Value : Guid.Empty);
                var Title = ServiceFactory<DepartmentTitleService>().GetById(i.TitleId.HasValue ? i.TitleId.Value : Guid.Empty);
                if (i.TitleId.HasValue)
                {
                    i.ObjectName = Title.Name;
                    i.ObjectId = Title.Id;
                }
                if (i.EmployeeId.HasValue)
                {
                    i.ObjectName = employee.Name;
                    i.ObjectId = employee.Id;
                }
                return i;
            }).ToList();
            return results;
        }
    }
}
