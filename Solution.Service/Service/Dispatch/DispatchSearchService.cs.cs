﻿using iDAS.ADO;
using iDAS.Service.API.Dispatch;
using System;
using System.Data;

namespace iDAS.Service.Service.Dispatch
{
    public class DispatchSearchService : IDispatchSearchService
    {
        public DataTable GetData(string numberDispatch, string name, DateTime sentDateFrom, DateTime sentDateTo, string content, Guid? securityID, Guid? urgencyID, bool? isGo, int status, int pageIndex, int pageSize, out int count)
        {
            count = 0;
            DataSet ds = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_Dispatchs_Search", parameter: new
            {
                NumberDispatch = numberDispatch,
                Name = name,
                SentdateFrom = sentDateFrom,
                SentdateTo = sentDateTo,
                Content = content,
                SecurityID = securityID,
                UrgencyID = urgencyID,
                IsGo = isGo,
                Status = status,
                PageIndex = pageIndex,
                PageSize = pageSize
            });

            if (ds.Tables == null)
                return null;
            else if (ds.Tables.Count < 2)
                return null;

            count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
            return ds.Tables[0];
        }

    }
}
