﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DispatchService : BaseService<DispatchDTO, DispatchBO>, IDispatchService
    {
        public int CountByCategory(Guid categoryId)
        {
            var result = Get(i => i.CategoryId == categoryId).Count();
            return result;
        }

        public DispatchBO CreateDefault(Guid CategoryId, Guid departmentId)
        {
            var data = new DispatchBO() { };
            try
            {
                var category = ServiceFactory<DispatchCategoryService>().GetById(CategoryId);
                data.CategoryId = category.Id;
                data.CategoryName = category.Name;
                var Department = ServiceFactory<DepartmentService>().GetById(departmentId);
                data.DepartmentId = departmentId;
                data.DepartmentName = Department.Name;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return data;
        }


        public DispatchBO GetDataById(Guid Id)
        {
            var data = base.GetById(Id);

            try
            {
                var category = ServiceFactory<DispatchCategoryService>().GetById(data.CategoryId.Value);
                data.CategoryName = category.Name;
                var Department = ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value);
                data.DepartmentName = Department.Name;
                var temp = ServiceFactory<DispatchAttachmentService>().GetFileIds(data.Id);
                data.FileAttachs = new FileUploadBO()
                {
                    Files = temp.ToList()
                };
                var source = ServiceFactory<DispatchAttachmentService>().GetFileSources(data.Id);
                data.FileSource = new FileUploadBO()
                {
                    Files = source.ToList()
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return data;
        }

        public DispatchBO GetDataByIdCheckReview(Guid Id)
        {
            var data = base.GetById(Id);

            try
            {
                var responsibility = ServiceFactory<DispatchResponsibilityService>().GetQuery().Where(p => p.DispatchId == data.Id && p.RoleType == (int)Resource.DispatchResponsibility.Review).Select(p => p.EmployeeId.Value);
                if (!responsibility.Contains(UserId))
                    throw new AccessDenyException();
                var category = ServiceFactory<DispatchCategoryService>().GetById(data.CategoryId.Value);
                data.CategoryName = category.Name;
                var Department = ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value);
                data.DepartmentName = Department.Name;
                var temp = ServiceFactory<DispatchAttachmentService>().GetFileIds(data.Id);
                data.FileAttachs = new FileUploadBO()
                {
                    Files = temp.ToList()
                };

            }
            catch (Exception)
            {
                throw;
            }
            return data;
        }



        public DispatchBO GetDataByIdCheckAproval(Guid Id)
        {
            var data = base.GetById(Id);

            try
            {
                var responsibility = ServiceFactory<DispatchResponsibilityService>().GetQuery().Where(p => p.DispatchId == data.Id && p.RoleType == (int)Resource.DispatchResponsibility.Approval).Select(p => p.EmployeeId.Value);
                if (!responsibility.Contains(UserId))
                    throw new AccessDenyException();
                var category = ServiceFactory<DispatchCategoryService>().GetById(data.CategoryId.Value);
                data.CategoryName = category.Name;
                var Department = ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value);
                data.DepartmentName = Department.Name;
                var temp = ServiceFactory<DispatchAttachmentService>().GetFileIds(data.Id);
                data.FileAttachs = new FileUploadBO()
                {
                    Files = temp.ToList()
                };

            }
            catch (Exception)
            {
                throw;
            }
            return data;
        }


        public Guid InsertDispatch(DispatchBO data, string jsonData, bool allowSave = true)
        {
            var fileIds = Enumerable.Empty<Guid>();
            //1. Lưu file đính kèm
            if (data != null && data.FileAttachs != null && data.FileAttachs.FileAttachments.Any())
            {
                if (data != null && data.FileAttachs != null)
                {
                    fileIds = ServiceFactory<FileService>().UploadRange(data.FileAttachs, false);
                }
            }
            // Lưu file nguồn phát sinh của công văn
            var fileSources = Enumerable.Empty<Guid>();
            if (data != null && data.FileSource != null && data.FileSource.FileAttachments.Any())
            {
                if (data != null && data.FileSource != null)
                {
                    fileSources = ServiceFactory<FileService>().UploadRange(data.FileSource, false);
                }
            }
            data.Status = (int)iDAS.Service.Common.Resource.DispatchStatus.WaitReview;
            data.Id = base.Insert(data, false);

            var responsibility = new DispatchResponsibilitieBO();
            responsibility.EmployeeId = UserId;
            responsibility.RoleType = (int)Resource.TargetResponsibility.Create;
            responsibility.DispatchId = data.Id;
            ServiceFactory<DispatchResponsibilityService>().Insert(responsibility, false);
            ServiceFactory<DispatchResponsibilityService>().Create(data.Id, jsonData, false);
            // File đính kèm
            if (fileIds != null && fileIds.Count() > 0)
            {
                foreach (var fileId in fileIds)
                {
                    var attachment = new DispatchAttachmentBO()
                    {
                        FileId = fileId,
                        DispatchId = data.Id
                    };
                    ServiceFactory<DispatchAttachmentService>().Insert(attachment, false);
                }
            }
            // File nguồn phát sinh
            if (fileSources != null && fileSources.Count() > 0)
            {
                foreach (var fileSource in fileSources)
                {
                    var source = new DispatchAttachmentBO()
                    {
                        FileId = fileSource,
                        DispatchId = data.Id,
                        IsSource = true
                    };
                    ServiceFactory<DispatchAttachmentService>().Insert(source, false);
                }
            }
            SaveTransaction(allowSave);
            return data.Id;
        }

        public void UpdateDispatch(DispatchBO data, string jsonData, bool allowSave = true)
        {
            var check = base.GetById(data.Id);
            if (check.CreateBy != UserId)
                throw new AccessDenyException();
            var fileIds = Enumerable.Empty<Guid>();
            if (data != null && data.FileAttachs != null && data.FileAttachs.FileAttachments.Any())
            {
                if (data != null && data.FileAttachs != null)
                {
                    fileIds = ServiceFactory<FileService>().UploadRange(data.FileAttachs, false);
                }
            }
            var fileSources = Enumerable.Empty<Guid>();
            if (data != null && data.FileSource != null && data.FileSource.FileAttachments.Any())
            {
                if (data != null && data.FileSource != null)
                {
                    fileSources = ServiceFactory<FileService>().UploadRange(data.FileSource, false);
                }
            }
            base.Update(data, false);
            ServiceFactory<DispatchResponsibilityService>().Create(data.Id, jsonData, false);
            if (data.FileAttachs != null && data.FileAttachs.FileRemoves != null && data.FileAttachs.FileRemoves.Count > 0)
                ServiceFactory<DispatchAttachmentService>().DeleteRange(data.Id, data.FileAttachs.FileRemoves);
            if (data.FileSource != null && data.FileSource.FileRemoves != null && data.FileSource.FileRemoves.Count > 0)
                ServiceFactory<DispatchAttachmentService>().DeleteRange(data.Id, data.FileSource.FileRemoves);
            foreach (var fileId in fileIds)
            {
                var attachment = new DispatchAttachmentBO()
                {
                    FileId = fileId,
                    DispatchId = data.Id
                };
                ServiceFactory<DispatchAttachmentService>().Insert(attachment, false);
            }
            foreach (var fileSource in fileSources)
            {
                var source = new DispatchAttachmentBO()
                {
                    FileId = fileSource,
                    DispatchId = data.Id,
                    IsSource = true
                };
                ServiceFactory<DispatchAttachmentService>().Insert(source, false);
            }
            SaveTransaction(allowSave);
        }

        public IEnumerable<DispatchBO> GetDataDispatch(int pageIndex, int pageSize, out int count, Guid? CategoryId, string filterName = default(string), Common.Resource.DispatchStatus status = 0)
        {
            var responsibilities = ServiceFactory<DispatchResponsibilityService>().GetQuery();
            //GET Dữ liệu quản lý sự không phù hợp theo từng phòng ban 
            var dataQuery = base.GetQuery()
                .Where(i => (status == Resource.DispatchStatus.All ? true : (i.Status.Value == (int)status)) && (i.CategoryId == CategoryId))
                //.Where(i => i.SecurityId == new Guid("9c0117d2-9971-455f-a6d8-6dfe979e4444") ? (i.CreateBy == UserId || responsibilities.Where(p => p.DispatchId == i.Id).Select(p => p.EmployeeId).Contains(UserId)) : true)
                .Where(i => string.IsNullOrEmpty(filterName) || (!string.IsNullOrEmpty(filterName) && (!string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(filterName.ToLower()))
               )).OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            var securityIds = results.Where(i => i.SecurityId.HasValue).Select(i => i.SecurityId.Value).Distinct();
            var securities = ServiceFactory<DispatchSecurityService>().GetByIds(securityIds).ToList();
            var UrgencyIds = results.Where(i => i.UrgencyId.HasValue).Select(i => i.UrgencyId.Value).Distinct();
            var Urgencys = ServiceFactory<DispatchUrgencyService>().GetByIds(UrgencyIds).ToList();
            //Lấy danh sách người phát hiện sự phát sinh
            results = results.Select(i =>
            {

                i.Security = securities.FirstOrDefault(u => i.SecurityId == u.Id);
                i.Urgency = Urgencys.FirstOrDefault(u => i.UrgencyId == u.Id);
                var memberCount = 0;
                // Lấy số thành phần liên quan
                i.MemberRelation = ServiceFactory<DispatchResponsibilityService>().CountResponsibility(i.Id, out memberCount);
                return i;
            }).ToList();
            return results;
        }

        /// <summary>
        /// Phê duyệt mục tiêu
        /// </summary>
        /// <param name="data"></param>
        public void ReView(DispatchBO data)
        {
            try
            {
                var responsibility = ServiceFactory<DispatchResponsibilityService>().GetQuery().Where(p => p.DispatchId == data.Id && p.RoleType == (int)Resource.DispatchResponsibility.Review).Select(p => p.EmployeeId.Value);
                if (!responsibility.Contains(UserId))
                    throw new AccessDenyException();
                if (data.IsAccept == true)
                {
                    data.Status = (int)Resource.DispatchStatus.WaitApprove;
                    // Cập nhật kế hoạch về trạng thái Duyệt

                }
                else
                {
                    data.Status = (int)Resource.DispatchStatus.NotReview;
                    // Cập nhật trạng thái kế hoạch về trạng thái Không duyệt

                }
                var responsibilityId = ServiceFactory<DispatchResponsibilityService>().GetQuery().Where(p => p.DispatchId == data.Id && p.RoleType == (int)Resource.DispatchResponsibility.Review).Select(p => p.Id).FirstOrDefault();
                var respon = new DispatchResponsibilitieBO
                {

                    Id = responsibilityId,
                    Note = data.Responsibilitie.Note
                };
                ServiceFactory<DispatchResponsibilityService>().Update(respon, false);
                base.Update(data, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Phê duyệt mục tiêu
        /// </summary>
        /// <param name="data"></param>
        public void Approval(DispatchBO data)
        {
            try
            {
                var responsibility = ServiceFactory<DispatchResponsibilityService>().GetQuery().Where(p => p.DispatchId == data.Id && p.RoleType == (int)Resource.DispatchResponsibility.Approval).Select(p => p.EmployeeId.Value);
                if (!responsibility.Contains(UserId))
                    throw new AccessDenyException();
                if (data.IsAccept == true)
                {
                    data.Status = (int)Resource.DispatchStatus.Approve;
                    // Cập nhật kế hoạch về trạng thái Duyệt

                }
                else
                {
                    data.Status = (int)Resource.DispatchStatus.NotApprove;
                    // Cập nhật trạng thái kế hoạch về trạng thái Không duyệt

                }
                var responsibilityId = ServiceFactory<DispatchResponsibilityService>().GetQuery().Where(p => p.DispatchId == data.Id && p.RoleType == (int)Resource.DispatchResponsibility.Approval).Select(p => p.Id).FirstOrDefault();
                var respon = new DispatchResponsibilitieBO
                {

                    Id = responsibilityId,
                    Note = data.Responsibilitie.Note
                };
                ServiceFactory<DispatchResponsibilityService>().Update(respon, false);
                base.Update(data, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}
