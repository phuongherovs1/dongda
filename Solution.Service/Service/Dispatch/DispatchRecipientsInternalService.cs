﻿using iDAS.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class DispatchRecipientsInternalService : BaseService<DispatchRecipientsInternalDTO, DispatchRecipientsInternalBO>, IDispatchRecipientsInternalService
    {
        public IEnumerable<DispatchRecipientsInternalBO> GetDataInternal(int pageIndex, int pageSize, out int count, Guid DispatchId)
        {
            var dataQuery = base.GetQuery()
              .Where(i => i.DispatchId == DispatchId)
              .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            results = results.Select(i =>
            {
                var employee = ServiceFactory<EmployeeService>().GetById(i.EmployeeId.HasValue ? i.EmployeeId.Value : Guid.Empty);
                var Department = ServiceFactory<DepartmentService>().GetById(i.DepartmentId.HasValue ? i.DepartmentId.Value : Guid.Empty);
                if (i.DepartmentId.HasValue)
                {
                    i.TypeName = Department.Name;
                    i.IdRelation = Department.Id;
                }
                if (i.EmployeeId.HasValue)
                {
                    i.TypeName = employee.Name;
                    i.IdRelation = employee.Id;
                }

                return i;
            }).ToList();
            return results;
        }



        public void InsertInternal(DispatchRecipientsInternalBO item, string datajson = "", bool allowSave = true)
        {
            var ids = base.Get(m => m.DispatchId == item.DispatchId).Select(i => i.Id);
            if (ids != null)
            {
                base.DeleteRange(ids, false);
            }
            if (!string.IsNullOrEmpty(datajson))
            {
                var RecipientsInternal = JsonConvert.DeserializeObject<List<DispatchRecipientsInternalBO>>(datajson);

                var details = new List<DispatchRecipientsInternalBO>();
                foreach (var it in RecipientsInternal)
                {
                    it.DispatchId = item.DispatchId;
                    // Insert nhân sự , phòng ban 
                    if (it.TypeId != null)
                    {
                        if (it.TypeId.Contains('_'))
                        {
                            string[] DepartmentIdsl = it.TypeId.Trim().Split('_');
                            if (DepartmentIdsl[0].ToString().Trim().Equals("Employee"))
                            {
                                it.EmployeeId = new Guid(DepartmentIdsl[1]);
                                it.Flag = 1;
                            }



                            if (DepartmentIdsl[0].ToString().Trim().Equals("Department"))
                            {
                                it.DepartmentId = new Guid(DepartmentIdsl[1]);
                                it.Flag = 3;

                            }
                        }
                    }
                    else
                    {
                        if (it.Flag == 1)
                        {
                            it.EmployeeId = it.IdRelation;
                        }


                        if (it.Flag == 3)
                        {
                            it.DepartmentId = it.IdRelation;
                        }
                    }



                    details.Add(it);
                }

                base.InsertRange(details, false);
            }
            SaveTransaction(allowSave);
        }

        public IEnumerable<DispatchBO> GetDataDispatchInternal(int pageIndex, int pageSize, out int count, Guid? DepartmentId)
        {
            var dataDispatchInternal = base.GetQuery().Where(p => p.DepartmentId == DepartmentId || p.EmployeeId == UserId);

            var DispatIds = ServiceFactory<DispatchCategoriToService>().GetQuery()
             .Where(p => dataDispatchInternal.Select(i => i.DispatchId.Value).Contains(p.DispatchId.Value) && p.DepartmentId == DepartmentId);

            var dispatchInternal = dataDispatchInternal.Where(p => !DispatIds.Select(n => n.DispatchId).Contains(p.DispatchId));


            //GET Dữ liệu quản lý sự không phù hợp theo từng phòng ban 
            var dataQuery = ServiceFactory<DispatchService>().GetQuery()
                .Where(p => dispatchInternal.Select(i => i.DispatchId.Value).Contains(p.Id))
                .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = ServiceFactory<DispatchService>().Page(dataQuery, pageIndex, pageSize).ToList();
            var securityIds = results.Where(i => i.SecurityId.HasValue).Select(i => i.SecurityId.Value).Distinct();
            var securities = ServiceFactory<DispatchSecurityService>().GetByIds(securityIds).ToList();
            var UrgencyIds = results.Where(i => i.UrgencyId.HasValue).Select(i => i.UrgencyId.Value).Distinct();
            var Urgencys = ServiceFactory<DispatchUrgencyService>().GetByIds(UrgencyIds).ToList();
            //Lấy danh sách người phát hiện sự phát sinh
            results = results.Select(i =>
            {
                i.Security = securities.FirstOrDefault(u => i.SecurityId == u.Id);
                i.Urgency = Urgencys.FirstOrDefault(u => i.UrgencyId == u.Id);
                var memberCount = 0;
                // Lấy số thành phần liên quan
                i.MemberRelation = ServiceFactory<DispatchResponsibilityService>().CountResponsibility(i.Id, out memberCount);
                return i;
            }).ToList();
            return results;
        }


        /// <summary>
        /// Đếm số công văn đến chưa xác nhận
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        public int CountDispatchInternal(string department)
        {
            if (!string.IsNullOrEmpty(department))
            {
                var departmentId = Guid.Empty;
                if (department.Contains('_'))
                    departmentId = new Guid(department.Split('_').ToList()[1]);
                else
                    departmentId = new Guid(department);
                var dataDispatchInternal = base.GetQuery().Where(p => p.DepartmentId == departmentId || p.EmployeeId == UserId);

                var DispatIds = ServiceFactory<DispatchCategoriToService>().GetQuery()
                 .Where(p => dataDispatchInternal.Select(i => i.DispatchId.Value).Contains(p.DispatchId.Value) && p.DepartmentId == departmentId);

                var dispatchInternal = dataDispatchInternal.Where(p => !DispatIds.Select(n => n.DispatchId).Contains(p.DispatchId));
                //GET Dữ liệu quản lý sự không phù hợp theo từng phòng ban 
                var dataQuery = ServiceFactory<DispatchService>().GetQuery()
                    .Where(p => dispatchInternal.Select(i => i.DispatchId.Value).Contains(p.Id))
                    .OrderByDescending(i => i.CreateAt);
                return dataQuery.Count();
            }
            else
                return 0;
        }
    }
}
