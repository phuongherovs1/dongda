﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DispatchArchiveService : BaseService<DispatchArchiveDTO, DispatchArchiveBO>, IDispatchArchiveService
    {
        /// <summary>
        /// Khởi tạo giá trị mặc định
        /// </summary>
        /// <param name="dispatchId"></param>
        /// <returns></returns>
        public DispatchArchiveBO CreateDefault(string dispatchId, string departmentId)
        {
            var data = new DispatchArchiveBO();
            if (!string.IsNullOrEmpty(dispatchId) && !string.IsNullOrEmpty(departmentId))
            {
                var dispatch = ServiceFactory<DispatchService>().GetQuery().FirstOrDefault(p => p.Id == new Guid(dispatchId));
                var department = ServiceFactory<DepartmentService>().GetQuery().FirstOrDefault(p => p.Id == new Guid(departmentId));
                data = base.GetQuery().FirstOrDefault(p => p.DispatchId == new Guid(dispatchId) && p.DepartmentArchiveId == new Guid(departmentId));
                if (data == null)
                {
                    data = new DispatchArchiveBO()
                    {
                        Id = Guid.Empty,
                        Dispatch = dispatch == null ? new DispatchBO() : dispatch,
                        DispatchId = dispatch == null ? Guid.Empty : dispatch.Id,
                        DepartmentArchiveId = new Guid(departmentId),
                        DepartmentArchiveName = department == null ? string.Empty : department.Name,
                        ArchiveDate = DateTime.Now,
                        DeadlineForArchiving = DateTime.Now,
                        DispatchName = dispatch == null ? string.Empty : dispatch.Name,
                        DispatchCode = dispatch == null ? string.Empty : dispatch.NumberDispatch
                    };
                }
                else
                {
                    data.Dispatch = dispatch == null ? new DispatchBO() : dispatch;
                    var temp = ServiceFactory<DispatchAttachmentService>().GetFileIds(dispatch.Id);
                    data.Dispatch.FileAttachs = new FileUploadBO()
                    {
                        Files = temp.ToList()
                    };
                    data.DispatchName = dispatch == null ? string.Empty : dispatch.Name;
                    data.DispatchCode = dispatch == null ? string.Empty : dispatch.NumberDispatch;
                    data.DepartmentArchiveName = department == null ? string.Empty : department.Name;
                }
            }
            return data;
        }

        /// <summary>
        /// Thêm mới hoặc cập nhật lưu trữ công văn
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid InsertOrUpdate(DispatchArchiveBO data)
        {
            if (data.Id == null || data.Id == Guid.Empty)
            {
                data.EmployeeArchiveId = UserId;
                data.Id = base.Insert(data, true);
                // Cập nhật công văn thành đã lưu trữ
                var dispatch = ServiceFactory<DispatchService>().GetById(data.DispatchId.Value);
                dispatch.Status = (int)Resource.DispatchStatus.Archived;
                ServiceFactory<DispatchService>().Update(dispatch, true);
            }
            else
            {
                base.Update(data, true);
            }
            return data.Id;
        }

        /// <summary>
        /// Danh sách công văn lưu trữ theo phòng ban
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filter"></param>
        /// <param name="archiveType"></param>
        /// <param name="department"></param>
        /// <returns></returns>
        public IEnumerable<DispatchArchiveBO> GetDispatchArchive(int pageIndex, int pageSize, out int count, string filter, Resource.ProfileArchiveTime archiveType, string department)
        {
            var dispatchArchive = Enumerable.Empty<DispatchArchiveBO>();
            // Lấy danh sách công văn lưu trữ theo phòng ban
            dispatchArchive = base.GetQuery();
            if (!string.IsNullOrEmpty(department) && department.Contains("_"))
            {
                var itemDepartment = department.Split('_').ToList();
                if (itemDepartment != null && itemDepartment.Count >= 2)
                {
                    dispatchArchive = dispatchArchive.Where(p => p.DepartmentArchiveId == new Guid(itemDepartment[1]));
                }
            }
            var dispatchs = ServiceFactory<DispatchService>().GetQuery();
            dispatchArchive = dispatchArchive.Where(p => archiveType == Resource.ProfileArchiveTime.All ? true : p.Type.HasValue && p.Type.Value == (int)archiveType)
                .Select(i =>
                {
                    if (i.DispatchId.HasValue)
                    {
                        var dispatch = dispatchs.FirstOrDefault(p => p.Id == i.DispatchId.Value);
                        i.DispatchName = dispatch == null ? string.Empty : dispatch.Name;
                        i.IsGoDispatch = dispatch == null ? null : dispatch.IsGo;
                        i.DispatchCode = dispatch == null ? string.Empty : dispatch.NumberDispatch;
                        i.DispatchStatus = dispatch == null ? string.Empty : dispatch.StatusText;
                    }
                    return i;
                });
            dispatchArchive = dispatchArchive.Where(i => string.IsNullOrEmpty(filter) || (!string.IsNullOrEmpty(filter) && (!string.IsNullOrEmpty(i.DispatchName) && i.DispatchName.ToLower().Contains(filter.ToLower())) ||
                        (!string.IsNullOrEmpty(i.DispatchCode) && i.DispatchCode.ToLower().Contains(filter.ToLower()))));
            count = dispatchArchive.Count();
            var result = Page(dispatchArchive.OrderBy(p => p.CreateAt), pageIndex, pageSize);
            var employeeIds = result.Select(i => i.EmployeeArchiveId).Distinct();
            var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            result = result.Select(i =>
                    {
                        i.ArchiveEmployee = employees.FirstOrDefault(u => u.Id == i.EmployeeArchiveId);
                        return i;
                    }).ToList();
            return result;
        }
        /// <summary>
        /// Thêm công văn đã lưu trữ vào hệ thống
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Guid InsertDispatchArchiveOld(DispatchArchiveBO data)
        {
            try
            {
                var dispatch = new DispatchBO()
                {
                    Name = data.DispatchName,
                    NumberDispatch = data.DispatchCode,
                    DepartmentId = data.Dispatch.DepartmentId,
                    CategoryId = data.Dispatch.CategoryId,
                    Sentdate = data.Dispatch.Sentdate,
                    SecurityId = data.Dispatch.SecurityId,
                    UrgencyId = data.Dispatch.UrgencyId,
                    SendmethodId = data.Dispatch.SendmethodId,
                    Recipients = data.Dispatch.Recipients,
                    Content = data.Dispatch.Content,
                    Note = data.Dispatch.Note,
                    IsGo = data.DispatchForm,
                    FileAttachs = data.Dispatch.FileAttachs
                };
                data.DispatchId = ServiceFactory<DispatchService>().InsertDispatch(dispatch, "");
                data.Id = InsertOrUpdate(data);
                return data.Id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Thêm công văn đã lưu trữ vào hệ thống
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public DispatchArchiveBO CreateDefault(string departmentId)
        {
            var model = new DispatchArchiveBO()
            {
                Id = Guid.Empty,
                Dispatch = new DispatchBO()
            };
            if (!string.IsNullOrEmpty(departmentId))
            {
                if (departmentId.Contains('_'))
                {
                    var department = departmentId.Split('_').ToList();
                    if (department != null && department.Count >= 2)
                    {
                        model.DepartmentArchiveId = new Guid(department[1]);
                        var itemDepartment = ServiceFactory<DepartmentService>().GetById(new Guid(department[1]));
                        model.DepartmentArchiveName = itemDepartment == null ? string.Empty : itemDepartment.Name;
                    }
                }
                else
                {
                    model.DepartmentArchiveId = new Guid(departmentId);
                    var itemDepartment = ServiceFactory<DepartmentService>().GetById(new Guid(departmentId));
                    model.DepartmentArchiveName = itemDepartment == null ? string.Empty : itemDepartment.Name;
                }
            }
            return model;
        }
    }
}
