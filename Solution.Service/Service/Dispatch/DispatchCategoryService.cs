﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;


namespace iDAS.Service
{
    public class DispatchCategoryService : BaseService<DispatchCategoryDTO, DispatchCategorieBO>, IDispatchCategoryService
    {
        public DispatchCategorieBO GetDetail(string id, string departmentId, string categoryId)
        {
            try
            {
                var departmentOfCurrentUser = ServiceFactory<DepartmentService>().GetDepartmentsAndChildrenByEmployeeID(UserId);
                var data = new DispatchCategorieBO();
                if (!String.IsNullOrEmpty(id))
                {
                    data = GetById(new Guid(id));
                    var departmentName = data.DepartmentId.HasValue ? ServiceFactory<DepartmentService>().GetById(data.DepartmentId.Value) : null;
                    data.DepartmentName = departmentName == null ? string.Empty : departmentName.Name;
                }
                else
                {
                    var department = new DepartmentBO();
                    var parentId = string.Empty;
                    if (!String.IsNullOrEmpty(departmentId))
                    {
                        if (departmentId.Contains("_"))
                        {
                            var items = departmentId.Split('_').ToList();
                            if (items != null && items.Count == 2)
                            {
                                if (!departmentOfCurrentUser.Any(p => p.Id == new Guid(items[1])))
                                    throw new AccessDenyException();
                                department = ServiceFactory<DepartmentService>().GetById(new Guid(items[1]));
                            }
                        }
                        else
                        {
                            if (!departmentOfCurrentUser.Any(p => p.Id == new Guid(departmentId)))
                                throw new AccessDenyException();
                            department = ServiceFactory<DepartmentService>().GetById(new Guid(departmentId));
                        }
                    }
                    if (!String.IsNullOrEmpty(categoryId))
                    {
                        parentId = categoryId;
                    }
                    data = new DispatchCategorieBO()
                    {
                        Id = Guid.Empty,
                        ParentId = parentId == string.Empty ? (Guid?)null : new Guid(parentId),
                        DepartmentId = department == null ? (Guid?)null : department.Id,
                        DepartmentName = department == null ? string.Empty : department.Name
                    };
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<DispatchCategorieBO> GetDataCategoryTo(Guid departmentId)
        {

            var dataByDepartment = GetQuery().Where(i => i.DepartmentId == departmentId && i.IsGo == false);
            return dataByDepartment;
        }

        public IEnumerable<DispatchCategorieBO> GetTreeDispatchCategory(Guid? parentId, Guid departmentId)
        {

            //1. Danh sách tài liệu theo phòng ban
            var dataByDepartment = GetQuery().Where(i => i.DepartmentId == departmentId && i.IsGo == true);
            //2. Danh sách danh mục theo danh mục cha
            var data = dataByDepartment.Where(i => parentId.HasValue ? i.ParentId == parentId.Value : !i.ParentId.HasValue);
            var dataIds = data.Select(u => u.Id);
            //3. Danh sách danh mục tài liệu còn lại, dùng để đếm số lượng danh mục con
            var remainCategory = dataByDepartment.Except(data);
            //4. Danh sách tài liệu theo danh mục
            var result = data.AsEnumerable()
                .Select(i =>
                {
                    i.IsParent = remainCategory.Any(t => t.ParentId == i.Id);
                    i.CountProfile = ServiceFactory<DispatchService>().CountByCategory(i.Id);
                    return i;
                });
            return result;
        }


        public IEnumerable<DispatchCategorieBO> GetTreeDispatchCategoryTo(Guid? parentId, Guid departmentId)
        {

            //1. Danh sách tài liệu theo phòng ban
            var dataByDepartment = GetQuery().Where(i => i.DepartmentId == departmentId && i.IsGo == false);
            //2. Danh sách danh mục theo danh mục cha
            var data = dataByDepartment.Where(i => parentId.HasValue ? i.ParentId == parentId.Value : !i.ParentId.HasValue);
            var dataIds = data.Select(u => u.Id);
            //3. Danh sách danh mục tài liệu còn lại, dùng để đếm số lượng danh mục con
            var remainCategory = dataByDepartment.Except(data);
            //4. Danh sách tài liệu theo danh mục
            var result = data.AsEnumerable()
                .Select(i =>
                {
                    i.IsParent = remainCategory.Any(t => t.ParentId == i.Id);
                    i.CountProfile = ServiceFactory<DispatchService>().CountByCategory(i.Id);
                    i.CountProfileTo = ServiceFactory<DispatchCategoriToService>().CountByCategory(i.Id);
                    return i;
                });
            return result;
        }

        /// <returns></returns>
        public IEnumerable<DispatchCategorieBO> GetByDepartment(string departmentId, string categoryId = default(string))
        {
            try
            {
                var data = Enumerable.Empty<DispatchCategorieBO>();
                if (!String.IsNullOrEmpty(departmentId))
                {
                    if (departmentId.Contains("_"))
                    {
                        var items = departmentId.Split('_').ToList();
                        if (items != null && items.Count == 2)
                            data = base.GetQuery().Where(i => i.DepartmentId == new Guid(items[1])).OrderBy(p => p.Name);
                    }
                    else
                        data = base.GetQuery().Where(i => i.DepartmentId == new Guid(departmentId)).OrderBy(p => p.Name);
                }
                if (!String.IsNullOrEmpty(categoryId) && new Guid(categoryId) != Guid.Empty)
                    data = data.Where(p => p.Id != new Guid(categoryId));
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteCategory(Guid id)
        {
            try
            {
                var category = base.GetById(id);
                var childCategory = base.GetQuery().Where(p => p.ParentId == id).AsEnumerable();
                var countProfile = ServiceFactory<DispatchService>().CountByCategory(id);
                if (category.CreateBy != UserId)
                    throw new AccessDenyException();
                if (countProfile > 0 || childCategory.Count() > 0)
                    throw new ProfileCategoryExitsProfile();
                else
                    base.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Lấy nhóm công văn đến hoặc đi theo phòng ban
        /// </summary>
        /// <param name="department"></param>
        /// <param name="dispatchForm"></param>
        /// <returns></returns>
        public IEnumerable<DispatchCategorieBO> GetCategoryToOrFormByDepartment(string department, bool dispatchForm)
        {
            var data = Enumerable.Empty<DispatchCategorieBO>();
            if (!string.IsNullOrEmpty(department))
            {
                var departmentId = Guid.Empty;
                if (department.Contains('_'))
                {
                    departmentId = new Guid(department.Split('_').ToList()[1]);
                }
                departmentId = new Guid(department);
                data = base.GetQuery().Where(p => p.DepartmentId == departmentId && p.IsGo == dispatchForm);
            }
            return data;
        }
    }
}
