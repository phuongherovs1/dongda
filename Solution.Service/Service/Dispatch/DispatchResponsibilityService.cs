﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class DispatchResponsibilityService : BaseService<DispatchResponsibilityDTO, DispatchResponsibilitieBO>, IDispatchResponsibilityService
    {

        public IEnumerable<Guid> Create(Guid DispatchId, string jsonData = "", bool allowSave = true)
        {
            try
            {
                var result = Enumerable.Empty<Guid>();
                if (!String.IsNullOrEmpty(jsonData))
                {
                    var data = JsonConvert.DeserializeObject<List<DispatchResponsibilitieBO>>(jsonData);
                    var createResponsibilities = ServiceFactory<DispatchResponsibilityService>().GetQuery().Where(p => p.DispatchId == DispatchId &&
                                                    p.RoleType == (int)Resource.DispatchResponsibility.Create && p.IsDelete != true);
                    if (data != null && data.Count() > 0)
                    {
                        var ids = ServiceFactory<DispatchResponsibilityService>().Get(p => p.DispatchId == DispatchId &&
                                        (p.RoleType == (int)Resource.DispatchResponsibility.Approval ||
                                         p.RoleType == (int)Resource.DispatchResponsibility.Review)).Select(p => p.Id);
                        if (ids != null && ids.Count() > 0)
                            base.DeleteRange(ids, true);
                        var datas = new List<DispatchResponsibilitieBO>();
                        foreach (var item in data)
                        {
                            item.DispatchId = DispatchId;
                            if (createResponsibilities != null && createResponsibilities.Count() > 0)
                            {
                                if (item.RoleType != (int)Resource.DispatchResponsibility.Create)
                                    datas.Add(item);
                            }
                            else
                                datas.Add(item);

                        }
                        result = InsertRange(datas, false);
                    }
                    SaveTransaction(allowSave);
                }
                return result;
            }
            catch
            {
                throw;
            }
        }

        public string CountResponsibility(Guid DispatchId, out int count)
        {
            count = GetQuery().Where(i => i.DispatchId == DispatchId).Count();
            return count == 0 ? "Không có" : count.ToString();
        }

        public IEnumerable<DispatchResponsibilitieBO> GetByDispatch(int pageIndex, int pageSize, out int count, string DispatchId)
        {
            try
            {

                var data = Enumerable.Empty<DispatchResponsibilitieBO>();
                var employees = ServiceFactory<EmployeeService>().GetQuery();
                if (!String.IsNullOrEmpty(DispatchId) && new Guid(DispatchId) != Guid.Empty)
                {
                    data = base.GetQuery().Where(p => p.DispatchId == new Guid(DispatchId) && p.IsDelete != true);
                    data = Page(data, pageIndex, pageSize)
                    .Select(i =>
                    {
                        var employee = employees.FirstOrDefault(p => p.Id == i.EmployeeId.Value);
                        i.EmployeeName = employee == null ? string.Empty : employee.Name;
                        return i;
                    });
                }
                count = data.Count();
                return data;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
