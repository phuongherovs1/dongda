﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DispatchCategoriToService : BaseService<DispatchCategoriToDTO, DispatchCategoriToBO>, IDispatchCategoriToService
    {
        public IEnumerable<DispatchBO> GetDataDispatchTo(int pageIndex, int pageSize, out int count, Guid? CategoryId, string filterName = default(string), Common.Resource.DispatchStatus status = 0)
        {
            var dataDispatchIds = base.GetQuery().Where(p => p.CategoryId == CategoryId).Select(p => p.DispatchId.Value);
            //GET Dữ liệu quản lý sự không phù hợp theo từng phòng ban 
            var dataQuery = ServiceFactory<DispatchService>().GetQuery()
                .Where(p => dataDispatchIds.Contains(p.Id) || p.CategoryId == CategoryId)
                .Where(i => (status == Resource.DispatchStatus.All ? true : (i.Status.Value == (int)status)))
                .Where(i => string.IsNullOrEmpty(filterName) || (!string.IsNullOrEmpty(filterName) && (!string.IsNullOrEmpty(i.Name) && i.Name.ToLower().Contains(filterName.ToLower()))
               ))
                .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = ServiceFactory<DispatchService>().Page(dataQuery, pageIndex, pageSize).ToList();
            var securityIds = results.Where(i => i.SecurityId.HasValue).Select(i => i.SecurityId.Value).Distinct();
            var securities = ServiceFactory<DispatchSecurityService>().GetByIds(securityIds).ToList();
            var UrgencyIds = results.Where(i => i.UrgencyId.HasValue).Select(i => i.UrgencyId.Value).Distinct();
            var Urgencys = ServiceFactory<DispatchUrgencyService>().GetByIds(UrgencyIds).ToList();
            //Lấy danh sách người phát hiện sự phát sinh
            results = results.Select(i =>
            {
                i.Security = securities.FirstOrDefault(u => i.SecurityId == u.Id);
                i.Urgency = Urgencys.FirstOrDefault(u => i.UrgencyId == u.Id);
                var memberCount = 0;
                // Lấy số thành phần liên quan
                i.MemberRelation = ServiceFactory<DispatchResponsibilityService>().CountResponsibility(i.Id, out memberCount);
                return i;
            }).ToList();
            return results;
        }

        public int CountByCategory(Guid categoryId)
        {
            var result = Get(i => i.CategoryId == categoryId).Count();
            return result;
        }
    }
}
