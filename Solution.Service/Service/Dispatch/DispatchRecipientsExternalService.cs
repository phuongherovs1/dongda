﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DispatchRecipientsExternalService : BaseService<DispatchRecipientsExternalDTO, DispatchRecipientsExternalBO>, IDispatchRecipientsExternalService
    {

        public IEnumerable<DispatchRecipientsExternalBO> GetDataExternal(int pageIndex, int pageSize, out int count, Guid DispatchId)
        {
            var dataQuery = base.GetQuery()
              .Where(i => i.DispatchId == DispatchId)
              .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            return results;
        }
    }
}
