﻿using iDAS.DataAccess;
using System;
using System.Linq;

namespace iDAS.Service
{
    public class DispatchTaskService : BaseService<DispatchTaskDTO, DispatchTaskBO>, IDispatchTaskService
    {
        /// <summary>
        /// Xóa công việc
        /// </summary>
        /// <param name="id"></param>
        public void DeleteTask(Guid id)
        {
            try
            {
                ServiceFactory<TaskService>().Delete(id, false);
                var TaskIds = base.GetQuery().Where(p => p.TaskId == id && p.IsDelete != true).Select(p => p.Id);
                base.DeleteRange(TaskIds, false);
                SaveTransaction(true);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
