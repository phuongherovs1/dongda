﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class DispatchAttachmentService : BaseService<DispatchAttachmentDTO, DispatchAttachmentBO>, IDispatchAttachmentService
    {
        public void DeleteRange(Guid DispatchId, IEnumerable<Guid> fileIds)
        {
            var ids = Get(i => i.DispatchId == DispatchId && fileIds.Contains(i.FileId.Value)).Select(i => i.Id);
            base.DeleteRange(ids);
        }

        public IEnumerable<Guid> GetFileIds(Guid DispatchId)
        {
            return base.Get(t => t.DispatchId == DispatchId && t.IsSource != true).Select(t => t.FileId.HasValue ? t.FileId.Value : Guid.Empty);
        }

        public IEnumerable<Guid> GetFileSources(Guid DispatchId)
        {
            return base.Get(t => t.DispatchId == DispatchId && t.IsSource == true).Select(t => t.FileId.HasValue ? t.FileId.Value : Guid.Empty);
        }
    }
}
