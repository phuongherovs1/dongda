﻿using iDAS.ADO;
using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace iDAS.Service
{
    public class TaskScheduleService : BaseService<TaskScheduleDTO, TaskScheduleBO>, ITaskScheduleService
    {
        /// <summary>
        /// Lấy danh sách lịch công tác liên quan nhân sự
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public IEnumerable<TaskScheduleBO> GetListScheduleByEmployee(Guid employeeId, int pageIndex, int pageSize, out int count)
        {
            var queryPerform = ServiceFactory<TaskSchedulePerformService>().GetQuery();
            var query = base.GetQuery().Where(i => i.EmployeeId == employeeId || i.CreateBy == employeeId ||
                        queryPerform.Any(z => z.EmployeeId == employeeId && z.ScheduleId == i.Id && z.IsSent == true));
            count = query.Count();
            var source = base.Page(query.OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            var data = getScheduleInfo(source);
            return data;
        }
        private IEnumerable<TaskScheduleBO> getScheduleInfo(IQueryable<TaskScheduleBO> query)
        {
            var employeeIds = query.Select(i => i.EmployeeId.Value);
            var employees = ServiceFactory<EmployeeService>().GetByIds(employeeIds);
            var scheduleIds = query.Select(i => i.Id);
            var performs = ServiceFactory<TaskSchedulePerformService>().GetListPerform(scheduleIds, UserId);
            var data = query.ToList();
            foreach (var item in data)
            {
                var employee = employees.FirstOrDefault(i => i.Id == item.EmployeeId);
                var perform = performs.FirstOrDefault(i => i.ScheduleId == item.Id);
                if (perform != null)
                {
                    item.SchedulePerformId = perform.Id;
                    item.NotifyToPerform = perform.Notify;
                }
                if (employee != null)
                    item.Employee = employee;
                item.UserId = UserId;
            }
            return data;
        }
        /// <summary>
        /// Lấy danh sách lịch công tác liên quan đến nhân sự theo tuần
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public IEnumerable<DayBO> GetWeek(Guid? employeeId, DateTime? date)
        {
            date = date ?? DateTime.Today;
            employeeId = employeeId ?? base.UserId;
            var week = Resource.Calendar.GetWeek(date.Value);
            var start = week.First().Day.AddDays(-1) + new TimeSpan(23, 59, 59);
            var end = week.Last().Day + new TimeSpan(23, 59, 59);
            var queryPerform = ServiceFactory<TaskSchedulePerformService>().GetQuery().Where(i => i.EmployeeId == employeeId && i.IsSent == true && i.IsCancel != true && i.IsPause != true && i.IsAccept == true);
            var query = base.GetQuery().Where(i => queryPerform.Any(z => z.ScheduleId == i.Id) && i.IsCancel != true && i.IsPause != true);
            var data = query.Where(i => i.StartAt <= end && i.EndAt >= start).ToList();
            foreach (var day in week)
            {
                var items = data.Where(i => ((i.StartAt.HasValue && i.StartAt.Value.Date <= day.Day.Date) && (i.EndAt.HasValue && i.EndAt.Value.Date >= day.Day.Date))
                                            || (i.StartAt.Value.Date == day.Day.Date) || (i.EndAt.Value.Date == day.Day.Date)
                                        )
                            .Select(i => new DayItemBO
                            {
                                Id = i.Id,
                                Content = i.Name,
                                Day = day.Day,
                                Start = i.StartAt.Value,
                                End = i.EndAt.Value
                            }).OrderBy(x => x.Start).ToList();
                day.Items.AddRange(items);
            }
            return week;
        }
        public IEnumerable<DayBO> GetWeek_Task(Guid? employeeId, DateTime? date)
        {
            date = date ?? DateTime.Today;
            employeeId = employeeId ?? base.UserId;
            var week = Resource.Calendar.GetWeek(date.Value);
            var start = week.First().Day.AddDays(-1) + new TimeSpan(23, 59, 59);
            var end = week.Last().Day + new TimeSpan(23, 59, 59);

            DataTable queryPerform = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Schedule_GetListData_FromTaskPerform",
                    parameter: new
                    {
                        employeeId = employeeId.HasValue ? employeeId.Value.ToString() : ""
                    }
                );
            //var queryPerform = ServiceFactory<TaskPerformService>().GetQuery().Where(i => i.EmployeeId == employeeId.Value && i.IsCancel != true && i.IsPause != true);
            List<TaskBO> lstTaskPerformBO = new List<TaskBO>();
            if(queryPerform != null && queryPerform.Rows.Count > 0)
            {
                foreach(DataRow item in queryPerform.Rows)
                {
                    TaskBO taskPerformBO = new TaskBO();
                    taskPerformBO.Id = item["Id"] != null ? new Guid(item["Id"].ToString()) : Guid.Empty;
                    taskPerformBO.Content = item["Content"] != null? item["Content"].ToString(): "";
                    taskPerformBO.Name = item["Name"] != null? item["Name"].ToString(): "";
                    if(item["StartAt"] != null)
                        taskPerformBO.StartAt = DateTime.Parse(item["StartAt"].ToString());
                    if (item["EndAt"] != null)
                        taskPerformBO.EndAt = DateTime.Parse(item["EndAt"].ToString());
                    lstTaskPerformBO.Add(taskPerformBO);
                }
            }
            if (lstTaskPerformBO.Count > 0)
            {
                var data = lstTaskPerformBO.Where(i => i.StartAt <= end && i.EndAt >= start).ToList();
                foreach (var day in week)
                {
                    var items = data.Where(i => ((i.StartAt.HasValue && i.StartAt.Value.Date <= day.Day.Date) && (i.EndAt.HasValue && i.EndAt.Value.Date >= day.Day.Date))
                                                || (i.StartAt.Value.Date == day.Day.Date) || (i.EndAt.Value.Date == day.Day.Date)
                                            )
                                .Select(i => new DayItemBO
                                {
                                    Id = i.Id,
                                    Content = i.Content,
                                    Day = day.Day,
                                    Start = i.StartAt.Value,
                                    End = i.EndAt.Value,
                                    Name = i.Name
                                }).OrderBy(x => x.Start).ToList();
                    day.Items.AddRange(items);
                }
            }
            return week;
        }
        /// <summary>
        /// Cập nhật trạng thái lịch công tác là đã gửi 
        /// </summary>
        /// <param name="id"></param>
        public void SetIsSent(Guid id, bool allowSave = true)
        {
            var item = new TaskScheduleBO()
            {
                Id = id,
                IsSent = true
            };
            base.Update(item, allowSave);
        }
        /// <summary>
        /// Lấy thông tin cho form nhập liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TaskScheduleBO GetFormItem(Guid? id)
        {
            TaskScheduleBO item;
            if (id == null)
            {
                var employee = base.GetUserCurrent();
                item = new TaskScheduleBO() { Employee = employee };
            }
            else
            {
                item = base.GetById(id.Value);
                item.Employee = base.GetEmployee(item.EmployeeId.Value);
            }
            return item;
        }
        /// <summary>
        /// Thêm mới lịch công tác
        /// </summary>
        /// <param name="item"></param>
        public void Create(TaskScheduleBO item)
        {
            if (checkScheduleTimeExist(item))
                throw new ArgumentException(Resource.Task.Exception.ConfigScheduleTimeExist);
            if (!checkTimeValid(item.StartAt, item.EndAt))
                throw new ArgumentException(Resource.Task.Exception.ConfigTimeInvalid);

            if (item.IsClosed == true)
                item.ClosedAt = item.ClosedAt ?? DateTime.Now;
            else
                item.ClosedAt = null;
            base.Insert(item);
        }
        /// <summary>
        /// Cập nhật lịch công tác
        /// </summary>
        /// <param name="item"></param>
        public void Update(TaskScheduleBO item)
        {
            if (checkScheduleTimeExist(item))
                throw new ArgumentException(Resource.Task.Exception.ConfigScheduleTimeExist);
            if (!checkTimeValid(item.StartAt, item.EndAt))
                throw new ArgumentException(Resource.Task.Exception.ConfigTimeInvalid);

            if (item.IsClosed == true)
                item.ClosedAt = item.ClosedAt ?? DateTime.Now;
            else
                item.ClosedAt = DateTime.MinValue;

            ServiceFactory<TaskSchedulePerformService>().SetIsEdit(item.Id);
            base.Update(item);
        }
        /// <summary>
        /// Hủy lịch công tác
        /// </summary>
        /// <param name="item"></param>
        public void Cancel(TaskScheduleBO item)
        {
            if (item.IsCancel == true)
            {
                item.CancelAt = DateTime.Now;
            }
            else
            {
                item.ReasonOfCancel = string.Empty;
                if (item.CancelAt.HasValue)
                    item.CancelAt = DateTime.MinValue;
            }
            base.Update(item);
        }
        /// <summary>
        /// Tạm dừng lịch công tác
        /// </summary>
        /// <param name="item"></param>
        public void Pause(TaskScheduleBO item)
        {
            if (item.IsPause == true)
            {
                item.PauseAt = DateTime.Now;
            }
            else
            {
                item.ReasonOfPause = string.Empty;
                if (item.PauseAt.HasValue)
                    item.PauseAt = DateTime.MinValue;
            }
            base.Update(item);
        }
        private bool checkScheduleTimeExist(TaskScheduleBO item)
        {
            return base.GetQuery().Count(i => i.Id != item.Id && i.Name == item.Name.Trim() && i.StartAt == item.StartAt && i.EndAt == item.EndAt) > 0;
        }
        private bool checkTimeValid(DateTime? startAt, DateTime? endAt)
        {
            if (!startAt.HasValue || !endAt.HasValue)
                return false;
            if (startAt.Value >= endAt.Value)
                return false;
            return true;
        }
    }
}
