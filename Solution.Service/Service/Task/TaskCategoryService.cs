﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class TaskCategoryService : BaseService<TaskCategoryDTO, TaskCategoryBO>, ITaskCategoryService
    {
        public Guid InsertTaskCategory(TaskCategoryBO data)
        {
            throw new NotImplementedException();
        }

        public void UpdateTaskCategory(TaskCategoryBO data)
        {
            throw new NotImplementedException();
        }
        public IEnumerable<TaskCategoryBO> GetAll()
        {
            var TitlesID = ServiceFactory<EmployeeTitleService>().Get(x => x.EmployeeId.ToString() == UserId.ToString()).Select(x => x.TitleId);
            var lstDepartmentID = ServiceFactory<DepartmentTitleService>().GetByIds(TitlesID).Select(x => x.DepartmentId.Value);
            var result = base.GetAll().Where(x => x.DepartmentId.HasValue && lstDepartmentID.Contains(x.DepartmentId.Value));
            return result;
        }
        public IEnumerable<TaskCategoryBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            count = GetAll().Count();
            var data = Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            foreach (var it in data)
            {
                var infordeparment = ServiceFactory<DepartmentService>().GetById((Guid)it.DepartmentId);
                it.DepartmentName = infordeparment.Name;
                var datatask = ServiceFactory<TaskService>().Get(x => x.TaskCategoryId.HasValue && x.TaskCategoryId.Value == it.Id);
                it.HasDelete = (datatask != null && datatask.ToList().Count > 0) ? false : true;
            }
            return data;
        }

        public IEnumerable<TaskCategoryBO> GetByfilterName(int pageIndex, int pageSize, string filterName, Guid deDepartmentId, out int count)
        {
            filterName = filterName.ToLower();
            if (deDepartmentId == Guid.Empty)
            {
                count = GetAll().Where(x => x.Name.ToLower().Contains(filterName) || (!string.IsNullOrEmpty(x.Content) && x.Content.ToLower().Contains(filterName))).Count();
                var data = Page(GetAll().Where(x => x.Name.ToLower().Contains(filterName) || (!string.IsNullOrEmpty(x.Content) && x.Content.ToLower().Contains(filterName))).OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
                foreach (var it in data)
                {
                    if (it.DepartmentId.HasValue)
                    {
                        var infordeparment = ServiceFactory<DepartmentService>().GetById((Guid)it.DepartmentId);
                        it.DepartmentName = infordeparment.Name;
                    }
                }
                return data;
            }
            else
            {
                count = base.GetAll().Where(x => ((!string.IsNullOrEmpty(x.Name) && x.Name.ToLower().Contains(filterName)) || (!string.IsNullOrEmpty(x.Content) && x.Content.ToLower().Contains(filterName))) && (x.DepartmentId.HasValue && x.DepartmentId.Value == deDepartmentId)).Count();
                var data = Page(base.GetAll().Where(x => ((!string.IsNullOrEmpty(x.Name) && x.Name.ToLower().Contains(filterName)) || (!string.IsNullOrEmpty(x.Content) && x.Content.ToLower().Contains(filterName))) && (x.DepartmentId.HasValue && x.DepartmentId.Value == deDepartmentId)).OrderByDescending(x => x.CreateAt), pageIndex, pageSize);
                foreach (var it in data)
                {
                    var infordeparment = ServiceFactory<DepartmentService>().GetById((Guid)it.DepartmentId);
                    it.DepartmentName = infordeparment.Name;
                }
                return data;
            }


        }
        public Dictionary<Guid, string> GetDepartmentByUser()
        {
            Dictionary<Guid, string> result = new Dictionary<Guid, string>();
            result = ServiceFactory<DepartmentTitleRoleService>().GetListDeparment_HasRoleEdit(UserId);
            return result;
        }
    }
}
