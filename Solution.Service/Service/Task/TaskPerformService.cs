﻿using iDAS.ADO;
using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
namespace iDAS.Service
{
    public class TaskPerformService : BaseService<TaskPerformDTO, TaskPerformBO>, ITaskPerformService
    {
        /// <summary>
        /// Lấy danh sách thực hiện công việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public IEnumerable<TaskPerformBO> GetAll(Guid taskId, int pageIndex, int pageSize, out int totalCount)
        {
            var performIds = ServiceFactory<TaskResourceService>().GetPerformIds(taskId);
            var query = base.GetQuery().Where(i => performIds.Any(z => z == i.Id));
            totalCount = query.Count();
            var source = base.Page(query.OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            var resources = ServiceFactory<TaskResourceService>().GetByIds(source.Select(i => i.Id));
            var task = ServiceFactory<TaskService>().GetById(taskId);
            var employees = base.GetEmployees(resources.Select(i => i.EmployeeId.Value));
            var employeeCreateId = ServiceFactory<TaskResourceService>().GetEmployeeCreateId(taskId).FirstOrDefault();
            var employeeAssignId = ServiceFactory<TaskResourceService>().GetEmployeeAssignId(taskId).FirstOrDefault();
            var employeeReviewId = ServiceFactory<TaskResourceService>().GetEmployeeReviewId(taskId).FirstOrDefault();
            var data = source.ToList();
            foreach (var item in data)
            {
                item.Employee = employees.FirstOrDefault(i => resources.Any(z => z.Id == item.Id && i.Id == z.EmployeeId));
                item.TaskStatus = task.Status;
                item.UserId = UserId;
                item.EmployeeCreateId = employeeCreateId;
                item.EmployeeAssignId = employeeAssignId;
                item.EmployeeReviewId = employeeReviewId;
            }
            return data;
        }
        /// <summary>
        /// Lấy thông tin cho form nhập liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TaskPerformBO GetFormItem(Guid? id, Guid? taskId)
        {
            TaskPerformBO item;
            if (id == null)
            {
                item = new TaskPerformBO() { TaskId = taskId };
            }
            else
            {
                item = base.GetById(id.Value);
                var employeeId = ServiceFactory<TaskResourceService>().GetById(item.Id).EmployeeId;
                item.Employee = base.GetEmployee(employeeId.Value);
            }
            if (taskId.HasValue)
            {
                var task = ServiceFactory<TaskService>().GetById(taskId.Value);
                item.TaskId = taskId;
                item.TaskStatus = task.Status;
                item.EmployeeCreateId = ServiceFactory<TaskResourceService>().GetEmployeeCreateId(taskId.Value).FirstOrDefault();
                item.EmployeeAssignId = ServiceFactory<TaskResourceService>().GetEmployeeAssignId(taskId.Value).FirstOrDefault();
                item.EmployeeReviewId = ServiceFactory<TaskResourceService>().GetEmployeeReviewId(taskId.Value).FirstOrDefault();
                item.CalendarId = task.CalendarId;
                if (id == null)
                {
                    item.StartAt = task.StartAt;
                    item.EndAt = task.EndAt;
                }
            }
            return item;
        }
        /// <summary>
        /// Lấy thông tin hủy công việc thực hiện
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TaskPerformBO GetCancelItem(Guid id)
        {
            var item = base.GetById(id);
            var taskId = ServiceFactory<TaskResourceService>().GetById(id).TaskId.Value;
            var task = ServiceFactory<TaskService>().GetById(taskId);
            item.TaskStatus = task.Status;
            item.EmployeeCreateId = ServiceFactory<TaskResourceService>().GetEmployeeCreateId(taskId).FirstOrDefault();
            item.EmployeeAssignId = ServiceFactory<TaskResourceService>().GetEmployeeAssignId(taskId).FirstOrDefault();
            item.EmployeeReviewId = ServiceFactory<TaskResourceService>().GetEmployeeReviewId(taskId).FirstOrDefault();
            if (item.IsCancel != true && task.IsCancel == true)
            {
                item.ReasonOfCancel = task.ReasonOfCancel;
                item.CancelAt = task.CancelAt;
            }
            return item;
        }
        /// <summary>
        /// Lấy thông tin tạm dừng công việc thực hiện
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TaskPerformBO GetPauseItem(Guid id)
        {
            var item = base.GetById(id);
            var taskId = ServiceFactory<TaskResourceService>().GetById(id).TaskId.Value;
            var task = ServiceFactory<TaskService>().GetById(taskId);
            item.EmployeeCreateId = ServiceFactory<TaskResourceService>().GetEmployeeCreateId(taskId).FirstOrDefault();
            item.EmployeeAssignId = ServiceFactory<TaskResourceService>().GetEmployeeAssignId(taskId).FirstOrDefault();
            item.EmployeeReviewId = ServiceFactory<TaskResourceService>().GetEmployeeReviewId(taskId).FirstOrDefault();
            item.TaskStatus = task.Status;
            if (item.IsPause != true && task.IsPause == true)
            {
                item.ReasonOfPause = task.ReasonOfPause;
                item.PauseAt = task.PauseAt;
            }
            return item;
        }
        /// <summary>
        /// Lấy thông tin kiểm tra công việc thực hiện
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TaskPerformBO GetCheckItem(Guid id)
        {
            var item = base.GetById(id);
            var taskId = ServiceFactory<TaskResourceService>().GetById(id).TaskId.Value;
            var task = ServiceFactory<TaskService>().GetById(taskId);
            item.EmployeeCreateId = ServiceFactory<TaskResourceService>().GetEmployeeCreateId(taskId).FirstOrDefault();
            item.EmployeeAssignId = ServiceFactory<TaskResourceService>().GetEmployeeAssignId(taskId).FirstOrDefault();
            item.EmployeeReviewId = ServiceFactory<TaskResourceService>().GetEmployeeReviewId(taskId).FirstOrDefault();
            item.TaskStatus = task.Status;
            return item;
        }
        /// <summary>
        /// Thêm mới thông tin giao việc cho nhân sự có vai trò là thực hiện
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        public Guid Create(TaskPerformBO item)
        {
            try
            {
                //check người dùng đã được giao thực hiện cho công việc hay chưa
                var ResourceAPO = ServiceFactory<TaskResourceService>().GetQuery().Where(i => i.TaskId == item.TaskId && i.EmployeeId == item.EmployeeId && i.IsRolePerform == true).Any();
                if (ResourceAPO == false)
                {
                    var resource = new TaskResourceBO()
                    {
                        TaskId = item.TaskId,
                        EmployeeId = item.EmployeeId,
                        IsRolePerform = true,
                    };
                    var resourceId = ServiceFactory<TaskResourceService>().Insert(resource, true);
                    item.DurationTime = ServiceFactory<CalendarService>().GetWorkingDay(item.CalendarId, item.StartAt.Value, item.EndAt.Value);
                    item.Id = resourceId;
                    return base.Insert(item);
                }
                else
                {
                    return Guid.Empty;
                }

            }
            catch
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }
        /// <summary>
        /// Cập nhật thông tin giao việc cho nhân sự có vai trò là thực hiện
        /// </summary>
        /// <param name="item"></param>
        public void Update(TaskPerformBO item)
        {
            try
            {
                item.DurationTime = ServiceFactory<CalendarService>().GetWorkingDay(item.CalendarId, item.StartAt.Value, item.EndAt.Value);
                ServiceFactory<TaskResourceService>().UpdatePerformer(item.Id, item.EmployeeId);
                base.Update(item);
            }
            catch
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }
        /// <summary>
        /// Cập nhật báo cáo thực hiện công việc
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        public void Report(TaskPerformBO item)
        {
            try
            {
                item.ReportAt = DateTime.Now;
                item.PerformAt = item.PerformAt ?? DateTime.Now;
                base.Update(item, true);
                if (!string.IsNullOrWhiteSpace(item.Report))
                {
                    var report = new TaskCommunicationBO()
                    {
                        TaskResourceId = item.Id,
                        Type = (int)Resource.Task.CommunicationType.Report,
                        Content = item.Report,
                        IsPublic = false
                    };
                    var CommunicationId =  ServiceFactory<TaskCommunicationService>().Insert(report, true);
                    //upload file
                    var fileIds = ServiceFactory<FileService>().UploadRange(item.FileAttachs, true);
                    //Insert file upload
                    foreach (var file in fileIds)
                    {
                        var obj = new TaskCommunicationFileBO();
                        obj.TaskCommunicationId = CommunicationId;
                        obj.FileId = file;
                        ServiceFactory<TaskCommunicationFileService>().Insert(obj, true);
                    }
                    #region Luu history
                    Sql_ADO.idasAdoService.ExecuteNoquery("sp_HistoryTask_Insert", parameter: new
                    {
                        taskId = item.TaskId,
                        employeeId = base.GetUserCurrent().Id,
                        content = item.Report,
                        percent = item.CompleteRateText,
                        createdate = DateTime.Now
                    });
                    #endregion
                }
                var taskId = ServiceFactory<TaskResourceService>().GetById(item.Id).TaskId;
                ServiceFactory<TaskService>().SetTaskPerformAt(taskId.Value);
                ServiceFactory<TaskService>().SetCompleteRate(taskId.Value);
                if(item.IsComplete)
                    ServiceFactory<TaskService>().SetCompleteAt(taskId.Value);
            }
            catch (Exception e)
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }
        /// <summary>
        /// Cập nhật kết quả kiểm tra thực hiện công việc
        /// </summary>
        /// <param name="item"></param>
        public void Check(TaskPerformBO item)
        {
            try
            {
                if (item.IsFinish == true)
                {
                    item.FinishAt = DateTime.Now;
                    item.FinishBy = UserId;
                }
                base.Update(item);
            }
            catch
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }
        /// <summary>
        /// Gửi công việc đến người thực hiện
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        public Boolean Send(TaskPerformBO item)
        {
            try
            {
                //check người dùng đã được giao thực hiện cho công việc hay chưa
                var ResourceAPO = ServiceFactory<TaskResourceService>().GetQuery().Where(i => i.TaskId == item.TaskId && i.EmployeeId == item.EmployeeId && i.IsRolePerform == true).Any();
                if (ResourceAPO == false)
                {
                    item.IsAssign = true;
                    item.AssignAt = DateTime.Now;
                    ServiceFactory<TaskService>().SetTaskIsAssigned(item.TaskId.Value, false);
                    if (item.Id == Guid.Empty || item.Id == null)
                        item.Id = Create(item);
                    else
                        Update(item);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }
        /// <summary>
        /// Hủy công việc của người thực hiện
        /// </summary>
        /// <param name="item"></param>
        public void Cancel(TaskPerformBO item)
        {
            if (item.IsCancel == true)
            {
                item.CancelAt = DateTime.Now;
            }
            else
            {
                item.ReasonOfCancel = string.Empty;
                if (item.CancelAt.HasValue)
                    item.CancelAt = DateTime.MinValue;
            }
            base.Update(item);
        }
        /// <summary>
        /// Tạm dừng công việc của người thực hiện
        /// </summary>
        /// <param name="item"></param>
        public void Pause(TaskPerformBO item)
        {
            if (item.IsPause == true)
            {
                item.PauseAt = DateTime.Now;
            }
            else
            {
                item.ReasonOfPause = string.Empty;
                if (item.PauseAt.HasValue)
                    item.PauseAt = DateTime.MinValue;
            }
            base.Update(item);
        }
        /// <summary>
        /// Kiểm tra người sử dụng có được phép tạo thông tin giao việc thực hiện
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public bool CheckIsCreate(Guid taskId)
        {
            var check1 = ServiceFactory<TaskService>().GetQuery().Any(i => i.Id == taskId && i.IsCancel != true && i.IsPause != true && i.IsFinish != true);
            var check2 = ServiceFactory<TaskResourceService>().GetQuery().Any(i => i.TaskId == taskId && i.EmployeeId == UserId && (i.IsRoleAssign == true || i.IsRoleCreate == true || i.IsRoleReview == true));
            return check1 && check2;
        }
        /// <summary>
        /// Lấy kết quả tiến độ thực hiện công việc
        /// </summary>
        /// <param name="taksId"></param>
        /// <returns></returns>
        public double GetCompleteRate(Guid taksId)
        {
            var resourceIds = ServiceFactory<TaskResourceService>().GetPerformIds(taksId);
            var performs = base.GetByIds(resourceIds);
            double rate = 0;
            foreach (var item in performs)
            {
                rate = rate + item.UnitComplete;
            }
            return rate;
        }
        /// <summary>
        /// Lấy danh sách công việc đang thực hiện của nhân sự trong thời gian bắt đầu và kết thúc
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="startAt"></param>
        /// <param name="endAt"></param>
        /// <returns></returns>
        public IEnumerable<TaskPerformBO> GetListPerformByEmployeeAndTime(Guid? employeeId, DateTime? startAt, DateTime? endAt, int pageIndex, int pageSize, out int totalCount)
        {
            if (!employeeId.HasValue || !startAt.HasValue || !endAt.HasValue)
            {
                totalCount = 0;
                return null;
            }
            var query1 = ServiceFactory<TaskResourceService>().GetListTaskByEmployeeQuery(employeeId.Value, (int)Resource.Task.Role.Performer, (int)Resource.Task.Status.Perform,0,0);
            var query2 = ServiceFactory<TaskResourceService>().GetListTaskByEmployeeQuery(employeeId.Value, (int)Resource.Task.Role.Performer, (int)Resource.Task.Status.WaitPerform,0,0);
            var query = query1.Concat(query2);
            var queryPerform = base.GetAll().Where(i => query.Any(z => z.Id == i.Id && i.StartAt <= endAt && i.EndAt >= startAt));
            if (queryPerform != null)
                totalCount = queryPerform.Count();
            else
                totalCount = 0;
            var source = base.Page(queryPerform.OrderByDescending(i => i.CreateAt), pageIndex, pageSize);

            var taskIds = query.Select(i => i.TaskId.Value);
            var tasks = ServiceFactory<TaskService>().GetByIds(taskIds);

            var employeeIds = query.Select(i => i.EmployeeId.Value);
            var employees = base.GetEmployees(employeeIds);

            var data = source.ToList();
            foreach (var item in data)
            {
                var resource = query.FirstOrDefault(i => i.Id == item.Id);
                var task = tasks.FirstOrDefault(i => i.Id == resource.TaskId);
                var employee = employees.FirstOrDefault(i => i.Id == resource.EmployeeId);
                item.Employee = employee;
                item.TaskId = task.Id;
                item.TaskName = task.Name;
            }
            return data;
        }
        /// <summary>
        /// Đưa ra công danh sách công việc từ lúc tạo đến lúc có thao tác cập nhật cuối cùng
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public IEnumerable<TaskPerformBO> GetSummaryBetweenDates(DateTime fromDate, DateTime toDate)
        {
            var data = Enumerable.Empty<TaskPerformBO>();
            var taskPerformByCurrentUserID = ServiceFactory<TaskResourceService>().GetByEmployeeIdPerformRole(UserId).Select(i => i.Id).AsEnumerable();
            data = GetByIds(taskPerformByCurrentUserID).Where(i =>
                (i.PerformAt.HasValue && i.PerformAt.Value.Date <= i.StartAt ? fromDate.Date <= i.PerformAt.Value.Date : fromDate.Date <= i.StartAt.Value.Date) &&
                (i.FinishAt.HasValue ? i.FinishAt.Value.Date <= toDate.Date
                : i.CancelAt.HasValue ? i.CancelAt.Value.Date <= toDate.Date
                : i.CompleteAt.HasValue ? i.CompleteAt.Value.Date < toDate.Date
                : i.PauseAt.HasValue ? i.PauseAt.Value.Date <= toDate.Date
                : i.PerformAt.HasValue ? i.PerformAt.Value.Date <= toDate.Date
                : i.EndAt.HasValue ? i.EndAt.Value.Date <= toDate.Date
                : true
                ));
            return data;
        }
        /// <summary>
        /// Đưa ra danh sách công việc thực hiện bởi người đang đăng nhập hệ thống
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TaskPerformBO> GetByCurrentUser()
        {
            var taskPerformByCurrentUserID = ServiceFactory<TaskResourceService>().GetByEmployeeIdPerformRole(UserId).Select(i => i.Id);
            var data = GetByIds(taskPerformByCurrentUserID);
            return data;
        }
        public IEnumerable<TaskPerformBO> GetByDepartment(Guid departmentId)
        {
            //bước 1. lấy danh sách id chức danh của phòng ban
            var roleIds = ServiceFactory<DepartmentTitleService>().GetQuery().Where(i => i.DepartmentId == departmentId).Select(t => t.Id);
            //bước 2. lấy danh sách id nhân sự theo danh sách id chức danh đã lấy ở bước 1
            var employeeIds = ServiceFactory<EmployeeTitleService>().GetQuery().Where(u => roleIds.Any(c => c == u.TitleId))
                            .Select(t => t.EmployeeId).Distinct();
            var taskPerformByDeartmentID = ServiceFactory<TaskResourceService>()
                            .GetQuery()
                            .Where(i => i.EmployeeId != null && employeeIds.Contains(i.EmployeeId.Value))
                            .Select(i => i.Id);
            var data = GetByIds(taskPerformByDeartmentID).ToList();
            return data;
        }

    }
}