﻿
using iDAS.DataAccess;
using System;

namespace iDAS.Service
{
    public class TaskHistoryService : BaseService<TaskHistoryDTO, TaskHistoryBO>, ITaskHistoryService
    {
        public void Insert(Guid taskID, byte[] data, bool allowSave = true)
        {
            var item = new TaskHistoryBO()
            {
                TaskId = taskID,
                Date = DateTime.Now,
                Data = data
            };
            base.Insert(item, allowSave);
        }
    }
}
