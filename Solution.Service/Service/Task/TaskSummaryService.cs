﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class TaskSummaryService : BaseService<TaskDTO, TaskBO>, ITaskSummaryService
    {
        public List<SummaryBO> GetSummaryNearly(int role = 0)
        {
            var firstMonth = DateTime.Now.AddMonths(-11);
            var firstDate = Utilities.GetFirstDayOfMonth(firstMonth.Month, firstMonth.Year);
            var lastDate = Utilities.GetLastDayOfMonth(DateTime.Now.Month, DateTime.Now.Year);
            var summayData = new List<SummaryBO>();
            var taskData = Enumerable.Empty<CommonSummaryBO>();
            taskData = role == (int)Resource.StatisRole.Perform ? ServiceFactory<TaskPerformService>().GetSummaryBetweenDates(firstDate, lastDate).Select(i => new CommonSummaryBO
            {
                AssignAt = i.AssignAt,
                CancelAt = i.CancelAt,
                CompleteAt = i.CompleteAt,
                CompleteRate = i.CompleteRate,
                CreateAt = i.CreateAt,
                CreateBy = i.CreateBy,
                EndAt = i.EndAt,
                FinishAt = i.FinishAt,
                FinishBy = i.FinishBy,
                IsAssign = i.IsAssign,
                IsCancel = i.IsCancel,
                IsDelete = i.IsDelete,
                IsFinish = i.IsFinish,
                IsPause = i.IsPause,
                PauseAt = i.PauseAt,
                PerformAt = i.PerformAt,
                ReportAt = i.ReportAt,
                StartAt = i.StartAt,
                UpdateAt = i.UpdateAt,
                UpdateBy = i.UpdateBy
            }) :
            taskData = ServiceFactory<TaskService>().GetSummaryBetweenDates(firstDate, lastDate)
                .Select(i => new CommonSummaryBO
                {
                    AssignAt = i.AssignAt,
                    CancelAt = i.CancelAt,
                    CompleteAt = i.CompleteAt,
                    CompleteRate = i.CompleteRate,
                    CreateAt = i.CreateAt,
                    CreateBy = i.CreateBy,
                    EndAt = i.EndAt,
                    FinishAt = i.FinishAt,

                    IsAssign = i.IsAssign,
                    IsCancel = i.IsCancel,
                    IsDelete = i.IsDelete,
                    IsFinish = i.IsFinish,
                    IsPause = i.IsPause,
                    PauseAt = i.PauseAt,
                    PerformAt = i.PerformAt,

                    StartAt = i.StartAt,
                    UpdateAt = i.UpdateAt,
                    UpdateBy = i.UpdateBy
                });

            for (int i = -11; i <= 0; i++)
            {
                // Đưa ra tháng tương ứng
                var monthSummary = DateTime.Now.AddMonths(i);
                // Đưa ra ngày cuối cùng của tháng
                var monthSummaryLastDate = i == 0 ? DateTime.Now : Utilities.GetLastDayOfMonth(monthSummary.Month, monthSummary.Year);
                // Công việc kết thúc trong tháng là công việc có ngày kết thúc trong tháng đó
                var countFinish = taskData.Count(u => u.FinishAt.HasValue && (u.FinishAt.Value.Date.Month == monthSummary.Month && u.FinishAt.Value.Date.Year == monthSummary.Year));
                // Công việc quá hạn là công việc chưa kết thúc được trong tháng đó 
                // có ngày kết thúc dự kiến trước ngày cuối cùng của tháng
                // không bị hủy bỏ trong tháng và kết thúc trong tháng
                var countOutOfDate = taskData.Count(u => (u.EndAt.HasValue && u.EndAt.Value.Date < monthSummaryLastDate.Date)
                    && (!u.FinishAt.HasValue || monthSummaryLastDate.Date < u.FinishAt.Value.Date)
                    && (!u.CancelAt.HasValue || monthSummaryLastDate.Date < u.CancelAt.Value.Date));
                // Công việc chưa kết thúc là có thời gian bắt đầu trước ngày kết thúc của tháng
                // và chưa có ngày kết thúc hoặc ngày kết thúc sau ngày kết thúc của tháng
                var countUnfinish = taskData.Count(u => u.StartAt.Value.Date <= monthSummaryLastDate.Date
                    && (!u.FinishAt.HasValue || monthSummaryLastDate.Date < u.FinishAt.Value.Date)
                    && (!u.CancelAt.HasValue || monthSummaryLastDate.Date < u.CancelAt.Value.Date));

                var lastDateByMonth = Utilities.GetLastDayOfMonth(monthSummary.Month, monthSummary.Year);
                var sumAllTask = taskData.Count(u =>
                    (u.PerformAt.HasValue && u.PerformAt.Value.Date < u.StartAt.Value.Date ? u.PerformAt.Value.Date <= lastDateByMonth.Date : u.StartAt.Value.Date <= lastDateByMonth.Date)
                    &&
                     (u.FinishAt.HasValue ? u.FinishAt.Value.Date <= lastDateByMonth.Date
                        : u.CancelAt.HasValue ? u.CancelAt.Value.Date <= lastDateByMonth.Date
                        : u.CompleteAt.HasValue ? u.CompleteAt.Value.Date < lastDateByMonth.Date
                        : u.PauseAt.HasValue ? u.PauseAt.Value.Date <= lastDateByMonth.Date
                        : u.PerformAt.HasValue ? u.PerformAt.Value.Date <= lastDateByMonth.Date
                        : u.EndAt.HasValue ? u.EndAt.Value.Date <= lastDateByMonth.Date
                        : true
                        )
                        );

                var summary = new SummaryBO()
                {
                    MonthSummary = monthSummary,
                    Finish = countFinish,
                    Unfinish = countUnfinish,
                    OutOfDate = countOutOfDate,
                    Total = sumAllTask
                };

                summayData.Add(summary);
            }
            return summayData;
        }

        public List<TaskPieChartBO> CurrentSummary(int role = 0)
        {
            var summaryData = new List<TaskPieChartBO>();
            IEnumerable<CommonSummaryBO> taskData = Enumerable.Empty<CommonSummaryBO>();
            taskData = role == (int)Resource.StatisRole.Perform ? ServiceFactory<TaskPerformService>().GetByCurrentUser().Select(i => new CommonSummaryBO
            {
                AssignAt = i.AssignAt,
                CancelAt = i.CancelAt,
                CompleteAt = i.CompleteAt,
                CompleteRate = i.CompleteRate,
                CreateAt = i.CreateAt,
                CreateBy = i.CreateBy,
                EndAt = i.EndAt,
                FinishAt = i.FinishAt,
                FinishBy = i.FinishBy,
                IsAssign = i.IsAssign,
                IsCancel = i.IsCancel,
                IsDelete = i.IsDelete,
                IsFinish = i.IsFinish,
                IsPause = i.IsPause,
                PauseAt = i.PauseAt,
                PerformAt = i.PerformAt,
                ReportAt = i.ReportAt,
                StartAt = i.StartAt,
                UpdateAt = i.UpdateAt,
                UpdateBy = i.UpdateBy
            }) :
            taskData = ServiceFactory<TaskService>().GetByCurrentUserAssign()
                .Select(i => new CommonSummaryBO
                {
                    AssignAt = i.AssignAt,
                    CancelAt = i.CancelAt,
                    CompleteAt = i.CompleteAt,
                    CompleteRate = i.CompleteRate,
                    CreateAt = i.CreateAt,
                    CreateBy = i.CreateBy,
                    EndAt = i.EndAt,
                    FinishAt = i.FinishAt,

                    IsAssign = i.IsAssign,
                    IsCancel = i.IsCancel,
                    IsDelete = i.IsDelete,
                    IsFinish = i.IsFinish,
                    IsPause = i.IsPause,
                    PauseAt = i.PauseAt,
                    PerformAt = i.PerformAt,

                    StartAt = i.StartAt,
                    UpdateAt = i.UpdateAt,
                    UpdateBy = i.UpdateBy
                });
            summaryData.Add(new TaskPieChartBO()
            {
                Name = "Chờ thực hiện",
                Value = taskData.Count(i => i.IsAssign == true && i.PerformAt.HasValue)
            });
            summaryData.Add(new TaskPieChartBO()
            {
                Name = "Tạm dừng",
                Value = taskData.Count(i => i.IsPause == true)
            });
            summaryData.Add(new TaskPieChartBO()
            {
                Name = "Hủy",
                Value = taskData.Count(i => i.IsCancel == true)
            });
            summaryData.Add(new TaskPieChartBO()
            {
                Name = "Thực hiện",
                Value = taskData.Count(i => i.PerformAt.HasValue == true && i.IsCancel != true && i.IsPause != true)
            });
            summaryData.Add(new TaskPieChartBO()
            {
                Name = "Kết thúc",
                Value = taskData.Count(i => i.IsFinish == true)
            });
            return summaryData;
        }
        public List<TaskPieChartBO> SummaryByDepartment(Guid departmentId)
        {
            var summaryData = new List<TaskPieChartBO>();
            IEnumerable<CommonSummaryBO> taskData = Enumerable.Empty<CommonSummaryBO>();
            var taskPerformByDeparmtent = ServiceFactory<TaskPerformService>().GetByDepartment(departmentId);
            taskData = taskPerformByDeparmtent.Select(i => new CommonSummaryBO
            {
                AssignAt = i.AssignAt,
                CancelAt = i.CancelAt,
                CompleteAt = i.CompleteAt,
                CompleteRate = i.CompleteRate,
                CreateAt = i.CreateAt,
                CreateBy = i.CreateBy,
                EndAt = i.EndAt,
                FinishAt = i.FinishAt,
                FinishBy = i.FinishBy,
                IsAssign = i.IsAssign,
                IsCancel = i.IsCancel,
                IsDelete = i.IsDelete,
                IsFinish = i.IsFinish,
                IsPause = i.IsPause,
                PauseAt = i.PauseAt,
                PerformAt = i.PerformAt,
                ReportAt = i.ReportAt,
                StartAt = i.StartAt,
                UpdateAt = i.UpdateAt,
                UpdateBy = i.UpdateBy
            });
            summaryData.Add(new TaskPieChartBO()
            {
                Name = "Chờ thực hiện",
                Value = taskData.Count(i => i.IsAssign == true && i.PerformAt.HasValue)
            });
            summaryData.Add(new TaskPieChartBO()
            {
                Name = "Tạm dừng",
                Value = taskData.Count(i => i.IsPause == true)
            });
            summaryData.Add(new TaskPieChartBO()
            {
                Name = "Hủy",
                Value = taskData.Count(i => i.IsCancel == true)
            });
            summaryData.Add(new TaskPieChartBO()
            {
                Name = "Thực hiện",
                Value = taskData.Count(i => i.PerformAt.HasValue == true && i.IsCancel != true && i.IsPause != true)
            });
            summaryData.Add(new TaskPieChartBO()
            {
                Name = "Kết thúc",
                Value = taskData.Count(i => i.IsFinish == true)
            });
            return summaryData;
        }



        public List<TaskPieChartBO> SummaryByCategories()
        {
            throw new NotImplementedException();
        }

        public List<PieChartBO> TaskStatusAnalytic(Guid departmentId)
        {
            var summaryData = new List<PieChartBO>();
            IEnumerable<CommonSummaryBO> taskData = Enumerable.Empty<CommonSummaryBO>();
            var taskPerformByDeparmtent = ServiceFactory<TaskPerformService>().GetByDepartment(departmentId);
            taskData = taskPerformByDeparmtent.Select(i => new CommonSummaryBO
            {
                AssignAt = i.AssignAt,
                CancelAt = i.CancelAt,
                CompleteAt = i.CompleteAt,
                CompleteRate = i.CompleteRate,
                CreateAt = i.CreateAt,
                CreateBy = i.CreateBy,
                EndAt = i.EndAt,
                FinishAt = i.FinishAt,
                FinishBy = i.FinishBy,
                IsAssign = i.IsAssign,
                IsCancel = i.IsCancel,
                IsDelete = i.IsDelete,
                IsFinish = i.IsFinish,
                IsPause = i.IsPause,
                PauseAt = i.PauseAt,
                PerformAt = i.PerformAt,
                ReportAt = i.ReportAt,
                StartAt = i.StartAt,
                UpdateAt = i.UpdateAt,
                UpdateBy = i.UpdateBy
            });
            summaryData.Add(new PieChartBO()
            {
                Name = "Chờ thực hiện",
                Value = taskData.Count(i => i.IsAssign == true && i.PerformAt.HasValue)
            });
            summaryData.Add(new PieChartBO()
            {
                Name = "Tạm dừng",
                Value = taskData.Count(i => i.IsPause == true)
            });
            summaryData.Add(new PieChartBO()
            {
                Name = "Hủy",
                Value = taskData.Count(i => i.IsCancel == true)
            });
            summaryData.Add(new PieChartBO()
            {
                Name = "Thực hiện",
                Value = taskData.Count(i => i.PerformAt.HasValue == true && i.IsCancel != true && i.IsPause != true)
            });
            summaryData.Add(new PieChartBO()
            {
                Name = "Kết thúc",
                Value = taskData.Count(i => i.IsFinish == true)
            });
            return summaryData;
        }


        public List<PieChartBO> TaskTypeAnalytic(Guid departmentId)
        {
            var summaryData = new List<PieChartBO>();
            var taskCate = ServiceFactory<TaskCategoryService>().GetQuery();
            var taskPerformByDeparmtent = ServiceFactory<TaskPerformService>().GetByDepartment(departmentId);
            var taskIds = taskPerformByDeparmtent.Select(c => c.TaskId.HasValue ? c.TaskId.Value : Guid.Empty).ToArray();
            var tasks = ServiceFactory<TaskService>().GetByIds(taskIds).ToList();
            foreach (var item in taskCate)
            {
                summaryData.Add(new PieChartBO()
                {
                    Name = item.Name,
                    Value = tasks.Where(c => c.TaskCategoryId == item.Id).Count()
                });
            }
            return summaryData;
        }
    }
}