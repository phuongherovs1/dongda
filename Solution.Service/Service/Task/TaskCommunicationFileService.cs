﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public class TaskCommunicationFileService : BaseService<TaskCommunicationFileDTO, TaskCommunicationFileBO>, ITaskCommunicationFileService
    {
        public IEnumerable<TaskCommunicationFileBO> GetByCommunication(Guid communicationId)
        {
            return base.Get(t => t.TaskCommunicationId == communicationId);
        }
    }
}
