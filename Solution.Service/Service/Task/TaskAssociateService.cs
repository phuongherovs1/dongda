﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public class TaskAssociateService : BaseService<TaskAssociateDTO, TaskAssociateBO>, ITaskAssociateService
    {
        public IEnumerable<TaskAssociateBO> GetByTaskID(Guid taskID)
        {
            var taskAssociates = base.Get(u => u.TaskId == taskID);
            return taskAssociates;
        }

        public IEnumerable<TaskAssociateBO> GetByTaskIDs(List<Guid> taskIDs)
        {
            var taskAssociates = base.Get(u => taskIDs.Contains(u.TaskId.Value));
            return taskAssociates;
        }
    }
}