﻿using iDAS.DataAccess;
using System;
using System.Linq;

namespace iDAS.Service
{
    public class TaskCommunicationShareService : BaseService<TaskCommunicationShareDTO, TaskCommunicationShareBO>, ITaskCommunicationShareService
    {
        public bool CheckExits(Guid taskCommunicationId, Guid userId)
        {
            return base.Get()
                .Where(t => t.TaskCommunicationId == taskCommunicationId)
                .Where(t => ServiceFactory<TaskResourceService>().GetById(t.TaskResourceId.HasValue ? t.TaskResourceId.Value : Guid.Empty).EmployeeId == userId)
                .Count() > 0;
        }
    }
}
