﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class TaskSchedulePerformService : BaseService<TaskSchedulePerformDTO, TaskSchedulePerformBO>, ITaskSchedulePerformService
    {
        /// <summary>
        /// Lấy danh sách nhân sự thực hiện từ lịch công tác
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public IEnumerable<TaskSchedulePerformBO> GetBySchedule(Guid scheduleId, int pageIndex, int pageSize, out int totalCount)
        {
            var query = base.GetQuery().Where(i => i.ScheduleId == scheduleId);
            totalCount = query.Count();
            var source = base.Page(query.OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            var data = getSchedulePerformInfo(source);
            return data;
        }
        private IEnumerable<TaskSchedulePerformBO> getSchedulePerformInfo(IQueryable<TaskSchedulePerformBO> query)
        {
            var employeeIds = query.Select(i => i.EmployeeId.Value);
            var employees = ServiceFactory<EmployeeService>().GetByIds(employeeIds);
            var scheduleIds = query.Select(i => i.ScheduleId.Value);
            var schedules = ServiceFactory<TaskScheduleService>().GetByIds(scheduleIds);

            var data = query.ToList();
            foreach (var item in data)
            {
                var employee = employees.FirstOrDefault(i => i.Id == item.EmployeeId);
                var schedule = schedules.FirstOrDefault(i => i.Id == item.ScheduleId);
                item.UserId = UserId;
                item.Employee = employee;
                item.SetSchedule(schedule);
            }
            return data;
        }
        /// <summary>
        /// Lây thông tin cho form nhập liệu
        /// </summary>
        /// <returns></returns>
        public TaskSchedulePerformBO GetFormItem(Guid? id, Guid? scheduleId)
        {
            TaskSchedulePerformBO item;
            if (id == null)
            {
                var employee = base.GetUserCurrent();
                item = new TaskSchedulePerformBO() { ScheduleId = scheduleId, Employee = employee };
            }
            else
            {
                item = base.GetById(id.Value);
                item.Employee = base.GetEmployee(item.EmployeeId.Value);
            }
            var schedule = ServiceFactory<TaskScheduleService>().GetById(scheduleId ?? item.ScheduleId.Value);
            item.AllowSend = schedule.AllowSendToPerform;
            return item;
        }
        /// <summary>
        /// Thêm mới người thực hiện vào lịch công tác
        /// </summary>
        /// <param name="item"></param>
        public void Create(TaskSchedulePerformBO item)
        {
            if (checkEmployeeExist(item))
                throw new ArgumentException(Resource.Task.Exception.EmployeeExist);
            item.Id = base.Insert(item);
        }
        /// <summary>
        /// Cập nhật thông tin của người thực hiện
        /// </summary>
        /// <param name="item"></param>
        public void Update(TaskSchedulePerformBO item)
        {
            if (checkEmployeeExist(item))
                throw new ArgumentException(Resource.Task.Exception.EmployeeExist);
            base.Update(item);
        }
        private bool checkEmployeeExist(TaskSchedulePerformBO item)
        {
            var exist = base.GetQuery().Any(i => i.Id != item.Id && i.ScheduleId == item.ScheduleId && i.EmployeeId == item.EmployeeId);
            if (exist)
                return true;
            else
                return false;
        }
        /// <summary>
        /// Lấy thông tin lịch công tác của người thực hiện
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TaskSchedulePerformBO GetById(Guid id)
        {
            var item = base.GetById(id);
            var schedule = ServiceFactory<TaskScheduleService>().GetById(item.ScheduleId.Value);
            item.SetSchedule(schedule);
            return item;
        }
        /// <summary>
        /// Gửi lịch công tác tới người xác nhận thực hiện
        /// </summary>
        /// <param name="id"></param>
        public void Send(TaskSchedulePerformBO item)
        {
            item.IsSent = true;
            item.SentAt = DateTime.Now;
            ServiceFactory<TaskScheduleService>().SetIsSent(item.ScheduleId.Value, false);
            if (item.Id == Guid.Empty || item.Id == null)
                Create(item);
            else
                Update(item);
        }
        /// <summary>
        /// Xác nhận lịch công tác của người thực hiện
        /// </summary>
        /// <param name="item"></param>
        public void Confirm(TaskSchedulePerformBO item)
        {
            if (item.IsAccept == true || item.IsChange == true || item.IsReject == true)
                item.ConfirmAt = DateTime.Now;
            else
                item.ConfirmAt = null;
            base.Update(item);
        }
        /// <summary>
        /// Hủy lịch công tác của người thực hiện
        /// </summary>
        /// <param name="item"></param>
        public void Cancel(TaskSchedulePerformBO item)
        {
            if (item.IsCancel == true)
            {
                item.CancelAt = DateTime.Now;
            }
            else
            {
                item.ReasonOfCancel = string.Empty;
                if (item.CancelAt.HasValue)
                    item.CancelAt = DateTime.MinValue;
            }
            base.Update(item);
        }
        /// <summary>
        /// Tạm dừng lịch công tác của người thực hiện
        /// </summary>
        /// <param name="item"></param>
        public void Pause(TaskSchedulePerformBO item)
        {
            if (item.IsPause == true)
            {
                item.PauseAt = DateTime.Now;
            }
            else
            {
                item.ReasonOfPause = string.Empty;
                if (item.PauseAt.HasValue)
                    item.PauseAt = DateTime.MinValue;
            }
            base.Update(item);
        }
        /// <summary>
        /// Lấy danh sách thực hiện lịch công tác của người thực hiện đã được gửi
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public IEnumerable<TaskSchedulePerformBO> GetListPerform(IQueryable<Guid> scheduleIds, Guid employeeId)
        {
            return base.GetQuery().Where(i => i.EmployeeId == employeeId && scheduleIds.Any(z => z == i.ScheduleId) && i.IsSent == true);
        }
        /// <summary>
        /// Cập nhật thông tin lịch công tác là đã chỉnh sửa tới tất cả người thực hiện
        /// </summary>
        /// <param name="id"></param>
        public void SetIsEdit(Guid id)
        {
            var source = base.GetQuery().Where(i => i.ScheduleId == id);
            foreach (var item in source)
            {
                item.IsEdit = true;
                base.Update(item, false);
            }
        }
        /// <summary>
        /// Lấy id thực hiện từ lịch công tác
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <returns></returns>
        public Guid GetId(Guid scheduleId)
        {
            return base.GetQuery().Where(i => i.ScheduleId == scheduleId).Select(i => i.Id).FirstOrDefault();
        }
    }
}
