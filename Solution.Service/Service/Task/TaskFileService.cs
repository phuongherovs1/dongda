﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class TaskFileService : BaseService<TaskFileDTO, TaskFileBO>, ITaskFileService
    {
        /// <summary>
        /// Lấy danh sách file đính kèm của công việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public FileUploadBO GetByTask(Guid taskId)
        {
            var item1 = base.GetQuery().Where(i => i.TaskId == taskId);
            var fileIds = base.GetQuery().Where(i => i.TaskId == taskId).Select(i => i.FileId.Value);
            var item = new FileUploadBO();
            item.Files = fileIds;
            return item;
        }
        public IQueryable<Guid> GetIds(Guid taskId, IEnumerable<Guid> fileIds)
        {
            return base.GetQuery().Where(i => i.TaskId == taskId && fileIds.Any(z => z == i.FileId)).Select(i => i.Id);
        }
    }
}
