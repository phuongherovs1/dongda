﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public class TaskService : BaseService<TaskDTO, TaskBO>, ITaskService
    {
        /// <summary>
        /// Lấy thông tin cho form nhập liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TaskBO GetFormItem(Guid? id)
        {
            TaskBO item;
            if (id == null)
            {
                var employee = base.GetUserCurrent();
                employee.RoleNames = string.Join(",", ServiceFactory<EmployeeService>().GetRoleNames(employee.Id));
                item = new TaskBO() { Employee = employee };
            }
            else
            {
                item = base.GetById(id.Value);
                item.FileAttachs = ServiceFactory<TaskFileService>().GetByTask(item.Id);
                var employeeId = ServiceFactory<TaskResourceService>().GetEmployeeAssignId(item.Id).FirstOrDefault();
                //item.Employee = base.GetEmployee(employeeId);
                item.Employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(employeeId);
                var lstEmployeePerform = ServiceFactory<TaskResourceService>().Get(x => x.TaskId.Value == id.Value && x.IsRolePerform == true);
                if (lstEmployeePerform != null && lstEmployeePerform.ToList().Count > 0)
                {
                    item.EmployeePerform = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(lstEmployeePerform.ToList()[0].EmployeeId.Value);
                    //item.EmployeePerform = ServiceFactory<EmployeeService>().GetById(lstEmployeePerform.ToList()[0].EmployeeId.Value);
                    //item.EmployeePerform.RoleNames = string.Join(",", ServiceFactory<EmployeeService>().GetRoleNames(item.EmployeePerform.Id));
                }
            }
            return item;
        }
        /// <summary>
        /// Tạo mới thông tin công việc
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Guid Create(TaskBO item)
        {
            if (base.GetQuery().Count(i => i.Id != item.Id && i.Name == item.Name.Trim()) > 0)
                throw new ArgumentException(Resource.Task.Exception.NameExist);
            try
            {
                item.DurationTime = ServiceFactory<CalendarService>().GetWorkingDay(item.CalendarId, item.StartAt.Value, item.EndAt.Value);
                item.Id = base.Insert(item, true);
                //upload file
                var fileIds = ServiceFactory<FileService>().UploadRange(item.FileAttachs, false);
                //Insert file upload
                foreach (var file in fileIds)
                {
                    var obj = new TaskFileBO();
                    obj.TaskId = item.Id;
                    obj.FileId = file;
                    ServiceFactory<TaskFileService>().Insert(obj, true);
                }
                var assigner = new TaskResourceBO()
                {
                    EmployeeId = item.EmployeeId,
                    IsRoleAssign = true,
                    TaskId = item.Id
                };
                ServiceFactory<TaskResourceService>().Insert(assigner, true);
                var creator = new TaskResourceBO()
                {
                    EmployeeId = UserId,
                    IsRoleCreate = true,
                    TaskId = item.Id
                };
                ServiceFactory<TaskResourceService>().Insert(creator, true);
                return item.Id;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }

        /// <summary>
        /// Giao việc quản lý chất lượng
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="QualityCAPAId"></param>
        /// <returns></returns>
        public IEnumerable<TaskBO> GetDataTaskQualityCAPA(int pageIndex, int pageSize, out int count, Guid QualityCAPAId)
        {
            var TaskIds = ServiceFactory<QualityCAPADetailService>().Get(p => p.QualityCAPAId == QualityCAPAId).Select(p => p.TaskId);
            var dataQuery = base.GetQuery()
                .Where(i => TaskIds.Contains(i.Id))
                .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            //var Employees = ServiceFactory<TaskResourceService>().Get(p => TaskIds.Contains(p.TaskId));
            //var employeeIds = Employees.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value).Distinct();
            //var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results.Select(i =>
            {
                var employeeId = ServiceFactory<TaskResourceService>().GetEmployeeAssignId(i.Id).FirstOrDefault();
                var employees = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(employeeId);
                i.EmployeePerformQuality = employees;
                return i;
            }).ToList();
            return results;
        }

        /// <summary>
        /// Danh sách công việc theo kế hoạch
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="QualityPlanId"></param>
        /// <returns></returns>
        public IEnumerable<TaskBO> GetDataTaskByQualityPlan(int pageIndex, int pageSize, out int count, Guid QualityPlanId)
        {
            try
            {
                var TaskIds = ServiceFactory<QualityTaskService>().Get(p => p.QualityPlanId == QualityPlanId).Select(p => p.TaskId);
                var dataQuery = base.GetQuery()
                    .Where(i => TaskIds.Contains(i.Id))
                    .OrderByDescending(i => i.CreateAt);
                count = dataQuery.Count();
                var results = Page(dataQuery, pageIndex, pageSize).ToList();
                results = results.Select(i =>
                {
                    var TaskResource = ServiceFactory<TaskResourceService>().Get(p => p.TaskId == i.Id).FirstOrDefault();
                    var employees = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(TaskResource.EmployeeId.Value);
                    i.EmployeePerformQuality = employees;
                    return i;
                }).ToList();
                return results;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Quản lý việc quản lý chất lượng
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="QualityCAPAId"></param>
        /// <returns></returns>
        public IEnumerable<TaskBO> GetDataTaskPerformQualityCAPA(int pageIndex, int pageSize, out int count, Guid QualityNCId)
        {
            var TaskIds = ServiceFactory<QualityCAPADetailService>().Get(p => p.QualityNCId == QualityNCId).Select(p => p.TaskId);
            var dataQuery = base.GetQuery()
                .Where(i => TaskIds.Contains(i.Id))
                .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            //var Employees = ServiceFactory<TaskResourceService>().Get(p => TaskIds.Contains(p.TaskId));
            //var employeeIds = Employees.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value).Distinct();
            //var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results.Select(i =>
            {
                var employeeId = ServiceFactory<TaskResourceService>().GetEmployeeAssignId(i.Id).FirstOrDefault();
                var employees = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(employeeId);
                i.EmployeePerformQuality = employees;
                return i;
            }).ToList();
            return results;
        }



        public IEnumerable<TaskBO> GetDataRiskTargetTask(int pageIndex, int pageSize, out int count, Guid RiskTargetPlanId)
        {
            var TaskIds = ServiceFactory<RiskTargetTaskService>().Get(p => p.RiskTargetPlanId == RiskTargetPlanId).Select(p => p.TaskId);
            var dataQuery = base.GetQuery()
                .Where(i => TaskIds.Contains(i.Id))
                .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            //var Employees = ServiceFactory<TaskResourceService>().Get(p => TaskIds.Contains(p.TaskId));
            //var employeeIds = Employees.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value).Distinct();
            //var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results.Select(i =>
            {
                var employeeId = ServiceFactory<TaskResourceService>().GetEmployeeAssignId(i.Id).FirstOrDefault();
                var employees = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(employeeId);
                i.EmployeePerformQuality = employees;
                return i;
            }).ToList();
            return results;
        }


        /// <summary>
        /// Tạo mới thông tin công việc
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Guid CreateTask(TaskBO item)
        {
            if (base.GetQuery().Count(i => i.Id != item.Id && i.Name == item.Name.Trim()) > 0)
                throw new ArgumentException(Resource.Task.Exception.NameExist);
            try
            {
                item.DurationTime = ServiceFactory<CalendarService>().GetWorkingDay(item.CalendarId, item.StartAt.Value, item.EndAt.Value);
                item.Id = base.Insert(item, false);
                //upload file
                var fileIds = ServiceFactory<FileService>().UploadRange(item.FileAttachs, false);
                //Insert file upload
                foreach (var file in fileIds)
                {
                    var obj = new TaskFileBO();
                    obj.TaskId = item.Id;
                    obj.FileId = file;
                    ServiceFactory<TaskFileService>().Insert(obj, false);
                }
                var assigner = new TaskResourceBO()
                {
                    EmployeeId = item.EmployeeId,
                    IsRoleAssign = true,
                    TaskId = item.Id
                };
                ServiceFactory<TaskResourceService>().Insert(assigner, false);
                var creator = new TaskResourceBO()
                {
                    EmployeeId = UserId,
                    IsRoleCreate = true,
                    TaskId = item.Id
                };
                ServiceFactory<TaskResourceService>().Insert(creator, false);

                var QualityCAPADetails = new QualityCAPADetailBO()
                {
                    QualityCAPAId = item.QualityCAPAId == Guid.Empty ? null : item.QualityCAPAId,
                    QualityNCId = item.QualityNCId == Guid.Empty ? null : item.QualityNCId,
                    TaskId = item.Id
                };
                ServiceFactory<QualityCAPADetailService>().Insert(QualityCAPADetails, false);
                SaveTransaction(true);
                return item.Id;
            }
            catch
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }


        /// <summary>
        /// Tạo mới thông tin công việc
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Guid CreateTaskRiskTarget(TaskBO item)
        {
            if (base.GetQuery().Count(i => i.Id != item.Id && i.Name == item.Name.Trim()) > 0)
                throw new ArgumentException(Resource.Task.Exception.NameExist);
            try
            {
                item.DurationTime = ServiceFactory<CalendarService>().GetWorkingDay(item.CalendarId, item.StartAt.Value, item.EndAt.Value);
                item.Id = base.Insert(item, true);
                //upload file
                var fileIds = ServiceFactory<FileService>().UploadRange(item.FileAttachs, true);
                //Insert file upload
                foreach (var file in fileIds)
                {
                    var obj = new TaskFileBO();
                    obj.TaskId = item.Id;
                    obj.FileId = file;
                    ServiceFactory<TaskFileService>().Insert(obj, true);
                }
                var assigner = new TaskResourceBO()
                {
                    EmployeeId = item.EmployeeId,
                    IsRoleAssign = true,
                    TaskId = item.Id
                };
                ServiceFactory<TaskResourceService>().Insert(assigner, true);
                var creator = new TaskResourceBO()
                {
                    EmployeeId = UserId,
                    IsRoleCreate = true,
                    TaskId = item.Id
                };
                ServiceFactory<TaskResourceService>().Insert(creator, true);

                var RiskTargetTask = new RiskTargetTaskBO()
                {
                    RiskTargetPlanId = item.RiskTargetPlanId == Guid.Empty ? null : item.RiskTargetPlanId,
                    TaskId = item.Id
                };
                ServiceFactory<RiskTargetTaskService>().Insert(RiskTargetTask, true);
                return item.Id;
            }
            catch
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }


        public Guid CreateDispatchTask(TaskBO item)
        {
            if (base.GetQuery().Count(i => i.Id != item.Id && i.Name == item.Name.Trim()) > 0)
                throw new ArgumentException(Resource.Task.Exception.NameExist);
            try
            {
                item.DurationTime = ServiceFactory<CalendarService>().GetWorkingDay(item.CalendarId, item.StartAt.Value, item.EndAt.Value);
                item.Id = base.Insert(item, false);
                //upload file
                var fileIds = ServiceFactory<FileService>().UploadRange(item.FileAttachs, false);
                //Insert file upload
                foreach (var file in fileIds)
                {
                    var obj = new TaskFileBO();
                    obj.TaskId = item.Id;
                    obj.FileId = file;
                    ServiceFactory<TaskFileService>().Insert(obj, false);
                }
                var assigner = new TaskResourceBO()
                {
                    EmployeeId = item.EmployeeId,
                    IsRoleAssign = true,
                    TaskId = item.Id
                };
                ServiceFactory<TaskResourceService>().Insert(assigner, false);
                var creator = new TaskResourceBO()
                {
                    EmployeeId = UserId,
                    IsRoleCreate = true,
                    TaskId = item.Id
                };
                ServiceFactory<TaskResourceService>().Insert(creator, false);

                var DispatchTask = new DispatchTaskBO()
                {
                    TaskId = item.Id,
                    DispatchId = item.DispatchId
                };
                ServiceFactory<DispatchTaskService>().Insert(DispatchTask, false);
                SaveTransaction(true);
                return item.Id;
            }
            catch
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }

        public IEnumerable<TaskBO> GetDataDispatchTask(int pageIndex, int pageSize, out int count, Guid DispatchId)
        {
            var TaskIds = ServiceFactory<DispatchTaskService>().Get(p => p.DispatchId == DispatchId).Select(p => p.TaskId);
            var dataQuery = base.GetQuery()
                .Where(i => TaskIds.Contains(i.Id))
                .OrderByDescending(i => i.CreateAt);
            count = dataQuery.Count();
            var results = Page(dataQuery, pageIndex, pageSize).ToList();
            //var Employees = ServiceFactory<TaskResourceService>().Get(p => TaskIds.Contains(p.TaskId));
            //var employeeIds = Employees.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value).Distinct();
            //var employees = ServiceFactory<EmployeeService>().GetEmployeesGeneralInfo(employeeIds).ToList();
            results = results.Select(i =>
            {
                var employeeId = ServiceFactory<TaskResourceService>().GetEmployeeAssignId(i.Id).FirstOrDefault();
                var employees = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(employeeId);
                i.EmployeePerformQuality = employees;
                return i;
            }).ToList();
            return results;
        }

        /// <summary>
        /// Cập nhật thông tin công việc
        /// </summary>
        /// <param name="item"></param>
        public void Update(TaskBO item)
        {
            if (base.GetQuery().Count(i => i.Id != item.Id && i.Name == item.Name.Trim()) > 0)
                throw new ArgumentException(Resource.Task.Exception.NameExist);

            try
            {
                item.DurationTime = ServiceFactory<CalendarService>().GetWorkingDay(item.CalendarId, item.StartAt.Value, item.EndAt.Value);
                base.Update(item, false);
                //upload file
                var fileIds = ServiceFactory<FileService>().UploadRange(item.FileAttachs, false);
                var taskFileIds = ServiceFactory<TaskFileService>().GetIds(item.Id, item.FileAttachs.FileRemoves);
                ServiceFactory<TaskFileService>().DeleteRange(taskFileIds, false);
                //Insert file upload
                foreach (var file in fileIds)
                {
                    var obj = new TaskFileBO();
                    obj.TaskId = item.Id;
                    obj.FileId = file;
                    ServiceFactory<TaskFileService>().Insert(obj, false);
                }
                ServiceFactory<TaskResourceService>().UpdateAssigner(item.Id, item.EmployeeId);
                this.SaveTransaction();
            }
            catch
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }
        /// <summary>
        /// Hủy công việc
        /// </summary>
        /// <param name="item"></param>
        public void Cancel(TaskBO item)
        {
            if (item.IsCancel == true)
            {
                item.CancelAt = DateTime.Now;
            }
            else
            {
                item.ReasonOfCancel = string.Empty;
                if (item.CancelAt.HasValue)
                    item.CancelAt = DateTime.MinValue;
            }
            base.Update(item);
        }
        /// <summary>
        /// Tạm dừng công việc
        /// </summary>
        /// <param name="item"></param>
        public void Pause(TaskBO item)
        {
            if (item.IsPause == true)
            {
                item.PauseAt = DateTime.Now;
            }
            else
            {
                item.ReasonOfPause = string.Empty;
                if (item.PauseAt.HasValue)
                    item.PauseAt = DateTime.MinValue;
            }
            base.Update(item);
        }
        /// <summary>
        /// Kết thúc công việc
        /// </summary>
        /// <param name="item"></param>
        public void Finish(TaskBO item)
        {
            base.Update(item);
        }
        /// <summary>
        /// Lấy thông tin công việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public TaskBO GetById(Guid taskId)
        {
            var employeeId = ServiceFactory<TaskResourceService>().GetEmployeeAssignId(taskId).FirstOrDefault();
            var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(employeeId);
            var task = base.GetById(taskId);
            task.Employee = employee;
            task.FileAttachs = ServiceFactory<TaskFileService>().GetByTask(task.Id);
            return task;

        }
        /// <summary>
        /// Cập nhật trạng thái cho công việc là đã giao việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="allowSave"></param>
        public void SetTaskIsAssigned(Guid taskId, bool allowSave = true)
        {
            if (base.GetQuery().Any(i => i.Id == taskId && i.IsAssign == true)) return;
            var task = new TaskBO()
            {
                Id = taskId,
                IsAssign = true,
                AssignAt = DateTime.Now,
            };
            base.Update(task, allowSave);
        }
        /// <summary>
        /// Cập nhật thời điểm bắt đầu thực hiện của công việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="allowSave"></param>
        public void SetTaskPerformAt(Guid taskId, bool allowSave = true)
        {
            if (base.GetQuery().Any(i => i.Id == taskId && i.PerformAt.HasValue)) return;
            var task = new TaskBO()
            {
                Id = taskId,
                PerformAt = DateTime.Now,
            };
            base.Update(task, allowSave);
        }
        /// <summary>
        /// Cập nhật tiến độ thực hiện công việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="allowSave"></param>
        public void SetCompleteRate(Guid taskId, bool allowSave = true)
        {
            var rate = ServiceFactory<TaskPerformService>().GetCompleteRate(taskId);
            var task = new TaskBO()
            {
                Id = taskId,
                CompleteRate = rate,
            };
            base.Update(task, allowSave);
        }
        public void SetCompleteAt(Guid taskId, bool allowSave = true)
        {
            var task = new TaskBO()
            {
                Id = taskId,
                CompleteAt = DateTime.Now,
            };
            base.Update(task, allowSave);
        }
        /// <summary>
        /// Lấy thông tin thông kê công việc theo trách nhiệm của người sử dụng hiện tại
        /// </summary>
        /// <param name="taskRole"></param>
        /// <returns></returns>
        public TaskSummaryBO GetSummary(int taskRole)
        {
            //var query = ServiceFactory<TaskResourceService>().GetListTaskByEmployeeQuery(UserId, taskRole, (int)Resource.Task.Status.All);
            var item = new TaskSummaryBO();
            var CTH = TaskGetSummary_CTH(item, taskRole);
            var DTHTH = TaskGetSummary_DTHTH(item, taskRole);
            var DTHQH = TaskGetSummary_DTHQH(item, taskRole);
            var KTDH = TaskGetSummary_KTDH(item, taskRole);
            var KTQH = TaskGetSummary_KTQH(item, taskRole);
            var HUY = TaskGetSummary_HUY(item, taskRole);
            var TD = TaskGetSummary_TD(item, taskRole);
            Task.WaitAll(CTH, DTHTH, DTHQH, KTDH, KTQH, HUY, TD);
            return item;
        }
        private async Task<string> TaskGetSummary_CTH(TaskSummaryBO item, int taskRole)
        {
            item.SummaryNew = ServiceFactory<TaskResourceService>().GetCountTask(taskRole, (int)Resource.Task.StatusNew.CTH);
            return null;
        }
        private async Task<string> TaskGetSummary_DTHTH(TaskSummaryBO item, int taskRole)
        {
            item.SummaryOnAction = ServiceFactory<TaskResourceService>().GetCountTask(taskRole, (int)Resource.Task.StatusNew.DTHTH);
            return null;
        }
        private async Task<string> TaskGetSummary_DTHQH(TaskSummaryBO item, int taskRole)
        {
            item.SummaryRole = ServiceFactory<TaskResourceService>().GetCountTask(taskRole, (int)Resource.Task.StatusNew.DTHQH);
            return null;
        }
        private async Task<string> TaskGetSummary_KTDH(TaskSummaryBO item, int taskRole)
        {
            item.SummaryRoling = ServiceFactory<TaskResourceService>().GetCountTask(taskRole, (int)Resource.Task.StatusNew.KTDH);
            return null;
        }
        private async Task<string> TaskGetSummary_KTQH(TaskSummaryBO item, int taskRole)
        {
            item.SummaryCancel = ServiceFactory<TaskResourceService>().GetCountTask(taskRole, (int)Resource.Task.StatusNew.KTQH);
            return null;
        }
        private async Task<string> TaskGetSummary_HUY(TaskSummaryBO item, int taskRole)
        {
            item.SummaryComplete = ServiceFactory<TaskResourceService>().GetCountTask(taskRole, (int)Resource.Task.StatusNew.HUY);
            return null;
        }
        private async Task<string> TaskGetSummary_TD(TaskSummaryBO item, int taskRole)
        {
            item.SummaryDelete = ServiceFactory<TaskResourceService>().GetCountTask(taskRole, (int)Resource.Task.StatusNew.TD);
            return null;
        }
        /// <summary>
        /// Đưa ra công danh sách công việc có dữ liệu với người đăng nhập tạo trong khoảng từ fromDate => toDate
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public IEnumerable<TaskBO> GetSummaryBetweenDates(DateTime fromDate, DateTime toDate)
        {
            //1. Đưa ra danh sách task Id liên quan đến người dùng với quyền giao việc
            var taskIds = ServiceFactory<TaskResourceService>().GetTaskIdByEmployeeAssignRole(UserId).AsEnumerable();
            //2. Đưa ra công việc liên quan
            //2.1 Nếu như công việc thực hiện trước ngày kế hoạch thì sử dụng ngày thực hiện làm điểm start, from date <= perform at
            //2.2 ngược lại, ngày thực hiện sau ngày bắt đầu kế hoạch thì sử dụng ngày kế hoạch làm điểm start, from date <= start at
            //2.3 Nếu công việc đã kết thúc thì lấy ngày kết thúc làm ngày giới hạn: Finish at <= to date
            //2.4 Nếu công việc bị hủy thì lấy ngày bị hủy làm ngày giới hạn: Cancel at <= to date
            //2.5 Nếu công việc đã hoàn thành thì lấy ngày hoàn thành làm ngày giới hạn: complete at <= to date
            //2.6 Nếu công việc bị dừng thì lấy ngày dừng làm ngày giới hạn: Pause at <= to date
            //2.7 Nếu công việc đã bắt đầu thực hiện thì lấy ngày bắt đầu thực hiện làm ngày giới hạn: perform at <=  to date
            //2.8 còn lại là lấy công việc theo kế hoạch: end at <= to date
            var data = GetByIds(taskIds).Where(i =>
                (i.PerformAt.HasValue && i.PerformAt.Value.Date <= i.StartAt ? fromDate.Date <= i.PerformAt.Value.Date : fromDate.Date <= i.StartAt.Value.Date) &&
                (i.FinishAt.HasValue ? i.FinishAt.Value.Date <= toDate.Date
                : i.CancelAt.HasValue ? i.CancelAt.Value.Date <= toDate.Date
                : i.CompleteAt.HasValue ? i.CompleteAt.Value.Date < toDate.Date
                : i.PauseAt.HasValue ? i.PauseAt.Value.Date <= toDate.Date
                : i.PerformAt.HasValue ? i.PerformAt.Value.Date <= toDate.Date
                : i.EndAt.HasValue ? i.EndAt.Value.Date <= toDate.Date
                : true
                ));
            return data;
        }

        public IEnumerable<TaskBO> GetByCurrentUserAssign()
        {
            var taskIds = ServiceFactory<TaskResourceService>().GetTaskIdByEmployeeAssignRole(UserId).AsEnumerable();
            var data = GetByIds(taskIds);
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentName"></param>
        /// <param name="version"></param>
        /// <param name="isAdjust"></param>
        /// <param name="writeBy"></param>
        /// <param name="approveBy"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="revision"></param>
        /// <param name="requestTime"></param>
        /// <returns></returns>
        public Guid CreateFromDocument(string documentName, string version, bool isAdjust, Guid writeBy, Guid approveBy, DateTime fromDate, DateTime toDate, int revision, int requestTime = 1, bool allowSave = true)
        {
            var task = new TaskBO()
            {
                Name = isAdjust ? requestTime == 1 ? string.Format(Common.Resource.CommonStringFormat.DocumentAdjustTask, documentName, version)
                : string.Format(Common.Resource.CommonStringFormat.DocumentReAdjustTask, documentName, version, revision, requestTime)
                : string.Format(Common.Resource.CommonStringFormat.DocumentCreateTask, documentName, version),

            };

            task.Id = base.Insert(task, allowSave);
            return task.Id;
        }

        /// <summary>
        /// Thêm công việc từ Lập kế hoạch thực hiện mục tiêu Module Quản lý chất lượng
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Guid CreatePlanTask(TaskBO item)
        {
            if (base.GetQuery().Count(i => i.Id != item.Id && i.Name == item.Name.Trim()) > 0)
                throw new DataHasBeenExistedException();
            try
            {
                item.DurationTime = ServiceFactory<CalendarService>().GetWorkingDay(item.CalendarId, item.StartAt.Value, item.EndAt.Value);
                if (item.Id == null || item.Id == Guid.Empty)
                {
                    item.Id = base.Insert(item);
                    var assigner = new TaskResourceBO()
                    {
                        EmployeeId = item.EmployeeId,
                        IsRoleAssign = true,
                        TaskId = item.Id
                    };
                    ServiceFactory<TaskResourceService>().Insert(assigner);
                    var creator = new TaskResourceBO()
                    {
                        EmployeeId = UserId,
                        IsRoleCreate = true,
                        TaskId = item.Id
                    };
                    ServiceFactory<TaskResourceService>().Insert(creator);
                    //if(item.QualityPlanId == null)
                    //{
                    //    // Thêm vào bảng trung gian giữa kế hoạch và công việc
                    //    var QualityTask = new QualityTaskBO()
                    //    {
                    //        QualityPlanId = item.QualityPlanId,
                    //        TaskId = item.Id
                    //    };
                    //    ServiceFactory<QualityTaskService>().Insert(QualityTask);
                    //}
                }
                else
                    base.Update(item);
                //upload file
                var fileIds = ServiceFactory<FileService>().UploadRange(item.FileAttachs);
                //Insert file upload
                var files = ServiceFactory<TaskFileService>().GetQuery().Where(p => p.TaskId == item.Id);
                if (fileIds != null && fileIds.Count() > 0)
                    ServiceFactory<TaskFileService>().DeleteRange(fileIds);
                foreach (var file in fileIds)
                {
                    var obj = new TaskFileBO();
                    obj.TaskId = item.Id;
                    obj.FileId = file;
                    ServiceFactory<TaskFileService>().Insert(obj);
                }
                return item.Id;
            }
            catch
            {
                throw new ArgumentException(Resource.Task.Exception.SystemFailure);
            }
        }
    }
}