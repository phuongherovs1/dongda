﻿using iDAS.ADO;
using iDAS.Service.API.Task;
using System;
using System.Data;

namespace iDAS.Service.Service.Task
{
    public class TaskSearchService : ITaskSearchService
    {
        public DataTable GetData(string textSearch, DateTime dateFrom, DateTime dateTo, Guid? employeeId, Guid? titleID, Guid? departmentID, int? status, int pageIndex, int pageSize, out int count)
        {
            count = 0;
            DataSet ds = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_Task_Search", parameter: new
            {
                TextSearch = textSearch,
                DateFrom = dateFrom,
                DateTo = dateTo,
                EmployeeId = employeeId,
                TitleID = titleID,
                DepartmentID = departmentID,
                Status = status,
                PageIndex = pageIndex,
                PageSize = pageSize,

            });

            if (ds.Tables == null)
                return null;
            else if (ds.Tables.Count < 2)
                return null;

            count = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
            return ds.Tables[0];
        }

        public DataTable GetTaskStatistic(DateTime fromDate, DateTime toDate, string year, string months, Guid? dataId, string statisticType)
        {
            try
            {
                // tổng hợp dữ liệu
                int i = Sql_ADO.idasAdoService.ExecuteNoquery("sp_Task_TaskStatistic", parameter: new
                {
                    FromDate = fromDate,
                    ToDate = toDate,
                    DataId = dataId,
                    StatisticType = statisticType
                });

                // lấy dữ liệu
                DataTable dt = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Task_GetTaskStatistic", parameter: new
                {
                    Year = year,
                    Months = months,
                    DataId = dataId,
                    StatisticType = statisticType
                });

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
