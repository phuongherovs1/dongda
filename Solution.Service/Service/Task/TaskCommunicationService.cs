﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    public class TaskCommunicationService : BaseService<TaskCommunicationDTO, TaskCommunicationBO>, ITaskCommunicationService
    {
        public override TaskCommunicationBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true)
        {
            return base.Get(t => t.Id == id)
            .Select(t =>
            {
                t.AttachFiles = new FileUploadBO
                {
                    Files = ServiceFactory<TaskCommunicationFileService>()
                    .GetByCommunication(id)
                    .Select(x => x.FileId.Value)
                    .ToList()
                };
                return t;
            }).FirstOrDefault();
        }
        public Guid Create(TaskCommunicationBO data)
        {
            var communicationId = base.Insert(data);
            //upload file
            var fileIds = ServiceFactory<FileService>().UploadRange(data.AttachFiles);
            //Xóa những file đã xóa
            ServiceFactory<TaskCommunicationFileService>().DeleteRange(data.AttachFiles.FileRemoves);
            //Insert file upload
            foreach (var item in fileIds)
            {
                var obj = new TaskCommunicationFileBO();
                obj.TaskCommunicationId = communicationId;
                obj.FileId = item;
                ServiceFactory<TaskCommunicationFileService>().Insert(obj);
            }
            return communicationId;
        }
        public override void Update(TaskCommunicationBO data, bool allowSave = true)
        {
            //upload file
            // var fileIds = fileUploadSV.Upload(data.AttachFiles);
            //Xóa những file đã xóa
            // new TaskCommunicationFileService().DeleteRange(data.AttachFiles.FileRemoves);
            //Insert file upload
            //foreach (var item in fileIds)
            //{
            //    var obj = new TaskCommunicationFileBO();
            //    obj.TaskCommunicationId = data.Id;
            //    obj.FileId = item;
            //    new TaskCommunicationFileService().Insert(obj);
            //}
            base.Update(data, allowSave);
        }
        /// <summary>
        /// Lấy danh sách trao đổi công việc từ nhân sự liên quan và kiểu thông tin trao đổi
        /// </summary>
        /// <param name="taskResourceId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<TaskCommunicationBO> Get(Guid taskResourceId, int type, Guid userId)
        {
            var taskId = ServiceFactory<TaskResourceService>().GetQueryById(taskResourceId).Select(t => t.TaskId.Value).FirstOrDefault();
            var taskResourceIds = ServiceFactory<TaskResourceService>().GetQuery().Where(t => t.TaskId == taskId).Select(t => t.Id);
            var PerformIdEmployee = ServiceFactory<TaskResourceService>().GetQuery().Where(t => t.TaskId == taskId && t.IsRolePerform == true).Select(t => t.EmployeeId);
            return base.Get().Where(i => taskResourceIds.Any(z => z == i.TaskResourceId)
                && (i.Type == type || type == (int)iDAS.Service.Common.Resource.Task.CommunicationType.All))
                  .Where(i => (i.CreateBy.Value == userId || i.UpdateBy == userId)
                   || (ServiceFactory<TaskResourceService>().CheckExits(i.TaskResourceId.HasValue ? i.TaskResourceId.Value : new Guid(), userId) && (i.IsPublic.HasValue && i.IsPublic.Value)) || ((ServiceFactory<TaskCommunicationShareService>().CheckExits(i.Id, userId) || (i.Type == (int)iDAS.Service.Common.Resource.Task.CommunicationType.Report && ServiceFactory<TaskResourceService>().EmployeeNotRoleView(i.TaskResourceId.HasValue ? i.TaskResourceId.Value : new Guid()).Any(z => z == userId))) && (i.IsPublic.HasValue && !i.IsPublic.Value))
                    )
                    .Where(c => !c.ParentId.HasValue)
                .OrderByDescending(t => t.CreateAt)
                .AsEnumerable()
                .Select(item =>
                {
                    var createReport = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.CreateBy.Value);
                    var files = ServiceFactory<TaskCommunicationFileService>().GetByCommunication(item.Id);
                    item.AttachFiles = new FileUploadBO
                    {
                        Files = files.Select(t => t.FileId.Value).ToList()
                    };
                    item.CommunicationReplies = base.Get(t => t.ParentId == item.Id)
                        .OrderByDescending(t => t.CreateAt)
                        .AsEnumerable()
                        .Select(e =>
                        {
                            var createReply = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(e.CreateBy.Value);
                            e.Employee_AvatarUrl = createReply.AvatarUrl;
                            e.Employee_Name = createReply.Name;
                            e.IsAllowEdit = e.CreateBy == userId || e.UpdateBy == userId;
                            return e;
                        }).ToList();
                    item.IsAllowEdit = item.CreateBy == userId || item.UpdateBy == userId;
                    item.Employee_AvatarUrl = createReport.AvatarUrl;
                    item.Employee_Name = createReport.Name;
                    if (PerformIdEmployee != null && PerformIdEmployee.ToList().Count > 0)
                        item.IsPerForm = PerformIdEmployee.ToList()[0] == userId;
                    return item;
                });
        }
        public IEnumerable<TaskCommunicationBO> GetDataByUser(int type, Guid userId)
        {
            return base.Get()
                .Where(i => (i.Type == type
                    || type == (int)iDAS.Service.Common.Resource.Task.CommunicationType.All)
                    && !i.ParentId.HasValue)
                .Where(i => (i.CreateBy.Value == userId || i.UpdateBy == userId)
                    || (ServiceFactory<TaskResourceService>().CheckExits(i.TaskResourceId.HasValue ? i.TaskResourceId.Value : new Guid(), userId)
                    && (i.IsPublic.HasValue && i.IsPublic.Value))
                    || ((ServiceFactory<TaskCommunicationShareService>().CheckExits(i.Id, userId)
                    || (i.Type == (int)iDAS.Service.Common.Resource.Task.CommunicationType.Report
                    && ServiceFactory<TaskResourceService>().EmployeeNotRoleView(i.TaskResourceId.HasValue ? i.TaskResourceId.Value : new Guid()).Any(z => z == userId)))
                    && (i.IsPublic.HasValue && !i.IsPublic.Value)
                        )
                    )
                .OrderByDescending(t => t.CreateAt)
                .AsEnumerable()
                .Select(item =>
                {
                    var createReport = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(item.CreateBy.Value);
                    var files = ServiceFactory<TaskCommunicationFileService>().GetByCommunication(item.Id);
                    item.AttachFiles = new FileUploadBO
                    {
                        Files = files.Select(t => t.FileId.Value).ToList()
                    };
                    item.CommunicationReplies = base.Get(t => t.ParentId == item.Id)
                        .AsEnumerable()
                        .OrderByDescending(t => t.CreateAt)
                        .Select(e =>
                        {
                            var createReply = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(e.CreateBy.Value);
                            e.Employee_AvatarUrl = createReply.AvatarUrl;
                            e.Employee_Name = createReply.Name;
                            e.IsAllowEdit = e.CreateBy == userId || e.UpdateBy == userId;
                            return e;
                        }).ToList();
                    item.Employee_AvatarUrl = createReport.AvatarUrl;
                    item.IsAllowEdit = item.CreateBy == userId || item.UpdateBy == userId;
                    item.Employee_Name = createReport.Name;
                    return item;
                });
        }
        public void InsertCommunication(TaskCommunicationBO dto, List<Guid> taskResourceGuidIds)
        {
            var taskCommunicationId = base.Insert(dto, false);
            var communications = new List<TaskCommunicationShareBO>();
            foreach (var item in taskResourceGuidIds)
            {
                var obj = new TaskCommunicationShareBO();
                obj.TaskCommunicationId = taskCommunicationId;
                obj.TaskResourceId = item;
                communications.Add(obj);
            }
            ServiceFactory<TaskCommunicationShareService>().InsertRange(communications, false);
            this.SaveTransaction();
        }
    }
}