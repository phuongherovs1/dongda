﻿using iDAS.DataAccess;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
namespace iDAS.Service
{
    public class TaskResourceService : BaseService<TaskResourceDTO, TaskResourceBO>, ITaskResourceService
    {
        /// <summary>
        /// Lấy thông tin cho form nhập liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TaskResourceBO GetFormItem(Guid? id, Guid? taskId)
        {
            TaskResourceBO item;
            if (id == null)
            {
                item = new TaskResourceBO() { TaskId = taskId };
            }
            else
            {
                item = base.GetById(id.Value);
                item.Employee = base.GetEmployee(item.EmployeeId.Value);
                item.EmployeeCreateId = ServiceFactory<TaskResourceService>().GetEmployeeCreateId(taskId.Value).FirstOrDefault();
                item.EmployeeAssignId = ServiceFactory<TaskResourceService>().GetEmployeeAssignId(taskId.Value).FirstOrDefault();
                var task = ServiceFactory<TaskService>().GetById(taskId.Value);
                item.TaskStatus = task.Status;
            }
            return item;
        }
        /// <summary>
        /// Lấy danh sách công việc liên quan đến nhân sự theo trách nhiệm và trạng thái công việc 
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="taskRole"></param>
        /// <param name="taskStatus"></param>
        /// <returns></returns>
        public IEnumerable<TaskResourceBO> GetListTaskByEmployee(Guid employeeId, int taskRole, int taskStatus, int pageSites, int pageIndex)
        {
            var query = GetListTaskByEmployeeQuery(employeeId, taskRole, taskStatus, pageSites, pageIndex);
            var data = getTaskInfo(query);
            return data;
        }
        public IQueryable<TaskResourceBO> GetListTaskByEmployeeQuery(Guid employeeId, int taskRole, int taskStatus, int pageSites, int pageIndex)
        {
            List<TaskResourceBO> query = new List<TaskResourceBO>();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString);
            conn.Open();
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = conn;
            if(pageSites == 0)
                sqlCommand.CommandText = "SELECT tr.* FROM dbo.TaskResources tr LEFT JOIN dbo.Tasks t ON tr.TaskId=t.Id WHERE tr.IsDelete = 0 AND tr.EmployeeId = '" + employeeId + "' ORDER BY t.StartAt DESC";
            else
            {
                switch (taskRole)
                {
                    case (int)Resource.Task.Role.Creator: 
                        sqlCommand.CommandText = "SELECT tr.* FROM dbo.TaskResources tr LEFT JOIN dbo.Tasks t ON tr.TaskId=t.Id WHERE tr.IsDelete = 0 AND tr.IsRoleCreate = 1 AND tr.EmployeeId = '" + employeeId + "' ORDER BY t.StartAt DESC OFFSET " + ((pageIndex - 1) * pageSites) + " ROWS FETCH NEXT " + pageSites + " ROWS ONLY";
                        break;
                    case (int)Resource.Task.Role.Assigner: 
                        sqlCommand.CommandText = "SELECT tr.* FROM dbo.TaskResources tr LEFT JOIN dbo.Tasks t ON tr.TaskId=t.Id WHERE tr.IsDelete = 0 AND tr.IsRoleAssign = 1 AND tr.EmployeeId = '" + employeeId + "' ORDER BY t.StartAt DESC OFFSET " + ((pageIndex - 1) * pageSites) + " ROWS FETCH NEXT " + pageSites + " ROWS ONLY";
                        break;
                    case (int)Resource.Task.Role.Performer: 
                        sqlCommand.CommandText = "SELECT tr.* FROM dbo.TaskResources tr LEFT JOIN dbo.Tasks t ON tr.TaskId=t.Id WHERE tr.IsDelete = 0 AND tr.IsRolePerform = 1 AND tr.EmployeeId = '" + employeeId + "' ORDER BY t.StartAt DESC OFFSET " + ((pageIndex - 1) * pageSites) + " ROWS FETCH NEXT " + pageSites + " ROWS ONLY";
                        break;
                    case (int)Resource.Task.Role.Reviewer: 
                        sqlCommand.CommandText = "SELECT tr.* FROM dbo.TaskResources tr LEFT JOIN dbo.Tasks t ON tr.TaskId=t.Id WHERE tr.IsDelete = 0 AND tr.IsRoleReview = 1 AND tr.EmployeeId = '" + employeeId + "' ORDER BY t.StartAt DESC OFFSET " + ((pageIndex - 1) * pageSites) + " ROWS FETCH NEXT " + pageSites + " ROWS ONLY";
                        break;
                    case (int)Resource.Task.Role.Viewer: 
                        sqlCommand.CommandText = "SELECT tr.* FROM dbo.TaskResources tr LEFT JOIN dbo.Tasks t ON tr.TaskId=t.Id WHERE tr.IsDelete = 0 AND tr.IsRoleView = 1 AND tr.EmployeeId = '" + employeeId + "' ORDER BY t.StartAt DESC OFFSET " + ((pageIndex - 1) * pageSites) + " ROWS FETCH NEXT " + pageSites + " ROWS ONLY";
                        break;
                    case (int)Resource.Task.Role.NotPerformer: 
                        sqlCommand.CommandText = "SELECT tr.* FROM dbo.TaskResources tr LEFT JOIN dbo.Tasks t ON tr.TaskId=t.Id WHERE tr.IsDelete = 0 AND tr.IsRolePerform <> 1 AND tr.EmployeeId = '" + employeeId + "' ORDER BY t.StartAt DESC OFFSET " + ((pageIndex - 1) * pageSites) + " ROWS FETCH NEXT " + pageSites + " ROWS ONLY";
                        break;
                    case (int)Resource.Task.Role.All:
                        sqlCommand.CommandText = "SELECT tr.* FROM dbo.TaskResources tr LEFT JOIN dbo.Tasks t ON tr.TaskId=t.Id WHERE tr.IsDelete = 0 AND tr.IsRoleAssign = 1 AND tr.EmployeeId = '" + employeeId + "' ORDER BY t.StartAt DESC OFFSET " + ((pageIndex - 1) * pageSites) + " ROWS FETCH NEXT " + pageSites + " ROWS ONLY";
                        break;
                }
            }
            SqlDataAdapter DAassigns = new SqlDataAdapter(sqlCommand);
            DataTable dataTableassigns = new DataTable();
            DAassigns.Fill(dataTableassigns);
            for (int i = 0; i < dataTableassigns.Rows.Count; i++)
            {
                query.Add(new TaskResourceBO()
                {
                    Id = new Guid(dataTableassigns.Rows[i][0].ToString()),
                    TaskId = new Guid(dataTableassigns.Rows[i][1].ToString()),
                    EmployeeId = new Guid(dataTableassigns.Rows[i][2].ToString()),
                    IsRoleCreate = (dataTableassigns.Rows[i][3] != null && !string.IsNullOrEmpty(dataTableassigns.Rows[i][3].ToString())) ? (bool)dataTableassigns.Rows[i][3] : false,
                    IsRoleAssign = (dataTableassigns.Rows[i][4] != null && !string.IsNullOrEmpty(dataTableassigns.Rows[i][4].ToString())) ? (bool)dataTableassigns.Rows[i][4] : false,
                    IsRolePerform = (dataTableassigns.Rows[i][5] != null && !string.IsNullOrEmpty(dataTableassigns.Rows[i][5].ToString())) ? (bool)dataTableassigns.Rows[i][5] : false,
                    IsRoleReview = (dataTableassigns.Rows[i][6] != null && !string.IsNullOrEmpty(dataTableassigns.Rows[i][6].ToString())) ? (bool)dataTableassigns.Rows[i][6] : false,
                    IsRoleView = (dataTableassigns.Rows[i][7] != null && !string.IsNullOrEmpty(dataTableassigns.Rows[i][7].ToString())) ? (bool)dataTableassigns.Rows[i][7] : false,
                    IsRoleMark = (dataTableassigns.Rows[i][8] != null && !string.IsNullOrEmpty(dataTableassigns.Rows[i][8].ToString())) ? (bool)dataTableassigns.Rows[i][8] : false,
                    Note = dataTableassigns.Rows[i][9].ToString(),
                    CreateAt = !string.IsNullOrEmpty(dataTableassigns.Rows[i][11].ToString()) ? Convert.ToDateTime(dataTableassigns.Rows[i][11].ToString()) : new DateTime(),
                    CreateBy = !string.IsNullOrEmpty(dataTableassigns.Rows[i][12].ToString()) ? new Guid(dataTableassigns.Rows[i][12].ToString()) : Guid.Empty,
                    UpdateAt = !string.IsNullOrEmpty(dataTableassigns.Rows[i][13].ToString()) ? Convert.ToDateTime(dataTableassigns.Rows[i][13].ToString()) : new DateTime(),
                    UpdateBy = !string.IsNullOrEmpty(dataTableassigns.Rows[i][14].ToString()) ? new Guid(dataTableassigns.Rows[i][14].ToString()) : Guid.Empty
                });
            }
            conn.Close();
            conn.Dispose();
            //query = getQueryByTaskRole(query, taskRole);
            query = getQueryByTaskStatus1(query, taskStatus);
            return query.AsQueryable();
        }
        private List<TaskResourceBO> getQueryByTaskRole(List<TaskResourceBO> query, int taskRole)
        {
            switch (taskRole)
            {
                case (int)Resource.Task.Role.Creator: query = query.Where(i => i.IsRoleCreate == true).ToList(); break;
                case (int)Resource.Task.Role.Assigner: query = query.Where(i => i.IsRoleAssign == true).ToList(); break;
                case (int)Resource.Task.Role.Performer: query = query.Where(i => i.IsRolePerform == true).ToList(); break;
                case (int)Resource.Task.Role.Reviewer: query = query.Where(i => i.IsRoleReview == true).ToList(); break;
                case (int)Resource.Task.Role.Viewer: query = query.Where(i => i.IsRoleView == true).ToList(); break;
                case (int)Resource.Task.Role.NotPerformer: query = query.Where(i => i.IsRolePerform != true).ToList(); break;
                case (int)Resource.Task.Role.All: break;
            }
            return query;
        }
        private List<TaskResourceBO> getQueryByTaskStatus(List<TaskResourceBO> query, int taskStatus)
        {
            switch (taskStatus)
            {
                case (int)Resource.Task.Status.New: query = getQueryTaskStatusNew(query); break;
                case (int)Resource.Task.Status.WaitPerform: query = getQueryTaskStatusWaitPerform(query); break;
                case (int)Resource.Task.Status.Perform: query = getQueryTaskStatusPerform(query); break;
                case (int)Resource.Task.Status.Pause: query = getQueryTaskStatusPause(query); break;
                case (int)Resource.Task.Status.NotComplete: query = getQueryTaskStatusNotComplete(query); break;
                case (int)Resource.Task.Status.Complete: query = getQueryTaskStatusComplete(query); break;
                case (int)Resource.Task.Status.Cancel: query = getQueryTaskStatusCancel(query); break;
                case (int)Resource.Task.Status.NotFinish: query = getQueryTaskStatusNotFinish(query); break;
                case (int)Resource.Task.Status.Finish: query = getQueryTaskStatusFinish(query); break;
                case (int)Resource.Task.Status.OutOfDate: query = getQueryTaskStatusOutOfDate(query); break;
                case (int)Resource.Task.Status.Other: query = getQueryTaskStatusOther(query); break;
                case (int)Resource.Task.Status.All: query = getQueryTaskStatusAll(query); break;
            }
            return query;
        }
        private List<TaskResourceBO> getQueryByTaskStatus1(List<TaskResourceBO> query, int taskStatus)
        {
            switch (taskStatus)
            {
                case (int)Resource.Task.StatusNew.ALL: query = getQueryTaskStatusAll(query); break;
                case (int)Resource.Task.StatusNew.CTH: query = getQueryTaskStatus_CTH(query); break;
                case (int)Resource.Task.StatusNew.DTHTH: query = getQueryTaskStatus_DTHTH(query); break;
                case (int)Resource.Task.StatusNew.DTHQH: query = getQueryTaskStatus_DTHQH(query); break;
                case (int)Resource.Task.StatusNew.KTDH: query = getQueryTaskStatus_KTTH(query); break;
                case (int)Resource.Task.StatusNew.KTQH: query = getQueryTaskStatus_KTQH(query); break;
                case (int)Resource.Task.StatusNew.HUY: query = getQueryTaskStatusCancel(query); break;
                case (int)Resource.Task.StatusNew.TD: query = getQueryTaskStatusPause(query); break;
            }
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatus_CTH(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetQuery();
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && z.PerformAt == null && (z.CompleteRate == null || z.CompleteRate == 0))).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatus_DTHTH(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && z.IsCancel != true && z.IsPause != true)).ToList();
            query = query.Where(i =>
                queryTask.Any(z => z.Id == i.TaskId && z.PerformAt != null && z.CompleteRate < 100 && z.IsFinish != true && i.IsRolePerform != true && z.EndAt != null && DateTime.Today.Date <= z.EndAt) ||
                queryPerform.Any(z => z.Id == i.Id && z.IsCancel != true && z.IsPause != true && z.PerformAt != null && z.CompleteRate < 100 && z.IsFinish != true)
            ).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatus_DTHQH(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && z.IsCancel != true && z.IsPause != true)).ToList();
            query = query.Where(i =>
                queryTask.Any(z => z.Id == i.TaskId && z.PerformAt != null && z.CompleteRate < 100 && z.IsFinish != true && i.IsRolePerform != true && z.EndAt != null && DateTime.Today.Date > z.EndAt) ||
                queryPerform.Any(z => z.Id == i.Id && z.IsCancel != true && z.IsPause != true && z.PerformAt != null && z.CompleteRate < 100 && z.IsFinish != true)
            ).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatus_KTTH(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && z.IsAssign == true && z.IsFinish == true && i.IsRolePerform != true && z.FinishAt != null && z.EndAt != null && z.FinishAt <= z.EndAt) ||
                queryPerform.Any(z => z.Id == i.Id && z.IsAssign == true && z.IsFinish == true)).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatus_KTQH(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && z.IsAssign == true && z.IsFinish == true && i.IsRolePerform != true && z.FinishAt != null && z.EndAt != null && z.FinishAt > z.EndAt) ||
                queryPerform.Any(z => z.Id == i.Id && z.IsAssign == true && z.IsFinish == true)).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatusNew(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetQuery();
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && z.IsAssign != true && (i.IsRoleAssign == true || i.IsRoleCreate == true || i.IsRoleReview == true || i.IsRoleView == true))).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatusWaitPerform(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && z.IsCancel != true && z.IsPause != true)).ToList();
            query = query.Where(i =>
                queryTask.Any(z => z.Id == i.TaskId && z.IsAssign == true && z.PerformAt == null && (z.CompleteRate == null || z.CompleteRate == 0) && i.IsRolePerform != true) ||
                queryPerform.Any(z => z.Id == i.Id && z.IsCancel != true && z.IsPause != true && z.IsAssign == true && (z.CompleteRate == null || z.CompleteRate == 0) && z.PerformAt == null)
            ).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatusPerform(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && z.IsCancel != true && z.IsPause != true)).ToList();
            query = query.Where(i =>
                queryTask.Any(z => z.Id == i.TaskId && z.PerformAt != null && z.CompleteRate < 100 && z.IsFinish != true && i.IsRolePerform != true) ||
                queryPerform.Any(z => z.Id == i.Id && z.IsCancel != true && z.IsPause != true && z.PerformAt != null && z.CompleteRate < 100 && z.IsFinish != true)
            ).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatusNotComplete(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && z.IsCancel != true && z.IsPause != true && z.IsAssign == true)).ToList();
            query = query.Where(i =>
                queryTask.Any(z => z.Id == i.TaskId && (z.CompleteRate == null || z.CompleteRate < 100) && i.IsRolePerform != true) ||
                queryPerform.Any(z => z.Id == i.Id && z.IsCancel != true && z.IsPause != true && z.IsAssign == true && (z.CompleteRate == null || z.CompleteRate < 100))
            ).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatusComplete(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && z.IsCancel != true && z.IsPause != true)).ToList();
            query = query.Where(i =>
                queryTask.Any(z => z.Id == i.TaskId && z.CompleteRate >= 100 && i.IsRolePerform != true) ||
                queryPerform.Any(z => z.Id == i.Id && z.IsCancel != true && z.IsPause != true && z.CompleteRate >= 100)
            ).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatusPause(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && z.IsCancel != true)).ToList();
            query = query.Where(i =>
                queryTask.Any(z => z.Id == i.TaskId && z.IsPause == true) ||
                queryPerform.Any(z => z.Id == i.Id && z.IsCancel != true && z.IsPause == true)
            ).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatusCancel(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i =>
                queryTask.Any(z => z.Id == i.TaskId && z.IsCancel == true) ||
                queryPerform.Any(z => z.Id == i.Id && z.IsCancel == true)
            ).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatusNotFinish(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i =>
                queryTask.Any(z => z.Id == i.TaskId && z.IsAssign == true && z.IsFinish != true && i.IsRolePerform != true) ||
                queryPerform.Any(z => z.Id == i.Id && z.IsAssign == true && z.IsFinish != true)
            ).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatusFinish(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && z.IsAssign == true && z.IsFinish == true && i.IsRolePerform != true) ||
                queryPerform.Any(z => z.Id == i.Id && z.IsAssign == true && z.IsFinish == true)).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatusOutOfDate(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && z.CompleteAt > z.EndAt && i.IsRolePerform != true) ||
                                    queryPerform.Any(z => z.Id == i.Id && z.CompleteAt > z.EndAt)).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatusOther(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i => queryTask.Any(z => z.Id == i.TaskId && (z.IsCancel == true || z.IsPause == true)) ||
                                    queryPerform.Any(z => z.Id == i.Id && (z.IsCancel == true || z.IsPause == true))).ToList();
            return query;
        }
        private List<TaskResourceBO> getQueryTaskStatusAll(List<TaskResourceBO> query)
        {
            var queryTask = ServiceFactory<TaskService>().GetByIds(query.Select(i => i.TaskId.Value));
            var queryPerform = ServiceFactory<TaskPerformService>().GetByIds(query.Select(i => i.Id));
            query = query.Where(i =>
                queryTask.Any(z => z.Id == i.TaskId && i.IsRolePerform != true) ||
                queryPerform.Any(z => z.Id == i.Id && z.IsAssign == true)
            ).ToList();
            return query;
        }
        private IEnumerable<TaskResourceBO> getTaskInfo(IEnumerable<TaskResourceBO> query)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString);
            conn.Open();
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = conn;
            var taskIds = query.Select(i => i.TaskId.Value).ToList();
            var tasks = ServiceFactory<TaskService>().GetByIds(taskIds, false).ToList();
            #region Get assigns
            List<TaskResourceBO> assigns = new List<TaskResourceBO>();
            sqlCommand.CommandText = "SELECT Id,TaskId,EmployeeId FROM TaskResources WHERE IsRoleAssign = 1 AND TaskId IN ('" + string.Join("','", taskIds) + "')";
            SqlDataAdapter DAassigns = new SqlDataAdapter(sqlCommand);
            DataTable dataTableassigns = new DataTable();
            DAassigns.Fill(dataTableassigns);
            for (int i = 0; i < dataTableassigns.Rows.Count; i++)
            {
                assigns.Add(new TaskResourceBO()
                {
                    Id = new Guid(dataTableassigns.Rows[i][0].ToString()),
                    TaskId = new Guid(dataTableassigns.Rows[i][1].ToString()),
                    EmployeeId = new Guid(dataTableassigns.Rows[i][2].ToString())
                });
            }
            #endregion
            #region Get mark
            List<TaskResourceBO> mark = new List<TaskResourceBO>();
            sqlCommand.CommandText = "SELECT Id,TaskId FROM TaskResources WHERE IsRoleMark = 1 AND TaskId IN ('" + string.Join("','", taskIds) + "')";
            SqlDataAdapter DAmark = new SqlDataAdapter(sqlCommand);
            DataTable dataTablmark = new DataTable();
            DAmark.Fill(dataTablmark);
            for (int i = 0; i < dataTablmark.Rows.Count; i++)
            {
                mark.Add(new TaskResourceBO()
                {
                    Id = new Guid(dataTablmark.Rows[i][0].ToString()),
                    TaskId = new Guid(dataTablmark.Rows[i][1].ToString())
                });
            }
            #endregion
            var employeeIds = assigns.Where(i => i.EmployeeId.HasValue).Select(i => i.EmployeeId.Value);
            var employees = ServiceFactory<EmployeeService>().GetByIds(employeeIds).ToList();
            var performIds = query.Select(i => i.Id);
            var performs = ServiceFactory<TaskPerformService>().GetByIds(performIds).ToList();
            var listroleAll = ServiceFactory<TaskResourceService>().Get(x => x.TaskRoleText == Common.Resource.Task.RoleText.Assigner || x.TaskRoleText == Common.Resource.Task.RoleText.Performer);
            #region Get lstKPICategory
            //List<KPICategoryBO> lstKPICategory = new List<KPICategoryBO>();
            //sqlCommand.CommandText = "SELECT Id,Maxscore FROM KPICategorys";
            //SqlDataAdapter DAlstKPICategory = new SqlDataAdapter(sqlCommand);
            //DataTable dataTablelstKPICategory = new DataTable();
            //DAlstKPICategory.Fill(dataTablelstKPICategory);
            //for(int i =0;i < dataTablelstKPICategory.Rows.Count; i++)
            //{
            //    lstKPICategory.Add(new KPICategoryBO()
            //    {
            //        Id = new Guid(dataTablelstKPICategory.Rows[i][0].ToString()),
            //        Maxscore = (dataTablelstKPICategory.Rows[i][1] != null && !string.IsNullOrEmpty(dataTablelstKPICategory.Rows[i][1].ToString())) ? (int)dataTablelstKPICategory.Rows[i][1] : 0
            //    });
            //}
            #endregion
            #region Get lstTaskCategory
            List<TaskCategoryBO> lstTaskCategory = new List<TaskCategoryBO>();
            sqlCommand.CommandText = "SELECT Id,Name FROM TaskCategories";
            SqlDataAdapter DATaskCategoryBO = new SqlDataAdapter(sqlCommand);
            DataTable dataTableTaskCategoryBO = new DataTable();
            DATaskCategoryBO.Fill(dataTableTaskCategoryBO);
            for (int i = 0; i < dataTableTaskCategoryBO.Rows.Count; i++)
            {
                lstTaskCategory.Add(new TaskCategoryBO()
                {
                    Id = new Guid(dataTableTaskCategoryBO.Rows[i][0].ToString()),
                    Name = dataTableTaskCategoryBO.Rows[i][1].ToString()
                });
            }
            #endregion
            conn.Close();
            conn.Dispose();
            var data = query.AsEnumerable().OrderByDescending(i => i.IsRoleAssign).GroupBy(i => new { i.EmployeeId, i.TaskId }).Select(i => i.First()).ToList();
            foreach (var item in data)
            {
                var task = tasks.FirstOrDefault(i => i.Id == item.TaskId);
                //lấy giá trị đã group trong danh sách ban đầu để gộp Role
                var listrole = listroleAll.Where(k => k.TaskId == item.TaskId);
                List<Result> ListRoleTextEmployee = 
                (
                    from p in listrole
                    group p by p.TaskRoleText into g
                    select new Result()
                    {
                        name = g.Key,
                        value = string.Join(", ", ServiceFactory<EmployeeService>().GetByIds(g.ToList().Select(x => x.EmployeeId.Value)).Select(x => x.Name))
                    }
                ).ToList();
                if (task != null)
                {
                    if (task.TaskCategoryId.HasValue)
                        item.TaskGroup = lstTaskCategory.Where(x => x.Id == task.TaskCategoryId.Value).First().Name;
                    //if (task.KPICatagoryId.HasValue)
                    //    item.MaxScore = lstKPICategory.Where(x => x.Id == task.KPICatagoryId.Value).First().Maxscore;
                }
                var assign = assigns.FirstOrDefault(i => i.TaskId == item.TaskId);
                var employee = assign == null ? null : employees.FirstOrDefault(i => i.Id == assign.EmployeeId);
                if (employee != null)
                    item.Employee = employee;
                item.IsRoleMark = mark == null ? false : mark.Any(x => x.TaskId.HasValue && x.TaskId.Value == item.TaskId.Value);
                item.TaskName = task == null ? string.Empty : task.Name;
                item.TaskContent = task == null ? string.Empty : task.Content;
                // nếu user là người thực hiện công việc thì hiển thông tin thực hiện công việc của người thực hiện
                // nếu user không phải là người thực hiện thì hiển thông tin công việc chung
                var lst = query.Where(k => k.EmployeeId.Value == UserId && k.TaskId == item.TaskId && k.IsRolePerform == true);
                if (lst != null && lst.ToList().Count > 0)
                {
                    item.Id = lst.ToList()[0].Id;
                    var perform = performs.FirstOrDefault(i => i.Id == item.Id);
                    if (task != null)
                        perform.TaskStatus = task.Status;
                    item.Duration = perform.Duration;
                    item.IsRolePerform = true;
                    item.StartAt = perform.StartAt;
                    item.EndAt = perform.EndAt;
                    item.CompleteRate = perform.CompleteRate;
                    item.TaskStatus = perform.TaskPerformStatus;
                    if (ListRoleTextEmployee.Count > 0)
                        foreach (Result i in ListRoleTextEmployee)
                            item.ListTaskRoleText += "<li>" + i.name + ": <b>" + i.value + "</b>" + "</li>";
                }
                else
                {
                    if (task != null)
                    {
                        item.Duration = task.Duration;
                        item.StartAt = task.StartAt;
                        item.EndAt = task.EndAt;
                        item.CompleteRate = task.CompleteRate;
                        item.TaskStatus = task.Status;
                        if (ListRoleTextEmployee.Count > 0)
                            foreach (Result i in ListRoleTextEmployee)
                                item.ListTaskRoleText += "<li>" + i.name + ": <b>" + i.value + "</b>" + "</li>";
                    }
                }
            }
            return data;
        }
        public int GetCountTask(int taskRole, int taskStatus)
        {
            var query = GetListTaskByEmployeeQuery(UserId, taskRole, taskStatus, 0 , 0);
            var data = query.AsEnumerable().OrderByDescending(i => i.IsRoleAssign).GroupBy(i => new { i.EmployeeId, i.TaskId }).Select(i => i.First()).ToList();
            return data.Count();
        }
        /// <summary>
        /// Lấy danh sách nhân sự có vai trò là theo dõi hoặc kiểm tra
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IEnumerable<TaskResourceBO> GetListViewAndReview(Guid taskId, int pageIndex, int pageSize, out int totalCount)
        {
            var query = base.GetQuery().Where(i => i.TaskId == taskId && (i.IsRoleReview == true || i.IsRoleView == true || i.IsRoleMark == true));
            totalCount = query.Count();
            var source = base.Page(query.OrderBy(i => i.CreateAt), pageIndex, pageSize);
            var employeeIds = query.Select(i => i.EmployeeId.Value);
            var employees = base.GetEmployees(employeeIds);
            var data = query.ToList();
            foreach (var item in data)
            {
                item.Employee = employees.FirstOrDefault(i => i.Id == item.EmployeeId);
            }
            return data;
        }
        /// <summary>
        /// Lấy danh sách id nhân sự liên quan đến công việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IQueryable<Guid> GetEmployeeIds(Guid taskId)
        {
            return base.GetQuery().Where(i => i.TaskId == taskId).Select(i => i.EmployeeId.Value);
        }
        /// <summary>
        /// Lấy danh sách id trách nhiệm là thực hiện 
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IQueryable<Guid> GetPerformIds(Guid taskId)
        {
            return base.GetQuery().Where(i => i.TaskId == taskId && i.IsRolePerform == true).Select(i => i.Id);
        }
        /// <summary>
        /// Lấy danh sách id của nhân sự có vai trò là thực hiện 
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IQueryable<Guid> GetEmployeePerformId(Guid taskId)
        {
            return base.GetQuery().Where(i => i.TaskId == taskId && i.IsRolePerform == true).Select(i => i.EmployeeId.Value);
        }
        /// <summary>
        /// Lấy id nhân sự có vai trò là giao việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IQueryable<Guid> GetEmployeeAssignId(Guid taskId)
        {
            return base.GetQuery().Where(i => i.TaskId == taskId && i.IsRoleAssign == true).Select(i => i.EmployeeId.Value);
        }
        /// <summary>
        /// Lấy id nhân sự có vai trò là kiểm soát
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IQueryable<Guid> GetEmployeeReviewId(Guid taskId)
        {
            return base.GetQuery().Where(i => i.TaskId == taskId && i.IsRoleReview == true).Select(i => i.EmployeeId.Value);
        }
        /// <summary>
        /// Lấy id nhân sự có vai trò là tạo việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IQueryable<Guid> GetEmployeeCreateId(Guid taskId)
        {
            return base.GetQuery().Where(i => i.TaskId == taskId && i.IsRoleCreate == true).Select(i => i.EmployeeId.Value);
        }
        /// <summary>
        /// Lấy id nhân sự có vai trò là theo dõi
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public IQueryable<Guid> GetEmployeeViewId(Guid taskId)
        {
            return base.GetQuery().Where(i => i.TaskId == taskId && i.IsRoleView == true).Select(i => i.EmployeeId.Value);
        }
        /// <summary>
        /// Kiểm tra người sử dụng có được phép tạo thông tin người liên quan
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public bool CheckIsCreate(Guid taskId)
        {
            var check1 = ServiceFactory<TaskService>().GetQuery().Any(i => i.Id == taskId && i.IsCancel != true && i.IsPause != true && i.IsFinish != true);
            var check2 = ServiceFactory<TaskResourceService>().GetQuery().Any(i => i.TaskId == taskId && i.EmployeeId == UserId && (i.IsRoleAssign == true || i.IsRoleCreate == true));
            return check1 && check2;
        }
        /// <summary>
        /// Cập nhật id của người giao việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="employeeId"></param>
        public void UpdateAssigner(Guid taskId, Guid employeeId)
        {
            var item = base.GetQuery().Where(i => i.TaskId == taskId && i.IsRoleAssign == true).FirstOrDefault();
            item.EmployeeId = employeeId;
            base.Update(item, false);
        }
        /// <summary>
        /// Cập nhật id của người thực hiện
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="employeeId"></param>
        public void UpdatePerformer(Guid id, Guid employeeId)
        {
            var item = base.GetQuery().Where(i => i.Id == id && i.IsRolePerform == true).FirstOrDefault();
            item.EmployeeId = employeeId;
            base.Update(item, false);
        }
        public IQueryable<TaskResourceBO> GetByEmployeeId(Guid employeeId)
        {
            return base.GetQuery().Where(i => i.EmployeeId == employeeId);
        }
        public IEnumerable<TaskResourceBO> GetByTaskResource(Guid taskResourceId)
        {
            var taskId = base.GetById(taskResourceId).TaskId;
            var taskResouces = Get(i => i.TaskId == taskId)
                .GroupBy(p => p.EmployeeId)
                .Select(g => g.First())
                .Select(
                i =>
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.EmployeeId.Value);
                    i.Employee_Name = employee.Name;
                    i.Employee_AvatarUrl = employee.AvatarUrl;
                    return i;
                });
            return taskResouces;
        }
        public IEnumerable<TaskResourceBO> GetByTask(Guid taskID)
        {
            var data = Get(i => i.TaskId == taskID && i.IsRolePerform == true)
                .Select(i =>
                {
                    var employee = ServiceFactory<EmployeeService>().GetEmployeeGeneralInfo(i.EmployeeId.Value);
                    var task = ServiceFactory<TaskService>().GetById(i.TaskId.Value);
                    var taskPerform = ServiceFactory<TaskPerformService>().GetById(i.Id);
                    taskPerform.TaskStatus = task.Status;
                    i.Employee = employee;
                    i.StartAt = taskPerform.StartAt;
                    i.EndAt = taskPerform.EndAt;
                    i.PerformAt = taskPerform.PerformAt;
                    i.CompleteAt = taskPerform.CompleteAt;
                    i.TaskStatus = taskPerform.TaskPerformStatus;
                    i.CompleteRate = taskPerform.CompleteRate;
                    i.CompletePlanAt = taskPerform.CompletePlanAt;
                    return i;
                });
            return data;
        }
        public bool CheckExits(Guid taskResoureId, Guid userId)
        {
            var taskId = base.GetById(taskResoureId).TaskId;
            return base.GetQuery()
                .Where(t => t.TaskId == taskId && t.EmployeeId == userId)
                .Count() > 0;
        }
        public IEnumerable<Guid> EmployeeNotRoleView(Guid taskResoureId)
        {
            var taskId = base.GetById(taskResoureId).TaskId;
            return base.GetQuery().Where(t => t.TaskId == taskId
                && (t.IsRoleAssign.Value
                || t.IsRoleCreate.Value
                || t.IsRolePerform.Value
                || t.IsRoleReview.Value))
                .Select(t => t.EmployeeId.Value)
                .Distinct();
        }
        public IQueryable<TaskResourceBO> GetByEmployeeIdPerformRole(Guid employeeId)
        {
            return base.GetQuery().Where(i => i.EmployeeId == employeeId && i.IsRolePerform == true);
        }
        public IQueryable<Guid> GetTaskIdByEmployeeAssignRole(Guid employeeId)
        {
            return base.GetQuery().Where(i => i.EmployeeId == employeeId && i.IsRoleAssign == true).Select(i => i.TaskId.Value);
        }
        public IEnumerable<TaskResourceBO> GetByCurrentUser()
        {
            return base.GetQuery().Where(i => i.EmployeeId == UserId).Take(30)
                .AsEnumerable()
                .Select(i =>
                {
                    var task = ServiceFactory<TaskService>().GetById(i.TaskId.Value);
                    i.TaskName = task.Name;
                    i.CompleteRate = task.CompleteRate;
                    return i;
                });
        }
        public DataTable GetTableByQuery(string query)
        {
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString);
                conn.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = conn;
                sqlCommand.CommandText = query;
                SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
                DataTable dataTable = new DataTable();
                da.Fill(dataTable);
                conn.Close();
                conn.Dispose();
                return dataTable;
            }
            catch (Exception e) { return new DataTable(); }
        }
    }
    public class Result
    {
        public string name { get; set; }
        public string value { get; set; }
    }
}