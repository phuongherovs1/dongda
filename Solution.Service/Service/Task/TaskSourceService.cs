﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class TaskSourceService : BaseService<TaskSourceDTO, TaskSourceBO>, ITaskSourceService
    {
        public IEnumerable<TaskSourceBO> GetAll()
        {
            var result = base.GetAll();

            return result;
        }

        public IEnumerable<TaskSourceBO> GetAll(int pageIndex, int pageSize, out int count)
        {
            count = GetAll().Count();
            return Page(GetAll().OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
        }

        public IEnumerable<TaskSourceBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count)
        {
            filterName = filterName.ToLower();
            count = GetAll().Where(x => x.Name.ToLower().Contains(filterName) || (!string.IsNullOrEmpty(x.Content) && x.Content.ToLower().Contains(filterName))).Count();
            var data = Page(GetAll().Where(x => x.Name.ToLower().Contains(filterName) || (!string.IsNullOrEmpty(x.Content) && x.Content.ToLower().Contains(filterName))).OrderByDescending(i => i.CreateAt), pageIndex, pageSize);
            return data;
        }

        public Guid InsertTaskSource(TaskSourceBO data)
        {
            throw new NotImplementedException();
        }

        public void UpdateTaskSource(TaskSourceBO data)
        {
            throw new NotImplementedException();
        }
    }
}
