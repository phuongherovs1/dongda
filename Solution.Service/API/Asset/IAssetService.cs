﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IAssetService : IService<AssetBO>
    {
        Guid Insert(AssetBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        void Update(AssetBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        IEnumerable<AssetBO> GetAll(string connection, int pageIndex, int pageSize, out int count);

        AssetBO GetFormItem();
        int Import(List<AssetBO> assetImport);
    }
}
