﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IAssetTypePropertyService : IService<AssetTypePropertyBO>
    {
        /// <summary>
        /// Get property ids by asset type id
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        IEnumerable<Guid> GetPropertyIdsByAssetTypeID(Guid assetTypeId);
        IEnumerable<Guid> GetAssetTypeIdsByPropertyID(Guid propertyID);
        IEnumerable<Guid> GetAssetTypeIdsByPropertyIds(IEnumerable<Guid> propertyIds);
        AssetTypePropertyBO GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true, bool includeAssetProperty = false);
        IEnumerable<AssetTypePropertyBO> GetByAssetType(Guid assetTypeId);
        IEnumerable<Guid> GetTypeByPropertyType(IEnumerable<Guid> propertyIds);
        IEnumerable<Guid> InsertRange(Guid typeId, IEnumerable<AssetTypePropertyBO> data, bool allowSave = true);
        void DeleteByAssetType(Guid assetTypeID, bool allowSave = true);
        void DeleteByAssetType(IEnumerable<Guid> assetTypeIds, bool allowSave = true);
    }
}
