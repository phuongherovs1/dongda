﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IAssetResourceRelateService : IService<AssetResourceRelateBO>
    {
        IEnumerable<AssetResourceRelateBO> GetSelectObject(Resource.ObjectRelative selectObject, string query = "");
        IEnumerable<AssetResourceRelateBO> GetByAssetId(Guid assetId);
        IEnumerable<AssetResourceRelateBO> GetRoleRelateTo(string cpnnection, int pageIndex, int pageSize, out int count, string filterName = "");
        IEnumerable<AssetBO> GetReview(int pageIndex, int pageSize, out int count, Guid? assetId = null);
        IEnumerable<AssetBO> GetAssetInfo(int pageIndex, int pageSize, out int count);
        /// <summary>
        /// Tìm kiếm tài sản đánh giá rủi ro
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="cateId">Nhóm tài sản</param>
        /// <param name="stroperator">Ví dụ >=, = ,...</param>
        /// <param name="assetValue">Giá trị của tài sản</param>
        /// <returns></returns>
        IEnumerable<AssetResourceRelateBO> GetAssetReview(string cpnnection, int pageIndex, int pageSize, out int count, Guid? cateId = null, string stroperator = default(string), decimal? assetValue = null);
    }
}
