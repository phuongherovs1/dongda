﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IAssetReviewService : IService<AssetReviewBO>
    {
        AssetReviewBO GetLastByAssetResource(Guid assetID);
        IEnumerable<AssetReviewBO> GetByAssetResource(Guid assetID);
        IEnumerable<AssetReviewBO> GetByAsset(Guid assetId);
    }
}
