﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IAssetTempService : IService<AssetTempBO>
    {
        IEnumerable<AssetTempBO> GetByAssetId(Guid assetId);
    }
}
