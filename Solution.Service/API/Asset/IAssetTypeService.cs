﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IAssetTypeService : IService<AssetTypeBO>
    {
        AssetTypeBO GetByProperty(IEnumerable<Guid> propertyIds);
        AssetTypeBO GetByAsset(Guid assetId);
        Guid Insert(AssetTypeBO data, string typeProperties = "", bool allowSave = true);
        void Update(AssetTypeBO data, string typeProperties = "", bool allowSave = true);
    }
}
