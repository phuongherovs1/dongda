﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IAssetPropertyService : IService<AssetPropertyBO>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        IEnumerable<AssetPropertyBO> GetByAsset(Guid assetId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="assetTypeId"></param>
        /// <returns></returns>
        IEnumerable<AssetPropertyBO> GetByAssetType(Guid assetTypeId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        IEnumerable<AssetPropertyBO> GetContainName(string key);

        IEnumerable<AssetPropertyBO> GetQuery(int pageIndex = 1, int pageSize = 20, string query = "");

        IEnumerable<AssetPropertyBO> QueryFilterAssetType(Guid assetTypeId, Guid currentProperty, int pageIndex = 1, int pageSize = 20, string query = "");

        IEnumerable<AssetPropertyBO> GetPropertyFilterType(IEnumerable<Guid> propertyIds, string query = "");
    }
}
