﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IAssetPropertySettingService : IService<AssetPropertySettingBO>
    {
        /// <summary>
        /// Get property ids by asset id
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        IEnumerable<Guid> GetPropertyIdsByAssetID(Guid assetId);

        IEnumerable<AssetPropertySettingBO> GetByAsset(Guid assetId);
    }
}
