﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IAssetReviewSettingService : IService<AssetReviewSettingBO>
    {
        bool IsConflictValue(Guid id, int from, int to, Resource.QualityReview selectReview);
        bool AllowReviewImportant();
        IEnumerable<AssetReviewSettingBO> SelectByReview(out int count, int pageIndex, int pageSize, Resource.QualityReview selectReview);
        AssetReviewSettingBO GetByValue(int value, Resource.QualityReview selectReview);
        Dictionary<int, string> GetLogicReview();
        Resource.ReviewLimitValue GetLimitValue();
        Resource.ReviewLimitValue GetLimitImportantValue();
    }
}
