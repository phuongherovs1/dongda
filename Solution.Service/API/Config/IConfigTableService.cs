﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IConfigTableService : IService<ConfigTableBO>
    {
        Guid Insert(ConfigTableBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        void Update(ConfigTableBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        IEnumerable<ConfigTableBO> GetAll(int pageIndex, int pageSize, out int count, bool allowDeleted = false);
        IEnumerable<ConfigTableBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count, bool allowDeleted = false);
        string GetByConfigKey(string key, bool allowDeleted = false);
        IEnumerable<ConfigTableBO> GetByConfigByType(string type, bool allowDeleted = false);
        ConfigTableBO GetFormItem();
    }
}
