﻿using System.Data;

namespace iDAS.Service
{
    public interface IInfoSysService
    {
        bool Insert(InfoSysBO infoSysBO);
        bool Update(InfoSysBO infoSysBO);
        bool Disable(int infoSysID);
        InfoSysBO GetInfoSysDetail(int infoSysID);
        DataTable GetAllInfoSystems();
        DataTable GetInfoSystems(int pageIndex, int pageSize, out int count);
    }
}
