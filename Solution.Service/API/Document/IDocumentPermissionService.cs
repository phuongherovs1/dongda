﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentPermissionService : IService<DocumentPermissionBO>
    {
        /// <summary>
        /// Danh sách quyền truy cập theo tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        IEnumerable<DocumentPermissionBO> GetByDocument(Guid documentId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jsonData"></param>
        void Update(string jsonData);

        /// <summary>
        /// Check quyền user và tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        bool CheckPermission(Guid documentId);
    }
}
