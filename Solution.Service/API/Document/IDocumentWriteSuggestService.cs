﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentWriteSuggestService : IService<DocumentWriteSuggestBO>
    {
        IEnumerable<DocumentWriteSuggestResponsibilityBO> GetByCurrentUser(int pageIndex, int pageSize, out int totalCount, iDAS.Service.Common.Resource.DocumentProcessRoleFilterSuggest roleType, Resource.DocumentSuggestStatusType statusType);
        //  Lấy danh sách những phòng ban cho phép thêm mới đề nghị
        List<DepartmentBO> GetDepartmentAllowSuggests();
        // kiểm tra xem người dùng có được thêm mới đề nghị không
        bool AllowSuggestByCurrentUser();
        DocumentWriteSuggestResponsibilityBO GetDetailForDocument(Guid id);
        DocumentWriteSuggestResponsibilityBO GetDetailForSugest(Guid id);
        DocumentWriteSuggestResponsibilityBO GetDetail(Guid id);
        Guid InsertAndSaveRole(DocumentWriteSuggestResponsibilityBO data);
        void UpdateAndSaveRole(DocumentWriteSuggestResponsibilityBO data);
        void SendAndSave(DocumentWriteSuggestResponsibilityBO data);
        bool SendFromSuggest(Guid suggestId);
        bool SendFromReview(DocumentWriteSuggestResponsibilityBO data);
        //Gửi phân công
        bool SendFromApprove(DocumentWriteSuggestResponsibilityBO data);
        IEnumerable<DocumentResponsibilityBO> GetEmployeeRoleBySuggest(Guid suggestId);
        bool RevertDocument(Guid responsibilityId);
        void DeleteByResponsibility(Guid responsibityId, bool allowSave = true);
        void Review(DocumentWriteSuggestResponsibilityBO data);
        void Approve(DocumentWriteSuggestResponsibilityBO data);
        // Chuyển đổi người thực hiện
        bool ChangePerform(DocumentWriteSuggestResponsibilityBO data);
    }
}
