﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentReviewSuggestService : IService<DocumentReviewSuggestBO>
    {
        /// <summary>
        /// Thực hiện lưu đề nghị
        /// </summary>
        /// <param name="item"></param>
        /// <param name="references"></param>
        /// <returns></returns>
        Guid Save(DocumentReviewSuggestBO item, string references);
        /// <summary>
        /// Phê duyệt đề nghị kiểm duyệt tài liệu
        /// </summary>
        /// <param name="review"></param>
        void Approve(DocumentReviewSuggestBO review);

        /// <summary>
        /// Duyệt đề nghị kiểm duyệt
        /// View Kiểm duyệt
        /// </summary>
        /// <returns></returns>
        int CountSuggestReview();

        /// <summary>
        /// Danh sách đề nghị được tạo ra bởi người đăng nhập hệ thống
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="approveStatus">Bao gồm: mới - chưa xem xét, đang duyệt - đã xem, Duyệt đạt , duyệt không đạt</param>
        /// <returns></returns>
        IEnumerable<DocumentReviewSuggestBO> GetAll(int pageIndex, int pageSize, out int count, Common.Resource.ApproveStatus approveStatus);

        /// <summary>
        /// Khởi tạo giá trị mặc định với Id yêu cầu
        /// Trả về thông tin đầy đủ của tài liệu
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        DocumentReviewSuggestBO CreateDefault(Guid? requestId, string id);

        /// <summary>
        /// Gửi đề nghị kiểm duyệt
        /// </summary>
        /// <param name="item"></param>
        Guid Send(DocumentReviewSuggestBO item, string references);

        /// <summary>
        /// Lấy ra đề nghị kiểm duyệt theo yêu cầu soạn thảo
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        DocumentReviewSuggestBO GetByRequestId(Guid requestId);

        /// <summary>
        /// Thu hồi
        /// </summary>
        /// <param name="suggestId"></param>
        void Revert(Guid id);

        void ApproveSuggest(DocumentReviewSuggestBO item);

        /// <summary>
        /// Danh sách gửi đến người phê duyệt là người đang đăng nhập hệ thống
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="approveStatus"></param>
        /// <returns></returns>
        IEnumerable<DocumentReviewSuggestBO> GetByApprover(int pageIndex, int pageSize, out int count, Resource.ApproveStatus approveStatus = Resource.ApproveStatus.All);
    }
}
