﻿namespace iDAS.Service
{
    public interface IDocumentRequestTransferService : IService<DocumentRequestTransferBO>
    {
        /// <summary>
        /// Chuyển phân công soạn thảo cho người khác
        /// </summary>
        /// <param name="item"></param>
        void Send(DocumentRequestTransferBO item);
    }
}
