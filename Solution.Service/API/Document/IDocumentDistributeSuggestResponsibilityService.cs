﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentDistributeSuggestResponsibilityService : IService<DocumentDistributeSuggestResponsibilityBO>
    {
        // Lấy danh sách trách nhiệm liên quan theo đề nghị phân phối
        IEnumerable<DocumentWriteSuggestResponsibilityBO> GetBySuggest(Guid suggestId);
        void InsertResponsibility(DocumentDistributeSuggestResponsibilityBO data);
        DocumentDistributeSuggestResponsibilityBO GetDetailPerform(Guid id);
        void ApproveSave(DocumentDistributeSuggestResponsibilityBO data);
    }
}
