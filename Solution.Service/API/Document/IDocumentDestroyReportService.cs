﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentDestroyReportService : IService<DocumentDestroyReportBO>
    {
        /// <summary>
        /// Lấy thông tin biên bản theo đề nghị hủy
        /// </summary>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        DocumentDestroyReportBO GetReportByDestroySuggestId(Guid destroySuggestId);

        /// <summary>
        /// Tạo/sửa biên bản
        /// </summary>
        /// <param name="report"></param>
        /// <param name="jsonDocument"></param>
        /// <param name="jsonDetail"></param>
        /// <returns></returns>
        Guid Create(DocumentDestroyReportBO report, string jsonDocument = "", string jsonDetail = "", bool allowSave = true);

        /// <summary>
        /// Tạo/sửa biên bản hủy từ yêu cầu hủy
        /// </summary>
        /// <param name="report"></param>
        /// <param name="jsonDetail"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        Guid CreateFromRequest(DocumentDestroyReportBO report, string jsonDetail = "", bool allowSave = true);

        /// <summary>
        /// Tạo mới biên bản cho tài liệu phân phối đề nghị hủy
        /// </summary>
        /// <param name="report"></param>
        /// <param name="jsonDetail"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        Guid CreateReportDistribute(DocumentDestroyReportBO report, string jsonDetail = "", bool allowSave = true);

        /// <summary>
        /// Lấy thông tin biên bản hủy theo yêu cầu hủy
        /// </summary>
        /// <param name="requestDestroyId"></param>
        /// <returns></returns>
        DocumentDestroyReportBO GetReportByRequestDestroyId(Guid requestDestroyId);

        /// <summary>
        /// Lấy tài liệu yêu cầu hủy
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetDocumentByRequestId(Guid requestId);
    }
}
