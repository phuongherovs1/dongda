﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentAdjustSuggestService : IService<DocumentAdjustSuggestBO>
    {
        // Bước 1: Đề nghị (1), Bước 2: Xem xét (1->n)
        // Bước 3: Phê duyệt(1), Bước 4: Phân công(1)
        /// <summary>
        /// Lấy danh sách đề nghị sửa đổi bởi người đăng nhập theo trạng thái và vai trò của người thực hiện
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"> </param>
        /// <param name="roleType">Vai trò</param>
        /// <param name="statusType">Loại trạng thái</param>
        /// <returns></returns>
        IEnumerable<DocumentAdjustSuggestResponsibilityBO> GetByCurrentUser(int pageIndex, int pageSize, out int totalCount, iDAS.Service.Common.Resource.DocumentProcessRoleFilterSuggest roleType, Resource.DocumentSuggestStatusType statusType);
        /// <summary>
        /// Danh sách trách nhiệm lấy theo id đề nghị
        /// </summary>
        /// <param name="suggestId"></param>
        /// <returns></returns>
        IEnumerable<DocumentResponsibilityBO> GetEmployeeRoleBySuggest(Guid suggestId);

        /// <summary>
        /// Thêm mới đề nghị sửa đổi, đồng thời cập nhật trách nhiệm của người sửa đổi cho tài liệu đó
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid InsertAdjustSuggest(DocumentAdjustSuggestResponsibilityBO data);
        /// <summary>
        /// Cập nhật đề nghị sửa đổi đồng thời cập nhật trách nhiệm của người sửa đổi cho tài liệu
        /// </summary>
        /// <param name="data"></param>
        void UpdateAdjustSuggest(DocumentAdjustSuggestResponsibilityBO data);
        /// <summary>
        /// Lấy thông tin đề nghị theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DocumentAdjustSuggestResponsibilityBO GetDetail(Guid id);
        // Chi tiết theo tài liệu
        DocumentAdjustSuggestResponsibilityBO GetDetailForDocument(Guid documentId);
        // Chi tiết
        DocumentAdjustSuggestResponsibilityBO GetDetailForSugest(Guid guid);
        /// <summary>
        /// Gửi đề nghị sửa đổi từ người đề nghị. Có 2 trường hợp:
        /// 1. Từ đề nghị sửa đổi -> Xem xét tài liệu:
        ///     Hoàn thành trách nhiệm của người tạo đề nghị 
        ///     Lưu thông tin người nhận tạo trách nhiệm cho người nhận là xem xét tài liệu và kích hoạt thực hiện 
        /// 2. Từ đề nghị sửa đổi -> Phê duyệt tài liệu:
        ///     Hoàn thành trách nhiệm của người tạo đề nghị
        ///     Lưu thông tin người nhận tạo trách nhiệm cho người nhận là phê duyệt tài liệu và kích hoạt thực hiện 
        /// </summary>
        /// <param name="data"></param>
        bool SendFromSuggest(Guid id);
        /// <summary>
        /// Gửi đề nghị và lưu trữ
        /// </summary>
        /// <param name="data"></param>
        void SendAndSave(DocumentAdjustSuggestResponsibilityBO data);
        /// <summary>
        ///     + Xem xét đề nghị, lưu thông tin xem xét.
        ///     + Khóa không cho phép thu hồi với các nhân sự thực hiện ở công đoạn trước
        ///     + Thực hiện gửi tới công đoạn tiếp theo
        /// </summary>
        /// <param name="data"></param>
        void Review(DocumentAdjustSuggestResponsibilityBO data);
        /// <summary>
        /// Gửi đề nghị sửa đổi từ người xem xét. Có 2 trường hợp:
        /// 1. Từ người xem xét tài liệu -> Xem xét tài liệu:
        ///     Hoàn thành trách nhiệm của người xem xét đề nghị hiện tại
        ///     Lưu thông tin người nhận tạo trách nhiệm cho người nhận là xem xét tài liệu và kích hoạt thực hiện
        /// 2. Từ người xem xét tài liệu -> Phê duyệt tài liệu:
        ///     Hoàn thành trách nhiệm của người xem xét đề nghị hiện tại
        ///     Lưu thông tin người nhận tạo trách nhiệm cho người nhận là phê duyệt tài liệu và kích hoạt thực hiện
        /// </summary>
        /// <param name="data"></param>
        bool SendFromReview(DocumentAdjustSuggestResponsibilityBO data);
        /// <summary>
        /// Phê duyệt đề nghị sửa đổi:
        ///     + Lưu thông tin duyệt
        ///     + Khóa không cho phép thu hồi với các nhân sự thực hiện ở công đoạn trước
        ///     + Thực hiện gửi phân công 
        /// </summary>
        /// <param name="data"></param>
        void Approve(DocumentAdjustSuggestResponsibilityBO data);
        /// <summary>
        /// Gửi đề nghị sửa đổi:Từ người phê duyệt tài liệu -> Phân công tài liệu
        ///     Hoàn thành trách nhiệm của người phê duyệt
        ///     Lưu thông tin người nhận tạo trách nhiệm cho người nhận là phân công tài liệu và kích hoạt thực hiện
        /// </summary>
        /// <param name="data"></param>
        bool SendFromApprove(DocumentAdjustSuggestResponsibilityBO data);
        /// <summary>
        /// Thu hồi tài liệu: Thực hiện kiểm tra xem khả năng thu hồi, Thực hiện thu hồi khôi phục trạng thái hoạt động ở những vai trò trước.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool RevertDocument(Guid id);
        /// Hủy tài liệu: Thực hiện hủy đề nghị, Hủy ở vai trò của đề nghị đó
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool DestroyDocument(Guid id);
        /// <summary>
        /// Xóa đề nghị theo id vai trò đồng thời xóa vai trò thực hiện của bản ghi
        /// </summary>
        /// <param name="responsibityId"></param>
        /// <param name="allowSave"></param>
        void DeleteByReposibility(Guid responsibityId, bool allowSave = true);
        /// <summary>
        /// Chuyển đổi nhân sự thực hiện đề nghị
        /// </summary>
        /// <param name="data"></param>
        bool ChangePerform(DocumentAdjustSuggestResponsibilityBO data);

        /// <summary>
        /// Hàm tạo model mặc định với tham số document Id
        /// thực hiện việc check 
        /// 1. người đề nghị có nằm trong quy trình hay không

        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        DocumentAdjustSuggestResponsibilityBO GetAjustFormItem(Guid documentId);
    }
}
