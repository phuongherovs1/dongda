﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IDocumentRequestService : IService<DocumentRequestBO>
    {

        void RevertPerform(Guid id);
        /// <summary>
        /// Thông tin khung phân công khi click vào item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DocumentRequestBO GetAssignInfoByRequestId(Guid id);

        /// <summary>
        /// Thông tin khung soạn thảo khi click vào item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DocumentRequestBO GetWriteInfoByRequestId(Guid id);



        /// <summary>
        /// Lưu yêu cầu lưu trữ
        /// </summary>
        /// <param name="data"></param>
        /// <param name="request"></param>
        void SaveRequestReceive(DocumentRequestBO data, bool allowSave = true);

        /// <summary>
        /// Danh sách yêu cầu biên soạn/soạn thảo thực hiện bởi người đăng nhập hệ thống
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="status"></param>
        /// <param name="typeWrite">True: Yêu cầu soạn thảo, False: yêu cầu biên soạn</param>
        /// <param name="isAdjust">True: Sửa đổi, False: Tạo mới</param>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetPeformByCurrentUser(int pageIndex, int pageSize, out int count, Resource.RequestStatus status = Resource.RequestStatus.All, bool typeWrite = true, bool isAdjust = false);

        /// <summary>
        /// Get chi tiết yêu cầu hủy
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        DocumentRequestBO GetdataDetailRequest(Guid RequestId);

        IEnumerable<DocumentRequestBO> GetRequestDesTroy();

        DocumentRequestBO GetdataDetailRequestAdjust(Guid RequestId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="typeWrite"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetRequestByCurrentUser(int pageIndex, int pageSize, out int count, bool typeWrite = false, Resource.RequestStatus status = Resource.RequestStatus.All);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        DocumentRequestBO CreateDefault(Guid documentId, string id = "");
        /// <summary>
        /// Gửi yêu cầu soạn thảo ( view biên soạn )
        /// </summary>
        /// <param name="id"></param>
        void Send(Guid id);
        /// <summary>
        /// Phê duyệt kết quả đạt cho yêu cầu
        /// </summary>
        /// <param name="id"></param>
        void Approve(Guid id);
        /// <summary>
        /// Phê duyệt yêu cầu kết quả không đạt
        /// </summary>
        /// <param name="id"></param>
        void Disapprove(Guid id);
        /// <summary>
        /// Thu hồi yêu cầu
        /// </summary>
        /// <param name="id"></param>
        void RevertAssign(Guid id);
        /// <summary>
        /// Gửi yêu cầu sửa đổi
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        void SendRequestAdjust(DocumentRequestBO data, bool allowSave = true);
        /// <summary>
        /// Yêu cầu hủy
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        void InsertRequestDestroy(DocumentRequestBO data, bool allowSave = true);
        /// <summary>
        /// insert yêu cầu sửa đổi
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        void SaveRequestAdjust(DocumentRequestBO data, bool allowSave = true);
        /// <summary>
        /// Get all yêu cầu hủy
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetRequestDestroy(int pageIndex, int pageSize, out int count, Resource.RequestStatus status = Resource.RequestStatus.All);
        /// <summary>
        /// Get all yêu cầu sửa đổi
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetRequestAdjust(int pageIndex, int pageSize, out int count, Resource.RequestProcessStatus status = Resource.RequestProcessStatus.None);
        /// <summary>
        /// Get all yêu cầu soạn thảo
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetRequestWriteNew(int pageIndex, int pageSize, out int count, Resource.RequestProcessStatus type = Resource.RequestProcessStatus.None, Resource.DocumentEmployeeRole status = iDAS.Service.Common.Resource.DocumentEmployeeRole.All);
        /// <summary>
        /// get all yêu cầu phân phối
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="type"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetRequestDistribute(int pageIndex, int pageSize, out int count, Resource.RequestStatus type = Resource.RequestStatus.All, Resource.DocumentEmployeeRole status = iDAS.Service.Common.Resource.DocumentEmployeeRole.All);
        /// <summary>
        /// Lưu yêu cầu soạn thảo
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        void SaveRequestWriteNew(DocumentRequestBO data, bool allowSave = true);
        /// <summary>
        /// Yêu cầu phân phối
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        void SaveRequestDistribute(DocumentRequestBO data, bool allowSave = true);
        /// <summary>
        /// Gửi yêu cầu soạn thảo
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        void SendRequestWriteNew(DocumentRequestBO data, bool allowSave = true);
        /// <summary>
        /// Gửi yêu cầu phân phối
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        void SendRequestDistribute(DocumentRequestBO data, bool allowSave = true);
        /// <summary>
        /// Update yêu cầu hủy
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        void UpdateRequest(DocumentRequestBO data, bool allowSave = true);
        /// <summary>
        /// Xóa yêu cầu soạn thảo
        /// </summary>
        /// <param name="RequestId"></param>
        /// <param name="allowSave"></param>
        void DeleteRequestWriteNew(Guid RequestId, bool allowSave = true);
        /// <summary>
        /// Get chi tiết yêu cầu soạn thảo
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        DocumentRequestBO GetDetailRequestWriteNew(Guid RequestId);
        /// <summary>
        /// Get chi tiết yêu cầu phân phối
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        DocumentRequestBO GetDetailRequestDistribute(Guid RequestId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        void DestroyAssign(Guid id);
        /// <summary>
        /// Lấy yêu cầu lưu trữ
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <param name="typeRole"></param>
        /// <returns></returns>
        DocumentRequestBO GetDataRequestArchives(Guid? DocumentId, Resource.DocumentProcessRole typeRole);
        /// <summary>
        /// Lay yeeu cau huy
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        DocumentRequestBO GetDataRequestDestroy(Guid? DocumentId);

        /// <summary>
        /// Lấy danh sách yêu cầu theo loại trách nhiệm
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="typeRole"></param>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetRequestByTypeRole(int pageIndex, int pageSize, out int count, iDAS.Service.Common.Resource.DocumentProcessRole typeRole);

        #region Thực hiện soạn thảo
        /// <summary>
        /// Danh sách yêu cầu soạn thảo được xuất phát từ một yêu cầu soạn thảo/biên soạn cha
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetRequestWriteByCompilation(Guid id);
        /// <summary>
        /// Thông tin chi tiết tài liệu
        /// id = empty : khởi tạo mặc định trong trường hợp tạo mới
        /// id != empty: lấy thông tin đã tồn tại
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        DocumentRequestBO GetRequestWriteFormItem(string id, Guid parentId);

        DocumentRequestBO SaveRequestWrite(DocumentRequestBO item, bool allowSave = true);
        void SendRequestWrite(DocumentRequestBO item, bool allowSave = true);

        /// <summary>
        /// Người biên soạn hoàn thành
        /// Cập nhật trạng thái => hoàn thành
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        void CompleteByCompile(DocumentRequestBO item, bool allowSave = true);
        #endregion
        /// <summary>
        /// Thu hồi yêu cầu
        /// </summary>
        /// <param name="id"></param>
        void Recover(Guid id);
        /// <summary>
        /// Thay đổi người thực hiện
        /// </summary>
        /// <param name="item"></param>
        void ChangePerform(DocumentRequestBO item);

        /// <summary>
        /// Danh sách yêu cầu ban hành tới tôi
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetRequestPromulgateCurrentUser();
        /// <summary>
        /// Hủy tài liệu
        /// </summary>
        /// <param name="Id"></param>
        void DestroyRequestDocument(Guid Id);
        /// <summary>
        /// Tạo/cập nhật yêu cầu hủy
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        DocumentRequestBO CreateDestroyRequest(Guid documentId, string id = "");
        /// <summary>
        /// Tạo/cập nhật yêu cầu sửa đổi
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        DocumentRequestBO CreateAdjustRequest(Guid documentId, string id = "");
        /// <summary>
        /// Tạo/cập nhật yêu cầu phân phôi
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        DocumentRequestBO CreateDistributeRequest(Guid documentId, string id = "");
        /// <summary>
        /// Lấy danh sách yêu cầu phân phối theo user đăng nhập
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetDistributeRequest();

        #region Phân công thực hiện yêu cầu viết mới =========================================================
        /// <summary>
        /// Phân công tạo mới yêu cầu soạn thảo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Guid SendRequestWriteByAssigner(DocumentRequestBO request);
        /// <summary>
        /// Phân công lưu yêu cầu soạn thảo
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Guid SaveRequestWriteByAssigner(DocumentRequestBO request);

        /// <summary>
        /// Get form item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DocumentRequestBO GetFormRequestWriteByAssigner(string id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        void RevertRequestWriteByAssigner(Guid id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        void CancelRequestWriteByAssigner(Guid id);

        #endregion

        #region Yêu cầu phân công phát sinh từ đề nghị =======================================================
        /// <summary>
        /// Form dữ liệu hiển thị thông tin đề nghị
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DocumentRequestBO GetSuggestToAssignItem(Guid id);
        #endregion

        #region Khung phân công ==============================================================================
        /// <summary>
        /// Danh sách yêu cầu khung phân công
        /// Người phê duyệt: người tạo ra yêu cầu
        /// Người thực hiện: người phân công
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetDataAssignArea();

        IEnumerable<DocumentRequestBO> GetDataAssignAreaTable(int pageIndex, int pageSize, out int count, Resource.DocumentEmployeeRole role = Resource.DocumentEmployeeRole.All, Resource.RequestStatus status = Resource.RequestStatus.All);
        IEnumerable<DocumentRequestBO> GetDataCreateByAssigner(int pageIndex, int pageSize, out int count, Resource.RequestStatus status = Resource.RequestStatus.All);
        /// <summary>
        /// Gửi yêu cầu
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Guid SendAssign(DocumentRequestBO item, bool allowSave = true);
        #endregion

        #region Khung soạn thảo ==============================================================================
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="role"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetDataWritenAreaTable(int pageIndex, int pageSize, out int count, Resource.DocumentEmployeeRole role = Resource.DocumentEmployeeRole.All, Resource.RequestStatus status = Resource.RequestStatus.All);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetDataCreateByCompilator(int pageIndex, int pageSize, out int count, Resource.RequestStatus status = Resource.RequestStatus.All);
        /// <summary>
        /// Danh sách yêu cầu soạn thảo
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentRequestBO> GetDataWritenArea();
        /// <summary>
        /// Người biên soạn thực hiện gửi đề nghị ban hành(đề nghị kiểm tra)
        /// </summary>
        /// <param name="responsibility"></param>
        void SendPublishSuggest(DocumentResponsibilityBO responsibility);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        Guid SaveAssign(DocumentRequestBO data, bool allowSave = true);
        /// <summary>
        /// Hoàn thành yêu cầu soạn thảo và gửi kết quả cho người biên soạn
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        void CompleteWrite(DocumentRequestBO data, bool allowSave = true);
        #endregion

        #region Tạo tài liệu =================================================================================

        DocumentRequestBO GetFormWriteByComiplator(string id);
        /// <summary>
        /// Lưu kết quả tạo tài liệu 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Guid SaveDocumentByCompilator(DocumentRequestBO request);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        DocumentRequestBO SendDocumentByCompilator(DocumentRequestBO request);
        /// <summary>
        /// Hoàn thành soạn thảo tài liệu. gửi kết quả đến người duyệt
        /// </summary>
        /// <param name="request"></param>
        void CompleteDocumentByCompilator(DocumentRequestBO request);

        void DeleteDocumentByRequestId(Guid requestId);
        #endregion
    }
}