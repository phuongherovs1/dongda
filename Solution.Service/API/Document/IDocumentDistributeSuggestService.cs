﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentDistributeSuggestService : IService<DocumentDistributeSuggestBO>
    {
        /// <summary>
        /// Khởi tạo mặc định đối tượng với tài liệu đã có
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        DocumentDistributeSuggestBO CreateDefault(Guid documentId, string id = default(string));

        /// <summary>
        /// Đề nghị phân phối
        /// View soạn thảo
        /// </summary>
        /// <returns></returns>
        int CountSuggest();

        /// <summary>
        /// Duyệt đề nghị phân phối
        /// View Kiểm duyệt
        /// </summary>
        /// <returns></returns>
        int CountApproveSuggest();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<DocumentDistributeSuggestBO> GetAll(int pageIndex, int pageSize, out int count, Common.Resource.ApproveStatus status = Common.Resource.ApproveStatus.All);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Guid Send(DocumentDistributeSuggestBO item);
        /// <summary>
        /// Thu hồi
        /// </summary>
        /// <param name="id"></param>
        void updateSuggestDistribute(Guid id);
        /// <summary>
        /// Thu hồi đề nghị phân phối tài liệu
        /// </summary>
        /// <param name="distributeSuggest"></param>
        void Revert(DocumentDistributeSuggestBO distributeSuggest);
        /// <summary>
        /// Danh sách đề nghị phân phối được gửi đến người đang đăng nhập hệ thống
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="approveStatus"></param>
        /// <returns></returns>
        IEnumerable<DocumentDistributeSuggestBO> GetByApprover(int pageIndex, int pageSize, out int count, Resource.ApproveStatus approveStatus = Resource.ApproveStatus.All);
        IEnumerable<EmployeeBO> GetPromulgateEmployees(int pageIndex, int pageSize, out int totalCount, string fillter, string departmentId);
        void SendToPromulgate(DocumentDistributeSuggestBO item);
        DocumentDistributeSuggestBO GetDetailPerform(Guid id);
        // Kiểm tra tồn tại vai trò đề nghị
        bool ExitsResponsibility(Guid suggestId);
        void AcceptDistributte(Guid id);
    }
}
