﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentDestinationService : IService<DocumentDestinationBO>
    {
        /// <summary>
        /// Đưa ra danh sách nơi nhận theo tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        IEnumerable<DocumentDestinationBO> GetByDocument(Guid documentId);

        /// <summary>
        /// Danh sách nơi nhân theo tài liệu
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="count"></param>
        /// <param name="documentId"></param>
        /// <returns></returns>
        IEnumerable<DocumentDestinationBO> GetByDocumentId(int pageSize, int pageIndex, out int count, Guid? documentId);
    }
}
