﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentDepartmentPermissionService : IService<DocumentDepartmentPermissionBO>
    {
        /// <summary>
        /// Kiểm tra quyền tạo danh mục của current User với phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool CheckRoleSettingCategory(Guid departmentId, Guid userId);
        bool CheckRoleSettingProcess(Guid departmentId, Guid userId);
        bool CheckRoleArchive(Guid departmentId, Guid userId);
        bool CheckRoleSettingCategory(Guid departmentId);
        bool CheckRoleSettingProcess(Guid departmentId);
        bool CheckRoleArchive(Guid departmentId);
        /// <summary>
        /// Lấy danh sách phòng ban mà user có quyền cập nhật
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<Guid> GetDepartmentRoleSettingCategoryByUser();
        IEnumerable<Guid> GetDepartmentRoleSettingProcessByUser();
        IEnumerable<Guid> GetDepartmentRoleArchiveByUser();
        /// Danh sách
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<DocumentDepartmentPermissionBO> GetAll(int pageSize, int pageIndex, out int count, Guid departmentId);
        void UpdatePermission(DocumentDepartmentPermissionBO item);

        void InsertPermission(DocumentDepartmentPermissionBO item, string strData);
        void UpdateRecordPermission(Guid id, bool isSettingProcess, bool isSettingCategory, bool isArchive);
    }
}
