﻿
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IDocumentService : IService<DocumentBO>
    {

        IEnumerable<FileBO> GetFileByDocumentId(Guid id);
        IEnumerable<DocumentBO> GetByCategory(Guid categoryId, int pageIndex, int pageSize, out int count, string filter, Resource.DocumentStatus status);

        /// <summary>
        /// Get thông tin tài liệu con
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DocumentBO GetChild(Guid id);

        /// <summary>
        /// Lấy danh sách yêu cầu biên soạn chưa thực hiện theo người đăng nhập
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentBO> GetDocumentRequestByCurrentUser();
        /// <summary>
        /// Lấy danh sách các tài liệu đang soạn thảo
        /// </summary>
        /// <returns></returns>
        /// 
        IEnumerable<DocumentBO> GetDocumentWriteByCurrentUser();
        IEnumerable<DocumentBO> GetAllDocument(int pageIndex, int pageSize, out int count, string filterName = default(string), Common.Resource.EDocumentStatus status = 0);
        IEnumerable<DocumentBO> GetDocumentForCurrentUser(int pageIndex, int pageSize, out int count, string filterName, Common.Resource.EDocumentStatus status = 0);
        /// <summary>
        /// Danh sách tài liệu theo thư mục và được phân phối tới tôi
        /// 
        /// </summary>
        /// <param name="count"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="distribute"></param>
        /// <returns></returns>
        IEnumerable<DocumentBO> GetAll(Guid categoryId, int pageIndex, int pageSize, out int count, string filterName = "", bool distribute = true);
        /// <summary>
        /// Lấy ra chức danh có quyền phê duyệt theo tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        Guid GetByPublishApprove(Guid documentId);
        /// <summary>
        /// Đưa ra danh sách tài liệu tham chiếu đến nó
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        IEnumerable<DocumentBO> GetReferencesByDocument(Guid documentId, int pageIndex, int pageSize, out int totalCount);

        /// <summary>
        /// Tài liệu tham chiếu 
        /// Chọn tài liệu tham chiếu, hệ thống cho phép chọn tài liệu tham chiếu có trạng thái ban hành và được phân phối đến người đăng nhập
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentBO> GetReferences();
        List<DocumentRoleObjectBO> GetRoleByText(string names, string values);


        /// <summary>
        /// Cập nhật tài liệu vào danh muc
        /// </summary>
        /// <returns></returns>
        void InsertDocumentCategory(DocumentCategoryBO item, bool allowSave = true);
        /// <summary>
        /// Xoa tai lieu khoi danh muc phan phoi
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        void DeleteDocumentCategoryDistribute(DocumentCategoryBO item, bool allowSave = true);
        /// <summary>
        /// Cập nhật tài liệu vào danh muc phân phối
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        void InsertDocumentCategoryDistribute(DocumentCategoryBO item, bool allowSave = true);
        /// <summary>
        /// Xóa tài liệu khỏi danh mục
        /// </summary>
        /// <param name="PublishId"></param>
        void DeleteDocumentCategory(Guid DocumentId);

        void RemoveFromCategory(Guid documentId);
        /// <summary>
        /// Tạo tài liệu đã ban hành
        /// </summary>
        /// <param name="item"></param>
        Guid CreatePublishDocument(DocumentBO item, bool allowSave = true);
        /// <summary>
        /// Cập nhật tài liệu đã ban hành
        /// </summary>
        /// <param name="item"></param>
        void UpdateDocument(DocumentBO item, bool allowSave = true);
        /// <summary>
        /// Laay document chua yeu cau huy
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentBO> GetAllByRequestDestroy();
        /// <summary>
        ///Laay document chua yeu cau phan phoi
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentBO> GetAllByRequestDistribute();
        /// <summary>
        /// Laay document chua yeu cau sua doi
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentBO> GetAllByRequestAdjust(int pageIndex, int pageSize, out int count, bool isDestroy = false, string filter = "", bool isDistribute = true, string departmentId = "");

        /// <summary>
        /// Lấy danh sách tài liệu đã ban hành
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="isDestroy"></param>
        /// <param name="filter">Tìm kiếm theo tên tài liệu hoặc mã tài liệu</param>
        /// <param name="isDistribute">True: tài liệu phân phối đến tôi, False: tài liệu không phân phối đến tôi</param>
        /// <param name="departmentId">Tài liệu theo phòng ban</param>
        /// <returns></returns>
        IEnumerable<DocumentBO> GetDocumentPublish(int pageIndex, int pageSize, out int count, bool isDestroy = false, string filter = "", bool isDistribute = true, string departmentId = "");

        /// <summary>
        /// Tạo đối tượng mặc định
        /// </summary>
        /// <param name="defaultCategory"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        DocumentBO GetFormItem(Guid defaultCategory, string id, Guid departmentId);
        /// <summary>
        /// Get du lieu phan phoi
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentBO> GetDistribute();
        /// <summary>
        /// Danh sách tài liệu phân phối theo danh mục phân phối
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        IEnumerable<DocumentBO> GetDistributeByCategory(Guid categoryId, int pageIndex, int pageSize, out int count, string filter, Resource.DocumentStatus status);

        /// <summary>
        /// Danh sách tài liệu đã ban hành
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        IEnumerable<DocumentBO> GetdataPublish(int pageIndex, int pageSize, out int count, string search);

        /// <summary>
        /// Thêm mới tài liệu
        /// Cập nhật: Bổ xung danh sách tài liệu liên quan
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        Guid InsertDocument(DocumentBO item, bool allowSave = true);

        IEnumerable<DocumentBO> GetDocumentDistributeToCurrentUser(int pageIndex, int pageSize, out int count, string key, string departmentId);
        void UpdateDocumentCategory(Guid Id, Guid CategoryId, bool allowSave = true);

        /// <summary>
        /// Thông tin chi tiết tài liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DocumentBO GetDetail(Guid id);


        IEnumerable<DocumentBO> GetByCode(string documentCode);


        string Compare(Guid src, Guid dest);
    }
}