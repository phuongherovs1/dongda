﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentExternalApplySuggestService : IService<DocumentExternalApplySuggestBO>
    {
        /// <summary>
        /// Lấy thông tin tài liệu bên ngoài đã ban hành
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        DocumentExternalApplySuggestBO CreateDefault(string documentId);

        /// <summary>
        /// Gửi thông tin đề nghị ban hành áp dụng đối với các tài liệu bên ngoài
        /// </summary>
        /// <param name="applySuggest"></param>
        /// <returns></returns>
        Guid Send(DocumentExternalApplySuggestBO applySuggest);

        /// <summary>
        /// Phê duyệt
        /// Nếu người nhận là người xem xét thì có thể gửi người xem xét khác
        /// Nếu người nhận là người phê duyệt thì kết thúc
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid NextSend(DocumentExternalApplySuggestBO data);

        /// <summary>
        /// Danh sách những nhân sự kiểm tra hoặc phê duyệt
        /// </summary>
        /// <param name="applySuggestId"></param>
        /// <returns></returns>
        IEnumerable<DocumentExternalApplySuggestBO> GetEmployeesReview(Guid applySuggestId);

        /// <summary>
        /// Phê duyệt
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid Approve(DocumentExternalApplySuggestBO data);
    }
}
