﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public interface IDocumentWriteSuggestPerformService : IService<DocumentWriteSuggestPerformBO>
    {
        IEnumerable<DocumentWriteSuggestPerformBO> GetByCurrentUser();
        IEnumerable<DocumentWriteSuggestPerformBO> GetBySuggest(Guid suggestId);
        IEnumerable<DocumentWriteSuggestPerformBO> GetEmployeeRoleBySuggest(Guid suggestId);
        DocumentWriteSuggestPerformBO GetDetail(Guid guid);
    }
}
