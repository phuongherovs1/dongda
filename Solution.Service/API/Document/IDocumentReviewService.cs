﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentReviewService : IService<DocumentReviewBO>
    {
        /// <summary>
        /// Phê duyệt đề nghị kiểm duyệt tài liệu
        /// </summary>
        /// <param name="review"></param>
        void Approve(DocumentReviewBO review);

        /// <summary>
        /// Duyệt đề nghị kiểm duyệt
        /// View Kiểm duyệt
        /// </summary>
        /// <returns></returns>
        int CountSuggestReview();

        /// <summary>
        /// Danh sách đề nghị được tạo ra bởi người đăng nhập hệ thống
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="approveStatus">Bao gồm: mới - chưa xem xét, đang duyệt - đã xem, Duyệt đạt , duyệt không đạt</param>
        /// <returns></returns>
        IEnumerable<DocumentReviewBO> GetAll(int pageIndex, int pageSize, out int count, Common.Resource.ApproveStatus approveStatus);

        /// <summary>
        /// Khởi tạo giá trị mặc định với documentId
        /// Trả về thông tin đầy đủ của tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        DocumentReviewBO CreateDefault(Guid documentId, string id);

        /// <summary>
        /// Gửi đề nghị kiểm duyệt
        /// </summary>
        /// <param name="item"></param>
        Guid Send(DocumentReviewBO item);
    }
}
