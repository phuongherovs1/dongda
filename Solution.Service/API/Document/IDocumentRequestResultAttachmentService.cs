﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentRequestResultAttachmentService : IService<DocumentRequestResultAttachmentBO>
    {
        IEnumerable<Guid> GetFileIds(Guid documentRequestResultId);
    }
}
