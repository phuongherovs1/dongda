﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentHistoryService : IService<DocumentHistoryBO>
    {
        /// <summary>
        /// Danh sách lịch sử sửa đổi của tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        IEnumerable<DocumentHistoryBO> GetByDocumentId(Guid documentId);
    }
}
