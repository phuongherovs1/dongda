﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentDestroySuggestDetailService : IService<DocumentDestroySuggestDetailBO>
    {
        /// <summary>
        /// Lưu thông tin tài liệu đề nghị hủy
        /// </summary>
        /// <param name="documentIds"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        IEnumerable<Guid> InsertRange(string documentIds, Guid destroySuggestId, bool allowSave = true);

        /// <summary>
        /// Lấy thông tin tài liệu chi tiết theo destroySuggestId hoặc requestId
        /// </summary>
        /// <param name="destroySuggestId"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        IEnumerable<DocumentDestroySuggestDetailBO> GetByDestroySuggestId(Guid destroySuggestId);

        /// <summary>
        /// Danh sách tài liệu đề nghị hủy
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        IEnumerable<DocumentDestroySuggestDetailBO> GetAll(int pageIndex, int pageSize, out int count, Guid destroySuggestId);

        /// <summary>
        /// Xóa chi tiết đề nghị hủy
        /// </summary>
        /// <param name="suggestId"></param>
        /// <param name="allowSave"></param>
        void DeleteDetail(Guid suggestId, bool allowSave = true);

        /// <summary>
        /// Lấy danh sách DocumentId của đề nghị
        /// </summary>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        IEnumerable<Guid> GetDocumentIdBySuggestId(Guid destroySuggestId);

        /// <summary>
        /// Update tài liệu chi tiết của đề nghị hủy
        /// Dùng khi tạo biên bản hủy
        /// </summary>
        /// <param name="jsonData"></param>
        void UpdateRange(string jsonData);
    }
}
