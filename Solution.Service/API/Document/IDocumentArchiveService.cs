﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentArchiveService : IService<DocumentArchiveBO>
    {
        /// <summary>
        /// Lấy vị trí lưu trữ của tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        DocumentArchiveBO GetByDocument(Guid documentId);
        /// <summary>
        /// Lấy danh sách tài liệu lưu trữ theo user đăng nhập
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<DocumentArchiveBO> GetAll(int pageIndex, int pageSize, out int count);
        void SaveArchive(Guid pesponsibilityId, DocumentArchiveBO item);
        void UpdateArchive(Guid pesponsibilityId, DocumentArchiveBO item);
    }
}
