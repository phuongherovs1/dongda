﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentDestroySuggestResponsibitityService : IService<DocumentDestroySuggestResponsibitityBO>
    {
        /// <summary>
        /// Lấy thông tin đề nghị hủy từ ResponsibilityId
        /// </summary>
        /// <param name="responsibilityId"></param>
        /// <returns></returns>
        DocumentDestroySuggestResponsibitityBO GetDestroySuggestByResponsibilityId(Guid responsibilityId);
        DocumentDestroySuggestBO GetDataDestroySuggestByResponsibilityId(Guid id);
        /// <summary>
        /// Lấy danh sách trách nhiệm
        /// </summary>
        /// <param name="suggestId"></param>
        /// <returns></returns>
        IEnumerable<DocumentDestroySuggestResponsibitityBO> GetEmployeeRoleByDestroySuggest(Guid suggestId);

        /// <summary>
        /// Phê duyệt xem xét
        /// </summary>
        /// <param name="data"></param>
        void Review(DocumentDestroySuggestResponsibitityBO data);

        /// <summary>
        /// Gửi bước tiếp theo từ view xem xét
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool SendFromReview(DocumentDestroySuggestResponsibitityBO data);

        /// <summary>
        /// Gửi cho người kiểm tra hoặc phê duyệt (không theo quy trình)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool SendReviewDestroySuggest(DocumentDestroySuggestResponsibitityBO data);

        /// <summary>
        /// Phê duyệt
        /// </summary>
        /// <param name="data"></param>
        void Approve(DocumentDestroySuggestResponsibitityBO data);

        void SendApproveDestroySuggest(DocumentDestroySuggestResponsibitityBO data);
        void FormPublishDestroySuggestDocument(Guid Id);
        /// <summary>
        /// Thu hồi
        /// </summary>
        /// <param name="id"></param>
        void Revert(Guid id);
        void RevertResponsibility(Guid id);

        /// <summary>
        /// CHuyển người khác thực hiện
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool ChangePerform(DocumentDestroySuggestResponsibitityBO data);

        /// <summary>
        /// Hủy vai trò hiện tại của đề nghị
        /// </summary>
        /// <param name="id"></param>
        void Destroy(Guid id);

        /// <summary>
        /// Danh sách trách nhiệm liên quan theo quy trình ban hành
        /// </summary>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        IEnumerable<DocumentDestroySuggestResponsibitityBO> GetEmployeeRoleByPublishProcess(Guid destroySuggestId);
    }
}
