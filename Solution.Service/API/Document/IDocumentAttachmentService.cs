﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public interface IDocumentAttachmentService : IService<DocumentAttachmentBO>
    {
        /// <summary>
        /// Đưa ra danh sách Id đính kèm theo tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        IEnumerable<Guid> GetByDocument(Guid documentId);

        /// <summary>
        /// Thực hiện cập nhật đính kèm khi của tài liệu
        /// </summary>
        /// <param name="fileUpload"></param>
        /// <param name="documentId"></param>
        /// <param name="allowSave"></param>
        void Update(FileUploadBO fileUpload, Guid documentId, bool allowSave = true);
        IQueryable<Guid> GetIds(Guid documentId, IEnumerable<Guid> fileIds);

    }
}
