﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentSummaryService
    {
        List<PieChartBO> DocumentStatusAnalytic(Guid departmentId);
        List<PieChartBO> DocumentTypeAnalytic(Guid departmentId);
        List<PieChartBO> DocumentApproveActorAnalytic();
        List<PieChartBO> DocumentPubishActorAnalytic();
        List<PieChartBO> DocumentArchiveActorAnalytic();
        IEnumerable<DocumentDasboardApproveBO> GetDocumentApproveByCurrentUser();
        IEnumerable<DocumentDasboardApproveBO> GetDocumentPaggingApproveByCurrentUser(int pageIndex, int pageSize, out int count, string filterName = default(string), Common.Resource.EDocumentStatus status = 0);
        IEnumerable<DocumentDasboardApproveBO> GetPromulgate();
        IEnumerable<DocumentDasboardApproveBO> GetPaggingPromulgate(int pageIndex, int pageSize, out int count, string filterName = default(string), Common.Resource.EDocumentStatus status = 0);
        IEnumerable<DocumentDasboardApproveBO> GetArchive();
        IEnumerable<DocumentDasboardApproveBO> GetPaggingArchive(int pageIndex, int pageSize, out int count, string filterName = default(string), Common.Resource.EDocumentStatus status = 0);
    }
}
