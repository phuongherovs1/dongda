﻿namespace iDAS.Service
{
    public interface IDocumentSuggestCheckService : IService<DocumentSuggestCheckBO>
    {
        void InsertCheckSuggest(DocumentSuggestCheckBO item, bool allowSave = true);
    }
}
