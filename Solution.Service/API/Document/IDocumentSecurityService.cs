﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentSecurityService : IService<DocumentSecurityBO>
    {
        /// <summary>
        /// Tìm kiếm danh sách mức độ bảo mật theo tên
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        IEnumerable<DocumentSecurityBO> GetContain(string name);
        /// <summary>
        /// Kiểm tra mức độ bảo mật đã tồn tại chưa ?
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool CheckExist(string name);
        /// <summary>
        /// Thêm mới mức độ bảo mật. Thêm vào mức độ bảo mật cho các phòng ban
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>

    }
}
