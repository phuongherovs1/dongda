﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentDistributeService : IService<DocumentDistributeBO>
    {

        /// <summary>
        /// Insert Distribute 
        /// </summary>
        /// <param name="Distribute"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        Guid InsertDistribute(DocumentDistributeBO data, bool allowSave = true);
        /// <summary>
        /// Đếm số lượng phân phối cho đối tượng
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="destination"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        int CountByDestination(Guid documentId, Guid destination, Common.Resource.DestinationType destinationType);

        /// <summary>
        /// Hàm khởi tạo mặc định phân phối cho tài liệu xác định
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        DocumentDistributeBO CreateDefault(Guid documentId);

        /// <summary>
        /// Đưa ra danh sách tài liệu phân phối đến người đang đăng nhập hệ thống
        /// Bao gồm phòng ban, chức danh, cá nhân
        /// </summary>
        /// <returns></returns>
        IEnumerable<Guid> GetDocumentIdsDistribute();
        /// <summary>
        /// Delete distribute
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        void DeleteDistribute(Guid DistributeId, bool allowSave = true);
        /// <summary>
        /// Getall Distribute
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<DocumentDistributeBO> GetAll(Guid DocumentId, int pageIndex, int pageSize, out int count);

        /// <summary>
        /// Lưu nơi nhận phân phối khi duyệt đề nghị ban hành
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="jsonData"></param>
        IEnumerable<Guid> SaveDistribute(Guid documentId, string jsonData = "", bool allowSave = true);
        /// <summary>
        /// Lấy chi tiết nơi phân phối
        /// </summary>
        /// <param name="DistributeId"></param>
        /// <returns></returns>
        DocumentDistributeBO GetDetailDistribute(Guid? DistributeId);

        IEnumerable<DocumentPermissionBO> GetPermissionData();

    }
}
