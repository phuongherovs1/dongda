﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentSecurityDepartmentService : IService<DocumentSecurityDepartmentBO>
    {
        /// <summary>
        /// Lấy danh sách mức độ bảo mật theo phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        IEnumerable<DocumentSecurityDepartmentBO> GetByDepartment(Guid departmentId);
        /// <summary>
        /// Lấy mức độ bảo mật cho phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <param name="securityId"></param>
        /// <returns></returns>
        DocumentSecurityDepartmentBO GetByDepartmentSecurity(Guid departmentId, Guid securityId);
    }
}
