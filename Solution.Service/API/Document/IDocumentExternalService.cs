﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentExternalService : IService<DocumentExternalBO>
    {
        /// <summary>
        /// Tìm kiếm danh sách cá nhân, tổ chức bên ngoài có chứa tên
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        IEnumerable<DocumentExternalBO> GetContainName(string name);

        /// <summary>
        /// Kiểm tra đã tồn tại cá nhân, tổ chứ bên ngoài có tên
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool checkExist(string name);

    }
}
