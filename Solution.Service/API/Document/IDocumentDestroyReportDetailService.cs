﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentDestroyReportDetailService : IService<DocumentDestroyReportDetailBO>
    {
        /// <summary>
        /// Danh sách chi tiết của biên bản
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="reportId"></param>
        /// <returns></returns>
        IEnumerable<DocumentDestroyReportDetailBO> GetByReportId(int pageIndex, int pageSize, out int count, Guid reportId);

        /// <summary>
        /// Tạo chi tiết của biên bản
        /// </summary>
        /// <param name="jsonData"></param>
        /// <param name="reportId"></param>
        /// <returns></returns>
        IEnumerable<Guid> Create(Guid reportId, string jsonData = "");
    }
}
