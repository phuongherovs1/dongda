﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentResponsibilityService : IService<DocumentResponsibilityBO>
    {
        /// <summary>
        /// Lấy danh sách trách nhiệm theo người phê duyệt và người tạo
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentResponsibilityBO> GetForCheckApproval();
        DocumentResponsibilityBO GetDetail(Guid id);
        void ChangeRole(DocumentResponsibilityBO item);
        void Cancel(Guid id);
        void RevertApproval(Guid id);
        void RevertRole(Guid id);
        void UpdateCompleteAndLock(Guid id, bool isComplete = true, bool allowSave = true);
        void Approval(DocumentResponsibilityBO data, bool allowSave = true);
        IEnumerable<DocumentResponsibilityBO> GetForPromulgate();
        IEnumerable<DocumentResponsibilityBO> GetForArchive();
        IEnumerable<DocumentResponsibilityBO> GetByDocument(Guid documentId, int pageIndex, int pageSize, out int totalCount);
        void InsertResponsibility(DocumentResponsibilityBO item, bool allowSave = true);
        void UpdateResponsibility(DocumentResponsibilityBO item);
        DocumentResponsibilityBO GetResponsibilitybyPublish(Guid documentId);
        void UpdateArchiveComplete(Guid id, bool isComplete = true, bool allowSave = true);
        void SendRequestArchive(DocumentResponsibilityBO item);
    }
}
