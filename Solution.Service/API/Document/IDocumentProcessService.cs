﻿using System;
using System.Collections.Generic;


namespace iDAS.Service
{
    public interface IDocumentProcessService : IService<DocumentProcessBO>
    {
        /// <summary>
        /// Lấy danh sách trách nhiệm theo quy trình của phòng ban
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="departmentId">Id phòng ban</param>
        /// <param name="process">quy trình</param>
        /// <returns></returns>
        IEnumerable<DocumentProcessBO> GetAll(int pageIndex, int pageSize, out int count, Guid departmentId, int process);
        bool UpdateOrder(bool up, Guid roleId);
        int GetMaxOrder(Guid departmentId, int process, int roleType);
        IEnumerable<Guid> GetTitleIds(int process, int roletype);
        IEnumerable<Guid> GetEmployeeIds(int process, int roletype);
        /// <summary>
        /// Kiểm tra xem phòng ban có quy trình hay chưa
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        bool CheckExitProcess(Guid? departmentId, int process);
        bool CheckExitRoleInProcess(Guid? departmentId, int process, int roletype);

        /// <summary>
        /// Kiểm tra người đăng nhập có quy trình không
        /// </summary>
        /// <param name="departmentId"></param>
        /// <param name="employeeId"></param>
        /// <param name="process"></param>
        /// <param name="roletype"></param>
        /// <returns></returns>
        bool CheckExitEmployeeInProcess(Guid? departmentId, Guid? employeeId, int process, int roletype);

        DocumentProcessBO GetByOrder(Guid departmentId, int process, int order);
        /// <summary>
        /// Lấy trách nhiệm tiếp theo của quy trình
        /// </summary>
        /// <param name="departmentId"></param>
        /// <param name="process"></param>
        /// <param name="order"></param>
        /// <param name="roleNext"></param>
        void GetRoleNext(Guid departmentId, int process, int order, ref DocumentProcessBO roleNext, int? startRole = null);
        DocumentProcessPerformBO GetProcess(Guid departmentId, iDAS.Service.Common.Resource.DocumentTypeProcess process);
        bool CheckRoleTypeExits(iDAS.Service.Common.Resource.DocumentProcessRole roleType);
        IEnumerable<DepartmentBO> GetDepartmentAllowRequest(iDAS.Service.Common.Resource.DocumentProcessRole roleType);
        void InsertProcess(DocumentProcessBO item);
        void UpdateProcess(DocumentProcessBO item);
        void DeleteProcess(Guid id);
        DocumentProcessBO GetRecord(Guid departmentId, int process, int roletype);
        void Copy(Guid departmentId, Guid departmentCopyId, int process);
        IEnumerable<EmployeeBO> GetEmployeeByRole(int pageIndex, int pageSize, out int count, Guid departmentId, int process, int roleType);

        /// <summary>
        /// Đưa ra danh sách phòng ban với vai trò của người dùng
        /// Trong trường hợp phòng ban không có quy trình => đưa ra tất cả phòng ban
        /// </summary>
        /// <param name="roleType"></param>
        /// <returns></returns>
        IEnumerable<DepartmentBO> GetDeparmentByRoleType(int roleType);
        // Lấy trách nhiệm tiếp theo của quy trình phân phối
        DocumentProcessBO GetDistributeRoleNext(Guid departmentId, int order = 0);
        List<DocumentProcessComboBO> GetNextRoleType(Guid departmentId, int typeProcess = 0, int? roleType = 0, int order = 0);

        #region Người phân công
        /// <summary>
        /// Danh sách phòng ban với vai trò phân công của người đăng nhập hệ thống
        /// - Lấy ra danh sách phòng ban của nó nếu như không có quy trình
        /// - Danh sách phòng phan của nó với vai trò nó là người phân công
        /// </summary>
        /// <returns></returns>
        IEnumerable<DepartmentBO> GetDepartmentByAssignRole();
        #endregion

        /// <summary>
        /// Kiểm tra người đó có vai trò gì với phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        bool CheckCurrentUserRole(Guid departmentId, int typeProcess, int roleType);
    }
}
