﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentPublishService : IService<DocumentPublishBO>
    {


        /// <summary>
        /// Đưa ra thông tin được duyệt ban hành cho tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        DocumentPublishBO GetByDocumentId(Guid documentId);


        /// <summary>
        /// Design by TuNT, Ask him descript to here
        /// </summary>
        /// <param name="data"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        Guid Insert(DocumentPublishBO data, string jsonData);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        DocumentPublishBO CreateDefault(Guid documentId);

        /// <summary>
        /// Danh sách tài liệu ban hành dùng cho form tạo đề nghị hủy
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="isCreate">True: không load chi tiết tài liệu của đề nghị hủy, False: ngược lại</param>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        IEnumerable<DataDocumentDestroyBO> GetAllForDestroySuggest(int pageIndex, int pageSize, out int count, bool isCreate = true, string destroySuggestId = "");

        /// <summary>
        /// Danh sách tài liệu đã ban hành dùng cho form tạo đề nghị phân phối
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="departmentId"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        IEnumerable<DocumentPublishBO> GetAllForDistributeSuggest(int pageIndex, int pageSize, out int count, string filter = "", string departmentId = "");
        /// <summary>
        /// Lưu tài liệu ban hành
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        void IsertPublish(DocumentPublishBO data, bool allowSave = true);
        /// <summary>
        /// Get dữ liệu đã ban hành
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="search">Tìm kiếm theo tên hoặc mã tài liệu</param>
        /// <returns></returns>
        IEnumerable<DocumentPublishBO> GetdataPublish(int pageIndex, int pageSize, out int count, string search);
        /// <summary>
        /// Get lần ban hành
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="DocumentCode"></param>
        /// <returns></returns>
        IEnumerable<DocumentPublishBO> GetCoutPublish(int pageIndex, int pageSize, out int count, Guid Id);
        /// <summary>
        /// Lây chi tiết tải liệu yêu cầu ban hành
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        DocumentPublishBO GetRequestPromulgateByUser(Guid Id);
    }
}
