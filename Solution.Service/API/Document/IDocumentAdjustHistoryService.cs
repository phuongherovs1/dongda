﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentAdjustHistoryService : IService<DocumentAdjustHistoryBO>
    {
        IEnumerable<DocumentAdjustHistoryBO> GetByDocumentId(Guid documentId);
    }
}
