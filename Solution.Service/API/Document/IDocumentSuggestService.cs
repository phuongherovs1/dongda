﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public interface IDocumentSuggestService : IService<DocumentSuggestBO>
    {
        IEnumerable<DocumentSuggestBO> GetByCurrentUser(int pageIndex, int pageSize, out int totalCount, iDAS.Service.Common.Resource.DocumentProcessRole roleType, Resource.DocumentSuggestStatusType statusType, Resource.DocumentSuggestType suggestType);
        /// <summary>
        /// Lấy thông tin chi tiết của đề nghị
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="allowGetSuggest">cho phép lấy thông tin chi tiết cho người đề nghị</param>
        /// <returns></returns>
        DocumentSuggestBO GetDetail(Guid Id, bool allowGetSuggest = true, bool allowGetDocument = true);
        Guid InsertAndSaveRole(DocumentSuggestBO data, bool allowSave = true);
        void UpdateAndSaveRole(DocumentSuggestBO data, bool allowSave = true);
        void UpdateStatus(Guid id, int? statusType, int? order, bool allowSave = true);
        void Cancel(Guid id);
        int? GetStatus(Guid id);
    }
}
