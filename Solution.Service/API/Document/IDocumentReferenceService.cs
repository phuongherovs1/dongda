﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentReferenceService : IService<DocumentReferenceBO>
    {
        /// <summary>
        /// Form thông tin tài liệu tham chiếu
        /// </summary>
        /// <param name="id"></param>
        /// <param name="documentId"></param>
        /// <returns></returns>
        DocumentReferenceBO GetFormItem(string id, Guid documentId);
        IEnumerable<DocumentReferenceBO> GetByDocumentId(Guid documentId, int pageIndex, int pageSize, out int count);

        Guid Save(DocumentReferenceBO reference);
        IEnumerable<Guid> InsertBySelectDocument(List<Guid> selectedDocumentIds, Guid documentId);
        void UpdateReference(DocumentReferenceBO data, bool allowSave = true);
    }
}
