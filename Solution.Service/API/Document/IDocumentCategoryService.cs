﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentCategoryService : IService<DocumentCategoryBO>
    {
        IEnumerable<DocumentCategoryBO> GetTreeDocumentCategory(Guid? parentId, Guid departmentId, Common.Resource.DocumentCategoryType type);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="department"></param>
        /// <returns></returns>
        IEnumerable<DocumentCategoryBO> GetTreeDocumentCategoryInternal(Guid? parentId, Guid department);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="department"></param>
        /// <returns></returns>
        IEnumerable<DocumentCategoryBO> GetTreeDocumentCategoryExternal(Guid? parentId, Guid department);
        /// <summary>
        /// Lấy tất cả document category con theo documentcategoryId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IEnumerable<DocumentCategoryBO> GetChildrenByDocumentCategory(Guid? id);

        /// <summary>
        /// Danh sách thư mục con
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        IEnumerable<DocumentCategoryBO> GetChildren(Guid? parentId);
        /// <summary>
        /// Danh sách thư mục trong phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        IEnumerable<DocumentCategoryBO> GetByDepartment(Guid? departmentId, Resource.DocumentCategoryType type);
        /// <summary>
        /// Danh mục tài liệu được phân phối đến user đăng nhập
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentCategoryBO> GetForeignDistribute(Guid? department);

        /// <summary>
        /// Lấy danh sách nhân sự ban hành tài liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IEnumerable<EmployeeBO> GetApproveEmployee(Guid id);
        /// <summary>
        /// Lấy danh sách danh mục ở phòng ban bạn
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentCategoryBO> GetCategorybyDepartment();
        /// <summary>
        /// lAY dANH MỤC BEN NGOAI O PHONG BAN NO
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentCategoryBO> GetCategorybyDepartmentExternal();
        /// <summary>
        /// LAY TAI LIEU PHAN PHOI
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        IEnumerable<DocumentCategoryBO> GetCategorybyDepartmentDistribute(Guid Id);
        /// <summary>
        /// Lấy danh mục chứa tài liệu noi bo
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        DocumentCategoryBO GetByCategory(Guid Id);
        /// <summary>
        /// Lay danh muc chua tai lieu phan phoi thuoc phong ban no
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        DocumentCategoryBO GetByCategoryDistribute(Guid Id);

        /// <summary>
        /// Đếm số danh mục trong phòng ban
        /// 
        /// </summary>
        /// <param name="departmentId"></param>
        /// <param name="type">Loại danh mục: Nội bộ/Bên ngoài/Phân phổi</param>
        /// <returns></returns>
        int CountCategoryByDepartment(Guid departmentId, Common.Resource.DocumentCategoryType type);

    }
}