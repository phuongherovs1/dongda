﻿namespace iDAS.Service
{
    public interface IDocumentDestroyService : IService<DocumentDestroyBO>
    {
        /// <summary>
        /// Yêu cầu hủy
        /// View người ban hành
        /// </summary>
        /// <returns></returns>
        int CountRequest();
    }
}
