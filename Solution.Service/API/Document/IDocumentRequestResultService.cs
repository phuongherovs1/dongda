﻿using System;

namespace iDAS.Service
{
    public interface IDocumentRequestResultService : IService<DocumentRequestResultBO>
    {
        DocumentBO GetDocumentWrite(Guid documentRequestId, Guid documentId);
        Guid SaveResult(DocumentRequestResultBO item, bool allowSave = true);
    }
}
