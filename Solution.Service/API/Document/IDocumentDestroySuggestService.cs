﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentDestroySuggestService : IService<DocumentDestroySuggestBO>
    {
        /// <summary>
        /// Danh sách đề nghị hủy
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="isApproveRole">thực hiện load dữ liệu với quyền người đang đăng nhập
        /// True (Người với quyền phê duyệt): Danh sách đề nghị gửi đến người đó
        /// False (Người đề nghị): Danh sách đề nghị người đó tạo ra
        /// </param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<DocumentDestroySuggestResponsibitityBO> GetAll(int pageIndex, int pageSize, out int count, Common.Resource.DocumentSuggestStatusType status = Common.Resource.DocumentSuggestStatusType.All,
            Common.Resource.DocumentProcessRoleFilterSuggest roleType = Common.Resource.DocumentProcessRoleFilterSuggest.All);

        /// <summary>
        /// Thêm mới đề nghị hủy khi chọn nhiều tài liệu cần hủy
        /// </summary>
        /// <param name="item"></param>
        /// <param name="documentIds"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        Guid Create(DocumentDestroySuggestBO item, string documentIds, bool allowSave = true);
        Guid SaveDestroySuggest(DocumentDestroySuggestBO data, string documentIds, bool allowSave = true);

        /// <summary>
        /// Chức năng thu hồi đề nghị thực hiện trên danh sách đề nghị
        /// </summary>
        /// <param name="destroySuggestId"></param>
        void Revert(Guid destroySuggestId);

        /// <summary>
        /// Lấy DocumentDestroySuggestBO theo ResponsibilityId
        /// </summary>
        /// <param name="responsibilityId"></param>
        /// <returns></returns>
        DocumentDestroySuggestBO GetByResponsibilityId(string responsibilityId);

        /// <summary>
        /// Gửi duyệt đề nghị
        /// </summary>
        /// <param name="data"></param>
        /// <param name="documentIds"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        Guid Send(DocumentDestroySuggestBO data, string documentIds, bool allowSave = true);
        Guid SendSuggestDestroy(DocumentDestroySuggestBO data, bool allowSave = true);

        /// <summary>
        /// Khởi tạo đối tượng mặc định
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        DocumentDestroySuggestBO CreateDefault(string documentId, string departmentId);

        /// <summary>
        /// Ẩn button thêm mới đề nghị hủy khi người đăng nhập là người phê duyệt theo quy trình, ngược lại thì hiển thị
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        bool ShowOrHideButtonCreate(string departmentId);
    }
}
