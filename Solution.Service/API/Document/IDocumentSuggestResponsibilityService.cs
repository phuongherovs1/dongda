﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public interface IDocumentSuggestResponsibilityService : IService<DocumentSuggestResponsibilityBO>
    {
        /// <summary>
        /// Lấy danh sách vai trò bởi người đăng nhập
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentSuggestResponsibilityBO> GetByCurrentUser();
        /// <summary>
        /// Lấy danh sách bản ghi vai trò theo đề nghị
        /// </summary>
        /// <param name="suggestId"></param>
        /// <returns></returns>
        IEnumerable<DocumentSuggestResponsibilityBO> GetBySuggest(Guid suggestId);
        /// <summary>
        /// Lấy danh sách người có vai trò với đề nghị ( bao gồm thông tin chi tiết về nhân sự thực hiện)
        /// </summary>
        /// <param name="suggestId"></param>
        /// <returns></returns>
        IEnumerable<DocumentSuggestResponsibilityBO> GetEmployeeRoleBySuggest(Guid suggestId);
        /// <summary>
        ///  Lấy thông tin chi tiết với vai trò thực hiện bao gồm cả thông tin đề nghị
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        DocumentSuggestResponsibilityBO GetDetail(Guid guid);
        /// <summary>
        ///  Gửi thông tin đề nghị, bao gồm việc cập nhật trạng thái và thay đổi thứ tự thực hiện của đề nghị
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        void Send(DocumentSuggestResponsibilityBO data, bool allowSave = true);
        /// <summary>
        /// Thu hồi thông tin đề nghị, bao gồm việc cập nhật trạng thái và thay đổi thứ tự thực hiện của đề nghị
        /// </summary>
        /// <param name="data"></param>
        /// <param name="allowSave"></param>
        void RevertDocument(DocumentSuggestResponsibilityBO data, bool allowSave = true);
        /// <summary>
        /// Phê duyệt
        /// </summary>
        /// <param name="data"></param>
        void UpdateApproval(DocumentSuggestResponsibilityBO data);
        void ChangeEmployeeApproval(DocumentSuggestResponsibilityBO data);
        void RevertApproval(Guid id);
    }
}
