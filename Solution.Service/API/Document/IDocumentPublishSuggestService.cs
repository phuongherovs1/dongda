﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDocumentPublishSuggestService : IService<DocumentPublishSuggestBO>
    {

        /// <summary>
        /// Form thông tin đề nghị ban hành
        /// </summary>
        /// <param name="id"></param>
        /// <param name="documentId"></param>
        /// <returns></returns>
        DocumentPublishSuggestBO GetSuggestForm(string id, Guid documentId);
        /// <summary>
        /// Phê duyệt đề nghị
        /// </summary>
        /// <param name="suggest"></param>
        void Approve(DocumentPublishSuggestBO suggest);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="suggest"></param>
        /// <param name="destinationString"></param>
        /// <param name="allowSave"></param>
        Guid Insert(DocumentPublishSuggestBO suggest, string destinationString, bool allowSave = true);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        DocumentPublishSuggestBO CreateDefault(Guid documentId, string id = "");

        /// <summary>
        /// Lưu thông tin đề nghị ban hành
        /// </summary>
        /// <param name="data"></param>
        /// <param name="jsonData"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        Guid Save(DocumentPublishSuggestBO data, string jsonData = "", bool allowSave = true);

        /// <summary>
        /// Gửi duyệt đề nghị ban hành
        /// </summary>
        /// <param name="item"></param>
        /// <param name="jsonData"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        Guid Send(DocumentPublishSuggestBO item, string jsonData = "", bool allowSave = true);

        /// <summary>
        /// Danh sách đề nghị ban hành tạo bởi người đăng nhập hệ thống
        /// Danh sách này dùng chung cho 2 view:
        /// 1. Khi isSuggest = true: Lấy danh sách đề nghị được tạo bởi User đăng nhập
        /// 2. Khi isSuggest = false: Lấy danh sách User đăng nhập có quyền phê duyệt
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="approveStatus">Lọc theo trạng thái</param>
        /// <param name="isSuggest">isSuggest = true: Lấy danh sách đề nghị được tạo bởi User đăng nhập, isSuggest = false: Lấy danh sách User đăng nhập có quyền phê duyệt</param>
        /// <returns></returns>
        IEnumerable<DocumentPublishSuggestBO> GetCreateByCurrentUser(int pageIndex, int pageSize, out int count, Resource.ApproveStatus approveStatus = Resource.ApproveStatus.All, bool isSuggest = true);

        /// <summary>
        /// Duyệt đề nghị ban hành
        /// </summary>
        /// <param name="publishSuggest"></param>
        /// <param name="jsonData"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        Guid Approve(DocumentPublishSuggestBO publishSuggest, string jsonData = "", bool allowSave = true);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="approveStatus"></param>
        /// <returns></returns>
        IEnumerable<DocumentPublishSuggestBO> GetApproveByCurrentUser(int pageIndex, int pageSize, out int count, Resource.ApproveStatus approveStatus = Resource.ApproveStatus.All);

        void SendPublishSuggest(DocumentPublishSuggestBO item);
    }
}
