﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IFileService : IService<FileBO>
    {
        /// <summary>
        /// upload file
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid Upload(FileUploadBO data, bool allowSave = true);
        /// <summary>
        /// upload file
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        IEnumerable<Guid> UploadRange(FileUploadBO data, bool allowSave = true);
        /// <summary>
        /// view file
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        FileBO View(Guid fileId);
        /// <summary>
        /// download file
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        FileBO Download(Guid fileId);
    }
}
