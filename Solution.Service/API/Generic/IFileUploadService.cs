﻿using iDAS.Service.FileServiceReference;
using System;
using System.Web;

namespace iDAS.Service
{
    public interface IFileUploadService
    {
        Guid Upload(FileBO file);
        ResponseFileInfo View(string fileId);
        ResponseFileInfo Download(string fileId);
        ResponseFileInfo Update(HttpPostedFileBase file, string fileId);
        string GetMessageError(FileError error);
    }
}
