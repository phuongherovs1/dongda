﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IRoleService : IService<RoleBO>, IRoleStore<RoleBO, Guid>, IQueryableRoleStore<RoleBO, Guid>, IDisposable
    {
        /// <summary>
        /// Insert the role if role not exist, otherwise update
        /// </summary>
        /// <param name="roles"></param>
        void InsertOrUpdate(IEnumerable<RoleBO> roles);

        /// <summary>
        /// Get all the roles of system and check out the roles that user already have
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<RoleBO> GetAll(Guid userId, bool allowDeleted = false);

        /// <summary>
        /// Get all the roles of system and check out the roles that user already have with paging
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<RoleBO> GetAll(Guid userId, int pageIdex, int pageSize, out int totalCount, bool allowDeleted = false);

        /// <summary>
        /// Get all the roles of module and check out the roles that user already have 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<RoleBO> GetAll(Guid userId, string module, bool allowDeleted = false);

        /// <summary>
        /// Get all the roles of module and check out the roles that user already have with paging
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<RoleBO> GetAll(Guid userId, string module, int pageIdex, int pageSize, out int totalCount, bool allowDeleted = false);

        /// <summary>
        /// Get all the module from the role of system
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> GetModules();
        IEnumerable<RoleBO> GetAllByTittle(Guid titleId, int pageIdex, int pageSize, out int totalCount, bool allowDeleted = false);
        IEnumerable<RoleBO> GetAllByTittle(Guid titleId, string module, int pageIdex, int pageSize, out int totalCount, bool allowDeleted = false);
    }
}
