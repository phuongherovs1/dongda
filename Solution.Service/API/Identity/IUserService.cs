﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IUserService : IService<UserBO>, IUserLoginStore<UserBO, Guid>, IUserRoleStore<UserBO, Guid>, IUserPasswordStore<UserBO, Guid>, IUserSecurityStampStore<UserBO, Guid>, IUserStore<UserBO, Guid>, IDisposable
    {
        /// <summary>
        /// Get all users display data by paging;
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IEnumerable<UserBO> GetAll(int pageIndex, int pageSize, out int totalCount, bool allowDeleted = false);
        IEnumerable<UserBO> GetByQuery(int pageIndex, int pageSize, out int totalCount, bool allowDeleted = false, string strFilter = "");

        UserBO GetCurrentUser();
    }
}
