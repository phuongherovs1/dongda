﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IUserRoleService : IService<UserRoleBO>
    {
        bool RoleExits(string code);
        bool CurrentUser(Guid userid);
        List<string> CurrentUserRole(Guid userid);
    }
}
