﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ITitleRoleService : IService<TitleRoleBO>
    {
        List<string> GetUserRole(Guid userid);
    }
}
