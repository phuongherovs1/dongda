﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface ITaskAssociateService : IService<TaskAssociateBO>
    {
        IEnumerable<TaskAssociateBO> GetByTaskID(Guid taskID);
    }
}
