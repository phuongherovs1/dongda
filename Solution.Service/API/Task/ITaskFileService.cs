﻿using System;

namespace iDAS.Service
{
    public interface ITaskFileService : IService<TaskFileBO>
    {
        /// <summary>
        /// Lấy danh sách file đính kèm của công việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        FileUploadBO GetByTask(Guid taskId);
    }
}
