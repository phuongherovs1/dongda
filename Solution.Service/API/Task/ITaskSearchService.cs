﻿using System;
using System.Data;

namespace iDAS.Service.API.Task
{
    public interface ITaskSearchService
    {
        DataTable GetData(string textSearch, DateTime dateFrom, DateTime dateTo, Guid? employeeId, Guid? titleID, Guid? departmentID, int? status, int pageIndex, int pageSize, out int count);
        DataTable GetTaskStatistic(DateTime fromDate, DateTime toDate, string year, string months, Guid? dataId, string statisticType);
    }
}
