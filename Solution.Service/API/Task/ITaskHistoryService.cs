﻿
using System;

namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface ITaskHistoryService : IService<TaskHistoryBO>
    {
        void Insert(Guid taskID, byte[] data, bool allowSave = true);
    }
}
