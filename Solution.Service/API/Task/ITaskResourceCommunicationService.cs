﻿using System;

namespace iDAS.Service
{
    public interface ITaskCommunicationShareService : IService<TaskCommunicationShareBO>
    {
        /// <summary>
        /// Kiểm tra xem người liên quan đến công việc có được xem phản hồi ở trạng thái private không
        /// </summary>
        /// <param name="taskCommunicationId">Id của phản hồi</param>
        /// <param name="taskResourceId">Id công việc liên quan</param>
        /// <returns></returns>
        bool CheckExits(Guid taskCommunicationId, Guid userId);
    }
}
