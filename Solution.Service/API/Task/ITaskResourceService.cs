﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface ITaskResourceService : IService<TaskResourceBO>
    {
        /// <summary>
        /// Lấy thông tin cho form nhập liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TaskResourceBO GetFormItem(Guid? id, Guid? taskId);
        /// <summary>
        /// Lấy danh sách công việc liên quan đến nhân sự theo trách nhiệm và trạng thái công việc
        /// </summary>
        /// <param name="employeeId">id nhân sự</param>
        /// <param name="taskRole"></param>
        /// <param name="taskStatus"></param>
        /// <returns></returns>
        IEnumerable<TaskResourceBO> GetListTaskByEmployee(Guid employeeId, int taskRole, int taskStatus, int pageSites = 14, int pageIndex = 0);
        /// <summary>
        /// Lấy danh sách nhân sự không phải vai trò là thực hiện
        /// </summary>
        /// <param name="taskId">id công việc</param>
        /// <returns></returns>
        IEnumerable<TaskResourceBO> GetListViewAndReview(Guid taskId, int pageIndex, int pageSize, out int totalCount);
        /// <summary>
        /// Kiểm tra người sử dụng có được phép tạo thông tin người liên quan
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        bool CheckIsCreate(Guid taskId);
        /// <summary>
        /// Lấy danh sách id của nhân sự liên quan đến công việc đang chọn
        /// </summary>
        /// <param name="taskId">id công việc</param>
        /// <returns></returns>
        IQueryable<Guid> GetEmployeeIds(Guid taskId);
        /// <summary>
        /// Lấy danh sách theo employeeId
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        IQueryable<TaskResourceBO> GetByEmployeeId(Guid employeeId);
        /// <summary>
        /// Lấy danh sách theo id công việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        IEnumerable<TaskResourceBO> GetByTaskResource(Guid taskResourceId);

        bool CheckExits(Guid taskResoureId, Guid userId);
        /// <summary>
        /// Lấy danh sách công việc liên quan mới nhất của nhân sự đang đăng nhập
        /// </summary>
        /// <returns></returns>
        IEnumerable<TaskResourceBO> GetByCurrentUser();


    }
}