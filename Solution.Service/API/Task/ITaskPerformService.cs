﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface ITaskPerformService : IService<TaskPerformBO>
    {
        /// <summary>
        /// Lấy danh sách thực hiện công việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        IEnumerable<TaskPerformBO> GetAll(Guid taskId, int pageIndex, int pageSize, out int totalCount);
        /// <summary>
        /// Lấy thông tin cho form nhập liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TaskPerformBO GetFormItem(Guid? id, Guid? taskId);
        /// <summary>
        /// Lấy thông tin hủy công việc thực hiện
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TaskPerformBO GetCancelItem(Guid id);
        /// <summary>
        /// Lấy thông tin tạm dừng công việc thực hiện
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TaskPerformBO GetPauseItem(Guid id);
        /// <summary>
        /// Lấy thông tin kiểm tra công việc thực hiện
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TaskPerformBO GetCheckItem(Guid id);
        /// <summary>
        /// Thêm mới thông tin giao việc cho nhân sự có vai trò là thực hiện
        /// </summary>
        /// <param name="taskPerform"></param>
        /// <returns></returns>
        Guid Create(TaskPerformBO item);
        /// <summary>
        /// Cập nhật thông tin giao việc cho nhân sự có vai trò là thực hiện
        /// </summary>
        /// <param name="item"></param>
        void Update(TaskPerformBO item);
        /// <summary>
        /// Cập nhật báo cáo thực hiện công việc
        /// </summary>
        /// <param name="taskPerform"></param>
        /// <param name="allowSave"></param>
        void Report(TaskPerformBO item);
        /// <summary>
        /// Cập nhật kết quả kiểm tra thực hiện công việc
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowSave"></param>
        void Check(TaskPerformBO item);
        /// <summary>
        /// Gửi công việc đến người thực hiện
        /// </summary>
        /// <param name="taskPerform"></param>
        /// <param name="allowSave"></param>
        Boolean Send(TaskPerformBO item);
        /// <summary>
        /// Hủy công việc của người thực hiện
        /// </summary>
        /// <param name="item"></param>
        void Cancel(TaskPerformBO item);
        /// <summary>
        /// Tạm dừng công việc của người thực hiện
        /// </summary>
        /// <param name="item"></param>
        void Pause(TaskPerformBO item);
        /// <summary>
        /// Kiểm tra người sử dụng có được phép tạo thông tin giao việc thực hiện
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        bool CheckIsCreate(Guid taskId);
        /// <summary>
        /// Lấy danh sách công việc đang thực hiện của nhân sự trong thời gian bắt đầu và kết thúc
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="startAt"></param>
        /// <param name="endAt"></param>
        /// <returns></returns>
        IEnumerable<TaskPerformBO> GetListPerformByEmployeeAndTime(Guid? employeeId, DateTime? startAt, DateTime? endAt, int pageIndex, int pageSize, out int totalCount);

    }
}