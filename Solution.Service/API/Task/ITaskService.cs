﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface ITaskService : IService<TaskBO>
    {
        /// <summary>
        /// Lấy thông tin cho form nhập liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TaskBO GetFormItem(Guid? id);
        /// <summary>
        /// Quản lý chất lượng giao việc
        /// </summary>
        /// <param name="item"></param>
        /// <param name="QualityCAPAId"></param>
        /// <returns></returns>
        Guid CreateTask(TaskBO item);
        Guid CreateTaskRiskTarget(TaskBO item);
        Guid CreateDispatchTask(TaskBO item);
        IEnumerable<TaskBO> GetDataRiskTargetTask(int pageIndex, int pageSize, out int count, Guid RiskTargetPlanId);
        /// <summary>
        /// Quản lý chất lượng - Giao việc trong kế hoạch thực hiện mục tiêu
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        Guid CreatePlanTask(TaskBO item);
        /// <summary>
        /// Tạo mới thông tin công việc
        /// </summary>
        /// <param name="item"></param>
        Guid Create(TaskBO item);

        IEnumerable<TaskBO> GetDataDispatchTask(int pageIndex, int pageSize, out int count, Guid DispatchId);
        /// <summary>
        /// Cập nhật thông tin công việc
        /// </summary>
        /// <param name="item"></param>
        void Update(TaskBO item);
        /// <summary>
        /// Hủy công việc
        /// </summary>
        /// <param name="item"></param>
        void Cancel(TaskBO item);
        /// <summary>
        /// Tạm dừng công việc
        /// </summary>
        /// <param name="item"></param>
        void Pause(TaskBO item);
        /// <summary>
        /// Kết thúc công việc
        /// </summary>
        /// <param name="item"></param>
        void Finish(TaskBO item);
        /// <summary>
        /// Lấy thông tin công việc
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        TaskBO GetById(Guid taskId);
        /// <summary>
        /// Lấy thông tin thông kê công việc theo trách nhiệm của người sử dụng hiện tại
        /// </summary>
        /// <param name="taskRole"></param>
        /// <returns></returns>
        TaskSummaryBO GetSummary(int taskRole);
        /// <summary>
        /// Tạo công việc khi tạo mới tài liệu
        /// Khi người đề nghị = người viết tài liệu: người đề nghị = người thực hiện công việc
        /// khi người đề nghị khác người viết tài liệu: người đề nghị = người theo dõi công việc, người viết = người thực hiện công việc
        /// người phê duyệt = người giao việc
        /// </summary>
        /// <param name="documentName"></param>
        /// <param name="createBy"></param>
        /// <param name="writeBy"></param>
        /// <param name="approveBy"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        Guid CreateFromDocument(string documentName, string version, bool isAdjust, Guid writeBy, Guid approveBy, DateTime fromDate, DateTime toDate, int revision, int requestTime = 1, bool allowSave = true);

        IEnumerable<TaskBO> GetDataTaskQualityCAPA(int pageIndex, int pageSize, out int count, Guid QualityCAPAId);

        /// <summary>
        /// Danh sách công việc theo kế hoạch
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="QualityPlanId"></param>
        /// <returns></returns>
        IEnumerable<TaskBO> GetDataTaskByQualityPlan(int pageIndex, int pageSize, out int count, Guid QualityPlanId);
        IEnumerable<TaskBO> GetDataTaskPerformQualityCAPA(int pageIndex, int pageSize, out int count, Guid QualityNCId);
    }
}
