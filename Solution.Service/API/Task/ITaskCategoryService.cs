﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ITaskCategoryService : IService<TaskCategoryBO>
    {
        IEnumerable<TaskCategoryBO> GetAll();
        Guid InsertTaskCategory(TaskCategoryBO data);
        void UpdateTaskCategory(TaskCategoryBO data);
        Dictionary<Guid, string> GetDepartmentByUser();
        IEnumerable<TaskCategoryBO> GetAll(int pageIndex, int pageSize, out int count);
        IEnumerable<TaskCategoryBO> GetByfilterName(int pageIndex, int pageSize, string filterName, Guid deDepartmentId, out int count);
    }
}