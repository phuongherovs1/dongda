﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Schedule Service
    /// </summary>
    public interface ITaskScheduleService : IService<TaskScheduleBO>
    {
        /// <summary>
        /// Lấy danh sách lịch công tác liên quan đến nhân sự
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<TaskScheduleBO> GetListScheduleByEmployee(Guid employeeId, int pageIndex, int pageSize, out int count);
        /// <summary>
        /// Lấy danh sách lịch công tác liên quan đến nhân sự theo tuần
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        IEnumerable<DayBO> GetWeek(Guid? employeeId, DateTime? date);
        IEnumerable<DayBO> GetWeek_Task(Guid? employeeId, DateTime? date);
        /// <summary>
        /// Lấy thông tin cho form nhập liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TaskScheduleBO GetFormItem(Guid? id);
        /// <summary>
        /// Thêm mới lịch công tác
        /// </summary>
        /// <param name="item"></param>
        void Create(TaskScheduleBO item);
        /// <summary>
        /// Cập nhật lịch công tác
        /// </summary>
        /// <param name="item"></param>
        void Update(TaskScheduleBO item);
        /// <summary>
        /// Hủy lịch công tác
        /// </summary>
        /// <param name="item"></param>
        void Cancel(TaskScheduleBO item);
        /// <summary>
        /// Tạm dừng lịch công tác
        /// </summary>
        /// <param name="item"></param>
        void Pause(TaskScheduleBO item);
    }
}
