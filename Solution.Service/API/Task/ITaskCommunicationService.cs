﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface ITaskCommunicationService : IService<TaskCommunicationBO>
    {
        /// <summary>
        /// Lấy danh sách trao đổi công việc từ nhân sự liên quan và kiểu thông tin trao đổi
        /// </summary>
        /// <param name="taskPerformId"></param>
        /// <returns></returns>
        IEnumerable<TaskCommunicationBO> Get(Guid taskResourceId, int type, Guid userId);
        /// <summary>
        /// Lấy danh sách trao đổi công việc theo loại trao đổi và user đăng nhập
        /// </summary>
        /// <param name="taskPerformId"></param>
        /// <returns></returns>
        IEnumerable<TaskCommunicationBO> GetDataByUser(int type, Guid userId);
        void InsertCommunication(TaskCommunicationBO dto, List<Guid> taskResourceGuidIds);
        Guid Create(TaskCommunicationBO data);
    }
}
