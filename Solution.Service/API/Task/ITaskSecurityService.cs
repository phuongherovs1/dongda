﻿namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface ITaskSecurityService : IService<TaskSecurityBO>
    {
    }
}
