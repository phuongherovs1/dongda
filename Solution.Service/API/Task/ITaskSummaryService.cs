﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface ITaskSummaryService
    {
        List<SummaryBO> GetSummaryNearly(int role = 0);
        List<TaskPieChartBO> CurrentSummary(int role = 0);
        List<TaskPieChartBO> SummaryByDepartment(Guid departmentId);
        List<PieChartBO> TaskStatusAnalytic(Guid departmentId);
        List<PieChartBO> TaskTypeAnalytic(Guid departmentId);
    }
}
