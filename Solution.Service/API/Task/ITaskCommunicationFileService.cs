﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ITaskCommunicationFileService : IService<TaskCommunicationFileBO>
    {
        IEnumerable<TaskCommunicationFileBO> GetByCommunication(Guid communicationId);

    }
}
