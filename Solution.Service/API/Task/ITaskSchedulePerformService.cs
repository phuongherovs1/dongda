﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Schedule Perform Service
    /// </summary>
    public interface ITaskSchedulePerformService : IService<TaskSchedulePerformBO>
    {
        /// <summary>
        /// Lấy danh sách nhân sự thực hiện từ lịch công tác
        /// </summary>
        /// <param name="taskScheduleId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        IEnumerable<TaskSchedulePerformBO> GetBySchedule(Guid taskScheduleId, int pageIndex, int pageSize, out int totalCount);
        /// <summary>
        /// Lây thông tin cho form nhập liệu
        /// </summary>
        /// <returns></returns>
        TaskSchedulePerformBO GetFormItem(Guid? id, Guid? taskScheduleId);
        /// <summary>
        /// Thêm mới người thực hiện vào lịch công tác
        /// </summary>
        /// <param name="item"></param>
        void Create(TaskSchedulePerformBO item);
        /// <summary>
        /// Cập nhật thông tin của người thực hiện
        /// </summary>
        /// <param name="item"></param>
        void Update(TaskSchedulePerformBO item);
        /// <summary>
        /// Xác nhận lịch công tác của người thực hiện
        /// </summary>
        /// <param name="item"></param>
        void Confirm(TaskSchedulePerformBO item);
        /// <summary>
        /// Hủy lịch công tác của người thực hiện
        /// </summary>
        /// <param name="item"></param>
        void Cancel(TaskSchedulePerformBO item);
        /// <summary>
        /// Tạm dừng lịch công tác của người thực hiện
        /// </summary>
        /// <param name="item"></param>
        void Pause(TaskSchedulePerformBO item);
        /// <summary>
        /// Gửi lịch công tác tới người xác nhận thực hiện
        /// </summary>
        /// <param name="id"></param>
        void Send(TaskSchedulePerformBO item);
        /// <summary>
        /// Lấy thông tin lịch công tác của người thực hiện
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TaskSchedulePerformBO GetById(Guid id);
    }
}
