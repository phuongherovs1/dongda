﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ITaskSourceService : IService<TaskSourceBO>
    {
        IEnumerable<TaskSourceBO> GetAll();
        Guid InsertTaskSource(TaskSourceBO data);
        void UpdateTaskSource(TaskSourceBO data);
        IEnumerable<TaskSourceBO> GetAll(int pageIndex, int pageSize, out int count);
        IEnumerable<TaskSourceBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count);
    }
}