﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualitySummaryService
    {
        List<PieChartBO> TargetStatusAnalytic(Guid departmentId);
        List<PieChartBO> QualityNCAnalytic(Guid departmentId);
    }
}
