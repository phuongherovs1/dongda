﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IQualityUnitService : IService<QualityUnitBO>
    {
        /// <summary>
        /// Thêm mới vấn đề
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        Guid InsertUnit(QualityUnitBO data);

        /// <summary>
        /// Danh sách các vấn đề
        /// </summary>
        /// <returns></returns>
        IEnumerable<QualityUnitBO> GetUnit();

        /// <summary>
        /// Chi tiết nhóm mục tiêu / vấn đề
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        QualityUnitBO GetDetail(string id);
    }
}