﻿
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IQualityPlanService : IService<QualityPlanBO>
    {
        /// <summary>
        /// Thêm mới/sửa kế hoạch
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid Create(QualityPlanBO data, string TaskId = "");

        /// <summary>
        /// Cập nhật kế hoạch
        /// </summary>
        /// <param name="data"></param>
        void UpdatePlan(QualityPlanBO data);

        /// <summary>
        /// Gửi duyệt kế hoạch
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        void Send(QualityPlanBO data, string TaskId = "");

        /// <summary>
        /// Phê duyệt kế hoạch
        /// </summary>
        /// <param name="data"></param>
        void Approval(QualityPlanBO data);

        /// <summary>
        /// Thông tin chi tiết kế hoạch
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <param name="targetId"></param>
        /// <returns></returns>
        QualityPlanBO GetDetail(string id, string departmentId, Guid targetId);

        /// <summary>
        /// Xóa kế hoạch
        /// </summary>
        /// <param name="id"></param>
        void DeletePlan(Guid id);

        /// <summary>
        /// Danh sách kế hoạc theo mục tiêu
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="targetId"></param>
        /// <returns></returns>
        IEnumerable<QualityPlanBO> GetByTargetId(int pageIndex, int pageSize, out int count, Guid targetId, string filter, Resource.QualityPlan status);
    }
}