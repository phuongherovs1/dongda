﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityAuditNotebookNCService : IService<QualityAuditNotebookNCBO>
    {
        List<Guid> GetNC(Guid notebookId);
        List<Guid> GetNCByPlanDetail(Guid qualityAuditPlanDetailId);
        List<Guid> GetNCIsConfirmByPlanDetail(Guid qualityAuditPlanDetailId);
    }
}
