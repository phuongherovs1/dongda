﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IQualityMeetingRecordMemberService : IService<QualityMeetingRecordMemberBO>
    {
        IEnumerable<EmployeeBO> GetData(Guid meetingId);

        IEnumerable<QualityMeetingRecordMemberBO> GetMember(Guid meetingId);

        IEnumerable<QualityMeetingRecordMemberBO> GetAvailableMember(Guid meetingId);

        void AddMember(string memberIds, Guid meetingId);

        void Delete(Guid meetingId, Guid memberId);

        /// <summary>
        /// Cập nhật vắng mặt
        /// </summary>
        /// <param name="id"></param>
        void ChangeAvailable(Guid employeeId, Guid meetingId);

        void ChangeAvailable(Guid id);
    }
}