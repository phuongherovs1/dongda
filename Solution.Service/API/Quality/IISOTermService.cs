﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IISOTermService : IService<ISOTermBO>
    {
        /// <summary>
        /// Lấy danh sách điều khoản theo tiêu chuẩn ISO
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="isoStandardId"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<ISOTermBO> GetAll(int pageIndex, int pageSize, out int count, Guid? isoStandardId, string query = default(string));
        /// <summary>
        /// Lấy danh sách điều khoản theo tiêu chuẩn ISO
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="isoStandardId"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<ISOTermBO> GetByISOStandards(int pageIndex, int pageSize, out int count, Guid planId, Guid programId);
    }
}
