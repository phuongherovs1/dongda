﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityNCRelationService : IService<QualityNCRelationBO>
    {
        IEnumerable<QualityNCRelationBO> GetDataQualityNCRelation(int pageIndex, int pageSize, out int count, Guid QualiyNCId);
    }
}
