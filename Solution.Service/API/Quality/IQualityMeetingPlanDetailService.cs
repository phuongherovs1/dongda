﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityMeetingPlanDetailService : IService<QualityMeetingPlanDetailBO>
    {
        QualityMeetingPlanDetailBO GetNewestMeeting(Guid planId);

        IEnumerable<QualityMeetingPlanDetailBO> GetByPlanId(Guid planId);

        /// <summary>
        /// Tạo cuộc họp theo định kỳ
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        Guid CreateByRoutine(Guid planId);

        /// <summary>
        /// Chuyển trạng thái cuộc họp => đang thực hiện
        /// Lấy thông tin đầy đủ cho cuộc họp
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        QualityMeetingPlanDetailBO GetFullInfo(Guid id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        void Approve(Guid id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        QualityMeetingPlanDetailBO GetNewMeetingForm(Guid planId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid NewMeeting(QualityMeetingPlanDetailBO data);

        void SendApprove(QualityMeetingPlanDetailBO data);
    }
}