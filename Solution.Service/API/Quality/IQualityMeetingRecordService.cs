﻿using System;
namespace iDAS.Service
{
    public interface IQualityMeetingRecordService : IService<QualityMeetingRecordBO>
    {
        QualityMeetingRecordBO GetFormItem(Guid meetingId);

        void Finish(Guid meetingId);

        void Save(QualityMeetingRecordBO data);
    }
}