﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IQualityMeetingPlanNCService : IService<QualityMeetingPlanNCBO>
    {
        /// <summary>
        /// Danh sách điểm không phù hợp theo vấn đề cuộc họp
        /// </summary>
        /// <param name="issueId"></param>
        /// <param name="meetingPlanReportIssueId"></param>
        /// <returns></returns>
        IEnumerable<QualityNCBO> GetByMeetingIssue(Guid issueId, Guid meetingPlanReportIssueId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="issueId"></param>
        /// <param name="meetingPlanReportIssueId"></param>
        /// <returns></returns>
        QualityMeetingPlanNCBO GetFormItem(Guid issueId, Guid meetingPlanReportIssueId);

        /// <summary>
        /// Tạo mới sự không phù hợp
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid Save(QualityNCBO data, Guid issueId, Guid meetingPlanReportId, string datajson);
    }
}