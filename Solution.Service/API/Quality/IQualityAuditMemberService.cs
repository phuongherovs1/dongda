﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityAuditMemberService : IService<QualityAuditMemberBO>
    {
        IEnumerable<QualityAuditMemberBO> GetAll(int pageIndex, int pageSize, out int count, Guid? qualityAuditPlanDetailId);
        void InsertMember(string qualityAuditMembers = default(string), bool allowSave = true);
        void UpdateRoleType(Guid id, int roleType);
        void UpdateNotAvailable(Guid id, bool isNotAvailable);
        EmployeeBO GetLeader(Guid qualityAuditProgramId);
        bool ExitsLeader(Guid id);
    }
}
