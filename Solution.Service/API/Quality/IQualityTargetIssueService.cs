﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IQualityTargetIssueService : IService<QualityTargetIssueBO>
    {
        /// <summary>
        /// Thêm mới vấn đề
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        Guid InsertIssue(QualityTargetIssueBO data);

        /// <summary>
        /// Danh sách các vấn đề
        /// </summary>
        /// <returns></returns>
        IEnumerable<QualityTargetIssueBO> GetIssue();

        /// <summary>
        /// Chi tiết nhóm mục tiêu / vấn đề
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        QualityTargetIssueBO GetDetail(string id);
    }
}