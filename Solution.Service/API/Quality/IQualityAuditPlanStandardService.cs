﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityAuditPlanStandardService : IService<QualityAuditPlanStandardBO>
    {
        void InsertStandardRange(List<QualityAuditPlanStandardBO> data, Guid planId, bool autoSave = true);
        void UpdateStandardRange(List<QualityAuditPlanStandardBO> data, Guid planId, bool autoSave = true);
    }
}
