﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityAuditProgramService : IService<QualityAuditProgramBO>
    {
        /// <summary>
        /// Lấy chương trình đánh giá theo chi tiết kế hoạch
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="qualityAuditPlanDetailId"></param>
        /// <returns></returns>
        IEnumerable<QualityAuditProgramBO> GetAll(int pageIndex, int pageSize, out int count, Guid? qualityAuditPlanDetailId);
        Guid InsertProgram(QualityAuditProgramBO data, string qualityAuditProgramTerms = default(string), string qualityAuditProgramDepartments = default(string), bool allowSave = true);
        void UpdateProgram(QualityAuditProgramBO data, string qualityAuditProgramTerms = default(string), string qualityAuditProgramDepartments = default(string), bool allowSave = true);
    }
}
