﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityAuditPlanDetailService : IService<QualityAuditPlanDetailBO>
    {
        /// <summary>
        /// Thêm thông tin cuộc họp định kỳ của kế hoạch
        /// </summary>
        /// <param name="planId"></param>
        /// <param name="timeValue"></param>
        /// <param name="unitTime"></param>
        /// <returns></returns>
        Guid InsertRoutine(Guid planId, int? timeValue, int? unitTime, bool allowSave = true);
        IEnumerable<QualityAuditPlanDetailBO> GetAll(int pageIndex, int pageSize, out int count, Guid? planId);
        void Destroy(Guid id);
        void Approval(QualityAuditPlanDetailBO data);
        void ChangeStatus(Guid id, int status);
        QualityAuditPlanDetailBO GetReportById(Guid id);
        void Report(QualityAuditPlanDetailBO data);
        void Copy(Guid planId, int? timeValue, int? unitTime, bool allowSave = true);
    }
}
