﻿using System;

namespace iDAS.Service
{
    public interface IQualityCAPADetailService : IService<QualityCAPADetailBO>
    {
        void DeleteTask(Guid id);
        void DeletePerformTask(Guid id);
    }
}
