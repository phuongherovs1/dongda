﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityAuditNotebookService : IService<QualityAuditNotebookBO>
    {
        IEnumerable<QualityAuditNotebookBO> GetAll(int pageIndex, int pageSize, out int count, Guid? programId);
    }
}
