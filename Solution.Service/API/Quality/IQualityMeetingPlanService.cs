﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IQualityMeetingPlanService : IService<QualityMeetingPlanBO>
    {
        QualityMeetingPlanBO GetFormItem(string id);

        Guid Save(QualityMeetingPlanBO data, bool autoSave = true);

        IEnumerable<QualityMeetingPlanBO> GetData(int pageIndex, int pageSize, out int count, string key, List<int> status = null);

    }
}