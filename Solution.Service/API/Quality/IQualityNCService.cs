﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityNCService : IService<QualityNCBO>
    {
        IEnumerable<QualityNCBO> GetDataQualityNC(int pageIndex, int pageSize, out int count, Guid? departmentId, string filterName = default(string), Common.Resource.QualityNCStatus status = 0);
        IEnumerable<QualityNCBO> GetDataByIds(List<Guid> ids, int pageIndex, int pageSize, out int count);
        IEnumerable<QualityNCBO> GetTotalNCs(List<Guid> ids, int pageIndex, int pageSize, out int count);
        IEnumerable<QualityNCBO> GetDataWaitConfirmNC(int pageIndex, int pageSize, out int count);
        IEnumerable<QualityNCBO> GetDataNotConfirmNC(int pageIndex, int pageSize, out int count);
        IEnumerable<QualityNCBO> GetDataConfirmNC(int pageIndex, int pageSize, out int count);
        Guid InsertQualityNC(QualityNCBO item, string datajson = "", bool allowSave = true);
        Guid InsertQualityNCApproval(QualityNCBO item, string datajson = "", bool allowSave = true);
        void UpdateQualityNC(QualityNCBO item, string datajson = "", bool allowSave = true);
        void ApprovalNC(QualityNCBO item, bool allowSave = true);
        QualityNCBO DetailQualityNC(Guid id);
        IEnumerable<QualityNCBO> GetDataRowExpander(int pageIndex, int pageSize, out int count, Guid Id);
        QualityNCBO CreatePerformQualityCAPA(Guid id);

        void ConfirmNCs(string qualityNCs = default(string), bool allowSave = true);

        /// <summary>
        /// Danh sách sự không phù hợp theo mục tiêu
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="targetId"></param>
        /// <returns></returns>
        IEnumerable<QualityNCBO> GetByTargetId(int pageIndex, int pageSize, out int count, Guid targetId, string filter, Resource.QualityNCStatus status);
        void DeleteQualityNC(Guid id);
        /// <summary>
        /// Gửi đến khung chưa xác nhận
        /// </summary>
        /// <param name="id"></param>
        void FromWaitConfirm(Guid id);
        /// <summary>
        /// Gửi đến khung xác nhận
        /// </summary>
        /// <param name="id"></param>
        void ToConfirm(Guid id);
        /// <summary>
        /// Gửi đền khung không đạt
        /// </summary>
        /// <param name="id"></param>
        void ToNotPass(Guid id);
        void SendApprovalNC(QualityNCBO item, bool allowSave = true);
        /// <summary>
        /// kéo vào nhau
        /// </summary>
        /// <param name="toId"></param>
        /// <param name="formId"></param>
        /// <param name="allowSave"></param>
        void ToFromQuality(Guid toId, Guid formId, bool allowSave = true);
        void DestroyQualityNC(Guid Id);
        IEnumerable<QualityNCBO> GetAllDataQualityNC(int pageIndex, int pageSize, out int count, string filterName = default(string), Common.Resource.QualityNCStatus status = 0);
    }
}
