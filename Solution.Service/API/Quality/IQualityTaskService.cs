﻿using System;
namespace iDAS.Service
{
    public interface IQualityTaskService : IService<QualityTaskBO>
    {
        /// <summary>
        /// Xóa công việc
        /// </summary>
        /// <param name="id"></param>
        void DeleteTask(Guid id);
    }
}