﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IISOStandardService : IService<ISOStandardBO>
    {
        /// <summary>
        /// Lấy danh sách tiêu chuẩn đánh giá theo kế hoạch đánh giá
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        IEnumerable<ISOStandardBO> GetByPlan(Guid? planId);
    }
}
