﻿using System;

namespace iDAS.Service
{
    public interface IQualityCAPAService : IService<QualityCAPABO>
    {
        QualityCAPABO CreateRequestQualityCAPA(Guid id);
    }
}
