﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityAuditProgramDepartmentService : IService<QualityAuditProgramDepartmentBO>
    {
        void InsertDepartmentRange(List<QualityAuditProgramDepartmentBO> data, Guid programId, bool autoSave = true);
        List<Guid> GetDepartmentByProgram(Guid programId);
    }
}
