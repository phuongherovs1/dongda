﻿
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IQualityTargetService : IService<QualityTargetBO>
    {
        /// <summary>
        /// Danh sách mục tiêu theo phòng ban
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        IEnumerable<QualityTargetBO> GetByDepartment(int pageIndex, int pageSize, out int count, string departmentId, Guid? issueId,
                    Resource.TargetStatus status, string filter, DateTime? endAt);

        /// <summary>
        /// Chi tiết mục tiêu
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        QualityTargetBO GetDetail(string id, string departmentId);

        /// <summary>
        /// Thêm mới mục tiêu
        /// </summary>
        /// <param name="data"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        Guid InsertTarget(QualityTargetBO data, string jsonData);

        /// <summary>
        /// Xóa mục tiêu
        /// </summary>
        /// <param name="id"></param>
        void DeleteTarget(Guid id);

        /// <summary>
        /// Hủy mục tiêu
        /// </summary>
        /// <param name="id"></param>
        void DestroyTarget(Guid id);

        /// <summary>
        /// Khôi phục mục tiêu đã hủy
        /// </summary>
        /// <param name="id"></param>
        void RevertTarget(Guid id);

        /// <summary>
        /// Cập nhật kết quả thực hiện
        /// </summary>
        /// <param name="data"></param>
        void UpdateResult(QualityTargetBO data);

        /// <summary>
        /// Phê duyệt mục tiêu
        /// </summary>
        /// <param name="data"></param>
        void Approval(QualityTargetBO data);
    }
}