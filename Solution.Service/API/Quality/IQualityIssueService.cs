﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IQualityIssueService : IService<QualityIssueBO>
    {
        IEnumerable<QualityIssueBO> GetByMeetingId(Guid meetingId);

        IEnumerable<QualityIssueBO> GetToSelectMeeting(Guid meetingId);

    }
}