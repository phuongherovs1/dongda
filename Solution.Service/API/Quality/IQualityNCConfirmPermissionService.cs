﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityNCConfirmPermissionService : IService<QualityNCConfirmPermissionBO>
    {
        void InsertPermissionQualityNC(QualityNCConfirmPermissionBO item, string strData);
        IEnumerable<QualityNCConfirmPermissionBO> GetAllNCConfirmPermission(int pageSize, int pageIndex, out int count, Guid departmentId);
        QualityNCConfirmPermissionBO GetDetail(Guid id);
        void UpdatePermission(QualityNCConfirmPermissionBO item);
    }
}
