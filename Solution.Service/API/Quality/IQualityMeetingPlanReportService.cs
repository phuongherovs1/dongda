﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IQualityMeetingPlanReportService : IService<QualityMeetingPlanReportBO>
    {
        IEnumerable<QualityMeetingPlanReportBO> GetSubjectByMeeting(Guid meetingId);

        QualityMeetingPlanReportBO GetFormItem(string id, Guid meetingId);

        Guid Save(QualityMeetingPlanReportBO data, string issueJsonData);

        /// <summary>
        /// Cập nhật nội dung báo cáo
        /// </summary>
        /// <param name="data"></param>
        void SaveReport(QualityMeetingPlanReportBO data);
    }
}