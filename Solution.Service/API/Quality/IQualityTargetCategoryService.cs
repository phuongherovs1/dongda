﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityTargetCategoryService : IService<QualityTargetCategoryBO>
    {
        /// <summary>
        /// Danh sách nhóm mục tiêu theo phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        IEnumerable<QualityTargetCategoryBO> GetTreeTargetCategory(string departmentId, bool checkAll);

        /// <summary>
        /// Chi tiết nhóm mục tiêu / vấn đề
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        QualityTargetCategoryBO GetDetail(string id);

        /// <summary>
        /// Xóa nhóm mục tiêu
        /// </summary>
        /// <param name="id"></param>
        void DeleteCategory(Guid id);
    }
}