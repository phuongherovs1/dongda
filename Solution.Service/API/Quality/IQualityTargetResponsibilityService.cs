﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IQualityTargetResponsibilityService : IService<QualityTargetResponsibilityBO>
    {
        /// <summary>
        /// Danh sách trách nhiệm theo mục tiêu
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="targetId"></param>
        /// <returns></returns>
        IEnumerable<QualityTargetResponsibilityBO> GetByTargetId(int pageIndex, int pageSize, out int count, string targetId);

        /// <summary>
        /// Thêm mới trách nhiệm
        /// </summary>
        /// <param name="targetId"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        IEnumerable<Guid> Create(Guid targetId, string jsonData = "", bool allowSave = true);

        // Kiểm tra trách nhiệm liên quan của tài khoản đăng nhập hiện tại
        bool CheckResponsibilityCurrentUser(Guid targetId);
    }
}