﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityAuditProgramTermService : IService<QualityAuditProgramTermBO>
    {
        void InsertTermRange(List<QualityAuditProgramTermBO> data, Guid programId, bool autoSave = true);
    }
}
