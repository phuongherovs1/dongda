﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IQualityMeetingPlanReportIssueService : IService<QualityMeetingPlanReportIssueBO>
    {
        /// <summary>
        /// Danh sách vấn đề báo 
        /// </summary>
        /// <param name="meetingPlanReportId"></param>
        /// <returns></returns>
        IEnumerable<QualityIssueBO> GetIssueReport(Guid meetingPlanReportId);

        Guid AddIssue(Guid issueId, Guid meetingPlanReportId);

        /// <summary>
        /// Danh sách 
        /// </summary>
        /// <param name="meetingPlanReportId"></param>
        /// <returns></returns>
        IEnumerable<QualityMeetingPlanReportIssueBO> GetIssueMeetingReport(Guid meetingPlanReportId);
    }
}