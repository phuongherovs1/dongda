﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IQualityAuditPlanService : IService<QualityAuditPlanBO>
    {
        /// <summary>
        /// Thêm mới kế hoạch
        /// </summary>
        /// <param name="data"></param>
        /// <param name="qualityAuditPlanStandards"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        Guid InsertPlan(QualityAuditPlanBO data, string qualityAuditPlanStandards = default(string), bool allowSave = true);
        /// <summary>
        /// Cập nhật kế hoạch
        /// </summary>
        /// <param name="data"></param>
        /// <param name="qualityAuditPlanStandards"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        void UpdatePlan(QualityAuditPlanBO data, string qualityAuditPlanStandards = default(string), bool allowSave = true);
        /// <summary>
        /// Kiểm tra xem có kế hoạch nào tồn tại chưa
        /// </summary>
        /// <param name="isRountine"></param>
        /// <param name="timeValue"></param>
        /// <param name="unitTime"></param>
        /// <param name="planIds"></param>
        bool CheckExits(Guid? planId, bool? isRountine, int? timeValue, int? unitTime, List<Guid> iSOStandardIds);
        IEnumerable<QualityAuditPlanBO> GetAll();
    }
}
