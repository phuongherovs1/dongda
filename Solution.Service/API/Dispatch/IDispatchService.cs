﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDispatchService : IService<DispatchBO>
    {
        DispatchBO CreateDefault(Guid CategoryId, Guid departmentId);
        Guid InsertDispatch(DispatchBO data, string jsonData, bool allowSave = true);
        void UpdateDispatch(DispatchBO data, string jsonData, bool allowSave = true);
        DispatchBO GetDataById(Guid Id);
        DispatchBO GetDataByIdCheckReview(Guid Id);
        DispatchBO GetDataByIdCheckAproval(Guid Id);
        IEnumerable<DispatchBO> GetDataDispatch(int pageIndex, int pageSize, out int count, Guid? CategoryId, string filterName = default(string), Common.Resource.DispatchStatus status = 0);

        void ReView(DispatchBO data);
        void Approval(DispatchBO data);
    }
}
