﻿using System;
using System.Data;

namespace iDAS.Service.API.Dispatch
{
    public interface IDispatchSearchService
    {
        DataTable GetData(string numberDispatch, string name, DateTime sentDateFrom, DateTime sentDateTo, string content, Guid? securityID, Guid? urgencyID, bool? isGo, int status, int pageIndex, int pageSize, out int count);
    }
}
