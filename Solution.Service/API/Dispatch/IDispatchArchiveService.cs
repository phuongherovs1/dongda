﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDispatchArchiveService : IService<DispatchArchiveBO>
    {
        DispatchArchiveBO CreateDefault(string dispatchId, string departmentId);

        /// <summary>
        /// Thêm công văn đã lưu trữ vào hệ thống
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        DispatchArchiveBO CreateDefault(string departmentId);

        /// <summary>
        /// Thêm mới hoặc cập nhật lưu trữ công văn
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid InsertOrUpdate(DispatchArchiveBO data);

        /// <summary>
        /// Danh sách công văn lưu trữ theo phòng ban
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filter"></param>
        /// <param name="archiveType"></param>
        /// <param name="department"></param>
        /// <returns></returns>
        IEnumerable<DispatchArchiveBO> GetDispatchArchive(int pageIndex, int pageSize, out int count, string filter, Resource.ProfileArchiveTime archiveType, string department);

        /// <summary>
        /// Thêm công văn đã lưu trữ vào hệ thống
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid InsertDispatchArchiveOld(DispatchArchiveBO data);
    }
}
