﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDispatchAttachmentService : IService<DispatchAttachmentBO>
    {
        IEnumerable<Guid> GetFileIds(Guid DispatchId);

        IEnumerable<Guid> GetFileSources(Guid DispatchId);
    }
}
