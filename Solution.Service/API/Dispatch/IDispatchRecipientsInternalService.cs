﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDispatchRecipientsInternalService : IService<DispatchRecipientsInternalBO>
    {
        void InsertInternal(DispatchRecipientsInternalBO item, string datajson = "", bool allowSave = true);
        IEnumerable<DispatchRecipientsInternalBO> GetDataInternal(int pageIndex, int pageSize, out int count, Guid DispatchId);
        IEnumerable<DispatchBO> GetDataDispatchInternal(int pageIndex, int pageSize, out int count, Guid? DepartmentId);

        /// <summary>
        /// Đếm số lượng công văn đến chưa xác nhận
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        int CountDispatchInternal(string department);
    }
}
