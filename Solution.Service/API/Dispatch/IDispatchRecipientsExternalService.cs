﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDispatchRecipientsExternalService : IService<DispatchRecipientsExternalBO>
    {
        IEnumerable<DispatchRecipientsExternalBO> GetDataExternal(int pageIndex, int pageSize, out int count, Guid DispatchId);
    }
}
