﻿using System;

namespace iDAS.Service
{
    public interface IDispatchTaskService : IService<DispatchTaskBO>
    {
        void DeleteTask(Guid id);
    }
}
