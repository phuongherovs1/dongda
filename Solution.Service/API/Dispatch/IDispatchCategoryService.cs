﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDispatchCategoryService : IService<DispatchCategorieBO>
    {
        DispatchCategorieBO GetDetail(string id, string departmentId, string categoryId);
        IEnumerable<DispatchCategorieBO> GetTreeDispatchCategory(Guid? parentId, Guid departmentId);
        IEnumerable<DispatchCategorieBO> GetTreeDispatchCategoryTo(Guid? parentId, Guid departmentId);
        void DeleteCategory(Guid id);
        IEnumerable<DispatchCategorieBO> GetByDepartment(string departmentId, string categoryId = default(string));
        IEnumerable<DispatchCategorieBO> GetDataCategoryTo(Guid departmentId);

        /// <summary>
        /// Lấy nhóm công văn đến hoặc đi theo phòng ban
        /// </summary>
        /// <param name="department"></param>
        /// <param name="dispatchForm"></param>
        /// <returns></returns>
        IEnumerable<DispatchCategorieBO> GetCategoryToOrFormByDepartment(string department, bool dispatchForm);
    }
}
