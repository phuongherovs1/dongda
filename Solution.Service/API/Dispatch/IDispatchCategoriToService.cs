﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDispatchCategoriToService : IService<DispatchCategoriToBO>
    {
        IEnumerable<DispatchBO> GetDataDispatchTo(int pageIndex, int pageSize, out int count, Guid? CategoryId, string filterName = default(string), Common.Resource.DispatchStatus status = 0);
    }
}
