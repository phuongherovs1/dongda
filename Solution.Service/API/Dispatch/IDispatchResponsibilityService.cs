﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDispatchResponsibilityService : IService<DispatchResponsibilitieBO>
    {
        IEnumerable<DispatchResponsibilitieBO> GetByDispatch(int pageIndex, int pageSize, out int count, string DispatchId);
    }
}
