﻿using System;
using System.Collections.Generic;
using System.Data;

namespace iDAS.Service
{
    public interface IFormApproveStatusService : IService<FormApproveStatusBO>
    {
        IEnumerable<FormApproveStatusBO> GetByType(string tableName);
        IEnumerable<FormApproveStatusBO> GetByFormId(Guid id);
    }
}
