﻿using System.Data;

namespace iDAS.Service.API.TimeKeeping
{
    public interface IPermissionService
    {
        DataTable Permission_GetData(int type, string name = "", string code = "");
        bool Delete(string id);

        bool Insert(string id);
        bool Update(string Code, string tMonth, string tDate, string TimeIn, string TimeOut);
    }
}
