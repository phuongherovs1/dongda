﻿namespace iDAS.Service
{
    public interface IISOProcessStepService : IService<ISOProcessStepBO>
    {
        void Save(ISOProcessBO data, bool allowSave = true);
    }
}
