﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IISOProcessService : IService<ISOProcessBO>
    {
        IEnumerable<ISOProcessBO> GetByDepartment(Guid? departmentId);

        ISOProcessBO GetFormItem(string id);

        ISOProcessBO GetGraphData(Guid id);
    }
}
