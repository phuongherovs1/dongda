﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IISOProcessRelationshipService : IService<ISOProcessRelationshipBO>
    {
        IEnumerable<ISOProcessRelationshipBO> GetDataRelationship(int pageIndex, int pageSize, out int count, Guid StepId);
    }
}
