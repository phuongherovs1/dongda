﻿using iDAS.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace iDAS.Service
{
    public interface IService
    {
        void SetDbContext(IUnitOfWork unitOfWork);
    }
    public interface IService<T> : IService
    {
        IEnumerable<T> GetAll(bool allowDeleted = false);
        IEnumerable<T> Get(Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IQueryable<T>> include = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderByDescending = null, bool allowDeleted = false);
        IQueryable<T> GetQuery(bool allowDeleted = false);
        T GetById(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true);
        T GetJustById(Guid id, bool allowDefaultIfNull = true);
        IQueryable<T> GetQueryById(Guid id, bool allowDeleted = false);
        IEnumerable<T> GetByIds(IEnumerable<Guid> ids, bool allowDeleted = false);
        IQueryable<T> GetByIds(IQueryable<Guid> ids, bool allowDeleted = false);
        IEnumerable<T> GetNotInIds(IEnumerable<Guid> ids, bool allowDeleted = false);
        IQueryable<T> GetNotInIds(IQueryable<Guid> ids, bool allowDeleted = false);
        Guid Insert(T data, bool allowSave = true);
        IEnumerable<Guid> InsertRange(IEnumerable<T> data, bool allowSave = true);
        void Update(T dto, bool allowSave = true);
        void Delete(Guid id, bool allowSave = true);
        void DeleteRange(IEnumerable<Guid> ids, bool allowSave = true);
        void Revert(Guid id, bool allowSave = true);
        void RevertRange(IEnumerable<Guid> ids, bool allowSave = true);
        void Remove(Guid id, bool allowSave = true);
        void RemoveRange(IEnumerable<Guid> ids, bool allowSave = true);
        void SaveTransaction(bool allowSaveTransaction = true);
        Guid UserId { get; }
        IEnumerable<T> Page(out int count, int pageIndex = 1, int pageSize = 20, bool allowDeleted = false);
        IEnumerable<T> Page(IEnumerable<T> data, int pageIndex, int pageSize);
        IQueryable<T> Page(IQueryable<T> data, int pageIndex, int pageSize);
        EmployeeBO GetUserCurrent();
    }
}
