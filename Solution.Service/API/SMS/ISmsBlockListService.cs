﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ISmsBlockListService : IService<SmsBlockListBO>
    {
        Guid Insert(SmsBlockListBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        void Update(SmsBlockListBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        IEnumerable<SmsBlockListBO> GetAll(int pageIndex, int pageSize, out int count, bool allowDeleted = false);
        IEnumerable<SmsBlockListBO> GetByfilterName(int pageIndex, int pageSize, string filterName, DateTime DateFrom, DateTime DateTo, out int count, bool allowDeleted = false);

        SmsBlockListBO GetFormItem();
        int Import(List<SmsBlockListBO> assetImport);

        List<SmsManagersSynchronizedBO> GetDataSMSWarning(int pageIndex, int pageSize, string filterName, out int count);
    }
}
