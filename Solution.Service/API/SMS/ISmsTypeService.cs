﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ISmsTypeService : IService<SmsTypeBO>
    {
        Guid Insert(SmsTypeBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        void Update(SmsTypeBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        IEnumerable<SmsTypeBO> GetAll(int pageIndex, int pageSize, out int count);
        IEnumerable<SmsTypeBO> GetDataByStringQuery(string query);
    }
}
