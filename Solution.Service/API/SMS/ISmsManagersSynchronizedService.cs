﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ISmsManagersSynchronizedService : IService<SmsManagersSynchronizedBO>
    {
        Guid Insert(SmsManagersSynchronizedBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        void Update(SmsManagersSynchronizedBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        IEnumerable<SmsManagersSynchronizedBO> GetAll(int pageIndex, int pageSize, out int count);
        IEnumerable<SmsManagersSynchronizedBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count);
        IEnumerable<SmsManagersSynchronizedBO> GetDataByStringQuery(string query, int pageIndex, int pageSize, out int count);

        SmsManagersSynchronizedBO GetFormItem();
        void Import(List<SmsManagersSynchronizedBO> assetImport);
    }
}
