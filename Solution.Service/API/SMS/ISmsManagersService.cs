﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ISmsManagersService : IService<SmsManagersBO>
    {
        Guid Insert(SmsManagersBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        void Update(SmsManagersBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        IEnumerable<SmsManagersBO> GetAll(int pageIndex, int pageSize, out int count);
        IEnumerable<SmsManagersBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count);

        SmsManagersBO GetFormItem();
        int Import(List<SmsManagersBO> assetImport);
    }
}
