﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ISmsFormContentService : IService<SmsFormContentBO>
    {
        Guid Insert(SmsFormContentBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        void Update(SmsFormContentBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        IEnumerable<SmsFormContentBO> GetAll(int pageIndex, int pageSize, out int count, bool allowDeleted = false);
        IEnumerable<SmsFormContentBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count, bool allowDeleted = false);
        SmsFormContentBO GetFormItem();
    }
}
