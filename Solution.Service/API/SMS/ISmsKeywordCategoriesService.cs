﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ISmsKeywordCategoriesService : IService<SmsKeywordCategoriesBO>
    {
        Guid Insert(SmsKeywordCategoriesBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        void Update(SmsKeywordCategoriesBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        IEnumerable<SmsKeywordCategoriesBO> GetAll(int pageIndex, int pageSize, out int count);
        IEnumerable<SmsKeywordCategoriesBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count);
        IEnumerable<SmsKeywordCategoriesBO> GetDataByStringQuery(string query);
        IEnumerable<OrganizationTreeBO> GetByNode();
        SmsKeywordCategoriesBO GetFormItem();
        int Import(List<SmsKeywordCategoriesBO> assetImport);
    }
}
