﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ISmsKeywordsService : IService<SmsKeywordsBO>
    {
        Guid Insert(SmsKeywordsBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        void Update(SmsKeywordsBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        IEnumerable<SmsKeywordsBO> GetAll(int pageIndex, int pageSize, out int count);
        IEnumerable<SmsKeywordsBO> GetByfilterName(int pageIndex, int pageSize, string filterName, Guid keywordCategoryId, out int count);

        SmsKeywordsBO GetFormItem();
        int Import(List<SmsKeywordsBO> assetImport);
    }
}
