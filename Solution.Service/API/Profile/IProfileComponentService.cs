﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IProfileComponentService : IService<ProfileComponentBO>
    {
        // Cập nhật các quy tắc thành phần hồ sơ
        void UpdateProfileComponentPrinciple(Guid profileId);
        /// <summary>
        /// Lấy thành phần hồ sơ theo hồ sơ
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="categoryId"></param>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<ProfileComponentBO> GetByProfile(int pageIndex, int pageSize, out int count, Guid profileId, Common.Resource.ProfileComponentStatus status = 0, string query = default(string));
        /// <summary>
        /// Lấy chi tiết thành phần hồ sơ
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ProfileComponentBO GetDetail(Guid id);
        /// <summary>
        /// Thêm mới thành phần hồ sơ
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid InsertProfilecomponent(ProfileComponentBO data);
        /// <summary>
        /// Cập nhật thành phần hồ sơ
        /// </summary>
        /// <param name="data"></param>
        void UpdateProfilecomponent(ProfileComponentBO data);

        // Sử dụng thành phần hồ sơ
        void UseComponent(Guid id);
        // Loại bỏ thành phần hồ sơ
        void CancelComponent(Guid id);

        /// <summary>
        /// Kiểm tra người đăng nhập co phải người tạo hồ sơ không:
        /// - Nếu người đăng nhập là người tạo hồ sơ và người được giao phân công thì hiển thị chức năng Thêm và Xóa, ngược lại thì ẩn
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        bool CheckRoleAddOrDelete(Guid profileId);

        /// <summary>
        /// Nếu người đăng nhập là người tạo hồ sơ hoặc người được phân công thì mới thực hiện được chức năng Cập nhật
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        bool CheckRoleUpdate(Guid profileId);

        /// <summary>
        /// Nếu người đăng nhập là người tạo hồ sơ hoặc người được phân công thì mới thực hiện được chức năng Lưu
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        bool CheckRoleSaveProfileComponent(Guid profileId);

        /// <summary>
        /// Thông tin hồ sơ
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        ProfileBO GetProfileInfo(Guid Id);
    }
}