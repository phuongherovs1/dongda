﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IProfileSecurityService : IService<ProfileSecurityBO>
    {
        /// <summary>
        /// Tìm kiếm danh sách mức độ bảo mật theo tên
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        IEnumerable<ProfileSecurityBO> GetContain(string name);
        /// <summary>
        /// Kiểm tra mức độ bảo mật đã tồn tại chưa ?
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool CheckExist(string name);
    }
}
