﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IProfileResponsibilityService : IService<ProfileResponsibilityBO>
    {
        /// <summary>
        /// Lấy danh sách trách nhiệm liên quan theo hồ sơ
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="categoryId"></param>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<ProfileResponsibilityBO> GetByProfile(int pageIndex, int pageSize, out int count, Guid profileId, Common.Resource.ProfileResponsibilityStatus status, string query = default(string));
        /// Lấy chi tiết thông tin trách nhiệm liên quan
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ProfileResponsibilityBO GetDetail(Guid id);
        /// <summary>
        /// Thêm mới thông tin trách nhiệm liên quan
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid InsertProfile(ProfileResponsibilityBO data);
        /// <summary>
        /// Cập nhật thông tin trách nhiệm liên quan
        /// </summary>
        /// <param name="data"></param>
        void UpdateProfile(ProfileResponsibilityBO data);

        void SendProfileResponsibilityById(Guid id);

        void RevertResponsibility(Guid id);

        /// <summary>
        /// Xóa trách nhiệm
        /// </summary>
        /// <param name="id"></param>
        void DeleteResponsibility(Guid id);

        /// <summary>
        /// Khởi tạo giá trị mặc định khi thêm mới trách nhiệm
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        ProfileResponsibilityBO CreateDefault(string profileId);
    }
}