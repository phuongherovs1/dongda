﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IProfileCategoryService : IService<ProfileCategoryBO>
    {
        /// <summary>
        /// Lấy cây danh mục hồ sơ theo phòng ban và danh mục cha
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        IEnumerable<ProfileCategoryBO> GetTreeProfileCategory(Guid? parentId, Guid departmentId);
        /// <summary>
        /// Lấy chi tiết thông tin danh mục hồ sơ
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ProfileCategoryBO GetDetail(string id, string departmentId, string categoryId);
        /// <summary>
        /// Thêm mới thông tin danh mục hồ sơ
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid InsertProfileCategory(ProfileCategoryBO data);
        /// <summary>
        /// Cập nhật thông tin danh mục hồ sơ
        /// </summary>
        /// <param name="data"></param>
        void UpdateProfileCategory(ProfileCategoryBO data);

        /// <summary>
        /// Danh sách danh mục theo phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        IEnumerable<ProfileCategoryBO> GetByDepartment(string departmentId, string categoryId = default(string));

        /// <summary>
        /// Xóa danh mục
        /// </summary>
        /// <param name="id"></param>
        void DeleteCategory(Guid id);
    }
}