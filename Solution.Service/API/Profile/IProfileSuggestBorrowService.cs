﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IProfileSuggestBorrowService : IService<ProfileSuggestBorrowBO>
    {
        IEnumerable<ProfileSuggestBorrowBO> GetByCurrentUser();
        ProfileSuggestBorrowBO GetDetail(Guid id);
        void ApprovalSuggest(ProfileSuggestBorrowBO item);
    }
}
