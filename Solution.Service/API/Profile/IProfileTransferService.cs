﻿using System;

namespace iDAS.Service
{
    public interface IProfileTransferService : IService<ProfileTransferBO>
    {
        /// <summary>
        /// Thêm phiếu chuyển lưu trữ hồ sơ
        /// </summary>
        /// <param name="item"></param>
        void TransferProfile(ProfileTransferBO item);

        ProfileTransferBO GetByProfile(Guid profileId);
        void ReceiveProfile(ProfileTransferBO item);
    }
}
