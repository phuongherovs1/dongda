﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IProfileAssignService : IService<ProfileAssignBO>
    {
        /// <summary>
        /// Lấy danh sách hồ sơ theo danh mục hồ sơ
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="categoryId"></param>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<ProfileAssignBO> GetByProfile(int pageIndex, int pageSize, out int count, Guid profileId, Common.Resource.ProfileAssignStatus status, string query = default(string));
        /// Lấy chi tiết thông tin hồ sơ
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ProfileAssignBO GetDetail(Guid id);
        /// <summary>
        /// Thêm mới thông tin hồ sơ
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid InsertProfile(ProfileAssignBO data);
        /// <summary>
        /// Cập nhật thông tin hồ sơ
        /// </summary>
        /// <param name="data"></param>
        void UpdateProfile(ProfileAssignBO data);

        // gửi phân công, thực hiện lưu dữ liệu phân công và cập nhật trạng thái
        void SendProfileAssign(ProfileAssignBO data);
        // gửi phân công theo Id bản ghi. thực hiện cập nhật trạng thái cho phân công
        void SendProfileAssignById(Guid id);
        // Thu hồi phân công. --> Trạng thái hồ sơ trở về chờ phân công
        void RevertAssign(Guid id);
        // Hủy phân công
        void DestroyAssign(Guid id);

        /// <summary>
        /// Lấy thông tin hồ sơ
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        ProfileBO GetProfileInfo(Guid Id);

        /// <summary>
        /// Xóa nhân sự được giao cập nhật hồ sơ
        /// </summary>
        /// <param name="id"></param>
        void DeleteAssign(Guid id);
    }
}