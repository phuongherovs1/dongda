﻿using System;
namespace iDAS.Service
{
    public interface IProfileAttachmentService : IService<ProfileAttachmentBO>
    {
        /// <summary>
        /// Cập nhật file đính kèm
        /// </summary>
        /// <param name="fileUpload"></param>
        /// <param name="profileId"></param>
        /// <param name="allowSave"></param>
        void Update(FileUploadBO fileUpload, Guid profileId, bool allowSave = true);
    }
}