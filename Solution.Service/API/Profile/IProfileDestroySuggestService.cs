﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IProfileDestroySuggestService : IService<ProfileDestroySuggestBO>
    {
        /// <summary>
        /// Danh sách đề nghị hủy hồ sơ
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<ProfileDestroySuggestBO> GetData(int pageIndex, int pageSize, out int count, iDAS.Service.Common.Resource.ProfileDestroyStatus status);

        /// <summary>
        /// Lưu thông tin đề nghị hủy
        /// </summary>
        /// <param name="data"></param>
        /// <param name="profiles"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        Guid Save(ProfileDestroySuggestBO data, string profiles, string departmentId, bool allowSave = true);

        /// <summary>
        /// Gửi thông tin đề nghị hủy
        /// </summary>
        /// <param name="data"></param>
        /// <param name="profiles"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        Guid Send(ProfileDestroySuggestBO data, string profiles, string departmentId, bool allowSave = true);

        /// <summary>
        /// Xóa đề nghị hủy và hồ sơ của đề nghị
        /// </summary>
        /// <param name="id"></param>
        void DeleteProfileDestroy(Guid id);

        /// <summary>
        /// Lấy những đề nghị hủy chưa duyệt đề cập nhật, nếu chưa có thì tạo mới
        /// Kiểm tra người đăng nhập có quyền tạo đề nghị hủy trong phòng ban này không
        /// </summary>
        /// <param name="departmentId"></param>
        ProfileDestroySuggestBO CreateDefault(string departmentId);

        /// <summary>
        /// Lấy bản ghi để phê duyệt
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ProfileDestroySuggestBO GetForApproval(string id);

        /// <summary>
        /// Phê duyệt đề nghị hủy
        /// </summary>
        /// <param name="data"></param>
        void Approve(ProfileDestroySuggestBO data);
    }
}