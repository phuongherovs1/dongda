﻿using System.Collections.Generic;
using System.Data;

namespace iDAS.Service
{
    public interface IProfileMasterService
    {
        int Insert(string id, string name, string type, string userId, int rank = default, string superId = "");
        int Update(string id, string name, string type, string userId, int rank = default);
        int Delete(string id, string type, string userId);
        DataTable GetMasterDataList(string type, string superId = "", string query = "");

        IEnumerable<ProfileMasterBO> ToEnumerable(string type, string superId = "");

        ProfileMasterBO GetSingleMasterData(string type, string id);
    }
}
