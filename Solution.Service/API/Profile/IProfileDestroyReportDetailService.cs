﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IProfileDestroyReportDetailService : IService<ProfileDestroyReportDetailBO>
    {
        /// <summary>
        /// Thêm nhân sự than gia hủy
        /// </summary>
        /// <param name="employeeIds"></param>
        /// <returns></returns>
        IEnumerable<Guid> Save(Guid reportId, string employeeIds, bool allowSave = true);
    }
}