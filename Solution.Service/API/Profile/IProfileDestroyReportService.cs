﻿using System;

namespace iDAS.Service
{
    public interface IProfileDestroyReportService : IService<ProfileDestroyReportBO>
    {
        /// <summary>
        /// Tạo biên bản mặc định
        /// Kiểm tra người đăng nhập đã được thiết lập quyền lưu trữ đối với phòng ban này chưa
        /// </summary>
        /// <returns></returns>
        ProfileDestroyReportBO CreateDefault(string departmentId, string ProfileDestroySuggest);

        /// <summary>
        /// Lưu biên bản hủy
        /// </summary>
        /// <param name="data"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        Guid Save(ProfileDestroyReportBO data, string employeeId);
    }
}