﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IProfilePrincipleService : IService<ProfilePrincipleBO>
    {
        /// <summary>
        /// Lấy danh sách nguyên tắc hồ sơ theo danh mục hồ sơ
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="categoryId"></param>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<ProfilePrincipleBO> GetByCategory(int pageIndex, int pageSize, out int count, Guid categoryId, string query = default(string));
        /// <summary>
        /// Lấy chi tiết thông tin nguyên tắc hồ sơ
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        ProfilePrincipleBO GetDetail(string id, string departmentId, string categoryId);
        /// <summary>
        /// Thêm mới thông tin nguyên tắc hồ sơ
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid InsertProfile(ProfilePrincipleBO data);
        /// <summary>
        /// Cập nhật thông tin nguyên tắc hồ sơ
        /// </summary>
        /// <param name="data"></param>
        void UpdateProfile(ProfilePrincipleBO data);

        /// <summary>
        /// Xóa hồ sơ nguyên tắc, xóa cả file đính kèm
        /// </summary>
        /// <param name="id"></param>
        void DeleteProfile(Guid id);

        /// <summary>
        /// Lấy danh sách tài liệu ban hành
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentPublishBO> GetDocumentPublish();

        /// <summary>
        /// Sắp xếp tăng
        /// </summary>
        /// <param name="id"></param>
        void SortASC(Guid id);

        /// <summary>
        /// Sắp xếp giảm 
        /// </summary>
        /// <param name="id"></param>
        void SortDESC(Guid id);

        /// <summary>
        /// Sử dụng
        /// </summary>
        /// <param name="id"></param>
        void Using(Guid id);

        /// <summary>
        /// Không sử dụng
        /// </summary>
        /// <param name="id"></param>
        void NotUse(Guid id);

        /// <summary>
        /// Kiểm tra người đăng nhập có phải là người tạo danh mục đang chọn hay không
        /// Nếu đúng thì hiện button thêm hồ sơ nguyên tắc, ngược lại ẩn button
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        bool ShowOrHideButton(string categoryId);
    }
}