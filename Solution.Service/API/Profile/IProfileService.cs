﻿
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IProfileService : IService<ProfileBO>
    {
        /// <summary>
        /// Lấy danh sách hồ sơ theo danh mục hồ sơ
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="categoryId"></param>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<ProfileBO> GetByCategory(int pageIndex, int pageSize, out int count, Guid? categoryId, string query = default(string), Common.Resource.ProfileStatus status = 0);
        /// <summary>
        /// Đếm số hồ sơ của danh mục hồ sơ
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        int CountProfilesByCategory(Guid categoryId);
        /// <summary>
        /// Lấy chi tiết thông tin hồ sơ
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ProfileBO GetDetail(Guid id);
        /// <summary>
        /// Thêm mới thông tin hồ sơ
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid InsertProfile(ProfileBO data);

        /// <summary>
        /// Lưu thông tin hồ sơ của nhân sự
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid InsertProfileEmployee(ProfileBO data);
        /// <summary>
        /// Cập nhật thông tin hồ sơ
        /// </summary>
        /// <param name="data"></param>
        void UpdateProfile(ProfileBO data);

        /// <summary>
        /// Cập nhật thông tin hồ sơ nhân sự
        /// </summary>
        /// <param name="data"></param>
        void UpdateProfileEmployee(ProfileBO data);

        // Lấy dữ liệu mặc định
        ProfileBO CreateDefault(Guid CategoryId, Guid departmentId);
        /// <summary>
        /// Lấy danh sách hồ sơ lưu trữ có phân trang
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filterName">Chuỗi filter</param>
        /// <param name="archiveType">Loại lưu trữ</param>
        /// <param name="status">Trạng thái hồ sơ</param>
        /// <param name="objectId">Id phòng ban hoặc cá nhân</param>
        /// <returns></returns>
        IEnumerable<ProfileBO> GetProfileArchives(int pageIndex, int pageSize, out int count, string filterName = default(string), Resource.ProfileArchiveTypes archiveType = 0, Resource.ProfileStatus status = 0, string objectId = default(string));

        /// <summary>
        /// Danh sách hồ sơ chờ hủy
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filterName"></param>
        /// <param name="archiveType"></param>
        /// <param name="status"></param>
        /// <param name="objectId"></param>
        /// <returns></returns>
        IEnumerable<ProfileBO> GetProfileWaitDestroy(int pageIndex, int pageSize, out int count, string filterName = default(string), Resource.ProfileArchiveTypes archiveType = 0, Resource.ProfileStatus status = 0, string objectId = default(string));
        /// <summary>
        /// Lấy danh sách hồ sơ mượn trả có phân trang
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filterName">Chuỗi filter</param>
        /// <param name="archiveType">Loại lưu trữ</param>
        /// <param name="status">Trạng thái mượn trả</param>
        /// <param name="objectId">Id phòng ban hoặc cá nhân</param>
        IEnumerable<ProfileBO> GetProfileBorrows(int pageIndex, int pageSize, out int count, string filterName = default(string), Resource.ProfileBorrowStatus status = 0, string objectId = default(string));
        IEnumerable<ProfileBO> GetProfileDestroys(int pageIndex, int pageSize, out int count, string filterName = default(string), string objectId = default(string));
        IEnumerable<ProfileBO> GetProfileTransfers(int pageIndex, int pageSize, out int count, string filterName = default(string), Resource.ProfileTransferStatus status = 0, string objectId = default(string));
        ProfileBO GetDetailDataProfile(Guid Id);
        ProfileBO GetProfileInfo(Guid Id);
        /// <summary>
        /// Xóa hồ sơ
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="allowSave"></param>
        void DeleteProfile(Guid Id, bool allowSave = true);

        /// <summary>
        /// Danh sách hồ sơ chờ hủy chưa được tạo đề nghị hủy để thêm vào đề nghị hủy
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<ProfileBO> GetProfileWaitDestroyForAddDestroySuggest(int pageIndex, int pageSize, out int count);


        ProfileBO CreateDefaultForHuman(Guid departmentId, string curiculmviateId);
        void UpdateProfileMulti(List<ProfileBO> data);
    }
}