﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IProfilePrincipleAttachmentService : IService<ProfilePrincipleAttachmentBO>
    {
        /// <summary>
        /// Lấy file đính kèm theo principleId
        /// </summary>
        /// <param name="principleId"></param>
        /// <returns></returns>
        IEnumerable<Guid> GetByPrinciple(Guid principleId);

        /// <summary>
        /// Lấy danh sách file theo danh sách principle
        /// </summary>
        /// <param name="principleIds"></param>
        /// <returns></returns>
        IEnumerable<ProfilePrincipleAttachmentBO> GetByPrinciples(IEnumerable<Guid> principleIds);

        /// <summary>
        /// Cập nhật file đính kèm theo principleId
        /// </summary>
        /// <param name="fileUpload"></param>
        /// <param name="principleId"></param>
        /// <param name="allowSave"></param>
        void Update(FileUploadBO fileUpload, Guid principleId, bool allowSave = true);
    }
}