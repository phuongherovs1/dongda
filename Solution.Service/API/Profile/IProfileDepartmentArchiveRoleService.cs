﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IProfileDepartmentArchiveRoleService : IService<ProfileDepartmentArchiveRoleBO>
    {
        // Lấy danh sách chức danh hoặc nhân sự có quyền lưu trữ
        IEnumerable<ProfileDepartmentArchiveRoleBO> GetByDepartment(Guid departmentId);
        // Lưu thông tin quyền lưu trữ
        IEnumerable<Guid> Save(string jsonData);
        bool CheckAllowByCurentUser(Guid departmentId);
    }
}