﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IProfileSummaryService
    {
        List<PieChartBO> ProfileStatusAnalytic(Guid departmentId);
        List<PieChartBO> ProfileBorrowAnalytic(Guid departmentId);
        /// <summary>
        /// Lấy danh sách công việc phân công cập nhật hồ sơ 
        /// Là những công việc hiện tại cần xử lý
        /// </summary>
        /// <returns></returns>
        IEnumerable<ProfileDasboardBO> GetDasboardAssignUpdate();
        /// <summary>
        /// Lấy danh sách đề nghị kiểm tra
        /// Là những công việc hiện tại cần xử lý
        /// </summary>
        /// <returns></returns>
        IEnumerable<ProfileDasboardBO> GetDasboardSuggestCheck();
        /// <summary>
        /// Lấy danh sách phê duyệt của đề nghị hủy, đề nghị mượn trả
        /// Là những công việc hiện tại cần xử lý
        /// </summary>
        /// <returns></returns>
        IEnumerable<ProfileDasboardBO> GetDasboardApproval();
        /// <summary>
        /// Lấy danh sách hồ sơ cần thực hiện cho việc lưu trữ
        /// Là những công việc hiện tại cần xử lý
        /// </summary>
        /// <returns></returns>
        IEnumerable<ProfileDasboardBO> GetDasboardArchire();
    }
}