﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IProfileDestroyDetailService : IService<ProfileDestroyDetailBO>
    {
        /// <summary>
        /// Danh sách hồ sơ của đề nghị hủy
        /// </summary>
        /// <param name="profileDestroySuggestId"></param>
        /// <returns></returns>
        IEnumerable<ProfileDestroyDetailBO> GetByProfileDestroySuggestId(Guid profileDestroySuggestId, string query, iDAS.Service.Common.Resource.ProfileStatus status);

        /// <summary>
        /// Lấy danh sách hồ sơ hủy trong biên bản hủy
        /// </summary>
        /// <param name="profileDestroySuggestId"></param>
        /// <param name="query"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<ProfileDestroyDetailBO> GetProfileDestroy(Guid profileDestroySuggestId, string query, iDAS.Service.Common.Resource.ProfileDestroyDetail status);

        /// <summary>
        /// Danh sách Id theo đề nghị hủy
        /// </summary>
        /// <param name="profileDestroySuggestId"></param>
        /// <returns></returns>
        IEnumerable<Guid> GetIdByProfileDestroySuggestId(Guid profileDestroySuggestId);

        /// <summary>
        /// Thêm hồ sơ của đề nghị hủy
        /// </summary>
        /// <param name="profileIds"></param>
        /// <param name="destroySuggestId"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        IEnumerable<Guid> InsertRange(string profileIds, Guid destroySuggestId, bool allowSave = true);

        /// <summary>
        /// Hiển thị thông tin hồ sơ để xem xét
        /// </summary>
        /// <param name="id"></param>
        ProfileDestroyDetailBO Review(Guid id);

        /// <summary>
        /// Cập nhật hồ sơ của đề nghị hủy
        /// </summary>
        /// <param name="data"></param>
        void UpdateDetroyDetail(ProfileDestroyDetailBO data);

        /// <summary>
        /// Cập nhật kết quả hủy
        /// </summary>
        /// <param name="data"></param>
        void UpdateDestroyResult(ProfileDestroyDetailBO data);
    }
}