﻿using System;
using System.Collections.Generic;
namespace iDAS.Service
{
    public interface IProfileComponentAttachmentService : IService<ProfileComponentAttachmentBO>
    {
        /// <summary>
        /// Lấy file đính kèm theo ComponentId
        /// </summary>
        /// <param name="ComponentId"></param>
        /// <returns></returns>
        IEnumerable<Guid> GetByComponent(Guid componentId);

        /// <summary>
        /// Lấy danh sách file theo danh sách Component
        /// </summary>
        /// <param name="ComponentIds"></param>
        /// <returns></returns>
        IEnumerable<ProfileComponentAttachmentBO> GetByComponents(IEnumerable<Guid> componentIds);

        /// <summary>
        /// Cập nhật file đính kèm theo ComponentId
        /// </summary>
        /// <param name="fileUpload"></param>
        /// <param name="ComponentId"></param>
        /// <param name="allowSave"></param>
        void Update(FileUploadBO fileUpload, Guid componentId, bool allowSave = true);
    }
}