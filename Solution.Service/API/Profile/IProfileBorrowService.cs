﻿using System;

namespace iDAS.Service
{
    public interface IProfileBorrowService : IService<ProfileBorrowBO>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        ProfileBorrowBO GetByProfile(Guid profileId);
        ProfileBorrowBO GetBorrowById(Guid id);
        void Borrow(ProfileBorrowBO item);
        void AssignProfile(ProfileBorrowBO item);
        void ReturnProfile(ProfileBorrowBO item);
        bool ExitBorrow(Guid profileId);

    }
}
