﻿using System.Collections.Generic;

namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface IMenuService
    {
        List<MenuBO> GetData(int userId);
    }
}
