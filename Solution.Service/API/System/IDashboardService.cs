﻿namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface IDashboardService
    {
        DashboardBO GetData(string code);
    }
}
