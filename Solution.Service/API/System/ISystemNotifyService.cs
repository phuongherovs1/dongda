﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ISystemNotifyService : IService<SystemNotifyBO>
    {
        IEnumerable<SystemNotifyBO> GetByUser(Guid userId);
        IEnumerable<SystemNotifyBO> GetByCurrentUser();
        IEnumerable<SystemNotifyBO> GetMoreByCurrentUser(int start, int limit, out int count);
        int GetTotalNotRead();
        int UpdateRead(Guid id, bool isRead = true);
        /// <summary>
        /// send notify to emplyees
        /// </summary>
        /// <param name="title">Tiêu đề thông báo</param>
        /// <param name="content">Nội dung thông báo</param>
        /// <param name="employeeIDs">Ids nhân sự</param>
        /// <param name="url">Đường dẫn của thông báo khi click vào có dạng: /Area/Controller/Action</param>
        /// <param name="param">tham 
        IEnumerable<string> Insert(string title, string content, IEnumerable<Guid> employeeIDs, string url, object param);
        /// <summary>
        /// send notify to emplyee
        /// </summary>
        /// <param name="title">Tiêu đề thông báo</param>
        /// <param name="content">Nội dung thông báo</param>
        /// <param name="employeeID">Id nhân sự</param>
        /// <param name="url">Đường dẫn của thông báo khi click vào có dạng: /Area/Controller/Action</param>
        /// <param name="param">tham số truyền vào action của url có dang: id:value </param>
        string Insert(string title, string content, Guid employeeID, string url, object param, int type = 0);
        int GetCountByCurrentUser();
        void Revert(Guid id);

    }
}
