﻿
using System;

namespace iDAS.Service
{
    public interface IAccountService : IService<AccountBO>
    {
        /// <summary>
        /// Lấy tài khoản của nhân sự
        /// </summary>
        /// <param name="employeeId">Id nhân sự</param>
        /// <returns></returns>
        AccountBO GetByEmployee(Guid employeeId);
        /// <summary>
        /// Kiểm tra xem nhân sự đã có tài khoản chưa
        /// </summary>
        /// <param name="employeeId">Id nhân sự</param>
        /// <returns>true: đã có tài khoản - false: chưa có tài khoản</returns>
        bool CheckAccountExits(Guid employeeId);
    }
}
