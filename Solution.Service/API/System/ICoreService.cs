﻿namespace iDAS.Service
{
    /// <summary>
    /// Interface of Core Service
    /// </summary>
    public interface ICoreService
    {
        bool UpdateDatabase(DatabaseBO item);
    }
}
