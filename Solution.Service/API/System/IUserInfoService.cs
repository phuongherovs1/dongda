﻿using System.Collections.Generic;

namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface IUserInfoService
    {
        List<UserInfoBO> GetInfo(int userId);
    }
}
