﻿using System;
using System.Data;

namespace iDAS.Service.API.System
{
    public interface IPermissionService
    {
        DataTable ViewDataMenu(Guid IdUser);
        DataTable GetListRolePermission(Guid IdUser, int RoleID);
        DataTable GetListTreeRole(int ID);
        bool UpdateUserRole(Guid IdUser, int RoleID, string Ispermission, string IsRoleEdit);
        DataTable GetListRoleTitlePermission(Guid DepartmentTitleId, int RoleID);
        bool UpdateUserRoleTitle(Guid DepartmentTitleId, int RoleID, string Ispermission, string IsRoleEdit);


    }
}
