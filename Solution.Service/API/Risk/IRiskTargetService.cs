﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IRiskTargetService : IService<RiskTargetBO>
    {
        IEnumerable<RiskTargetBO> GetPolicy(bool allowDeleted = false);
        IEnumerable<RiskTargetBO> GetScene(Guid departmentId, int Year, bool allowDeleted = false);
        IEnumerable<RiskTargetBO> GetTarget(Guid departmentId, int Year, bool allowDeleted = false);
        RiskTargetBO CreateDefault(Guid departmentId);
        RiskTargetBO GetdatabyId(Guid Id);
        List<ListYearBO> GetYear(Guid departmentId);
    }
}
