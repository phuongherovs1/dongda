﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IRiskTargetPlanService : IService<RiskTargetPlanBO>
    {
        IEnumerable<RiskTargetPlanBO> GetDataRiskTargetPlan(int pageIndex, int pageSize, out int count, Guid? RiskTargetId, string filterName = default(string));
    }
}
