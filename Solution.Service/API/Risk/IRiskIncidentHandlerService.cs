﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IRiskIncidentHandlerService : IService<RiskIncidentHandlerBO>
    {
        IEnumerable<RiskIncidentHandlerBO> GetAll(int pageIndex, int pageSize, out int count, string incidentId = default(string));
    }
}
