﻿using System;

namespace iDAS.Service
{
    public interface IRiskTargetTaskService : IService<RiskTargetTaskBO>
    {
        void DeleteTask(Guid id);
    }
}
