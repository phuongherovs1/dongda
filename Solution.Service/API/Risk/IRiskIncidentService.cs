﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IRiskIncidentService : IService<RiskIncidentBO>
    {
        IEnumerable<RiskIncidentBO> GetAll(int pageIndex, int pageSize, out int count, string departmentId = default(string));
        int Import(List<RiskIncidentBO> riskIncidentsImport);
    }
}
