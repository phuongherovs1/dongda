﻿namespace iDAS.Service
{
    public interface IRiskPlanAuditService : IService<RiskPlanAuditBO>
    {
        void Save(RiskPlanAuditBO item, string jsonData = default(string), bool autoSave = true);
    }
}
