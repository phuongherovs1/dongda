﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IRiskAssetAuditService : IService<RiskAssetAuditBO>
    {
        IEnumerable<RiskAssetAuditBO> GetAll(int pageIndex, int pageSize, out int count);
    }
}
