﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IRiskHandleService : IService<RiskHandleBO>
    {
        IEnumerable<RiskHandleBO> GetAll(int pageIndex, int pageSize, out int count);
    }
}
