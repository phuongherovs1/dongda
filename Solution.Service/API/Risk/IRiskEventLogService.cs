﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IRiskEventLogService : IService<RiskEventLogBO>
    {
        IEnumerable<RiskEventLogBO> GetAll(int pageIndex, int pageSize, out int count, string departmentId = default(string));
        int Import(List<RiskEventLogBO> eventLogsImport);
    }
}
