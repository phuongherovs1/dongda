﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ICalendarService : IService<CalendarBO>
    {
        TimeSpan GetWorkingDay(Guid? calendarID, DateTime startDate, DateTime endDate);

        IEnumerable<CalendarBO> GetActive();
    }
}
