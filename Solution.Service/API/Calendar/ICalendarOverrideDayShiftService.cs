﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface ICalendarDayOverrideShiftService : IService<CalendarDayOverrideShiftBO>
    {
        /// <summary>
        /// Lấy danh sách ca của ngày thay đổi
        /// </summary>
        /// <param name="calendarOverrideId"></param>
        /// <returns></returns>
        IEnumerable<CalendarDayOverrideShiftBO> GetByCalendarDayOverride(Guid calendarOverrideId);
        IEnumerable<CalendarDayOverrideShiftBO> GetByCalendar(Guid calendarId);
        IEnumerable<CalendarDayOverrideShiftBO> GetByDate(Guid calendarId, DateTime date);
        /// <summary>
        /// copy all calendar
        /// </summary>
        /// <param name="fromCalendarOverride"></param>
        /// <param name="toCalendarOverride"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        IEnumerable<Guid> CopyCalendarShiftOverride(Guid fromCalendarOverride, Guid toCalendarOverride, bool allowSave = false);

        /// <summary>
        /// Copy shift to override 
        /// Source: calendar day shift
        /// Destination: calendar day shift override
        /// </summary>
        /// <param name="calendarOverrideId"></param>
        /// <param name="calendarShift"></param>
        /// <param name="allowSave"></param>
        /// <returns></returns>
        IEnumerable<Guid> InsertRange(Guid calendarOverrideId, Guid calendarId, int dayType, bool allowSave = false);
    }
}
