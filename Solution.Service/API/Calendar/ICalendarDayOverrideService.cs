﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface ICalendarDayOverrideService : IService<CalendarDayOverrideBO>
    {
        /// <summary>
        /// Lấy ngày thay đổi theo lịch làm việc
        /// </summary>
        /// <param name="calendarId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        CalendarDayOverrideBO GetByCalendarAndDate(Guid calendarId, DateTime date);
        /// Lấy danh sách ngày thay đổi của lịch
        /// </summary>
        /// <param name="calendarId"></param>
        /// <returns></returns>
        IEnumerable<CalendarDayOverrideBO> GetByCalendar(Guid calendarId);
        /// <summary>
        /// Kiểm tra xem ngày đã được thay đổi chưa
        /// </summary>
        /// <param name="calendarId"></param>
        /// <param name="?"></param>
        /// <param name="date"></param>
        /// <returns>true: đã thay đổi - false: chưa thay đổi</returns>
        bool CheckDayOverrideHasOverrided(Guid calendarId, DateTime date);

        CalendarDayOverrideBO InsertUpdate(CalendarDayOverrideBO data, bool allowSave = true);
    }
}
