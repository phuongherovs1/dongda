﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface ICalendarWeekShiftService : IService<CalendarWeekShiftBO>
    {
        /// <summary>
        /// Lấy danh sách ca của tuần theo lịch làm việc
        /// </summary>
        /// <param name="calendarId"></param>
        /// <returns></returns>
        IEnumerable<object> GetWeekShiftCalendar(Guid calendarId);
        /// <summary>
        /// Lấy ca làm việc của tuần theo rowId và kiểu ngày
        /// </summary>
        /// <param name="rowId"></param>
        /// <param name="dayType">kiểu ngày</param>
        /// <returns></returns>
        CalendarWeekShiftBO GetByRowIDAndDayType(Guid rowId, int dayType);
        /// <summary>
        /// Lấy lịch theo rowId
        /// </summary>
        /// <param name="rowId"></param>
        /// <returns></returns>
        Guid GetCalendarByRowID(Guid rowId);
        /// <summary>
        /// Lấy danh sách ca theo lịch làm việc và  kiểu ngày
        /// </summary>
        /// <param name="calendarId"></param>
        /// <param name="dayType"></param>
        /// <returns></returns>
        IEnumerable<CalendarWeekShiftBO> GetByCalendarAndDayType(Guid calendarId, int dayType);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="calendarId"></param>
        /// <param name="dayType"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        bool CheckTimeShiftExits(Guid calendarId, int dayType, TimeSpan startTime, TimeSpan endTime);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="calendarId"></param>
        /// <param name="dayType"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        bool CheckTimeShiftExitsEdit(Guid id, Guid calendarId, int dayType, TimeSpan startTime, TimeSpan endTime);

    }
}
