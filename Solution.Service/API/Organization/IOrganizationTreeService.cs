﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IOrganizationTreeService
    {
        /// <summary>
        /// Load danh sách node theo node cha
        /// 1. Khi chọn phòng ban thì hiển thị phòng ban trực thuộc + chức danh
        /// 2. Khi chọn chức danh thì hiển thị nhân sự theo chức danh
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        IEnumerable<OrganizationTreeBO> GetByNode(string node, bool showTitle = false, bool showEmployee = false, string onlyTitle = default(string));
    }
}
