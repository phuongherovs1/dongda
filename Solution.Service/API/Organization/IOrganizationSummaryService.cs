﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IOrganizationSummaryService
    {
        List<PieChartBO> HumanSexAnalytic();
        List<PieChartBO> HumanAnalyticByDepartment();
    }
}
