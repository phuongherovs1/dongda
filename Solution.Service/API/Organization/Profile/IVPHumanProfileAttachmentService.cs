﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileAttachmentService : IService<VPHumanProfileAttachmentBO>
    {
        VPHumanProfileAttachmentBO GetdataById(Guid Id);
        void HumanProfileAttachment(VPHumanProfileAttachmentBO data, bool allowSave = true);
        IEnumerable<VPHumanProfileAttachmentBO> GetAll();
        IEnumerable<VPHumanProfileAttachmentBO> GetAllByEmployeeId(int p1, int p2, out int totalCount, Guid EmployeeID);
        IEnumerable<VPHumanProfileAttachmentBO> GetAllByEmployeeIdNotPaging(Guid EmployeeID);
    }
}
