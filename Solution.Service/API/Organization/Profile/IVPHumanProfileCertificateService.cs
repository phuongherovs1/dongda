﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileCertificateService : IService<VPHumanProfileCertificateBO>
    {
        VPHumanProfileCertificateBO GetdataById(Guid Id);
        IEnumerable<VPHumanProfileCertificateBO> GetAllByEmployeeID(Guid employeeId, int pageIndex, int pageSize, out int count);
        IEnumerable<VPHumanProfileCertificateBO> GetAll();
        void HumanProfileCertificate(VPHumanProfileCertificateBO data, bool allowSave = true);
        IEnumerable<VPHumanProfileCertificateBO> GetAllByEmployeeIDNotPaging(Guid employeeId);
    }
}
