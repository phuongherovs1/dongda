﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileSalaryService : IService<VPHumanProfileSalaryBO>
    {
        VPHumanProfileSalaryBO GetdataById(Guid Id);
        void HumanProfileSalary(VPHumanProfileSalaryBO data, bool allowSave = true);
        IEnumerable<VPHumanProfileSalaryBO> GetAll();
        IEnumerable<VPHumanProfileSalaryBO> GetAllByEmployeeId(int p1, int p2, out int totalCount, Guid EmployeeID);

        IEnumerable<VPHumanProfileSalaryBO> GetAllByEmployeeIdNotPaging(Guid EmployeeID);
    }
}
