﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileAllowanceService : IService<VPHumanProfileAllowanceBO>
    {
        IEnumerable<VPHumanProfileAllowanceBO> GetAllByEmployeeId(Guid employeeId, int pageIndex, int pageSize, out int count);
        IEnumerable<VPHumanProfileAllowanceBO> GetAll();
        IEnumerable<VPHumanProfileAllowanceBO> GetAllByEmployeeIdNotPaging(Guid employeeId);
        VPHumanProfileAllowanceBO GetdataById(Guid Id);
        void VPHumanProfileAllowance(VPHumanProfileAllowanceBO data, bool allowSave = true);
    }
}
