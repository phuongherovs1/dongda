﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileAttachmentFileService : IService<VPHumanProfileAttachmentFileBO>
    {
        IEnumerable<Guid> GetFileByProfileId(Guid Id);
    }
}
