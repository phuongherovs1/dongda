﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileWorkTrialResultService : IService<VPHumanProfileWorkTrialResultBO>
    {
        /// <summary>
        /// Tạo giá trị mặc định
        /// </summary>
        /// <param name="humanProfileWorkTrialId"></param>
        /// <returns></returns>
        VPHumanProfileWorkTrialResultBO CreateDefault(Guid humanProfileWorkTrialId);

        /// <summary>
        /// Thêm mới/sửa yêu cầu tự đánh giá của nhân viên thử việc
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid Create(VPHumanProfileWorkTrialResultBO data);

        /// <summary>
        /// Danh sách kết quả tự đánh giá
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="humanProfileWorkTrialId"></param>
        /// <returns></returns>
        IEnumerable<VPHumanProfileWorkTrialResultBO> GetByHumanProfileWorkTrialId(int pageIndex, int pageSize, out int count, Guid humanProfileWorkTrialId);

        /// <summary>
        /// Update danh sách kết quả tự đánh giá
        /// </summary>
        /// <param name="jsonData"></param>
        /// <param name="humanProfileWorkTrialId"></param>
        void UpdateProfileWorkTrial(string jsonData, Guid humanProfileWorkTrialId);
    }
}
