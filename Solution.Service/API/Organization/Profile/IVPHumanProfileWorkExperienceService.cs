﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileWorkExperienceService : IService<VPHumanProfileWorkExperienceBO>
    {
        VPHumanProfileWorkExperienceBO GetdataById(Guid Id);
        void HumanProfileExperience(VPHumanProfileWorkExperienceBO data, bool allowSave = true);
        VPHumanProfileWorkExperienceBO GetFormItem(string id, Guid employeeId);
        IEnumerable<VPHumanProfileWorkExperienceBO> GetAll(int p1, int p2, out int totalCount);
        IEnumerable<VPHumanProfileWorkExperienceBO> GetByEmployeeId(Guid employeeId, int pageIndex, int pageSize, out int count);

        IEnumerable<VPHumanProfileWorkExperienceBO> GetByEmployeeIdNotPaging(Guid employeeId);
    }
}
