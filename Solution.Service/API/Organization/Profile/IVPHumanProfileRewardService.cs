﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileRewardService : IService<VPHumanProfileRewardBO>
    {
        VPHumanProfileRewardBO GetdataById(Guid Id);
        void HumanProfileReward(VPHumanProfileRewardBO data, bool allowSave = true);
        IEnumerable<VPHumanProfileRewardBO> GetAllByEmployeeId(int p1, int p2, out int totalCount, Guid EmployeeID);
        IEnumerable<VPHumanProfileRewardBO> GetAll();
        IEnumerable<VPHumanProfileRewardBO> GetAllByEmployeeIdNotPaging(Guid EmployeeID);
    }
}
