﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileWorkTrialService : IService<VPHumanProfileWorkTrialBO>
    {
        /// <summary>
        /// Danh sách nhân sự thử việc
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<VPHumanProfileWorkTrialBO> GetAll(int pageIndex, int pageSize, out int count);

        /// <summary>
        /// Danh sách nhân sự thử việc
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<VPHumanProfileWorkTrialBO> GetByEmployee(int pageIndex, int pageSize, out int count);


        /// <summary>
        /// Danh sách nhân sự thử việc
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<VPHumanProfileWorkTrialBO> GetByEmployee(int pageIndex, int pageSize, out int count, Guid employeeId);

        IEnumerable<VPHumanProfileWorkTrialBO> GetByEmployeeNotPaging(Guid employeeId);

        /// <summary>
        /// Danh sách nhân sự thử việc theo người quản lý
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<VPHumanProfileWorkTrialBO> GetByManege(int pageIndex, int pageSize, out int count);

        /// <summary>
        /// Danh sách nhân sự thử việc chờ phê duyệt
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<VPHumanProfileWorkTrialBO> GetDataApproval(int pageIndex, int pageSize, out int count);

        /// <summary>
        /// Thêm mới/sửa nhân sự thử việc
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Guid Create(VPHumanProfileWorkTrialBO data, string manegeId, string roleId);

        /// <summary>
        /// Thông tin chi tiết nhân sự thử việc
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        VPHumanProfileWorkTrialBO GetDetail(string id);

        /// <summary>
        /// Phê duyệt nhân sự thử việc
        /// </summary>
        /// <param name="data"></param>
        void Approval(VPHumanProfileWorkTrialBO data);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="profileId"></param>
        /// <returns></returns>
        VPHumanProfileWorkTrialBO GetNewEmployeeTrialForm(string id, Guid profileId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="recruitmentResultId"></param>
        /// <returns></returns>
        VPHumanProfileWorkTrialBO GetNewEmployeeTrialForm(Guid profileId, Guid recruitmentResultId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="profileWorkTrial"></param>
        void CreateEmployeeTrial(VPHumanProfileWorkTrialBO profileWorkTrial);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="profileWorkTrial"></param>
        void CreateEmployee(VPHumanProfileWorkTrialBO profileWorkTrial);
    }
}
