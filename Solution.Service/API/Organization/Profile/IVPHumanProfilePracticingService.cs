﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfilePracticingService : IService<VPHumanProfilePracticingBO>
    {
        IEnumerable<VPHumanProfilePracticingBO> GetAllByEmployeeId(Guid employeeId, int pageIndex, int pageSize, out int count);
        IEnumerable<VPHumanProfilePracticingBO> GetAll();
        IEnumerable<VPHumanProfilePracticingBO> GetAllByEmployeeIdNotPaging(Guid employeeId);
        VPHumanProfilePracticingBO GetdataById(Guid Id);
        void VPHumanProfilePracticing(VPHumanProfilePracticingBO data, bool allowSave = true);
    }
}
