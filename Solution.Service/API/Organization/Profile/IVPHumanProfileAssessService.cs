﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileAssessService : IService<VPHumanProfileAssessBO>
    {
        IEnumerable<VPHumanProfileAssessBO> GetAllByEmployeeId(Guid employeeId, int pageIndex, int pageSize, out int count);
        IEnumerable<VPHumanProfileAssessBO> GetAll();
        IEnumerable<VPHumanProfileAssessBO> GetAllByEmployeeIdNotPaging(Guid employeeId);
    }
}
