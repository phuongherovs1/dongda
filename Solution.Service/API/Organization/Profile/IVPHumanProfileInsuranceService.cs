﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileInsuranceService : IService<VPHumanProfileInsuranceBO>
    {
        VPHumanProfileInsuranceBO GetdataById(Guid Id);
        void HumanProfileInsuranceBO(VPHumanProfileInsuranceBO data, bool allowSave = true);
        IEnumerable<VPHumanProfileInsuranceBO> GetAll();
        IEnumerable<VPHumanProfileInsuranceBO> GetAllByEmployeeId(int p1, int p2, out int totalCount, Guid EmployeeID);
        IEnumerable<VPHumanProfileInsuranceBO> GetAllByEmployeeIdNotPaging(Guid EmployeeID);
    }
}
