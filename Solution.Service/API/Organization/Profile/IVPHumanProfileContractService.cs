﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileContractService : IService<VPHumanProfileContractBO>
    {
        VPHumanProfileContractBO GetdataById(Guid Id);
        void HumanProfileContract(VPHumanProfileContractBO data, bool allowSave = true);
        IEnumerable<VPHumanProfileContractBO> GetAllByEmployeeId(int p1, int p2, out int totalCount, Guid EmployeeID);
        IEnumerable<VPHumanProfileContractBO> GetAll();
        IEnumerable<VPHumanProfileContractBO> GetAllByEmployeeIdNotPaging(Guid EmployeeID);
    }
}
