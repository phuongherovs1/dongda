﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileRelationshipService : IService<VPHumanProfileRelationshipBO>
    {
        VPHumanProfileRelationshipBO GetdataById(Guid Id);
        void HumanProfileRelationship(VPHumanProfileRelationshipBO data, bool allowSave = true);
        IEnumerable<VPHumanProfileRelationshipBO> GetAll();
        IEnumerable<VPHumanProfileRelationshipBO> GetAllByEmployeeId(int p1, int p2, out int totalCount, Guid EmployeeID);
        IEnumerable<VPHumanProfileRelationshipBO> GetAllByEmployeeIdNotPaging(Guid EmployeeID);
    }
}
