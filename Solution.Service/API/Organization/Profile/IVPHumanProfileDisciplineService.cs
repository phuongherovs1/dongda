﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileDisciplineService : IService<VPHumanProfileDisciplineBO>
    {

        IEnumerable<VPHumanProfileDisciplineBO> GetAllByEmployeeId(int p1, int p2, out int totalCount, Guid EmployeeID);
        IEnumerable<VPHumanProfileDisciplineBO> GetAll();
        VPHumanProfileDisciplineBO GetdataById(Guid Id);
        void HumanProfileDiscipline(VPHumanProfileDisciplineBO data, bool allowSave = true);
        IEnumerable<VPHumanProfileDisciplineBO> GetAllByEmployeeIdNotPaging(Guid EmployeeID);
    }
}
