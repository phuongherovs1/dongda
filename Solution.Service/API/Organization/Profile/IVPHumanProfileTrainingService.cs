﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileTrainingService : IService<VPHumanProfileTrainingBO>
    {
        VPHumanProfileTrainingBO GetdataById(Guid Id);
        void HumanProfileTraining(VPHumanProfileTrainingBO data, bool allowSave = true);
        IEnumerable<VPHumanProfileTrainingBO> GetAllByEmployeeId(Guid employeeId, int pageIndex, int pageSize, out int count);
        IEnumerable<VPHumanProfileTrainingBO> GetAll();
        IEnumerable<VPHumanProfileTrainingBO> GetAllByEmployeeIdNotPaging(Guid employeeId);
    }
}
