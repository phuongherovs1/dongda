﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileDiplomaService : IService<VPHumanProfileDiplomaBO>
    {
        VPHumanProfileDiplomaBO GetdataById(Guid Id);
        IEnumerable<VPHumanProfileDiplomaBO> GetAllByEmployeeId(Guid employeeId, int pageIndex, int pageSize, out int count);
        IEnumerable<VPHumanProfileDiplomaBO> GetAll();
        void InsertHumanProfileDiploma(VPHumanProfileDiplomaBO data, bool allowSave = true);
        IEnumerable<VPHumanProfileDiplomaBO> GetAllByEmployeeIdNotPaging(Guid employeeId);
    }
}
