﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanProfileCuriculmViateService : IService<VPHumanProfileCuriculmViateBO>
    {
        void InsertHumanProfileCuriculmViate(VPHumanProfileCuriculmViateBO data, bool allowSave = true);
        IEnumerable<VPHumanProfileCuriculmViateBO> GetAll();
        VPHumanProfileCuriculmViateBO GetByEmployeeId(Guid EmployeeId);
    }
}
