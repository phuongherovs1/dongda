﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public interface IJobTitleService : IService<JobTitleBO>
    {
        IEnumerable<JobTitleBO> GetAll(int pageIndex, int pageSize, out int count);
        IEnumerable<JobTitleBO> GetAllNotPaging();
        JobTitleBO GetSingle(string id);
    }
}
