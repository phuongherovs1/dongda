﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanTrainingPractionerService : IService<VPHumanTrainingPractionerBO>
    {
        IEnumerable<VPHumanTrainingPractionerBO> GetAll(int pageIndex, int pageSize, out int count, Guid detailId);
        bool CheckInvalidEmployees(Guid? employeesId, Guid? detailId);
        bool CheckOverNumber(Guid? detailId, int? number);
        void InsertObject(string stringId, Guid detailId);
        void InsertToProfile(Guid id);
    }
}
