﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanTrainingRequirementEmployeeService : IService<VPHumanTrainingRequirementEmployeeBO>
    {
        IEnumerable<VPHumanTrainingRequirementEmployeeBO> GetAll(int pageIndex, int pageSize, out int count, Guid requirementId);
    }
}
