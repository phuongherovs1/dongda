﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanTrainingQuestionService : IService<VPHumanTrainingQuestionBO>
    {
        VPHumanTrainingQuestionBO CreateDefaut(Guid Id);
        VPHumanTrainingQuestionBO ViewDetalQuest(Guid Id);
        IEnumerable<VPHumanTrainingQuestionBO> GetQuestion(int pageIndex, int pageSize, out int totalCount, Guid QuestionCategorId);
    }
}
