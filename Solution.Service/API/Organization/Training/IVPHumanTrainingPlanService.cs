﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanTrainingPlanService : IService<VPHumanTrainingPlanBO>
    {
        IEnumerable<VPHumanTrainingPlanBO> GetAll(int pageIndex, int pageSize, out int count);
        IEnumerable<VPHumanTrainingPlanBO> GetPlanIsAccept(int pageIndex, int pageSize, out int count);
        void SendApproval(VPHumanTrainingPlanBO data);
        void Approval(VPHumanTrainingPlanBO data);
    }
}
