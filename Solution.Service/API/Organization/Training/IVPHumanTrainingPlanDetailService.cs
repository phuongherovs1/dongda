﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanTrainingPlanDetailService : IService<VPHumanTrainingPlanDetailBO>
    {
        IEnumerable<VPHumanTrainingPlanDetailBO> GetAll(int pageIndex, int pageSize, out int count, Guid planId);
        IEnumerable<VPHumanTrainingPlanDetailBO> GetResultTraining(int pageIndex, int pageSize, out int count, Guid planId);
    }
}
