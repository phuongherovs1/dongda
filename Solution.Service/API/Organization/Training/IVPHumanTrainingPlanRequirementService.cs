﻿using System;

namespace iDAS.Service
{
    public interface IVPHumanTrainingPlanRequirementService : IService<VPHumanTrainingPlanRequirementBO>
    {
        void UpdateSelected(Guid id, bool isSelected, Guid planId);
    }
}
