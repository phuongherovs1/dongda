﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanTrainingRequirementService : IService<VPHumanTrainingRequirementBO>
    {
        IEnumerable<VPHumanTrainingRequirementBO> GetAll(int pageIndex, int pageSize, out int count);
        IEnumerable<VPHumanTrainingRequirementBO> GetByPlan(int pageIndex, int pageSize, out int count, Guid planId);
        IEnumerable<VPHumanTrainingRequirementBO> GetRequirementByPlan(int pageIndex, int pageSize, out int count, Guid planId);
        void SendApproval(VPHumanTrainingRequirementBO data);
        void Approval(VPHumanTrainingRequirementBO data);
    }
}
