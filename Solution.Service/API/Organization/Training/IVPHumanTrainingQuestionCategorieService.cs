﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanTrainingQuestionCategorieService : IService<VPHumanTrainingQuestionCategorieBO>
    {
        IEnumerable<VPHumanTrainingQuestionCategorieBO> GetQuestionCategory(int pageIndex, int pageSize, out int totalCount);
    }
}
