﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public interface IEmployeeJobTitleService : IService<EmployeeJobTitleBO>
    {
        IEnumerable<EmployeeJobTitleBO> GetByEmployeeId(Guid id);

        void Update(EmployeeJobTitleBO jobtitle);
    }
}
