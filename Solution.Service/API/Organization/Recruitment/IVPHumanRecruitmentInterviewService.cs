﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanRecruitmentInterviewService : IService<VPHumanRecruitmentInterviewBO>
    {

        IEnumerable<VPHumanRecruitmentInterviewBO> GetByProfieInterviewID(int p1, int p2, out int totalCount, Guid ProfileInterviewID, Guid PlanID);
        void UpdateOrInsert(VPHumanRecruitmentInterviewBO item);
    }
}
