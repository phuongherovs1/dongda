﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanRecruitmentPlanService : IService<VPHumanRecruitmentPlanBO>
    {
        IEnumerable<VPHumanRecruitmentPlanBO> GetData(int pageIndex, int pageSize, out int count);

        Guid Save(VPHumanRecruitmentPlanBO data, string jsonIds);

        void Update(VPHumanRecruitmentPlanBO data, string jsonIds);

        void SendApprove(VPHumanRecruitmentPlanBO data);

        IEnumerable<VPHumanRecruitmentPlanBO> GetAllAprovel(int pageIndex, int pageSize, out int totalCount);

        VPHumanRecruitmentPlanBO GetById(Guid? id);

        void Approve(VPHumanRecruitmentPlanBO data);
    }
}
