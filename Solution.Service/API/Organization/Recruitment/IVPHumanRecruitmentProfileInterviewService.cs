﻿
using System;

namespace iDAS.Service
{
    public interface IVPHumanRecruitmentProfileInterviewService : IService<VPHumanRecruitmentProfileInterviewBO>
    {
        void DleteProfileInterview(Guid planId, Guid ProfileId);
        void InsertProfileResult(VPHumanRecruitmentProfileResultBO item);

        VPHumanRecruitmentProfileResultBO GetResultByProfileInterViewID(Guid ProfileInterviewID);
    }
}
