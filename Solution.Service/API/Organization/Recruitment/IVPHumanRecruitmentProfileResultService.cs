﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanRecruitmentProfileResultService : IService<VPHumanRecruitmentProfileResultBO>
    {
        /// <summary>
        /// Danh sách kết quả trúng tuyển theo kế hoạch tuyển dụng
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="planId"></param>
        /// <returns></returns>
        IEnumerable<VPHumanRecruitmentProfileResultBO> GetProfileResultByPlan(int pageIndex, int pageSize, out int count, Guid planId);

        VPHumanRecruitmentProfileResultBO GetDetailResultFormItem(Guid id);

        void Approve(VPHumanRecruitmentProfileResultBO data);
    }
}
