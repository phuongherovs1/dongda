﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanRecruitmentCriteriaService : IService<VPHumanRecruitmentCriteriaBO>
    {
        IEnumerable<VPHumanRecruitmentCriteriaBO> GetByDepartment(int pageIndex, int pageSize, out int totalCount, Guid departmentID);
        void Active(Guid id, bool IsActive);
        void IsDelete(Guid id);
        IEnumerable<VPHumanRecruitmentCriteriaBO> GetCriteria(int pageIndex, int pageSize, out int count, Guid profileInterviewID, Guid RequirementID);
        VPHumanRecruitmentCriteriaBO getDetail(Guid Id);

    }
}
