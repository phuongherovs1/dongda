﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanRecruitmentPlanRequirementService : IService<VPHumanRecruitmentPlanRequirementBO>
    {
        IEnumerable<VPHumanRecruitmentPlanRequirementBO> GetByPlanId(Guid planId, int pageIndex, int pageSize, out int count);
        Guid Insert(Guid planId, Guid requirementId);
        void Delete(Guid planId, Guid requirementId);
    }
}
