﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanRecruitmentProfileService : IService<VPHumanRecruitmentProfileBO>
    {
        IEnumerable<VPHumanRecruitmentProfileBO> GetAll(int pageIndex, int pageSize, out int totalCount);
        IEnumerable<VPHumanRecruitmentProfileBO> GetAllProfileInterview(int pageIndex, int pageSize, out int totalCount, Guid PlanId, string filterName = default(string));
        IEnumerable<VPHumanRecruitmentProfileBO> LoadProfileInterviewByPlan(int pageIndex, int pageSize, out int totalCount, Guid PlanId);
        void Update(VPHumanRecruitmentProfileBO item);
        void Insert(VPHumanRecruitmentProfileBO item);
    }
}
