﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanRecruitmentPlanInterviewService : IService<VPHumanRecruitmentPlanInterviewBO>
    {
        IEnumerable<VPHumanRecruitmentPlanInterviewBO> GetByPlanId(Guid planId, int pageIndex, int pageSize, out int count);
        IEnumerable<VPHumanRecruitmentPlanInterviewBO> GetByProfieInterviewId(Guid planId, Guid profileInterviewId, int pageIndex, int pageSize, out int count);
    }
}
