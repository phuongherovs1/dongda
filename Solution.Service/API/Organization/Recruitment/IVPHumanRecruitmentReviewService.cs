﻿namespace iDAS.Service
{
    public interface IVPHumanRecruitmentReviewService : IService<VPHumanRecruitmentReviewBO>
    {
        void UpdateOrInsert(VPHumanRecruitmentReviewBO item);
    }
}
