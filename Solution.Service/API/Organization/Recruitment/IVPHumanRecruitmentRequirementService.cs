﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IVPHumanRecruitmentRequirementService : IService<VPHumanRecruitmentRequirementBO>
    {
        IEnumerable<VPHumanRecruitmentRequirementBO> GetAllByDeptID(int pageIndex, int pageSize, out int totalCount, Guid DepartmentID);
        IEnumerable<VPHumanRecruitmentRequirementBO> GetAll(int pageIndex, int pageSize, out int totalCount);
        VPHumanRecruitmentRequirementBO updateRecruitment(Guid Id);
        void Approve(VPHumanRecruitmentRequirementBO updateData);
        void DeleteRequirAndApprov(Guid id);

        IEnumerable<VPHumanRecruitmentRequirementBO> GetRequirementApproved(string planId, int pageIndex, int pageSize, out int count);
        IEnumerable<VPHumanRecruitmentRequirementBO> GetRequirementByplanId(Guid planId);
    }
}
