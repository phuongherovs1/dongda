﻿
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IDepartmentService : IService<DepartmentBO>
    {
        /// <summary>
        ///  Lấy thông tin chi tiết của phòng ban
        /// </summary>
        /// <param name="id"></param>
        /// <param name="checkParent"></param>
        /// <returns></returns>
        DepartmentBO GetDepartmentDetail(Guid id, bool checkParent = false);
        /// <summary>
        ///lấy danh sách phòng ban theo id phòng ban cha
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        IEnumerable<DepartmentBO> GetTreeDepartment(Guid? id);
        IEnumerable<DepartmentBO> GetTreeDepartmentChecked(Guid? id, Guid? programId);
        /// <summary>
        /// lấy tất cả các phòng ban con, cháu của phòng ban
        /// </summary>
        /// <param name="departmentId">id phòng ban</param>
        /// <returns></returns>
        IEnumerable<DepartmentBO> GetChildrenByDepartment(Guid id);
        /// <summary>
        /// lấy danh sách phòng ban của nhân sự
        /// </summary>
        /// <param name="employeeId">id của nhân sự</param>
        /// <returns></returns>
        IEnumerable<DepartmentBO> GetDepartmentsByEmployeeID(Guid employeeId);
        /// <summary>
        /// get phong ban phan phoi
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        IEnumerable<DepartmentBO> GetDepartmentDistribute(Guid Id);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<DepartmentBO> GetDepartmentsByCurrentUser();
        /// <summary>
        /// Lấy danh sách id phòng ban cha
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentIds"></param>
        void GetParentByDepartment(Guid id, ref List<Guid> departmentIds);

        /// <summary>
        /// Đưa ra danh mục phòng ban
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        DepartmentBO GetFormItem(string id, string parentId);
        /// <summary>
        /// Kiểm tra có đã có tổ chức gốc 
        /// </summary>
        /// <returns></returns>
        bool CheckHasRoot();

        /// <summary>
        /// Load danh sách node theo node cha
        /// 1. Khi chọn phòng ban thì hiển thị phòng ban trực thuộc + chức danh
        /// 2. Khi chọn chức danh thì hiển thị nhân sự theo chức danh
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        IEnumerable<OrganizationTreeBO> GetByNode(string node, bool showTitle = false, bool showEmployee = false, string onlyTitle = default(string));

        /// <summary>
        /// Danh sách phòng ban có thể cập nhật danh mục tài liệu
        /// </summary>
        /// <returns></returns>
        IEnumerable<DepartmentBO> GetDepartmentCanUpdateDocument();

        /// <summary>
        /// Lấy tất cả phòng ban và phòng ban con của phòng ban người đăng nhập
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        IEnumerable<DepartmentBO> GetDepartmentsAndChildrenByEmployeeID(Guid employeeId);
    }
}
