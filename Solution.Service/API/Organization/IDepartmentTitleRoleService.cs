﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public interface IDepartmentTitleRoleService : IService<DepartmentTitleRoleBO>
    {
        
        IEnumerable<DepartmentTitleRoleBO> GetRolesByTitle(Guid titleId);
        DepartmentTitleRoleBO GetRole(Guid titleId, Guid departmentId);
        bool CheckTitleRole_View(Guid userId);
        bool CheckTitleRole_Edit(Guid userId);

        void Update(DepartmentTitleRoleBO de);

        Guid Insert(DepartmentTitleRoleBO de);
       
        void Delete(DepartmentTitleRoleBO de);
    }
}
