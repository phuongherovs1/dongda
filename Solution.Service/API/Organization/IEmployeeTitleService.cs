﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public interface IEmployeeTitleService : IService<EmployeeTitleBO>
    {
        string GetStringDepartmentRole(Guid empId);
        /// <summary>
        /// lấy danh sách chức danh của nhân sự
        /// </summary>
        /// <param name="employeeId">employee id</param>
        /// <returns>roles</returns>
        IQueryable<EmployeeTitleBO> GetRolesByEmployeeID(Guid employeeId, bool allowGetRoleName = true);
        /// <summary>
        /// kiểm tra xem nhân sự đã tồn tại chức danh hay chưa
        /// </summary>
        /// <param name="employeeId">employee id</param>
        /// <param name="roleId">role id</param>
        /// <returns>true: exits - false: not exits</returns>
        bool CheckEmployeeRoleExits(Guid employeeId, Guid roleId);
        /// <summary>
        /// lấy chức danh của nhân sự trong phòng ban
        /// </summary>
        /// <param name="employeeId">Id nhân sự</param>
        /// <param name="departmentId">Id phòng ban</param>
        /// <returns>Chức danh A, Chức danh B...</returns>
        string GetRoleNamesByEmployeeIDAndDepartmentID(Guid employeeId, Guid departmentId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleIds"></param>
        /// <returns></returns>
        IEnumerable<Guid> GetEmployeeIDsByRoleIDs(IEnumerable<Guid> roleIds);
        /// <summary>
        /// Get Employee role by role id
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        IEnumerable<EmployeeTitleBO> GetByTitleId(Guid roleId);
        /// <summary>
        /// Kiểm tra xem nhân sự có chức danh chưa
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        bool CheckEmployeeIDExits(Guid employeeId);

        /// <summary>
        /// Đưa ra danh sách chức danh của người đang đăng nhập hệ thống
        /// </summary>
        /// <returns></returns>
        IQueryable<Guid> GetTitleIdsByCurrentUser();
        void Delete(Guid Id);
    }
}
