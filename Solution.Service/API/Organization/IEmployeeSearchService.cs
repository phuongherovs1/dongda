﻿using System.Data;

namespace iDAS.Service.API.Organization
{
    public interface IEmployeeSearchService
    {
        DataTable GetData(string religion, string identityCard, string people, string placeOfWork, string position);

        DataTable GetDataByCertificate(string diplomasName, string faculty, string major, string certificatesName, string placeOfTraining);

        DataTable GetDataByDecision(string numberOfDecisionReward, string reasonReward, string formReward, string numberOfDecisionDiscipline, string reasonDiscipline, string formDiscipline);

        DataTable GetDataByContract(string numberOfContracts, string typeOfContracts, string numberOfInsurances, string typeOfInsurances);

        DataTable GetStatistic(string statisticType);
    }
}
