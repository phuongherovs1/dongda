﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public interface IEmployeeService : IService<EmployeeBO>
    {
        IEnumerable<string> GetDeparmentRoleNames(Guid id);
        /// <summary>
        /// Danh sách member hiển thị chọn để chat
        /// Loại trừ người đang đăng nhập hệ thống
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        IEnumerable<EmployeeBO> GetUserChatList(int start, int limit, out int count, string name = default(string));
        IEnumerable<string> GetRoleNames(Guid id);
        // Nhân sự đăng nhập
        EmployeeBO GetCurrentUser();
        // lấy thông tin nhân sự theo chuỗi id
        IEnumerable<EmployeeBO> GetInfoByIds(IEnumerable<Guid> ids, bool allowGetRoleName = false, bool allowDeleted = false);
        // Lấy danh sách nhân sự chưa được chọn
        IEnumerable<EmployeeBO> GetInfoNotInIds(IQueryable<Guid> employeeIds, string query = "", bool allowGetRoleName = false, bool allowDeleted = false);
        // Lấy thông tin chi tiết nhân sự
        EmployeeBO GetDetail(Guid id, bool allowDeleted = false, bool allowDefaultIfNull = true);
        // Lấy thông tin chung của nhân sự
        EmployeeBO GetEmployeeGeneralInfo(Guid id);
        // Lấy danh sách tất cả nhân sự
        IEnumerable<EmployeeBO> GetAll(int pageIndex, int pageSize, out int totalCount, bool allowGetRoleName = false, bool allowDeleted = false);
        /// <summary>
        /// Lấy nhân sự có chức danh
        /// </summary>
        /// <param name="query">chuỗi tìm kiếm</param>
        /// <returns></returns>
        IEnumerable<EmployeeBO> GetAllHasRole(int pageIndex, int pageSize, out int totalCount, string query = "", bool allowGetRoleName = false);
        /// <summary>
        /// Lấy nhân sự chưa có chức danh
        /// </summary>
        /// <param name="query">chuỗi tìm kiếm</param>
        /// <returns></returns>
        IEnumerable<EmployeeBO> GetAllRoleEmpty(string query = "");
        // Lấy danh sách nhân sự thuộc chức danh
        IEnumerable<EmployeeBO> GetByRoleIDs(IEnumerable<Guid> roleIds);
        /// Lấy danh sách nhân sự theo phòng ban
        IEnumerable<EmployeeBO> GetByDepartment(int pageIndex, int pageSize, out int totalCount, Guid departmentId, string query = "", bool allowGetRoleName = false);
        /// <summary>
        /// Danh sách nhân sự theo phòng ban
        /// </summary>
        /// <param name="DepartmentID"></param>
        /// <returns></returns>
        IEnumerable<EmployeeBO> GetByDepartment(Guid departmentId);

        /// <summary>
        /// Get list employees by list users
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        IEnumerable<EmployeeBO> GetByUsers(IEnumerable<UserBO> users);

        // danh sách nhân sự có quyền phê duyệt ban hành tài liệu
        IEnumerable<EmployeeBO> GetByPublishApprove(Guid categoryId);

        // danh sách nhân sự có quyền kiểm duyệt tài liệu
        IEnumerable<EmployeeBO> GetByReviewApprove(Guid categoryId);

        // Danh sách nhân sự có vai trò soạn thảo
        IEnumerable<EmployeeBO> GetByPerformWrite(Guid categoryId);

        // Đưa ra danh sách nhân sự có vai trò lưu trữ tài liệu
        IEnumerable<EmployeeBO> GetByArchiveRole(Guid categoryId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="query"></param>
        /// <param name="departmentId"></param>
        /// <param name="process"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        IEnumerable<EmployeeBO> GetEmployeeInProcessDocument(int pageIndex, int pageSize, out int totalCount, string query, string departmentId = default(string), int process = 0, int order = 0, int? roleType = 0);
        IEnumerable<EmployeeBO> GetEmployeeByRoleType(int pageIndex, int pageSize, out int totalCount, string query, int roleType = 0);
        /// <summary>
        /// Chuyển nhân sự khác thiết lập trong quy trình
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalCount"></param>
        /// <param name="query"></param>
        /// <param name="roleType"></param>
        /// <param name="typeProcess"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        IEnumerable<EmployeeBO> GetEmployeeByProcess(int pageIndex, int pageSize, out int totalCount, string query, int roleType = 0, int typeProcess = 0, string departmentId = "");
        /// <summary>
        /// Danh sách nhân sự có vai trò lưu trữ trong phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        IEnumerable<EmployeeBO> GetArchiveRoleByDepartment(Guid departmentId, string name);


        #region User login infor: Thông tin nhân sự và thông tin đăng nhập của nhân sự 

        EmployeeBO GetWithLoginInfo(Guid id);
        EmployeeBO GetWithLoginInfo();
        #endregion


        IEnumerable<EmployeeBO> GetByDepartmentIndex(Guid departmentId, string query = "", bool allowGetRoleName = false, List<string> lstEmployeeTitles = null);
        IEnumerable<EmployeeBO> GetData(int pageIndex, int pageSize, out int totalCount, string query = "", bool allowGetRoleName = false);
        Boolean GetByDepartmentEmployee(Guid departmentId);
        IEnumerable<EmployeeBO> GetInfo(int pageIndex, int pageSize, out int totalCount, string query = "", bool allowGetRoleName = false, bool allowDeleted = false);
    }
}
