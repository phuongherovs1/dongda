﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public interface IDepartmentTitleService : IService<TitleBO>
    {
        /// <summary>
        /// Lấy danh sách chức danh theo phòng ban
        /// </summary>
        /// <param name="departmentId">Id phòng ban cần lấy</param>
        /// <returns></returns>
        IEnumerable<TitleBO> GetRolesByDepartment(Guid departmentId);
        /// <summary>
        /// Thay đổi thứ tự chức danh
        /// </summary>
        /// <param name="up">true: đưa chức danh lên trên chức danh liền kề, false đưa chức danh xuống dưới chức danh liền kề</param>
        /// <param name="roleId"></param>
        void UpdateOrder(bool up, Guid roleId);
        /// <summary>
        /// Lấy danh sách chức danh thuộc phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        IEnumerable<Guid> GetRoleIdsByDepartment(Guid departmentId);
        /// <summary>
        /// Lấy thứ tự lớn nhất của chức danh trong phòng ban
        /// </summary>
        /// <param name="departmentId">Id của phòng ban</param>
        /// <returns></returns>
        int GetMaxOrderRole(Guid departmentId);
        /// <summary>
        /// Lấy phòng ban theo chức danh
        /// </summary>
        /// <param name="roleId">Id chức danh</param>
        /// <returns></returns>
        Guid GetDepartmentByRole(Guid roleId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleIds"></param>
        /// <returns></returns>
        IQueryable<Guid> GetDepartmentByRoleIDs(IQueryable<Guid> roleIds);
        void DeleteByOrder(Guid Id);
    }
}
