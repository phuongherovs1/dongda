﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IKPICategoryService : IService<KPICategoryBO>
    {
        Guid Insert(KPICategoryBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        void Update(KPICategoryBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        IEnumerable<KPICategoryBO> GetAll(int pageIndex, int pageSize, out int count, bool allowDeleted = false);
        IEnumerable<KPICategoryBO> GetByfilterName(int pageIndex, int pageSize, int level, Guid? id, string filterName, out int count, bool allowDeleted = false);
        IEnumerable<KPICategoryBO> GetTreeDepartment(Guid? id, string filterName = default(string));
        KPICategoryBO GetFormItem();
    }
}
