﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IKPIForTaskService : IService<KPIForTaskBO>
    {
        Guid Insert(KPIForTaskBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        IEnumerable<TaskPerformBO> GetDataToSendPointAdmin(bool IsParent, string filter = default(string), string TaskIdSearch = default(string));
        IEnumerable<KPIForTaskBO> GetDataScoreOfEmployee(DateTime DateFrom, DateTime DateTo, Guid? DepartmentId, int pageIndex, int pageSize, out int count, string filterName = default(string));
        IEnumerable<KPIForTaskBO> GetDataScoreOfEmployeeMark(DateTime DateFrom, DateTime DateTo, Guid? EmployeeId);
    }
}
