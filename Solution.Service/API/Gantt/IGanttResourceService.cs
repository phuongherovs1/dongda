﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IGanttResourceService
    {
        IEnumerable<GanttResourceBO> GetByTask(Guid taskID);
    }
}
