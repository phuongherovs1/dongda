﻿
using System;

namespace iDAS.Service
{
    /// <summary>
    /// Interface of Task Business Service
    /// </summary>
    public interface IGanttService
    {
        GanttBO GetAll();
        GanttBO GetByUserID(Guid userID);
    }
}
