﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IBotnetManagerService : IService<BotnetManagerBO>
    {
        Guid Insert(BotnetManagerBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        void Update(BotnetManagerBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        IEnumerable<BotnetManagerBO> GetAll(int pageIndex, int pageSize, out int count);
        IEnumerable<BotnetManagerBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count);

        BotnetManagerBO GetFormItem();
        int Import(List<BotnetManagerBO> botnetImport);

    }
}
