﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IIPManagerService : IService<IPManagerBO>
    {
        Guid Insert(IPManagerBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        void Update(IPManagerBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        // history == true Query những bản khi trên Grid HistoryBotnet-------CountInfection > 0
        IEnumerable<IPManagerBO> GetAll(int pageIndex, int pageSize, bool history, out int count);
        IEnumerable<IPManagerBO> GetByfilterName(int pageIndex, int pageSize, string filterName, bool history, out int count);
        IEnumerable<IPManagerBO> GetByabbreviation(int pageIndex, int pageSize, string abbreviation, bool history, out int count);
        IPManagerBO GetByIp(string filterName, out int count);
        IPManagerBO GetFormItem();
        int Import(List<IPManagerBO> botnetImport);
        IEnumerable<IPManagerBO> GetByCountInfection(int pageIndex, int pageSize, out int count);
    }
}
