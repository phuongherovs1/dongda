﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IBotnetInfectionService : IService<BotnetInfectionBO>
    {
        Guid Insert(BotnetInfectionBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        void Update(BotnetInfectionBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
        IEnumerable<BotnetInfectionBO> GetAll(int pageIndex, int pageSize, out int count);
        IEnumerable<BotnetInfectionBO> GetByfilterName(int pageIndex, int pageSize, string filterName, out int count);

        BotnetInfectionBO GetFormItem();
        int Import(List<BotnetInfectionBO> botnetImport);
    }
}
