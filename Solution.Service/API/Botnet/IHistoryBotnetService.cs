﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IHistoryBotnetService : IService<HistoryBotnetBO>
    {
        IEnumerable<HistoryBotnetBO> GetAll(int pageIndex, int pageSize, out int count);
        Guid Insert(HistoryBotnetBO data, string dataPropertySettings = "", string dataResourceRelates = "", bool allowSave = true);
    }
}
