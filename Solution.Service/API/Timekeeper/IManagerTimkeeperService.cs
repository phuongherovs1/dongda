﻿using iDAS.Service.POCO.Timekeeper;
using System;
using System.Data;

namespace iDAS.Service.API.Timekeeper
{
    public interface IManagerTimkeeperService
    {
        bool CreateTimkeeper(TimekeeperBO timkeeperBO);
        bool UpdateTimkeeper(TimekeeperBO timkeeperBO);
        bool DeleteTimkeeper(int _IDTimekeeper);
        DataTable SelectTimkeeper();
        //DataTable SelectTimkeeper(int idTimkeeper);
        TimekeeperBO SelectTimkeeper(int idTimkeeper);
        bool InputTimekeeping(int idTimkeeper, string IP, int Post, out string error);
        string GetdataExpost(DateTime fromdate, DateTime todate);

    }
}
