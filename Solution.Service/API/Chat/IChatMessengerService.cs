﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public interface IChatMessengerService : IService<ChatMessengerBO>
    {
        /// <summary>
        /// Danh sách tin nhắn của người đăng nhập với người kia
        /// </summary>
        /// <param name="toUserId"></param>
        IEnumerable<ChatMessengerBO> GetDataChatToUser(Guid toUserId);

        IEnumerable<ChatMessengerBO> GetMoreByCurrentUser(int start, int limit, out int count);
        IEnumerable<ChatMessengerBO> GetTopMessenger(int top);

        Guid NewMessage(Guid toUserId, string content);
        int GetTotalNotRead();
        int GetCount();
        int UpdateRead(Guid id, bool isRead = true);
        void Revert(Guid id);

    }
}
