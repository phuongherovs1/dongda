﻿using System;

namespace iDAS.Service
{
    public interface IChatPublicService : IService<ChatPublicBO>
    {
        Guid NewMessage(string content);

    }
}
