﻿namespace iDAS.Service
{
    public interface IChatUserOnlineService : IService<ChatUserOnlineBO>
    {
        /// <summary>
        /// Cập nhật trạng thái khi user online
        /// </summary>
        /// <param name="username"></param>
        void OnConnected(string username);

        /// <summary>
        /// Cập nhật trạng thái khi user offline
        /// </summary>
        /// <param name="username"></param>
        void OnDisconnected(string username);
    }
}
