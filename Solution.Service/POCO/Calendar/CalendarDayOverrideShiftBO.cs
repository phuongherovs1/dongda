﻿using System;

namespace iDAS.Service
{
    public class CalendarDayOverrideShiftBO : BaseBO
    {
        #region Base
        public Guid? CalendarDayOverrideId { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public bool? IsOverTime { get; set; }
        public string Note { get; set; }

        public Nullable<System.Guid> CopyFrom { get; set; }
        public Nullable<bool> IsOverride { get; set; }

        #endregion
        #region Rule
        public string StartTimeText
        {
            get
            {
                return StartTime.HasValue ? StartTime.ToString().Remove(StartTime.ToString().Length - 3) : string.Empty;
            }
        }
        public string EndTimeText
        {
            get
            {
                return EndTime.HasValue ? EndTime.ToString().Remove(EndTime.ToString().Length - 3) : string.Empty;
            }
        }
        public Guid? CalendarDayOverrideCopyId { get; set; }
        #endregion
    }
}
