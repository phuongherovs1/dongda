﻿using System;
using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public class CalendarBO : BaseBO
    {
        #region Base
        public Guid? ParentId { get; set; }
        [Required(ErrorMessage = "Trường không được để trống!")]
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
        public string Note { get; set; }
        #endregion
    }
}
