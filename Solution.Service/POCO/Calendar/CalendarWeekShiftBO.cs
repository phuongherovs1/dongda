﻿using System;

namespace iDAS.Service
{
    public class CalendarWeekShiftBO : BaseBO
    {
        #region Base
        public System.Guid CalendarId { get; set; }
        public Guid? RowId { get; set; }
        public int? DayType { get; set; }
        public string Name { get; set; }
        public Nullable<System.Guid> CopyFrom { get; set; }
        public Nullable<bool> IsOverride { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        #endregion
    }
}
