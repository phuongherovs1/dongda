﻿using System;

namespace iDAS.Service
{
    public class CalendarDayOverrideBO : BaseBO
    {
        #region Base
        public System.Guid CalendarId { get; set; }
        public int DayType
        {
            get
            {
                return (int)Date.Value.DayOfWeek;
            }
            set
            {
            }
        }
        public DateTime? Date { get; set; }
        public bool? IsHoliday { get; set; }
        public bool? IsWorkingDay { get; set; }
        public string Note { get; set; }
        public Nullable<System.Guid> CopyFrom { get; set; }
        public Nullable<bool> IsOverride { get; set; }
        #endregion

        #region Rules
        public Guid? CopyToId { get; set; }

        #endregion
    }
}
