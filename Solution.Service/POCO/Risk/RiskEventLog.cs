using System;

namespace iDAS.Service
{
    public class RiskEventLogBO : BaseBO
    {
        public Nullable<System.Guid> DepartmentId { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Code { get; set; }
        public string DepartmentName { get; set; }
        public string SummaryEvent { get; set; }
        public string Info { get; set; }
        public string ProcessHandler { get; set; }
        public Nullable<bool> IsIncident { get; set; }
        public string Reason { get; set; }
        public string CodeIncident { get; set; }
    }
    public class RiskEventLogImport
    {
        public int Date { get; set; }
        public int Code { get; set; }
        public int SummaryEvent { get; set; }
        public int Info { get; set; }
        public int ProcessHandler { get; set; }
        public int IsIncident { get; set; }
        public int Reason { get; set; }
        public int CodeIncident { get; set; }
    }
}
