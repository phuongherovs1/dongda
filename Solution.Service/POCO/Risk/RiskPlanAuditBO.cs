using System;

namespace iDAS.Service
{
    public class RiskPlanAuditBO : BaseBO
    {
        public Nullable<System.Guid> DepartmentId { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTime> DateAudit { get; set; }
    }
}
