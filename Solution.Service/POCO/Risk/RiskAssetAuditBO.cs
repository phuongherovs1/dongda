﻿using System;

namespace iDAS.Service
{
    public class RiskAssetAuditBO : BaseBO
    {
        public Nullable<System.Guid> RiskPlanAuditId { get; set; }
        public Nullable<System.Guid> AssetResourceRelateId { get; set; }
        public Nullable<decimal> AssetValue { get; set; }
        public string Weakness { get; set; }
        public string CurrentCountermove { get; set; }
        public string Hazard { get; set; }
        public Nullable<decimal> ImpactLevel { get; set; }
        public Nullable<decimal> Possible { get; set; }
        public Nullable<decimal> RiskLevel { get; set; }
        public Nullable<int> AcceptCriteria { get; set; }
        #region Rule
        /// <summary>
        /// Tiêu đề báo cáo
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Tên tài sản
        /// </summary>
        public string AssetName { get; set; }
        public string ProcessExpediency { get; set; }
        public string AcceptCriteriaName { get; set; }
        #endregion
    }
}
