using System;

namespace iDAS.Service
{
    public class RiskIncidentHandlerBO : BaseBO
    {
        public Nullable<System.Guid> RiskIncidentId { get; set; }
        public string ContentHandler { get; set; }
        public Nullable<System.Guid> DepartmentId { get; set; }
        public Nullable<decimal> Rate { get; set; }
        public Nullable<System.DateTime> DateFinish { get; set; }
        public string ProofDocument { get; set; }
        public string Note { get; set; }

        public string DepartmentName { get; set; }
    }
}
