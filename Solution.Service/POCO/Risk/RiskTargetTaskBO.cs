﻿using System;

namespace iDAS.Service
{
    public class RiskTargetTaskBO : BaseBO
    {

        public Guid? RiskTargetPlanId { get; set; }

        public Guid? TaskId { get; set; }
    }
}
