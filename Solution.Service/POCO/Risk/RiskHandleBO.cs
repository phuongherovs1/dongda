using System;

namespace iDAS.Service
{
    public class RiskHandleBO : BaseBO
    {
        public Nullable<System.Guid> RiskAssetAuditId { get; set; }
        public Nullable<System.Guid> DepartmentId { get; set; }
        public Nullable<System.Guid> EmployeeId { get; set; }
        public Nullable<int> MethodHandle { get; set; }
        public string MeasureControl { get; set; }
        public string Clause { get; set; }
        public string CombineComponent { get; set; }
        public Nullable<System.DateTime> DateFinish { get; set; }
        public Nullable<System.DateTime> DateCheck { get; set; }
        public Nullable<System.Guid> EmployeeCheckId { get; set; }
        public string Conclude { get; set; }
        public Nullable<decimal> AssetValue { get; set; }
        public Nullable<decimal> ImpactLevel { get; set; }
        public Nullable<decimal> Possible { get; set; }
        public Nullable<decimal> RiskLevel { get; set; }
        public Nullable<int> AcceptCriteria { get; set; }

        public string Title { get; set; }
        public string AssetName { get; set; }
        public string MethodHandlerName { get; set; }
        public string AcceptCriteriaName { get; set; }
        public string DepartmentName { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeCheckName { get; set; }
        public string Weakness { get; set; }
        public string Hazard { get; set; }
        public string AssetRelaionship { get; set; }
        public string ProcessExpediency { get; set; }

    }
}
