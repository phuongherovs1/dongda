namespace iDAS.Service
{
    public class RiskLikelihoodBO : BaseBO
    {
        public int? Value { get; set; }
        public string Description { get; set; }
    }
}
