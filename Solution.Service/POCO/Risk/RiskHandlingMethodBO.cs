﻿namespace iDAS.Service
{
    public class RiskHandlingMethodBO : BaseBO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
