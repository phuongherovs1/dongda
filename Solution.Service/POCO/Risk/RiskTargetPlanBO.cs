﻿using System;

namespace iDAS.Service
{
    public class RiskTargetPlanBO : BaseBO
    {
        public Guid? DepartmentId { get; set; }

        public Guid? RiskTargetId { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public DateTime? StartAtPlan { get; set; }

        public DateTime? EndAtPlan { get; set; }

    }
}
