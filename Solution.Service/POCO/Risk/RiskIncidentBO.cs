using System;

namespace iDAS.Service
{
    public class RiskIncidentBO : BaseBO
    {
        public string CodeIncident { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<System.Guid> DepartmentId { get; set; }
        public string Description { get; set; }
        public string Reason { get; set; }
        public string AnotherIdea { get; set; }
        public string Conclude { get; set; }
        #region Rule
        public string DepartmentName { get; set; }
        public bool IsDoing { get; set; }
        public bool IsFinish { get; set; }
        public string StatusText
        {
            get
            {
                var status = Common.Resource.HandlerIncidentText.New;
                if (IsDoing && !IsFinish)
                    status = Common.Resource.HandlerIncidentText.Doing;
                if (IsFinish && !IsDoing)
                    status = Common.Resource.HandlerIncidentText.Finish;
                return status;
            }
        }
        #endregion
    }
    public class RiskIncidentImport
    {
        public int CodeIncident { get; set; }
        public int Date { get; set; }
        public int Description { get; set; }
        public int Reason { get; set; }
        public int AnotherIdea { get; set; }
        public int Conclude { get; set; }
    }
}