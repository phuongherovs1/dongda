﻿using System;

namespace iDAS.Service
{
    public class RiskTargetBO : BaseBO
    {
        public Guid? DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public bool? IsTarget { get; set; }
        public bool? IsPolicy { get; set; }
        public bool? IsScene { get; set; }
    }
}
