﻿using iDAS.Service.Common;
using System;
using System.IO;
using System.Linq;
using System.Web;

namespace iDAS.Service
{
    public class FileBO : BaseBO
    {
        #region base
        public string Name { get; set; }
        public Nullable<long> Size { get; set; }
        public string Type { get; set; }
        public string Extension { get; set; }
        #endregion

        #region rule
        public byte[] Data { get; set; }
        public virtual Stream Stream { get; set; }
        public string Url
        {
            get
            {
                if (Data != null && Data.Count() > 0)
                    return string.Format("data:image;base64,{0}", Convert.ToBase64String(Data));
                else
                    return @"/Content/Images/underfind.jpg";
            }
        }
        private HttpPostedFileBase file;
        public HttpPostedFileBase File
        {
            get
            {
                return file;
            }
            set
            {
                if (value != null)
                {
                    file = value;
                    Name = value.FileName;
                    Stream = value.InputStream;
                    Data = Utilities.ConvertStreamToByte(value.InputStream);
                    Type = value.ContentType;
                    Size = value.ContentLength;
                    Extension = System.IO.Path.GetExtension(value.FileName).Trim('.');
                }
            }
        }
        #endregion
    }
}
