﻿using System;

namespace iDAS.Service
{
    public class EmployeeJobTitleBO : BaseBO
    {
        #region Base
        public Guid? HumanEmployeeId { get; set; }
        public Guid? JobTitleId { get; set; }
        #endregion
    }
}
