﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public class FieldsGroupModelBO
    {
        public FieldsGroupModelBO()
        {
            this.CheckedItems = new List<CheckedFieldModel>();
        }

        public string FieldLabel { get; set; }

        public List<CheckedFieldModel> CheckedItems { get; set; }
    }

    public class CheckedFieldModel
    {
        public string BoxLabel { get; set; }
    }
}
