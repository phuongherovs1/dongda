﻿using System;

namespace iDAS.Service
{
    public class EmployeeTitleBO : BaseBO
    {
        #region Base
        public System.Guid TitleId { get; set; }
        public System.Guid TitleId_Update { get; set; }
        public System.Guid EmployeeId { get; set; }
        public string Note { get; set; }
        #endregion

        #region Rule
        /// <summary>
        /// tên chức danh
        /// </summary>
        public string RoleName { get; set; }
        /// <summary>
        /// tên phòng ban theo chức danh
        /// </summary>
        public string DepartmentRoleName { get; set; }
        public string DepartmentName { get; set; }
        public string TitleName { get; set; }
        /// <summary>
        /// id phòng ban theo chức danh
        /// </summary>
        public System.Guid DepartmentId { get; set; }
        public System.Guid DepartmentId_Update { get; set; }
        public Guid? ProfileWorkExperienceId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public FileUploadBO FileAttachs { get; set; }
        #endregion
    }
}
