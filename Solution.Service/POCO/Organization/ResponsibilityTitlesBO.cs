﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public class ResponsibilityTitlesBO : BaseBO
    {
        public Guid ResponsibilityId { get; set; }
        public Guid? TitleId { get; set; }
        public string NameResponsibility { get; set; }
        public string Loop { get; set; }
        public string Mission { get; set; }
        public string Request { get; set; }
        public string Result { get; set; }
        public string Criteria { get; set; }
    }
}
