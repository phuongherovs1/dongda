﻿using System;
using System.Linq;

namespace iDAS.Service
{
    public class EmployeeBOExt : BaseBO
    {
        #region Base
        public byte[] Avatar { get; set; }
        private string name = string.Empty;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string Code { get; set; }
        public string PhoneNumber { get; set; }
        public string ExtensionNumber { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public int? Sex { get; set; }
        #endregion

        #region Rule
        public string UserName { get; set; }
        public string LastName { get; set; }
        public string FullNameFormat
        {
            get
            {
                var fullNameFormat = string.IsNullOrEmpty(UserName) ? Name : string.Format("{0} - {1}", Name, UserName);
                return fullNameFormat;
            }
        }

        /// <summary>
        /// đường dẫn ảnh đại diện của nhân sự
        /// </summary>
        public string AvatarUrl
        {
            get
            {
                if (Avatar != null && Avatar.Count() > 0)
                    return string.Format("data:image;base64,{0}", Convert.ToBase64String(Avatar));
                else
                    return @"/Content/Images/underfind.jpg";
            }
        }
        /// <summary>
        /// chức danh của nhân sự có dạng: Chức danh 1, Chức danh 2...
        /// </summary>
       // private string roleName;
        public string RoleNames
        {
            get;
            set;
            //get
            //{
            //    return roleName = roleName ?? string.Empty;
            //}
            //set
            //{
            //    roleName = value;
            //}
        }
        private FileBO fileAvatar;
        public FileBO FileAvatar
        {
            get
            {
                return fileAvatar = fileAvatar ?? new FileBO();
            }
            set
            {
                fileAvatar = value;
            }
        }
        public string Password { get; set; }

        public Guid? Parent { get; set; }
        //private EmployeeBO employee;
        //public EmployeeBO Employee
        //{
        //    get
        //    {
        //        return employee ?? (employee = new EmployeeBO());
        //    }
        //    set
        //    {
        //        employee = value;
        //    }
        //}
        #endregion
    }
}
