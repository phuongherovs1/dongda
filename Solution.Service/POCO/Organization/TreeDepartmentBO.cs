﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class TreeDepartmentBO
    {
        public bool FromView { get; set; }
        private IEnumerable<Guid> departmentIds;
        public IEnumerable<Guid> DepartmentIds
        {
            get { return departmentIds; }
            set
            {
                departmentIds = value;
                if (!FromView)
                {
                    listDepartment = departmentIds.Select(i => i.ToString()).Aggregate((current, next) => current + "," + next);
                }
            }
        }
        public List<DepartmentBO> Departments { get; set; }
        public List<TitleBO> Titles { get; set; }
        public List<EmployeeBO> Employees { get; set; }

        private IEnumerable<Guid> titleIds;
        public IEnumerable<Guid> TitleIds
        {
            get { return titleIds; }
            set
            {
                titleIds = value;
                if (!FromView)
                {
                    listTitle = titleIds.Select(i => i.ToString()).Aggregate((current, next) => current + "," + next);
                }
            }
        }

        private IEnumerable<Guid> employeeIds;
        public IEnumerable<Guid> EmployeeIds
        {
            get { return employeeIds; }
            set
            {
                employeeIds = value;
                if (!FromView)
                {
                    listEmployee = employeeIds.Select(i => i.ToString()).Aggregate((current, next) => current + "," + next);
                }
            }
        }

        private string _value;
        public string Value
        {
            get
            {
                var strIds = string.Empty;
                var strNames = string.Empty;
                if (Departments != null && Departments.Count() > 0)
                {
                    foreach (var item in Departments)
                    {
                        strIds += "Department_" + item.Id;
                        strIds += ",";
                        strNames += item.Name;
                        strNames += ",";
                    }
                }
                if (Titles != null && Titles.Count() > 0)
                {
                    foreach (var item in Titles)
                    {
                        strIds += "Title_" + item.Id;
                        strIds += ",";
                        strNames += item.Name;
                        strNames += ",";

                    }
                }
                if (Employees != null && Employees.Count() > 0)
                {
                    foreach (var item in Employees)
                    {
                        strIds += "Employee_" + item.Id;
                        strIds += ",";
                        strNames += item.Name;
                        strNames += ",";
                    }
                }
                if (strIds != string.Empty)
                {
                    _value = strIds;
                    _text = strNames;
                }
                return _value;
            }
            set
            {
                _value = value;
                if (FromView)
                {
                    listDepartment = string.Empty;
                    listTitle = string.Empty;
                    listEmployee = string.Empty;
                    if (_value.Contains(','))
                    {
                        var objData = _value.Trim(',').Split(',');
                        if (objData != null && objData.Length > 0)
                        {
                            foreach (var item in objData)
                            {
                                if (item.Contains('_'))
                                {
                                    var obj = item.Split('_');
                                    if (obj.Length >= 2)
                                    {
                                        switch (obj[0])
                                        {
                                            case "Department":
                                                listDepartment += "," + obj[1];
                                                break;
                                            case "Title":
                                                listDepartment += "," + obj[1];
                                                break;
                                            case "Employee":
                                                listDepartment += "," + obj[1];
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        private string _text;

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }


        private string listDepartment;
        public string ListDepartment
        {
            get { return listDepartment; }
            set { listDepartment = value; }
        }

        private string listTitle;
        public string ListTitle
        {
            get { return listTitle; }
            set { listTitle = value; }
        }

        private string listEmployee;
        public string ListEmployee
        {
            get { return listEmployee; }
            set { listEmployee = value; }
        }
    }
}
