using System;

namespace iDAS.Service
{
    public class VPHumanProfileDisciplineBO : BaseBO
    {


        public Guid? HumanEmployeeId { get; set; }

        public string NumberOfDecision { get; set; }

        public DateTime? DateOfDecision { get; set; }

        public string Reason { get; set; }
        public FileUploadBO FileAttachs { get; set; }
        public string Form { get; set; }










    }
}
