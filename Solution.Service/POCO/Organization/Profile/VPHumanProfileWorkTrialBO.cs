﻿using System;

namespace iDAS.Service
{
    public class VPHumanProfileWorkTrialBO : BaseBO
    {
        #region Base
        public Guid? HumanEmployeeId { get; set; }

        public Guid? HumanRoleId { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public string Note { get; set; }

        public bool IsEdit { get; set; }

        public bool IsApproval { get; set; }

        public bool IsAccept { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public Guid? ApprovalBy { get; set; }

        public string ApprovalNote { get; set; }

        public int? DirectorApproval { get; set; }

        public DateTime? DirectorApprovalAt { get; set; }

        public int? ContractType { get; set; }

        public DateTime? ContractStartTime { get; set; }

        public Guid? ManagerId { get; set; }
        /// <summary>
        /// Yêu cầu tự đánh giá: True/False
        /// </summary>
        public bool? Status { get; set; }
        #endregion

        #region Rule
        /// <summary>
        /// Nhân sự thử việc
        /// </summary>
        public string EmployeeTrialName { get; set; }
        /// <summary>
        /// Chức danh nhân sự thử việc
        /// </summary>
        public string TitleName { get; set; }
        /// <summary>
        /// Phòng ban
        /// </summary>
        public string DepartmentName { get; set; }
        public EmployeeBO EmployeeId { get; set; }
        public EmployeeBO EmployeeManege { get; set; }
        public string EmployeeManegeId { get; set; }
        public string EmployeeManegeName { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }

        public string StatusApproval
        {
            get { return getStatus(); }
            set { }
        }

        private string getStatus()
        {
            string result = "";
            EStatus status = EStatus.New;
            if (!IsEdit && ApprovalBy != null)
            {
                status = EStatus.ApprovalWait;
            }
            //if (IsEdit && IsApproval)
            if (IsEdit == true)
            {
                status = EStatus.Change;
            }
            if (IsApproval && IsAccept)
            {
                status = EStatus.Success;
            }
            if (IsApproval && !IsAccept && !IsEdit)
            {
                status = EStatus.Fail;
            }
            switch (status)
            {
                case EStatus.New: result = "<span style=\"color:green\">Mới</span>"; break;
                case EStatus.Change: result = "<span style=\"color:red\">Sửa đổi</span>"; break;
                case EStatus.ApprovalWait: result = "<span style=\"color:#8e210b\">Chờ duyệt</span>"; break;
                case EStatus.Success: result = "<span style=\"color:blue\">Duyệt</span>"; break;
                case EStatus.Fail: result = "<span style=\"color:#41519f\">Không duyệt</span>"; break;
                default: result = "<span style=\"color:#045fb8\">Mới</span>"; break;
            }
            return result;
        }
        private enum EStatus
        {
            New,
            Change,
            ApprovalWait,
            Success,
            Fail,
        }

        public EmployeeBO EmployeeApprover { get; set; }

        public Guid? ProfileID { get; set; }

        public string ProfileName { get; set; }

        public Guid? RecruitmentResultId { get; set; }

        public UserBO User { get; set; }

        public bool? IsEmployee { get; set; }

        public bool? AuditStatus { get; set; }
        #endregion
    }
}
