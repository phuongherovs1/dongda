using System;

namespace iDAS.Service
{
    public class VPHumanProfileContractBO : BaseBO
    {


        public Guid? HumanEmployeeId { get; set; }

        public string NumberOfContracts { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Type { get; set; }

        public string Condition { get; set; }
        public FileUploadBO FileAttachs { get; set; }

    }
}
