using System;

namespace iDAS.Service
{
    public class VPHumanProfileCertificateBO : BaseBO
    {
        public Guid? HumanEmployeeId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Level { get; set; }

        public string CertificateType { get; set; }

        public string PlaceOfTraining { get; set; }

        public DateTime? DateIssuance { get; set; }
        public FileUploadBO FileAttachs { get; set; }
        public DateTime? DateExpiration { get; set; }
    }
}