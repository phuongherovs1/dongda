using System;

namespace iDAS.Service
{
    public class VPHumanProfileDiplomaBO : BaseBO
    {


        public Guid? HumanEmployeeId { get; set; }

        public string Name { get; set; }

        public string Faculty { get; set; }

        public string Major { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Level { get; set; }

        public string FormOfTrainning { get; set; }
        public FileUploadBO FileAttachs { get; set; }
        public string Type { get; set; }

        public string Rank { get; set; }

        public string Place { get; set; }

        public string Condition { get; set; }

        public DateTime? DateOfGraduation { get; set; }










    }
}
