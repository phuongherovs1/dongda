using System;

namespace iDAS.Service
{
    public class VPHumanProfileRelationshipBO : BaseBO
    {


        public Guid? HumanEmployeeId { get; set; }

        public bool? IsPartnerRelationship { get; set; }
        public string Name { get; set; }

        public short? Age { get; set; }

        public bool? IsMale { get; set; }

        public string Relationship { get; set; }

        public string Job { get; set; }

        public string PlaceOfJob { get; set; }
        public FileUploadBO FileAttachs { get; set; }










        public string Phone { get; set; }

        public string Adress { get; set; }
    }
}
