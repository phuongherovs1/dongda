using System;

namespace iDAS.Service
{
    public class VPHumanProfileTrainingBO : BaseBO
    {


        public Guid? HumanEmployeeId { get; set; }

        public string Name { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Form { get; set; }

        public string Content { get; set; }

        public string Certificate { get; set; }

        public string Result { get; set; }

        public string Reviews { get; set; }
        public string AddressTranning { get; set; }

        public FileUploadBO FileAttachs { get; set; }










    }
}
