using System;

namespace iDAS.Service
{
    public class VPHumanProfileAttachmentBO : BaseBO
    {


        public Guid? HumanEmployeeId { get; set; }
        public FileUploadBO FileAttachs { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }

    }
}
