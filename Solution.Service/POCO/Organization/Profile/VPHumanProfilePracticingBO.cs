using System;

namespace iDAS.Service
{
    public class VPHumanProfilePracticingBO : BaseBO
    {
        public Guid? EmployeeId { get; set; }
        public string NumberDecisions { get; set; }
        public string Professional { get; set; }
        public string Scope { get; set; }
        public DateTime? DateAppoint { get; set; }
        public FileUploadBO FileAttachs { get; set; }
    }
}
