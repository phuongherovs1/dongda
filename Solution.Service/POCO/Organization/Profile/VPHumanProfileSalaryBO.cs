using System;

namespace iDAS.Service
{
    public class VPHumanProfileSalaryBO : BaseBO
    {


        public Guid? HumanEmployeeId { get; set; }

        public string Level { get; set; }
        public string CodeJob { get; set; }

        public string Wage { get; set; }
        public string Extra { get; set; }

        public DateTime? DateOfApp { get; set; }
        public DateTime? DateOfOff { get; set; }

        public FileUploadBO FileAttachs { get; set; }
        
        public string Note { get; set; }
    }
}
