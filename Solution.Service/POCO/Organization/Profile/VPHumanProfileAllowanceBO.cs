using System;

namespace iDAS.Service
{
    public class VPHumanProfileAllowanceBO : BaseBO
    {
        public Guid? EmployeeId { get; set; }
        public string NumberDecisions { get; set; }
        public string Note { get; set; }
        public string PCCV { get; set; }
        public string Position { get; set; }
        public DateTime? DateAppoint { get; set; }
        public FileUploadBO FileAttachs { get; set; }
    }
}
