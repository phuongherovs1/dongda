using System;

namespace iDAS.Service
{
    public class VPHumanProfileInsuranceBO : BaseBO
    {


        public Guid? HumanEmployeeId { get; set; }

        public string Number { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Type { get; set; }

        public string Note { get; set; }
        public FileUploadBO FileAttachs { get; set; }










        public decimal? InSuranceNorms { get; set; }
    }
}
