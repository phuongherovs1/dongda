﻿using System;

namespace iDAS.Service
{
    public class VPHumanProfileWorkTrialResultBO : BaseBO
    {
        #region Base
        public Guid? HumanProfileWorkTrialId { get; set; }

        public string CriteriaName { get; set; }

        public int? EmployeePoint { get; set; }

        public int? ManagerPoint { get; set; }

        public string Note { get; set; }
        #endregion
        #region Rule
        /// <summary>
        /// Tên nhân sự thử việc
        /// </summary>
        public string EmployeeTrialName { get; set; }
        #endregion
    }
}
