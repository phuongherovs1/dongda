using System;

namespace iDAS.Service
{
    public class VPHumanProfileAttachmentFileBO : BaseBO
    {
        public Guid? AttachmentFileId { get; set; }

        public Guid? HumanProfileAttachmentId { get; set; }
    }
}
