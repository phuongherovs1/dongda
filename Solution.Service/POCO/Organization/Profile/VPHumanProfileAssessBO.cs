using System;

namespace iDAS.Service
{
    public class VPHumanProfileAssessBO : BaseBO
    {

        public Guid? HumanEmployeeId { get; set; }

        public string Name { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Score { get; set; }

        public string Result { get; set; }

        public string Note { get; set; }

    }
}
