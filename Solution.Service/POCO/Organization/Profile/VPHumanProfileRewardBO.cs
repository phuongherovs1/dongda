using System;

namespace iDAS.Service
{
    public class VPHumanProfileRewardBO : BaseBO
    {


        public Guid? HumanEmployeeId { get; set; }

        public string NumberOfDecision { get; set; }

        public DateTime? DateOfDecision { get; set; }

        public string Reason { get; set; }

        public string Form { get; set; }
        public string DepartmentBonus { get; set; }

        public FileUploadBO FileAttachs { get; set; }
    }
}
