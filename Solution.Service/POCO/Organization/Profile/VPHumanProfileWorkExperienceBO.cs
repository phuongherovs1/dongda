using System;

namespace iDAS.Service
{
    public class VPHumanProfileWorkExperienceBO : BaseBO
    {
        public Guid? HumanEmployeeId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Position { get; set; }

        public string JobDescription { get; set; }

        public string Department { get; set; }
        public FileUploadBO FileAttachs { get; set; }
        public string PlaceOfWork { get; set; }
    }
}
