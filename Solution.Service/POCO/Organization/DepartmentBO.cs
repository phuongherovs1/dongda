﻿using System;

namespace iDAS.Service
{
    public class DepartmentBO : BaseBO
    {
        #region Base
        /// <summary>
        /// id của phòng ban cha
        /// </summary>
        public Guid? ParentId { get; set; }
        /// <summary>
        /// tên của phòng ban
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// ghi chú của phòng ban
        /// </summary>
        public string Note { get; set; }
        public string Functions { get; set; }

        public string Task { get; set; }
        /// <summary>
        /// Level type tree of department
        /// </summary>
        public int? Level { get; set; }


        #endregion

        #region Rule
        /// <summary>
        /// giá trị: true - phòng ban chứa phòng ban con, false - phòng ban không chứa phòng ban con
        /// </summary>
        public bool IsParent
        {
            get;
            set;
        }
        public bool IsDisabled
        {
            get;
            set;
        }
        public bool Checked { get; set; }

        public bool IsView { get; set; }
        public bool IsUpdate { get; set; }
        #endregion
    }
}
