﻿using System;

namespace iDAS.Service
{
    public class DepartmentTitleRoleBO : BaseBO
    {
        private string capacityrequire;
        #region Base
        public Guid TitleId { get; set; }
        public Guid DepartmentId { get; set; }

        public bool ViewAll { get; set; }
        public bool UpdateAll { get; set; }
        #endregion
    }
}
