﻿using System;

namespace iDAS.Service
{
    public class VPHumanRecruitmentProfileInterviewBO : BaseBO
    {
        #region Base
        public Guid? HumanRecruitmentProfileId { get; set; }

        public Guid? HumanRecruitmentPlanId { get; set; }

        public Guid? HumanRecruitmentRequirementId { get; set; }

        #endregion

        #region Rule
        public string ProfileName { get; set; }
        public bool? IsEdit { get; set; }
        public bool? IsRoleEdit { get; set; }
        #endregion
    }
}
