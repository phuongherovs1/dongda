﻿using System;

namespace iDAS.Service
{
    public class VPHumanRecruitmentProfileResultBO : BaseBO
    {
        #region Base
        public Guid? HumanRecruitmentProfileInterviewId { get; set; }

        public int? TotalPoint { get; set; }

        public DateTime? StartTime { get; set; }

        public decimal? Salary { get; set; }

        public bool? IsApproval { get; set; }

        public bool? IsPass { get; set; }

        public Guid? ApprovalBy { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public string Note { get; set; }

        public bool? IsTrial { get; set; }
        public bool? IsEmployee { get; set; }
        public Guid? EmployeeId { get; set; }

        #endregion

        #region Rule
        public string ProfileName { get; set; }
        // Vị trí tuyển dụng
        public string RoleName { get; set; }
        // Kế hoạch tuyển dụng
        public String PlanName { get; set; }
        public Guid PlanID { get; set; }
        public Guid RequirementID { get; set; }
        // Điểm nhỏ nhất tính toán được từ các tiêu chí lựa chọn
        public int? CriteriaMinPoint { get; set; }
        // Điểm lớn nhất tính toán được từ các tiêu chí lựa chọn
        public int? CriteriaMaxPoint { get; set; }

        public EmployeeBO EmployeeApproveBy { get; set; }

        public string HumanName { get; set; }

        public Guid? ProfileId { get; set; }

        #endregion
    }
}
