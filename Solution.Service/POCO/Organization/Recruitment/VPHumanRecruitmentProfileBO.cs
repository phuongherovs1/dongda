﻿using System;
using System.Linq;

namespace iDAS.Service
{
    public class VPHumanRecruitmentProfileBO : BaseBO
    {
        #region Base
        public Guid? EmployeeId { get; set; }
        public Guid? HumanRecruitmentPlanId { get; set; }
        public Guid? ProfileInterViewId { get; set; }
        public Guid? RequirementID { get; set; }
        public Guid? FileId { get; set; }
        public string Name { get; set; }

        public string RequirementName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public byte[] Avatar { get; set; }

        public string Address { get; set; }

        public DateTime? Birthday { get; set; }

        public bool? Gender { get; set; }

        public string Nationality { get; set; }

        public string People { get; set; }

        public string Religion { get; set; }

        public string PlaceOfBirth { get; set; }

        public string NumberOfIdentityCard { get; set; }

        public DateTime? DateIssueOfIdentityCard { get; set; }

        public string PlaceIssueOfIdentityCard { get; set; }

        public string HomePhone { get; set; }

        public string PlaceOfTranning { get; set; }

        public string Specicalization { get; set; }

        public string LevelOfComputerization { get; set; }

        public string ForeignLanguage { get; set; }

        public string Literacy { get; set; }

        public string Qualifications { get; set; }

        public string ListOfCertificates { get; set; }

        public string Experience { get; set; }

        public decimal? Salary { get; set; }

        public short? YearsOfExperience { get; set; }

        public bool? check { get; set; }

        public bool? IsEmployee { get; set; }
        #endregion

        #region Rule
        /// <summary>
        /// đường dẫn ảnh đại diện của nhân sự
        /// </summary>
        public string AvatarUrl
        {
            get
            {
                if (Avatar != null && Avatar.Count() > 0)
                    return string.Format("data:image;base64,{0}", Convert.ToBase64String(Avatar));
                else
                    return @"/Content/Images/underfind.jpg";
            }
        }

        private FileBO fileAvatar;
        public FileBO FileAvatar
        {
            get
            {
                return fileAvatar = fileAvatar ?? new FileBO();
            }
            set
            {
                fileAvatar = value;
            }
        }


        public string Status
        {
            get { return getStatus(); }
            set { }
        }

        private string getStatus()
        {
            string result = "";
            result = IsApproval == true ? (IsPass == true ? "<span style=\"color:blue\">Duyệt</span>" : "<span style=\"color:#41519f\">Không duyệt</span>")
                : (IsEdit == false ? "<span style=\"color:#8e210b\">Chờ duyệt</span>" : "<span style=\"color:green\">Mới</span>");
            return result;
            //EStatus status = EStatus.New;
            //if (IsEdit != true)
            //{
            //    status = EStatus.ApprovalWait;
            //}
            //if (IsEdit == true && IsApproval == true)
            //{
            //    status = EStatus.Change;
            //}
            //if (IsApproval == true && IsAccept == true)
            //{
            //    status = EStatus.Success;
            //}
            //if (IsApproval == true && IsAccept != true && IsEdit != true) 
            //{
            //    status = EStatus.Fail;
            //}
            //switch (status)
            //{
            //    case EStatus.New: result = "<span style=\"color:green\">Mới</span>"; break;
            //    case EStatus.Change: result = "<span style=\"color:red\">Sửa đổi</span>"; break;
            //    case EStatus.ApprovalWait: result = "<span style=\"color:#8e210b\">Chờ duyệt</span>"; break;
            //    case EStatus.Success: result = "<span style=\"color:blue\">Duyệt</span>"; break;
            //    case EStatus.Fail: result = "<span style=\"color:#41519f\">Không duyệt</span>"; break;
            //    default: result = "<span style=\"color:#045fb8\">Mới</span>"; break;
            //}
            //return result;
        }
        public bool? IsEdit { get; set; }
        public bool? IsSend { get { return !IsEdit; } }
        public bool? IsSelect { get; set; }
        public bool? IsApproval { get; set; }
        public bool? IsPass { get; set; }
        public bool? IsRoleEdit { get; set; }
        public FileUploadBO FileAttachs { get; set; }
        #endregion
    }
}
