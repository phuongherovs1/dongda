﻿using System;

namespace iDAS.Service
{
    public class VPHumanRecruitmentRequirementBO : BaseBO
    {
        #region Base
        public Guid? HumanRoleId { get; set; }

        public string Name { get; set; }

        public string DepartmentName { get; set; }

        public int? Number { get; set; }

        public int? ApproveNumber { get; set; }

        public int? StatusApproval { get; set; }

        public DateTime? DateRequired { get; set; }

        public string Form { get; set; }

        public string Reason { get; set; }

        public bool? IsEdit { get; set; }

        public bool? IsApproval { get; set; }

        public bool? IsAccept { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public Guid? ApprovalBy { get; set; }

        public string Note { get; set; }


        #endregion

        #region Rule
        public bool? IsSelected { get; set; }

        public EmployeeBO EmployeeApproveBy { get; set; }
        public string NameEmployeeApproveBy { get; set; }
        public string RoleName { get; set; }
        public string Status
        {
            get { return getStatus(); }
            set { }
        }
        public string StatusApprovalText
        {
            get
            {
                if (StatusApproval == 0)
                    return "<span style=\"color:green\">Đang yêu cầu</span>";
                if (StatusApproval == 1)
                    return "<span style=\"color:#8e210b\">Đang tuyển</span>";
                if (StatusApproval == 2)
                    return "<span style=\"color:blue\">Đã tuyển xong</span>";
                if (StatusApproval == 3)
                    return "<span style=\"color:red\">Hủy</span>";
                else
                    return "";
            }
        }
        private string getStatus()
        {
            string result = "";
            EStatus status = EStatus.New;
            if (IsEdit != true)
            {
                status = EStatus.ApprovalWait;
            }
            if (IsEdit == true && IsApproval == true)
            {
                status = EStatus.Change;
            }
            if (IsApproval == true && IsAccept == true)
            {
                status = EStatus.Success;
            }
            if (IsApproval == true && IsAccept != true && IsEdit != true)
            {
                status = EStatus.Fail;
            }
            switch (status)
            {
                case EStatus.New: result = "<span style=\"color:green\">Mới</span>"; break;
                case EStatus.Change: result = "<span style=\"color:red\">Sửa đổi</span>"; break;
                case EStatus.ApprovalWait: result = "<span style=\"color:#8e210b\">Chờ duyệt</span>"; break;
                case EStatus.Success: result = "<span style=\"color:blue\">Phê duyệt</span>"; break;
                case EStatus.Fail: result = "<span style=\"color:#41519f\">Không duyệt</span>"; break;
                default: result = "<span style=\"color:#045fb8\">Mới</span>"; break;
            }
            return result;
        }
        private enum EStatus
        {
            New,
            Change,
            ApprovalWait,
            Success,
            Fail,
            Complete

        }
        #endregion
    }
}
