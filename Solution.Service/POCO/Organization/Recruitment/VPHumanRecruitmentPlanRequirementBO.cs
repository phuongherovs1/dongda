﻿using System;

namespace iDAS.Service
{
    public class VPHumanRecruitmentPlanRequirementBO : BaseBO
    {
        #region Base
        public Guid? HumanRecruitmentRequirementId { get; set; }

        public Guid? HumanRecruitmentPlanId { get; set; }
        #endregion

        #region Rule


        /// <summary>
        /// Tên yêu cầu tuyển dụng
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Lý do tuyển dụng
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Có được lựa chọn bởi kế hoạch
        /// </summary>
        public bool IsSelect { get; set; }

        /// <summary>
        /// Tên chức danh tuyển dụng
        /// </summary>
        public string RoleName { get; set; }
        #endregion
    }
}
