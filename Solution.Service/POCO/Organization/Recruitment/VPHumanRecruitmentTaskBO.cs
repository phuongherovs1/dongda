﻿using System;

namespace iDAS.Service
{
    public class VPHumanRecruitmentTaskBO : BaseBO
    {
        #region Base
        public Guid? HumanRecruitmentPlanId { get; set; }

        public Guid? TaskId { get; set; }

        public decimal? Cost { get; set; }
        #endregion

        #region Rule

        #endregion
    }
}
