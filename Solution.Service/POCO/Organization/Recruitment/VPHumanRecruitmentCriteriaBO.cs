﻿using System;

namespace iDAS.Service
{
    public class VPHumanRecruitmentCriteriaBO : BaseBO
    {
        #region Base
        public Guid? HumanRoleId { get; set; }

        public string Name { get; set; }

        public string Content { get; set; }

        public bool? IsActive { get; set; }

        public int? MinPoint { get; set; }

        public int? MaxPoint { get; set; }

        public int? Factor { get; set; }
        #endregion

        #region Rule
        public string RoleName { get; set; }

        public Guid? HumanRecruitmentProfileId { get; set; }

        public Guid? HumanRecruitmentCriteriaId { get; set; }
        public string CriteriaName { get; set; }

        public int? Point { get; set; }

        public DateTime? Time { get; set; }

        public string Note { get; set; }
        #endregion
    }
}
