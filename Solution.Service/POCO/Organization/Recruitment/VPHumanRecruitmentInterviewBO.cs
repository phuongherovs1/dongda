﻿using System;

namespace iDAS.Service
{
    public class VPHumanRecruitmentInterviewBO : BaseBO
    {
        #region Base
        public Guid? HumanRecruitmentProfileInterviewId { get; set; }

        public Guid? HumanRecruitmentPlanInterviewId { get; set; }

        public string Result { get; set; }

        public DateTime? Time { get; set; }

        public string Note { get; set; }
        #endregion

        public DateTime? StartTime { get; set; }
        public string PlanInterviewName { get; set; }
        public DateTime? EndTime { get; set; }
        public string InterviewContent { get; set; }
        #region Rule

        #endregion
    }
}
