﻿using System;

namespace iDAS.Service
{
    public class VPHumanRecruitmentPlanBO : BaseBO
    {
        #region Base
        public string Name { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal? TotalCost { get; set; }

        public string Content { get; set; }

        public bool? IsEdit { get; set; }

        public bool? IsApproval { get; set; }

        public bool? IsAccept { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public Guid? ApprovalBy { get; set; }

        public string Note { get; set; }

        public Guid? UserId { get; set; }

        #endregion


        #region Rule

        public string Status
        {
            get { return getStatus(); }
            set { }
        }

        private string getStatus()
        {
            string result = "";
            result = IsApproval == true ? (IsAccept == true ? "<span style=\"color:blue\">Duyệt</span>" : "<span style=\"color:#41519f\">Không duyệt</span>")
                : (IsEdit == false ? "<span style=\"color:#8e210b\">Chờ duyệt</span>" : "<span style=\"color:green\">Mới</span>");
            return result;
        }

        private enum EStatus
        {
            New,
            Change,
            ApprovalWait,
            Success,
            Fail,
        }

        public EmployeeBO EmployeeApproveBy { get; set; }
        public string NameEmployeeApproveBy { get; set; }
        public bool? IsRoleEdit { get; set; }
        #endregion
    }
}
