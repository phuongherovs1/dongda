﻿using System;

namespace iDAS.Service
{
    public class VPHumanRecruitmentPlanInterviewBO : BaseBO
    {
        #region Base
        public Guid? HumanRecruitmentPlanId { get; set; }
        public Guid? HumanRecruitmentProfileInterviewId { get; set; }
        public Guid? HumanRecruitmentPlanInterviewId { get; set; }
        public string Name { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public string Content { get; set; }
        #endregion

        #region Rule
        public string Result { get; set; }

        public DateTime? Time { get; set; }

        public string Note { get; set; }
        public VPHumanRecruitmentInterviewBO InterviewResult { get; set; }

        #endregion
    }
}
