﻿using System;

namespace iDAS.Service
{
    public class VPHumanRecruitmentReviewBO : BaseBO
    {
        #region Base
        public Guid? HumanRecruitmentProfileId { get; set; }

        public Guid? HumanRecruitmentCriteriaId { get; set; }
        public string CriteriaName { get; set; }

        public int? Point { get; set; }

        public DateTime? Time { get; set; }

        public string Note { get; set; }
        #endregion

        #region Rule

        #endregion
    }
}
