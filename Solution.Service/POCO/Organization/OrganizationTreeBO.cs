﻿namespace iDAS.Service
{
    public class OrganizationTreeBO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Common.Resource.OrganizationTreeType Type { get; set; }
        public string AvatarUrl { get; set; }
        public bool IsLeaf { get; set; }
    }



}
