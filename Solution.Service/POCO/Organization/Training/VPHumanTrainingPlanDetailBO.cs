﻿using System;

namespace iDAS.Service
{
    public class VPHumanTrainingPlanDetailBO : BaseBO
    {
        #region Base
        public Guid? VPHumanTrainingPlanId { get; set; }

        public string Contents { get; set; }

        public int? Number { get; set; }

        public int? StatusRequest { get; set; }

        public string Reason { get; set; }

        public bool? Type { get; set; }

        public decimal? ExpectedCost { get; set; }

        public string Certificate { get; set; }

        public string Note { get; set; }

        public bool? IsCommit { get; set; }

        public string CommitContent { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public string Address { get; set; }

        public bool? IsCancel { get; set; }

        public string ReasonCancel { get; set; }
        #endregion
        #region view
        public string StatusRequestText
        {
            get
            {
                if (StatusRequest == 1)
                    return "<span style=\"color:#8e210b\">Đang đào tạo</span>";
                else if (StatusRequest == 2)
                    return "<span style=\"color:blue\">Đã hoàn thành</span>";
                else if (StatusRequest == 3)
                    return "<span style=\"color:red\">Hủy</span>";
                else
                    return "<span style=\"color:green\">Chưa thực hiện</span>";
            }
        }
        public int? NumberAssign { get; set; }
        #endregion
    }
}
