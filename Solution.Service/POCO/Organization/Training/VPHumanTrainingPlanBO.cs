﻿using System;

namespace iDAS.Service
{
    public class VPHumanTrainingPlanBO : BaseBO
    {
        #region Base

        public string Name { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public decimal? Cost { get; set; }
        public bool? IsEdit { get; set; }
        public bool? IsEnd { get; set; }

        public DateTime? EndAt { get; set; }

        public bool? IsApproval { get; set; }

        public bool? IsAccept { get; set; }

        public Guid? ApprovalBy { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public string Contents { get; set; }

        public string Note { get; set; }
        #endregion
        public EmployeeBO EmployeeApproval { get; set; }
        public string Status
        {

            get
            {
                string result = "<span style=\"color:green\">Mới</span>";
                if (IsEdit == false)
                {
                    result = "<span style=\"color:#8e210b\">Chờ duyệt</span>";
                }
                if (IsEdit == true && IsApproval == true)
                {
                    result = "<span style=\"color:red\">Sửa đổi</span>";
                }
                if (IsApproval == true && IsAccept == true)
                {
                    result = "<span style=\"color:blue\">Duyệt</span>";
                }
                if (IsApproval == true && IsAccept == false && IsEdit == false)
                {
                    result = "<span style=\"color:#41519f\">Không duyệt</span>";
                }
                if (IsEnd == true)
                {
                    result = "<span style=\"gray\">Kết thúc</span>";
                }
                return result;
            }
        }
    }
}
