﻿using System;

namespace iDAS.Service
{
    public class VPHumanTrainingRequirementBO : BaseBO
    {
        #region Base
        public Guid? EmployeeId { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public string Contents { get; set; }

        public bool? IsEdit { get; set; }

        public bool? IsApproval { get; set; }

        public bool? IsAccept { get; set; }

        public int? StatusRequest { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public Guid? ApprovalBy { get; set; }

        public string Note { get; set; }
        #endregion
        public EmployeeBO RequireBy { get; set; }
        public EmployeeBO EmployeeApproval { get; set; }
        public string Status
        {
            get
            {
                string result = "<span style=\"color:green\">Mới</span>";
                if (IsEdit == false)
                {
                    result = "<span style=\"color:#8e210b\">Chờ duyệt</span>";
                }
                if (IsEdit == true && IsApproval == true)
                {
                    result = "<span style=\"color:red\">Sửa đổi</span>";
                }
                if (IsApproval == true && IsAccept == true)
                {
                    result = "<span style=\"color:blue\">Đã duyệt</span>";
                }
                if (IsApproval == true && IsAccept == false && IsEdit == false)
                {
                    result = "<span style=\"color:#41519f\">Không duyệt</span>";
                }
                return result;
            }
        }
        public string StatusRequestText
        {
            get
            {
                if (StatusRequest == 1)
                    return "<span style=\"color:#8e210b\">Đang thực hiện</span>";
                else if (StatusRequest == 2)
                    return "<span style=\"color:blue\">Đã hoàn thành</span>";
                else if (StatusRequest == 3)
                    return "<span style=\"color:red\">Hủy</span>";
                else
                    return "<span style=\"color:green\">Đang yêu cầu</span>";
            }
        }
        public bool IsSelected { get; set; }
    }
}

