﻿using System;

namespace iDAS.Service
{
    public class VPHumanTrainingPlanRequirementBO : BaseBO
    {
        #region Base
        public Guid? VBHumanTrainingRequirementId { get; set; }

        public Guid? VBHumanTrainingPlanId { get; set; }
        #endregion
    }
}
