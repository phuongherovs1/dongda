﻿using System;

namespace iDAS.Service
{
    public class VPHumanTrainingQuestionBO : BaseBO
    {
        public Guid? VPHumanTrainingQuestionId { get; set; }

        public string Contents { get; set; }
        public string NameQuestionCategory { get; set; }
        public string Note { get; set; }

    }
}
