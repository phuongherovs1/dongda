﻿using System;

namespace iDAS.Service
{
    public class VPHumanTrainingPractionerBO : BaseBO
    {
        #region Base
        public Guid? VPHumanTrainingPlanDetailId { get; set; }

        public Guid? EmployeeId { get; set; }

        public int? Rank { get; set; }

        public bool? IsRegister { get; set; }

        public bool? IsAcceptCommit { get; set; }

        public DateTime? TimeRegister { get; set; }

        public bool? IsJoin { get; set; }

        public string ResonUnJoin { get; set; }

        public int? NumberPresence { get; set; }

        public int? NumberAbsence { get; set; }

        public decimal? TotalPoint { get; set; }

        public string CommentOfTeacher { get; set; }

        public bool? IsInProfile { get; set; }
        #endregion
        public EmployeeBO Practioner { get; set; }
        public bool IsCommit { get; set; }
        public string ContentCommit { get; set; }
        public int Number { get; set; }
        public string RankName
        {
            get
            {
                var result = string.Empty;
                switch (Rank)
                {
                    case 1:
                        result = "Giỏi";
                        break;
                    case 2:
                        result = "Khá";
                        break;
                    case 3:
                        result = "Trung bình";
                        break;
                    case 4:
                        result = "Yếu";
                        break;
                    case 5:
                        result = "Kém";
                        break;
                    default:
                        break;
                }
                return result;
            }
        }

    }
}
