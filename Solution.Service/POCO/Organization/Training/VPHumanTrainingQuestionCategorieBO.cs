﻿namespace iDAS.Service
{
    public class VPHumanTrainingQuestionCategorieBO : BaseBO
    {
        public string Contents { get; set; }

        public string Note { get; set; }
    }
}
