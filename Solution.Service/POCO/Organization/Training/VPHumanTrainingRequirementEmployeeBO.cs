﻿using System;

namespace iDAS.Service
{
    public class VPHumanTrainingRequirementEmployeeBO : BaseBO
    {
        #region Base

        public Guid? VPHumanTrainingRequirementId { get; set; }

        public Guid? EmployeeId { get; set; }

        public string Contents { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public string Note { get; set; }
        #endregion
        public EmployeeBO Member { get; set; }


    }
}
