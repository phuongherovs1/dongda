﻿using System;

namespace iDAS.Service
{
    public class JobTitleBO : BaseBO
    {
        #region Base
        public string Name { get; set; }
        public Guid? Rank { get; set; }
        #endregion
    }
}
