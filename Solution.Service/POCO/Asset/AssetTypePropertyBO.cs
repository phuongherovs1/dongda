﻿namespace iDAS.Service
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class AssetTypePropertyBO : BaseBO
    {
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public Guid? AssetTypeId { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public Guid? PropertyId { get; set; }

        private string description;
        public string Description
        {
            get
            {
                return string.IsNullOrEmpty(description) ? string.Empty : description;
            }
            set
            {
                description = value;
            }
        }
        private string propertyName;
        public string PropertyName
        {
            get
            {
                return string.IsNullOrEmpty(propertyName) ? string.Empty : propertyName;
            }
            set
            {
                propertyName = value;
            }
        }

        public string PropertyCode { get; set; }
    }
}
