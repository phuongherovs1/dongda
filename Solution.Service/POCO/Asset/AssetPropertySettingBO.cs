namespace iDAS.Service
{
    using System;

    public class AssetPropertySettingBO : BaseBO
    {
        #region basic
        public Guid? AssetId { get; set; }
        public Guid? PropertyId { get; set; }
        public string Value { get; set; }
        #endregion
        public string PropertyName { get; set; }
    }
}
