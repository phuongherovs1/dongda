﻿using System;
using System.ComponentModel.DataAnnotations;
namespace iDAS.Service
{
    public class AssetBO : BaseBO
    {
        #region Base
        private string name;
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Name
        {
            get;
            set;
        }
        public string Code { get; set; }
        public Guid? TitleId { get; set; }
        public string TitleIdText { get; set; }
        public string TypeTitleId { get; set; }
        public string Unit { get; set; }
        public double? Quantity { get; set; }
        [Required]
        public Guid? AssetGroupID { get; set; }
        #endregion
        #region Rule
        public string AssetGroupName { get; set; }
        public string LabelTemp { get; set; }
        public string AssetTypeName { get; set; }
        public FileUploadBO Temp { get; set; }
        public int? ValueOfInviblate { get; internal set; }
        public int? ValueOfReady { get; internal set; }
        public int? ValueOfSecurity { get; internal set; }
        public string InviblateName { get; internal set; }
        public string SecurityName { get; internal set; }
        public string ReadyName { get; internal set; }
        public int? Value { get; internal set; }
        public string ImportantName { get; internal set; }
        #endregion
    }
    public class AssetImportObject
    {
        public int Name { get; set; }
        public int Code { get; set; }
        public int Quantity { get; set; }
        public int Unit { get; set; }
    }
}
