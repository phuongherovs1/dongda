﻿namespace iDAS.Service
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    public class AssetTypeBO : BaseBO
    {
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Name { get; set; }
        public bool? IsEvaluate { get; set; }
        private string description;
        public string Description
        {
            get { return string.IsNullOrEmpty(description) ? string.Empty : description; }
            set { description = value; }
        }
        public IEnumerable<Guid> PropetyIds { get; set; }
    }
}
