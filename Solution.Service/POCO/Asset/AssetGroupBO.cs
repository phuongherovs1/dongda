﻿namespace iDAS.Service
{
    using System;
    using System.ComponentModel.DataAnnotations;
    public class AssetGroupBO : BaseBO
    {
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Name
        {
            get;
            set;
        }
        public string AssetGroupCode { get; set; }
        private string description;
        public string Description
        {
            get
            {
                return string.IsNullOrEmpty(description) ? string.Empty : description;
            }
            set
            {
                description = value;
            }
        }
        public Nullable<bool> IsDefault { get; set; }
    }
}
