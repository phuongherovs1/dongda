namespace iDAS.Service
{
    using iDAS.Service.Common;
    using System;
    using System.ComponentModel.DataAnnotations;
    public class AssetResourceRelateBO : BaseBO
    {
        #region Basic
        public Guid? AssetId { get; set; }
        private Guid? employeeId;
        public Guid? EmployeeId
        {
            get
            {
                return employeeId;
            }
            set
            {
                if (!FromView)
                {
                    employeeId = value;
                    if (value != null)
                    {
                        objectId = value;
                        objectRelate = Resource.ObjectRelative.Employee;
                    }
                }
                else
                {
                    if (objectRelate == Resource.ObjectRelative.Employee)
                        employeeId = objectId;
                }
            }
        }
        private Guid? departmentId;
        public Guid? DepartmentId
        {
            get
            {
                return departmentId;
            }
            set
            {
                if (!FromView)
                {
                    departmentId = value;
                    if (value != null)
                    {
                        objectId = value;
                        objectRelate = Resource.ObjectRelative.Department;
                    }
                }
                else
                {
                    if (objectRelate == Resource.ObjectRelative.Department)
                        departmentId = objectId;
                }
            }
        }
        private bool? isUsing;
        public bool? IsUsing
        {
            get { return isUsing; }
            set { isUsing = FromView ? IsUse : value; }
        }
        private Guid? roleId;
        public Guid? RoleId
        {
            get
            {
                return roleId;
            }
            set
            {
                if (!FromView)
                {
                    roleId = value;
                    if (value != null)
                    {
                        objectId = value;
                        objectRelate = Resource.ObjectRelative.Role;
                    }
                }
                else
                {
                    if (objectRelate == Resource.ObjectRelative.Role)
                        roleId = objectId;
                }
            }
        }
        private string other;
        public string Other
        {
            get
            {
                return other;
            }
            set
            {
                if (!FromView)
                {
                    other = value;
                    if (!string.IsNullOrEmpty(value))
                    {
                        objectId = null;
                        objectRelate = Resource.ObjectRelative.Other;
                    }
                }
                else
                {
                }
            }
        }
        public bool? IsUse { get; set; }
        public bool? IsManage { get; set; }
        public bool? IsOwner { get; set; }

        public string ProcessExpediency { get; set; }
        #endregion
        #region Rule
        /// <summary>
        /// Use for employee ID or department ID or role ID.
        /// Else is empty when use for other
        /// </summary>
        [Display(Name = "Object id")]
        private Guid? objectId;
        public Guid? ObjectId
        {
            get { return objectId; }
            set
            {
                objectId = value;
            }
        }
        public string ObjectName
        {
            get;
            set;
        }
        public string ObjectImage { get; set; }
        private Resource.ObjectRelative objectRelate;
        public Resource.ObjectRelative ObjectRelate
        {
            get
            {
                return objectRelate;
            }
            set
            {
                objectRelate = value;
                if (FromView)
                {
                    employeeId = value == Resource.ObjectRelative.Employee ? objectId : null;
                    departmentId = value == Resource.ObjectRelative.Department ? objectId : null;
                    roleId = value == Resource.ObjectRelative.Role ? objectId : null;
                    other = value == Resource.ObjectRelative.Other ? ObjectName : null;
                }
                else
                {

                }
            }
        }
        public bool FromView { get; set; }
        public string LabelTemp { get; set; }
        #endregion
        #region Review
        public string AssetName { get; set; }
        public string AssetCode { get; set; }
        public string Owner { get; set; }
        public string User { get; set; }
        public string Manager { get; set; }
        public string ReviewerAvatarUrl { get; set; }
        public string ReviewerName { get; set; }
        public FileUploadBO Temp { get; set; }
        public int? ValueOfSecurity { get; set; }
        public int? ValueOfInviblate { get; set; }
        public int? ValueOfReady { get; set; }
        public int? ValueOfImportant
        {
            get
            {
                return getValueOfImportant();
            }
        }
        public int? Value
        {
            get;
            set;
        }
        public string SecurityName { get; set; }
        public string InviblateName { get; set; }
        public string ReadyName { get; set; }
        public string ImportantName { get; set; }
        public string AssetGroupSummaryName { get { return string.Format("{0} - {1} - {2}", AssetName, AssetCode, Owner); } }

        #endregion
        #region Method
        private int? getValueOfImportant()
        {
            var value = ValueOfSecurity == null && ValueOfInviblate == null && ValueOfReady == null ? (int?)null
                : (ValueOfSecurity == null ? 0 : ValueOfSecurity.Value)
                + (ValueOfInviblate == null ? 0 : ValueOfInviblate.Value)
                + (ValueOfReady == null ? 0 : ValueOfReady.Value);
            return value;
        }
        #endregion
    }
}
