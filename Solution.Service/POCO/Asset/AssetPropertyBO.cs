﻿namespace iDAS.Service
{
    using System.ComponentModel.DataAnnotations;

    public class AssetPropertyBO : BaseBO
    {
        #region Basic
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Name
        {
            get;
            set;
        }
        public string Code { get; set; }
        #endregion

    }
}
