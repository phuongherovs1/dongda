﻿namespace iDAS.Service
{
    using System;
    using System.ComponentModel.DataAnnotations;
    public class AssetReviewBO : BaseBO
    {
        #region Basic
        public Guid? AssetResourceRelateId { get; set; }
        public DateTime? DateOfReview { get; set; }
        public Guid? EmployeeId { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        private string other;
        public string Other
        {
            get
            {
                return string.IsNullOrEmpty(other) ? null : other;
            }
            set
            {
                other = value;
            }
        }
        public int? ValueOfSecurity { get; set; }
        public int? ValueOfInviblate { get; set; }
        public int? ValueOfReady { get; set; }
        public int? Value { get; set; }
        #endregion
        #region Rule
        public int ValueOfImportant
        {
            get
            {
                return getValueOfImportant();
            }
        }
        [Required(ErrorMessage = "Không được để trống trường này")]
        public EmployeeBO Reviewer { get; set; }
        public string ImportantColor { get; set; }
        public string ImportantName { get; set; }
        public string SecurityName { get; set; }
        public string InviblateName { get; set; }
        public string ReadyName { get; set; }

        public string AssetName { get; set; }
        public string AssetCode { get; set; }
        public string Owner { get; set; }
        public string User { get; set; }

        public string ReviewerAvatarUrl { get; set; }
        public FileUploadBO Temp { get; set; }
        #endregion
        #region method
        private int getValueOfImportant()
        {
            var value = (ValueOfSecurity == null ? 0 : ValueOfSecurity.Value)
                + (ValueOfInviblate == null ? 0 : ValueOfInviblate.Value)
                + (ValueOfReady == null ? 0 : ValueOfReady.Value);
            return value;
        }
        #endregion
    }
}
