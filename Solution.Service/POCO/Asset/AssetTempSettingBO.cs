﻿namespace iDAS.Service
{
    using System;
    using System.ComponentModel.DataAnnotations;
    public class AssetTempSettingBO : BaseBO
    {
        #region Basic
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Rule { get; set; }
        public string Method { get; set; }
        private string description;
        public string Description
        {
            get { return string.IsNullOrEmpty(description) ? string.Empty : description; }
            set { description = value; }
        }
        public Guid? Template { get; set; }
        public bool? IsUse { get; set; }
        #endregion
        #region Rule
        public FileUploadBO TemplateFile
        {
            get;
            set;
        }
        public bool IsChangeFile { get; set; }

        public int From { get; set; }
        public int To { get; set; }
        #endregion
    }
}
