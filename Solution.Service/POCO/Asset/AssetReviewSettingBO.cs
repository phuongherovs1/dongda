﻿namespace iDAS.Service
{
    using iDAS.Service.Common;
    using System;
    using System.ComponentModel.DataAnnotations;
    public class AssetReviewSettingBO : BaseBO
    {
        #region Basic

        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Name { get; set; }
        public bool? IsSecurity { get; set; }
        public bool? IsInviblate { get; set; }
        public bool? IsReady { get; set; }
        public bool? IsImportant { get; set; }
        public int? From { get; set; }
        public int? To { get; set; }
        [Required]
        public string Color { get; set; }
        public Guid? AssetTempSettingId { get; set; }
        private string description;
        public string Description
        {
            get { return string.IsNullOrEmpty(description) ? string.Empty : description; }
            set { description = value; }
        }
        public bool? IsUse { get; set; }
        public string LogicValue { get { return getLogicValue(); } }
        #endregion
        #region Rules
        public int QualityReview
        {
            get;
            set;
        }
        public bool IsOutOfValue { get; set; }
        #endregion
        private string getLogicValue()
        {
            var result = string.Empty;
            if (From.HasValue && To.HasValue)
            {
                result = From != To ? string.Format("{0} - {1}", From.Value, To.Value) : From.Value.ToString();
            }
            return result;
        }
        private int getQualityReview()
        {
            var result = IsSecurity == true ? (int)Resource.QualityReview.IsSecurity
                : IsInviblate == true ? (int)Resource.QualityReview.IsInviblate
                : IsReady == true ? (int)Resource.QualityReview.IsReady
                : (int)Resource.QualityReview.IsImportant;
            return result;
        }
    }
}
