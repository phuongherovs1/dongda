namespace iDAS.Service
{
    using System;

    public class AssetTempBO : BaseBO
    {
        #region Basic
        public Guid? AssetID { get; set; }
        public Guid? TempSettingID { get; set; }
        public Guid? Paster { get; set; }
        public DateTime? DateOfPaste { get; set; }
        public bool? IsUse { get; set; }
        public bool? IsNew { get; set; }
        public Guid? TempFile { get; set; }
        public string LabelTemp { get; set; }
        #endregion
        #region
        public EmployeeBO Creater { get; set; }

        public FileUploadBO Temp
        {
            get;
            set;
        }
        public FileUploadBO Template
        {
            get;
            set;
        }
        public string EmployeeName { get; set; }
        public string AvartarUrl { get; set; }
        public bool IsChangeFile { get; set; }
        public string TemplateName { get; set; }
        #endregion
    }
}
