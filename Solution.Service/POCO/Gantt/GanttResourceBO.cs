﻿using System;

namespace iDAS.Service
{
    public class GanttResourceBO
    {
        public Guid Id { get; set; }
        public string ResourceName { get; set; }
        public string ResourceImage { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public double PercentDone { get; set; }
        public double baselinePercentDone { get { return PercentDone; } }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? BaselineStartDate { get; set; }
        public DateTime? BaselineEndDate { get; set; }
        public bool leaf { get { return true; } }
        public bool expanded { get { return true; } }
        public string StartDateText
        {
            get { return GetDateText(StartDate); }
        }

        public string StartEndText
        {
            get { return GetDateText(EndDate); }
        }

        public string BaselineStartDateText
        {
            get { return GetDateText(BaselineStartDate); }
        }
        public string BaselineEndDateText
        {
            get { return GetDateText(BaselineEndDate); }
        }
        private string GetDateText(DateTime? value)
        {
            var result = value == null ? string.Empty
                : value.Value.ToString("dd-MM-yyyy hh:mm");
            return result;
        }
    }
}
