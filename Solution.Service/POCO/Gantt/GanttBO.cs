﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public class GanttBO
    {
        public GanttTask tasks { get; set; }
        public GanttDependency dependencies { get; set; }
        public bool success { get { return true; } }
    }
    public class GanttTask
    {
        public List<GanttTaskBO> rows { get; set; }
    }
    public class GanttDependency
    {
        public List<GanttDependencyBO> rows { get; set; }
    }
    public class GanttTaskBO
    {
        public Guid ID { get; set; }
        public int? HumanDepartmentID { get; set; }
        public Guid? HumanEmployeeID { get; set; }
        public Guid? TaskCategoryID { get; set; }
        public Guid? TaskLevelID { get; set; }
        public int? AuditID { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsPrivate { get; set; }
        public bool? IsNew { get; set; }
        public bool? IsEdit { get; set; }
        public bool? IsComplete { get; set; }
        public bool? IsPass { get; set; }
        public bool? IsPause { get; set; }
        public double? PercentDone { get; set; }
        public double? Cost { get; set; }
        public string Content { get; set; }
        public string Note { get; set; }
        public DateTime? CreateAt { get; set; }
        public Guid? CreateBy { get; set; }
        public DateTime? UpdateAt { get; set; }
        public Guid? UpdateBy { get; set; }
        public DateTime? CompleteTime { get; set; }
        public Guid? ParentID { get; set; }
        public double Duration { get { return getDurationDate(); } }
        public string DurationUnit { get { return "d"; } }
        public string SchedulingMode { get; set; }
        public DateTime? BaselineStartDate { get; set; }
        public DateTime? BaselineEndDate { get; set; }
        public int? BaselinePercentDone { get; set; }
        public string Cls { get; set; }
        public int? CalendarIdRaw { get; set; }
        public int? index { get; set; }
        public bool? expanded { get; set; }
        public double? Effort { get; set; }
        public string EffortUnit { get; set; }
        public string ConstraintType { get; set; }
        public DateTime? ConstraintDate { get; set; }
        public bool? ManuallyScheduled { get; set; }
        public bool? Draggable { get; set; }
        public bool? Resizable { get; set; }
        public bool? Rollup { get; set; }
        public bool? ShowInTimeline { get; set; }
        public string Color { get; set; }
        public List<GanttTaskSegments> Segments { get; set; }
        public List<GanttTaskBO> children { get; set; }
        public bool leaf { get { return children == null || children.Count == 0; } }
        private double getDurationDate()
        {
            var value = StartDate.HasValue && EndDate.HasValue ? (EndDate.Value.Date.AddDays(1) - StartDate.Value.Date).TotalDays : 0;
            return value;
        }

    }
    public class GanttDependencyBO
    {
        public Guid Id { get; set; }
        public Guid? From { get; set; }
        public Guid? To { get; set; }
        public int? Type { get; set; }
        public string Cls { get; set; }
        public int? Lag { get; set; }
        public string LagUnit { get; set; }
    }
    public class GanttTaskSegments
    {
        public Guid ID { get; set; }
        public Guid? TaskIdRaw { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? Duration { get { return getDuration(); } }
        public string DurationUnit { get; set; }
        public string Cls { get; set; }
        private decimal getDuration()
        {
            var result = StartDate == null || EndDate == null ? 0 : (EndDate.Value - StartDate.Value).TotalDays;
            return (decimal)result;
        }
    }
}
