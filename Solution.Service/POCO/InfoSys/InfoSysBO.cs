﻿namespace iDAS.Service
{
    public class InfoSysBO
    {
        public int Id { get; set; }
        public string InfoSystemCode { get; set; }
        public string Name { get; set; }
        public string Purpose { get; set; }
        public bool InternalAccess { get; set; }
        public bool RemoteAccess { get; set; }
        public bool PartnerAccess { get; set; }
        public string IsActive { get; set; }
    }
}
