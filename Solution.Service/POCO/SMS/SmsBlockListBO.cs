﻿using System;

namespace iDAS.Service
{
    public class SmsBlockListBO : BaseBO
    {
        #region Base
        public string SendNumber { get; set; }
        public string MessageContent { get; set; }
        public string Keyword { get; set; }
        public string NetworkProvider { get; set; }
        public string Type { get; set; }
        public DateTime? SendDate { get; set; }
        public Guid? IdSms { get; set; }
        #endregion
        #region Rule
        public string TypeString
        {
            get
            {
                if (Type == "3")
                    return "<span style='color:red'>Đầu số</span>";
                else if (Type == "2")
                    return "<span style='color:blue'>Số điện thoại đài</span>";
                else
                    return "<span style='color:green'>Số định danh</span>";
            }
        }
        public string Sendreflect { get; set; }
        public string MessageContentSynchronized { get; set; }
        #endregion
    }
}
