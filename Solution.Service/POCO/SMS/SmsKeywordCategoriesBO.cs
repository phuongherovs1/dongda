﻿using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public class SmsKeywordCategoriesBO : BaseBO
    {
        private string name;
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Name { get; set; }
        public bool IsParent { get; set; }
        public string Description { get; set; }
    }
}
