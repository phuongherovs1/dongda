﻿using System;
using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public class SmsManagersBO : BaseBO
    {
        #region Base
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string RecieveNumber { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string SendNumber { get; set; }
        public DateTime? SynchronizedDate { get; set; }
        public bool? IsSynchronized { get; set; }
        public string MessageContent { get; set; }
        public string NetworkProvider { get; set; }
        public DateTime? SendDate { get; set; }
        #endregion
    }
}
