﻿using System;

namespace iDAS.Service
{
    public class SmsTypeBO : BaseBO
    {
        public Guid? IdSms { get; set; }
        public Guid? IdType { get; set; }
    }
}
