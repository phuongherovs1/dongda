﻿using System;
using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public class SmsKeywordsBO : BaseBO
    {
        #region Base
        private string keywordvalue;
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string KeywordValue
        {
            get;
            set;
        }
        public string Description { get; set; }
        [Required]
        public Guid? KeywordCategoryId { get; set; }
        #endregion
    }
}
