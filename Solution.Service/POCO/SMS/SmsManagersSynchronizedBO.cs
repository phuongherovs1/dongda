﻿using System;
using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public class SmsManagersSynchronizedBO : BaseBO
    {
        #region Base
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string RecieveNumber { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string SendNumber { get; set; }
        public string MessageContent { get; set; }
        public string NetworkProvider { get; set; }
        public string lstFroupKey { get; set; }
        //public bool? FilterContentStatus { get; set; }
        public DateTime? TypingDate { get; set; }
        public DateTime? FilterContentDate { get; set; }
        public DateTime? SendDate { get; set; }
        //public bool? IsTyping { get; set; }
        private bool? isTyping;
        public bool? IsTyping
        {
            get
            {
                return isTyping ?? (isTyping = false);
            }
            set
            {
                isTyping = value;
            }
        }
        private bool? filterContentStatus;
        public bool? FilterContentStatus
        {
            get
            {
                return filterContentStatus ?? (filterContentStatus = false);
            }
            set
            {
                filterContentStatus = value;
            }
        }
        #endregion
    }
}
