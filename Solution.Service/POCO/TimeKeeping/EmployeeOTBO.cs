﻿using System;

namespace iDAS.Service
{
    public class EmployeeOTBO : BaseBO
    {
        public Guid EmployeeId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Reason { get; set; }
        public Guid? ApprovedBy { get; set; }
        public int Status { get; set; }
        public int Level { get; set; }

        private EmployeeBO employeePerform;
        public EmployeeBO EmployeePerform
        {
            get
            {
                return employeePerform ?? (employeePerform = new EmployeeBO());
            }
            set
            {
                employeePerform = value;
            }
        }
        private EmployeeBO employeePerformNext;
        public EmployeeBO EmployeePerformNext
        {
            get
            {
                return employeePerformNext ?? (employeePerformNext = new EmployeeBO());
            }
            set
            {
                employeePerformNext = value;
            }
        }
    }
}
