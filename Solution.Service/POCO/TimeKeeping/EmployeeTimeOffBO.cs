﻿namespace iDAS.Service
{
    using System;

    public class EmployeeTimeOffBO : BaseBO
    {
        public Guid EmployeeId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string EmployeeReplace { get; set; }
        public string Reason { get; set; }
        public Guid? ApprovedBy { get; set; }
        public int Status { get; set; }

        private EmployeeBO employeePerform;
        private EmployeeBO employeePerformNext;
        public EmployeeBO EmployeePerform
        {
            get
            {
                return employeePerform ?? (employeePerform = new EmployeeBO());
            }
            set
            {
                employeePerform = value;
            }
        }
        public EmployeeBO EmployeePerformNext
        {
            get
            {
                return employeePerformNext ?? (employeePerformNext = new EmployeeBO());
            }
            set
            {
                employeePerformNext = value;
            }
        }
    }
}
