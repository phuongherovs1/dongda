﻿namespace iDAS.Service
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class TimekeepingBO
    {
        public Guid Id { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? EmployeeId { get; set; }

        public string DepartmentName { get; set; }

        public string ReasonExplanation { get; set; }

        public string Reason { get; set; }

        public string ApproveName { get; set; }

        public Guid? ApproveId { get; set; }

        private EmployeeBO approveEmployee;

        public EmployeeBO ApproveEmployee
        {
            get
            {
                return approveEmployee ?? (approveEmployee = new EmployeeBO());
            }
            set
            {
                approveEmployee = value;
            }
        }

        public int Status { get; set; }

        public string StatusText
        {
            get
            {
                if (Status == 0 && !string.IsNullOrEmpty(Reason))
                    return "Chưa yêu cầu giải trình";
                else if (Status == 1)
                    return "Chờ giải trình";
                else if (Status == 2)
                    return "Đã giải trình, Chờ xét duyệt";
                else if (Status == 3)
                    return "Đã duyệt";
                else if (Status == 4)
                    return "Từ chối";
                else
                    return "";
            }
        }

        public string DateOfWeek
        {
            get
            {
                return Common.Utilities.DayOfWeek.GetNameDayOfWeek(Date.Value);
            }
        }

        public string Name { get; set; }

        public DateTime? Date { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(50)]
        public string TimeIn { get; set; }

        [StringLength(50)]
        public string TimeOut { get; set; }

        [StringLength(50)]
        public string TimeIn1 { get; set; }

        [StringLength(50)]
        public string TimeIn2 { get; set; }

        [StringLength(50)]
        public string TimeIn3 { get; set; }

        [StringLength(50)]
        public string TimeOut1 { get; set; }

        [StringLength(50)]
        public string TimeOut2 { get; set; }

        [StringLength(50)]
        public string TimeOut3 { get; set; }

        public string DepartmentTitle { get; set; }

        public int NumberExplanation { get; set; }
        public bool RoleEdit { get; set; }

    }
}
