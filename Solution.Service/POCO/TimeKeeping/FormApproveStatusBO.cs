﻿using System;

namespace iDAS.Service
{
    public class FormApproveStatusBO : BaseBO
    {
        public Guid FormId { get; set; }
        public string TableName { get; set; }
        public bool isFinal { get; set; }
        public int Status { get; set; }
        public string Reason { get; set; }
        public Guid? SentBy { get; set; }
        public Guid? ApprovedBy { get; set; }
        public DateTime? ApprovedAt { get; set; }

        private EmployeeBO form;
        public EmployeeBO employeeForm
        {
            get
            {
                return form ?? (form = new EmployeeBO());
            }
            set
            {
                form = value;
            }
        }
    }
}
