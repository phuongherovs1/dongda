﻿namespace iDAS.Service
{
    using System;

    public class EmployeeWorkOutBO : BaseBO
    {
        public Guid EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Address { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Reason { get; set; }
        public Guid? ApprovedBy { get; set; }

        // 1-Gửi duyệt
        // 2-Duyệt
        // 3-Ko duyệt
        public int Status { get; set; }

        private EmployeeBO employeePerform;
        public EmployeeBO EmployeePerform
        {
            get
            {
                return employeePerform ?? (employeePerform = new EmployeeBO());
            }
            set
            {
                employeePerform = value;
            }
        }
        private EmployeeBO employeePerformNext;
        public EmployeeBO EmployeePerformNext
        {
            get
            {
                return employeePerformNext ?? (employeePerformNext = new EmployeeBO());
            }
            set
            {
                employeePerformNext = value;
            }
        }
    }
}
