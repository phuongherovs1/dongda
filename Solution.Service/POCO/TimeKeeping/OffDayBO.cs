﻿using System;

namespace iDAS.Service
{
    public class OffDayBO : BaseBO
    {
        public Guid? EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentTitle { get; set; }
        public int Using { get; set; }
        public int Space
        {
            get
            {
                if (Using == 0)
                    return OffDays;
                else
                    return OffDays - Using;
            }
        }
        public int OffDays { get; set; }

        private EmployeeBO employeePerform;
        public EmployeeBO EmployeePerform
        {
            get
            {
                return employeePerform ?? (employeePerform = new EmployeeBO());
            }
            set
            {
                employeePerform = value;
            }
        }
    }
}
