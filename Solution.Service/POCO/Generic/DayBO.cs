﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public class DayItemBO
    {
        public Guid Id { get; set; }
        public DateTime Day { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string TimeText
        {
            get
            {
                var text = string.Empty;
                if (Start.Date == Day.Date)
                {
                    text = Start.Hour + "h:" + Start.Minute;
                }
                else
                {
                    text = "Từ sáng";
                }
                if (End.Date == Day.Date)
                {
                    text = text + " - " + End.Hour + "h:" + End.Minute;
                }
                else
                {
                    text = text + " - " + "Cuối ngày";
                }
                if (Start.Date < Day.Date && End.Date > Day.Date)
                {
                    text = "Cả ngày";
                }
                return text;
            }
        }
        public string Content { get; set; }
        public string Name { get; set; }
        public string color
        {
            get
            {
                if (DateTime.Now.Subtract(End).Minutes > 0)
                    return "gray";
                else
                    return "black";
            }
        }
    }
    public class DayBO
    {
        public DateTime Day { get; set; }
        public bool IsCurrentDay
        {
            get
            {
                return Day.Date == DateTime.Now.Date;
            }
        }
        public string DayOfWeekText
        {
            get
            {
                var text = Resource.Calendar.GetDayOfWeekText(Day.DayOfWeek);
                return string.Format("{0} ({1})", text, Day.ToString("dd/MM"));
            }
        }
        private List<DayItemBO> items;
        public List<DayItemBO> Items
        {
            get
            {
                return items = items ?? new List<DayItemBO>();
            }
            set
            {
                items = value;
            }
        }
    }
}
