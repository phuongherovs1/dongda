﻿using System;

namespace iDAS.Service
{
    public class DocumentEmployeeBO : BaseBO
    {
        #region Base
        public Guid? DepartmentId { get; set; }
        public int Process { get; set; }
        public int RoleType { get; set; }
        public int NextOrder { get; set; }
        public string Name
        {
            get;
            set;
        }
        #endregion

        #region Rule
        /// <summary>
        /// đường dẫn ảnh đại diện của nhân sự
        /// </summary>
        public string AvatarUrl
        {
            get;
            set;
        }
        /// <summary>
        /// chức danh của nhân sự có dạng: Chức danh 1, Chức danh 2...
        /// </summary>
        public string RoleNames
        {
            get;
            set;
        }
        #endregion
    }

}
