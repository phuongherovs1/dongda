﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iDAS.Service
{
    public class FileUploadBO
    {
        /// <summary>
        /// Danh sách dữ liệu đưa lên
        /// </summary>
        public IEnumerable<Guid> Files { get; set; }
        public string ListFile
        {
            get
            {
                if (Files != null && Files.Count() > 0)
                {
                    var value = Files.Select(i => i.ToString()).Aggregate((current, next) => current + "," + next);
                    return value;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        /// <summary>
        /// Dùng khi xóa 
        /// </summary>
        public List<Guid> FileRemoves
        {
            get
            {
                var value = iDAS.Service.Common.Utilities.ToListGuid(ListFileRemove);
                return value;
            }
        }
        public string ListFileRemove { get; set; }
        /// <summary>
        /// Trong trường hợp thêm mới
        /// </summary>
        public List<HttpPostedFileBase> FileAttachments { get; set; }
    }
}
