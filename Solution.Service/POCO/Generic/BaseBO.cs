﻿using iDAS.Service.Common;
using System;
using System.Web;

namespace iDAS.Service
{
    public abstract class BaseBO
    {
        public Guid Id { get; set; }
        private bool? isDelete;
        public bool? IsDelete
        {
            get
            {
                return isDelete ?? (isDelete = false);
            }
            set
            {
                isDelete = value;
            }
        }
        public DateTime? CreateAt { get; set; }
        public Guid? CreateBy { get; set; }
        public DateTime? UpdateAt { get; set; }
        public Guid? UpdateBy { get; set; }

        public string GetStringValue(string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : HttpUtility.HtmlDecode(value) == value ? HttpUtility.HtmlEncode(value) : value;
        }
        public Guid UserId { get; set; }
        public bool DisableDelete { get; set; }
        public bool DisableUpdate { get; set; }
        public Resource.Operation Operator
        {
            get
            {
                if (Id == null || Id == Guid.Empty)
                    return Resource.Operation.Create;
                else
                    if ((UserId == CreateBy || AllowUpdate) && !DisableUpdate)
                {
                    if (DisableDelete)
                        return Resource.Operation.Update;
                    else
                        return Resource.Operation.UpdateDelete;
                }
                else
                    return Resource.Operation.Read;
            }
        }

        private bool? allowCreate;
        public virtual bool AllowCreate
        {
            get
            {
                return allowCreate ?? (Id == null || Id == Guid.Empty);
            }
            set
            {
                allowCreate = value;
            }
        }
        private bool? allowUpdate;
        public virtual bool AllowUpdate
        {
            get
            {
                return allowUpdate ?? (Id != null && Id != Guid.Empty && UserId == CreateBy);
            }
            set
            {
                allowUpdate = value;
            }
        }
        private bool? allowDelete;
        public virtual bool AllowDelete
        {
            get
            {
                return allowDelete ?? (Id != null && Id != Guid.Empty && UserId == CreateBy);
            }
            set
            {
                allowDelete = value;
            }
        }
        private bool? allowRead;
        public virtual bool AllowRead
        {
            get
            {
                return allowRead ?? (CreateBy.HasValue && CreateBy != UserId);
            }
            set
            {
                allowRead = value;
            }
        }
    }
}
