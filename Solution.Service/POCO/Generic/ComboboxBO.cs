﻿namespace iDAS.Service
{
    public class ComboboxBO
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public int Order { get; set; }
        public int Process { get; set; }
    }
}
