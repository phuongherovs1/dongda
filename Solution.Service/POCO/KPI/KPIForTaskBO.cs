﻿using System;

namespace iDAS.Service
{
    public class KPIForTaskBO : BaseBO
    {
        #region Base
        public Guid? TaskId { get; set; }

        public string EmployeeName { get; set; }

        public Guid? EmployeeId { get; set; }
        //Điểm tự chấm
        public int? Score { get; set; }

        //Điểm theo chất lượng
        public int? Score_Value { get; set; }
        //Điểm theo thời gian
        public int? Score_Time { get; set; }
        //Điểm cộng
        public int? Score_Plus { get; set; }
        //Điểm trừ
        public int? Score_Minus { get; set; }
        //Điểm thái độ
        public int? Score_Attitude { get; set; }
        public int? Attitude1 { get; set; }
        public int? Attitude2 { get; set; }
        public int? Attitude3 { get; set; }
        public int? Attitude4 { get; set; }
        public int? AttitudePlus { get; set; }
        public int? AttitudeMinus { get; set; }

        public int? Manager { get; set; }
        public int? ManagerPlus { get; set; }
        public int? ManagerMinus { get; set; }

        public int? Weightscore { get; set; }

        public string Note { get; set; }
        #endregion

        #region Rule
        public string EmployeeImageUrl { get; set; }

        public string EmployeeImageUrlMark { get; set; }

        public string EmployeeNameMark { get; set; }

        public string DepartmentNames { get; set; }

        public string TaskName { get; set; }

        public string StatusProcessTask { get; set; }

        public int? MaxScore { get; set; }

        public string DepartmentIds { get; set; }

        public Guid? EmployeeIdMark { get; set; }

        public int? SumScore { get; set; }

        public EmployeeBO Employee { get; set; }
        #endregion
    }
}
