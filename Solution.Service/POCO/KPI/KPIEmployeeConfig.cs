﻿using System;

namespace iDAS.Service
{
    public class KPIEmployeeConfig : BaseBO
    {

        public int Type { get; set; }

        public Guid EmployeeId { get; set; }
    }
}
