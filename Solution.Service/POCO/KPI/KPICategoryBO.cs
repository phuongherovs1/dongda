﻿using System;

namespace iDAS.Service
{
    public class KPICategoryBO : BaseBO
    {
        #region Base

        public string Name { get; set; }

        public string Note { get; set; }

        public string Description { get; set; }
        /// <summary>
        /// Id của thằng KPICategory Cha
        /// </summary>
        public Guid? ParrentId { get; set; }

        public int? Maxscore { get; set; }

        public int? Level { get; set; }

        public int? Weightscore { get; set; }

        public int? Type { get; set; }
        #endregion

        #region Rule
        /// <summary>
        /// giá trị: true - Category chứa Categorycon, false - Category không chứa Category con
        /// </summary>
        public bool IsParent { get; set; }
        public string ParrentText { get; set; }
        public bool IsDisabled
        {
            get;
            set;
        }
        public bool Checked { get; set; }
        #endregion
    }
}
