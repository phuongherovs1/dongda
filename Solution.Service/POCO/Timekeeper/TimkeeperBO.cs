﻿namespace iDAS.Service.POCO.Timekeeper
{
    public class TimekeeperBO
    {
        public int IDTimekeeper { set; get; }
        public string Name { set; get; }
        public string IP { set; get; }
        public int Status { set; get; }
        public int Post { set; get; }
        public string Serial { set; get; }
    }
}
