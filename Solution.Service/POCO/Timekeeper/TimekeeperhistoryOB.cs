﻿using System;

namespace iDAS.Service.POCO.Timekeeper
{
    public class TimekeeperhistoryOB
    {
        public long IDTimekeepinghistory { set; get; }
        public int IDTimeKeeper { set; get; }
        public string Code { set; get; }
        public DateTime dateTimekeeping { set; get; }

    }
}
