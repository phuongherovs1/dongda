﻿using System;
using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public class BotnetManagerBO : BaseBO
    {
        public object city;
        #region Base

        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public DateTime? TimeStamp { get; set; }

        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Ip { get; set; }

        public DateTime? TypingInfectionDate { get; set; }

        public bool? IsTypingInfection { get; set; }
        public bool? isTypingInfection
        {
            get
            {
                return IsTypingInfection ?? (IsTypingInfection = false);
            }
            set
            {
                IsTypingInfection = value;
            }
        }
        public string Port { get; set; }


        public string Asn { get; set; }


        public string Geo { get; set; }


        public string Region { get; set; }


        public string City { get; set; }


        public string HostName { get; set; }


        public string Type { get; set; }


        public string Infection { get; set; }


        public string Url { get; set; }


        public string Agent { get; set; }


        public string CC { get; set; }


        public string CC_Port { get; set; }


        public string CC_Asn { get; set; }


        public string CC_Geo { get; set; }


        public string CC_Dns { get; set; }

        public int? Count { get; set; }


        public string P0f_Genre { get; set; }


        public string P0f_Detail { get; set; }
        public int Status { get; set; }

        #endregion

    }
}
