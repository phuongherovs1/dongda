﻿using iDAS.DataModel;
using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public class BotnetInfectionBO : BaseBO
    {
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Treatment { get; set; }

        public string Link { get; set; }
        public virtual HistoryBotnet HistoryBotnet { get; set; }
    }
}
