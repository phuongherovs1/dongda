﻿using iDAS.DataModel;
using System;

namespace iDAS.Service
{
    public class HistoryBotnetBO : BaseBO
    {
        #region Base

        public Guid? IdHeadquarters { get; set; }
        public Guid? IdInfection { get; set; }
        public Guid? IdBotnet { get; set; }
        public int? Status { get; set; }
        public DateTime? InfectionDate { get; set; }
        public string Abbreviation { get; set; }
        public virtual BotnetInfection BotnetInfection { get; set; }
        #endregion
    }
}
