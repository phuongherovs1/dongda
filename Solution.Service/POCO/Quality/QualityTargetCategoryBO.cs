﻿namespace iDAS.Service
{
    public class QualityTargetCategoryBO : BaseBO
    {
        #region Base
        public string Name { get; set; }

        public string Note { get; set; }
        #endregion

        #region Rule
        /// <summary>
        /// Số lượng mục tiêu theo từng nhóm
        /// </summary>
        public int? CountTarget { get; set; }
        /// <summary>
        /// Tên nhóm mục tiêu
        /// </summary>
        public string NameFormat
        {
            get
            {
                var result = CountTarget > 0 ? string.Format("{0} ({1})", Name, CountTarget) : string.Format("{0}", Name);
                return result;

            }
        }
        #endregion

    }
}
