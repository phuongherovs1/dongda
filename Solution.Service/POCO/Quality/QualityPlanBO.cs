﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class QualityPlanBO : BaseBO
    {
        #region Base
        public Guid? QualityTargetId { get; set; }

        public Guid? ParentId { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? Planner { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public int? Status { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }
        #endregion

        #region Rule
        /// <summary>
        /// Phòng ban
        /// </summary>
        public string DepartmentName { get; set; }
        /// <summary>
        /// Tên mục tiêu
        /// </summary>
        public string TargetName { get; set; }
        /// <summary>
        /// Người phê duyệt
        /// </summary>
        public EmployeeBO EmployeeApproveBy { get; set; }
        /// <summary>
        /// Người lập
        /// </summary>
        public EmployeeBO EmployeeCreateBy { get; set; }

        public string StatusText
        {
            get
            {
                var statusText = string.Empty;
                switch (Status)
                {
                    case (int)Resource.QualityPlan.New:
                        statusText = Resource.QualityPlanText.New;
                        break;
                    case (int)Resource.QualityPlan.WaitApprove:
                        statusText = Resource.QualityPlanText.WaitApprove;
                        break;
                    case (int)Resource.QualityPlan.Approved:
                        statusText = Resource.QualityPlanText.Approved;
                        break;
                    case (int)Resource.QualityPlan.NotApprove:
                        statusText = Resource.QualityPlanText.NotApprove;
                        break;
                }
                return statusText;
            }
        }

        public bool? IsAccept { get; set; }

        /// <summary>
        /// Tổng số công việc của kế hoạch
        /// </summary>
        public int? TotalTask { get; set; }

        /// <summary>
        /// Công việc hoàn thành
        /// </summary>
        public int? TaskFinish { get; set; }

        /// <summary>
        /// Phần trăm thực hiện kế hoạch
        /// </summary>
        public float ResultTask { get; set; }
        #endregion

    }
}
