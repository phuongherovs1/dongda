﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public class QualityTargetBO : BaseBO
    {
        #region Base
        public Guid? DepartmentId { get; set; }

        public Guid? QualityTargetIssueId { get; set; }

        public Guid? QualityUnitId { get; set; }

        public string TargetValueExpression { get; set; }

        public string Name { get; set; }

        public decimal? TargetValue { get; set; }

        public decimal? BaseValue { get; set; }

        public decimal? Scale { get; set; }

        public int? Destination { get; set; }

        public decimal? ResultValue { get; set; }

        public bool? IsScale { get; set; }

        public DateTime? ExpectAt { get; set; }

        public int? TargetType { get; set; }

        public string Note { get; set; }

        public int? Status { get; set; }

        public string Description { get; set; }
        #endregion

        #region Rule
        /// <summary>
        /// Vấn đề của mục tiêu
        /// </summary>
        public string Issue { get; set; }
        /// <summary>
        /// Đơn vị
        /// </summary>
        public string Unit { get; set; }
        /// <summary>
        /// Trạng thái
        /// </summary>
        public string StatusText
        {
            get
            {
                var statusText = string.Empty;
                switch (Status)
                {
                    case (int)Resource.TargetStatus.Approve:
                        statusText = Resource.TargetStatusText.Approve;
                        break;
                    case (int)Resource.TargetStatus.Destroy:
                        statusText = Resource.TargetStatusText.Destroy;
                        break;
                    case (int)Resource.TargetStatus.New:
                        statusText = Resource.TargetStatusText.New;
                        break;
                    case (int)Resource.TargetStatus.NotApprove:
                        statusText = Resource.TargetStatusText.NotApprove;
                        break;
                    case (int)Resource.TargetStatus.Perform:
                        statusText = Resource.TargetStatusText.Perform;
                        break;
                    case (int)Resource.TargetStatus.WaitApproval:
                        statusText = Resource.TargetStatusText.WaitApproval;
                        break;
                    case (int)Resource.TargetStatus.Expire:
                        statusText = Resource.TargetStatusText.Expire;
                        break;
                    case (int)Resource.TargetStatus.Pass:
                        statusText = Resource.TargetStatusText.Pass;
                        break;
                    case (int)Resource.TargetStatus.NotPass:
                        statusText = Resource.TargetStatusText.NotPass;
                        break;
                }
                return statusText;
            }
        }
        /// <summary>
        /// Tên phòng ban
        /// </summary>
        public string DepartmentName { get; set; }

        public List<ExpressionBO> Expressions { get; set; }

        /// <summary>
        /// Dấu biểu thức thứ nhất
        /// </summary>
        public int? Expression1 { get; set; }
        /// <summary>
        /// Giá trị biểu thức thứ nhất
        /// </summary>
        public int? ValueExpression1 { get; set; }
        /// <summary>
        /// Dấu biểu thức thứ 2
        /// </summary>
        public int? Expression2 { get; set; }
        /// <summary>
        /// Giá trị biểu thức thứ 2
        /// </summary>
        public int? ValueExpression2 { get; set; }
        /// <summary>
        /// Dấu biểu thức thứ 3
        /// </summary>
        public int? Expression3 { get; set; }
        /// <summary>
        /// Giá trị biểu thức thứ 3
        /// </summary>
        public int? ValueExpression3 { get; set; }
        /// <summary>
        /// Dấu biểu thức thứ 4
        /// </summary>
        public int? Expression4 { get; set; }
        /// <summary>
        /// Giá trị biểu thức thứ 4
        /// </summary>
        public int? ValueExpression4 { get; set; }
        /// <summary>
        /// Dấu biểu thức thứ 5
        /// </summary>
        public int? Expression5 { get; set; }
        /// <summary>
        /// Giá trị biểu thức thứ 5
        /// </summary>
        public int? ValueExpression5 { get; set; }

        /// <summary>
        /// Ẩn hoặc hiện control nhập biểu thức
        /// </summary>
        public bool? Show1 { get; set; }
        public bool? Show2 { get; set; }
        public bool? Show3 { get; set; }
        public bool? Show4 { get; set; }
        public bool? Show5 { get; set; }

        public bool? IsAccept { get; set; }
        #endregion
    }

    public class ExpressionBO
    {
        public int Operator { get; set; }
        public int Value { get; set; }
    }
}
