﻿using System;

namespace iDAS.Service
{
    public class QualityNCRelationBO : BaseBO
    {
        #region Base
        public string TypeId { get; set; }
        public Guid? QualityNCId { get; set; }
        public string TypeName { get; set; }
        public Guid? IdRelation { get; set; }
        public Guid? EmployeeId { get; set; }

        public Guid? DepartmentTitleId { get; set; }

        public Guid? DepartmentId { get; set; }
        public int? Flag { get; set; }
        #endregion
    }
}
