using System;

namespace iDAS.Service
{
    public class QualityMeetingRecordBO : BaseBO
    {
        public Guid? QualityMeetinPlanDetailId { get; set; }

        public DateTime? MeetingAt { get; set; }

        public string Location { get; set; }

        public string Contents { get; set; }

        public string Result { get; set; }

        public int? MeetingStatus { get; set; }

    }
}
