using System;

namespace iDAS.Service
{
    public class QualityMeetingPlanReportBO : BaseBO
    {
        #region Basic
        public Guid? QualityMeetingPlanDetailId { get; set; }

        public Guid? DepartmentId { get; set; }

        public string Contents { get; set; }

        public string ReportContent { get; set; }

        public string ReportSuggest { get; set; }

        public Guid? Reporter { get; set; }

        public string DepartmentName { get; set; }
        #endregion 
        #region rule
        public string ReporterName { get; set; }
        #endregion

    }
}
