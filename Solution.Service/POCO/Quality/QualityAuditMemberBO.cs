﻿using System;

namespace iDAS.Service
{
    public class QualityAuditMemberBO : BaseBO
    {
        #region Base
        public Guid? QualityAuditPlanDetailId { get; set; }
        public Guid? EmployeeId { get; set; }
        public int? RoleType { get; set; }
        public bool? IsNotAvailable { get; set; }
        #endregion
        public EmployeeBO Member { get; set; }
    }
}
