﻿using System;

namespace iDAS.Service
{
    public class QualityAuditProgramTermBO : BaseBO
    {
        #region Base
        public Guid? QualityAuditProgramId { get; set; }

        public Guid? ISOTermId { get; set; }
        #endregion

    }
}
