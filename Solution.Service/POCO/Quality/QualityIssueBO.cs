namespace iDAS.Service
{
    public class QualityIssueBO : BaseBO
    {
        public string Name { get; set; }
        public string Note { get; set; }

        #region Rule
        public string NCText { get; set; }
        #endregion
    }
}
