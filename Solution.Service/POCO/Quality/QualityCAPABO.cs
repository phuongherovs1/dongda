﻿using System;

namespace iDAS.Service
{
    public class QualityCAPABO : BaseBO
    {
        #region Base


        public Guid? QualityNCId { get; set; }

        public string Solution { get; set; }

        public Guid? AssignBy { get; set; }

        public Guid? EmployeeId { get; set; }
        public DateTime? StartAt { get; set; }
        public DateTime? EndAt { get; set; }

        public int? Status { get; set; }
        #endregion

        public QualityNCBO QualityNC { get; set; }
        public EmployeeBO EmployeePerformBy { get; set; }
    }
}
