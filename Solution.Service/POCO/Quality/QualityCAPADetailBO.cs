﻿using System;

namespace iDAS.Service
{
    public class QualityCAPADetailBO : BaseBO
    {
        #region Base

        public Guid? QualityCAPAId { get; set; }
        public Guid? QualityNCId { get; set; }
        public Guid? TaskId { get; set; }
        #endregion
    }
}
