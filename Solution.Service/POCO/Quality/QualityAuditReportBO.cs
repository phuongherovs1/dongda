﻿using System;

namespace iDAS.Service
{
    public class QualityAuditReportBO : BaseBO
    {
        #region Base
        public Guid? QualityAuditPlanDetailId { get; set; }

        public bool? IsSave { get; set; }

        public string Contents { get; set; }

        public string Scope { get; set; }

        public string Location { get; set; }
        #endregion
    }
}
