﻿using System;

namespace iDAS.Service
{
    public class QualityAuditNotebookNCBO : BaseBO
    {
        #region Base
        public Guid? QualityAuditNotebookId { get; set; }

        public Guid? QualityNCId { get; set; }

        public bool? IsAccept { get; set; }
        #endregion
    }
}
