﻿using System;

namespace iDAS.Service
{
    public class ISOTermBO : BaseBO
    {
        #region Base
        public Guid? ISOStandardId { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        #endregion
        public bool IsSelected { get; set; }
        public string ISOStandardName { get; set; }
    }
}
