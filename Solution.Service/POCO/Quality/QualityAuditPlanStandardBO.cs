﻿using System;

namespace iDAS.Service
{
    public class QualityAuditPlanStandardBO : BaseBO
    {
        #region Base
        public Guid? QualityAuditPlanId { get; set; }

        public Guid? ISOStandardId { get; set; }
        #endregion

    }
}
