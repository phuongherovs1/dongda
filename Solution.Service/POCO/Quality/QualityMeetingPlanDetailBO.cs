﻿using System;

namespace iDAS.Service
{
    public class QualityMeetingPlanDetailBO : BaseBO
    {
        #region Basic
        public Guid? QualityMeetingPlanId { get; set; }

        public int? OrderTime { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public Guid? DepartmentId { get; set; }

        public string Location { get; set; }

        public string Note { get; set; }

        public int? Status { get; set; }

        public Guid? ApproveBy { get; set; }
        #endregion 

        #region Rule

        public EmployeeBO EmployeeApprove { get; set; }

        public string PlanName { get; set; }

        public QualityMeetingPlanBO Plan { get; set; }

        public TimeSpan? StartAtTime { get; set; }
        public TimeSpan? EndAtTime { get; set; }
        /// <summary>
        /// Thành phần tham gia
        /// </summary>
        public string MeetingMemberJoin { get; set; }
        public int MeetingMemberJoinCount { get; set; }

        /// <summary>
        /// Nội dung cuộc họp
        /// </summary>
        public string SubjectText { get; set; }
        public int SubjectCount { get; set; }

        public string MeetingDepartmentName { get; set; }

        public string StatusText
        {
            get
            {
                var result = Status == (int)Common.Resource.MeetingStatus.Cancel ? Common.Resource.MeetingStatusText.Cancel
                    : Status == (int)Common.Resource.MeetingStatus.Finish ? Common.Resource.MeetingStatusText.Finish
                    : Status == (int)Common.Resource.MeetingStatus.Perform ? Common.Resource.MeetingStatusText.Perform
                    : Status == (int)Common.Resource.MeetingStatus.Approve ? Common.Resource.MeetingStatusText.Approve
                    : Status == (int)Common.Resource.MeetingStatus.WaitApprove ? Common.Resource.MeetingStatusText.WaitApprove
                    : Status == (int)Common.Resource.MeetingStatus.New ? Common.Resource.MeetingStatusText.New
                    : Common.Resource.MeetingStatusText.New;
                return result;
            }
        }

        public string StatusColor
        {
            get
            {
                var result = Status == (int)Common.Resource.MeetingStatus.Cancel ? Common.Resource.MeetingStatusColor.Cancel
                    : Status == (int)Common.Resource.MeetingStatus.Finish ? Common.Resource.MeetingStatusColor.Finish
                    : Status == (int)Common.Resource.MeetingStatus.Perform ? Common.Resource.MeetingStatusColor.Perform
                    : Status == (int)Common.Resource.MeetingStatus.Approve ? Common.Resource.MeetingStatusColor.Approve
                    : Status == (int)Common.Resource.MeetingStatus.WaitApprove ? Common.Resource.MeetingStatusColor.WaitApprove
                    : Status == (int)Common.Resource.MeetingStatus.New ? Common.Resource.MeetingStatusColor.New
                    : Common.Resource.MeetingStatusText.New;
                return result;
            }
        }

        public string MeetingTimeText
        {
            get
            {
                var result = string.Empty;
                result = StartAt.Value.Date == EndAt.Value.Date
                    ? (StartAt.Value.Date == DateTime.Now.Date ? string.Format("Hôm nay: từ {0} đến {1}", StartAt.Value.ToString("HH:mm"), EndAt.Value.ToString("HH:mm"))
                    : string.Format("Ngày: {0} từ {1} đến {2}", StartAt.Value.ToString("dd-MM-yyyy"), StartAt.Value.ToString("HH:mm"), EndAt.Value.ToString("HH:mm")))
                    : string.Format("Từ ngày: {0} đến {1}", StartAt.Value.ToString("dd-MM-yyyy HH:mm"), EndAt.Value.ToString("dd-MM-yyyy HH:mm"));
                return result;
            }
        }

        #endregion
    }
}
