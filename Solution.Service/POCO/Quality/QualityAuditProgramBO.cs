﻿using System;

namespace iDAS.Service
{
    public class QualityAuditProgramBO : BaseBO
    {
        #region Base
        public Guid QualityAuditPlanDetailId { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public Guid? AuditBy { get; set; }

        public string BasicAudit { get; set; }
        #endregion
        #region Rule
        public bool IsAuditer { get; set; }
        public EmployeeBO Auditer { get; set; }
        public Guid QualityAuditPlanId { get; set; }
        public string DepartmentAudits { get; set; }
        public string TermAudits { get; set; }
        public string ObjectName { get; set; }
        public string ObjectID
        {
            get
            {
                if (AuditBy.HasValue && AuditBy != Guid.Empty)
                    return "Employee_" + AuditBy.Value;
                else
                    return string.Empty;
            }
        }
        public bool CountReferences { get; set; }
        public string TimeText
        {
            get
            {
                var result = string.Empty;
                result = StartAt.Value.Date == EndAt.Value.Date
                    ? (StartAt.Value.Date == DateTime.Now.Date ? string.Format("Hôm nay<br/>{0} - {1}", StartAt.Value.ToString("HH:mm"), EndAt.Value.ToString("HH:mm"))
                    : string.Format("{0}<br/>{1} - {2}", StartAt.Value.ToString("dd/MM/yyyy"), StartAt.Value.ToString("HH:mm"), EndAt.Value.ToString("HH:mm")))
                    : string.Format("{0}<br/>{1}", StartAt.Value.ToString("dd/MM/yyyy HH:mm"), EndAt.Value.ToString("dd/MM/yyyy HH:mm"));
                return result;
            }
        }
        #endregion
    }
}
