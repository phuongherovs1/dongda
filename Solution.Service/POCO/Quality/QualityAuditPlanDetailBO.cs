﻿using System;

namespace iDAS.Service
{
    public class QualityAuditPlanDetailBO : BaseBO
    {
        #region Base
        public Guid QualityAuditPlanId { get; set; }

        public int? Orders { get; set; }

        public int? Year { get; set; }

        public DateTime? StartPlanAt { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public string Scope { get; set; }

        public string Location { get; set; }

        public string RequestContents { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }
        public bool? IsAccept { get; set; }
        public bool? IsSend { get; set; }
        public string ApprovalNote { get; set; }

        public int? Status { get; set; }

        public string ReportContents { get; set; }
        #endregion
        #region Rule
        public string Name
        {
            get
            {
                return "Lần " + Orders + " năm " + Year;
            }
        }
        public string StatusText
        {
            get
            {

                switch (Status)
                {
                    case (int)iDAS.Service.Common.Resource.PlanAuditStatus.New:
                        return iDAS.Service.Common.Resource.PlanAuditStatusText.New;
                    case (int)iDAS.Service.Common.Resource.PlanAuditStatus.Pending:
                        return iDAS.Service.Common.Resource.PlanAuditStatusText.Pending;
                    case (int)iDAS.Service.Common.Resource.PlanAuditStatus.Approval:
                        return iDAS.Service.Common.Resource.PlanAuditStatusText.Approval;
                    case (int)iDAS.Service.Common.Resource.PlanAuditStatus.NotApprove:
                        return iDAS.Service.Common.Resource.PlanAuditStatusText.NotApprove;
                    case (int)iDAS.Service.Common.Resource.PlanAuditStatus.Auditing:
                        return iDAS.Service.Common.Resource.PlanAuditStatusText.Auditing;
                    case (int)iDAS.Service.Common.Resource.PlanAuditStatus.Finished:
                        return iDAS.Service.Common.Resource.PlanAuditStatusText.Finished;
                    case (int)iDAS.Service.Common.Resource.PlanAuditStatus.Destroy:
                        return iDAS.Service.Common.Resource.PlanAuditStatusText.Destroy;
                    default:
                        return string.Empty;
                }

            }
        }
        public string TimeText
        {
            get
            {
                var result = string.Empty;
                if (StartAt.HasValue && EndAt.HasValue)
                {
                    result = StartAt.Value.Date == EndAt.Value.Date
                        ? (StartAt.Value.Date == DateTime.Now.Date ? string.Format("Hôm nay<br/>{0} - {1}", StartAt.Value.ToString("HH:mm"), EndAt.Value.ToString("HH:mm"))
                        : string.Format("{0}<br/>{1} - {2}", StartAt.Value.ToString("dd/MM/yyyy"), StartAt.Value.ToString("HH:mm"), EndAt.Value.ToString("HH:mm")))
                        : string.Format("{0}<br/>{1}", StartAt.Value.ToString("dd/MM/yyyy HH:mm"), EndAt.Value.ToString("dd/MM/yyyy HH:mm"));
                }
                return result;
            }
        }
        public EmployeeBO EmployeeApproval { get; set; }
        public bool IsAllowApproval { get; set; }
        public string GroupAudit { get; set; }
        public string TimeChange
        {
            get
            {
                var result = string.Empty;
                if (StartPlanAt.HasValue && StartAt.HasValue)
                {
                    var dateDiff = StartAt.Value.Date.Subtract(StartPlanAt.Value.Date);
                    result = dateDiff.TotalDays == 0
                        ? "<span style=\'color:blue\'>Đúng định kỳ</span>"
                        : (dateDiff.TotalDays < 0 ? "<span style=\'color:green\'>Trước " + -dateDiff.TotalDays + " ngày</span>" : "<span style=\'color:red\'>Trễ " + dateDiff.TotalDays + " ngày</span>");
                }
                return result;
            }
        }
        public bool IsLeader { get; set; }
        #endregion
    }
}
