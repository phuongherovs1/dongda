﻿using System;

namespace iDAS.Service
{
    public class QualityAuditNotebookBO : BaseBO
    {
        #region Base
        public Guid? QualityAuditProgramId { get; set; }

        public Guid? DepartmentId { get; set; }

        public DateTime? AuditAt { get; set; }

        public Guid? AuditBy { get; set; }

        public string Contents { get; set; }
        #endregion
        public string DepartmentName { get; set; }
        public bool IsAuditer { get; set; }
    }
}
