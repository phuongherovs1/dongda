using System;

namespace iDAS.Service
{
    public class QualityMeetingRecordMemberBO : BaseBO
    {
        #region Basic
        public Guid? QualityMeetingRecordId { get; set; }

        public Guid? QualityMeetingPlanDetailId { get; set; }

        public Guid? EmployeeId { get; set; }

        public Guid? DepartmentTitleId { get; set; }

        public bool? IsNotAvailable { get; set; }
        #endregion

        #region Rule
        public EmployeeBO Employee { get; set; }
        #endregion

    }
}
