﻿using System;

namespace iDAS.Service
{
    public class QualityAuditProgramDepartmentBO : BaseBO
    {
        #region Base
        public Guid? QualityAuditProgramId { get; set; }

        public Guid? DepartmentId { get; set; }
        #endregion
    }
}
