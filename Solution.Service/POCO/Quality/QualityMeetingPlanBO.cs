﻿using System;

namespace iDAS.Service
{
    public class QualityMeetingPlanBO : BaseBO
    {

        #region Base
        public string Name { get; set; }
        public bool? IsRoutine { get; set; }
        public int? UnitTime { get; set; }
        public int? MeetingValue { get; set; }
        public int? LastTime { get; set; }
        public Guid? ApproveBy { get; set; }
        #endregion

        #region Rule



        public bool IsExpander { get; set; }

        public Guid MeetingId { get; set; }
        public string MeetingTimeText { get; set; }
        public string MeetingIssueText { get; set; }
        public string MeetingLocation { get; set; }
        /// <summary>
        /// Thành phần tham gia
        /// </summary>
        public string MeetingMemberJoin { get; set; }
        public int MeetingMemmberJoinCount { get; set; }
        /// <summary>
        /// Phòng ban chủ trì cuộc họp
        /// </summary>
        public string MeetingDepartmentName { get; set; }

        /// <summary>
        /// Nội dung cuộc họp
        /// </summary>
        public string SubjectText { get; set; }
        public int SubjectCount { get; set; }
        public int Status { get; set; }
        public string StatusText { get; set; }
        public string StatusColor { get; set; }

        public QualityMeetingPlanDetailBO Detail { get; set; }
        #endregion
    }
}
