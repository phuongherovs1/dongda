using System;

namespace iDAS.Service
{
    public class QualityMeetingPlanReportIssueBO : BaseBO
    {
        public Guid? QualityIssueId { get; set; }

        public Guid? QualityMeetingPlanReportId { get; set; }

        public string IssueName { get; set; }
        public string CountNCText { get; set; }
    }
}
