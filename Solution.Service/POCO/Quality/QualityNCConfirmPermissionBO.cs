﻿using System;

namespace iDAS.Service
{
    public class QualityNCConfirmPermissionBO : BaseBO
    {
        public Guid? DepartmentId { get; set; }
        public Guid? EmployeeId { get; set; }

        public Guid? DepartmentTitleId { get; set; }
        public string Description { get; set; }
        public string DepartmentName { get; set; }

        public bool IsEmployee { get { return EmployeeId.HasValue; } }
        public TitleBO DepartmentTitle { get; set; }
        public EmployeeBO Employee { get; set; }
        public string ObjectName { get; set; }
        public string ObjectID
        {
            get
            {
                if (EmployeeId.HasValue && EmployeeId != Guid.Empty)
                    return "Employee_" + EmployeeId.Value;
                else if (DepartmentTitleId.HasValue && DepartmentTitleId != Guid.Empty)
                    return "Title_" + DepartmentTitleId.Value;
                else
                    return string.Empty;
            }
        }
    }
}
