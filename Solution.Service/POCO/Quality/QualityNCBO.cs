﻿using System;

namespace iDAS.Service
{
    public class QualityNCBO : BaseBO
    {
        #region Base

        public Guid? QualityTargetId { get; set; }
        public Guid? QualityAuditPlanId { get; set; }
        public string Name { get; set; }
        public string Contents { get; set; }
        public string OldIds { get; set; }
        public bool? IsOld { get; set; }
        public DateTime? DiscoveredAt { get; set; }

        public int? Type { get; set; }

        public string Reason { get; set; }

        public string Solution { get; set; }

        public int? Status { get; set; }
        public string Note { get; set; }
        public Guid? ConfirmBy { get; set; }

        public DateTime? ConfirmAt { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? DiscovereBy { get; set; }
        public bool? IsAccept { get; set; }
        #endregion
        #region Rule
        public int CountReferences { get; set; }
        public int CountRelation { get; set; }
        public string MemberRelation { get; set; }
        public string DepartmentName { get; set; }
        public EmployeeBO EmployeeConfirmBy { get; set; }
        public EmployeeBO EmployeeDiscovereBy { get; set; }
        public EmployeeBO EmployeeApprovalBy { get; set; }
        public EmployeeBO AssignBy { get; set; }
        public string StatusText
        {
            get
            {
                var result = string.Empty;
                switch (Status)
                {
                    case (int)Common.Resource.QualityNCStatus.WaitConfirm:
                        result = Common.Resource.QualityNCStatusText.WaitConfirm;
                        break;
                    case (int)Common.Resource.QualityNCStatus.Confirm:
                        result = Common.Resource.QualityNCStatusText.Confirm;
                        break;
                    case (int)Common.Resource.QualityNCStatus.NotConfirm:
                        result = Common.Resource.QualityNCStatusText.NotConfirm;
                        break;
                    case (int)Common.Resource.QualityNCStatus.WaitApprove:
                        result = Common.Resource.QualityNCStatusText.WaitApprove;
                        break;
                    case (int)Common.Resource.QualityNCStatus.Approve:
                        result = Common.Resource.QualityNCStatusText.Approve;
                        break;
                    case (int)Common.Resource.QualityNCStatus.NotApprove:
                        result = Common.Resource.QualityNCStatusText.NotApprove;
                        break;
                    case (int)Common.Resource.QualityNCStatus.Pass:
                        result = Common.Resource.QualityNCStatusText.Pass;
                        break;
                    case (int)Common.Resource.QualityNCStatus.NotPass:
                        result = Common.Resource.QualityNCStatusText.NotPass;
                        break;
                    case (int)Common.Resource.QualityNCStatus.Destroy:
                        result = Common.Resource.QualityNCStatusText.Destroy;
                        break;
                    default:
                        break;
                }
                return result;
            }
        }
        public string QualityRelation { get { return Name + " [" + StatusText + "] "; } }
        public string TypeText
        {
            get
            {
                var result = string.Empty;
                switch (Type)
                {
                    case (int)Common.Resource.QualityNCTypeStatus.m:
                        result = Common.Resource.QualityNCTypeStatusText.m;
                        break;
                    case (int)Common.Resource.QualityNCTypeStatus.M:
                        result = Common.Resource.QualityNCTypeStatusText.M;
                        break;
                    case (int)Common.Resource.QualityNCTypeStatus.obs:
                        result = Common.Resource.QualityNCTypeStatusText.obs;
                        break;
                    default:
                        break;
                }
                return result;
            }
        }

        public Guid? QualityAuditNotebookId { get; set; }
        public bool IsSelected
        {
            get
            {
                return (Status != (int)Common.Resource.QualityNCStatus.WaitConfirm && Status != (int)Common.Resource.QualityNCStatus.NotConfirm);
            }
        }

        public int TotalMax { get; set; }
        public int Totalmin { get; set; }
        public int TotalObs { get; set; }
        #endregion
    }
}

