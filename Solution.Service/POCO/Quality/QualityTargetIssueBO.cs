﻿using System;

namespace iDAS.Service
{
    public class QualityTargetIssueBO : BaseBO
    {
        #region Base
        public Guid? QualityTargetCategoryId { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }
        #endregion

        #region Rule

        #endregion

    }
}
