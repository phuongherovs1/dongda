﻿namespace iDAS.Service
{
    public class QualityAuditPlanBO : BaseBO
    {
        #region Base
        public string Name { get; set; }

        public bool? IsRoutine { get; set; }

        public int? TimeValue { get; set; }

        public int? UnitTime { get; set; }
        #endregion
        public string UnitText
        {
            get
            {
                if (UnitTime.HasValue)
                {
                    switch ((int)UnitTime)
                    {
                        case (int)iDAS.Service.Common.Resource.UnitTime.Day:
                            return iDAS.Service.Common.Resource.UnitTimeText.Day;
                        case (int)iDAS.Service.Common.Resource.UnitTime.Week:
                            return iDAS.Service.Common.Resource.UnitTimeText.Week;
                        case (int)iDAS.Service.Common.Resource.UnitTime.Month:
                            return iDAS.Service.Common.Resource.UnitTimeText.Month;
                        case (int)iDAS.Service.Common.Resource.UnitTime.Year:
                            return iDAS.Service.Common.Resource.UnitTimeText.Year;
                        default:
                            return string.Empty;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public string ISO { get; set; }
        public string NameExtend
        {
            get
            {
                return Name + " (" + ISO + ") " + (IsRoutine == true ? " - Định kỳ " + TimeValue + " " + UnitText : " - Không định kỳ");
            }
        }

    }
}
