﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class QualityTargetResponsibilityBO : BaseBO
    {
        #region Base
        public Guid? EmployeeId { get; set; }

        public Guid? QualityTargetId { get; set; }

        public int? RoleType { get; set; }

        public int? Status { get; set; }
        #endregion

        #region Rule

        public string EmployeeName { get; set; }

        public string DepartmentName { get; set; }

        public string RoleName
        {
            get
            {
                var roleName = string.Empty;
                switch (RoleType)
                {
                    case (int)Resource.TargetResponsibility.Create:
                        roleName = Resource.TargetResponsibilityText.Create;
                        break;
                    case (int)Resource.TargetResponsibility.Approval:
                        roleName = Resource.TargetResponsibilityText.Approval;
                        break;
                    case (int)Resource.TargetResponsibility.Control:
                        roleName = Resource.TargetResponsibilityText.Control;
                        break;
                }
                return roleName;
            }
        }

        public string StatusText { get; set; }
        #endregion

    }
}
