﻿namespace iDAS.Service
{
    public class ISOStandardBO : BaseBO
    {
        #region Base
        public string Name { get; set; }
        public string Note { get; set; }

        #endregion
        public bool IsSelected { get; set; }
    }
}
