using System;

namespace iDAS.Service
{
    public class QualityMeetingPlanNCBO : BaseBO
    {

        public Guid? QualityMeetingPlanReportIssueId { get; set; }

        public Guid? QualityNCId { get; set; }

        public Guid? QualityIssueId { get; set; }

        public QualityNCBO QualityNC { get; set; }

    }
}
