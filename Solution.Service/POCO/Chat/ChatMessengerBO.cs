﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace iDAS.Service
{
    public class ChatMessengerBO : BaseBO
    {
        public Guid? To { get; set; }

        [Column(TypeName = "ntext")]
        public string Content { get; set; }

        public bool? IsFromDelete { get; set; }

        public bool? IsToDelete { get; set; }

        public bool? IsRead { get; set; }

        public DateTime? ReadAt { get; set; }

        public bool IsCurrentUser
        {
            get { return CreateBy == UserId; }
        }
        public EmployeeBO Creater { get; set; }
        public string TimeSend
        {
            get
            {
                if (CreateAt.HasValue)
                {
                    var dateDiff = DateTime.Now.Subtract(CreateAt.Value);
                    if ((int)dateDiff.TotalDays < 1 && (int)dateDiff.TotalHours < 1 && (int)dateDiff.TotalMinutes < 1)
                        return string.Format("{0} giây trước", (int)dateDiff.TotalSeconds);
                    else if ((int)dateDiff.TotalDays < 1 && (int)dateDiff.TotalHours < 1 && (int)dateDiff.TotalMinutes > 1)
                        return string.Format("{0} phút trước", (int)dateDiff.TotalMinutes);
                    else if ((int)dateDiff.TotalDays < 1 && (int)dateDiff.TotalHours > 1)
                        return string.Format("{0} giờ trước", (int)dateDiff.TotalHours);
                    else
                        return string.Format("{0} lúc {1}", CreateAt.Value.ToString("dd/MM/yyyy"), CreateAt.Value.Hour + ":" + CreateAt.Value.Minute);
                }
                else
                    return string.Empty;
            }
        }
        public bool ShowAvartar { get; set; }
    }
}
