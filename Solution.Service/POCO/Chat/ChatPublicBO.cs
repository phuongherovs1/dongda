using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace iDAS.Service
{
    public class ChatPublicBO : BaseBO
    {
        public Guid? From { get; set; }

        [Column(TypeName = "ntext")]
        public string Content { get; set; }

        public string FromName { get; set; }
        public string FromAvatarUrl { get; set; }

        public bool IsCurrentUser { get { return CreateBy == UserId; } }

        public string CreateAtText { get { return CreateAt.HasValue ? CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : string.Empty; } }
    }
}
