﻿using System;

namespace iDAS.Service
{
    public class ChatItemBO
    {
        public string Id { get; set; }
        /// <summary>
        /// Id người gửi
        /// </summary>
        public string From { get; set; }
        /// <summary>
        /// Id người nhận
        /// </summary>
        public string To { get; set; }
        /// <summary>
        /// Nội dung
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// Tài khoản người gửi
        /// </summary>
        public string FromName { get; set; }
        /// <summary>
        /// Tài khoản người nhận
        /// </summary>
        public string ToName { get; set; }
        /// <summary>
        /// Tên người gửi
        /// </summary>
        public string FromFullName { get; set; }
        /// <summary>
        /// Tên người nhận
        /// </summary>
        public string ToFullName { get; set; }
        /// <summary>
        /// Ảnh người gửi
        /// </summary>
        public string FromAvatarUrl { get; set; }
        /// <summary>
        /// Ảnh người nhận
        /// </summary>
        public string ToAvatarUrl { get; set; }
        /// <summary>
        /// Người nhận đọc 
        /// </summary>
        public bool IsRead { get; set; }
        /// <summary>
        /// Thời gian người nhận đọc
        /// </summary>
        public DateTime? ReadAt { get; set; }
        public string ReadAtText { get { return ReadAt.HasValue ? ReadAt.Value.ToString("dd-MM-yyyy HH:mm") : string.Empty; } }
        public string CreateBy { get; set; }
        public DateTime? CreateAt { get; set; }

        public string CreateAtText { get { return CreateAt.HasValue ? CreateAt.Value.ToString("dd-MM-yyyy HH:mm") : string.Empty; } }
    }
}
