using System;
namespace iDAS.Service
{
    public class ChatGroupMemberBO : BaseBO
    {
        public Guid? GroupId { get; set; }
        public Guid? MemberId { get; set; }

    }
}
