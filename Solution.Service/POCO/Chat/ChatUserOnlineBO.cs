using System;
namespace iDAS.Service
{
    public class ChatUserOnlineBO : BaseBO
    {
        public Guid? UserOnlineId { get; set; }
        public string UserName { get; set; }
    }
}
