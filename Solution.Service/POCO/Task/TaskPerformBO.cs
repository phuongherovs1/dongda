﻿using iDAS.Service.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public partial class TaskPerformBO : BaseBO
    {
        #region Basic
        public string Content { get; set; }
        public Nullable<long> Duration { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public Nullable<System.DateTime> StartAt { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public Nullable<System.DateTime> EndAt { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        [Range(0, 100, ErrorMessage = "Bạn cần nhập giá trị trong khoảng từ 0 đến 100")]
        public Nullable<double> Unit { get; set; }
        private Nullable<double> completeRate;

        //Điểm theo chất lượng
        public int? Score_Value { get; set; }
        //Điểm theo thời gian
        public int? Score_Time { get; set; }
        //Điểm cộng
        public int? Score_Plus { get; set; }
        //Điểm trừ
        public int? Score_Minus { get; set; }
        //Điểm thái độ
        public int? Score_Attitude { get; set; }

        public int? Attitude1 { get; set; }
        public int? Attitude2 { get; set; }
        public int? Attitude3 { get; set; }
        public int? Attitude4 { get; set; }
        public int? AttitudePlus { get; set; }
        public int? AttitudeMinus { get; set; }


        public int? Manager { get; set; }
        public int? ManagerPlus { get; set; }
        public int? ManagerMinus { get; set; }


        public Nullable<double> CompleteRate
        {
            get { return completeRate; }
            set { completeRate = value ?? 0; }
        }
        public Nullable<bool> IsAssign { get; set; }
        public Nullable<bool> IsPause { get; set; }
        public Nullable<bool> IsCancel { get; set; }
        public Nullable<bool> IsFinish { get; set; }
        private string reasonOfPause;
        public string ReasonOfPause
        {
            get
            {
                return reasonOfPause;
            }
            set
            {
                reasonOfPause = value ?? string.Empty;
            }
        }
        private string reasonOfCancel;
        public string ReasonOfCancel
        {
            get
            {
                return reasonOfCancel;
            }
            set
            {
                reasonOfCancel = value ?? string.Empty;
            }
        }
        public Nullable<System.DateTime> AssignAt { get; set; }
        public Nullable<System.DateTime> PerformAt { get; set; }
        public Nullable<System.DateTime> PauseAt { get; set; }
        public Nullable<System.DateTime> CancelAt { get; set; }
        public Nullable<System.DateTime> CompleteAt { get; set; }
        public Nullable<System.DateTime> ReportAt { get; set; }
        public Nullable<System.DateTime> FinishAt { get; set; }
        public Nullable<System.Guid> FinishBy { get; set; }
        private string auditNote;
        public string AuditNote
        {
            get
            {
                return auditNote;
            }
            set
            {
                auditNote = value ?? string.Empty;
            }
        }
        #endregion

        #region rule
        public bool IsPerform
        {
            get
            {
                return PerformAt != null && CompleteRate != 100 && IsFinish != true && IsPause != true && IsCancel != true;
            }
        }
        public bool IsComplete
        {
            get
            {
                return CompleteRate >= 100 && IsCancel != true;
            }
        }
        public bool IsOutOfDate
        {
            get
            {
                return (!IsComplete && DateTime.Now > EndAt) || (IsComplete && CompleteAt > EndAt);
            }
        }
        public bool IsBehindSchedule
        {
            get
            {
                return CompletePlanAt > EndAt;
            }
        }
        public Resource.Task.Status TaskStatus { get; set; }
        public Resource.Task.Status TaskPerformStatus
        {
            get
            {
                var status = TaskStatus == Resource.Task.Status.Cancel ? TaskStatus :
                             TaskStatus == Resource.Task.Status.Pause ? TaskStatus :
                             IsFinish == true ? Resource.Task.Status.Finish :
                             IsCancel == true ? Resource.Task.Status.Cancel :
                             IsComplete == true ? Resource.Task.Status.Complete :
                             IsPause == true ? Resource.Task.Status.Pause :
                             IsPerform == true ? Resource.Task.Status.Perform :
                             IsAssign == true ? Resource.Task.Status.WaitPerform :
                             Resource.Task.Status.WaitAssign;
                return status;
            }
        }
        private EmployeeBO employee;
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public EmployeeBO Employee
        {
            get
            {
                return employee = employee ?? new EmployeeBO();
            }
            set
            {
                employee = value;
            }
        }
        public Guid EmployeeId
        {
            get
            {
                return Employee.Id;
            }
        }
        public string EmployeeName
        {
            get
            {
                return Employee.Name;
            }
        }
        public string EmployeeAvatarUrl
        {
            get
            {
                return Employee.AvatarUrl;
            }
        }
        public string Report { get; set; }
        public FileUploadBO FileAttachs { get; set; }
        public double UnitComplete
        {
            get
            {
                var unit = Unit ?? 0;
                var rate = CompleteRate ?? 0;
                return (unit * rate) / 100;
            }
        }
        public double CompleteRatePlan
        {
            get
            {
                double rate = DateTime.Now >= EndAt ? 100 : 0;
                if (DateTime.Now > StartAt && rate < 100)
                {
                    var deltaTime = EndAt - StartAt ?? new TimeSpan();
                    var deltaTime1 = DateTime.Now - StartAt ?? new TimeSpan();
                    rate = Math.Round(deltaTime1.TotalMinutes / deltaTime.TotalMinutes * 100, 2);
                }
                return rate;
            }
        }
        private DateTime? completePlanAt;
        public DateTime? CompletePlanAt
        {
            get
            {
                if (!CompleteRate.HasValue || CompleteRate == 0) return null;
                if (!completePlanAt.HasValue)
                {
                    var deltaTime = (DateTime.Now - PerformAt) ?? new TimeSpan();
                    var minus = (100 - CompleteRate) / (CompleteRate / deltaTime.TotalMinutes);
                    completePlanAt = DateTime.Now + new TimeSpan(0, 0, (int)minus, 0);
                }
                return completePlanAt;
            }
        }
        public Guid EmployeeCreateId { get; set; }
        public Guid EmployeeAssignId { get; set; }
        public Guid EmployeeReviewId { get; set; }
        public Guid? TaskId { get; set; }
        public string TaskContent { get; set; }
        public Guid? CalendarId { get; set; }
        public TimeSpan DurationTime
        {
            get
            {
                return TimeSpan.FromTicks(Duration ?? 0);
            }
            set
            {
                Duration = value.Ticks;
            }
        }
        public override bool AllowDelete
        {
            get
            {
                return IsAssign != true && base.AllowDelete;
            }
            set
            {
                base.AllowDelete = value;
            }
        }
        public override bool AllowUpdate
        {
            get
            {
                var check1 = TaskStatus != Resource.Task.Status.Cancel && TaskStatus != Resource.Task.Status.Pause && TaskStatus != Resource.Task.Status.Finish;
                var check2 = IsCancel != true && IsPause != true && IsFinish != true;
                var check3 = EmployeeCreateId == UserId || EmployeeAssignId == UserId || EmployeeReviewId == UserId;
                return check1 && check2 && check3 && !base.AllowCreate;
            }
            set
            {
                base.AllowUpdate = value;
            }
        }
        public bool ViewCancel
        {
            get
            {
                return (IsAssign == true
                    && IsFinish != true
                    && AllowCreate != true)
                    || IsCancel == true;
            }
        }
        public bool ViewPause
        {
            get
            {
                return (IsAssign == true
                    && IsFinish != true
                    && IsCancel != true
                    && IsComplete != true
                    && PerformAt != null
                    && AllowCreate != true)
                    || IsPause == true;
            }
        }
        public bool ViewCheck
        {
            get
            {
                var check1 = EmployeeCreateId == UserId || EmployeeAssignId == UserId || EmployeeReviewId == UserId || EmployeeId == UserId;
                var check2 = IsAssign == true;
                return check1 && check2;
            }
        }
        public bool HideFinish
        {
            get
            {
                return IsAssign != true || Operator == Resource.Operation.Create;
            }
        }
        public bool AllowCancel
        {
            get
            {
                var check1 = EmployeeCreateId == UserId || EmployeeAssignId == UserId || EmployeeReviewId == UserId;
                var check2 = TaskStatus != Resource.Task.Status.Cancel;
                return ViewCancel && check1 && check2;
            }
        }
        public bool AllowPause
        {
            get
            {
                var check1 = EmployeeCreateId == UserId || EmployeeAssignId == UserId || EmployeeReviewId == UserId;
                var check2 = TaskStatus != Resource.Task.Status.Cancel && TaskStatus != Resource.Task.Status.Pause;
                return ViewPause && check1 && check2;
            }
        }
        public bool AllowFinish
        {
            get
            {
                if (Operator == Resource.Operation.Read)
                    return false;
                return IsFinish == true || IsComplete;
            }
        }
        public bool AllowReport
        {
            get
            {
                return IsFinish != true && IsCancel != true && IsPause != true;
            }
        }
        public bool AllowCheck
        {
            get
            {
                var check1 = EmployeeCreateId == UserId || EmployeeAssignId == UserId || EmployeeReviewId == UserId;
                var check2 = TaskStatus != Resource.Task.Status.Cancel && IsCancel != true;
                return ViewCheck && check1 && check2;
            }
        }
        #endregion

        #region view
        public string OutOfDateText
        {
            get
            {
                var text = "Chưa quá hạn";
                if (IsOutOfDate)
                {
                    var time = IsComplete ? CompleteAt - EndAt : DateTime.Now - EndAt;
                    text = "Quá hạn: {0} ngày {1} giờ";
                    text = string.Format(text, time.Value.Days, time.Value.Hours);
                }
                return text;
            }
        }
        public string BehindScheduleText
        {
            get
            {
                var text = CompletePlanAt.HasValue ? "Không chậm tiến độ" : "Chưa thực hiện";
                if (IsBehindSchedule)
                {
                    if (!CompletePlanAt.HasValue) return null;
                    var time = CompletePlanAt - EndAt;
                    text = "Chậm tiến độ: {0} ngày {1} giờ";
                    text = string.Format(text, time.Value.Days, time.Value.Hours);
                }
                return text;
            }
        }
        public string TimePerformText
        {
            get
            {
                return "";
            }
        }
        public string CompleteRateText
        {
            get
            {
                return (CompleteRate.HasValue ? CompleteRate.Value.ToString() : "0") + "%";
            }
        }
        public string TaskStatusText
        {
            get
            {
                return Common.Resource.Task.GetStatusText(TaskPerformStatus);
            }
        }
        public string TaskStatusColor
        {
            get
            {
                return Common.Resource.Task.GetStatusColor(TaskPerformStatus);
            }
        }
        public string TaskName { get; set; }
        public string UnitText
        {
            get
            {
                var text = "Giao {0}% - Thực hiện {1}%";
                return string.Format(text, Unit, UnitComplete);
            }
        }

        public double? CompleteRateTask { get; set; }
        public int Maxscore { get; set; }
        public int? Score { get; set; }
        public int? Weightscore { get; set; }
        public string Note { get; set; }
        #endregion
    }
}