using System;
using System.Linq;

namespace iDAS.Service
{
    public partial class TaskHistoryBO : BaseBO
    {
        public Guid? TaskId { get; set; }
        public byte[] Data { get; set; }
        public DateTime? Date { get; set; }
    }
    public partial class HistoryTaskBO : BaseBO
    {
        public byte[] Avatar { get; set; }
        public Guid? TaskId { get; set; }
        public Guid? EmployeeId { get; set; }
        public string Content { get; set; }
        public string Percent { get; set; }
        public string Name { get; set; }
        public string AvatarUrl
        {
            get
            {
                if (Avatar != null && Avatar.Count() > 0)
                    return string.Format("data:image;base64,{0}", Convert.ToBase64String(Avatar));
                else
                    return @"/Content/Images/underfind.jpg";
            }
        }
        private FileBO fileAvatar;
        public FileBO FileAvatar
        {
            get
            {
                return fileAvatar = fileAvatar ?? new FileBO();
            }
            set
            {
                fileAvatar = value;
            }
        }
        public DateTime? CreateDate { get; set; }
    }
}
