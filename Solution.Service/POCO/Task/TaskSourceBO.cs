﻿namespace iDAS.Service
{
    public class TaskSourceBO : BaseBO
    {
        #region Basic
        public string Name { get; set; }
        public string Content { get; set; }
        #endregion
    }
}
