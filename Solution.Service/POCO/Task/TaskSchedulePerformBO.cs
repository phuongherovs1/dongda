﻿using iDAS.Service.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public class TaskSchedulePerformBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> ScheduleId { get; set; }
        public Nullable<System.Guid> EmployeeId { get; set; }
        private bool? isSent;
        public Nullable<bool> IsSent
        {
            get
            {
                return isSent;
            }
            set
            {
                isSent = value;
                if (isSent == true)
                {
                    DisableDelete = true;
                }
            }
        }
        private bool? isAccept;
        public Nullable<bool> IsAccept
        {
            get
            {
                return isAccept;
            }
            set
            {
                isAccept = value;
                if (isAccept == true)
                {
                    IsEdit = false;
                    DisableUpdate = true;
                }
            }
        }
        private bool? isChange;
        public Nullable<bool> IsChange
        {
            get
            {
                return isChange;
            }
            set
            {
                isChange = value;
                if (isChange == true)
                {
                    IsEdit = false;
                    DisableUpdate = true;
                }
            }
        }
        private bool? isReject;
        public Nullable<bool> IsReject
        {
            get
            {
                return isReject;
            }
            set
            {
                isReject = value;
                if (isReject == true)
                {
                    IsEdit = false;
                    DisableUpdate = true;
                }
            }
        }
        public Nullable<bool> IsPause { get; set; }
        public Nullable<bool> IsCancel { get; set; }
        public Nullable<bool> IsEdit { get; set; }
        public string ReasonOfPause { get; set; }
        public string ReasonOfCancel { get; set; }
        public string Comment { get; set; }
        public string Feedback { get; set; }
        private DateTime? sentAt;
        public Nullable<System.DateTime> SentAt
        {
            get
            {
                return sentAt;
            }
            set
            {
                sentAt = value ?? DateTime.MinValue;
            }
        }
        private DateTime? confirmAt;
        public Nullable<System.DateTime> ConfirmAt
        {
            get
            {
                return confirmAt;
            }
            set
            {
                confirmAt = value ?? DateTime.MinValue;
            }
        }
        public Nullable<System.DateTime> PauseAt { get; set; }
        public Nullable<System.DateTime> CancelAt { get; set; }
        #endregion

        #region Rule
        public bool IsConfirm
        {
            get
            {
                return IsChange == true || IsCancel == true || IsAccept == true;
            }
        }
        public bool IsPerform
        {
            get
            {
                return IsAccept == true && IsCancel != true && IsPause != true && StartAt <= DateTime.Now && EndAt >= DateTime.Now;
            }
        }
        public bool IsFinish
        {
            get
            {
                return DateTime.Now > EndAt && IsCancel != true && IsPause != true && IsAccept == true;
            }
        }
        public Resource.Task.ScheduleStatus Status
        {
            get
            {
                var status = IsFinish == true ? Resource.Task.ScheduleStatus.Finish :
                    IsPause == true ? Resource.Task.ScheduleStatus.Pause :
                    IsCancel == true ? Resource.Task.ScheduleStatus.Cancel :
                    IsAccept == true ? Resource.Task.ScheduleStatus.Accept :
                    IsReject == true ? Resource.Task.ScheduleStatus.Reject :
                    IsChange == true ? Resource.Task.ScheduleStatus.Change :
                    IsSent == true ? Resource.Task.ScheduleStatus.WaitConfirm :
                    Resource.Task.ScheduleStatus.New;
                return status;
            }
        }
        public string Name { get; set; }
        public string Content { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }
        private EmployeeBO employee;
        [Required(ErrorMessage = "Bạn cần chọn nhân sự!")]
        public EmployeeBO Employee
        {
            get
            {
                return employee = employee ?? new EmployeeBO();
            }
            set
            {
                employee = value;
                EmployeeId = employee.Id;
            }
        }
        public string EmployeeName
        {
            get
            {
                return Employee.Name;
            }
        }
        public string EmployeeAvatarUrl
        {
            get
            {
                return Employee.AvatarUrl;
            }
        }
        public Guid EmployeeSetupId { get; set; }
        public bool IsRolePerform
        {
            get
            {
                return UserId == EmployeeId;
            }
        }
        public bool IsRoleSetup
        {
            get
            {
                return UserId == EmployeeSetupId;
            }
        }
        public bool AllowSend { get; set; }
        public bool AllowCancel
        {
            get
            {
                return IsSent == true && IsRoleSetup && !IsPerform && !IsFinish;
            }
        }
        public bool AllowPause
        {
            get
            {
                return IsSent == true && IsAccept == true && StartAt <= DateTime.Now && EndAt >= DateTime.Now;
            }
        }
        public bool AllowSave
        {
            get
            {
                return IsRolePerform || IsRoleSetup;
            }
        }
        public bool AllowConfirm
        {
            get
            {
                return IsRolePerform && IsCancel != true;
            }
        }
        public string Notify
        {
            get
            {
                var text = string.Empty;
                if (IsConfirm && IsEdit == true)
                    text = "Lịch công tác đã có sự điều chỉnh. ";
                if (IsCancel == true)
                    text = text + "Lịch công tác của bạn đã bị hủy. ";
                if (IsPause == true)
                    text = text + "Lịch công tác của bạn đã bị tạm dừng.";
                return text.Trim(' ', '.');
            }
        }
        #endregion

        #region View
        public string StatusText
        {
            get
            {
                return Resource.Task.GetScheduleStatusText(Status);
            }
        }
        public string StatusColor
        {
            get
            {
                return Resource.Task.GetScheduleStatusColor(Status);
            }
        }
        #endregion

        public void SetSchedule(TaskScheduleBO item)
        {
            this.Name = item.Name;
            this.Content = item.Content;
            if (item.StartAt.HasValue)
                this.StartAt = item.StartAt.Value;
            if (item.EndAt.HasValue)
                this.EndAt = item.EndAt.Value;
            if (item.EmployeeId.HasValue)
                this.EmployeeSetupId = item.EmployeeId.Value;
        }
    }
}
