﻿using System;

namespace iDAS.Service
{
    public class TaskFileBO : BaseBO
    {
        public Nullable<System.Guid> TaskId { get; set; }
        public Nullable<System.Guid> FileId { get; set; }
    }
}
