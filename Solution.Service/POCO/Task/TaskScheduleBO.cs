﻿using iDAS.Service.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public class TaskScheduleBO : BaseBO
    {
        #region Base
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public Nullable<System.Guid> EmployeeId { get; set; }
        private string name;
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        [RegularExpression(@"^[\s\S]*\S+[\s\S]*$", ErrorMessage = "Bạn cần nhập ký tự khác khoảng trắng!")]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = GetStringValue(value);
            }
        }
        private string content;
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        [RegularExpression(@"^[\s\S]*\S+[\s\S]*$", ErrorMessage = "Bạn cần nhập ký tự khác khoảng trắng!")]
        public string Content
        {
            get
            {
                return content;
            }
            set
            {
                content = GetStringValue(value);
            }
        }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public Nullable<System.DateTime> StartAt { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        [DataType(DataType.DateTime, ErrorMessage = "Không đúng kiểu thời gian")]
        public Nullable<System.DateTime> EndAt { get; set; }
        private bool? isSent;
        public Nullable<bool> IsSent
        {
            get
            {
                return isSent;
            }
            set
            {
                isSent = value;
                if (isSent == true)
                {
                    DisableDelete = true;
                }
            }
        }
        public bool? IsConfirm { get; set; }
        private bool? isClosed;
        public Nullable<bool> IsClosed
        {
            get
            {
                return isClosed;
            }
            set
            {
                isClosed = value;
                if (isClosed == true && (IsPerform || IsFinish))
                {
                    DisableUpdate = true;
                }
            }
        }
        private bool? isCancel;
        public Nullable<bool> IsCancel
        {
            get
            {
                return isCancel;
            }
            set
            {
                isCancel = value;
                if (isCancel == true)
                {
                    DisableUpdate = true;
                }
            }
        }
        private bool? isPause;
        public Nullable<bool> IsPause
        {
            get
            {
                return isPause;
            }
            set
            {
                isPause = value;
                if (isPause == true)
                {
                    DisableUpdate = true;
                }
            }
        }
        private string reasonOfPause;
        [RegularExpression(@"^[\s\S]*\S+[\s\S]*$", ErrorMessage = "Bạn cần nhập ký tự khác khoảng trắng!")]
        public string ReasonOfPause
        {
            get
            {
                return reasonOfPause;
            }
            set
            {
                reasonOfPause = GetStringValue(value);
            }
        }
        private string reasonOfCancel;
        [RegularExpression(@"^[\s\S]*\S+[\s\S]*$", ErrorMessage = "Bạn cần nhập ký tự khác khoảng trắng!")]
        public string ReasonOfCancel
        {
            get
            {
                return reasonOfCancel;
            }
            set
            {
                reasonOfCancel = GetStringValue(value);
            }
        }
        public Nullable<System.DateTime> ClosedAt { get; set; }
        public Nullable<System.DateTime> PauseAt { get; set; }
        public Nullable<System.DateTime> CancelAt { get; set; }
        #endregion

        #region Rule
        public bool IsOutOfDate
        {
            get
            {
                return IsClosed != true && IsCancel != true && IsPause != true && DateTime.Now > StartAt;
            }
        }
        public bool IsPerform
        {
            get
            {
                return IsClosed == true && IsCancel != true && IsPause != true && StartAt <= DateTime.Now && EndAt >= DateTime.Now;
            }
        }
        public bool IsFinish
        {
            get
            {
                return IsClosed == true && IsCancel != true && IsPause != true && DateTime.Now > EndAt;
            }
        }
        public Resource.Task.ScheduleStatus Status
        {
            get
            {
                var status = 
                    IsFinish == true ? Resource.Task.ScheduleStatus.Finish :
                    IsPause == true ? Resource.Task.ScheduleStatus.Pause :
                    IsPerform == true ? Resource.Task.ScheduleStatus.Perform :
                    IsCancel == true ? Resource.Task.ScheduleStatus.Cancel :
                    IsSent == true ? Resource.Task.ScheduleStatus.Perform :
                    Resource.Task.ScheduleStatus.New;
                return status;
            }
        }
        private EmployeeBO employee;
        [Required(ErrorMessage = "Bạn cần chọn nhân sự!")]
        public EmployeeBO Employee
        {
            get
            {
                return employee = employee ?? new EmployeeBO();
            }
            set
            {
                employee = value;
                EmployeeId = employee.Id;
            }
        }
        public string EmployeeName
        {
            get
            {
                return Employee.Name;
            }
        }
        public string EmployeeAvatarUrl
        {
            get
            {
                return Employee.AvatarUrl;
            }
        }
        public Guid SchedulePerformId { get; set; }
        public bool IsRolePerform
        {
            get
            {
                return SchedulePerformId != Guid.Empty && SchedulePerformId != null;
            }
        }
        public bool IsRoleSetup
        {
            get
            {
                return EmployeeId == UserId || CreateBy == UserId;
            }
        }
        public bool AllowConfirm
        {
            get
            {
                return IsSent == true && IsRolePerform && IsCancel != true && !IsOutOfDate && IsConfirm != true;
            }
        }
        public bool AllowCancel
        {
            get
            {
                return IsRoleSetup && IsSent == true && !IsPerform && !IsFinish;
            }
        }
        public bool AllowPause
        {
            get
            {
                return IsRoleSetup && IsClosed == true && StartAt <= DateTime.Now && EndAt >= DateTime.Now;
            }
        }
        public bool AllowCreatePerform
        {
            get
            {
                return IsRoleSetup && IsCancel != true;
            }
        }
        public bool AllowSendToPerform
        {
            get
            {
                return !IsOutOfDate && IsCancel != true && IsPause != true;
            }
        }
        private string notifyToPerform;
        public string NotifyToPerform
        {
            get
            {
                return notifyToPerform ?? string.Empty;
            }
            set
            {
                notifyToPerform = value;
            }
        }
        #endregion

        #region View
        public string StatusText
        {
            get
            {
                return Resource.Task.GetScheduleStatusText(Status);
            }
        }
        public string StatusColor
        {
            get
            {
                return Resource.Task.GetScheduleStatusColor(Status);
            }
        }
        #endregion
    }
}
