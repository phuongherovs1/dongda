﻿using System;

namespace iDAS.Service
{
    public class TaskCategoryBO : BaseBO
    {
        #region Basic
        public string Name { get; set; }
        public Guid? DepartmentId { get; set; }
        public Guid? KPICatagoryId { get; set; }
        public string Content { get; set; }
        #endregion
        #region View
        public string DepartmentName
        {
            get; set;
        }
        public bool HasDelete { get; set; }
        #endregion
    }
}
