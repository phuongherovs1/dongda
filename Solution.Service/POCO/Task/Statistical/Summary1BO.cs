﻿using System;

namespace iDAS.Service
{
    public class Summary1BO
    {
        //"UnPerForm", "PerForm1", "PerForm2", "Finish1", "Finish2", "Cancel"
        public DateTime MonthSummary { get; set; }
        public string MonthSummaryText
        {
            get
            {
                return MonthSummary.ToString("MM/yyyy");
            }
        }

        public int UnPerForm { get; set; } // Chờ thực hiện

        public int PerForm1 { get; set; } //Đang thực hiện (trong hạn)

        public int PerForm2 { get; set; } // Đang thực hiện (quá hạn)

        public int Finish1 { get; set; } // Kết thúc (trong hạn)

        public int Finish2 { get; set; } // Kết thúc (quá hạn)

        public int Cancel { get; set; } // Hủy

        public int Total { get { return UnPerForm + PerForm1 + PerForm2 + Finish1 + Finish2 + Cancel; } }

        public string UnPerFormPercent
        {
            get
            {
                return Total == 0 ? string.Empty : ((float)UnPerForm / Total * 100).ToString("0.00");
            }
        }

        public string PerForm1Percent
        {
            get
            {
                return Total == 0 ? string.Empty : ((float)PerForm1 / Total * 100).ToString("0.00");
            }
        }

        public string PerForm2Percent
        {
            get
            {
                return Total == 0 ? string.Empty : ((float)PerForm2 / Total * 100).ToString("0.00");
            }
        }
        public string Finish1Percent
        {
            get
            {
                return Total == 0 ? string.Empty : ((float)Finish1 / Total * 100).ToString("0.00");
            }
        }
        public string Finish2Percent
        {
            get
            {
                return Total == 0 ? string.Empty : ((float)Finish2 / Total * 100).ToString("0.00");
            }
        }
        public string CancelPercent
        {
            get
            {
                return Total == 0 ? string.Empty : ((float)Cancel / Total * 100).ToString("0.00");
            }
        }
    }
}
