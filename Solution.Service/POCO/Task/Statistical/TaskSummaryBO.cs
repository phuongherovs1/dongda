﻿using System;

namespace iDAS.Service
{
    public class TaskSummaryBO
    {
        public Common.Resource.Task.Role Role { get; set; }
        public DateTime MonthSummary { get; set; }

        public string MonthSummaryText
        {
            get
            {
                return MonthSummary.ToString("MM/yyyy");
            }
        }
        //chưa hoàn thành
        public int SummaryNotComplete { get; set; }
        //hoàn thành
        public int SummaryComplete { get; set; }
        //quá hạn
        public int SummaryOutOfDate { get; set; }
        //kết thúc
        public int SummaryFinish { get; set; }
        //khác
        public int SummaryOther { get; set; }
        //chờ chưa kết thúc
        public int SummaryOnAction { get; set; }
        //chờ thực hiện
        public int SummaryRole { get; set; }
        //Đang thực hiện
        public int SummaryRoling { get; set; }
        //Đang tạm dừng
        public int SummaryCancel { get; set; }
        //Đang hủy
        public int SummaryDelete { get; set; }
        //Đang mới
        public int SummaryNew { get; set; }
    }
}
