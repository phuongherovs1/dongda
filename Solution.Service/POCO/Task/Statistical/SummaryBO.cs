﻿using System;

namespace iDAS.Service
{
    public class SummaryBO
    {
        public DateTime MonthSummary { get; set; }
        public int WaitPerform { get; set; }
        public int Pause { get; set; }
        public int OutOfDate { get; set; }
        public int Perform { get; set; }
        public int Cancel { get; set; }
        public int Complete { get; set; }
        public int Finish { get; set; }
        public int Unfinish { get; set; }
        public int Total { get; set; }

        public string OutOfDatePercent
        {
            get
            {
                return Total == 0 ? string.Empty : ((float)OutOfDate / Total * 100).ToString("0.00");
            }
        }

        public string FinishPercent
        {
            get
            {
                return Total == 0 ? string.Empty : ((float)Finish / Total * 100).ToString("0.00");
            }
        }

        public string UnfinishPercent
        {
            get
            {
                return Total == 0 ? string.Empty : ((float)Unfinish / Total * 100).ToString("0.00");
            }
        }

        public string MonthSummaryText
        {
            get
            {
                return MonthSummary.ToString("MM/yyyy");
            }
        }
    }
}
