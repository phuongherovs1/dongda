﻿using System;

namespace iDAS.Service
{
    public class CommonSummaryBO
    {
        public DateTime? StartAt { get; set; }
        public DateTime? EndAt { get; set; }
        public double? CompleteRate { get; set; }
        public bool? IsAssign { get; set; }
        public bool? IsPause { get; set; }
        public bool? IsCancel { get; set; }
        public bool? IsFinish { get; set; }
        public DateTime? AssignAt { get; set; }
        public DateTime? PerformAt { get; set; }
        public DateTime? PauseAt { get; set; }
        public DateTime? CancelAt { get; set; }
        public DateTime? CompleteAt { get; set; }
        public DateTime? ReportAt { get; set; }
        public DateTime? FinishAt { get; set; }
        public Guid? FinishBy { get; set; }
        public bool? IsDelete { get; set; }
        public DateTime? CreateAt { get; set; }
        public Guid? CreateBy { get; set; }
        public DateTime? UpdateAt { get; set; }
        public Guid? UpdateBy { get; set; }
    }
}
