using iDAS.Service.Common;
using System;
namespace iDAS.Service
{
    public class TaskAssociateBO : BaseBO
    {
        #region Base
        public Guid? TaskId { get; set; }
        public Guid? TaskAssociateId { get; set; }
        public int? Type { get; set; }
        #endregion
        #region Rules
        /// <summary>
        /// Name of task associate
        /// </summary>
        public string TaskAssociateName { get; set; }
        /// <summary>
        /// Name of relate task: required, semi-required, unrequired
        /// </summary>
        public string TypeName { get { return getType(); } }
        #endregion
        #region Private methods
        private string getType()
        {
            var data = Type == null ? Resource.AssociateType.Required
                : Type == 0 ? Resource.AssociateType.UnRequired
                : Type == 1 ? Resource.AssociateType.SemiRequired
                : Resource.AssociateType.Required;
            return data;
        }
        #endregion
    }
}