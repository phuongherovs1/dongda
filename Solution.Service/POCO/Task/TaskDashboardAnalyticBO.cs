﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public class TaskDashboardAnalyticBO
    {
        public int SumTask { get; set; }
        public int CompleteTask { get; set; }
        public int OverrieTask { get; set; }
        public float ResultTask { get; set; }
        public List<TaskDashboardAnalyticColumnBO> Columns { get; set; }

    }
    public class TaskDashboardAnalyticColumnBO
    {
        public string Month { get; set; }
        public int NumberOfTask { get; set; }
    }
}
