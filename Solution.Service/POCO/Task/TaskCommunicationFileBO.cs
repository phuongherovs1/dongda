﻿using System;

namespace iDAS.Service
{
    public class TaskCommunicationFileBO : BaseBO
    {
        public Nullable<System.Guid> TaskCommunicationId { get; set; }
        public Nullable<System.Guid> FileId { get; set; }
    }
}
