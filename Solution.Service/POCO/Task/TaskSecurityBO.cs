namespace iDAS.Service
{
    public class TaskSecurityBO : BaseBO
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public string Description { get; set; }

    }
}
