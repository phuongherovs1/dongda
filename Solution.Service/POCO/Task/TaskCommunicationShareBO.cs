﻿using System;

namespace iDAS.Service
{
    public class TaskCommunicationShareBO : BaseBO
    {
        public Guid? TaskCommunicationId { get; set; }
        public Guid? TaskResourceId { get; set; }
        public Guid? EmployeeId { get; set; }
    }
}
