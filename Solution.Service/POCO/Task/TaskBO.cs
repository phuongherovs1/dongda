﻿using iDAS.Service.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public class TaskBO : BaseBO
    {
        #region Basic
        private Nullable<System.Guid> parentId;
        public Guid? TaskSourceId { get; set; }
        public Guid? TaskCategoryId { get; set; }
        public Guid? SecurityId { get; set; }
        /// <summary>
        /// Quản lý chất lượng giao việc
        /// </summary>
        public Guid? QualityCAPAId { get; set; }
        public Guid? DispatchId { get; set; }
        public Guid? RiskTargetPlanId { get; set; }
        public Guid? QualityNCId { get; set; }
        public Nullable<System.Guid> ParentId
        {
            get { return parentId; }
            set { parentId = value ?? Guid.Empty; }
        }
        private Nullable<System.Guid> calendarId;
        public Nullable<System.Guid> CalendarId
        {
            get { return calendarId; }
            set { calendarId = value ?? Guid.Empty; }
        }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Content { get; set; }
        private Nullable<double> completeRate;
        public Nullable<double> CompleteRate
        {
            get { return completeRate; }
            set { completeRate = value ?? 0; }
        }
        public Nullable<long> Duration { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public Nullable<System.DateTime> StartAt { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public Nullable<System.DateTime> EndAt { get; set; }
        public Nullable<System.DateTime> DeadlineAt { get; set; }
        public Nullable<bool> IsAssign { get; set; }
        public Nullable<bool> IsPause { get; set; }
        public Nullable<bool> IsCancel { get; set; }
        public Nullable<bool> IsFinish { get; set; }
        private string reasonOfPause;
        public string ReasonOfPause
        {
            get { return reasonOfPause; }
            set { reasonOfPause = value ?? string.Empty; }
        }
        private string reasonOfCancel;
        public string ReasonOfCancel
        {
            get { return reasonOfCancel; }
            set { reasonOfCancel = value ?? string.Empty; }
        }
        private string auditNote;
        public string AuditNote
        {
            get { return auditNote; }
            set { auditNote = value ?? string.Empty; }
        }
        public Nullable<System.DateTime> AssignAt { get; set; }
        public Nullable<System.DateTime> PerformAt { get; set; }
        public Nullable<System.DateTime> PauseAt { get; set; }
        public Nullable<System.DateTime> CancelAt { get; set; }
        public Nullable<System.DateTime> CompleteAt { get; set; }
        public Nullable<System.DateTime> FinishAt { get; set; }
        public Guid? KPICatagoryId { get; set; }
        #endregion

        #region Rule
        public EmployeeBO EmployeePerformQuality { get; set; }
        public FileUploadBO FileAttachs { get; set; }
        public bool IsPerform
        {
            get
            {
                return PerformAt != null && CompleteRate != 100 && IsFinish != true && IsPause != true && IsCancel != true;
            }
        }
        public bool IsComplete
        {
            get
            {
                return CompleteRate >= 100 && IsCancel != true;
            }
        }
        public Resource.Task.Status Status
        {
            get
            {
                var status = IsFinish == true ? Common.Resource.Task.Status.Finish
                            : IsCancel == true ? Common.Resource.Task.Status.Cancel
                            : IsComplete == true ? Common.Resource.Task.Status.Complete
                            : IsPause == true ? Common.Resource.Task.Status.Pause
                            : IsPerform == true ? Common.Resource.Task.Status.Perform
                            : IsAssign == true ? Common.Resource.Task.Status.WaitPerform
                            : Common.Resource.Task.Status.New;
                return status;
            }
        }
        public bool IsOutOfDate
        {
            get
            {
                var time = CompleteAt ?? DateTime.Now;
                return time > EndAt;
            }
        }
        private EmployeeBO employee;
        public EmployeeBO Employee
        {
            get
            {
                return employee ?? (employee = new EmployeeBO());
            }
            set
            {
                employee = value;
            }
        }
        public Guid? EmployeeIdOther { get; set; }
        private EmployeeBO employeePerform;
        public EmployeeBO EmployeePerform
        {
            get
            {
                return employeePerform ?? (employeePerform = new EmployeeBO());
            }
            set
            {
                employeePerform = value;
            }
        }
        public Guid EmployeeId
        {
            get
            {
                return Employee.Id;
            }
        }
        public TimeSpan DurationTime
        {
            get
            {
                return TimeSpan.FromTicks(Duration ?? 0);
            }
            set
            {
                Duration = value.Ticks;
            }
        }
        public override bool AllowDelete
        {
            get
            {
                return IsAssign != true && base.AllowDelete;
            }
            set
            {
                base.AllowDelete = value;
            }
        }
        public override bool AllowUpdate
        {
            get
            {
                return (IsCancel != true && IsPause != true && IsFinish != true) && (base.AllowUpdate || (Id != null && Id != Guid.Empty && EmployeeId == UserId));
            }
            set
            {
                base.AllowUpdate = value;
            }
        }
        public bool ViewCancel
        {
            get
            {
                return (IsAssign == true
                    && IsFinish != true
                    && AllowCreate != true)
                    || IsCancel == true;
            }
        }
        public bool ViewPause
        {
            get
            {
                return (IsAssign == true
                    && IsFinish != true
                    && IsCancel != true
                    && IsComplete != true
                    && PerformAt != null
                    && AllowCreate != true)
                    || IsPause == true;
            }
        }
        public bool ViewFinish
        {
            get
            {
                return IsAssign != true || Operator == Resource.Operation.Create;
            }
        }
        public bool AllowCancel
        {
            get
            {
                return ViewCancel && (EmployeeId == UserId || CreateBy == UserId);
            }
        }
        public bool AllowPause
        {
            get
            {
                return ViewPause && (EmployeeId == UserId || CreateBy == UserId);
            }
        }
        public bool AllowFinish
        {
            get
            {
                if (Operator == Resource.Operation.Read)
                    return false;
                return IsFinish == true || IsComplete;
            }
        }

        public Guid? QualityPlanId { get; set; }
        public double CompleteRatePlan
        {
            get
            {
                double rate = DateTime.Now >= EndAt ? 100 : 0;
                if (DateTime.Now > StartAt && rate < 100)
                {
                    var deltaTime = EndAt - StartAt ?? new TimeSpan();
                    var deltaTime1 = DateTime.Now - StartAt ?? new TimeSpan();
                    rate = Math.Round(deltaTime1.TotalMinutes / deltaTime.TotalMinutes * 100, 2);
                }
                return rate;
            }
        }
        #endregion

        #region View
        public string StatusText
        {
            get
            {
                return Common.Resource.Task.GetStatusText(Status);
            }
        }
        public string MaxScore { get; set; }
        public string PerformTimeText
        {
            get
            {
                var textday = DurationTime.Days > 0 ? DurationTime.Days + " ngày, " : string.Empty;
                var texthour = DurationTime.Hours > 0 ? DurationTime.Hours + " giờ, " : string.Empty;
                var textminus = DurationTime.Minutes > 0 ? DurationTime.Minutes + " phút" : string.Empty;
                return (textday + texthour + textminus).Trim(' ', ',');
            }
        }
        public string KPICatagoryText { get; set; }
        #endregion
    }
}