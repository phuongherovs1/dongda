﻿using iDAS.Service.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public partial class TaskResourceBO : BaseBO
    {
        #region Base
        public Guid? TaskId { get; set; }
        public Guid? EmployeeId { get; set; }
        public bool? IsRoleCreate { get; set; }
        public bool? IsRoleAssign { get; set; }
        public bool? IsRolePerform { get; set; }
        public bool? IsRoleReview { get; set; }
        public bool? IsRoleView { get; set; }
        public bool? IsRoleMark { get; set; }
        private string note;
        public string Note
        {
            get
            {
                return note;
            }
            set
            {
                note = value ?? string.Empty;
            }
        }
        #endregion
        #region Rule
        public double? CompleteRate { get; set; }
        public Resource.Task.Status TaskStatus { get; set; }
        public Resource.Task.Role TaskRole
        {
            get
            {
                var role = IsRoleAssign == true ? Common.Resource.Task.Role.Assigner :
                    IsRolePerform == true ? Common.Resource.Task.Role.Performer :
                    IsRoleReview == true ? Common.Resource.Task.Role.Reviewer :
                    IsRoleCreate == true ? Common.Resource.Task.Role.Creator :
                    IsRoleMark == true ? Common.Resource.Task.Role.Mark :
                    Common.Resource.Task.Role.Viewer;
                return role;
            }
        }
        public DateTime? StartAt { get; set; }
        public DateTime? EndAt { get; set; }
        private EmployeeBO employee;
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public EmployeeBO Employee
        {
            get
            {
                return employee = employee ?? new EmployeeBO();
            }
            set
            {
                employee = value;
                EmployeeId = employee.Id;
            }
        }
        public string EmployeeName
        {
            get
            {
                return Employee.Name;
            }
        }
        public string Employee_Name { get; set; }
        public string EmployeeAvatarUrl
        {
            get
            {
                return Employee.AvatarUrl;
            }
        }
        public string Employee_AvatarUrl { get; set; }
        public double? Unit { get; set; }
        public DateTime? CompleteAt { get; set; }
        public bool IsOutOfDate
        {
            get
            {
                CompleteAt = CompleteAt ?? DateTime.Now;
                return CompleteAt > EndAt;
            }
        }
        public Nullable<long> Duration { get; set; }
        public TimeSpan DurationTime
        {
            get
            {
                return TimeSpan.FromTicks(Duration ?? 0);
            }
            set
            {
                Duration = value.Ticks;
            }
        }
        public bool AllowAssign
        {
            get
            {
                return TaskStatus != Resource.Task.Status.Pause && TaskStatus != Resource.Task.Status.Cancel && TaskStatus != Resource.Task.Status.Finish;
            }
        }
        public bool AllowReport
        {
            get
            {
                return TaskStatus != Resource.Task.Status.Pause && TaskStatus != Resource.Task.Status.Cancel && TaskStatus != Resource.Task.Status.Finish;
            }
        }
        public DateTime? PerformAt { get; set; }
        public DateTime? CompletePlanAt { get; set; }
        public Guid EmployeeCreateId { get; set; }
        public Guid EmployeeAssignId { get; set; }
        public override bool AllowUpdate
        {
            get
            {
                var check1 = TaskStatus != Resource.Task.Status.Pause && TaskStatus != Resource.Task.Status.Cancel && TaskStatus != Resource.Task.Status.Finish;
                var check2 = EmployeeCreateId == UserId || EmployeeAssignId == UserId;
                return check1 && check2 && !base.AllowCreate;
            }
            set
            {
                base.AllowUpdate = value;
            }
        }
        public override bool AllowDelete
        {
            get
            {
                var check1 = TaskStatus != Resource.Task.Status.Pause && TaskStatus != Resource.Task.Status.Cancel && TaskStatus != Resource.Task.Status.Finish;
                var check2 = EmployeeCreateId == UserId || EmployeeAssignId == UserId;
                return check1 && check2 && !AllowCreate;
            }
            set
            {
                base.AllowDelete = value;
            }
        }
        #endregion
        #region View
        public string TaskName { get; set; }
        public string TaskGroup { get; set; }
        public bool TaskGroupCheck
        {
            get
            {
                if (!string.IsNullOrEmpty(TaskGroup))
                    return true;
                else
                    return false;
            }
        }
        public string TaskContent { get; set; }
        public string TaskRoleText
        {
            get
            {
                return Common.Resource.Task.GetResourceRoleText(TaskRole);
            }
        }
        public string TaskRoleColor
        {
            get
            {
                if (IsOutOfDate == true)
                    return "red";
                else
                    return "blue";
            }
        }
        public string ListTaskRoleText
        {
            get; set;
        }
        public string CompleteRateText
        {
            get
            {
                return (CompleteRate.HasValue ? CompleteRate.Value.ToString() : "0") + "%";
            }
        }
        public string TaskStatusText
        {
            get
            {
                return "<span style='font-weight:600;color:" + Common.Resource.Task.GetStatusColor(TaskStatus) + ";'>" + Common.Resource.Task.GetStatusText(TaskStatus) + "</span>";
            }
        }
        public string TaskStatusColor
        {
            get
            {
                return Common.Resource.Task.GetStatusColor(TaskStatus);
            }
        }
        public string StartAtText
        {
            get
            {
                return StartAt.HasValue ? StartAt.Value.ToString("dd/MM/yyyy") : string.Empty;
            }
        }
        public string EndAtText
        {
            get
            {
                return EndAt.HasValue ? EndAt.Value.ToString("dd/MM/yyyy") : string.Empty;
            }
        }
        public string CreateAtText
        {
            get
            {
                return CreateAt.HasValue ? CreateAt.Value.ToString("dd/MM/yyyy") : string.Empty;
            }
        }
        public string UpdateAtText
        {
            get
            {
                return UpdateAt.HasValue ? UpdateAt.Value.ToString("dd/MM/yyyy") : CreateAtText;
            }
        }
        public string PerformTimeText
        {
            get
            {
                var textday = DurationTime.Days > 0 ? DurationTime.Days + " ngày, " : string.Empty;
                var texthour = DurationTime.Hours > 0 ? DurationTime.Hours + " giờ, " : string.Empty;
                var textminus = DurationTime.Minutes > 0 ? DurationTime.Minutes + " phút" : string.Empty;
                return (textday + texthour + textminus).Trim(' ', ',');
            }
        }
        #endregion
    }
}