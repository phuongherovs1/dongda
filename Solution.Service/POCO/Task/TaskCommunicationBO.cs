﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Service
{
    public class TaskCommunicationBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> ParentId { get; set; }
        public Nullable<System.Guid> TaskResourceId { get; set; }
        public Nullable<int> Type { get; set; }
        public string Content { get; set; }
        public Nullable<bool> IsPublic { get; set; }
        #endregion
        #region Rule
        /// <summary>
        /// Tên loại phản hồi
        /// </summary>
        public string CommunicationType
        {
            get
            {
                switch (Type)
                {
                    case (int)iDAS.Service.Common.Resource.Task.CommunicationType.Report:
                        return iDAS.Service.Common.Resource.Task.CommunicationText.Report;
                    case (int)iDAS.Service.Common.Resource.Task.CommunicationType.Requirement:
                        return iDAS.Service.Common.Resource.Task.CommunicationText.Requirement;
                    case (int)iDAS.Service.Common.Resource.Task.CommunicationType.Suggest:
                        return iDAS.Service.Common.Resource.Task.CommunicationText.Suggest;
                    default:
                        return iDAS.Service.Common.Resource.Task.CommunicationText.Other;
                }
            }
        }
        /// <summary>
        /// Tên nhân sự phản hồi
        /// </summary>
        public string Employee_Name { get; set; }
        /// <summary>
        /// Đường dẫn ảnh của nhân sự phản hồi
        /// </summary>
        public string Employee_AvatarUrl { get; set; }
        /// <summary>
        /// File đính kèm của phản hồi
        /// </summary>
        //public FileBO File_TaskCommunication { get; set; }
        /// <summary>
        /// Khoảng thời gian đã phản hồi
        /// Ví dụ: 2 phút trước, 30 phút trước
        /// </summary>
        public string TimePost
        {
            get
            {
                if (CreateAt.HasValue)
                {
                    var dateDiff = DateTime.Now.Subtract(CreateAt.Value);
                    if ((int)dateDiff.TotalDays < 1 && (int)dateDiff.TotalHours < 1 && (int)dateDiff.TotalMinutes < 1)
                        return string.Format("{0} giây trước", (int)dateDiff.TotalSeconds);
                    else if ((int)dateDiff.TotalDays < 1 && (int)dateDiff.TotalHours < 1 && (int)dateDiff.TotalMinutes > 1)
                        return string.Format("{0} phút trước", (int)dateDiff.TotalMinutes);
                    else if ((int)dateDiff.TotalDays < 1 && (int)dateDiff.TotalHours > 1)
                        return string.Format("{0} giờ trước", (int)dateDiff.TotalHours);
                    else
                        return string.Format("{0} lúc {1}", CreateAt.Value.ToString("dd/MM/yyyy"), CreateAt.Value.Hour + ":" + CreateAt.Value.Minute);
                }
                else
                    return string.Empty;
            }
        }
        /// <summary>
        /// Danh sách trả lời phản hồi
        /// </summary>
        public List<TaskCommunicationBO> CommunicationReplies { get; set; }
        /// <summary>
        /// Tổng số phản hồi
        /// </summary>
        public int TotalComments
        {
            get
            {
                return CommunicationReplies == null ? 0 : CommunicationReplies.Count();
            }
        }
        /// <summary>
        /// Có cho phép sửa xóa phản hồi không true: cho phép, false: không cho phép
        /// </summary>
        public bool IsAllowEdit { get; set; }
        public bool IsPerForm { get; set; }
        public FileUploadBO AttachFiles { get; set; }
        public string TaskName { get; set; }
        public Guid? TaskId { get; set; }
        #endregion
    }
}
