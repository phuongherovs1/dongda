﻿using System;

namespace iDAS.Service
{
    public class ProfileDepartmentArchiveRoleBO : BaseBO
    {
        #region Base
        public Guid? DepartmentId { get; set; }

        public Guid? EmployeeId { get; set; }

        public Guid? TitleId { get; set; }

        public string ObjectName { get; set; }
        public string ObjectImage { get; set; }
        #endregion

        #region Rule
        public bool IsEmployee { get { return EmployeeId.HasValue && EmployeeId != Guid.Empty; } }
        public string DepartmentName { get; set; }
        #endregion
    }

}
