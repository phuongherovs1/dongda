﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class ProfileBorrowBO : BaseBO
    {
        #region Base
        public Guid? BorrowEmployeeId { get; set; }
        public Guid? AssignEmployeeId { get; set; }
        public Guid? ProfileId { get; set; }

        public Guid? ProfileSuggestBorrowId { get; set; }

        public DateTime? BorrowDate { get; set; }

        public DateTime? ReturnEstimateDate { get; set; }

        public DateTime? ExtendDate { get; set; }

        public string ReasonExtend { get; set; }

        public DateTime? ReturnEstimateDateExtend { get; set; }

        public int? Status { get; set; }

        public DateTime? ReturnDate { get; set; }

        public bool? IsClose { get; set; }

        public string Note { get; set; }

        #endregion

        #region Rule
        public string StatusText
        {
            get
            {
                var result = string.Empty;
                switch (Status)
                {
                    case (int)Resource.ProfileBorrowStatus.WaitAssign:
                        result = Resource.ProfileBorrowStatusText.WaitAssign;
                        break;
                    case (int)Resource.ProfileBorrowStatus.Borrowing:
                        result = Resource.ProfileBorrowStatusText.Borrowing;
                        break;
                    case (int)Resource.ProfileBorrowStatus.Destroy:
                        result = Resource.ProfileBorrowStatusText.Destroy;
                        break;
                    case (int)Resource.ProfileBorrowStatus.Extend:
                        result = Resource.ProfileBorrowStatusText.Extend;
                        break;
                    case (int)Resource.ProfileBorrowStatus.OverTimeReturn:
                        result = Resource.ProfileBorrowStatusText.OverTimeReturn;
                        break;
                    case (int)Resource.ProfileBorrowStatus.Return:
                        result = Resource.ProfileBorrowStatusText.Return;
                        break;
                    default:
                        break;
                }
                return result;
            }
        }
        /// <summary>
        /// Người mượn hồ sơ
        /// </summary>
        public EmployeeBO BorrowEmployee { get; set; }
        public EmployeeBO AssignEmployee { get; set; }
        public ProfileBO Profile { get; set; }
        #endregion

    }
}
