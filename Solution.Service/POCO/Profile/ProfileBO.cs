﻿using System;

namespace iDAS.Service
{
    public class ProfileBO : BaseBO
    {
        #region Base
        public Guid? DepartmentId { get; set; }
        public Guid? CategoryId { get; set; }
        public Guid? SecurityId { get; set; }
        public Guid? DepartmentArchiveId { get; set; }
        public Guid? EmployeeArchiveId { get; set; }
        public string Name { get; set; }
        // Ghi chú
        public string Note { get; set; }
        // thời gian mở
        public DateTime? TimeOpen { get; set; }
        // thời gian đóng hồ sơ
        public DateTime? TimeClose { get; set; }
        // Mã hồ sơ
        public string Code { get; set; }
        // Trạng thái
        public int? Status { get; set; }
        // Ngày lập hồ sơ
        public DateTime? ProfileCreateAt { get; set; }
        // Nội dung hồ sơ
        public string ProfileContent { get; set; }
        // Ngày lưu trữ
        public DateTime? ArchiveDate { get; set; }
        // Thời hạn lưu trữ
        public DateTime? ArchiveDeadline { get; set; }
        // Số tờ
        public int? ProfileNumber { get; set; }
        // Thời gian lưu trữ
        public int? ArchiveTime { get; set; }
        // Hình thức lưu trữ
        public int? FormalitySave { get; set; }
        // Số cặp
        public string ProfileBox { get; set; }
        // Kiểu tháng ,năm,ngày lưu trữ
        public int? TypeDate { get; set; }
        public DateTime? DestroyAt { get; set; }

        public string ContentHtml { get; set; }
        #endregion

        #region Rule
        public string DepartmentName { get; set; }
        public string CategoryName { get; set; }
        public ProfileSecurityBO Security { get; set; }
        public string SecurityName { get { return Security == null ? string.Empty : Security.Name; } }
        public string SecurityColor { get { return Security == null ? string.Empty : Security.Color; } }
        public EmployeeBO EmployeeArchive { get; set; }
        public string FormalitySaveText
        {
            get
            {
                var status = string.Empty;

                if (FormalitySave == (int)iDAS.Service.Common.Resource.FormalitySaveStatus.Hard)
                    status = iDAS.Service.Common.Resource.FormalitySaveStatusText.Hard;
                if (FormalitySave == (int)iDAS.Service.Common.Resource.FormalitySaveStatus.Soft)
                    status = iDAS.Service.Common.Resource.FormalitySaveStatusText.Soft;
                return status;
            }
        }
        public string TimeArchiveText
        {
            get
            {
                var status = string.Empty;
                switch (TypeDate)
                {
                    case (int)iDAS.Service.Common.Resource.ProfileArchiveTime.Day:
                        status = ArchiveTime.HasValue ? ArchiveTime + " " + iDAS.Service.Common.Resource.ArchiveTimeText.Day : string.Empty;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileArchiveTime.Month:
                        status = ArchiveTime.HasValue ? ArchiveTime + " " + iDAS.Service.Common.Resource.ArchiveTimeText.Month : string.Empty;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileArchiveTime.Year:
                        status = ArchiveTime.HasValue ? ArchiveTime + " " + iDAS.Service.Common.Resource.ArchiveTimeText.Year : string.Empty;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileArchiveTime.Permanent:
                        status = iDAS.Service.Common.Resource.ArchiveTimeText.Permanent;
                        break;
                }
                return status;
            }
        }
        public string StatusText
        {
            get
            {
                var type = string.Empty;
                switch (Status)
                {
                    case (int)iDAS.Service.Common.Resource.ProfileStatus.All:
                        type = iDAS.Service.Common.Resource.ProfileStatusText.All;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileStatus.Archive:
                        type = iDAS.Service.Common.Resource.ProfileStatusText.Archive;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileStatus.Close:
                        type = iDAS.Service.Common.Resource.ProfileStatusText.Close;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileStatus.Destroy:
                        type = iDAS.Service.Common.Resource.ProfileStatusText.Destroy;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileStatus.New:
                        type = iDAS.Service.Common.Resource.ProfileStatusText.New;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileStatus.Update:
                        type = iDAS.Service.Common.Resource.ProfileStatusText.Update;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileStatus.Open:
                        type = iDAS.Service.Common.Resource.ProfileStatusText.Open;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileStatus.WaitDestroy:
                        type = iDAS.Service.Common.Resource.ProfileStatusText.WaitDestroy;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileStatus.WaitReview:
                        type = iDAS.Service.Common.Resource.ProfileStatusText.WaitReview;
                        break;
                }
                return type;
            }
        }
        public string DepartmentArchiveName { get; set; }
        public string strDepartmentArchiveId { get; set; }
        public EmployeeBO ArchiveEmployee { get; set; }
        public string ArchiveTimeText
        {
            get
            {
                var result = string.Empty;
                switch (TypeDate)
                {
                    case (int)Common.Resource.ProfileArchiveTime.Day:
                        if (ArchiveTime != null)
                            result = DateTime.Now >= ArchiveDate.Value.AddDays(ArchiveTime.Value) ? "Quá hạn" : ArchiveTime + " ngày";
                        break;
                    case (int)Common.Resource.ProfileArchiveTime.Month:
                        if (ArchiveTime != null)
                            result = DateTime.Now >= ArchiveDate.Value.AddMonths(ArchiveTime.Value) ? "Quá hạn" : ArchiveTime + " tháng";
                        break;
                    case (int)Common.Resource.ProfileArchiveTime.Year:
                        if (ArchiveTime != null)
                            result = DateTime.Now >= ArchiveDate.Value.AddYears(ArchiveTime.Value) ? "Quá hạn" : ArchiveTime + " năm";
                        break;
                    case (int)Common.Resource.ProfileArchiveTime.Permanent:
                        result = "Vĩnh viễn";
                        break;
                    default:
                        break;
                }
                return result;
            }
        }

        /// <summary>
        /// Thời hạn lưu trữ đến ngày
        /// </summary>
        public string DeadlineDate
        {
            get
            {
                string result = string.Empty;
                switch (TypeDate)
                {
                    case (int)Common.Resource.ProfileArchiveTime.Day:
                        if (ArchiveTime != null)
                            result = ArchiveTime + " ngày";
                        break;
                    case (int)Common.Resource.ProfileArchiveTime.Month:
                        if (ArchiveTime != null)
                            result = ArchiveTime + " tháng";
                        break;
                    case (int)Common.Resource.ProfileArchiveTime.Year:
                        if (ArchiveTime != null)
                            result = ArchiveTime + " năm";
                        break;
                    case (int)Common.Resource.ProfileArchiveTime.Permanent:
                        result = "Vĩnh viễn";
                        break;
                    default:
                        break;
                }
                return result;
            }
        }

        /// <summary>
        /// Thời gian lưu trữ hồ sơ trong danh sách hồ sơ lưu trữ
        /// - Nếu hồ sơ chưa nộp lưu thì lấy thời gian lưu trữ tại cá nhân trong danh mục
        /// - Nếu hồ sơ đã nộp lưu rồi thì lấy thời gian lưu trữ tại bộ phận trong danh mục
        /// </summary>
        public string ProfileTimeArchive { get; set; }

        /// <summary>
        /// Thời hạn lưu trữ là thời gian lưu trữ tại bộ phận lấy trong danh mục
        /// </summary>
        public string DeadlineArchiveText { get; set; }

        /// <summary>
        /// Ngày lưu trữ hồ sơ
        /// - Nếu hồ sơ chưa nộp lưu thì lấy ngày mở hồ sơ
        /// - Nếu hồ sơ đã nộp lưu rồi thì ngày lưu trữ là ngày tiếp nhận trong biên bản bàn giao
        /// </summary>
        public DateTime? ProfileArchiveDate { get; set; }

        /// <summary>
        /// Ngày lưu trữ
        /// - Nếu hồ sơ chưa nộp lưu trữ thì ngày lưu trữ là ngày mở hồ sơ
        /// - Nếu hồ sơ đã nộp lưu trứ thì ngày lưu trữ là ngày tiếp nhận (trong biên bản bàn giao)
        /// </summary>
        public DateTime? DayOfStorage { get; set; }

        public ProfileBorrowBO ProfileBorrow { get; set; }
        public DepartmentBO DepartmentArchive { get; set; }
        public DepartmentBO TransferToDepartment { get; set; }
        public string StatusTransferText
        {
            get
            {
                var result = iDAS.Service.Common.Resource.ProfileTransferStatusText.WaitTransfer;
                if (ArchiveDeadline.HasValue && ArchiveDeadline <= DateTime.Now)
                    result = iDAS.Service.Common.Resource.ProfileTransferStatusText.OverTimeTransfer;
                if (Status == (int)iDAS.Service.Common.Resource.ProfileTransferStatus.Archive)
                    result = iDAS.Service.Common.Resource.ProfileTransferStatusText.Archive;
                if (Status == (int)iDAS.Service.Common.Resource.ProfileTransferStatus.WaitArchive)
                    result = iDAS.Service.Common.Resource.ProfileTransferStatusText.WaitArchive;
                if (Status == (int)iDAS.Service.Common.Resource.ProfileTransferStatus.WaitAdd)
                    result = iDAS.Service.Common.Resource.ProfileTransferStatusText.WaitAdd;
                if (Status == (int)iDAS.Service.Common.Resource.ProfileTransferStatus.Destroy)
                    result = iDAS.Service.Common.Resource.ProfileTransferStatusText.Destroy;
                return result;
            }
        }
        public ProfileTransferBO ProfileTransfer { get; set; }
        public string DestroyName { get; set; }

        /// <summary>
        /// Ẩn button thêm thành phần hồ sơ khi xem chi tiết
        /// </summary>
        public bool? IsHiddenButton { get; set; }

        /// <summary>
        /// Kiểm tra ẩn hoặc hiện context menu khi người đăng nhập có phải người tạo hồ sơ không
        /// </summary>
        public bool? HideContextMenuDelete { get; set; }

        public FileUploadBO FileAttachment { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid? ProfileCuriculmviateId { get; set; }
        public bool? profileDestroySuggeststatus { get; set; }
        public string GroupSummaryName { get; set; }
        public Guid? profileDestroySuggestId { get; set; }

        #endregion

    }
}
