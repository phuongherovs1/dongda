﻿using System;

namespace iDAS.Service
{
    public class ProfileSuggestBorrowBO : BaseBO
    {
        #region Base

        public Guid? ProfileId { get; set; }

        public DateTime? BorrowDate { get; set; }

        public DateTime? ReturnEstimateDate { get; set; }

        public string ReasonBorrow { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public bool? IsSend { get; set; }

        public DateTime? SendAt { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }

        public string ReasonReject { get; set; }

        public DateTime? ApprovalAt { get; set; }
        #endregion

        #region Rule
        public ProfileBO Profile { get; set; }
        public EmployeeBO Approval { get; set; }
        public EmployeeBO Creater { get; set; }
        #endregion
    }
}
