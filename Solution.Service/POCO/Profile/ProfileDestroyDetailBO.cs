﻿using System;

namespace iDAS.Service
{
    public class ProfileDestroyDetailBO : BaseBO
    {
        #region Base
        public Guid? ProfileDestroySuggestId { get; set; }

        public Guid? ProfileId { get; set; }

        public int? Status { get; set; }

        /// <summary>
        /// Xem xét: True: cập nhật hồ sơ sang trạng thái chờ hủy, False: cập nhật hồ sơ về trạng thái lưu trữ
        /// </summary>
        public bool? IsAccept { get; set; }

        public string Note { get; set; }

        public bool? IsDestroy { get; set; }

        public string RejectOfDestroy { get; set; }
        #endregion

        #region Rule
        public ProfileBO Profile { get; set; }
        public string ProfileName { get { return Profile == null ? string.Empty : Profile.Name; } }
        public string ProfileCode { get { return Profile == null ? string.Empty : Profile.Code; } }
        public string ProfileBox { get { return Profile == null ? string.Empty : Profile.ProfileBox; } }
        public string ProfileTimeArchiveText { get { return Profile == null ? string.Empty : Profile.TimeArchiveText; } }
        public DateTime? ProfileArchiveDeadline { get { return Profile == null ? null : Profile.ArchiveDeadline; } }
        public DateTime? ProfileArchiveDate { get { return Profile == null ? null : Profile.ArchiveDate; } }
        /// <summary>
        /// trạng thái
        /// </summary>
        public string ResultText
        {
            get
            {
                var result = string.Empty;
                switch (Status)
                {
                    case (int)iDAS.Service.Common.Resource.ProfileStatus.WaitReview:
                        result = iDAS.Service.Common.Resource.ProfileStatusText.WaitReview;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileStatus.WaitDestroy:
                        result = iDAS.Service.Common.Resource.ProfileStatusText.WaitDestroy;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileStatus.Archive:
                        result = iDAS.Service.Common.Resource.ProfileStatusText.Archive;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileStatus.Destroy:
                        result = iDAS.Service.Common.Resource.ProfileStatusText.Destroy;
                        break;
                }
                return result;
            }
        }

        /// <summary>
        /// Kết quả hủy
        /// </summary>
        public string DestroyResult
        {
            get
            {
                var result = IsDestroy == true ? iDAS.Service.Common.Resource.ProfileDestroyDetailText.Destroy
                    : IsDestroy == false ? iDAS.Service.Common.Resource.ProfileDestroyDetailText.NotDestroy : "Chưa hủy";
                return result;
            }
        }
        #endregion
    }
}
