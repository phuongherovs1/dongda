﻿namespace iDAS.Service
{
    public class ProfileMasterBO : BaseBO
    {
        public string Name { get; set; }

        public int? Rank { get; set; }

        public string DataType { get; set; }
    }
}
