﻿using System;

namespace iDAS.Service
{
    public class ProfileResponsibilityBO : BaseBO
    {
        #region Base
        public Guid? ProfileId { get; set; }

        public Guid? EmployeeId { get; set; }

        public int? ResultType { get; set; }

        public int? RoleType { get; set; }

        public DateTime? ResponsibiltityAt { get; set; }

        public DateTime? SendAt { get; set; }

        public DateTime? ApprovalAt { get; set; }

        public int? StatusType { get; set; }

        public string Note { get; set; }

        #endregion

        #region Rule
        public string ProfileName { get; set; }
        public EmployeeBO EmployeePerformBy { get; set; }
        public EmployeeBO EmployeeCreateBy { get; set; }
        public string RoleText
        {
            get
            {
                var result = "";
                switch (RoleType)
                {
                    case (int)Common.Resource.ProfileResponsibilityRole.Creater: result = Common.Resource.ProfileResponsibilityRoleText.Creater; break;
                    case (int)Common.Resource.ProfileResponsibilityRole.Checker: result = Common.Resource.ProfileResponsibilityRoleText.Checker; break;
                    case (int)Common.Resource.ProfileResponsibilityRole.Approval: result = Common.Resource.ProfileResponsibilityRoleText.Approval; break;
                }
                return result;
            }
        }
        public string StatusText
        {
            get
            {
                var result = "";
                switch (StatusType)
                {
                    case (int)Common.Resource.ProfileResponsibilityStatus.WaitSend: result = Common.Resource.ProfileResponsibilityStatusText.WaitSend; break;
                    case (int)Common.Resource.ProfileResponsibilityStatus.Sent: result = Common.Resource.ProfileResponsibilityStatusText.Sent; break;
                    case (int)Common.Resource.ProfileResponsibilityStatus.WaitCheck: result = Common.Resource.ProfileResponsibilityStatusText.WaitCheck; break;
                    case (int)Common.Resource.ProfileResponsibilityStatus.CheckAccept: result = Common.Resource.ProfileResponsibilityStatusText.CheckAccept; break;
                    case (int)Common.Resource.ProfileResponsibilityStatus.CheckFail: result = Common.Resource.ProfileResponsibilityStatusText.CheckFail; break;
                    case (int)Common.Resource.ProfileResponsibilityStatus.WaitApprove: result = Common.Resource.ProfileResponsibilityStatusText.WaitApprove; break;
                    case (int)Common.Resource.ProfileResponsibilityStatus.ApproveAccept: result = Common.Resource.ProfileResponsibilityStatusText.ApproveAccept; break;
                    case (int)Common.Resource.ProfileResponsibilityStatus.ApproveFail: result = Common.Resource.ProfileResponsibilityStatusText.ApproveFail; break;
                }
                return result;
            }
        }
        #endregion

    }
}
