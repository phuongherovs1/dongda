﻿using System;

namespace iDAS.Service
{
    public class ProfileCategoryBO : BaseBO
    {
        #region Base
        public Guid? ParentId { get; set; }

        public Guid? DepartmentId { get; set; }

        // Phòng ban chuyển lữu trữ
        public Guid? TransferDepartmentId { get; set; }

        public string Name { get; set; }

        public string CodeRule { get; set; }

        public string Content { get; set; }

        public bool? IsArchivePerson { get; set; }

        public bool? IsArchivePart { get; set; }

        public bool? IsTransferArchive { get; set; }

        public int? ArchiveTime1 { get; set; }

        public int? Times1 { get; set; }

        public int? ArchiveTime2 { get; set; }

        public int? Times2 { get; set; }
        #endregion

        #region Rule
        /// <summary>
        /// Tên phòng ban
        /// </summary>
        public string DepartmentName { get; set; }
        //  <summary>
        /// giá trị: true - category chứa category con, false - ngược lại
        /// </summary>
        public bool IsParent
        {
            get;
            set;
        }
        /// <summary>
        /// Đếm số lượng tài liệu trong danh mục
        /// </summary>
        public int CountProfile { get; set; }

        /// <summary>
        /// Tên danh mục
        /// </summary>
        public string NameFormat
        {
            get
            {
                var result = CountProfile > 0 ? string.Format("{0} ({1} hồ sơ)", Name, CountProfile) : string.Format("{0}", Name);
                return result;

            }
        }
        #endregion
    }
}
