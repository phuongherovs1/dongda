﻿using System;

namespace iDAS.Service
{
    public class ProfileDestroyReportBO : BaseBO
    {
        #region Base
        public Guid? ProfileDestroySuggestId { get; set; }

        public bool? IsComplete { get; set; }

        public DateTime? ReportAt { get; set; }

        public DateTime? DestroyAt { get; set; }

        public string Note { get; set; }
        public Guid? ProfileTypeDestroyId { get; set; }

        #endregion

        #region Rule
        public ProfileDestroySuggestBO ProfileDestroy { get; set; }

        /// <summary>
        /// Người tạo biên bản
        /// </summary>
        public EmployeeBO EmployeeCreateBy { get; set; }

        /// <summary>
        /// Chuỗi EmployeeId nhân sự tham gia
        /// </summary>
        public string EmployeeIds { get; set; }

        /// <summary>
        /// Chuỗi tên nhân sự tham gia
        /// </summary>
        public string EmployeeName { get; set; }
        #endregion
    }
}
