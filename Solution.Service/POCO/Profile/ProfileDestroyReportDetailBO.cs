﻿using System;

namespace iDAS.Service
{
    public class ProfileDestroyReportDetailBO : BaseBO
    {
        #region Base
        public Guid? ProfileDestroyReportId { get; set; }
        public Guid? EmployeeId { get; set; }

        #endregion

        #region Rule

        #endregion
    }
}
