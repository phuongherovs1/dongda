﻿namespace iDAS.Service
{
    public class ProfileTypeDestroyBO : BaseBO
    {
        #region Base
        public string Name { get; set; }

        public string Color { get; set; }

        public string Description { get; set; }

        #endregion

        #region Rule

        #endregion
    }
}
