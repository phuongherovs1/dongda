﻿using System;

namespace iDAS.Service
{
    public class ProfileTransferBO : BaseBO
    {
        #region Base
        public Guid? SendDepartmentId { get; set; }

        public Guid? SendEmployeeId { get; set; }

        public Guid? DepartmentArchiveId { get; set; }

        public Guid? EmployeeArchiveId { get; set; }

        public DateTime? DateReceive { get; set; }

        public DateTime? DateSend { get; set; }

        public int? SendResult { get; set; }

        public string Contents { get; set; }
        #endregion

        #region Rule
        public EmployeeBO SendEmployee { get; set; }
        public EmployeeBO ArchiveEmployee { get; set; }

        public ProfileBO Profile { get; set; }
        public Guid ProfileId { get; set; }
        #endregion
    }
}
