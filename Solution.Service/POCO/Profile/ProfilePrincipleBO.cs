﻿using System;

namespace iDAS.Service
{
    public class ProfilePrincipleBO : BaseBO
    {
        #region Base
        public Guid? DepartmentId { get; set; }

        public Guid? CategoryId { get; set; }

        public int? ArchiveType { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Guid? DocumentPublishId { get; set; }

        public bool? IsUse { get; set; }

        public int? Order { get; set; }
        #endregion

        #region Rule
        /// <summary>
        /// Tên phòng ban
        /// </summary>
        public string DepartmentName { get; set; }

        /// <summary>
        /// Tên danh mục
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// Tên tài liệu ban hành
        /// </summary>
        public string DocumentName { get; set; }

        /// <summary>
        /// File đính kèm
        /// </summary>
        public FileUploadBO FileAttachments { get; set; }

        public string ArchiveText
        {
            get
            {
                return ArchiveType == (int)iDAS.Service.Common.Resource.FormalitySaveStatus.Hard ? iDAS.Service.Common.Resource.FormalitySaveStatusText.Hard :
                       ArchiveType == (int)iDAS.Service.Common.Resource.FormalitySaveStatus.Soft ? iDAS.Service.Common.Resource.FormalitySaveStatusText.Soft : string.Empty;
            }
        }

        public string Use
        {
            get
            {
                return IsUse == true ? "Có" : IsUse == false ? "Không" : string.Empty;
            }
        }
        #endregion
    }
}
