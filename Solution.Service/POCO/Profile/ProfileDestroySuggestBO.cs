﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public class ProfileDestroySuggestBO : BaseBO
    {
        #region Base
        public Guid? DepartmentId { get; set; }

        public string ReasonOfReject { get; set; }

        public string ReasonOfSuggest { get; set; }

        public DateTime? SuggestAt { get; set; }

        public int? ProfileDestroyTotal { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }

        public int? Status { get; set; }

        public bool? IsSend { get; set; }
        #endregion

        #region Rule
        /// <summary>
        /// Tên người đề nghị
        /// </summary>
        public string SuggestName { get; set; }
        /// <summary>
        /// Người phê duyệt
        /// </summary>
        public EmployeeBO EmployeeApproveBy { get; set; }
        /// <summary>
        /// Người tạo đề nghị
        /// </summary>
        public EmployeeBO EmployeeSuggestBy { get; set; }
        public string GroupSummaryName { get { return string.Format("Đề nghị hủy hồ sơ ngày - {0} ({1} hồ sơ) - {2}", String.Format("{0:dd/MM/yyyy HH:mm}", CreateAt), ProfileDestroyTotal, IsApprove == true ? "Đã được duyệt" : "Chưa được duyệt"); } }
        /// <summary>
        /// Đếm số hồ sơ trong đề nghị hủy
        /// </summary>
        public int? CountProfile { get; set; }
        /// <summary>
        /// Danh sách hồ sơ của đề nghị hủy
        /// </summary>
        public IEnumerable<Guid> ProfileIds { get; set; }

        /// <summary>
        /// Lưu lại chuỗi ProfileId đã chọn
        /// </summary>
        public string strProfile { get; set; }

        /// <summary>
        /// Trạng thái
        /// </summary>
        public string StatusText
        {
            get
            {
                var status = string.Empty;
                switch (Status)
                {
                    case (int)Resource.ProfileDestroyStatus.New:
                        status = Resource.ProfileDestroyStatusText.New;
                        break;
                    case (int)Resource.ProfileDestroyStatus.WaitApprove:
                        status = Resource.ProfileDestroyStatusText.WaitApprove;
                        break;
                    case (int)Resource.ProfileDestroyStatus.WaitPerform:
                        status = Resource.ProfileDestroyStatusText.WaitPerform;
                        break;
                    case (int)Resource.ProfileDestroyStatus.Approved:
                        status = Resource.ProfileDestroyStatusText.Approved;
                        break;
                    case (int)Resource.ProfileDestroyStatus.Reject:
                        status = Resource.ProfileDestroyStatusText.Reject;
                        break;
                    case (int)Resource.ProfileDestroyStatus.BillCreated:
                        status = Resource.ProfileDestroyStatusText.BillCreated;
                        break;
                }
                return status;
            }
        }


        public bool? IsShow { get; set; }
        #endregion
    }
}
