﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class ProfileDasboardBO
    {
        #region Base

        #endregion

        #region Rule
        public Guid? ProfileId { get; set; }
        string _color = iDAS.Service.Common.Resource.RequestStatusColor.Perform;
        public Guid Id { get; set; }
        public DateTime? CreateAt { get; set; }
        public string Time { get; set; }
        public string Name { get; set; }
        public bool? IsApproval { get; set; }
        public bool? IsAccept { get; set; }
        public string StatusText
        {
            get
            {
                var status = string.Empty;
                switch (RoleType)
                {
                    case (int)Resource.ProfileRoleType.BorrowApproval:
                        if (IsApproval != true)
                        {
                            status = Common.Resource.ApproveStatusText.Wait;
                        }
                        if (IsAccept == true)
                        {
                            status = Common.Resource.ApproveStatusText.Approve;
                        }
                        if (IsAccept == false)
                        {
                            status = Common.Resource.ApproveStatusText.Reject;
                        }
                        break;
                    // Phê duyệt đề nghị hủy hồ sơ
                    case (int)Resource.ProfileRoleType.DestroyApproval:
                        if (IsApproval != true)
                        {
                            status = Common.Resource.ApproveStatusText.WaitReview;
                        }
                        if (IsAccept == true)
                        {
                            status = Common.Resource.ApproveStatusText.Approve;
                        }
                        if (IsAccept == false)
                        {
                            status = Common.Resource.ApproveStatusText.Reject;
                        }
                        break;
                    // Phân công cập nhật hồ sơ
                    case (int)Resource.ProfileRoleType.ProfileAssign:
                        status = StatusAssign == (int)Resource.ProfileAssignStatus.WaitUpdate ? Resource.ProfileAssignStatusText.WaitUpdate : string.Empty;
                        break;
                    // Người kiểm tra hồ sơ
                    case (int)Resource.ProfileRoleType.ProfileChecker:
                        status = StatusResponsibility == (int)Resource.ProfileResponsibilityStatus.WaitCheck ? Resource.ProfileResponsibilityStatusText.WaitCheck
                                : StatusResponsibility == (int)Resource.ProfileResponsibilityStatus.CheckFail ? Resource.ProfileResponsibilityStatusText.CheckFail
                                : string.Empty;
                        break;
                    // Người phê duyệt hồ sơ
                    case (int)Resource.ProfileRoleType.ProfileApprover:
                        status = StatusResponsibility == (int)Resource.ProfileResponsibilityStatus.WaitApprove ? Resource.ProfileResponsibilityStatusText.WaitApprove
                                : StatusResponsibility == (int)Resource.ProfileResponsibilityStatus.ApproveFail ? Resource.ProfileResponsibilityStatusText.ApproveFail
                                : string.Empty;
                        break;
                    // Người lưu trữ hồ sơ
                    case (int)Resource.ProfileRoleType.ProfileArchiver:
                        status = StatusArchive == (int)Resource.ESendResult.WaitArchive ? Resource.ESendResultText.WaitArchive
                                : string.Empty;
                        break;
                }
                return status;
            }
        }
        public string Color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
            }
        }
        /// <summary>
        /// Vai trò
        /// </summary>
        public string RoleText
        {
            get
            {
                var roleText = string.Empty;
                switch (RoleType)
                {
                    case (int)iDAS.Service.Common.Resource.ProfileRoleType.DestroyApproval:
                        roleText = iDAS.Service.Common.Resource.ProfileRoleTypeText.DestroyApproval;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileRoleType.BorrowApproval:
                        roleText = iDAS.Service.Common.Resource.ProfileRoleTypeText.BorrowApproval;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileRoleType.BorrowCreater:
                        roleText = iDAS.Service.Common.Resource.ProfileRoleTypeText.BorrowCreater;
                        break;
                    case (int)iDAS.Service.Common.Resource.ProfileRoleType.ProfileAssign:
                        roleText = iDAS.Service.Common.Resource.ProfileRoleTypeText.ProfileAssign;
                        break;
                    // Người kiểm tra hồ sơ
                    case (int)iDAS.Service.Common.Resource.ProfileRoleType.ProfileChecker:
                        roleText = iDAS.Service.Common.Resource.ProfileRoleTypeText.ProfileChecker;
                        break;
                    // Người phê duyệt hồ sơ
                    case (int)iDAS.Service.Common.Resource.ProfileRoleType.ProfileApprover:
                        roleText = iDAS.Service.Common.Resource.ProfileRoleTypeText.BorrowApproval;
                        break;
                    // Người lưu trữ hồ sơ
                    case (int)iDAS.Service.Common.Resource.ProfileRoleType.ProfileArchiver:
                        roleText = iDAS.Service.Common.Resource.ProfileRoleTypeText.ProfileArchiver;
                        break;
                    default:
                        break;
                }
                return roleText;
            }
        }
        /// <summary>
        /// Loại thông tin
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// Loại vai trò
        /// </summary>
        public int RoleType { get; set; }

        /// <summary>
        /// True: nếu là đề nghị hủy. !=True: không phải để nghị hủy
        /// </summary>
        public bool? IsDestroy { get; set; }

        /// <summary>
        /// Trạng thái của phân công cập nhật hồ sơ
        /// </summary>
        public int? StatusAssign { get; set; }
        /// <summary>
        /// Trạng thái của vai trò
        /// </summary>
        public int? StatusResponsibility { get; set; }
        /// <summary>
        /// Trạng thái lưu trữ
        /// </summary>
        public int? StatusArchive { get; set; }
        /// <summary>
        /// True: nếu là phân công cập nhật hồ sơ, != True: không phải
        /// </summary>
        public bool? IsAssign { get; set; }
        /// <summary>
        /// True: nếu là phê duyệt hồ sơ, != True: không phải
        /// </summary>
        public bool? IsProfileApproval { get; set; }
        #endregion

    }
}
