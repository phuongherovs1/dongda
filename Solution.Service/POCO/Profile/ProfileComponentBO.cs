﻿using System;

namespace iDAS.Service
{
    public class ProfileComponentBO : BaseBO
    {
        #region Base
        public Guid? ProfileId { get; set; }

        public Guid? ProfilePrincipleId { get; set; }

        public string ProfileName { get; set; }

        public string ProfileCategoryName { get; set; }

        public Guid? EmployeeId { get; set; }

        public EmployeeBO EmployeePerformBy { get; set; }

        public string DocumentName { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public int? ArchiveType { get; set; }

        public bool? IsUpdated { get; set; }

        public bool? IsCancel { get; set; }

        public bool? IsProfilePrinciple { get; set; }

        public DateTime? ProfileUpdateAt { get; set; }
        #endregion

        #region Rule
        public string ArchiveTypeText
        {
            get
            {
                var result = "";
                switch (ArchiveType)
                {
                    case (int)iDAS.Service.Common.Resource.FormalitySaveStatus.Hard: result = iDAS.Service.Common.Resource.FormalitySaveStatusText.Hard; break;
                    case (int)iDAS.Service.Common.Resource.FormalitySaveStatus.Soft: result = iDAS.Service.Common.Resource.FormalitySaveStatusText.Soft; break;
                }
                return result;
            }
        }
        public string StatusText
        {
            get
            {
                var result = "";
                result = IsUpdated == true ? iDAS.Service.Common.Resource.ProfileComponentStatusText.Updating : iDAS.Service.Common.Resource.ProfileComponentStatusText.WaitUpdate;
                if (IsCancel == true) result = iDAS.Service.Common.Resource.ProfileComponentStatusText.Eliminate;
                return result;
            }
        }
        public FileUploadBO FileAttachments { get; set; }
        #endregion

    }
}
