﻿namespace iDAS.Service
{
    public class ProfileSecurityBO : BaseBO
    {
        #region Base
        public string Name { get; set; }

        public bool? IsUse { get; set; }

        public string Color { get; set; }

        public string Description { get; set; }
        #endregion

        #region Rule

        #endregion

    }
}
