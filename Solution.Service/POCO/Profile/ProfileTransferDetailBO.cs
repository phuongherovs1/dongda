﻿using System;

namespace iDAS.Service
{
    public class ProfileTransferDetailBO : BaseBO
    {
        #region Base
        public Guid? ProfileTransferId { get; set; }
        public Guid? ProfileId { get; set; }
        #endregion
    }
}
