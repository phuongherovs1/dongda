﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class ProfileAssignBO : BaseBO
    {
        #region Base
        public Guid? ProfileId { get; set; }
        public Guid? DepartmentId { get; set; }
        public DepartmentBO Department { get; set; }
        public string ProfileName { get; set; }
        public string ContentRequire { get; set; }
        public string ContentOfSuggest { get; set; }
        public DateTime? StartAt { get; set; }
        public DateTime? EndAt { get; set; }
        public Guid? PerformBy { get; set; }
        public DateTime? AssignAt { get; set; }
        public DateTime? DateUpdateProfile { get; set; }
        public int? StatusType { get; set; }
        #endregion

        #region Rule
        public EmployeeBO EmployeePerformBy { get; set; }

        public string StatusText
        {
            get
            {
                var result = "";
                switch (StatusType)
                {
                    case (int)Resource.ProfileAssignStatus.WaitSend: result = Resource.ProfileAssignStatusText.WaitSend; break;
                    case (int)Resource.ProfileAssignStatus.WaitUpdate: result = Resource.ProfileAssignStatusText.WaitUpdate; break;
                    case (int)Resource.ProfileAssignStatus.Updating: result = Resource.ProfileAssignStatusText.Updating; break;
                    case (int)Resource.ProfileAssignStatus.Complete: result = Resource.ProfileAssignStatusText.Complete; break;
                    case (int)Resource.ProfileAssignStatus.CompleteFail: result = Resource.ProfileAssignStatusText.CompleteFail; break;
                    case (int)Resource.ProfileAssignStatus.Destroy: result = Resource.ProfileAssignStatusText.Destroy; break;
                }
                return result;
            }
        }

        /// <summary>
        /// Nếu người đăng nhâp là người tạo hồ sơ thì mới được tạo mới hoặc sửa
        /// </summary>
        public bool? IsHideButtonSave { get; set; }

        /// <summary>
        /// Tổng số thành phần được người cập nhật thực hiện
        /// </summary>
        public string CountComponent { get; set; }
        #endregion

    }
}
