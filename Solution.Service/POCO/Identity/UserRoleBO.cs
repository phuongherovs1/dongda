﻿using System;

namespace iDAS.Service
{
    public class UserRoleBO : BaseBO
    {
        public Guid? SystemUserId { get; set; }
        public Guid? SystemRoleId { get; set; }

        public Guid UserId
        {
            get
            {
                return SystemUserId ?? Guid.Empty;
            }
            set
            {
                SystemUserId = value;
            }
        }
        public Guid RoleId
        {
            get
            {
                return SystemRoleId ?? Guid.Empty;
            }
            set
            {
                SystemRoleId = value;
            }
        }
        public bool IsPermission
        {
            get
            {
                return IsDelete.HasValue ? !IsDelete.Value : true;
            }
            set
            {
                IsDelete = !value;
            }
        }
    }
}
