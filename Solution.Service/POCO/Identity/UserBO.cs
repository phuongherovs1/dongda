﻿using Microsoft.AspNet.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public class UserBO : BaseBO, IUser<Guid>
    {
        #region Base
        private string email;
        [DataType(DataType.EmailAddress)]
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value ?? string.Empty;
            }
        }
        public bool? EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        private string phoneNumber;
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }
            set
            {
                phoneNumber = value ?? string.Empty;
            }
        }
        public bool? PhoneNumberConfirmed { get; set; }
        public bool? TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool? LockoutEnabled { get; set; }
        public int? AccessFailedCount { get; set; }
        private string reasonOfLockout;
        public string ReasonOfLockout
        {
            get
            {
                return reasonOfLockout;
            }
            set
            {
                reasonOfLockout = value ?? string.Empty;
            }
        }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string UserName { get; set; }
        #endregion

        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        //[Required]
        public string Code { get; set; }
        public string LastName { get; set; }
        private string fullName;
        private string name;
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string FullName
        {
            get
            {
                return string.IsNullOrEmpty(fullName) ? UserName : fullName;
            }
            set
            {
                fullName = value;
            }
        }
        private FileBO avatar;
        public FileBO Avatar
        {
            get
            {
                return avatar = avatar ?? new FileBO();
            }
            set
            {
                avatar = value;
            }
        }
        public string AvatarUrl
        {
            get
            {
                return Avatar.Url;
            }
        }
        private FileBO fileAvatar;
        public FileBO FileAvatar
        {
            get
            {
                return fileAvatar = fileAvatar ?? new FileBO();
            }
            set
            {
                fileAvatar = value;
            }
        }
        public bool Locked
        {
            get
            {
                return LockoutEnabled ?? false;
            }
            set
            {
                LockoutEnabled = value;
            }
        }
        public bool IsExistEmployee { get; set; }
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
        public string Status { get; set; }
        public string DepartmentName { get; set; }
        public string Name { get => name; set => name = value; }
    }
}
