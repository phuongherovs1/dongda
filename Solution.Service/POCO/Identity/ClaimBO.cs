﻿using System;

namespace iDAS.Service
{
    public class ClaimBO : BaseBO
    {
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public Guid UserId { get; set; }
    }
}
