﻿using System;

namespace iDAS.Service
{
    public class UserLoginBO : BaseBO
    {
        public Guid SystemUserId { get; set; }
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
    }
}
