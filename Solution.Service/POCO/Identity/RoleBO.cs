﻿using Microsoft.AspNet.Identity;
using System;

namespace iDAS.Service
{
    public class RoleBO : BaseBO, IRole<Guid>
    {
        public string Name { get; set; }
        public string Module { get; set; }
        public string Code { get; set; }
        public string Descriptsion { get; set; }

        public bool IsPermission { get; set; }
    }
}
