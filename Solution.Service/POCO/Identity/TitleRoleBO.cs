﻿using System;

namespace iDAS.Service
{
    public class TitleRoleBO : BaseBO
    {
        public Guid? DepartmentTitleId { get; set; }
        public Guid? SystemRoleId { get; set; }
        public Guid RoleId
        {
            get
            {
                return SystemRoleId ?? Guid.Empty;
            }
            set
            {
                SystemRoleId = value;
            }
        }
        public bool IsPermission
        {
            get
            {
                return IsDelete.HasValue ? !IsDelete.Value : true;
            }
            set
            {
                IsDelete = !value;
            }
        }
    }
}
