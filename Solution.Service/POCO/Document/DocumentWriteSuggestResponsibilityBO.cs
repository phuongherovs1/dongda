﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DocumentWriteSuggestResponsibilityBO : DocumentResponsibilityBO
    {
        #region Base
        public string Name { get; set; }
        public string ReasonOfSuggest { get; set; }
        public DateTime? EstimateAt { get; set; }

        public Guid? EmployeeAssignId { get; set; }

        public Guid? EmployeeWriteId { get; set; }

        public DateTime? SuggestAt { get; set; }

        public Guid? SuggestBy { get; set; }

        public int? StatusType { get; set; }
        #endregion

        #region Rule
        public string Status
        {
            get
            {
                var result = "";
                switch (StatusType)
                {
                    case (int)Resource.DocumentSuggestStatusType.New:
                        result = Resource.DocumentSuggestStatus.New;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.Review:
                        result = Resource.DocumentSuggestStatus.Review;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.Approve:
                        result = Resource.DocumentSuggestStatus.Approve;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.ApproveAccept:
                        result = Resource.DocumentSuggestStatus.ApproveAccept;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.ApproveFail:
                        result = Resource.DocumentSuggestStatus.ApproveFail;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.Destroy:
                        result = Resource.DocumentSuggestStatus.Destroy;
                        break;
                }
                return result;
            }
        }
        public bool IsAllowDelete
        {
            get
            {
                return StatusType == (int)Resource.DocumentSuggestStatusType.New;
            }
        }
        public bool IsAllowDestroy
        {
            get
            {
                return (RoleType != (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit && (IsComplete == false || IsComplete == null));
            }
        }
        public EmployeeBO EmployeeSuggest { get; set; }
        #endregion

    }
}
