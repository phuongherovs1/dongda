﻿using System;

namespace iDAS.Service
{
    public class RequestReceiveBO
    {
        #region Rule
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Date { get; set; }
        public string Statust { get; set; }
        #endregion
    }
}
