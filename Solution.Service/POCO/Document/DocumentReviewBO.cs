﻿using System;

namespace iDAS.Service
{
    public class DocumentReviewBO : BaseBO
    {
        #region Base
        public Guid? DocumentId { get; set; }
        public Guid? ReviewBy { get; set; }
        public DateTime? ReviewAt { get; set; }
        public bool? IsReview { get; set; }
        public bool? IsPass { get; set; }
        public bool? IsSend { get; set; }
        public string Note { get; set; }
        public string ReasonOfReject { get; set; }
        #endregion

        #region Rule
        public DocumentBO Document { get; set; }
        public string DocumentCode { get { return Document.Code; } }
        public string DocumentName { get { return Document.Name; } }
        /// <summary>
        /// Người kiểm duyệt
        /// </summary>
        public EmployeeBO EmployeeReviewBy { get; set; }
        /// <summary>
        /// Tên người duyệt 
        /// </summary>
        public string Reviewer { get; set; }
        public string StatusText
        {
            get
            {
                var status = IsReview == null && IsPass == null && IsSend == null ? Common.Resource.ApproveStatusText.New :
                    IsReview == true && IsPass == true ? Common.Resource.ApproveStatusText.Approve :
                    IsReview == true && IsPass == false ? Common.Resource.ApproveStatusText.Reject :
                    IsSend == true ? Common.Resource.ApproveStatusText.Wait :
                    string.Empty;
                return status;
            }
        }

        /// <summary>
        /// File đính kèm
        /// </summary>
        public FileUploadBO FileAttachs { get { return Document == null ? null : Document.FileAttachs; } }
        #endregion
    }
}
