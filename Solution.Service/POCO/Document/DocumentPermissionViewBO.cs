using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DocumentPermissionViewBO
    {
        #region Rule
        public string Id { get; set; }
        public Nullable<System.Guid> DocumentId { get; set; }
        public Nullable<System.Guid> DepartmentId { get; set; }
        public Nullable<System.Guid> TitleId { get; set; }
        public Nullable<System.Guid> EmployeeId { get; set; }
        public bool? AllowView { get; set; }
        public bool? AllowDownload { get; set; }
        public bool? Allow
        {
            get
            {
                var allow = AllowView == true && AllowDownload == true;
                return allow;
            }
        }
        public bool? Disallow
        {
            get
            {
                var disallow = AllowView != true && AllowDownload != true;
                return disallow;
            }
        }
        public string ObjectId { get; set; }
        public string ObjectName { get; set; }
        public string ObjectImage { get; set; }
        public string ObjectTitle { get; set; }
        public Resource.OrganizationTreeType? ObjectType { get; set; }
        public bool FromView { get; set; }

        public TitleBO DepartmentTitle { get; set; }
        public EmployeeBO Employee { get; set; }
        #endregion
    }
}
