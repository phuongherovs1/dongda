﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DocumentExternalApplySuggestBO : BaseBO
    {
        #region Basic
        public Nullable<System.Guid> ParentId { get; set; }
        public Nullable<System.Guid> DocumentId { get; set; }
        public string SuggestContent { get; set; }
        public Nullable<System.Boolean> IsSend { get; set; }
        public Nullable<System.Boolean> IsApprove { get; set; }
        public Nullable<System.Boolean> IsApply { get; set; }
        public Nullable<System.Int16> RoleStatus { get; set; }
        public Nullable<System.Guid> ApproveBy { get; set; }
        public string ApproveNote { get; set; }
        public Nullable<System.DateTime> ApplyAt { get; set; }
        #endregion

        #region Rule
        public string DocumentName { get; set; }
        public string DocumentCode { get; set; }
        public DocumentBO Document { get; set; }
        /// <summary>
        /// Người phê duyệt
        /// </summary>
        public EmployeeBO EmployeeApprove { get; set; }
        /// <summary>
        /// Người ban hành
        /// </summary>
        public EmployeeBO EmployeePublish { get; set; }

        /// <summary>
        /// Ngày ban hành
        /// </summary>
        public DateTime? PublishApplyAt { get; set; }
        /// <summary>
        /// Ngày hết hạn ban hành
        /// </summary>
        public DateTime? PublishExpireAt { get; set; }
        /// <summary>
        /// Lần ban hành
        /// </summary>
        public int? PublishTime { get; set; }

        public string StatusText
        {
            get
            {
                var status = IsApprove == true && IsApply == true ? Resource.ApproveStatusText.Approve
                    : IsApprove == true && IsApply == false ? Resource.ApproveStatusText.Reject
                    : string.Empty;
                return status;
            }
        }

        public EmployeeBO EmployeeSend { get; set; }

        /// <summary>
        /// Hiển thị button Gửi nếu là người xem xét hoặc phê duyệt, ngược lại thì ẩn
        /// </summary>
        public bool IsShow { get; set; }
        #endregion
    }
}
