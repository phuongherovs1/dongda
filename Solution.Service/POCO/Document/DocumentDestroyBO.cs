using System;

namespace iDAS.Service
{
    public class DocumentDestroyBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> SuggestId { get; set; }
        public Nullable<System.Guid> DocumentId { get; set; }
        public string DestroyType { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<System.DateTime> DestroyAt { get; set; }
        public Nullable<bool> IsDestroy { get; set; }
        public string Note { get; set; }
        public Nullable<int> SuggestQuantity { get; set; }
        #endregion

        #region Rule

        #endregion

    }
}
