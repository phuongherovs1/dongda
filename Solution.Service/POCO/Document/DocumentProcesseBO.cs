﻿using System;

namespace iDAS.Service
{
    public class DocumentProcessBO : BaseBO
    {
        #region Base
        public Guid? DepartmentId { get; set; }
        public int? TypeProcess { get; set; }
        public int? TypeRole { get; set; }
        public Guid? DepartmentTitleId { get; set; }
        public Nullable<Guid> EmployeeId { get; set; }
        public int? Order { get; set; }
        public string Note { get; set; }
        #endregion

        #region Rule
        public TitleBO DepartmentTitle { get; set; }
        public EmployeeBO Employee { get; set; }
        public bool IsEmployee { get { return EmployeeId.HasValue; } }
        public bool IsTitle { get { return DepartmentTitleId.HasValue; } }
        public string RoleName
        {
            get
            {
                if (TypeRole != null)
                {
                    var status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Check;
                    switch ((int)TypeRole.Value)
                    {
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.ApprovalWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.ApproveDocument;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Archiver:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Archiver;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.AssignmentWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.AssignmentWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Check:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Check;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DestroyApproval;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DestroyBillCreater;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DestroyCreater;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DestroyReviewer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Distributer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Distributer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestApproval:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DistributeSuggestApproval;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggester:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DistributeSuggester;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestReviewer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DistributeSuggestReviewer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Promulgate;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.ReviewWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.SuggesterWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Write:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Write;
                            break;
                        default:
                            break;
                    }
                    return status;
                }
                else
                {
                    return string.Empty;
                }
            }

        }
        public iDAS.Service.Common.Resource.DocumentProcessRole EnumRole
        {
            get
            {
                var status = iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit;
                if (TypeRole != null)
                {
                    switch ((int)TypeRole.Value)
                    {
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Archiver:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Archiver;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.AssignmentWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.AssignmentWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Check:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Check;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyCreater;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Distributer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Distributer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestApproval:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestApproval;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggester:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggester;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestReviewer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestReviewer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Write:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Write;
                            break;
                        default:
                            break;
                    }
                }
                return status;
            }
        }
        public DateTime DateUpdate
        {
            get
            {
                if (UpdateAt.HasValue)
                    return UpdateAt.Value;
                else return CreateAt.HasValue ? CreateAt.Value : DateTime.Now;
            }
        }
        public string ObjectName { get; set; }
        public string ObjectID
        {
            get
            {
                if (EmployeeId.HasValue && EmployeeId != Guid.Empty)
                    return "Employee_" + EmployeeId.Value;
                else if (DepartmentTitleId.HasValue && DepartmentTitleId != Guid.Empty)
                    return "Title_" + DepartmentTitleId.Value;
                else
                    return string.Empty;
            }
        }
        #endregion
    }
}
