﻿using System;

namespace iDAS.Service
{
    public class DocumentDestroyReportBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> DestroySuggestId { get; set; }
        public Nullable<System.Guid> RequestId { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTime> DestroyAt { get; set; }
        #endregion

        #region Rule
        public DocumentBO Document { get; set; }

        /// <summary>
        /// Người phê duyệt đề nghị
        /// </summary>
        public EmployeeBO EmployeeApproveSuggest { get; set; }

        /// <summary>
        /// Người tạo đề nghị
        /// </summary>
        public EmployeeBO EmployeeSuggestCreate { get; set; }
        /// <summary>
        /// Ngày phê duyệt đề nghị hủy/Ngày yêu cầu hủy
        /// </summary>
        public DateTime? ApproveAt { get; set; }
        /// <summary>
        /// Nhân xét của đề nghị hủy
        /// </summary>
        public string SuggestNote { get; set; }

        /// <summary>
        /// Ngày yêu cầu
        /// </summary>
        public DateTime? RequestAt { get; set; }

        public DocumentDestroySuggestBO DestroySuggest { get; set; }
        #endregion
    }
}
