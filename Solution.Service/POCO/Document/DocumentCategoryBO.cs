﻿using System;

namespace iDAS.Service
{
    public class DocumentCategoryBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> ParentId { get; set; }
        public Nullable<System.Guid> DepartmentId { get; set; }
        public Nullable<System.Guid> DistributeCategoryIds { get; set; }
        //[RegularExpression(@"^[\s\S]*\S+[\s\S]*$", ErrorMessage = "Bạn cần nhập ký tự khác khoảng trắng!")]
        //  [RegularExpression(@"<([A-Z][a-z0-9]*)\b[^>]*>(.*?)</\1>", ErrorMessage = "Bạn cần nhập ký tự khác khoảng trắng!")]
        public string Name { get; set; }
        public string CodeRule { get; set; }
        /// <summary>
        /// Danh mục tài liệu nội bộ
        /// </summary>
        public Nullable<bool> IsInternal { get; set; }
        /// <summary>
        /// Danh mục tài liệu bên ngoài
        /// </summary>
        public Nullable<bool> IsExternal { get; set; }
        /// <summary>
        /// Danh mục tài liệu phân phối
        /// </summary>
        public Nullable<bool> IsDistribute { get; set; }

        public string DescriptionRule { get; set; }
        public string Content { get; set; }
        #endregion

        #region Rule
        public string NoteCategory { get; set; }

        public Guid? DocumentId { get; set; }

        /// <summary>
        /// Mã tài liệu
        /// </summary>
        public string DocumentCode { get; set; }

        //  <summary>
        /// giá trị: true - category chứa category con, false - ngược lại
        /// </summary>
        public bool IsParent
        {
            get;
            set;
        }
        /// <summary>
        /// Đếm số lượng tài liệu trong danh mục
        /// </summary>
        public int CountDocument { get; set; }

        public string NameFormat
        {
            get
            {
                var result = string.Format("{0} ({1})", Name, CountDocument);
                return result;

            }
        }
        public Guid? ExternalId { get; set; }
        #endregion
    }
}
