﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DocumentDistributeSuggestBO : BaseBO
    {
        #region Basic
        public Guid? DepartmentId { get; set; }
        public Guid? DocumentId { get; set; }
        public bool? IsSoft { get; set; }
        public bool? IsHard { get; set; }
        public DateTime? ReceiveAt { get; set; }
        public Guid? ApproveBy { get; set; }
        public DateTime? ApproveAt { get; set; }
        public DateTime? ApplyAt { get; set; }
        public bool? IsSend { get; set; }
        public bool? IsApprove { get; set; }
        public bool? IsAccept { get; set; }
        public bool? IsSendFromPromulgate { get; set; }
        public bool? IsComplete { get; set; }
        public int? Quantity { get; set; }
        public string ReasonOfSuggest { get; set; }
        public string ReasonOfReject { get; set; }
        public string Note { get; set; }
        public int? RoleType { get; set; }
        #endregion

        #region Rule
        public iDAS.Service.Common.Resource.DocumentProcessRole RoleView
        {
            get
            {
                var status = iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit;
                if (RoleType != null)
                {
                    switch ((int)RoleType)
                    {
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggester:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggester;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate;
                            break;
                        default:
                            break;
                    }
                }
                return status;
            }
        }
        public DocumentBO Document { get; set; }
        public EmployeeBO EmployeeSuggest { get; set; }
        public EmployeeBO EmployeeApproveBy { get; set; }
        /// <summary>
        /// Tên người phê duyệt
        /// </summary>
        public string ApproverName { get { return EmployeeApproveBy == null ? string.Empty : EmployeeApproveBy.Name; } }
        /// <summary>
        /// Ảnh người phê duyệt
        /// </summary>
        public string ApproveUrl { get { return EmployeeApproveBy == null ? string.Empty : EmployeeApproveBy.AvatarUrl; } }
        /// <summary>
        /// Chức danh người phê duyệt
        /// </summary>
        public string ApproveTitle { get; set; }
        public string DocumentCode { get { return Document == null ? string.Empty : Document.Code; } }
        public string DocumentName { get { return Document == null ? string.Empty : Document.Name; } }

        public Resource.ApproveStatus Status
        {
            get
            {
                var result = IsApprove == true ? (IsAccept == true ? Resource.ApproveStatus.Approve : Resource.ApproveStatus.Reject)
                    : IsSend == true ? Resource.ApproveStatus.Wait : Resource.ApproveStatus.New;
                return result;
            }
        }
        public string StatusText
        {
            get
            {
                var result = IsApprove == true ? (IsAccept == true ? Resource.ApproveStatusText.Approve : Resource.ApproveStatusText.Reject)
                    : IsSend == true ? Resource.ApproveStatusText.Wait : Resource.ApproveStatusText.New;
                return result;
            }
        }
        public bool IsCreater
        {
            get
            {
                return ApproveBy == UserId;
            }
        }

        public bool IsCheck
        {
            get
            {
                return CreateBy == UserId;
            }
        }
        /// <summary>
        /// Ngày ban hành
        /// </summary>
        public DateTime? PublishAt { get; set; }
        /// <summary>
        /// Lần ban hành
        /// </summary>
        public int? PublishTimes { get; set; }

        public string SuggestorName { get; set; }
        /// <summary>
        /// Dùng để phân biệt tài liệu nội bộ hoặc bên ngoài
        /// </summary>
        public bool? IsExternal { get; set; }
        public Guid? CategoryId { get; set; }
        #endregion


    }
}
