﻿using System;

namespace iDAS.Service
{
    public class DocumentRequestReviewSuggestApproveBO : BaseBO
    {
        #region Base
        public Guid? DocumentReviewSuggestId { get; set; }
        public string Contents { get; set; }
        public bool? IsSend { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }
        public bool? IsCancel { get; set; }

        public string Comments { get; set; }
        #endregion

        #region Rule
        public EmployeeBO EmployeeApprove { get; set; }
        public EmployeeBO EmpoyeeSuggest { get; set; }
        public DocumentBO Document { get; set; }
        public string Title { get { return "[" + (UpdateAt.HasValue ? UpdateAt.Value.ToString() : CreateAt.Value.ToString()) + "] Đề nghị xem xét tài liệu"; } }
        public string RoleText
        {
            get
            {
                var status = iDAS.Service.Common.Resource.DocumentProcessRoleText.ApprovalWriteAndEdit;
                if (CreateBy.HasValue && CreateBy.Value == UserId)
                    status = "Người đề nghị";
                return status;
            }
        }
        public string ResultText
        {
            get
            {
                if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                    return "<span style='color:blue'>Đạt</span>";
                else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                    return "<span style='color:red'>Không đạt</span>";
                else if (IsCancel.HasValue && IsCancel.Value)
                    return "<span style='color:black'>Hủy</span>";
                else
                    return "<span style='color:green'>Chờ duyệt</span>";
            }
        }
        #endregion
    }
}
