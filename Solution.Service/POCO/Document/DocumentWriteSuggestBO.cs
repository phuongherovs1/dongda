﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DocumentWriteSuggestBO : BaseBO
    {
        #region Base
        public Guid? DocumentId { get; set; }
        public string Name { get; set; }
        public string ReasonOfSuggest { get; set; }
        public DateTime? EstimateAt { get; set; }

        public Guid? EmployeeAssignId { get; set; }

        public Guid? EmployeeWriteId { get; set; }

        public DateTime? SuggestAt { get; set; }

        public Guid? SuggestBy { get; set; }

        public int? StatusType { get; set; }

        public int? Order { get; set; }
        #endregion
        #region Rule
        public DocumentEmployeeBO EmployeeSuggestBy { get; set; }
        public string Description { get; set; }
        public DateTime? ApproveAt { get; set; }
        public string ApproveNote { get; set; }
        public string Status
        {
            get
            {
                var result = Resource.DocumentSuggestStatus.New;
                switch (StatusType)
                {
                    case (int)Resource.DocumentSuggestStatusType.New:
                        result = Resource.DocumentSuggestStatus.New;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.Review:
                        result = Resource.DocumentSuggestStatus.Review;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.Approve:
                        result = Resource.DocumentSuggestStatus.Approve;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.Destroy:
                        result = Resource.DocumentSuggestStatus.Destroy;
                        break;
                }
                return result;
            }
        }
        #endregion
    }
}
