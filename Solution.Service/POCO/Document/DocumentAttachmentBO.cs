using System;

namespace iDAS.Service
{
    public class DocumentAttachmentBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> DocumentId { get; set; }
        public Nullable<System.Guid> FileId { get; set; }
        #endregion

        #region Rule

        #endregion
    }
}
