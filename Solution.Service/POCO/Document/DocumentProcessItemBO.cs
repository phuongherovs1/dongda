﻿using System;

namespace iDAS.Service
{
    public class DocumentProcessItemBO
    {
        #region Rule
        public Guid ObjectId { get; set; }
        public string Name { get; set; }
        public bool IsTitle { get; set; }
        public int Order { get; set; }
        public int RoleType { get; set; }
        #endregion

    }
}
