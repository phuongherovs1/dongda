﻿using System;

namespace iDAS.Service
{
    public class DocumentResponsibilityBO : BaseBO
    {
        #region Base
        public Guid? DepartmentId { get; set; }
        public Guid? DocumentId { get; set; }
        public Guid? ExtendId { get; set; }
        public int? RoleType { get; set; }
        public bool? IsNew { get; set; }
        public bool? IsApprove { get; set; }
        public bool? IsAccept { get; set; }
        public bool? IsComplete { get; set; }
        public bool? IsLock { get; set; }
        public bool? IsCancel { get; set; }
        public string ContentSuggest { get; set; }
        public string ApproveNote { get; set; }
        public Guid? PerformBy { get; set; }
        public DateTime? PerformAt { get; set; }
        public DateTime? PublishDate { get; set; }
        public DateTime? ApplyDate { get; set; }
        public int? Order { get; set; }
        public Guid? SendTo { get; set; }
        public int? SenderRoleType { get; set; }
        public int? SenderOrder { get; set; }
        #endregion

        #region Rule
        public bool IsCreater
        {
            get
            {
                return CreateBy == UserId;
            }
        }
        public bool IsPerform
        {
            get
            {
                return PerformBy == UserId;
            }
        }
        public DocumentArchiveBO Archive { get; set; }
        public DocumentEmployeeBO EmployeeRecive { get; set; }
        public EmployeeBO EmployeePerformBy { get; set; }
        public EmployeeBO EmployeeSend { get; set; }
        public EmployeeBO EmployeeChange { get; set; }
        public DocumentBO Document { get; set; }

        public string DepartmentName { get; set; }

        public iDAS.Service.Common.Resource.DocumentProcessRole RoleView
        {
            get
            {
                var status = iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit;
                if (RoleType != null)
                {
                    switch ((int)RoleType)
                    {
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Archiver:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Archiver;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.AssignmentWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.AssignmentWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Check:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Check;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyCreater;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Distributer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Distributer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestApproval:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestApproval;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggester:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggester;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestReviewer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestReviewer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Write:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Write;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeRequester:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeRequester;
                            break;
                        default:
                            break;
                    }
                }
                return status;
            }
        }
        public string RoleText
        {
            get
            {
                var status = string.Empty;
                if (RoleType != null)
                {
                    switch ((int)RoleType)
                    {
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.ApprovalWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.ApproveDocument;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Archiver:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Archiver;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.AssignmentWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.AssignmentWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Check:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Check;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DestroyApproval;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DestroyBillCreater;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DestroyCreater;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DestroyReviewer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Distributer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Distributer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestApproval:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DistributeSuggestApproval;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggester:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DistributeSuggester;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestReviewer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DistributeSuggestReviewer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Promulgate;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.ReviewWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.SuggesterWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Write:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Write;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeRequester:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DistributeRequester;
                            break;
                        default:
                            break;
                    }
                }
                return status;
            }
        }
        public string ResultText
        {
            get
            {
                var result = string.Empty;
                if (RoleType != null)
                {
                    switch ((int)RoleType)
                    {
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit:
                            if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                                result = "Đạt";
                            else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                                result = "Không đạt";
                            else
                                result = "Chờ duyệt";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument:
                            if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                                result = "Đạt";
                            else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                                result = "Không đạt";
                            else
                                result = "Chờ duyệt";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Archiver:
                            if (IsComplete == true)
                                result = "Đã lưu trữ";
                            else
                                result = "Chờ lưu trữ";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.AssignmentWriteAndEdit:
                            if (IsComplete == true)
                                result = "Đã phân công";
                            else
                                result = "Chờ phân công";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Check:
                            if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                                result = "Đạt";
                            else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                                result = "Không đạt";
                            else
                                result = "Chờ kiểm tra";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval:
                            if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                                result = "Đạt";
                            else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                                result = "Không đạt";
                            else
                                result = "Chờ phê duyệt";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater:
                            if (IsComplete == true)
                                result = "Đã hoàn thành";
                            else
                                result = "Chờ tạo biên bản";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyCreater:
                            result = string.Empty;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer:
                            if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                                result = "Đạt";
                            else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                                result = "Không đạt";
                            else
                                result = "Chờ xem xét";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Distributer:
                            result = string.Empty;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestApproval:
                            if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                                result = "Đạt";
                            else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                                result = "Không đạt";
                            else
                                result = "Chờ phê duyệt";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggester:
                            result = string.Empty;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestReviewer:
                            if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                                result = "Đạt";
                            else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                                result = "Không đạt";
                            else
                                result = "Chờ xem xét";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate:
                            if (IsComplete == true)
                                result = "Đã ban hành";
                            else
                                result = "Chờ ban hành";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit:
                            if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                                result = "Đạt";
                            else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                                result = "Không đạt";
                            else
                                result = "Chờ xem xét";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit:
                            result = IsComplete == true ? "Đã gửi" : string.Empty;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Write:
                            if (IsComplete == true)
                                result = "Đã soạn thảo";
                            else
                                result = "Chờ soạn thảo";
                            break;
                        default:
                            break;
                    }
                }
                return result;
            }
        }


        #endregion
    }
}
