﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public class DocumentDestroySuggestBO : BaseBO
    {
        #region Base
        public string ReasonOfReject { get; set; }
        public string ReasonOfSuggest { get; set; }
        public Nullable<System.DateTime> DestroyStartAt { get; set; }
        public Nullable<System.DateTime> DestroyEstimateAt { get; set; }
        public string Type { get; set; }
        public int? StatusType { get; set; }
        #endregion

        #region Rule
        public Guid? DepartmentId { get; set; }
        public Nullable<System.Guid> ApproveBy { get; set; }
        public int? RoleType { get; set; }
        public string GroupSummaryName { get { return string.Format("Đề nghị hủy tài liệu ngày - {0} ({1} tài liệu)", String.Format("{0:dd/MM/yyyy HH:mm}", CreateAt), CountDocument); } }
        public string DocumentName { get; set; }
        public DocumentBO Document { get; set; }
        public Guid DocumentId { get; set; }
        /// <summary>
        /// Danh sách DocumentId chi tiết của đề nghị
        /// </summary>
        public IEnumerable<Guid> DocumentIds { get; set; }

        public bool? IsNew { get; set; }
        public bool? IsApprove { get; set; }
        public string DepartmentName { get; set; }
        public bool? IsAccept { get; set; }

        /// <summary>
        /// Trạng thái
        /// </summary>
        public string StatusText
        {
            get
            {
                var result = Resource.DocumentSuggestStatus.New;
                switch (StatusType)
                {
                    case (int)Resource.DocumentSuggestStatusType.New:
                        result = Resource.DocumentSuggestStatus.New;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.Waitcheck:
                        result = Resource.DocumentSuggestStatus.Waitcheck;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.Review:
                        result = Resource.DocumentSuggestStatus.Review;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.Approve:
                        result = Resource.DocumentSuggestStatus.Approve;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.WaitBillCreate:
                        result = Resource.DocumentSuggestStatus.WaitBillCreate;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.Destroy:
                        result = Resource.DocumentSuggestStatus.Destroy;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.AcceptFail:
                        result = Resource.DocumentSuggestStatus.AcceptFail;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.SendWait:
                        result = Resource.DocumentSuggestStatus.SendWait;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.BillCreated:
                        result = Resource.DocumentSuggestStatus.BillCreated;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.ApproveFail:
                        result = Resource.DocumentSuggestStatus.ApproveFail;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.ApproveAccept:
                        result = Resource.DocumentSuggestStatus.ApproveAccept;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.WaitDestroy:
                        result = Resource.DocumentSuggestStatus.WaitDestroy;
                        break;
                    case (int)Resource.DocumentSuggestStatusType.Destroyed:
                        result = Resource.DocumentSuggestStatus.Destroyed;
                        break;
                }
                return result;
            }
        }
        /// <summary>
        /// Người đề nghị
        /// </summary>
        public string SuggestorName { get; set; }
        /// <summary>
        /// Ảnh đại diện người đề nghị
        /// </summary>
        public string SuggestorUrl { get; set; }
        /// <summary>
        /// Chức danh người đề nghị
        /// </summary>
        public string SuggestorTitle { get; set; }
        public EmployeeBO EmployeeApproveBy { get; set; }
        public EmployeeBO EmployeeCreateBy { get; set; }
        public DocumentEmployeeBO EmployeeApprove { get; set; }

        public DocumentEmployeeBO EmployeeApproveDetail { get; set; }

        /// <summary>
        /// Tên người phê duyệt
        /// </summary>
        public string ApproverName { get; set; }
        /// <summary>
        /// Ảnh đại diện người phê duyệt
        /// </summary>
        public string ApproveUrl { get; set; }
        /// <summary>
        /// Chức danh người phê duyệt
        /// </summary>
        public string ApproveTitle { get; set; }
        /// <summary>
        /// Vai trò
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        /// Đếm số tài liệu trong đề nghị hủy
        /// </summary>
        public int? CountDocument { get; set; }

        /// <summary>
        /// True: nếu người tạo đề nghị hủy từ danh sách tài liệu, !=True: nếu tạo đề nghị hủy từ 1 tài liệu
        /// </summary>
        public bool? IsArchiver { get; set; }
        /// <summary>
        /// False: khi chỉ lưu đề nghị, True: khi gửi đề nghị
        /// </summary>
        public bool Temp { get; set; }

        /// <summary>
        /// Hiển thị hoặc ẩn button Thu hồi
        /// </summary>
        public bool? IsShowButtonRevert { get; set; }
        #endregion
    }
}
