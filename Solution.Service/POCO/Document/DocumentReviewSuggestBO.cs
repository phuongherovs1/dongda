﻿using System;

namespace iDAS.Service
{
    public class DocumentReviewSuggestBO : BaseBO
    {
        #region Base
        public Guid? RequestId { get; set; }
        public Guid? ApproveBy { get; set; }
        public DateTime? ApproveAt { get; set; }
        public bool? IsApprove { get; set; }
        public bool? IsAccept { get; set; }
        public string ReasonOfReject { get; set; }
        public bool? IsSend { get; set; }
        public DateTime? SendAt { get; set; }
        public string Note { get; set; }
        #endregion

        #region Rule
        public DocumentRequestBO Request { get; set; }
        public DocumentBO Document { get; set; }
        public string DocumentCode { get { return Document == null ? string.Empty : Document.Code; } }
        public string DocumentName { get { return Document == null ? string.Empty : "[" + Document.Code + "]" + Document.Name; } }
        /// <summary>
        /// Người kiểm duyệt
        /// </summary>
        public EmployeeBO EmployeeApproveBy { get; set; }
        /// <summary>
        /// Tên người duyệt 
        /// </summary>
        public string ApproverName { get { return EmployeeApproveBy == null ? string.Empty : EmployeeApproveBy.Name; } }
        public string StatusText
        {
            get
            {
                var status = IsApprove == null && IsAccept == null && IsSend == null ? Common.Resource.ApproveStatusText.New
                    : IsApprove == true ? (IsAccept == true ? Common.Resource.ApproveStatusText.Approve : Common.Resource.ApproveStatusText.Reject)
                    : IsSend == true ? Common.Resource.ApproveStatusText.Wait
                    : string.Empty;
                return status;
            }
        }
        /// <summary>
        /// File đính kèm
        /// </summary>
        public FileUploadBO FileAttachs { get { return Document == null ? null : Document.FileAttachs; } }
        /// <summary>
        /// Tên người duyệt 
        /// </summary>
        public string Approver { get; set; }
        /// <summary>
        /// Ảnh người kiểm tra
        /// </summary>
        public string ApproverAvatarUrl { get; set; }
        public string SuggestorName { get; set; }

        /// <summary>
        /// checkbox người soạn thảo thay thế trên form duyệt đề nghị kiểm duyệt
        /// </summary>
        public bool? WriterReplace { get; set; }
        /// <summary>
        /// Người soạn thảo cũ
        /// </summary>
        public EmployeeBO EmployeeWriteBy { get; set; }
        /// <summary>
        /// Chọn người soạn thảo thay thế
        /// </summary>
        public EmployeeBO EmployeePerformBy { get; set; }
        /// <summary>
        /// Kết quả phê duyệt
        /// </summary>
        public string ResultReview { get; set; }
        /// <summary>
        /// Trách nhiệm
        /// </summary>
        public string TextRole { get; set; }
        #endregion
    }
}
