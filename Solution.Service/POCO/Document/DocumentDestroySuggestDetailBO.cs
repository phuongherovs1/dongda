﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DocumentDestroySuggestDetailBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> DestroySuggestId { get; set; }
        public Nullable<System.Guid> DocumentId { get; set; }
        public string Reason { get; set; }
        public Nullable<System.Boolean> IsDestroy { get; set; }
        #endregion

        #region Rule
        public string DocumentName { get; set; }
        public string DocumentCode { get; set; }
        public DocumentBO Document { get; set; }

        public string GroupSummaryName { get; set; }
        /// <summary>
        /// Thời gian bắt đầu đề nghị
        /// </summary>
        public DateTime? DestroyStartAt { get; set; }
        /// <summary>
        /// Thời gian kết thúc đề nghị
        /// </summary>
        public DateTime? DestroyEstimateAt { get; set; }
        /// <summary>
        /// Trạng thái
        /// </summary>
        public string StatusText { get; set; }
        /// <summary>
        /// Ngày tạo đề nghị
        /// </summary>
        public DateTime? CreateSuggestAt { get; set; }

        /// <summary>
        /// Hình thức hủy
        /// </summary>
        public string DestroyType { get; set; }

        /// <summary>
        /// Trạng thái
        /// </summary>
        public string DestroyStatusText
        {
            get
            {
                var result = IsDestroy == true ? Resource.DocumentStatusText.Destroy
                    : Resource.DocumentStatusText.NotDestroy;
                return result;
            }
        }
        #endregion
    }
}
