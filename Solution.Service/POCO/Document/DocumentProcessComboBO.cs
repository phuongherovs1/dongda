﻿namespace iDAS.Service
{
    public class DocumentProcessComboBO
    {
        #region Rule
        public int ID { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public int ProcessType { get; set; }
        #endregion
    }
}
