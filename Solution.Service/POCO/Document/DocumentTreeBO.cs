﻿using System;

namespace iDAS.Service
{
    public class DocumentTreeBO
    {
        #region Base

        #endregion

        #region Rule
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public bool IsCategory { get; set; }
        public string Name { get; set; }
        public bool Leaf { get; set; }
        #endregion

    }
}
