﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public class DocumentSuggestBO : BaseBO
    {
        public Guid? DepartmentId { get; set; }

        public string ReasonOfSuggest { get; set; }

        public DateTime? SuggestAt { get; set; }

        public Guid? SuggestBy { get; set; }

        public int? SuggestType { get; set; }

        public int? StatusType { get; set; }

        public int? Order { get; set; }
        #region extend
        // Nhân sự đề nghị
        public EmployeeBO EmployeeSuggest { get; set; }
        // Nhân sự gửi xem xét
        public DocumentEmployeeBO EmployeeSendTo { get; set; }
        // Vai trò
        public int? RoleType { get; set; }
        /// <summary>
        /// Tài liệu kèm theo đề nghị
        /// </summary>
        public DocumentBO Document { get; set; }

        #endregion
       
    }
}
