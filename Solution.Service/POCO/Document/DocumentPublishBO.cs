﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DocumentPublishBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> DocumentId { get; set; }
        public Nullable<System.Guid> RequestId { get; set; }
        public Nullable<bool> IsPublish { get; set; }
        public Nullable<System.Guid> PublishBy { get; set; }
        public Nullable<System.DateTime> PublishAt { get; set; }
        public int? QuantityPublish { get; set; }
        public Nullable<bool> IsObsolete { get; set; }
        public Nullable<bool> CheckCategory { get; set; }
        public Nullable<System.DateTime> ObsoleteAt { get; set; }
        public string ReasonOfObsolete { get; set; }
        public Nullable<System.DateTime> ApplyAt { get; set; }
        public Nullable<System.DateTime> ExpireAt { get; set; }
        public string Note { get; set; }
        public string RequestPublish { get; set; }
        public string NameSecurity { get; set; }
        public string Color { get; set; }

        #endregion

        #region Rule
        public DocumentBO Document { get; set; }
        public DocumentResponsibilityBO DocumentResponsibility { get; set; }
        /// <summary>
        /// Kiểm tra tài liệu đã được lưu trữ hay chưa
        /// </summary>
        /// 
        public string NameDocument { get { return Document == null ? string.Empty : "[" + Document.Code + "] " + Document.Name; } }
        public string SecurityName { get { return Document == null ? string.Empty : Document.SecurityName; } }
        public string SecurityColor { get { return Document == null ? string.Empty : Document.SecurityColor; } }
        public string RoleText { get { return DocumentResponsibility == null ? string.Empty : DocumentResponsibility.RoleText; } }
        public string ResultText { get { return DocumentResponsibility == null ? string.Empty : DocumentResponsibility.ResultText; } }
        public DocumentEmployeeBO EmployeeRecive { get { return DocumentResponsibility == null ? null : DocumentResponsibility.EmployeeRecive; } }
        /// <summary>
        /// Mã tài liệu
        /// </summary>
        public string CodeDocument { get { return Document == null ? string.Empty : Document.Code; } }
        public bool IsArchive { get; set; }
        public string DocumentCode { get; set; }
        public string DocumentName { get; set; }
        public Guid? DepartmentId { get; set; }
        public string DocumentSecurityName { get; set; }
        public string DocumentSecurityColor { get; set; }
        public string DocumentReversion { get; set; }
        public string Version { get; set; }
        public string DocumentCategoryName { get; set; }
        /// <summary>
        /// Loại tài liệu: true: nội bộ, false: bên ngoài
        /// </summary>
        public bool? IsInternal { get; set; }
        /// <summary>
        /// Loại tài liệu
        /// </summary>
        public string DocumentType { get { return Document != null ? Document.DocumentType : string.Empty; } }
        /// <summary>
        /// Hình thức lưu trữ
        /// true: bản mềm, false: bản cứng
        /// </summary>
        public bool IsSoft { get; set; }
        public FileUploadBO FileAttachs { get; set; }
        /// <summary>
        /// Người phê duyệt
        /// </summary>
        public EmployeeBO EmployeeApproveBy { get; set; }
        /// <summary>
        /// Người soạn thảo
        /// </summary>
        public EmployeeBO Writer { get; set; }
        /// <summary>
        /// Ngày soạn thảo
        /// </summary>
        public Nullable<System.DateTime> WriteAt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Nullable<System.DateTime> ApproveAt { get; set; }
        public string TitleEmploy { get; set; }
        /// <summary>
        /// Người gửi
        /// </summary>
        public string Receiver { get { return Publisher == null ? null : Publisher.Name; } }
        /// <summary>
        /// Anh
        /// </summary>
        public string AvatarUrl { get { return Publisher == null ? null : Publisher.AvatarUrl; } }

        /// <summary>
        /// Người ban hành
        /// </summary>
        public EmployeeBO Publisher { get; set; }

        /// <summary>
        /// Trạng thái
        /// </summary>
        public Resource.DocumentStatus Status
        {
            get
            {
                var status = IsObsolete == true ? Resource.DocumentStatus.Obsolate
                    : (ExpireAt.HasValue && ExpireAt.Value > DateTime.Now) ? Resource.DocumentStatus.Publish
                    : (ExpireAt.HasValue && ExpireAt.Value < DateTime.Now) ? Resource.DocumentStatus.Expire
                    : Resource.DocumentStatus.New;
                return status;
            }
        }

        /// <summary>
        /// Trạng thái
        /// </summary>
        public string StatusText
        {
            get
            {
                var status = IsObsolete == true ? Resource.DocumentStatusText.Obsolate
                     : (PublishAt.HasValue && PublishAt.Value <= DateTime.Now) && (DateTime.Now < ApplyAt.Value && ApplyAt.HasValue) ? Resource.DocumentStatusText.Publish
                     : (ApplyAt.HasValue && ApplyAt.Value > DateTime.Now) ? Resource.DocumentStatusText.ApproveComplete
                     : (ApplyAt.HasValue && ApplyAt.Value <= DateTime.Now) && (DateTime.Now < ExpireAt.Value && ExpireAt.HasValue) ? Resource.DocumentStatusText.ApplyAt
                     : (ExpireAt.HasValue && ExpireAt.Value < DateTime.Now) ? Resource.DocumentStatusText.Expire

                     : string.Empty;
                return status;
            }
        }

        /// <summary>
        /// True: khi tài liệu đã được thêm trong đề nghị hủy
        /// </summary>
        public bool IsCheck { get; set; }

        /// <summary>
        /// Check trạng thái yêu cầu đã thực hiện hay chưa
        /// </summary>
        public bool? IsCompile { get; set; }
        #endregion
    }
}
