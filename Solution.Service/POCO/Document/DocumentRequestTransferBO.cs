using System;

namespace iDAS.Service
{
    public class DocumentRequestTransferBO : BaseBO
    {
        #region Base
        public Guid? RequestId { get; set; }
        public string Note { get; set; }
        public Guid? TransferTo { get; set; }
        #endregion

        #region Rule
        public EmployeeBO EmployeeTransferTo { get; set; }
        #endregion

    }
}
