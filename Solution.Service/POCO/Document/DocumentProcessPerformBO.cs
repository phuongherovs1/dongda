﻿using System.Collections.Generic;

namespace iDAS.Service
{
    public class DocumentProcessPerformBO
    {
        public bool IsExits { get; set; }
        public List<DocumentProcessItemBO> Items { get; set; }
    }
}
