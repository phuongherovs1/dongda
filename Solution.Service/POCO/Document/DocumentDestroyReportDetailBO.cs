﻿using System;

namespace iDAS.Service
{
    public class DocumentDestroyReportDetailBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> DocumentDestroyReportId { get; set; }
        public Nullable<System.Guid> EmployeeId { get; set; }
        public Nullable<System.Boolean> IsConfirm { get; set; }
        #endregion

        #region Rule
        /// <summary>
        /// Phòng ban của nhân viên
        /// </summary>
        public string DepartmentName { get; set; }
        /// <summary>
        /// Tên nhân viên
        /// </summary>
        public string EmployeeName { get; set; }
        /// <summary>
        /// Ảnh nhân viên
        /// </summary>
        public string EmployeeUrl { get; set; }
        /// <summary>
        /// Chức vụ nhân viên
        /// </summary>
        public string Title { get; set; }
        #endregion
    }
}
