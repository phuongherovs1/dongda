﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DocumentBO : BaseBO
    {
        #region Base
        public Guid? ParentId { get; set; }
        public Guid? CategoryId { get; set; }
        public Guid? DepartmentId { get; set; }
        public string Code { get; set; }
        public string DistributeCategoryIds { get; set; }
        public string Name { get; set; }
        public Guid? SecurityId { get; set; }
        public string Version { get; set; }
        public string Revision { get; set; }
        public bool? IsSoft { get; set; }
        public bool? IsHard { get; set; }
        public bool? IsNew { get; set; }
        public bool? IsExternal { get; set; }

        //public bool? IsWaitCompilation { get; set; }
        //public bool? IsComplation { get; set; }
        public bool? IsWaitWrite { get; set; }
        public bool? IsWrite { get; set; }
        public bool? IsWaitReview { get; set; }
        public bool? IsReviewApprove { get; set; }
        public bool? IsReviewAccept { get; set; }
        public bool? IsWaitPublish { get; set; }
        public bool? IsPublishApprove { get; set; }
        public bool? IsPublishAccept { get; set; }
        /// <summary>
        /// Nơi ban hành
        /// </summary>
        public string PublishFrom { get; set; }
        private bool? isPublish;
        /// <summary>
        /// Trạng thái ban hành
        /// </summary>
        public bool? IsPublish
        {
            get { return isPublish; }
            set { isPublish = value; }
        }

        public DateTime? PublishAt { get; set; }
        public DateTime? ApplyAt { get; set; }

        public bool? IsApply { get; set; }

        public bool? IsAccept { get; set; }

        private bool? isExpire;
        /// <summary>
        /// Trạng thái hết hiệu lực
        /// </summary>
        public bool? IsExpire
        {
            get { return isExpire; }
            set { isExpire = value; }
        }


        public DateTime? ExpireAt { get; set; }

        private bool? isObsolate;
        /// <summary>
        /// Trạng thái lỗi thời
        /// </summary>
        public bool? IsObsolate
        {
            get { return isObsolate; }
            set { isObsolate = value; }
        }
        public DateTime? ObsolateAt { get; set; }

        private bool? isDestroy;
        /// <summary>
        /// Trạng thái hủy
        /// </summary>
        public bool? IsDestroy
        {
            get { return isDestroy; }
            set { isDestroy = value; }
        }
        public DateTime? DestroyAt { get; set; }

        public Guid? WriteBy { get; set; }

        public DateTime? WriteAt { get; set; }

        public string Note { get; set; }
        public string NoteCategory { get; set; }
        /// <summary>
        /// Tài liệu hiện hành
        /// </summary>
        public bool? IsExist { get; set; }

        public bool? IsSuggestWite { get; set; }

        public bool? IsRequestWrite { get; set; }
        /// <summary>
        /// Trích yếu nội dung
        /// </summary>
        public string ShortContent { get; set; }

        public string PublishExternalName { get; set; }


        #endregion

        #region Rule
        public int CountReferences { get; set; }
        public string ObjectName { get; set; }
        public DocumentArchiveBO Archive { get; set; }
        /// <summary>
        /// Phòng ban
        /// </summary>
        public DocumentCategoryBO Category { get; set; }

        public string CategoryStatus
        {
            get
            {
                var result = IsExternal == true ? "Bên ngoài"
                    : "Nội bộ";
                return result;
            }
        }

        public Resource.DocumentCategoryType CategoryType { get; set; }
        public string DepartmentName { get; set; }
        public string CategoryName { get; set; }

        /// <summary>
        /// Kiểm tra tài liệu đã được lưu trữ hay chưa
        /// </summary>
        public bool IsArchive { get; set; }
        public DocumentSecurityBO Security { get; set; }
        public string SecurityName { get { return Security == null ? string.Empty : Security.Name; } }
        public string SecurityColor { get { return Security == null ? string.Empty : Security.Color; } }

        public EmployeeBO EmployeeWriteBy { get; set; }
        public EmployeeBO EmployeePerformBy { get; set; }
        public EmployeeBO EmployeePublishBy { get; set; }
        public string Writer { get; set; }
        public Resource.EDocumentStatus Status
        {
            get
            {
                var status = IsDestroy == true ? Resource.EDocumentStatus.Destroy : IsObsolate == true ? Resource.EDocumentStatus.Obsolate
                     : IsExpire == true ? Resource.EDocumentStatus.Expire
                     : IsPublish == true ? Resource.EDocumentStatus.Publish
                     : IsWaitPublish == true ? Resource.EDocumentStatus.WaitPublish
                     : IsNew == true ? Resource.EDocumentStatus.New
                     : Resource.EDocumentStatus.New;
                return status;
            }
        }

        /// <summary>
        /// Dữ liệu binding từ form
        /// </summary>
        public bool IsFormUpdate
        {
            get;
            set;
        }
        /// <summary>
        /// Chọn trạng thái tài liệu
        /// </summary>
        public int? SelectStatus
        {
            get
            {
                var status = IsDestroy == true ? (int)Resource.EDocumentStatus.Destroy : IsObsolate == true ? (int)Resource.EDocumentStatus.Obsolate
                    : IsExpire == true ? (int)Resource.EDocumentStatus.Expire
                    : IsPublish == true ? (int)Resource.EDocumentStatus.Publish
                    : IsWaitPublish == true ? (int)Resource.EDocumentStatus.WaitPublish
                    : IsNew == true ? (int)Resource.EDocumentStatus.New
                    : (int)Resource.EDocumentStatus.New;
                return status;
            }
            set
            {
                switch (value)
                {
                    case (int)Resource.EDocumentStatus.Destroy:
                        IsDestroy = true;
                        break;
                    case (int)Resource.EDocumentStatus.Obsolate:
                        IsObsolate = true;
                        break;
                    case (int)Resource.EDocumentStatus.Expire:
                        IsExpire = true;
                        break;
                    case (int)Resource.EDocumentStatus.Publish:
                        IsPublish = true;
                        break;
                    case (int)Resource.EDocumentStatus.WaitPublish:
                        IsWaitPublish = true;
                        break;
                    case (int)Resource.EDocumentStatus.New:
                        IsNew = true;
                        break;
                    default:
                        break;
                }

            }
        }
        public string StatusText
        {
            get
            {
                var status = IsDestroy == true ? Resource.EDocumentStatusText.Destroy : IsObsolate == true ? Resource.EDocumentStatusText.Obsolate
                     : IsExpire == true ? Resource.EDocumentStatusText.Expire
                     : IsPublish == true ? Resource.EDocumentStatusText.Publish
                     : IsWaitPublish == true ? Resource.EDocumentStatusText.WaitPublish
                     : IsNew == true ? Resource.EDocumentStatusText.New
                     : Resource.EDocumentStatusText.New;
                return status;
            }
        }
        public FileUploadBO FileAttachs { get; set; }
        public bool IsReferenceExits { get; set; }
        /// <summary>
        /// Lần ban hành
        /// </summary>
        public string PublishTime { get; set; }
        public string PublishStatus { get; set; }
        /// <summary>
        /// Nội dung yêu cầu sửa đổi
        /// </summary>
        public string AdjustNote { get; set; }
        /// <summary>
        /// Yêu cầu sửa đổi từ ngày
        /// </summary>
        public DateTime? AdjustStartAt { get; set; }
        /// <summary>
        /// Yêu cầu sửa đổi đến ngày
        /// </summary>
        public DateTime? AdjustEndAt { get; set; }
        /// <summary>
        /// Id của yêu cầu sửa đổi
        /// </summary>
        public Guid RequestId { get; set; }
        /// <summary>
        /// Người thực hiện yêu cầu sửa đổi
        /// </summary>
        public EmployeeBO EmployeeAdjustPerform { get; set; }
        /// <summary>
        /// Dung de phan biet tai lieu phan phoi 
        /// </summary>
        public bool Check { get; set; }
        public string StatusCheck { get { return Check == true ? "Tài liệu phân phối" : "Tài liệu nội bộ"; } }

        /// <summary>
        /// Loại tài liệu: IsExternal: True: Bên ngoài, !=True: Nội bộ
        /// </summary>
        public string DocumentType
        {
            get { return IsExternal == true ? "Bên ngoài" : "Nội bộ"; }
        }

        public string StatusApply
        {
            get
            {
                var status = string.Empty;
                if (IsExternal == true)
                {
                    if (IsApply == null && IsAccept == null)
                        status = "Chưa duyệt";
                    if (IsApply == true && IsAccept == null)
                        status = "Chờ duyệt";
                    if (IsApply == true && IsAccept == true)
                        status = "Duyệt";
                    if (IsApply == true && IsAccept == false)
                        status = "Không duyệt";
                }
                return status;
            }
        }
        #endregion
    }
}
