using System;

namespace iDAS.Service
{
    public class DocumentArchiveBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> DocumentId { get; set; }
        public Nullable<int> Quantity { get; set; }
        public string Position { get; set; }
        public Nullable<int> DurationMonth { get; set; }
        public Nullable<System.DateTime> StartAt { get; set; }
        public string Note { get; set; }
        #endregion

        #region Rule
        public DocumentBO Document { get; set; }
        #endregion

    }
}
