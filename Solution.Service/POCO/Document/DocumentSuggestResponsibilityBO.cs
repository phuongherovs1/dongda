﻿using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iDAS.Service
{
    public class DocumentSuggestResponsibilityBO : BaseBO
    {
        public DocumentSuggestBO DocumentSuggest { get; set; }
        public Guid? DocumentSuggestId { get; set; }
        public int? RoleType { get; set; }
        public bool? IsNew { get; set; }
        public bool? IsApprove { get; set; }
        public bool? IsAccept { get; set; }
        public bool? IsComplete { get; set; }
        public string ContentSuggest { get; set; }
        public string ApproveNote { get; set; }
        public Guid? ApproveBy { get; set; }
        public EmployeeBO EmployeeApprove { get; set; }
        public EmployeeBO EmployeeCreate { get; set; }

        public DateTime? ApproveAt { get; set; }
        public int? Order { get; set; }

        public string RoleText
        {
            get
            {
                var status = string.Empty;
                if (RoleType != null)
                {
                    switch ((int)RoleType)
                    {
                        //case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Approve:
                        //    status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Approve;
                        //    break;
                        //case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Assignment:
                        //    status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Assignment;
                        //    break;
                        //case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Write:
                        //    status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Write;
                        //    break;
                        //case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Review:
                        //    status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Review;
                        //    break;
                        //case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Check:
                        //    status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Check;
                        //    break;
                        //case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Distributer:
                        //    status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Distribute;
                        //    break;
                        //case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate:
                        //    status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Promulgate;
                        //    break;
                        //case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Archiver:
                        //    status = iDAS.Service.Common.Resource.DocumentProcessRoleText.Save;
                        //    break;
                        default:
                            break;
                    }
                }
                return status;
            }
        }
        public string ResultText
        {
            get
            {
                if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                    return "Đạt";
                else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                    return "Không đạt";
                else
                    return "Chờ duyệt";
            }
        }
    }
}
