﻿using System;

namespace iDAS.Service
{
    public class DocumentReferenceAttachmentBO : BaseBO
    {
        #region Base
        public Guid? FileId { get; set; }
        public Guid? ReferenceId { get; set; }
        #endregion

        #region Rule

        #endregion
    }
}
