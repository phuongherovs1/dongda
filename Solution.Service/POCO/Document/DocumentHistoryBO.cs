using System;

namespace iDAS.Service
{
    public class DocumentHistoryBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> DocumentId { get; set; }
        public string AuditTrail { get; set; }
        public Nullable<short> Action { get; set; }
        public int Time { get; set; }
        public string Description { get; set; }
        #endregion

        #region Rule
        public EmployeeBO EmployeeCreateBy { get; set; }
        #endregion

    }
}
