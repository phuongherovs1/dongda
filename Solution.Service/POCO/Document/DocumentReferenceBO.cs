﻿using System;

namespace iDAS.Service
{
    public class DocumentReferenceBO : BaseBO
    {
        #region Base
        public Guid? DocumentId { get; set; }

        public Guid? ReferenceId { get; set; }

        public string ReferenceName { get; set; }

        public string ReferenceCode { get; set; }

        public Guid? ReferenceType { get; set; }
        public string Note { get; set; }
        #endregion

        #region Rule
        public string ReferenceNameFormat
        {
            get
            {
                var nameFormat = string.IsNullOrEmpty(ReferenceCode) ? ReferenceName : string.Format("[{0}] {1}", ReferenceCode, ReferenceName);
                return nameFormat;
            }
        }
        public DocumentBO DocumentReference { get; set; }
        public string ReferenceTypeName { get; set; }

        /// <summary>
        /// Danh sách file đính kèm
        /// </summary>
        public FileUploadBO FileAttachs { get; set; }
        #endregion
    }

    public class DocumentReferenceViewBO
    {
        #region Rule
        public string Id { get; set; }

        public string DocumentId { get; set; }

        public string ReferenceId { get; set; }

        public string ReferenceName { get; set; }

        public string ReferenceCode { get; set; }

        public string ReferenceType { get; set; }

        public string Note { get; set; }
        /// <summary>
        /// Danh sách file đính kèm
        /// </summary>
        public FileUploadBO FileAttachs { get; set; }

        #endregion
    }
}
