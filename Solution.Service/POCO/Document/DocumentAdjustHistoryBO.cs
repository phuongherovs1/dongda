using System;

namespace iDAS.Service
{
    public class DocumentAdjustHistoryBO : BaseBO
    {
        #region Base
        public Guid? DocumentId { get; set; }
        public string AdjustContent { get; set; }
        public int? AdjustTimes { get; set; }
        public DateTime? AdjustAt { get; set; }
        public Guid? AdjustBy { get; set; }
        #endregion

        #region rule
        public EmployeeBO EmployeeAdjustBy { get; set; }
        #endregion
    }
}
