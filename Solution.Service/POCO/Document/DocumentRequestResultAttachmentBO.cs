using System;

namespace iDAS.Service
{
    public partial class DocumentRequestResultAttachmentBO : BaseBO
    {
        #region Base
        public Guid? ResultId { get; set; }

        #endregion

        #region Rule

        #endregion

        public Guid? FileId { get; set; }
    }
}
