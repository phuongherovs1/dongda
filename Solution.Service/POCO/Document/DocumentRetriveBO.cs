using System;

namespace iDAS.Service
{
    public class DocumentRetriveBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> DocumentId { get; set; }
        public string Type { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<System.Guid> DepartmentId { get; set; }
        public Nullable<System.Guid> EmployeeId { get; set; }
        public Nullable<System.Guid> ExternalId { get; set; }
        public Nullable<System.Guid> ReturnBy { get; set; }
        public Nullable<System.DateTime> ReturnAt { get; set; }
        public string Note { get; set; }
        #endregion

        #region Rule

        #endregion

    }
}
