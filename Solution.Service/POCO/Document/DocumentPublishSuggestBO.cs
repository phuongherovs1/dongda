﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DocumentPublishSuggestBO : BaseBO
    {
        public Guid? DocumentId { get; set; }
        public Guid? ApproveBy { get; set; }
        public DateTime? SuggestAt { get; set; }
        public DateTime? ApproveAt { get; set; }
        public bool? IsSend { get; set; }
        public bool? IsApprove { get; set; }
        public bool? IsAccept { get; set; }
        public string Note { get; set; }
        public string ReasonOfReject { get; set; }
        public string ReasonOfSuggest { get; set; }
        public DateTime? ApplyAt { get; set; }
        public DateTime? ExpireAt { get; set; }

        #region rule
        public EmployeeBO EmployeeApproveBy { get; set; }
        public DocumentEmployeeBO EmployeeApprove { get; set; }
        public string EmployeeApproveName { get { return EmployeeApproveBy == null ? string.Empty : EmployeeApproveBy.Name; } }
        /// <summary>
        /// Đường dẫn ảnh người phê duyệt
        /// </summary>
        public string EmployeeApproveAvatarUrl { get { return EmployeeApproveBy == null ? string.Empty : EmployeeApproveBy.AvatarUrl; } }

        public DateTime DocumentApproveAt { get; set; }
        public DocumentBO Document { get; set; }
        public string DocumentName { get { return Document == null ? string.Empty : Document.Name; } }
        public Resource.ApproveStatus Status
        {
            get
            {
                var result = IsApprove == true ? (IsAccept == true ? Resource.ApproveStatus.Approve : Resource.ApproveStatus.Reject)
                    : IsSend == true ? Resource.ApproveStatus.Wait : Resource.ApproveStatus.New;
                return result;
            }
        }
        public string StatusText
        {
            get
            {
                var result = IsApprove == true ? (IsAccept == true ? Resource.ApproveStatusText.Approve : Resource.ApproveStatusText.Reject)
                    : IsSend == true ? Resource.ApproveStatusText.Wait : Resource.ApproveStatusText.New;
                return result;
            }
        }
        /// <summary>
        /// File đính kèm
        /// </summary>
        public FileUploadBO FileAttachs { get { return Document == null ? null : Document.FileAttachs; } }
        public EmployeeBO EmployeeSuggestBy { get; set; }
        /// <summary>
        /// Người đề nghị
        /// </summary>
        public string SuggestName { get; set; }
        /// <summary>
        /// Ảnh người đề nghị
        /// </summary>
        public string SuggestAvatarUrl { get; set; }
        /// <summary>
        /// Kết quả ban hành
        /// </summary>
        public string ResultPublish { get; set; }
        /// <summary>
        /// Trách nhiệm
        /// </summary>
        public string TextRole { get; set; }
        #endregion
    }
}

