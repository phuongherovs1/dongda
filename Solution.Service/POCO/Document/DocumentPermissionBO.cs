using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DocumentPermissionBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> DocumentId { get; set; }
        public Nullable<System.Guid> DepartmentId { get; set; }
        public Nullable<System.Guid> TitleId { get; set; }
        public Nullable<System.Guid> EmployeeId { get; set; }
        public bool? AllowView { get; set; }
        public bool? AllowDownload { get; set; }

        #endregion

        #region Rule
        /// <summary>
        /// Cho ph�p
        /// </summary>
        public bool Allow
        {
            get
            {
                var allow = AllowView == true && AllowDownload == true;
                return allow;
            }
        }
        public bool Disallow
        {
            get
            {
                var disallow = AllowView != true && AllowDownload != true;
                return disallow;
            }
        }
        public string ObjectId { get; set; }
        public string ObjectName { get; set; }
        public string ObjectImage { get; set; }
        public string ObjectTitle { get; set; }
        public Resource.OrganizationTreeType? ObjectType
        {
            get
            {
                var result = DepartmentId.HasValue ? Resource.OrganizationTreeType.Department
                    : TitleId.HasValue ? Resource.OrganizationTreeType.Title
                    : EmployeeId.HasValue ? Resource.OrganizationTreeType.Employee
                    : (Resource.OrganizationTreeType?)null;
                return result;
            }

        }
        public bool FromView { get; set; }

        #endregion
    }
}
