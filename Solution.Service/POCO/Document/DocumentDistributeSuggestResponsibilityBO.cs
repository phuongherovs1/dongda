﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DocumentDistributeSuggestResponsibilityBO : BaseBO
    {
        #region Base
        public Guid? DocumentDistributeSuggestId { get; set; }

        public Guid? DepartmentId { get; set; }

        public int? RoleType { get; set; }

        public bool? IsNew { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }

        public bool? IsComplete { get; set; }

        public bool? IsLock { get; set; }

        public string ContentSuggest { get; set; }

        public string ApproveNote { get; set; }

        public Guid? PerformBy { get; set; }

        public DateTime? PerformAt { get; set; }

        public int? Order { get; set; }

        public Guid? SendTo { get; set; }

        public int? SenderRoleType { get; set; }

        public int? SenderOrder { get; set; }

        public bool? IsCancel { get; set; }

        public DateTime? ApplyAt { get; set; }

        public int? Quantity { get; set; }

        public string ReasonOfSuggest { get; set; }
        #endregion

        #region Rule
        public DocumentEmployeeBO EmployeeRecive { get; set; }
        public DocumentBO Document { get; set; }
        public EmployeeBO EmployeeSuggest { get; set; }
        public Resource.DocumentProcessRole RoleView
        {
            get
            {
                if (RoleType == (int)Resource.DocumentProcessRole.Check)
                {
                    return Resource.DocumentProcessRole.Check;
                }
                if (RoleType == (int)Resource.DocumentProcessRole.ApproveDocument)
                {
                    return Resource.DocumentProcessRole.ApproveDocument;
                }
                return Resource.DocumentProcessRole.DistributeSuggester;
            }
        }
        #endregion


    }
}
