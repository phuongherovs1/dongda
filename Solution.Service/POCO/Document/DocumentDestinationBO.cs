﻿using System;

namespace iDAS.Service
{
    public class DocumentDestinationBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> DocumentId { get; set; }
        public Guid? DepartmentId { get; set; }
        public Guid? TitleId { get; set; }
        public Guid? EmployeeId { get; set; }
        public Guid? ExternalId { get; set; }
        #endregion

        #region rule

        private string destinationId;

        public string DestinationId
        {
            get { return destinationId; }
            set { destinationId = value; }
        }

        public string DestinationName { get; set; }
        public Common.Resource.DestinationType DestinationType { get; set; }
        /// <summary>
        /// Loại nơi nhận: Nội bộ/Bên ngoài
        /// </summary>
        public string DestinationKind { get; set; }
        #endregion
    }
}
