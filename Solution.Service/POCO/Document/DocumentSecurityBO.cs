using System;

namespace iDAS.Service
{
    public class DocumentSecurityBO : BaseBO
    {
        #region Base
        public string Name { get; set; }
        public Nullable<bool> IsUse { get; set; }
        public string Color { get; set; }
        public string Description { get; set; }
        #endregion

        #region Rule
        public Guid? ApproveBy { get; set; }
        public Guid? DepartmentId { get; set; }
        public string ApproveTitleName { get; set; }
        public Guid? DepartmentSecurityId { get; set; }
        #endregion

    }
}
