﻿using System;

namespace iDAS.Service
{
    public class DocumentDistributeBO : BaseBO
    {
        #region Base
        public Nullable<System.Guid> DocumentId { get; set; }
        public Guid? DepartmentId { get; set; }
        public Nullable<System.Guid> ReceiveBy { get; set; }
        public Nullable<System.Guid> RequestId { get; set; }
        public string NameDepartment { get; set; }
        public string NameEmploy { get; set; }
        public Nullable<System.Guid> ExternalId { get; set; }
        public string Type { get; set; }
        public Nullable<int> Quantity { get; set; }
        public string Code { get; set; }
        public string Note { get; set; }
        public bool IsExternal { get; set; }
        public Nullable<System.DateTime> DistributesAt { get; set; }
        #endregion

        #region Rule
        public EmployeeBO EmployeeReceive { get; set; }
        public DocumentBO Document { get; set; }
        public DocumentExternalBO DocumentExternal { get; set; }
        /// <summary>
        /// Người gửi
        /// </summary>
        public string Receiver { get { return EmployeeReceive == null ? null : EmployeeReceive.Name; } }

        public string Name { get { return DocumentExternal == null ? null : DocumentExternal.Name; } }
        public string EmployeeName { get { return DocumentExternal == null ? null : DocumentExternal.EmployeeName; } }

        /// <summary>
        /// Chức danh
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Anh
        /// </summary>
        public string AvatarUrl { get { return EmployeeReceive == null ? null : EmployeeReceive.AvatarUrl; } }
        public string Url = @"/Content/Images/underfind.jpg";
        #endregion
    }
}
