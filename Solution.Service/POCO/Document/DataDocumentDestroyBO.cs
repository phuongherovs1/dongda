﻿using System;

namespace iDAS.Service
{
    public class DataDocumentDestroyBO
    {
        #region Base

        #endregion

        #region Rule
        public bool? IsCheck { get; set; }
        public Guid DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentCode { get; set; }
        public string StatusText { get; set; }
        #endregion
    }
}
