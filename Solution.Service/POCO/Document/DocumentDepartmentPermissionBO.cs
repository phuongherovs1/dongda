﻿using System;

namespace iDAS.Service
{
    public class DocumentDepartmentPermissionBO : BaseBO
    {
        #region Base
        public Guid? DepartmentId { get; set; }
        public Guid? EmployeeId { get; set; }
        public Guid? DepartmentTitleId { get; set; }
        public string Description { get; set; }
        public bool? IsSettingProcess { get; set; }
        public bool? IsSettingCategory { get; set; }
        public bool? IsArchive { get; set; }
        #endregion

        #region Rule
        public string DepartmentName { get; set; }
        public bool IsEmployee { get { return EmployeeId.HasValue; } }
        public TitleBO DepartmentTitle { get; set; }
        public EmployeeBO Employee { get; set; }
        public DateTime DateUpdate
        {
            get
            {
                if (UpdateAt.HasValue)
                    return UpdateAt.Value;
                else return CreateAt.HasValue ? CreateAt.Value : DateTime.Now;
            }
        }
        public string ObjectName { get; set; }
        public string ObjectID
        {
            get
            {
                if (EmployeeId.HasValue && EmployeeId != Guid.Empty)
                    return "Employee_" + EmployeeId.Value;
                else if (DepartmentTitleId.HasValue && DepartmentTitleId != Guid.Empty)
                    return "Title_" + DepartmentTitleId.Value;
                else
                    return string.Empty;
            }
        }
        #endregion
    }
}
