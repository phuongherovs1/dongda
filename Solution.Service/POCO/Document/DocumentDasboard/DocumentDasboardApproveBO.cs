﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DocumentDasboardApproveBO
    {
        #region Rule
        public Guid Id { get; set; }
        public string Time { get; set; }
        public bool IsAdjust
        {
            get;
            set;
        }
        public bool? TypeDestroy
        {
            get;
            set;
        }
        public bool? CheckDestroy
        {
            get;
            set;
        }
        public bool IsWrite
        {
            get;
            set;
        }
        public string Name
        {
            get
            {
                var _name = string.Empty;
                if (IsWrite && (RoleType == (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit
                            || RoleType == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit
                            || RoleType == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit))
                {
                    _name = "Đề nghị viết mới";
                }
                if (IsAdjust && (RoleType == (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit
                            || RoleType == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit
                            || RoleType == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit))
                {
                    _name = "Đề nghị sửa đổi";
                }
                if (RoleType == (int)Resource.DocumentProcessRole.Check && CheckDestroy != true)
                {
                    _name = "Đề nghị ban hành";
                }
                if (RoleType == (int)Resource.DocumentProcessRole.Check && CheckDestroy == true)
                {
                    _name = "Đề nghị hủy";
                }
                if (RoleType == (int)Resource.DocumentProcessRole.Promulgate && CheckDestroy != true)
                {
                    _name = "Đề nghị ban hành";
                }

                if (RoleType == (int)Resource.DocumentProcessRole.Promulgate && CheckDestroy == true)
                {
                    _name = "Đề nghị hủy";
                }
                if (RoleType == (int)Resource.DocumentProcessRole.Archiver)
                {
                    _name = "Yêu cầu lưu trữ";
                }
                // Title đề nghị hủy tài liệu
                if (RoleType == (int)Resource.DocumentProcessRole.DestroyCreater)
                {
                    _name = "Đề nghị hủy";
                }

                if (RoleType == (int)Resource.DocumentProcessRole.ApproveDocument && CheckDestroy == true)
                {
                    _name = "Đề nghị hủy";
                }

                if (RoleType == (int)Resource.DocumentProcessRole.ApproveDocument && CheckDestroy != true)
                {
                    _name = "Đề nghị ban hành";
                }
                if (RoleType == (int)Resource.DocumentProcessRole.DistributeRequester)
                {
                    _name = "Yêu cầu phân phối";
                }
                if (RoleType == (int)Resource.DocumentProcessRole.DistributeSuggester
                    || (RoleType == (int)Resource.DocumentProcessRole.Check && IsDistribute)
                    || (RoleType == (int)Resource.DocumentProcessRole.ApproveDocument && IsDistribute))
                {
                    _name = "Đề nghị phân phối ";
                }
                if (IsDistribute == true && RoleType == (int)Resource.DocumentProcessRole.Promulgate)
                {
                    _name = "Đề nghị phân phối";
                }
                if (RoleType == (int)Resource.DocumentProcessRole.DestroyRequester)
                {
                    _name = "Yêu cầu hủy";
                }
                // Đề nghị áp dụng
                if (IsSuggestApply == true)
                {
                    _name = "Đề nghị áp dụng";
                }
                return _name;
            }
        }
        public bool? IsNew { get; set; }
        public bool IsDestroy { get; set; }
        public bool? IsApprove { get; set; }
        public bool? IsAccept { get; set; }
        public bool? IsComplete { get; set; }
        public int? RoleType { get; set; }
        public string StatusText
        {
            get
            {
                var status = string.Empty;
                switch (RoleType)
                {
                    case (int)Resource.DocumentProcessRole.Check:
                        if (IsNew == true)
                        {
                            status = Common.Resource.ApproveStatusText.WaitCheck;
                        }
                        if (IsApprove == true && IsAccept == true)
                        {
                            status = Common.Resource.ApproveStatusText.CheckAccept;
                        }
                        if (IsApprove == true && IsAccept == false)
                        {
                            status = Common.Resource.ApproveStatusText.CheckNotAccept;
                        }
                        if (IsDestroy == true)
                        {
                            status = Common.Resource.ApproveStatusText.Destroy;
                        }
                        if (CheckDestroy == true && IsApprove == null && IsAccept == null)
                        {
                            status = Common.Resource.ApproveStatusText.WaitReview;
                        }
                        if (CheckDestroy == true && IsApprove == true && IsAccept == true)
                        {
                            status = Common.Resource.ApproveStatusText.ReviewAccept;
                        }
                        if (CheckDestroy == true && IsApprove == true && IsAccept == false)
                        {
                            status = Common.Resource.ApproveStatusText.ReviewNotAccept;
                        }
                        if (IsDistribute) status = Common.Resource.ApproveStatusText.WaitCheck;
                        break;
                    case (int)Resource.DocumentProcessRole.ApproveDocument:
                        if (IsNew == true)
                        {
                            status = Common.Resource.ApproveStatusText.Wait;
                        }
                        if (IsApprove == true && IsAccept == true)
                        {
                            status = Common.Resource.ApproveStatusText.Approve;
                        }
                        if (IsApprove == true && IsAccept == false)
                        {
                            status = Common.Resource.ApproveStatusText.Reject;
                        }
                        if (IsDestroy == true)
                        {
                            status = Common.Resource.ApproveStatusText.Destroy;
                        }
                        break;
                    case (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit:
                        if (IsNew == true)
                        {
                            status = Common.Resource.ApproveStatusText.WaitReview;
                        }
                        if (IsApprove == true)
                        {
                            status = Common.Resource.ApproveStatusText.WaitSend;
                        }
                        if (IsComplete == true)
                        {
                            status = Common.Resource.ApproveStatusText.Wait;
                        }
                        if (IsDestroy == true)
                        {
                            status = Common.Resource.ApproveStatusText.Destroy;
                        }
                        break;
                    case (int)Resource.DocumentProcessRole.ReviewWriteAndEdit:
                        // Trạng thái đề nghị áp dụng
                        if (IsSuggestApply == true)
                        {
                            if (IsNew == true)
                                status = Common.Resource.ApproveStatusText.WaitReview;
                            if (IsApprove == true && IsAccept == true)
                                status = Common.Resource.ApproveStatusText.Approve;
                            if (IsApprove == true && IsAccept == false)
                                status = Common.Resource.ApproveStatusText.Reject;
                        }
                        else
                        {
                            if (IsNew == true)
                            {
                                status = Common.Resource.ApproveStatusText.WaitReview;
                            }
                            if (IsApprove == true)
                            {
                                status = Common.Resource.ApproveStatusText.WaitSend;
                            }
                            if (IsComplete == true)
                            {
                                status = Common.Resource.ApproveStatusText.Wait;
                            }
                            if (IsDestroy == true)
                            {
                                status = Common.Resource.ApproveStatusText.Destroy;
                            }
                        }
                        break;
                    case (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit:
                        // Trạng thái đề nghị áp dụng
                        if (IsSuggestApply == true)
                        {
                            if (IsNew == true)
                                status = Common.Resource.ApproveStatusText.Wait;
                            if (IsApprove == true && IsAccept == true)
                                status = Common.Resource.ApproveStatusText.Approve;
                            if (IsApprove == true && IsAccept == false)
                                status = Common.Resource.ApproveStatusText.Reject;
                        }
                        else
                        {
                            if (IsNew == true)
                            {
                                status = Common.Resource.ApproveStatusText.Wait;
                            }
                            if (IsApprove == true && IsAccept == true)
                            {
                                status = Common.Resource.ApproveStatusText.Approve;
                            }
                            if (IsApprove == true && IsAccept == false)
                            {
                                status = Common.Resource.ApproveStatusText.Reject;
                            }
                        }
                        break;
                    case (int)Resource.DocumentProcessRole.Promulgate:
                        if (IsNew == true)
                        {
                            status = Common.Resource.ApproveStatusText.WaitPromulgate;
                        }
                        if (IsComplete == true)
                        {
                            status = Common.Resource.ApproveStatusText.Promulgated;
                        }
                        if (IsDestroy == true)
                        {
                            status = Common.Resource.ApproveStatusText.Destroy;
                        }
                        if (IsDistribute == true)
                        {
                            status = Common.Resource.ApproveStatusText.WaitDistribute;
                        }
                        if (CheckDestroy == true && IsComplete != true)
                        {
                            status = Common.Resource.ApproveStatusText.WaitDestroy;
                        }
                        if (CheckDestroy == true && IsComplete == true)
                        {
                            status = Common.Resource.ApproveStatusText.Destroyed;
                        }
                        break;
                    case (int)Resource.DocumentProcessRole.DistributeSuggester:
                        status = Common.Resource.ApproveStatusText.WaitDistribute;
                        break;
                    case (int)Resource.DocumentProcessRole.Archiver:
                        if (IsNew == true)
                        {
                            status = Common.Resource.ApproveStatusText.WaitArchive;
                        }
                        if (IsComplete == true)
                        {
                            status = Common.Resource.ApproveStatusText.Archived;
                        }
                        if (IsDestroy == true)
                        {
                            status = Common.Resource.ApproveStatusText.Destroy;
                        }
                        break;
                    // Trạng thái người tạo đề nghị hủy tài liệu
                    case (int)Resource.DocumentProcessRole.DestroyCreater:
                        if (IsNew == true)
                        {
                            status = Common.Resource.ApproveStatusText.Wait;
                        }
                        if (IsApprove == true && IsAccept == true)
                        {
                            status = Common.Resource.ApproveStatusText.Approve;
                        }
                        if (IsApprove == true && IsAccept == false)
                        {
                            status = Common.Resource.ApproveStatusText.Reject;
                        }
                        if (IsDestroy == true)
                        {
                            status = Common.Resource.ApproveStatusText.Destroy;
                        }
                        break;
                    case (int)Resource.DocumentProcessRole.DistributeRequester:
                        if (IsComplete != true)
                        {
                            status = Common.Resource.ApproveStatusText.WaitDistribute;
                        }
                        else
                        {
                            status = Common.Resource.ApproveStatusText.Distributed;
                        }
                        if (IsDestroy == true)
                        {
                            status = Common.Resource.ApproveStatusText.Destroy;
                        }
                        break;

                    case (int)Resource.DocumentProcessRole.DestroyRequester:
                        if (IsComplete != true)
                        {
                            status = Common.Resource.ApproveStatusText.WaitDestroy;
                        }
                        else
                        {
                            status = Common.Resource.ApproveStatusText.Destroyed;
                        }
                        if (IsDestroy == true)
                        {
                            status = Common.Resource.ApproveStatusText.Destroy;
                        }
                        break;

                }
                return status;
            }
        }
        public string StatusColor
        {
            get
            {
                var result = IsDestroy == true ? Resource.RequestStatusColor.Destroy : IsComplete == true ? Resource.RequestStatusColor.Complete
                    : IsNew == true ? Resource.RequestStatusColor.Wait
                    : Resource.RequestStatusColor.New;
                return result;
            }
        }
        public string RoleText
        {
            get
            {
                var status = string.Empty;
                switch (RoleType)
                {
                    case (int)Resource.DocumentProcessRole.Check:
                        status = Resource.DocumentSuggestRole.Check;
                        break;
                    case (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit:
                        status = Resource.DocumentSuggestRole.Suggest;
                        break;
                    case (int)Resource.DocumentProcessRole.ReviewWriteAndEdit:
                        // Vai trò của đề nghị áp dụng
                        if (IsSuggestApply == true)
                            status = Resource.DocumentExternalApplyStatusText.Review;
                        else
                            status = Resource.DocumentSuggestRole.Review;
                        break;
                    case (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit:
                        // Vai trò của đề nghị áp dụng
                        if (IsSuggestApply == true)
                            status = Resource.DocumentExternalApplyStatusText.Approve;
                        else
                            status = Resource.DocumentSuggestRole.Approve;
                        break;
                    case (int)Resource.DocumentProcessRole.ApproveDocument:
                        status = Resource.DocumentSuggestRole.Approve;
                        break;
                    case (int)Resource.DocumentProcessRole.DistributeRequester:
                        status = Resource.DocumentSuggestRole.Distribute;
                        break;
                    case (int)Resource.DocumentProcessRole.DistributeSuggester:
                        status = Resource.DocumentSuggestRole.Suggest;
                        break;
                    case (int)Resource.DocumentProcessRole.Promulgate:
                        status = Resource.DocumentSuggestRole.Promulgate;
                        break;
                    case (int)Resource.DocumentProcessRole.Archiver:
                        status = Resource.DocumentSuggestRole.Archive;
                        break;
                    case (int)Resource.DocumentProcessRole.DestroyRequester:
                        status = Resource.DocumentSuggestRole.Destroy;
                        break;
                    // Vai trò người tạo đề nghị hủy tài liệu
                    case (int)Resource.DocumentProcessRole.DestroyCreater:
                        status = Resource.DocumentSuggestRole.Suggest;
                        break;
                }
                if (IsCreater && !IsPerform)
                    status = "Người tạo";
                return status;
            }
        }
        public Resource.DocumentProcessRole RoleView { get; set; }
        public bool IsCreater
        {
            get;
            set;
        }
        public bool IsPerform
        {
            get;
            set;
        }
        public bool IsDistribute { get; set; }
        public DateTime? CreateAt { get; set; }
        public bool? IsLock { get; set; }
        /// <summary>
        /// True: nếu đề nghị được tạo từ người lưu trữ, !=True: nếu đề nghị hủy được tạo từ 1 tài liệu
        /// </summary>
        public bool? IsArchiver { get; set; }
        public Guid? DocumentDestroyId { get; set; }
        /// <summary>
        /// Đề nghị áp dụng
        /// </summary>
        public bool? IsSuggestApply { get; set; }
        /// <summary>
        /// Người gửi đề nghị
        /// </summary>
        public EmployeeBO Sender { get; set; }
        public Guid? CreateBy { get; set; }
        public Guid? DocumentId { get; set; }
        public DocumentBO Document { get; set; }
        #endregion
    }
}
