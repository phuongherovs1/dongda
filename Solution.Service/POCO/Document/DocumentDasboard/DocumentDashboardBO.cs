﻿using System;

namespace iDAS.Service
{
    public class PieChartBO
    {
        public string Name { get; set; }
        public Guid? DeparmentId { get; set; }
        public int Value { get; set; }
        public int ValueOther { get; set; }
        public int ValueThree { get; set; }
        public decimal Rate { get; set; }
    }
    public class DocumentWriteActorAnalyticBO
    {
        public int PerformWrite { get; set; }
        public int SugestApprove { get; set; }
        public int SugestAdjust { get; set; }
        public int SugestDistribute { get; set; }
    }

    public class DocumentApproveActorAnalyticBO
    {
        public int PerformWrite { get; set; }
        public int SuggestPublish { get; set; }
        public int ApproveSuggestReview { get; set; }
        public int ApproveSugestDistribute { get; set; }
    }
    public class DocumentPubishActorAnalyticBO
    {
        public int RequestWrite { get; set; }
        public int ApproveSuggestPublish { get; set; }
        public int ApproveSuggestDestroy { get; set; }
        public int RequestDestroy { get; set; }
    }
    public class DocumentArchiveActorAnalyticBO
    {
        public int ReceiveArchive { get; set; }
        public int SugestDestroy { get; set; }
        public int PerformDistribute { get; set; }
        public int PerformDestroy { get; set; }

    }
}
