using System;

namespace iDAS.Service
{
    public class DocumentRequestResultBO : BaseBO
    {
        #region Base
        public Guid? DocumentRequestId { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public DateTime? StartAt { get; set; }
        public DateTime? EndAt { get; set; }
        public Guid? PerformBy { get; set; }
        #endregion

        #region Rule
        public FileUploadBO FileAttachs { get; set; }
        #endregion
    }
}
