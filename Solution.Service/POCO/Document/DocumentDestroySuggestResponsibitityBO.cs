﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public class DocumentDestroySuggestResponsibitityBO : BaseBO
    {
        #region Base
        public string DocumentName { get; set; }
        public string DocumentCode { get; set; }
        public Guid? DepartmentId { get; set; }

        public Guid? DocumentDestroyId { get; set; }

        public int? RoleType { get; set; }

        public bool? IsNew { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }

        public bool? IsComplete { get; set; }

        public bool? IsLock { get; set; }

        public bool? IsArchiver { get; set; }

        public string ApproveNote { get; set; }

        public Guid? ApproveBy { get; set; }

        public DateTime? ApproveAt { get; set; }

        public int? Order { get; set; }

        public bool? IsCancel { get; set; }
        #endregion

        #region Rule
        public DocumentBO Document { get; set; }
        public DocumentDestroySuggestBO DestroySuggest { get; set; }

        public string GroupSummaryName { get { return DestroySuggest == null ? string.Empty : DestroySuggest.GroupSummaryName; } }
        public string StatusText { get { return DestroySuggest == null ? string.Empty : DestroySuggest.StatusText; } }
        public IEnumerable<Guid> DocumentIds { get; set; }

        public EmployeeBO EmployeeDestroyCreate { get; set; }
        public bool IsCreater
        {
            get
            {
                return CreateBy == UserId;
            }
        }

        public bool IsnotCreater
        {
            get
            {
                return ApproveBy == UserId;
            }
        }
        /// <summary>
        /// Vai trò
        /// </summary>
        public string RoleText
        {
            get
            {
                var status = string.Empty;
                if (RoleType != null)
                {
                    switch ((int)RoleType)
                    {
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.SuggesterWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.ReviewWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.ApprovalWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.DestroyBillCreater;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Check:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.ReviewWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument:
                            status = iDAS.Service.Common.Resource.DocumentProcessRoleText.ApproveDocument;
                            break;
                        default:
                            status = string.Empty;
                            break;
                    }
                }
                return status;
            }
        }
        public iDAS.Service.Common.Resource.DocumentProcessRole RoleView
        {
            get
            {
                var status = iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit;
                if (RoleType != null)
                {
                    switch ((int)RoleType)
                    {
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Archiver:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Archiver;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.AssignmentWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.AssignmentWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Check:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Check;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyCreater;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Distributer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Distributer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestApproval:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestApproval;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggester:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggester;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestReviewer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestReviewer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Write:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Write;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeRequester:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeRequester;
                            break;
                        default:
                            break;
                    }
                }
                return status;
            }
        }
        /// <summary>
        /// Kết quả phê duyệt/xem xét
        /// </summary>
        public string ResultText
        {
            get
            {
                var result = string.Empty;
                if (RoleType != null)
                {
                    switch ((int)RoleType)
                    {
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer:
                            if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                                result = "Đạt";
                            else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                                result = "Không đạt";
                            else
                                result = "Chờ duyệt";
                            break;

                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument:
                            if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                                result = "Đạt";
                            else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                                result = "Không đạt";
                            else
                                result = "Chờ duyệt";
                            break;

                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Check:
                            if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                                result = "Đạt";
                            else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                                result = "Không đạt";
                            else
                                result = "Chờ xem xét";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval:
                            if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && IsAccept.Value)
                                result = "Đạt";
                            else if (IsApprove.HasValue && IsApprove.Value && IsAccept.HasValue && !IsAccept.Value)
                                result = "Không đạt";
                            else
                                result = "Chờ duyệt";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater:
                            if (IsComplete == true)
                                result = "Đã hoàn thành";
                            else
                                result = "Chờ tạo biên bản";
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyCreater:
                            result = IsNew == true ? "Đã gửi" : string.Empty;
                            break;

                        default:
                            break;
                    }
                }
                return result;
            }
        }

        public EmployeeBO EmployeeSend { get; set; }

        public DocumentEmployeeBO EmployeeRecive { get; set; }

        /// <summary>
        /// Chuyển người thực hiện
        /// </summary>
        public EmployeeBO EmployeeChange { get; set; }

        /// <summary>
        /// Phòng ban đề nghị
        /// </summary>
        public string DepartmentName { get; set; }

        /// <summary>
        /// Đếm số tài liệu của đề nghị hủy
        /// </summary>
        public int CountDocument { get; set; }

        public EmployeeBO EmployeeApproveBy { get; set; }
        #endregion
    }
}
