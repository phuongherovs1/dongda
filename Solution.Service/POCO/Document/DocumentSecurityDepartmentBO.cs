﻿using System;

namespace iDAS.Service
{
    public class DocumentSecurityDepartmentBO : BaseBO
    {


        #region Base
        public Nullable<System.Guid> DepartmentId { get; set; }
        public Nullable<System.Guid> SecurityId { get; set; }
        public Nullable<System.Guid> ApproveBy { get; set; }
        public string ApproveName { get; set; }
        #endregion

        #region Rule
        /// <summary>
        /// Tên mức độ bảo mật
        /// </summary>
        public string SercurityName { get; set; }
        /// <summary>
        /// Màu sắc mức độ bảo mật
        /// </summary>
        public string SercurityColor { get; set; }

        public string SercurityDescription { get; set; }

        public Nullable<bool> SercurityIsUse { get; set; }
        #endregion
    }
}
