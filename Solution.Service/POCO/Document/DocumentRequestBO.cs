﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DocumentRequestBO : BaseBO
    {
        #region Basic
        /// <summary>
        /// 
        /// </summary>
        public Guid? DepartmentId { get; set; }
        public Guid? ParentId { get; set; }
        public int? Order { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid? DocumentId { get; set; }
        /// <summary>
        /// Xuất phát từ đề nghị soạn thảo
        /// </summary>
        public Guid? DocumentSuggestId { get; set; }
        /// <summary>
        /// Người thực hiện yêu cầu
        /// </summary>
        public Guid? PerformBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? TypeDestroy { get; set; }
        /// <summary>
        /// Xác định yêu cầu biên soạn hay soạn thảo? : false: biên soạn, true: soạn thảo
        /// </summary>
        public bool? TypeWrite { get; set; }
        /// <summary>
        /// Xác định yêu cầu đang thực hiện là biên soạn hay soạn thảo
        /// True: Biên soạn khi tạo ra yêu cầu soạn thảo khác
        /// False: Chỉ là soạn thảo khi không tạo ra yêu cầu soạn thảo khác
        /// </summary>
        public bool? TypeCompile { get; set; }

        public bool? TypeArchive { get; set; }
        /// <summary>
        /// Sửa đổi hay tạo mới
        /// </summary>
        public bool? TypeAdjust { get; set; }
        /// <summary>
        /// Đã gửi yêu cầu
        /// </summary>
        public bool? IsSend { get; set; }
        /// <summary>
        /// Đã thực hiện yêu cầu
        /// </summary>
        public bool? IsPerform { get; set; }

        /// <summary>
        /// Yêu cầu hoàn thành
        /// </summary>
        public bool? IsComplete { get; set; }

        /// <summary>
        /// Yêu cầu kết thúc
        /// </summary>
        public bool? IsFinish { get; set; }
        /// <summary>
        /// Ghi chú
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// Ngày bắt đầu
        /// </summary>
        public DateTime? StartAt { get; set; }
        /// <summary>
        /// Ngày kết thúc
        /// </summary>
        public DateTime? EndAt { get; set; }
        public DateTime? EstimateAt { get; set; }
        //  public FileUploadBO FileAttachs { get; set; }
        /// <summary>
        /// Người duyệt
        /// </summary>
        public Guid? ApproveBy { get; set; }
        /// <summary>
        /// Ngày duyệt
        /// </summary>
        public DateTime? ApproveAt { get; set; }

        public DateTime? SendAt { get; set; }
        public bool? IsApprove { get; set; }
        public bool? IsAccept { get; set; }
        public string ReasonOfReject { get; set; }

        public bool? IsDestroy { get; set; }
        public string ReasonOfDestroy { get; set; }
        public int? TypeRole { get; set; }
        /// <summary>
        /// Trạng thái xuyên suốt quá trình thực hiện của yêu cầu 
        /// 1	Mới
        /// 2	Chờ phân công
        /// 3	Chờ thực hiện
        /// 4	Đang thực hiện
        /// 5	Chờ kiểm tra (trạng thái của đề nghị kiểm tra)
        /// 6	Không đạt
        /// 7	Chờ gửi duyệt
        /// 8	Chờ duyệt
        /// 9	Hoàn thành
        /// 10	Hủy
        /// 11	Quá hạn
        /// </summary>
        public int? ProcessStatus { get; set; }
        /// <summary>
        /// Người phát sinh tài liệu
        /// 1. Từ người yêu cầu
        /// 2. Từ người phân công
        /// 3. Từ người biên soạn
        /// 4. Từ người đề nghị
        /// 5. Từ người xem xét
        /// </summary>
        public int? RequestFrom { get; set; }
        /// <summary>
        /// Yêu cầu gửi đến
        /// 2. Người phân công
        /// 3. Người biên soạn
        /// </summary>
        public int? RequestTo { get; set; }
        /// <summary>
        /// Nguồn phát sinh yêu cầu
        /// 1. Từ đề nghị viết mới
        /// 2. Từ đề nghị sửa đổi
        /// 3. Từ yêu cầu viết mới
        /// 4. Từ yêu cầu sửa đổi
        /// 5. Từ người phân công
        /// 6. Từ người biên soạn
        /// </summary>
        public int? RequestSource { get; set; }

        #endregion

        #region Rule
        public string SendAtText
        {
            get
            {
                var result = SendAt == null ? string.Empty : SendAt.Value.ToString("dd-MM-yyyy HH:mm");
                return result;
            }
        }
        public DocumentRequestResultBO Result { get; set; }
        /// <summary>
        /// thông tin nguồn đề nghị
        /// </summary>
        public DocumentWriteSuggestBO Suggest { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DocumentAdjustSuggestBO SuggestAdjust { get; set; }
        /// <summary>
        /// Thông tin nguồn yêu cầu
        /// </summary>
        public DocumentRequestBO Parent { get; set; }
        /// <summary>
        /// Thông tin yêu cầu tạo ra
        /// </summary>
        public DocumentRequestBO NewRequest { get; set; }
        public string RequesterName { get; set; }
        public string RequesterAvatarUrl { get; set; }
        public Resource.RequestStatus Status
        {
            get
            {
                var result = IsFinish == true ? Resource.RequestStatus.Finish
                    : IsComplete == true ? Resource.RequestStatus.Complete
                    : IsPerform == true ? Resource.RequestStatus.Perform
                    : IsSend == true ? Resource.RequestStatus.Wait
                    : Resource.RequestStatus.New;
                return result;
            }
        }
        /// <summary>
        /// Tên trạng thái
        /// </summary>
        public string StatusText
        {
            get
            {
                var result = IsDestroy == true ? Resource.RequestStatusText.Destroy
                    : IsFinish == true ? Resource.RequestStatusText.Finish
                    : IsComplete == true ? Resource.RequestStatusText.Complete
                    : IsPerform == true ? Resource.RequestStatusText.Perform
                    : (EndAt.HasValue && EndAt.Value.Date < DateTime.Now.Date) ? Resource.RequestStatusText.OverwriteTime
                    : IsSend == true ? Resource.RequestStatusText.Wait
                    : Resource.RequestStatusText.New;
                return result;
            }
        }
        /// <summary>
        /// Màu trạng thái
        /// </summary>
        public string StatusColor
        {
            get
            {
                var result = IsFinish == true ? Resource.RequestStatusColor.Finish
                    : IsComplete == true ? Resource.RequestStatusColor.Complete
                    : IsPerform == true ? Resource.RequestStatusColor.Perform
                    : IsSend == true ? Resource.RequestStatusColor.Wait
                    : Resource.RequestStatusColor.New;
                return result;
            }
        }
        /// <summary>
        /// Xác định: Phân công xuất phát từ yêu cầu hoặc đề nghị
        /// </summary>
        public Resource.DocumentAssignType AssignType
        {
            get
            {
                var result = (RequestSource.HasValue && RequestSource == (int)Resource.RequestSource.Compilation) ? Resource.DocumentAssignType.NewDocument
                    : TypeWrite == true ? (!DocumentSuggestId.HasValue ? Resource.DocumentAssignType.RequestWrite : Resource.DocumentAssignType.SuggestCreate)
                    : TypeAdjust == true ? (!DocumentSuggestId.HasValue ? Resource.DocumentAssignType.RequestAdjust : Resource.DocumentAssignType.SuggestAdjust)
                    : Resource.DocumentAssignType.None;
                return result;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AssignTypeText
        {
            get
            {
                var result = (RequestSource.HasValue && RequestSource == (int)Resource.RequestSource.Compilation) ? Resource.DocumentAssignTypeText.NewDocument
                    : TypeWrite == true ? (!DocumentSuggestId.HasValue ? Resource.DocumentAssignTypeText.RequestWrite : Resource.DocumentAssignTypeText.SuggestCreate)
                    : TypeAdjust == true ? (!DocumentSuggestId.HasValue ? Resource.DocumentAssignTypeText.RequestAdjust : Resource.DocumentAssignTypeText.SuggestAdjust)
                    : Resource.DocumentAssignTypeText.None;
                return result;
            }
        }

        public string PublishTypeText
        {
            get
            {
                var result = TypeRole == (int)iDAS.Service.Common.Resource.DocumentProcessRole.Archiver ? Resource.DocumentAssignTypeText.RequesPublish : Resource.DocumentAssignTypeText.SuggestCreate;
                return result;
            }
        }

        public Resource.DocumentEmployeeRole? EmployeeRole
        {
            get
            {
                Resource.DocumentEmployeeRole? result = null;
                // Ưu tiên xét trường hợp người thực hiện
                if (PerformBy == UserId)
                {
                    switch (RequestTo)
                    {
                        case (int)Resource.RequestTo.Assigner:
                            result = Resource.DocumentEmployeeRole.Assigner;
                            break;
                        case (int)Resource.RequestTo.Compilator:
                            result = Resource.DocumentEmployeeRole.Compiler;
                            break;
                        case (int)Resource.RequestTo.Writer:
                            result = Resource.DocumentEmployeeRole.Writer;
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    switch (RequestFrom)
                    {
                        case (int)Resource.RequestFrom.Requestor:
                            result = Resource.DocumentEmployeeRole.Approver;
                            break;
                        case (int)Resource.RequestFrom.Assigner:
                            result = Resource.DocumentEmployeeRole.Assigner;
                            break;
                        case (int)Resource.RequestFrom.Compilator:
                            result = Resource.DocumentEmployeeRole.Compiler;
                            break;
                        case (int)Resource.RequestFrom.Reviewer:
                            result = Resource.DocumentEmployeeRole.Reviewer;
                            break;
                        case (int)Resource.RequestFrom.Suggestor:
                            result = Resource.DocumentEmployeeRole.Suggester;
                            break;
                        default:
                            break;
                    }
                }
                return result;
            }
        }

        public string EmployeeRoleText
        {
            get
            {
                var result = string.Empty;
                if (PerformBy == UserId)
                {
                    switch (RequestTo)
                    {
                        case (int)Resource.RequestTo.Assigner:
                            result = Resource.DocumentEmployeeRoleText.Assigner;
                            break;
                        case (int)Resource.RequestTo.Compilator:
                            result = Resource.DocumentEmployeeRoleText.Compiler;
                            break;
                        case (int)Resource.RequestTo.Writer:
                            result = Resource.DocumentEmployeeRoleText.Writer;
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    switch (RequestFrom)
                    {
                        case (int)Resource.RequestFrom.Requestor:
                            result = Resource.DocumentEmployeeRoleText.Approver;
                            break;
                        case (int)Resource.RequestFrom.Assigner:
                            result = Resource.DocumentEmployeeRoleText.Assigner;
                            break;
                        case (int)Resource.RequestFrom.Compilator:
                            result = Resource.DocumentEmployeeRoleText.Compiler;
                            break;
                        case (int)Resource.RequestFrom.Reviewer:
                            result = Resource.DocumentEmployeeRoleText.Reviewer;
                            break;
                        case (int)Resource.RequestFrom.Suggestor:
                            result = Resource.DocumentEmployeeRoleText.Suggester;
                            break;
                        default:
                            break;
                    }
                }
                return result;
            }
        }

        public EmployeeBO EmployeeApproveBy { get; set; }
        /// <summary>
        /// Check phân biệt vai trò
        /// </summary>
        public bool? Check { get; set; }
        public bool? notCheck { get; set; }

        #region Thêm các trường dùng để thêm vào Document
        public string ProcessStatusText
        {
            get
            {
                var result = Resource.RequestProcessStatusText.None;
                if (ProcessStatus != (int)Resource.RequestProcessStatus.Complete
                    && ProcessStatus != (int)Resource.RequestProcessStatus.Cancel
                    && (EstimateAt.HasValue && EstimateAt.Value.Date < DateTime.Now.Date))
                {
                    result = Resource.RequestProcessStatusText.OutOfDate;
                }
                else
                {
                    switch (ProcessStatus)
                    {
                        case (int)Resource.RequestProcessStatus.New:
                            result = Resource.RequestProcessStatusText.New;
                            break;
                        case (int)Resource.RequestProcessStatus.WaitAssign:
                            result = Resource.RequestProcessStatusText.WaitAssign;
                            break;
                        case (int)Resource.RequestProcessStatus.WaitPerform:
                            result = Resource.RequestProcessStatusText.WaitPerform;
                            break;
                        case (int)Resource.RequestProcessStatus.Perform:
                            result = Resource.RequestProcessStatusText.Perform;
                            break;
                        case (int)Resource.RequestProcessStatus.WaitReview:
                            result = Resource.RequestProcessStatusText.WaitReview;
                            break;
                        case (int)Resource.RequestProcessStatus.ReviewReject:
                            result = Resource.RequestProcessStatusText.ReviewReject;
                            break;
                        case (int)Resource.RequestProcessStatus.WaitSendApprove:
                            result = Resource.RequestProcessStatusText.WaitSendApprove;
                            break;
                        case (int)Resource.RequestProcessStatus.WaitApprove:
                            result = Resource.RequestProcessStatusText.WaitApprove;
                            break;
                        case (int)Resource.RequestProcessStatus.Complete:
                            result = Resource.RequestProcessStatusText.Complete;
                            break;
                        case (int)Resource.RequestProcessStatus.Cancel:
                            result = Resource.RequestProcessStatusText.Cancel;
                            break;
                        default:
                            break;
                    }
                }
                return result;
            }
        }
        /// <summary>
        /// Thông tin tài liệu
        /// </summary>
        public DocumentBO Document { get; set; }
        #region Thông tin tài liệu mới trong trường hợp sửa đổi

        /// <summary>
        /// ID tài liệu mới
        /// </summary>
        public Guid? NewDocumentId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DocumentBO NewDocument { get; set; }

        /// <summary>
        /// Trích yếu nội dung
        /// </summary>
        public string NewDocumentShortContent { get { return NewDocument == null ? string.Empty : NewDocument.ShortContent; } }
        /// <summary>
        /// Đính kèm tài liệu
        /// </summary>
        public FileUploadBO NewDocumentAttachment { get { return NewDocument == null ? new FileUploadBO() : NewDocument.FileAttachs; } }
        #endregion

        public DocumentEmployeeBO EmployeePerform { get; set; }
        public DocumentEmployeeBO EmployeePerformwrite { get; set; }
        public DocumentEmployeeBO EmployeeCheck { get; set; }
        public Guid? CheckerBy { get; set; }
        public DocumentEmployeeBO EmployeeApprove { get; set; }
        public Guid? ApproverBy { get; set; }
        public DocumentEmployeeBO EmployeePromulgate { get; set; }
        public Guid? PromulgaterBy { get; set; }
        public EmployeeBO EmployeePerformBy { get; set; }

        public EmployeeBO EmployeeCreateBy { get; set; }
        public bool IsSuggest
        {
            get
            {
                return DocumentSuggestId.HasValue;
            }
        }

        /// <summary>
        /// Tên tài liệu
        /// </summary>
        public string DocumentName { get { return Document == null ? string.Empty : "[" + Document.Code + "] " + Document.Name; } }
        public string DocumentNameRequestWrite { get { return Document == null ? AssignTypeText : string.Format("{0}-{1}", AssignTypeText, Document.Name); } }
        public string SecurityName { get { return Document == null ? string.Empty : Document.SecurityName; } }
        public string SecurityColor { get { return Document == null ? string.Empty : Document.SecurityColor; } }
        /// <summary>
        /// Mã tài liệu
        /// </summary>
        public string DocumentCode { get { return Document == null ? string.Empty : Document.Code; } }
        /// <summary>
        /// Phiên bản
        /// </summary>
        /// 
        public string Version { get; set; }
        /// <summary>
        /// Danh mục tài liệu
        /// </summary>
        public Nullable<System.Guid> DocumentCategoryId { get; set; }
        /// <summary>
        /// Loại tài liệu: nội bộ
        /// </summary>
        public bool IsInternal { get; set; }
        /// <summary>
        /// Loại tài liệu: bên ngoài
        /// </summary>
        public bool IsExternal { get; set; }
        /// <summary>
        /// Bản mềm
        /// </summary>
        public bool IsSoft { get; set; }
        /// <summary>
        /// Bản cứng
        /// </summary>
        public bool IsHard { get; set; }
        /// <summary>
        /// File đính kèm
        /// </summary>
        public FileUploadBO DocumentAttachments { get; set; }

        public string TitleEmploy { get; set; }
        public FileUploadBO FileAttachs { get; set; }

        /// <summary>
        /// Người gửi
        /// </summary>
        public string Receiver { get { return EmployeeCreateBy == null ? null : EmployeeCreateBy.Name; } }
        /// <summary>
        /// Anh
        /// </summary>
        public string AvatarUrl { get { return EmployeeCreateBy == null ? null : EmployeeCreateBy.AvatarUrl; } }
        #endregion
        #endregion

        #region Button hidden
        /// <summary>
        /// Ẩn trong các trường hợp:
        /// - Đã gửi
        /// - Đã hủy
        /// - Đã hoàn thành
        /// - Đã kết thúc
        /// </summary>
        public bool HiddenSave
        {
            get
            {
                var hidden = IsSend == true || IsDestroy == true || IsComplete == true || IsFinish == true;
                return hidden;
            }
        }
        /// <summary>
        /// Ẩn trong trường hợp
        /// - Đã gửi
        /// - Đã hủy
        /// - Đã hoàn thành
        /// - Đã kết thúc
        /// 
        /// </summary>
        public bool HiddenSend
        {
            get
            {
                var hidden = IsSend == true || IsDestroy == true || IsComplete == true || IsFinish == true;
                return hidden;
            }

        }
        /// <summary>
        /// Ẩn trong trường hợp:
        /// - Chưa gửi
        /// Gửi
        /// - Đã hoàn thành
        /// - Đã kết thúc
        /// - Đã hủy
        /// </summary>
        public bool HiddenRevert
        {
            get
            {
                var hidden = IsSend != true || (IsSend == true && (IsComplete == true || IsFinish == true || IsDestroy == true));
                return hidden;
            }

        }
        /// <summary>
        /// Ẩn trong trường hợp 
        /// - Sau khi gửi đi
        /// - Đã xóa
        /// </summary>
        public bool HiddenDelete
        {
            get
            {
                var hidden = IsSend != true;
                return hidden;
            }
        }

        /// <summary>
        /// Ẩn trong trường hợp:
        /// - Chưa gửi
        /// - Đã hủy
        /// 
        /// </summary>
        public bool HiddenDestroy
        {
            get
            {
                var hidden = IsSend != true || IsDestroy == true;
                return hidden;
            }

        }
        /// <summary>
        /// Ẩn trong trường hợp
        /// - Khác hoàn thành
        /// - Kết thúc
        /// - Hủy
        /// </summary>
        public bool HiddenApprove
        {
            get
            {
                var hidden = IsComplete != true;
                return hidden;
            }

        }
        /// <summary>
        /// Ẩn trong trường hợp
        /// - Khác hoàn thành
        /// - Khác kết thúc
        /// - Khác hủy
        /// </summary>
        public bool HiddenDisApprove { get; set; }

        #endregion

        #region CuongPC
        public iDAS.Service.Common.Resource.DocumentProcessRole RoleView
        {
            get
            {
                var status = iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit;
                if (TypeRole != null)
                {
                    switch ((int)TypeRole)
                    {
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Archiver:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Archiver;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.AssignmentWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.AssignmentWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Check:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Check;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyApproval;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyBillCreater;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyCreater:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyCreater;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyReviewer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Distributer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Distributer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestApproval:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestApproval;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggester:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggester;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestReviewer:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeSuggestReviewer;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.Write:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.Write;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DistributeRequester:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DistributeRequester;
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.DestroyRequester:
                            status = iDAS.Service.Common.Resource.DocumentProcessRole.DestroyRequester;
                            break;
                        default:
                            break;
                    }
                }
                return status;
            }
        }
        public bool IsCreater
        {
            get
            {
                return CreateBy == UserId;
            }
        }
        public bool IsPerformBy
        {
            get
            {
                return PerformBy == UserId;
            }
        }
        public string ResultText
        {
            get
            {
                if (IsComplete.HasValue && IsComplete.Value)
                    return "<span style='color:blue'>Hoàn thành</span>";
                else
                    return "<span style='color:green'>Chờ lưu trữ</span>";
            }
        }
        #region Lưu trữ
        public DocumentArchiveBO Archive { get; set; }
        #endregion
        #endregion

    }
}
