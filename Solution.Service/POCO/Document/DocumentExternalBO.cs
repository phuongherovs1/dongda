namespace iDAS.Service
{
    public class DocumentExternalBO : BaseBO
    {
        #region Base
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }

        #endregion

        #region Rule
        public string EmployeeName { get; set; }
        public EmployeeBO EmployeeApproveBy { get; set; }
        #endregion


    }
}
