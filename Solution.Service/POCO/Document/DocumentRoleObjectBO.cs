﻿using System;

namespace iDAS.Service
{
    public class DocumentRoleObjectBO
    {
        #region Base

        #endregion

        #region Rule
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        #endregion
    }
}
