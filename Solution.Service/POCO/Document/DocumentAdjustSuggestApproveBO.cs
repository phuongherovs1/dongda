using System;

namespace iDAS.Service
{
    public class DocumentAdjustSuggestApproveBO : BaseBO
    {
        #region Base
        public Guid? AdjustSuggestId { get; set; }

        public Guid? ApproveBy { get; set; }

        public bool? IsSend { get; set; }

        public bool? IsApprove { get; set; }

        public bool? IsAccept { get; set; }
        #endregion

        #region Rule

        #endregion

    }
}
