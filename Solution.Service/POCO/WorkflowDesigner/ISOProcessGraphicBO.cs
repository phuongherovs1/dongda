﻿using System;

namespace iDAS.Service
{
    public class ISOProcessGraphicBO : BaseBO
    {
        public Guid? ProcessId { get; set; }

        public string GraphData { get; set; }

    }
}
