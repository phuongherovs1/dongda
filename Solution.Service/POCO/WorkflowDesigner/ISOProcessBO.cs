﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public class ISOProcessBO : BaseBO
    {
        #region Basic

        public string Name { get; set; }
        public Guid? DepartmentId { get; set; }
        public bool? IsStop { get; set; }

        public string Note { get; set; }
        #endregion
        #region Rules
        public List<Cell> Cells { get; set; }
        public List<ISOProcessRelationshipBO> Relationship { get; set; }

        public List<Guid> lstStepId { get; set; }

        public string Status
        {
            get
            {
                var result = IsStop == true ? "Tạm dừng" : "Áp dụng";
                return result;
            }
        }

        public string GraphData { get; set; }

        public List<Guid> ProcessStepIds { get; set; }
        #endregion

    }


}
