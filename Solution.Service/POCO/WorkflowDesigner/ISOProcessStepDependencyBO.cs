using System;

namespace iDAS.Service
{
    public class ISOProcessStepDependencyBO : BaseBO
    {
        public Guid? ProcessId { get; set; }
        public string Name { get; set; }
        public Guid? Source { get; set; }
        public Guid? Target { get; set; }
        public int? DependencyType { get; set; }
        public string Condition { get; set; }
        public string Note { get; set; }

    }
}
