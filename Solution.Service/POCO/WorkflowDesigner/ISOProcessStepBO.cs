using System;

namespace iDAS.Service
{
    public class ISOProcessStepBO : BaseBO
    {


        public string Name { get; set; }

        public Guid? ProcessId { get; set; }

        public string Note { get; set; }

    }
    public class ProcessStepDetail
    {
        public Guid ProcessStepId { get; set; }
        public Guid ImageId { get; set; }
    }
}
