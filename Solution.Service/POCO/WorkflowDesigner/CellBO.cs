﻿using System;
using System.Collections.Generic;


namespace iDAS.Service
{

    public class Cell
    {

        public Guid Id { get; set; }

        public string Type { get; set; }

        public Guid? Source { get; set; }

        public Guid? Target { get; set; }

        public string Name { get; set; }

        public Guid? DataId { get; set; }
        public List<ResonsibityObject> Responsibilities { get; set; }

    }
    public class ResonsibityObject
    {
        public Guid Id { get; set; }
        public string ObjectId { get; set; }
        public string ObjectName { get; set; }
        public string ObjectType { get; set; }

    }
}