using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public class ISOProcessRelationshipBO : BaseBO
    {


        public Guid? StepId { get; set; }

        public Guid? TitleId { get; set; }

        public Guid? EmployeeId { get; set; }
        public Guid? ObjectId { get; set; }
        public string ObjectName { get; set; }
        public string Reponsibility { get; set; }

        public string Note { get; set; }

        #region Rule
        public List<ISOProcessRelationshipBO> lstDetail { get; set; }
        #endregion
    }
}
