﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public class SystemNotifyBO : BaseBO
    {
        public Guid? EmployeeId { get; set; }
        public string Url { get; set; }
        public string Params { get; set; }
        public string Title { get; set; }
        public string Contents { get; set; }
        public int? Type { get; set; }
        public bool? IsRead { get; set; }
        public DateTime? DateSend { get; set; }
        public DateTime? ReadTime { get; set; }

        public string TimeSend
        {
            get
            {
                if (CreateAt.HasValue)
                {
                    var dateDiff = DateTime.Now.Subtract(CreateAt.Value);
                    if ((int)dateDiff.TotalDays < 1 && (int)dateDiff.TotalHours < 1 && (int)dateDiff.TotalMinutes < 1)
                        return string.Format("{0} giây trước", (int)dateDiff.TotalSeconds);
                    else if ((int)dateDiff.TotalDays < 1 && (int)dateDiff.TotalHours < 1 && (int)dateDiff.TotalMinutes > 1)
                        return string.Format("{0} phút trước", (int)dateDiff.TotalMinutes);
                    else if ((int)dateDiff.TotalDays < 1 && (int)dateDiff.TotalHours > 1)
                        return string.Format("{0} giờ trước", (int)dateDiff.TotalHours);
                    else
                        return string.Format("{0} lúc {1}", CreateAt.Value.ToString("dd/MM/yyyy"), CreateAt.Value.Hour + ":" + CreateAt.Value.Minute);
                }
                else
                    return string.Empty;
            }
        }
        public IEnumerable<Guid> EmployeeIDs { get; set; }
        public string Creater
        {
            get;
            set;
        }
    }
}
