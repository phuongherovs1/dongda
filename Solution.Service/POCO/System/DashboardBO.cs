﻿using System;
using System.Collections.Generic;

namespace iDAS.Service
{
    public class DashboardBO
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public string Icon { get; set; }
        public List<PortletBO> Portlets { get; set; }
    }
}
