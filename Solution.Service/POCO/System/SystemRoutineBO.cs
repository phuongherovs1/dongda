﻿using System;

namespace iDAS.Service
{
    public class SystemRoutineBO : BaseBO
    {
        #region Base
        public DateTime? DatePerform { get; set; }

        public int? Type { get; set; }

        public string Params { get; set; }

        public bool? IsComplete { get; set; }
        #endregion

        #region Rule

        #endregion
    }
}
