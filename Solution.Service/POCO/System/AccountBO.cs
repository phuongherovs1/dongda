﻿namespace iDAS.Service
{
    public class AccountBO : BaseBO
    {
        #region Base
        /// <summary>
        /// tên tài khoản
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// mật khẩu tài khoản
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// trạng thái tài khoản: true-khóa tài khoản, false - kích hoạt tài khoản
        /// </summary>
        public bool? IsLock { get; set; }
        #endregion
    }
}
