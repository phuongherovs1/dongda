﻿using System;

namespace iDAS.Service
{
    public class PortletBO
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Action { get; set; }
        public string ControllerName { get; set; }
        public string AreaName { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }
        public int Height { get; set; }
        public int Row { get; set; }
        public int Column { get; set; }
    }
}
