﻿using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public class DatabaseBO
    {
        [Required]
        public string Server { get; set; }
        [Required]
        public string Database { get; set; }
        [Required]
        public string UserId { get; set; }
        [Required]
        public string Password { get; set; }
        public string Code { get; set; }
    }
}
