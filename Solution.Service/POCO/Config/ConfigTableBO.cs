﻿using System.ComponentModel.DataAnnotations;

namespace iDAS.Service
{
    public class ConfigTableBO : BaseBO
    {
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string Configkey { get; set; }
        [Required(ErrorMessage = "Bạn cần nhập thông tin!")]
        public string ConfigValue { get; set; }

        public string ConfigType { get; set; }
    }
}
