﻿using System;

namespace iDAS.Service
{
    public class DispatchRecipientsExternalBO : BaseBO
    {
        public Guid? DispatchId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string EmployeeName { get; set; }

        public string Address { get; set; }

        public string Note { get; set; }

        public DateTime? date { get; set; }
    }
}
