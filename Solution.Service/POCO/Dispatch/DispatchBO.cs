﻿using System;

namespace iDAS.Service
{
    public class DispatchBO : BaseBO
    {
        public Guid? DepartmentId { get; set; }
        public string MemberRelation { get; set; }
        public Guid? CategoryId { get; set; }

        public Guid? SecurityId { get; set; }
        public EmployeeBO EmployeeApproveBy { get; set; }
        public Guid? SendmethodId { get; set; }

        public Guid? UrgencyId { get; set; }
        public bool? IsGo { get; set; }
        public DateTime Sentdate { get; set; }

        public DateTime? Receiveddate { get; set; }

        public string Name { get; set; }

        public string NumberDispatch { get; set; }

        public string Content { get; set; }

        public string Note { get; set; }
        public FileUploadBO FileAttachs { get; set; }

        public string Recipients { get; set; }

        public string Placedelivery { get; set; }

        public string Numberto { get; set; }

        public int? Status { get; set; }

        #region Rule
        public DispatchResponsibilitieBO Responsibilitie { get; set; }
        public string DepartmentName { get; set; }
        public string CategoryName { get; set; }
        public DispatchSecuritieBO Security { get; set; }
        public DispatchUrgencyBO Urgency { get; set; }
        public string UrgencyName { get { return Urgency == null ? string.Empty : Urgency.Name; } }
        public string UrgencyColor { get { return Urgency == null ? string.Empty : Urgency.Color; } }
        public string SecurityName { get { return Security == null ? string.Empty : Security.Name; } }
        public string SecurityColor { get { return Security == null ? string.Empty : Security.Color; } }
        public EmployeeBO EmployeeArchive { get; set; }
        public bool? IsAccept { get; set; }
        public string Type { get { return IsGo == true ? "Nội bộ" : "Bên ngoài"; } }
        public string StatusText
        {
            get
            {
                var type = string.Empty;
                switch (Status)
                {
                    case (int)iDAS.Service.Common.Resource.DispatchStatus.All:
                        type = iDAS.Service.Common.Resource.DispatchStatusText.All;
                        break;
                    case (int)iDAS.Service.Common.Resource.DispatchStatus.WaitReview:
                        type = iDAS.Service.Common.Resource.DispatchStatusText.WaitReview;
                        break;
                    case (int)iDAS.Service.Common.Resource.DispatchStatus.Review:
                        type = iDAS.Service.Common.Resource.DispatchStatusText.Review;
                        break;
                    case (int)iDAS.Service.Common.Resource.DispatchStatus.NotReview:
                        type = iDAS.Service.Common.Resource.DispatchStatusText.NotReview;
                        break;
                    case (int)iDAS.Service.Common.Resource.DispatchStatus.WaitApprove:
                        type = iDAS.Service.Common.Resource.DispatchStatusText.WaitApprove;
                        break;
                    case (int)iDAS.Service.Common.Resource.DispatchStatus.Approve:
                        type = iDAS.Service.Common.Resource.DispatchStatusText.Approve;
                        break;
                    case (int)iDAS.Service.Common.Resource.DispatchStatus.NotApprove:
                        type = iDAS.Service.Common.Resource.DispatchStatusText.NotApprove;
                        break;
                    case (int)iDAS.Service.Common.Resource.DispatchStatus.Delete:
                        type = iDAS.Service.Common.Resource.DispatchStatusText.Delete;
                        break;
                    case (int)iDAS.Service.Common.Resource.DispatchStatus.WaitSend:
                        type = iDAS.Service.Common.Resource.DispatchStatusText.WaitSend;
                        break;
                    case (int)iDAS.Service.Common.Resource.DispatchStatus.Send:
                        type = iDAS.Service.Common.Resource.DispatchStatusText.Send;
                        break;
                    case (int)iDAS.Service.Common.Resource.DispatchStatus.Archived:
                        type = iDAS.Service.Common.Resource.DispatchStatusText.Archived;
                        break;
                }
                return type;
            }
        }
        public string DepartmentArchiveName { get; set; }
        public string strDepartmentArchiveId { get; set; }
        public EmployeeBO ArchiveEmployee { get; set; }

        public FileUploadBO FileSource { get; set; }

        /// <summary>
        /// Thời gian lưu trữ công văn trong bảng DispatchArchive
        /// </summary>
        public string DispatchArchiveTime { get; set; }

        /// <summary>
        /// Thời hạn lưu trữ là thời gian lưu trữ tại bộ phận lấy trong danh mục
        /// </summary>
        public string DeadlineArchiveText { get; set; }

        /// <summary>
        /// Ngày lưu trữ hồ sơ
        /// - Nếu hồ sơ chưa nộp lưu thì lấy ngày mở hồ sơ
        /// - Nếu hồ sơ đã nộp lưu rồi thì ngày lưu trữ là ngày tiếp nhận trong biên bản bàn giao
        /// </summary>
        public DateTime? ProfileArchiveDate { get; set; }

        /// <summary>
        /// Ngày lưu trữ
        /// - Nếu hồ sơ chưa nộp lưu trữ thì ngày lưu trữ là ngày mở hồ sơ
        /// - Nếu hồ sơ đã nộp lưu trứ thì ngày lưu trữ là ngày tiếp nhận (trong biên bản bàn giao)
        /// </summary>
        public DateTime? DayOfStorage { get; set; }

        public ProfileBorrowBO ProfileBorrow { get; set; }
        public DepartmentBO DepartmentArchive { get; set; }
        public DepartmentBO TransferToDepartment { get; set; }

        public ProfileTransferBO ProfileTransfer { get; set; }
        public string DestroyName { get; set; }

        /// <summary>
        /// Ẩn button thêm thành phần hồ sơ khi xem chi tiết
        /// </summary>
        public bool? IsHiddenButton { get; set; }

        /// <summary>
        /// Kiểm tra ẩn hoặc hiện context menu khi người đăng nhập có phải người tạo hồ sơ không
        /// </summary>
        public bool? HideContextMenuDelete { get; set; }

        public FileUploadBO FileAttachment { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid? ProfileCuriculmviateId { get; set; }
        #endregion
    }
}
