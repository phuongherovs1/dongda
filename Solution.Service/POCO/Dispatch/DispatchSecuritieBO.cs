﻿namespace iDAS.Service
{
    public class DispatchSecuritieBO : BaseBO
    {
        public string Name { get; set; }

        public bool? IsUse { get; set; }

        public string Color { get; set; }

        public string Description { get; set; }
    }
}
