﻿using System;

namespace iDAS.Service
{
    public class DispatchCategoriToBO : BaseBO
    {

        public Guid? DispatchId { get; set; }

        public Guid? DepartmentId { get; set; }

        public Guid? CategoryId { get; set; }

        public string Note { get; set; }
    }
}
