﻿using System;

namespace iDAS.Service
{
    public class DispatchCategorieBO : BaseBO
    {
        public Guid? ParentId { get; set; }

        public Guid? DepartmentId { get; set; }
        public string Name { get; set; }
        public bool? IsGo { get; set; }
        public string CodeRule { get; set; }

        public string Content { get; set; }

        public string Note { get; set; }


        #region Rule
        /// <summary>
        /// Tên phòng ban
        /// </summary>
        public string DepartmentName { get; set; }
        //  <summary>
        /// giá trị: true - category chứa category con, false - ngược lại
        /// </summary>
        public bool IsParent
        {
            get;
            set;
        }
        /// <summary>
        /// Đếm số lượng tài liệu trong danh mục
        /// </summary>
        public int CountProfile { get; set; }
        public int CountProfileTo { get; set; }

        /// <summary>
        /// Tên danh mục
        /// </summary>
        public string NameFormat
        {
            get
            {
                var result = CountProfile > 0 ? string.Format("{0} ({1} Công văn)", Name, CountProfile + CountProfileTo) : string.Format("{0}", Name);
                return result;

            }
        }
        #endregion

    }
}
