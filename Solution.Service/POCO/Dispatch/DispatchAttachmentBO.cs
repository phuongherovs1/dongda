﻿using System;

namespace iDAS.Service
{
    public class DispatchAttachmentBO : BaseBO
    {
        public Guid? DispatchId { get; set; }

        public Guid? FileId { get; set; }

        public bool? IsSource { get; set; }
    }
}
