﻿using System;

namespace iDAS.Service
{
    public class DispatchRecipientsInternalBO : BaseBO
    {
        public Guid? DispatchId { get; set; }

        public Guid? EmployeeId { get; set; }

        public Guid? DepartmentId { get; set; }

        public int? Flag { get; set; }

        public string Note { get; set; }

        public string TypeId { get; set; }
        public string TypeName { get; set; }
        public Guid? IdRelation { get; set; }
    }
}
