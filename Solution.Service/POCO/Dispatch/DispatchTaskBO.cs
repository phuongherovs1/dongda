﻿using System;

namespace iDAS.Service
{
    public class DispatchTaskBO : BaseBO
    {
        public Guid? DispatchId { get; set; }

        public Guid? TaskId { get; set; }
    }
}
