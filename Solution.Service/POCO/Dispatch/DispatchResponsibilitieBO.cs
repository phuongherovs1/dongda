﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DispatchResponsibilitieBO : BaseBO
    {
        public Guid? DispatchId { get; set; }

        public Guid? EmployeeId { get; set; }

        public int? RoleType { get; set; }

        public int? Status { get; set; }
        public string Note { get; set; }

        #region Rule
        public string EmployeeName { get; set; }

        public string DepartmentName { get; set; }

        public string RoleName
        {
            get
            {
                var roleName = string.Empty;
                switch (RoleType)
                {
                    case (int)Resource.DispatchResponsibility.Create:
                        roleName = Resource.DispatchResponsibilityText.Create;
                        break;
                    case (int)Resource.DispatchResponsibility.Approval:
                        roleName = Resource.DispatchResponsibilityText.Approval;
                        break;
                    case (int)Resource.DispatchResponsibility.Review:
                        roleName = Resource.DispatchResponsibilityText.Review;
                        break;
                }
                return roleName;
            }
        }

        public string StatusText { get; set; }
        #endregion
    }
}
