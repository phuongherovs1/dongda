﻿using iDAS.Service.Common;
using System;

namespace iDAS.Service
{
    public class DispatchArchiveBO : BaseBO
    {
        public Guid? DispatchId { get; set; }

        public DateTime? ArchiveDate { get; set; }

        public int? FormalitySave { get; set; }

        public int? ArchiveTime { get; set; }

        public int? Type { get; set; }

        public string Note { get; set; }
        public string TypeDispatch { get { return IsGoDispatch == true ? "Nội bộ" : "Bên ngoài"; } }
        public bool? IsGoDispatch { get; set; }
        /// <summary>
        /// Thời hạn nộp lưu trữ
        /// </summary>
        public DateTime? DeadlineForArchiving { get; set; }

        public Guid EmployeeArchiveId { get; set; }

        public Guid? DepartmentArchiveId { get; set; }

        #region Rule
        public string DispatchName { get; set; }

        public string DispatchCode { get; set; }

        public string DepartmentArchiveName { get; set; }

        /// <summary>
        /// Thời gian lưu trữ
        /// </summary>
        public string DispatchArchiveTime
        {
            get
            {
                string result = string.Empty;
                switch (Type)
                {
                    case (int)Resource.ProfileArchiveTime.Day:
                        result = ArchiveTime + " " + Resource.ArchiveTimeText.Day.ToLower();
                        break;
                    case (int)Resource.ProfileArchiveTime.Month:
                        result = ArchiveTime + " " + Resource.ArchiveTimeText.Month.ToLower();
                        break;
                    case (int)Resource.ProfileArchiveTime.Year:
                        result = ArchiveTime + " " + Resource.ArchiveTimeText.Year.ToLower();
                        break;
                    case (int)Resource.ProfileArchiveTime.Permanent:
                        result = Resource.ArchiveTimeText.Permanent;
                        break;
                    default:
                        break;
                }
                return result;
            }
        }

        /// <summary>
        /// Trạng thái công văn
        /// </summary>
        public string DispatchStatus { get; set; }

        public EmployeeBO ArchiveEmployee { get; set; }

        public DispatchBO Dispatch { get; set; }

        /// <summary>
        /// True: công văn đến, False: công văn đến
        /// </summary>
        public bool DispatchForm { get; set; }
        #endregion
    }
}
