﻿namespace iDAS.Service
{
    public class DispatchSendmethodBO : BaseBO
    {
        public string Name { get; set; }

        public bool? IsUse { get; set; }

        public string Description { get; set; }
    }
}
