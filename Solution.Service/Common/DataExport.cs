﻿using iDAS.ADO;
using System.Data;

namespace iDAS.Service.Common
{
    public class DataExport
    {
        public static DataSet ExportSummaryRelityEmployee(string ListEmployee)
        {
            return Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_ExportSummaryRelityEmployee", parameter: new { ListEmployee = ListEmployee });
        }
    }
}
