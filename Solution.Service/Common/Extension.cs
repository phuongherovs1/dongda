﻿using System;

namespace iDAS.Service.Common
{
    public static class Extension
    {
        public static Guid ToGuid(this string value)
        {
            Guid result = Guid.Empty;
            try
            {
                result = new Guid(value);
            }
            catch (Exception)
            {
            }

            return result;
        }

        public static Guid? ToGuidNull(this string value)
        {
            Guid? result = null;
            try
            {
                result = new Guid(value);
            }
            catch (Exception)
            {
            }

            return result;
        }

        public static Guid NodeToGuid(this string value)
        {
            var result = Guid.Empty;
            try
            {
                if (!string.IsNullOrEmpty(value))
                    if (value.Contains("_"))
                    {
                        var arr = value.Split('_');
                        if (arr != null && arr.Length > 1)
                            result = arr[1].ToGuid();
                    }
                    else
                    {
                        result = value.ToGuid();
                    }
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}
