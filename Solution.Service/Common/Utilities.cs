﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace iDAS.Service.Common
{
    public class Utilities
    {
        /// <summary>
        /// Convert from stream input to array byte
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static byte[] ConvertStreamToByte(Stream stream)
        {
            byte[] buffer;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                buffer = memoryStream.ToArray();
            }
            return buffer;
        }
        /// <summary>
        /// Lấy ngày đầu tháng
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static DateTime GetFirstDayOfMonth(int month, int year)
        {
            DateTime dtReult = new DateTime(year, month, 1);
            dtReult = dtReult.AddDays((-dtReult.Day) + 1);
            return dtReult;
        }
        /// <summary>
        /// Lấy ngày cuối tháng
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static DateTime GetLastDayOfMonth(int month, int year)
        {
            DateTime dtReult = new DateTime(year, month, 1);
            dtReult = dtReult.AddMonths(1);
            dtReult = dtReult.AddDays(-(dtReult.Day));
            return dtReult;
        }
        public class Color
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }
        public class DayOfWeek
        {
            public static string[] Name = new[] { "Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy" };
            public static List<DayBO> GetDayOfWeek(DateTime dayofweek)
            {
                var data = new List<DayBO>();
                int currentDayOfWeek = (int)dayofweek.DayOfWeek;
                var monday = dayofweek.AddDays(-currentDayOfWeek + 1);
                if (currentDayOfWeek == 0)
                {
                    monday = monday.AddDays(-7);
                }
                for (int i = 0; i < 7; i++)
                {
                    var dayBO = new DayBO() { Day = monday.AddDays(i) };
                    data.Add(dayBO);
                }
                return data;
            }
            public static List<int> GetDayOfWeek()
            {
                var data = new List<int>();
                for (int i = 0; i < 7; i++)
                {
                    data.Add(i);
                }
                return data;
            }
            public static string GetNameDayOfWeek(DateTime date)
            {
                int day = (int)date.DayOfWeek;
                return Name[day];
            }

            /// <summary>
            /// The function count all days of week beetween 2 date
            /// </summary>
            /// <param name="dayTypes"></param>
            /// <param name="fromDate"></param>
            /// <param name="toDate"></param>
            /// <returns></returns>
            public static int CountByDayTypeBetweenDate(IEnumerable<int> dayTypes, DateTime fromDate, DateTime toDate)
            {
                //var countDay = 0;
                //foreach (var item in dayTypes)
                //{
                //    countDay += CountByDayTypeBetweenDate(item, fromDate, toDate);
                //}
                //return countDay;
                List<DateTime> dataBetween = Enumerable.Range(0, (int)((toDate - fromDate).TotalDays) + 1)
                  .Select(n => fromDate.AddDays(n))
                   .ToList();
                var countDay = dataBetween.Count(u => dayTypes.Contains((int)u.Date.DayOfWeek));
                return countDay;
            }

            public static int CountByDayTypeBetweenDate(int day, DateTime fromDate, DateTime toDate)
            {
                List<DateTime> dataBetween = Enumerable.Range(0, (int)((toDate - fromDate).TotalDays) + 1)
                    .Select(n => fromDate.AddDays(n))
                     .ToList();
                var countDay = dataBetween.Count(u => (int)u.Date.DayOfWeek == day);
                return countDay;
                //fromDate = fromDate.Date.AddDays((day + 7 - (int)fromDate.DayOfWeek) % 7);
                //var countDay = ((int)(toDate - fromDate).TotalDays) / 7 + 1;
                //return countDay;
            }
        }
        public static List<Color> RenderColor()
        {
            System.Collections.Generic.List<Color> colors = new System.Collections.Generic.List<Color> {
                new Color { Id = "#000000", Name = "#000000" },
                new Color { Id = "#993300", Name = "#993300" },
                new Color { Id = "#333300", Name = "#333300" },
                new Color { Id = "#003300", Name = "#003300" },
                new Color { Id = "#003366", Name = "#003366" },
                new Color { Id = "#000080", Name = "#000080" },
                new Color { Id = "#333399", Name = "#333399" },
                new Color { Id = "#333333", Name = "#333333" },
                new Color { Id = "#800000", Name = "#800000" },
                new Color { Id = "#ff6600", Name = "#ff6600" },
                new Color { Id = "#808000", Name = "#808000" },
                new Color { Id = "#008000", Name = "#008000" },
                new Color { Id = "#008080", Name = "#008080" },
                new Color { Id = "#0000ff", Name = "#0000ff" },
                new Color { Id = "#666699", Name = "#666699" },
                new Color { Id = "#808080", Name = "#808080" },
                new Color { Id = "#ff0000", Name = "#ff0000" },
                new Color { Id = "#ff9900", Name = "#ff9900" },
                new Color { Id = "#99cc00", Name = "#99cc00" },
                new Color { Id = "#339966", Name = "#339966" },
                new Color { Id = "#33cccc", Name = "#33cccc" },
                new Color { Id = "#3366ff", Name = "#3366ff" },
                new Color { Id = "#800080", Name = "#800080" },
                new Color { Id = "#ff00ff", Name = "#ff00ff" },
                new Color { Id = "#ffcc00", Name = "#ffcc00" },
                new Color { Id = "#ffff00", Name = "#ffff00" },
                new Color { Id = "#00ff00", Name = "#00ff00" },
                new Color { Id = "#00ffff", Name = "#00ffff" },
                new Color { Id = "#00ccff", Name = "#00ccff" },
                new Color { Id = "#993366", Name = "#993366" },
                new Color { Id = "#c0c0c0", Name = "#c0c0c0" },
                new Color { Id = "#ff99cc", Name = "#ff99cc" },
                new Color { Id = "#ffcc99", Name = "#ffcc99" },
                new Color { Id = "#ffff99", Name = "#ffff99" },
                new Color { Id = "#ccffcc", Name = "#ccffcc" },
                new Color { Id = "#ccffff", Name = "#ccffff" },
                new Color { Id = "#99ccff", Name = "#99ccff" },
                new Color { Id = "#cc99ff", Name = "#cc99ff" },
                new Color { Id = "#ffffff", Name = "#ffffff" }
            };
            return colors;
        }
        public class ModuleCode
        {
            public const string ModuleTask = "Task";
        }
        public static Guid ConvertToGuid(string str)
        {
            try
            {
                var result = string.IsNullOrEmpty(str) ? Guid.Empty : new Guid(str);
                return result;
            }
            catch (Exception)
            {
                return Guid.Empty;
            }
        }
        public static List<Guid> ToListGuid(string str)
        {
            List<Guid> ids = new List<Guid>(0);
            try
            {
                if (!string.IsNullOrEmpty(str))
                {
                    ids = str.Split(',').Select(n => Guid.Parse(n)).ToList();
                }
            }
            catch (Exception)
            {
                return ids;
            }
            return ids;
        }
        public static string SubString(string str, int length)
        {
            var strSub = "";
            if (str != null && str.Length > length)
            {
                strSub = str.Substring(0, str.LastIndexOf(" ", length)) + "...";
                return strSub;
            }
            else
            {
                return str;
            }
        }
        public static List<int> ToListInt(string str)
        {
            var data = new List<int>();
            if (str != null)
            {
                if (!string.IsNullOrEmpty(str.Trim()))
                {
                    var source = str.Split(',');
                    foreach (var item in source)
                    {
                        if (string.IsNullOrEmpty(item)) continue;
                        var n = 0;
                        try
                        {
                            n = System.Convert.ToInt32(item);
                        }
                        catch
                        {
                            n = 0;
                        }
                        data.Add(n);
                    }
                }
            }
            return data;
        }
        public static byte[] SerializeObject(object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            return Encoding.UTF8.GetBytes(json);
        }
        public static T DerializeObject<T>(byte[] data)
        {
            var json = Encoding.UTF8.GetString(data);
            return JsonConvert.DeserializeObject<T>(json);
        }
        public static Dictionary<int, string> GetObjectRelative()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)Resource.ObjectRelative.Other, Resource.ObjectRelativeText.Other);
            dic.Add((int)Resource.ObjectRelative.Department, Resource.ObjectRelativeText.Department);
            dic.Add((int)Resource.ObjectRelative.Role, Resource.ObjectRelativeText.Role);
            dic.Add((int)Resource.ObjectRelative.Employee, Resource.ObjectRelativeText.Employee);
            return dic;
        }
        public static Dictionary<int, string> GetLogicReview()
        {
            var dict = new Dictionary<int, string>();
            dict.Add((int)Resource.QualityReview.IsSecurity, Resource.QualityReviewText.IsSecurity);
            dict.Add((int)Resource.QualityReview.IsInviblate, Resource.QualityReviewText.IsInviblate);
            dict.Add((int)Resource.QualityReview.IsReady, Resource.QualityReviewText.IsReady);
            dict.Add((int)Resource.QualityReview.IsImportant, Resource.QualityReviewText.IsImportant);
            return dict;
        }
        public static Dictionary<string, string> GetTypeData()
        {
            var dict = new Dictionary<string, string>();
            dict.Add(Resource.TypeDataText.Currency, Resource.TypeDataText.Currency);
            dict.Add(Resource.TypeDataText.Date, Resource.TypeDataText.Date);
            dict.Add(Resource.TypeDataText.Number, Resource.TypeDataText.Number);
            dict.Add(Resource.TypeDataText.Percentage, Resource.TypeDataText.Percentage);
            dict.Add(Resource.TypeDataText.Text, Resource.TypeDataText.Text);
            dict.Add(Resource.TypeDataText.Time, Resource.TypeDataText.Time);
            return dict;
        }
    }
}