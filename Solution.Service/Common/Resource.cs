﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace iDAS.Service.Common
{
    public class Resource
    {
        public class DocumentText
        {
            public const string All = "Tất cả";
            public const string NewDocument = "Tài liệu mới";
            public const string ApproveDocument = "Phê duyệt tài liệu";
            public const string RequestAdjust = "Yêu cầu sửa đổi";
            public const string RequestNew = "Yêu cầu tạo mới";
            public const string SuggestAdjust = "Đề nghị sửa đổi";
            public const string SuggestNew = "Đề nghị tạo mới";
            public const string RequestAppove = "Duyệt tài liệu";
            public const string New = "Mới";
            public const string ApproveFailed = "Duyệt không đạt";
            public const string Approve = "Đã duyệt";
            public const string Pending = "Chờ duyệt";
            public const string Publish = "Ban hành";
            public const string Edit = "Sửa đổi";
            public const string DefaultVersion = "01";

        }
        public class AssociateType
        {
            public const string UnRequired = "Đồng thời";
            public const string SemiRequired = "Trước";
            public const string Required = "Sau";
        }
        public class CommonLabel
        {
            public const string Undefine = "Chưa xác định";
        }
        public class CommonStringFormat
        {
            public const string CreatedDate = "Đã tạo được {0} ngày {1} giờ.";
            public const string RemainDate = "Có {0} ngày {1} giờ thực hiện";
            /// <summary>
            /// Tạo mới: {Tên tài liệu},phiên bản {tên phiên bản}
            /// </summary>
            public const string DocumentCreateTask = "Tạo mới: {0} phiên bản {1}";
            /// <summary>
            /// Sửa đổi: {Tên tài liệu},phiên bản {Tên phiên bản}, lần cập nhật {lần cập nhật}
            /// </summary>
            public const string DocumentAdjustTask = "Sửa đổi: {0} phiên bản {1} lần cập nhật {2}";
            /// <summary>
            /// Sửa đổi {Tên tài liệu} phiên bản {tên phiên bản} lần cập nhật{lần cập nhật} lần thứ {lần thứ}
            /// </summary>
            public const string DocumentReAdjustTask = "Sửa đổi: {0} phiên bản {1} lần cập nhật {2} lần thứ {3}";
        }
        public class Task
        {
            public enum Status
            {
                NotFinish,
                New,
                WaitAssign,
                WaitPerform,
                Perform,
                Pause,
                NotComplete,
                Complete,
                Cancel,
                Finish,
                OutOfDate,
                All,
                Other
            }

            public enum StatusNew
            {
                ALL,
                CTH,
                DTHTH,
                DTHQH,
                KTDH,
                KTQH,
                HUY,
                TD
            }

            public class StatusTextNew
            {
                public const string All = "Tất cả";
                public const string CTH = "Chờ thực hiện";
                public const string DTHTH = "Đang thực hiện (trong hạn)";
                public const string DTHQH = "Đang thực hiện (quá hạn)";
                public const string KTDH = "Kết thúc (đúng hạn)";
                public const string KTQH = "Kết thúc (quá hạn)";
                public const string HUY = "Hủy";
                public const string TD = "Tạm dừng";
            }

            public class StatusText
            {
                public const string All = "Tất cả";
                public const string New = "Mới";
                public const string WaitAssign = "Chưa giao";
                public const string WaitPerform = "Chờ thực hiện";
                public const string Perform = "Đang thực hiện";
                public const string Pause = "Tạm dừng";
                public const string NotComplete = "Chưa hoàn thành";
                public const string Complete = "Hoàn thành";
                public const string NotFinish = "Chưa kết thúc";
                public const string Cancel = "Hủy";
                public const string Finish = "Kết thúc";
            }


            public class StatusColor
            {
                public const string All = "transparent";
                public const string New = "#B92D14";
                public const string WaitAssign = "#3F6ABA";
                public const string WaitPerform = "#833C0C";
                public const string Perform = "#048dcc";
                public const string Pause = "#806000";
                public const string NotComplete = "#00A030";
                public const string Complete = "#00B050";
                public const string NotFinish = "transparent";
                public const string Cancel = "#000000";
                public const string Finish = "#203764";
            }
            public static Dictionary<int, string> GetStatusNotFinish()
            {
                var dic = new Dictionary<int, string>();
                dic.Add((int)Status.All, StatusText.All);
                dic.Add((int)Status.New, StatusText.New);
                dic.Add((int)Status.NotFinish, StatusText.NotFinish);
                dic.Add((int)Status.WaitPerform, StatusText.WaitPerform);
                dic.Add((int)Status.Perform, StatusText.Perform);
                dic.Add((int)Status.Pause, StatusText.Pause);
                dic.Add((int)Status.Complete, StatusText.Complete);
                dic.Add((int)Status.Cancel, StatusText.Cancel);
                return dic;
            }

            public static Dictionary<int, string> GetStatusNew()
            {
                var dic = new Dictionary<int, string>();
                dic.Add((int)StatusNew.ALL, StatusTextNew.All);
                dic.Add((int)StatusNew.CTH, StatusTextNew.CTH);
                dic.Add((int)StatusNew.DTHTH, StatusTextNew.DTHTH);
                dic.Add((int)StatusNew.DTHQH, StatusTextNew.DTHQH);
                dic.Add((int)StatusNew.KTDH, StatusTextNew.KTDH);
                dic.Add((int)StatusNew.KTQH, StatusTextNew.KTQH);
                dic.Add((int)StatusNew.HUY, StatusTextNew.HUY);
                dic.Add((int)StatusNew.TD, StatusTextNew.TD);
                return dic;
            }
            public static Dictionary<int, string> GetStatus()
            {
                var dic = new Dictionary<int, string>();
                var source = Enum.GetValues(typeof(Status)).Cast<Status>();
                foreach (var item in source)
                {
                    var text = GetStatusText(item);
                    var key = (int)item;
                    dic.Add(key, text);
                }
                return dic;
            }
            public static string GetStatusText(Status status)
            {
                var result = string.Empty;
                switch (status)
                {
                    case Status.New: result = StatusText.New; break;
                    case Status.WaitAssign: result = StatusText.WaitAssign; break;
                    case Status.WaitPerform: result = StatusText.WaitPerform; break;
                    case Status.Perform: result = StatusText.Perform; break;
                    case Status.Pause: result = StatusText.Pause; break;
                    case Status.NotComplete: result = StatusText.NotComplete; break;
                    case Status.Complete: result = StatusText.Complete; break;
                    case Status.Cancel: result = StatusText.Cancel; break;
                    case Status.Finish: result = StatusText.Finish; break;
                    case Status.All: result = StatusText.All; break;
                }
                return result;
            }
            public static string GetStatusColor(Status status)
            {
                var result = string.Empty;
                switch (status)
                {
                    case Status.New: result = StatusColor.New; break;
                    case Status.WaitAssign: result = StatusColor.WaitAssign; break;
                    case Status.WaitPerform: result = StatusColor.WaitPerform; break;
                    case Status.Perform: result = StatusColor.Perform; break;
                    case Status.Pause: result = StatusColor.Pause; break;
                    case Status.NotComplete: result = StatusColor.NotComplete; break;
                    case Status.Complete: result = StatusColor.Complete; break;
                    case Status.Cancel: result = StatusColor.Cancel; break;
                    case Status.Finish: result = StatusColor.Finish; break;
                    case Status.All: result = StatusColor.All; break;
                }
                return result;
            }

            public enum Role
            {
                All,
                Assigner,
                Performer,
                Reviewer,
                Viewer,
                Creator,
                NotPerformer,
                Mark
            }
            public class RoleText
            {
                public const string All = "Tất cả";
                public const string Assigner = "Người giao việc";
                public const string Performer = "Người thực hiện";
                public const string Reviewer = "Người kiểm soát";
                public const string Viewer = "Người theo dõi";
                public const string Creator = "Người tạo việc";
                public const string Mark = "Người cho điểm";
            }
            public static Dictionary<int, string> GetResourceRoles()
            {
                var dic = new Dictionary<int, string>();
                var source = Enum.GetValues(typeof(Role)).Cast<Role>();
                foreach (var item in source)
                {
                    var text = GetResourceRoleText(item);
                    if (item != Role.NotPerformer)
                    {
                        var key = (int)item;
                        dic.Add(key, text);
                    }
                }
                return dic;
            }
            public static Dictionary<int, string> GetYear(int number = 5)
            {
                var dic = new Dictionary<int, string>();
                for (int i = 0; i < number; i++)
                {
                    string year = (DateTime.Now.Year - i).ToString();
                    dic.Add(DateTime.Now.Year - i, year);
                }
                return dic;
            }
            public static string GetResourceRoleText(Role resourceRole)
            {
                var result = string.Empty;
                switch (resourceRole)
                {
                    case Role.Assigner: result = RoleText.Assigner; break;
                    case Role.Reviewer: result = RoleText.Reviewer; break;
                    case Role.Viewer: result = RoleText.Viewer; break;
                    case Role.Performer: result = RoleText.Performer; break;
                    case Role.Creator: result = RoleText.Creator; break;
                    case Role.All: result = RoleText.All; break;
                    case Role.Mark: result = RoleText.Mark; break;
                }
                return result;
            }

            public enum ScheduleStatus
            {
                All,
                New,
                WaitConfirm,
                Reject,
                Change,
                Accept,
                Pause,
                Cancel,
                Perform,
                Finish,
                NotClosed,
                Closed,
            }
            public class ScheduleStatusText
            {
                public const string All = "Tất cả";
                public const string New = "Chưa gửi";
                public const string WaitConfirm = "Chờ xác nhận";
                public const string Reject = "Từ chối";
                public const string Change = "Cần điều chỉnh";
                public const string Accept = "Đồng ý";
                public const string Pause = "Tạm dừng";
                public const string Cancel = "Hủy";
                public const string Perform = "Đang thực hiện";
                public const string Finish = "Kết thúc";
                public const string NotClosed = "Chưa chốt";
                public const string Closed = "Đã chốt";
            }


            public class ScheduleStatusColor
            {
                public const string All = "transparent";
                public const string New = "#ca9634";
                public const string WaitConfirm = "#ca5f1f";
                public const string Reject = "#101010";
                public const string Change = "#0c84f1";
                public const string Accept = "#3c800b";
                public const string Pause = "#929292";
                public const string Cancel = "#ff0626";
                public const string Perform = "#14AB14";
                public const string Finish = "#075EBD";
                public const string NotClosed = "#653125";
                public const string Closed = "#127107";
            }
            public static Dictionary<int, string> GetScheduleStatus()
            {
                var dic = new Dictionary<int, string>();
                var source = Enum.GetValues(typeof(ScheduleStatus)).Cast<ScheduleStatus>();
                foreach (var item in source)
                {
                    var text = GetScheduleStatusText(item);
                    var key = (int)item;
                    dic.Add(key, text);
                }
                return dic;
            }
            public static string GetScheduleStatusText(ScheduleStatus status)
            {
                var result = string.Empty;
                switch (status)
                {
                    case ScheduleStatus.New: result = ScheduleStatusText.New; break;
                    case ScheduleStatus.WaitConfirm: result = ScheduleStatusText.WaitConfirm; break;
                    case ScheduleStatus.Reject: result = ScheduleStatusText.Reject; break;
                    case ScheduleStatus.Change: result = ScheduleStatusText.Change; break;
                    case ScheduleStatus.Accept: result = ScheduleStatusText.Accept; break;
                    case ScheduleStatus.Pause: result = ScheduleStatusText.Pause; break;
                    case ScheduleStatus.Cancel: result = ScheduleStatusText.Cancel; break;
                    case ScheduleStatus.Perform: result = ScheduleStatusText.Perform; break;
                    case ScheduleStatus.Finish: result = ScheduleStatusText.Finish; break;
                    case ScheduleStatus.Closed: result = ScheduleStatusText.Closed; break;
                    case ScheduleStatus.NotClosed: result = ScheduleStatusText.NotClosed; break;
                    case ScheduleStatus.All: result = ScheduleStatusText.All; break;
                }
                return result;
            }
            public static string GetScheduleStatusColor(ScheduleStatus status)
            {
                var result = string.Empty;
                switch (status)
                {
                    case ScheduleStatus.New: result = ScheduleStatusColor.New; break;
                    case ScheduleStatus.WaitConfirm: result = ScheduleStatusColor.WaitConfirm; break;
                    case ScheduleStatus.Reject: result = ScheduleStatusColor.Reject; break;
                    case ScheduleStatus.Change: result = ScheduleStatusColor.Change; break;
                    case ScheduleStatus.Accept: result = ScheduleStatusColor.Accept; break;
                    case ScheduleStatus.Pause: result = ScheduleStatusColor.Pause; break;
                    case ScheduleStatus.Cancel: result = ScheduleStatusColor.Cancel; break;
                    case ScheduleStatus.Perform: result = ScheduleStatusColor.Perform; break;
                    case ScheduleStatus.Finish: result = ScheduleStatusColor.Finish; break;
                    case ScheduleStatus.Closed: result = ScheduleStatusColor.Closed; break;
                    case ScheduleStatus.NotClosed: result = ScheduleStatusColor.NotClosed; break;
                    case ScheduleStatus.All: result = ScheduleStatusColor.All; break;
                }
                return result;
            }
            public class Exception
            {
                public const string VersionIsNotValid = "Phiên bản không hợp lệ!";
                public const string NameExist = "Tên công việc này đã tồn tại trên hệ thống!";
                public const string ConfigScheduleTimeExist = "Lịch công tác đã tồn tại!";
                public const string ConfigTimeInvalid = "Thiết lập thời gian không phù hợp!";
                public const string ConfigStartTimeInvalid = "Thiết lập thời gian bắt đầu nhỏ hơn thời gian hiện tại!";
                public const string ConfigDeadlineInvalid = "Thiết lập thời hạn cuối không phù hợp!";
                public const string EmployeeExist = "Nhân sự này đã được chọn!";
                public const string SystemFailure = "Hệ thống xử lý phát sinh lỗi!";
                public const string CodeExist = "Tài liệu đã tồn tại!";
            }
            public static Dictionary<int, string> GetCommunicationType()
            {
                var dic = new Dictionary<int, string>();

                dic.Add((int)CommunicationType.All, CommunicationText.All);
                dic.Add((int)CommunicationType.Report, CommunicationText.Report);
                dic.Add((int)CommunicationType.Suggest, CommunicationText.Suggest);
                dic.Add((int)CommunicationType.Requirement, CommunicationText.Requirement);
                dic.Add((int)CommunicationType.Other, CommunicationText.Other);
                return dic;
            }
            public static Dictionary<int, string> GetCommunicationTypeAdd()
            {
                var dic = new Dictionary<int, string>();

                dic.Add((int)CommunicationType.Suggest, CommunicationText.Suggest);
                dic.Add((int)CommunicationType.Requirement, CommunicationText.Requirement);
                dic.Add((int)CommunicationType.Other, CommunicationText.Other);
                return dic;
            }
            public enum CommunicationType
            {
                All,
                Report,
                Suggest,
                Requirement,
                Other,
            }
            public class CommunicationText
            {
                public const string All = "Tất cả";
                public const string Report = "Báo cáo công việc";
                public const string Suggest = "Đề xuất công việc";
                public const string Requirement = "Yêu cầu công việc";
                public const string Other = "Trao đổi khác";

            }
        }
        public class Calendar
        {
            public static string GetDayOfWeekText(DayOfWeek dayOfWeek)
            {
                var result = string.Empty;
                switch (dayOfWeek)
                {
                    case DayOfWeek.Monday: result = "Thứ hai"; break;
                    case DayOfWeek.Tuesday: result = "Thứ ba"; break;
                    case DayOfWeek.Wednesday: result = "Thứ tư"; break;
                    case DayOfWeek.Thursday: result = "Thứ năm"; break;
                    case DayOfWeek.Friday: result = "Thứ sáu"; break;
                    case DayOfWeek.Saturday: result = "Thứ bảy"; break;
                    case DayOfWeek.Sunday: result = "Chủ nhật"; break;
                }
                return result;
            }
            public static IEnumerable<DayBO> GetWeek(DateTime date)
            {
                var data = new List<DayBO>();
                int currentDayOfWeek = (int)date.DayOfWeek;
                var monday = date.AddDays((currentDayOfWeek == 0 ? -7 : -currentDayOfWeek) + 1);
                for (int i = 0; i < 7; i++)
                {
                    data.Add(new DayBO() { Day = monday.AddDays(i) });
                }
                return data;
            }
        }
        public class File
        {
            public static string GetException(iDAS.Service.FileServiceReference.FileError error)
            {
                var message = string.Empty;
                switch (error)
                {
                    case FileServiceReference.FileError.CodeNullOrEmpty: break;
                    case FileServiceReference.FileError.Exist: break;
                    case FileServiceReference.FileError.Fail: break;
                    case FileServiceReference.FileError.FileNullOrEmpty: break;
                    case FileServiceReference.FileError.NotFound: break;
                    case FileServiceReference.FileError.NotView: break;
                    case FileServiceReference.FileError.OverSize: break;
                    case FileServiceReference.FileError.Success: break;
                }
                return message;
            }
            public class Exception
            {
                public const string CodeNullOrEmpty = "Mã hệ thống không phù hợp!";
                public const string Exist = "Tệp đã tồn tại trên hệ thống!";
                public const string Fail = "Hệ thống xử lý phát sinh lỗi!";
                public const string FileNullOrEmpty = "Tệp rỗng không có kích cỡ!";
                public const string NotFound = "Không tìm thấy tệp phù hợp!";
                public const string NotView = "Không cho phép xem loại tệp này!";
                public const string OverSize = "Tệp tin quá kích cỡ cho phép!";
                public const string Success = "Hệ thống xử lý thành công!";
            }
        }

        public class FileExtension
        {
            public const string Word2007 = "docx";
        }
        #region Enum
        public enum OrganizationTreeType
        {
            Department = 1,
            Title = 2,
            Employee = 3
        }

        public enum StatisRole
        {
            Perform = 0,
            Assign = 1
        }
        public enum Operation
        {
            Create,
            Update,
            UpdateDelete,
            Read,
        }
        public enum QualityReview
        {
            IsSecurity = 0,
            IsInviblate = 1,
            IsReady = 2,
            IsImportant = 3
        }
        public enum DestinationType
        {
            Department = 1,
            Title = 2,
            Employee = 3,
            External = 4
        }
        public enum DocumentAction
        {
            /// <summary>
            /// Yêu cầu tạo mới
            /// </summary>
            RequestNew = 1,
            /// <summary>
            /// Soạn thảo
            /// </summary>
            Write = 2,
            /// <summary>
            /// Đề nghị kiểm duyệt
            /// </summary>
            SuggestReview = 3,
            /// <summary>
            /// Đề nghị ban hành
            /// </summary>
            SuggestPublish = 4,
            /// <summary>
            /// Phê duyệt và ban hành
            /// </summary>
            Approve = 5,
            /// <summary>
            /// Lưu trữ
            /// </summary>
            Archive = 6,
            /// <summary>
            /// Phân phối
            /// </summary>
            Distribute = 7,
            /// <summary>
            /// Thu hồi
            /// </summary>
            Retrive = 8,
            /// <summary>
            /// Lỗi thời
            /// </summary>
            Obsolate = 9


        }
        public enum DasboardDocumentSuggestType
        {
            All = 0,
            RequestAppove = 1,
            RequestNew = 2,
            RequestAdjust = 3,
            SuggestNew = 4,
            SuggestAdjust = 5
        }
        public enum DocumentSuggestType
        {
            WriteNew = 1,
            Adjust = 2,
            Check = 3,
            Distribute = 4,
            Apply = 5,
            Destroy = 6,
            Promulgate = 7,//Ban hành
            Archive = 8//Lưu trữ

        }
        public enum DocumentType
        {
            All = 0,
            New = 1,
            Adjust = 2,
            RequestNew = 3,
            RequestAdjust = 4,
            SuggestNew = 5,
            SuggestAdjust = 6
        }

        public enum TypeData
        {
            Number = 0,
            Currency = 1,
            Date = 2,
            Time = 3,
            Percentage = 4,
            Text = 5
        }

        public enum ObjectRelative
        {
            Other = 0,
            Department = 1,
            Role = 2,
            Employee = 3
        }

        public enum DocumentStatus
        {
            All = 0,
            /// <summary>
            /// Mới
            /// </summary>
            New = 1,
            /// <summary>
            /// Chờ biên soạn
            /// </summary>
            WaitCompile = 2,
            /// <summary>
            /// Đang biên soạn
            /// </summary>
            Compile = 3,
            /// <summary>
            /// Chờ soạn thảo
            /// </summary>
            WaitWrite = 4,
            /// <summary>
            /// Đang soạn thảo
            /// </summary>
            Write = 5,
            /// <summary>
            /// Chờ kiểm duyệt
            /// </summary>
            WaitReview = 6,
            /// <summary>
            /// Duyệt đạt
            /// </summary>
            ApproveReview = 7,
            /// <summary>
            /// Duyệt không đạt
            /// </summary>
            RejectReview = 8,
            /// <summary>
            /// Chờ duyệt ban hành
            /// </summary>
            WaitPublish = 9,
            /// <summary>
            /// Duyệt đề nghị ban hành
            /// </summary>
            ApprovePublish = 10,
            /// <summary>
            /// Không duyệt đề nghị ban hành
            /// </summary>
            RejectPublish = 11,
            /// <summary>
            /// Ban hành
            /// </summary>
            Publish = 12,
            /// <summary>
            /// Lỗi thời
            /// </summary>
            Obsolate = 13,
            /// <summary>
            /// Hết hiệu lực
            /// </summary>
            Expire = 14,
            /// <summary>
            /// Hủy
            /// </summary>
            Destroy = 15,
            /// <summary>
            /// Chưa hủy
            /// </summary>
            NotDestroy = 16
        }
        public enum EDocumentStatus
        {
            /// <summary>
            /// Tất cả
            /// </summary>
            All = 0,
            /// <summary>
            /// Chờ ban hành
            /// </summary>
            New = 1,
            /// <summary>
            /// Chờ ban hành
            /// </summary>
            WaitPublish = 2,
            /// <summary>
            /// Ban hành
            /// </summary>
            Publish = 3,
            /// <summary>
            /// Lỗi thời
            /// </summary>
            Obsolate = 4,
            /// <summary>
            /// Hết hiệu lực
            /// </summary>
            Expire = 5,
            /// <summary>
            /// Hủy
            /// </summary>
            Destroy = 6
        }
        public enum ApproveStatus
        {
            All = 0,
            New = 1,
            Wait = 2,
            Approve = 3,
            Reject = 4
        }

        public enum RequestStatus
        {
            All = 0,
            New = 1,
            Wait = 2,
            Perform = 3,
            Complete = 4,
            Finish = 5,
            Destroy = 6
        }

        /// <summary>
        /// Theo dõi trạng thái thực hiện của yêu cầu
        /// </summary>
        public enum RequestProcessStatus
        {
            /// <summary>
            /// Tất cả
            /// </summary>
            None = 0,
            /// <summary>
            /// Mới 
            /// </summary>
            New = 1,
            /// <summary>
            /// Chờ phân công
            /// </summary>
            WaitAssign = 2,
            /// <summary>
            /// Chờ thực hiện
            /// </summary>
            WaitPerform = 3,
            /// <summary>
            /// Đang thực hiện
            /// </summary>
            Perform = 4,
            /// <summary>
            /// Chờ kiểm tra
            /// </summary>
            WaitReview = 5,
            /// <summary>
            /// Kiểm tra duyệt không đạt
            /// </summary>
            ReviewReject = 6,
            /// <summary>
            /// Chờ gửi duyệt
            /// </summary>
            WaitSendApprove = 7,
            /// <summary>
            /// Chờ duyệt
            /// </summary>
            WaitApprove = 8,
            /// <summary>
            /// Hoàn thành
            /// </summary>
            Complete = 9,
            /// <summary>
            /// Hủy
            /// </summary>
            Cancel = 10,
            /// <summary>
            /// Quá hạn
            /// </summary>
            OutOfDate = 11
        }

        public class RequestProcessStatusText
        {
            /// <summary>
            /// Tất cả
            /// </summary>
            public const string None = "Tất cả";
            /// <summary>
            /// Mới 
            /// </summary>
            public const string New = "Mới";
            /// <summary>
            /// Chờ phân công
            /// </summary>
            public const string WaitAssign = "Chờ phân công";
            /// <summary>
            /// Chờ thực hiện
            /// </summary>
            public const string WaitPerform = "Chờ thực hiện";
            /// <summary>
            /// Đang thực hiện
            /// </summary>
            public const string Perform = "Đang thực hiện";
            /// <summary>
            /// Chờ kiểm tra
            /// </summary>
            public const string WaitReview = "Chờ kiểm tra";
            /// <summary>
            /// Kiểm tra duyệt không đạt
            /// </summary>
            public const string ReviewReject = "Không đạt";
            /// <summary>
            /// Chờ gửi duyệt
            /// </summary>
            public const string WaitSendApprove = "Chờ gửi duyệt";
            /// <summary>
            /// Chờ duyệt
            /// </summary>
            public const string WaitApprove = "Chờ duyệt";
            /// <summary>
            /// Hoàn thành
            /// </summary>
            public const string Complete = "Hoàn thành";
            /// <summary>
            /// Hủy
            /// </summary>
            public const string Cancel = "Hủy";
            /// <summary>
            /// Quá hạn
            /// </summary>
            public const string OutOfDate = "Quá hạn";
        }

        public class RequestStatusColor
        {
            public const string New = "#ff0000";
            public const string Wait = "#2027ff";
            public const string Perform = "#0f94e4";
            public const string Complete = "#01882b";
            public const string Finish = "#0029c3";
            public const string Destroy = "#afafaf";
        }
        public class SuggestStatusColor
        {
            public const string New = "#ff0000";
            public const string Wait = "#04ea8b";
            public const string Accept = "#0f94e4";
            public const string NotAccept = "#ca3995";
            public const string Cancel = "#c30000";
        }
        #endregion
        #region Enum Text
        public class ObjectRelativeText
        {
            public const string Department = "Phòng ban";
            public const string Role = "Chức vụ";
            public const string Employee = "Nhân sự";
            public const string Other = "Khác";
        }
        public class TypeDataText
        {
            public const string Number = "Kiểu số";
            public const string Currency = "Kiểu tiền tệ";
            public const string Date = "Kiểu ngày tháng";
            public const string Time = "Kiểu thời gian";
            public const string Percentage = "Kiểu phần trăm";
            public const string Text = "Kiểu chuỗi";
        }
        public class QualityReviewText
        {
            public const string IsSecurity = "Tính bảo mật";
            public const string IsInviblate = "Tính toàn vẹn";
            public const string IsReady = "Tính sẵn sàng";
            public const string IsImportant = "Tính quan trọng";
        }
        public class DocumentTypeProcessText
        {
            public const string WriteAndEditSuggest = "ĐỀ NGHỊ VIẾT MỚI SỬA ĐỔI TÀI LIỆU";
            public const string WriteAndRelatetion = "SOẠN THẢO VÀ BAN HÀNH TÀI LIỆU";
            public const string DistributeSuggest = "ĐỀ NGHỊ PHÂN PHỐI TÀI LIỆU";
            public const string CancelSuggest = "ĐỀ NGHỊ HỦY TÀI LIỆU";
        }
        public class DocumentProcessRoleText
        {
            /// <summary>
            /// Người đề nghị viết mới và sửa đổi
            /// </summary>
            public const string SuggesterWriteAndEdit = "Đề nghị";
            /// <summary>
            /// Người xem xét đề nghị viết mới và sửa đổi
            /// </summary>
            public const string ReviewWriteAndEdit = "Xem xét";
            /// <summary>
            /// Người phê duyệt đề nghị viết mới và sửa đổi
            /// </summary>            
            public const string ApprovalWriteAndEdit = "Phê duyệt";
            /// <summary>
            /// Người phân công viết mới sửa đổi
            /// </summary>
            public const string AssignmentWriteAndEdit = "Phân công";
            /// <summary>
            /// Người soạn thảo
            /// </summary>
            public const string Write = "Soạn thảo";
            /// <summary>
            /// Người kiểm tra
            /// </summary>
            public const string Check = "Kiểm tra";
            /// <summary>
            /// Người phê duyệt tài liệu
            /// </summary>
            public const string ApproveDocument = "Phê duyệt";
            /// <summary>
            /// Người ban hành tài liệu
            /// </summary>
            public const string Promulgate = "Ban hành";
            /// Người lưu trữ tài liệu
            /// </summary>
            public const string Archiver = "Lưu trữ";
            /// <summary>
            /// Người đề nghị phân phối tài liệu
            /// </summary>
            public const string DistributeSuggester = "Đề nghị";
            /// <summary>
            /// Người xem xét đề nghị phân phối
            /// </summary>
            public const string DistributeSuggestReviewer = "Xem xét";
            /// <summary>
            /// Người xem xét đề nghị phân phối
            /// </summary>
            public const string DistributeSuggestApproval = "Phê duyệt";
            /// <summary>
            /// Người phân phối tài liệu
            /// </summary>
            public const string Distributer = "Phân phối";
            /// <summary>
            /// Người tạo đề nghị hủy tài liệu
            /// </summary>
            public const string DestroyCreater = "Đề nghị";
            /// <summary>
            /// Người xem xét đề ngị hủy
            /// </summary>
            public const string DestroyReviewer = "Xem xét";
            /// <summary>
            /// Người phê duyệt đề nghị hủy
            /// </summary>
            public const string DestroyApproval = "Phê duyệt";
            /// <summary>
            /// Người lập biên bản hủy
            /// </summary>
            public const string DestroyBillCreater = "Lập biên bản hủy";
            /// <summary>
            /// Yêu cầu phân phối
            /// </summary>
            public const string DistributeRequester = "Yêu cầu phân phối";
            /// <summary>
            /// 
            /// </summary>
            public const string Destroy = "Hủy";

        }
        public class DocumentStatusText
        {
            public const string All = "Tất cả";
            /// <summary>
            /// Mới
            /// </summary>
            public const string New = "Mới";
            /// <summary>
            /// Chờ biên soạn
            /// </summary>
            public const string WaitCompile = "Chờ biên soạn";
            /// <summary>
            /// Đang biên soạn
            /// </summary>
            public const string Compile = "Đang biên soạn";
            /// <summary>
            /// Chờ soạn thảo
            /// </summary>
            public const string WaitWrite = "Chờ soạn thảo";
            /// <summary>
            /// Đang soạn thảo
            /// </summary>
            public const string Write = "Đang soạn thảo";
            /// <summary>
            /// Chờ kiểm duyệt
            /// </summary>
            public const string WaitReview = "Chờ kiểm duyệt";
            /// <summary>
            /// Duyệt đạt
            /// </summary>
            public const string ApproveReview = "Duyệt đạt";
            /// <summary>
            /// Duyệt không đạt
            /// </summary>
            public const string RejectReview = "Duyệt không đạt";
            /// <summary>
            /// Chờ duyệt ban hành
            /// </summary>
            public const string WaitPublish = "Chờ duyệt ban hành";
            /// <summary>
            /// Duyệt đề nghị ban hành
            /// </summary>
            public const string ApprovePublish = "Duyệt đề nghị ban hành";
            /// <summary>
            /// Không duyệt đề nghị ban hành
            /// </summary>
            public const string RejectPublish = "Không duyệt đề nghị ban hành";
            /// <summary>
            /// Ban hành
            /// </summary>
            public const string Publish = "Ban hành";
            /// <summary>
            /// Lỗi thời
            /// </summary>
            public const string Obsolate = "Lỗi thời";
            /// <summary>
            /// Hết hiệu lực
            /// </summary>
            public const string Expire = "Hết hiệu lực";
            public const string ApplyAt = "Có hiệu lực";
            public const string ApproveComplete = "Đã duyệt";
            /// <summary>
            /// Hủy
            /// </summary>
            public const string Destroy = "Hủy";
            /// <summary>
            /// Chưa hủy
            /// </summary>
            public const string NotDestroy = "Chưa hủy";
        }

        public class EDocumentStatusText
        {
            /// <summary>
            /// Tất cả
            /// </summary>
            public const string All = "Tất cả";
            /// <summary>
            /// Mới
            /// </summary>
            public const string New = "Mới";
            /// <summary>
            /// Chờ ban hành
            /// </summary>
            public const string WaitPublish = "Chờ ban hành";
            /// <summary>
            /// Ban hành
            /// </summary>
            public const string Publish = "Ban hành";
            /// <summary>
            /// Lỗi thời
            /// </summary>
            public const string Obsolate = "Lỗi thời";
            /// <summary>
            /// Hết hiệu lực
            /// </summary>
            public const string Expire = "Hết hiệu lực";
            /// <summary>
            /// Hủy
            /// </summary>
            public const string Destroy = "Hủy";

        }
        public class DocumentSuggestStatus
        {
            public const string New = "Mới";
            public const string Review = "Chờ xem xét";
            public const string Approve = "Chờ duyệt";
            public const string ApproveAccept = "Duyệt";
            public const string ApproveFail = "Không duyệt";
            public const string OverwriteTime = "Quá hạn";
            public const string Destroy = "Hủy";
            public const string WaitBillCreate = "Chờ thực hiện";
            public const string AcceptFail = "Không đạt";
            public const string SendWait = "Chờ gửi";
            public const string BillCreated = "Đã lập biên bản";
            public const string Assigned = "Đã giao soạn thảo";
            public const string WaitDestroy = "Chờ hủy";
            public const string Waitcheck = "Chờ kiểm tra";
            public const string AssignWrite = "Giao soạn thảo";
            public const string Destroyed = "Đã hủy";
        }
        public enum DocumentSuggestStatusType
        {
            All = 0,
            // Mới
            New = 1,
            // Chờ xem xét
            Review = 2,
            // Chờ duyệt
            Approve = 3,
            ApproveAccept = 4,
            ApproveFail = 5,
            OverwriteTime = 6,
            Destroy = 7,
            WaitBillCreate = 8,
            SendWait = 9,
            BillCreated = 10,
            AcceptFail = 11,
            Assigned = 12,
            WaitDestroy = 13,
            Waitcheck = 14,
            AssignWrite = 15,
            Destroyed = 16

        }
        public class ApproveStatusText
        {
            public const string All = "Tất cả";
            public const string New = "Mới";
            public const string Wait = "Chờ duyệt";
            public const string WaitCheck = "Chờ kiểm tra";
            public const string WaitPromulgate = "Chờ ban hành";
            public const string Promulgated = "Đã ban hành";
            public const string WaitArchive = "Chờ lưu trữ";
            public const string Archived = "Đã lưu trữ";
            public const string Approve = "Duyệt đạt";
            public const string CheckAccept = "Kiểm tra đạt";
            public const string CheckNotAccept = "Kiểm tra không đạt";
            public const string Reject = "Duyệt không đạt";
            public const string WaitDistribute = "Chờ phân phối";
            public const string Distributed = "Đã phân phối";
            public const string Destroy = "Hủy";
            public const string WaitReview = "Chờ xem xét";
            public const string WaitSend = "Chờ gửi";
            public const string WaitDestroy = "Chờ hủy";
            public const string Destroyed = "Đã hủy";
            public const string ReviewAccept = "Xem xét đạt";
            public const string ReviewNotAccept = "Xem xét không đạt";
        }
        public class HandlerIncidentText
        {
            public const string New = "Chưa xử lý";
            public const string Doing = "Đang xử lý";
            public const string Finish = "Đã xử lý";
        }
        public class RequestStatusText
        {
            public const string All = "Tất cả";
            public const string New = "Mới";
            public const string Wait = "Chờ thực hiện";
            public const string Perform = "Đang thực hiện";
            public const string Complete = "Hoàn thành";
            public const string Finish = "Kết thúc";
            public const string Destroy = "Hủy";
            public const string OverwriteTime = "Quá hạn";
        }

        #endregion
        #region Ngoại lệ
        public class ExceptionErrorMessage
        {
            public const string PropertyNameHasExist = "Thuộc tính đã tồn tại";
            public const string LessEqualValue = "Giá trị sau phải lớn hơn hoặc bằng";
            public const string AssetTypeProperty = "Loại và thuộc tính đã tồn tại";
            public const string AssetTypePropertyBeingContain = "Loại tài sản đã bao gồm tất cả các thuộc tính trong các loại tài sản sau: {0}";
            public const string ConflictException = "Conflict Exception";
            public const string DataHasBeenExisted = "Dữ liệu đã tồn tại";
            public const string ProfileNameHasBeenExisted = "Tên đã tồn tại trong hệ thống";
            public const string CategoryHasDocument = "Đang tồn tại tài liệu nằm trong danh mục này.";
            public const string AccessDeny = "Bạn không có quyền truy cập vào chức năng này!";
            public const string DataIsNotValid = "Dữ liệu không đúng!";
            public const string SuggestAdjustIsNotFinish = "Bạn đã đề nghị sửa đổi tài liệu này rồi. Xin chờ đến khi đề nghị kết thúc.";
            public const string DocumentPublishExisted = "Tài liệu này đã ban hành";
            public const string SercurityExisted = "Tên hoặc màu mức độ bảo mật đã tồn tại";
            public const string DocumentPublish = "Tài liệu này chưa ban hành đề nghị bạn ban hành trước khi lưu trữ";
            public const string DeleteProfileCategory = "Bạn không thể xóa danh mục đã có hồ sơ hoặc danh mục con.";
            public const string DocumentCanNotSuggestAdjust = "Không thể sửa đổi tài liệu khi tài liệu chưa ban hành hoặc chưa áp dụng!";
            public const string DocumentExternalCanNotUseThisFunction = "Tài liệu bên ngoài không thể sử dụng chức năng này!";
            public const string DocumentHasBeenApply = "Tài liệu này đã được áp dụng";
            public const string DocumentMustToApplyBeforeDistribute = "Tài liệu phải được áp dụng trước khi phân phối";
            public const string DocumentIsNotInDepartment = "Tài liệu chưa xác định phòng ban";
            public const string DocumentDistributeWhenWaitDistribute = "Chỉ có thể gửi đề nghị khi tài liệu đang chờ ban hành.";

            public const string NotExistDestroySuggest = "Chưa có đề nghị hủy nào được duyệt đạt.";
            public const string NotExistDestroySuggestChoose = "Chưa có đề nghị hủy nào được chọn";
            public const string NotExistProfileCuriculmViate = "Chưa có thông tin hồ sơ lý lịch nhân sự.";
            public const string NotExistProfileDestroy = "Chưa có hồ sơ nào, chỉ chọn những hồ sơ có trạng thái Lưu trữ.";
            public const string DocumentCanNotUpdateCategoryBeforePublish = "Tài liệu không thể cập nhật danh mục khi chưa ban hành";

            public const string DeleteProfileTargetCategory = "Bạn không thể xóa nhóm mục tiêu đã có mục tiêu.";
        }
        #endregion
        #region Tài sản
        public class ReviewLimitValue
        {
            public Int32 SecurityMinValue { get; set; }
            public Int32 SecurityMaxValue { get; set; }
            public Int32 InviblateMinValue { get; set; }
            public Int32 InviblateMaxValue { get; set; }
            public Int32 ReadyMinValue { get; set; }
            public Int32 ReadyMaxValue { get; set; }
            public Int64 ImportantMinValue { get; set; }
            public Int64 ImportantMaxValue { get; set; }
        }
        #endregion
        #region Tài liệu
        #region Enum
        /// <summary>
        /// Xác định nguồn phát sinh yêu cầu
        /// </summary>
        public enum RequestSource
        {
            /// <summary>
            /// Từ đề nghị viết mới
            /// </summary>
            SuggestWrite = 1,
            /// <summary>
            /// Từ đề nghị sửa đổi
            /// </summary>
            SuggestAdjust = 2,
            /// <summary>
            /// Từ yêu cầu viết mới
            /// </summary>
            RequestWrite = 3,
            /// <summary>
            /// Từ yêu cầu sửa đổi
            /// </summary>
            RequestAdjust = 4,
            /// <summary>
            /// Từ người phân công tạo ra
            /// </summary>
            Assignation = 5,
            /// <summary>
            /// Từ người biên soạn tạo ra
            /// </summary>
            Compilation = 6
        }
        /// <summary>
        /// Xác định người phát sinh yêu cầu
        /// </summary>
        public enum RequestFrom
        {
            /*
             * 1. Đề nghị: Viết mới, đề nghị sửa đổi
             * 2. Yêu cầu viết mới, sửa đổi
             * 3. Tự phân công tạo ra: Thêm 1 trường trong bảng req
             */
            /// <summary>
            /// Người yêu cầu
            /// </summary>
            Requestor = 1,
            /// <summary>
            /// Người phân công
            /// </summary>
            Assigner = 2,
            /// <summary>
            /// Người biên soạn
            /// </summary>
            Compilator = 3,
            /// <summary>
            /// Từ người đề nghị
            /// </summary>
            Suggestor = 4,
            /// <summary>
            /// Từ người xem xét
            /// </summary>
            Reviewer = 5
        }
        /// <summary>
        /// Xác định đích đến của yêu cầu
        /// </summary>
        public enum RequestTo
        {
            /// <summary>
            /// Người phân công
            /// </summary>
            Assigner = 1,
            /// <summary>
            /// Người biên soạn
            /// </summary>
            Compilator = 2,
            /// <summary>
            /// Người soạn thảo
            /// </summary>
            Writer = 3
        }
        public enum DocumentRole
        {
            Write = 1,
            Test = 2,
            Review = 3,
            Approve = 4,
            Archive = 5,
            Create = 6,
            Manage = 7,
            CategoryManage = 8
        }
        /// <summary>
        /// Loại yêu cầu gửi đến khung phân công
        /// </summary>
        public enum DocumentAssignType
        {
            SuggestCreate = 1,
            SuggestAdjust = 2,
            RequestCreate = 3,
            RequestAdjust = 4,
            RequestWrite = 5,
            NewDocument = 6,
            None = 0
        }
        /// <summary>
        /// Loại danh mục tài liệu
        /// </summary>
        public enum DocumentCategoryType
        {
            /// <summary>
            /// Nội bộ
            /// </summary>
            Internal = 1,
            /// <summary>
            /// Bên ngoài
            /// </summary>
            External = 2,
            /// <summary>
            /// Phân phối
            /// </summary>
            Distribute = 3
        }
        /// <summary>
        /// Trách nhiệm
        /// </summary>
        public enum DocumentEmployeeRole
        {
            All = 0,
            /// <summary>
            /// Người phê duyệt
            /// </summary>
            Approver = 1,
            /// <summary>
            /// Người phân công
            /// </summary>
            /// 
            Assigner = 2,
            /// <summary>
            /// Người biên soạn
            /// </summary>
            Compiler = 3,
            /// <summary>
            /// Người soạn thảo
            /// </summary>
            Writer = 4,
            /// <summary>
            /// Người đề nghị
            /// </summary>
            Suggester = 5,
            /// <summary>
            /// Người xem xét
            /// </summary>
            Reviewer = 6

        }
        public enum DocumentTypeProcess
        {
            /// <summary>
            /// Quy trình đề nghị viết mới/sửa đổi tài liệu
            /// </summary>
            WriteAndEditSuggest = 0,
            /// <summary>
            /// Quy trình soạn thảo, ban hành tài liệu
            /// </summary>
            WriteAndRelation = 1,
            /// <summary>
            /// Quy trình phân phối tài liệu
            /// </summary>
            DistributeSuggest = 2,
            /// <summary>
            /// Quy trình hủy tài liệu
            /// </summary>
            CancelSuggest = 3
        }
        public enum DocumentProcessRole
        {

            /// <summary>
            /// Người đề nghị viết mới và sửa đổi
            /// </summary>
            SuggesterWriteAndEdit = 0,
            /// <summary>
            /// Người xem xét đề nghị viết mới và sửa đổi
            /// </summary>
            ReviewWriteAndEdit = 1,
            /// <summary>
            /// Người phê duyệt đề nghị viết mới và sửa đổi
            /// </summary>            
            ApprovalWriteAndEdit = 2,
            /// <summary>
            /// Người phân công viết mới sửa đổi
            /// </summary>
            AssignmentWriteAndEdit = 3,
            /// <summary>
            /// Người soạn thảo
            /// </summary>
            Write = 4,
            /// <summary>
            /// Người kiểm tra
            /// </summary>
            Check = 5,
            /// <summary>
            /// Người phê duyệt tài liệu
            /// </summary>
            ApproveDocument = 6,
            /// <summary>
            /// Người ban hành tài liệu
            /// </summary>
            Promulgate = 7,
            /// Người lưu trữ tài liệu
            /// </summary>
            Archiver = 8,
            /// <summary>
            /// Người đề nghị phân phối tài liệu
            /// </summary>
            DistributeSuggester = 9,
            /// <summary>
            /// Người xem xét đề nghị phân phối
            /// </summary>
            DistributeSuggestReviewer = 10,
            /// <summary>
            /// Người xem xét đề nghị phân phối
            /// </summary>
            DistributeSuggestApproval = 11,
            /// <summary>
            /// Người phân phối tài liệu
            /// </summary>
            Distributer = 12,
            /// <summary>
            /// Người tạo đề nghị hủy tài liệu
            /// </summary>
            DestroyCreater = 13,
            /// <summary>
            /// Người xem xét đề ngị hủy
            /// </summary>
            DestroyReviewer = 14,
            /// <summary>
            /// Người phê duyệt đề nghị hủy
            /// </summary>
            DestroyApproval = 15,
            /// <summary>
            /// Người lập biên bản hủy
            /// </summary>
            DestroyBillCreater = 16,
            /// <summary>
            /// Yêu cầu phân phối
            /// </summary>
            DistributeRequester = 17,
            /// <summary>
            /// Yêu cầu hủy
            /// </summary>
            DestroyRequester = 18

        }
        public enum DocumentProcessRoleFilterSuggest
        {
            All = -1,
            /// <summary>
            /// Người đề nghị viết mới và sửa đổi
            /// </summary>
            SuggesterWriteAndEdit = 0,
            /// <summary>
            /// Người xem xét đề nghị viết mới và sửa đổi
            /// </summary>
            ReviewWriteAndEdit = 1,
            /// <summary>
            /// Người phê duyệt đề nghị viết mới và sửa đổi
            /// </summary>            
            ApprovalWriteAndEdit = 2,
            /// <summary>
            /// Người phân công viết mới sửa đổi
            /// </summary>
            AssignmentWriteAndEdit = 3,
            /// <summary>
            /// Người soạn thảo
            /// </summary>
            Write = 4,
            /// <summary>
            /// Người kiểm tra
            /// </summary>
            Check = 5,
            /// <summary>
            /// Người phê duyệt tài liệu
            /// </summary>
            ApproveDocument = 6,
            /// <summary>
            /// Người ban hành tài liệu
            /// </summary>
            Promulgate = 7,
            /// Người lưu trữ tài liệu
            /// </summary>
            Archiver = 8,
            /// <summary>
            /// Người đề nghị phân phối tài liệu
            /// </summary>
            DistributeSuggester = 9,
            /// <summary>
            /// Người xem xét đề nghị phân phối
            /// </summary>
            DistributeSuggestReviewer = 10,
            /// <summary>
            /// Người xem xét đề nghị phân phối
            /// </summary>
            DistributeSuggestApproval = 11,
            /// <summary>
            /// Người phân phối tài liệu
            /// </summary>
            Distributer = 12,
            /// <summary>
            /// Người tạo đề nghị hủy tài liệu
            /// </summary>
            DestroyCreater = 13,
            /// <summary>
            /// Người xem xét đề nghị hủy
            /// </summary>
            DestroyReviewer = 14,
            /// <summary>
            /// Người phê duyệt đề nghị hủy
            /// </summary>
            DestroyApproval = 15,
            /// <summary>
            /// Người lập biên bản hủy
            /// </summary>
            DestroyBillCreater = 16
        }
        public enum DocumentApproveView
        {
            suggestWriteNewReview,
            suggestWriteNewApprove,
            suggestReviewApprove,
            suggestReivewReview,
            suggestApproveRevert,

        }
        public enum DocumentAssignStatus
        {
            New = 1,
            Perform = 2,
            Complete = 3,
            Finish = 4
        }

        public enum DocumentExternalApplyStatus
        {
            Review = 1,
            Approve = 2
        }
        #endregion
        #region Text
        public class DocumentSuggestRole
        {
            public const string Suggest = "Người đề nghị";
            public const string Review = "Người xem xét";
            public const string Check = "Người kiểm tra";
            public const string Approve = "Người phê duyệt";
            public const string Storage = "Người phân công";
            public const string Promulgate = "Người ban hành";
            public const string Archive = "Người lưu trữ";
            public const string Destroy = "Người hủy";
            public const string Distribute = "Người phân phối";
        }
        public class DocumentRoleText
        {
            public const string Write = "Soạn thảo";
            public const string Test = "Kiểm tra";
            public const string Review = "Xem xét";
            public const string Approve = "Phê duyệt";
            public const string Storage = "Lưu trữ";
            public const string New = "Tạo mới";
            public const string Manage = "Quản lý";
            public const string CategoryManage = "Cập nhật danh mục";
        }
        public class DocumentAssignTypeText
        {

            public const string SuggestCreate = "Đề nghị viết mới";
            public const string SuggestAdjust = "Đề nghị sửa đổi";
            public const string RequestCreate = "Yêu cầu viết mới";
            public const string RequestAdjust = "Yêu cầu sửa đổi";
            public const string RequestWrite = "Yêu cầu soạn thảo";
            public const string RequesPublish = "Yêu cầu ban hành";
            public const string NewDocument = "Thực hiện soạn thảo";
            public const string None = "";
        }
        public class DocumentCategoryTypeText
        {
            /// <summary>
            /// Nội bộ
            /// </summary>
            public const string Internal = "Nội bộ";
            /// <summary>
            /// Bên ngoài
            /// </summary>
            public const string External = "Bên ngoài";
            /// <summary>
            /// Phân phối
            /// </summary>
            public const string Distribute = "Phân phối";
        }
        public class DocumentEmployeeRoleText
        {
            public const string All = "Tất cả";
            /// <summary>
            /// Người phê duyệt
            /// </summary>
            public const string Approver = "Người yêu cầu";
            /// <summary>
            /// Người biên soạn
            /// </summary>
            public const string Assigner = "Người phân công";
            /// <summary>
            /// Người biên soạn
            /// </summary>
            public const string Compiler = "Người biên soạn";
            /// <summary>
            /// Người soạn thảo
            /// </summary>
            public const string Writer = "Người soạn thảo";
            /// <summary>
            /// Người đề nghị
            /// </summary>
            public const string Suggester = "Người đề nghị";
            /// <summary>
            /// Người xem xét
            /// </summary>
            public const string Reviewer = "Người xem xét";

        }

        public class DocumentExternalApplyStatusText
        {
            public const string Review = "Xem xét";
            public const string Approve = "Phê duyệt";
        }
        #endregion
        #region Dictionary

        /// <summary>
        /// Loại tài liệu
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetTypeDocument()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)DocumentType.All, DocumentText.All);
            dic.Add((int)DocumentType.New, DocumentText.NewDocument);
            dic.Add((int)DocumentType.SuggestNew, DocumentText.SuggestNew);
            dic.Add((int)DocumentType.RequestNew, DocumentText.RequestNew);
            dic.Add((int)DocumentType.SuggestAdjust, DocumentText.SuggestAdjust);
            dic.Add((int)DocumentType.RequestAdjust, DocumentText.RequestAdjust);
            return dic;
        }

        /// <summary>
        /// Trạng thái yêu cầu
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetRequestStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)RequestStatus.All, RequestStatusText.All);
            dic.Add((int)RequestStatus.New, RequestStatusText.New);
            dic.Add((int)RequestStatus.Wait, RequestStatusText.Wait);
            dic.Add((int)RequestStatus.Perform, RequestStatusText.Perform);
            dic.Add((int)RequestStatus.Complete, RequestStatusText.Complete);
            dic.Add((int)RequestStatus.Finish, RequestStatusText.Finish);
            return dic;
        }

        /// <summary>
        /// Vai trò khung phân công
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetAssignAreaRoleType()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)DocumentEmployeeRole.All, DocumentEmployeeRoleText.All);
            dic.Add((int)DocumentEmployeeRole.Approver, DocumentEmployeeRoleText.Approver);
            dic.Add((int)DocumentEmployeeRole.Assigner, DocumentEmployeeRoleText.Assigner);
            dic.Add((int)DocumentEmployeeRole.Reviewer, DocumentEmployeeRoleText.Reviewer);
            dic.Add((int)DocumentEmployeeRole.Suggester, DocumentEmployeeRoleText.Suggester);
            return dic;
        }

        public static Dictionary<int, string> GetWriteAreaRoleType()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)DocumentEmployeeRole.All, DocumentEmployeeRoleText.All);
            dic.Add((int)DocumentEmployeeRole.Approver, DocumentEmployeeRoleText.Approver);
            dic.Add((int)DocumentEmployeeRole.Assigner, DocumentEmployeeRoleText.Assigner);
            dic.Add((int)DocumentEmployeeRole.Compiler, DocumentEmployeeRoleText.Compiler);
            dic.Add((int)DocumentEmployeeRole.Writer, DocumentEmployeeRoleText.Writer);
            return dic;
        }

        /// <summary>
        /// Trạng thái yêu cầu biên soạn
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetRequestCompilationStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)RequestStatus.All, RequestStatusText.All);
            dic.Add((int)RequestStatus.Perform, RequestStatusText.Perform);
            dic.Add((int)RequestStatus.Complete, RequestStatusText.Complete);
            dic.Add((int)RequestStatus.Finish, RequestStatusText.Finish);
            return dic;
        }

        /// <summary>
        /// Trách nhiệm với tài liệu
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetDocumentRole()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)DocumentRole.Write, DocumentRoleText.Write);
            dic.Add((int)DocumentRole.Test, DocumentRoleText.Test);
            dic.Add((int)DocumentRole.Review, DocumentRoleText.Review);
            dic.Add((int)DocumentRole.Approve, DocumentRoleText.Approve);
            dic.Add((int)DocumentRole.Archive, DocumentRoleText.Storage);
            dic.Add((int)DocumentRole.Create, DocumentRoleText.New);
            dic.Add((int)DocumentRole.Manage, DocumentRoleText.Manage);
            return dic;
        }

        /// <summary>
        /// Trạng thái tài liệu bên ngoài
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetDocumentStatusOnCategory()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)DocumentStatus.All, DocumentStatusText.All);
            dic.Add((int)DocumentStatus.Publish, DocumentStatusText.Publish);
            dic.Add((int)DocumentStatus.Obsolate, DocumentStatusText.Obsolate);
            dic.Add((int)DocumentStatus.Expire, DocumentStatusText.Expire);
            dic.Add((int)DocumentStatus.Destroy, DocumentStatusText.Destroy);
            return dic;
        }
        /// <summary>
        /// Trạng thái hồ sơ
        /// </summary>
        /// <returns></returns>



        /// <summary>
        /// Trạng thái tài liệu bên ngoài
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetExternalDocumentStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)DocumentStatus.Publish, DocumentStatusText.Publish);
            dic.Add((int)DocumentStatus.Obsolate, DocumentStatusText.Obsolate);
            dic.Add((int)DocumentStatus.Expire, DocumentStatusText.Expire);
            dic.Add((int)DocumentStatus.Destroy, DocumentStatusText.Destroy);
            return dic;
        }

        /// <summary>
        /// Trách nhiệm đối với đề nghị áp dụng
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetExternalApplyStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)DocumentExternalApplyStatus.Review, DocumentExternalApplyStatusText.Review);
            dic.Add((int)DocumentExternalApplyStatus.Approve, DocumentExternalApplyStatusText.Approve);
            return dic;
        }

        public static Dictionary<int, string> GetRoleTypeDestroySuggest()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)DocumentProcessRole.Check, DocumentExternalApplyStatusText.Review);
            dic.Add((int)DocumentProcessRole.ApproveDocument, DocumentExternalApplyStatusText.Approve);
            return dic;
        }

        public static Dictionary<int, string> GetEDocumentStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)EDocumentStatus.All, EDocumentStatusText.All);
            dic.Add((int)EDocumentStatus.New, EDocumentStatusText.New);
            dic.Add((int)EDocumentStatus.WaitPublish, EDocumentStatusText.WaitPublish);
            dic.Add((int)EDocumentStatus.Publish, EDocumentStatusText.Publish);
            dic.Add((int)EDocumentStatus.Obsolate, EDocumentStatusText.Obsolate);
            dic.Add((int)EDocumentStatus.Expire, EDocumentStatusText.Expire);
            dic.Add((int)EDocumentStatus.Destroy, EDocumentStatusText.Destroy);
            return dic;
        }
        public static Dictionary<int, string> GetAddEDocumentStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)EDocumentStatus.WaitPublish, EDocumentStatusText.WaitPublish);
            dic.Add((int)EDocumentStatus.Publish, EDocumentStatusText.Publish);
            dic.Add((int)EDocumentStatus.Expire, EDocumentStatusText.Expire);
            dic.Add((int)EDocumentStatus.Obsolate, EDocumentStatusText.Obsolate);
            //dic.Add((int)EDocumentStatus.Destroy, EDocumentStatusText.Destroy);
            return dic;
        }
        /// <summary>
        /// Danh sách trách nhiệm
        /// </summary>

        public static Dictionary<int, string> GetDocumentTypeProcess()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)DocumentTypeProcess.WriteAndEditSuggest, DocumentTypeProcessText.WriteAndEditSuggest);
            dic.Add((int)DocumentTypeProcess.WriteAndRelation, DocumentTypeProcessText.WriteAndRelatetion);
            //dic.Add((int)DocumentTypeProcess.DistributeSuggest, DocumentTypeProcessText.DistributeSuggest);
            //dic.Add((int)DocumentTypeProcess.CancelSuggest, DocumentTypeProcessText.CancelSuggest);
            return dic;
        }
        public static Dictionary<int, string> GetDocumentProcessRole()
        {
            var dic = new Dictionary<int, string>();
            var source = Enum.GetValues(typeof(DocumentProcessRole)).Cast<DocumentProcessRole>();
            dic.Add((int)DocumentProcessRole.ApprovalWriteAndEdit, DocumentProcessRoleText.ApprovalWriteAndEdit);
            dic.Add((int)DocumentProcessRole.ApproveDocument, DocumentProcessRoleText.ApproveDocument);
            dic.Add((int)DocumentProcessRole.Archiver, DocumentProcessRoleText.Archiver);
            dic.Add((int)DocumentProcessRole.AssignmentWriteAndEdit, DocumentProcessRoleText.AssignmentWriteAndEdit);
            dic.Add((int)DocumentProcessRole.Check, DocumentProcessRoleText.Check);
            dic.Add((int)DocumentProcessRole.DestroyApproval, DocumentProcessRoleText.DestroyApproval);
            dic.Add((int)DocumentProcessRole.DestroyBillCreater, DocumentProcessRoleText.DestroyBillCreater);
            dic.Add((int)DocumentProcessRole.DestroyCreater, DocumentProcessRoleText.DestroyCreater);
            dic.Add((int)DocumentProcessRole.DestroyReviewer, DocumentProcessRoleText.DestroyReviewer);
            dic.Add((int)DocumentProcessRole.Distributer, DocumentProcessRoleText.Distributer);
            dic.Add((int)DocumentProcessRole.DistributeSuggestApproval, DocumentProcessRoleText.DistributeSuggestApproval);
            dic.Add((int)DocumentProcessRole.DistributeSuggester, DocumentProcessRoleText.DistributeSuggester);
            dic.Add((int)DocumentProcessRole.DistributeSuggestReviewer, DocumentProcessRoleText.DistributeSuggestReviewer);
            dic.Add((int)DocumentProcessRole.Promulgate, DocumentProcessRoleText.Promulgate);
            dic.Add((int)DocumentProcessRole.ReviewWriteAndEdit, DocumentProcessRoleText.ReviewWriteAndEdit);
            dic.Add((int)DocumentProcessRole.SuggesterWriteAndEdit, DocumentProcessRoleText.SuggesterWriteAndEdit);
            dic.Add((int)DocumentProcessRole.Write, DocumentProcessRoleText.Write);
            dic.Add((int)DocumentProcessRole.DistributeRequester, DocumentProcessRoleText.DistributeRequester);
            return dic;
        }
        public static Dictionary<int, string> GetStatusApprove()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ApproveStatus.All, ApproveStatusText.All);
            dic.Add((int)ApproveStatus.New, ApproveStatusText.New);
            dic.Add((int)ApproveStatus.Wait, ApproveStatusText.Wait);
            dic.Add((int)ApproveStatus.Approve, ApproveStatusText.Approve);
            dic.Add((int)ApproveStatus.Reject, ApproveStatusText.Reject);
            return dic;
        }
        /// <summary>
        /// Trạng thái dùng trên các danh sách duyệt đề nghị kiểm duyệt, đề nghị phân phối
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetStatusApproveSuggest()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ApproveStatus.All, ApproveStatusText.All);
            dic.Add((int)ApproveStatus.New, ApproveStatusText.New);
            dic.Add((int)ApproveStatus.Wait, ApproveStatusText.Wait);
            dic.Add((int)ApproveStatus.Approve, ApproveStatusText.Approve);
            dic.Add((int)ApproveStatus.Reject, ApproveStatusText.Reject);
            return dic;
        }
        public static Dictionary<int, string> GetRequestProcessStatusText()
        {
            var dic = new Dictionary<int, string>();
            var source = Enum.GetValues(typeof(RequestProcessStatus)).Cast<RequestProcessStatus>();
            dic.Add((int)RequestProcessStatus.None, RequestProcessStatusText.None);
            dic.Add((int)RequestProcessStatus.New, RequestProcessStatusText.New);
            dic.Add((int)RequestProcessStatus.WaitAssign, RequestProcessStatusText.WaitAssign);
            dic.Add((int)RequestProcessStatus.WaitPerform, RequestProcessStatusText.WaitPerform);
            dic.Add((int)RequestProcessStatus.Perform, RequestProcessStatusText.Perform);
            dic.Add((int)RequestProcessStatus.WaitReview, RequestProcessStatusText.WaitReview);
            dic.Add((int)RequestProcessStatus.ReviewReject, RequestProcessStatusText.ReviewReject);
            dic.Add((int)RequestProcessStatus.WaitSendApprove, RequestProcessStatusText.WaitSendApprove);
            dic.Add((int)RequestProcessStatus.WaitApprove, RequestProcessStatusText.WaitApprove);
            dic.Add((int)RequestProcessStatus.Complete, RequestProcessStatusText.Complete);
            dic.Add((int)RequestProcessStatus.Cancel, RequestProcessStatusText.Cancel);
            dic.Add((int)RequestProcessStatus.OutOfDate, RequestProcessStatusText.OutOfDate);
            return dic;
        }
        #endregion
        #endregion
        #region Công văn
        public enum DispatchStatus
        {
            // Tất cả
            All = 0,
            // Chờ xem xét
            WaitReview = 1,
            // Xem xét
            Review = 2,
            //Xem xét không đạt
            NotReview = 3,
            // Chờ duyệt
            WaitApprove = 4,
            // duyệt
            Approve = 5,
            //Duyệt không đạt
            NotApprove = 6,
            //Chờ gửi
            WaitSend = 7,
            // Đã gửi
            Send = 8,
            //Xóa
            Delete = 9,
            // Lưu trữ
            Archived = 10
        }


        public class DispatchStatusText
        {
            public const string All = "Tất cả";
            /// <summary>
            /// Chờ xem xét
            /// </summary>
            public const string WaitReview = "Chờ xem xét";
            /// <summary>
            /// Xem xét đạt
            /// </summary>
            public const string Review = "Xem xét đạt";
            /// <summary>
            /// Xem xét không đạt
            /// </summary>
            public const string NotReview = "Xem xét không đạt";
            /// <summary>
            /// Chờ duyệt
            /// </summary>
            public const string WaitApprove = "Chờ duyệt";
            /// <summary>
            /// Duyệt đạt
            /// </summary>
            public const string Approve = "Duyệt đạt";
            /// <summary>
            /// Duyệt không đạt
            /// </summary>
            public const string NotApprove = "Duyệt không đạt";
            /// <summary>
            /// Chờ gửi
            /// </summary>
            public const string WaitSend = "Chờ gửi";
            /// <summary>
            /// Đã gửi
            /// </summary>
            public const string Send = "Đã gửi";
            /// <summary>
            /// Xóa
            /// </summary>
            public const string Delete = "Xóa";
            public const string Archived = "Lưu trữ";
        }

        public static Dictionary<int, string> GetDispatchStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)DispatchStatus.All, DispatchStatusText.All);
            dic.Add((int)DispatchStatus.WaitReview, DispatchStatusText.WaitReview);
            dic.Add((int)DispatchStatus.Review, DispatchStatusText.Review);
            dic.Add((int)DispatchStatus.NotReview, DispatchStatusText.NotReview);
            dic.Add((int)DispatchStatus.WaitApprove, DispatchStatusText.WaitApprove);
            dic.Add((int)DispatchStatus.Approve, DispatchStatusText.Approve);
            dic.Add((int)DispatchStatus.NotApprove, DispatchStatusText.NotApprove);
            dic.Add((int)DispatchStatus.WaitSend, DispatchStatusText.WaitSend);
            dic.Add((int)DispatchStatus.Send, DispatchStatusText.Send);
            dic.Add((int)DispatchStatus.Delete, DispatchStatusText.Delete);
            dic.Add((int)DispatchStatus.Archived, DispatchStatusText.Archived);
            return dic;
        }

        #endregion
        #region Hồ sơ
        public enum ProfileRoleType
        {
            /// <summary>
            /// Người phê duyệt đề nghị hủy
            /// </summary>
            DestroyApproval = 1,
            /// <summary>
            /// Người phê duyệt đề nghị mượn hồ sơ
            /// </summary>
            BorrowApproval = 2,
            /// <summary>
            /// Người tạo đề nghị mượn hồ sơ
            /// </summary>
            BorrowCreater = 3,
            /// <summary>
            /// Cập nhật hồ sơ
            /// </summary>
            ProfileAssign = 4,
            /// <summary>
            /// Người kiểm tra hồ sơ
            /// </summary>
            ProfileChecker = 5,
            /// <summary>
            /// Người phê duyệt hồ sơ
            /// </summary>
            ProfileApprover = 6,
            /// <summary>
            /// Người lưu trữ
            /// </summary>
            ProfileArchiver = 7
        }
        public enum ESendResult
        {
            /// <summary>
            /// Chờ lưu trữ
            /// </summary>
            WaitArchive = 1,
            /// <summary>
            /// Đã lưu trữ
            /// </summary>
            Archived = 2,
            /// <summary>
            /// Chờ bổ sung
            /// </summary>
            WaitUpdate = 3
        }
        public class ESendResultText
        {
            public const string WaitArchive = "Chờ lưu trữ";
            public const string Archived = "Đã lưu trữ";
            public const string WaitUpdate = "Chờ bổ sung";
        }
        public class ProfileRoleTypeText
        {
            public const string DestroyApproval = "Người phê duyệt";
            public const string BorrowApproval = "Người phê duyệt";
            public const string BorrowCreater = "Người tạo";
            public const string ProfileAssign = "Người cập nhật";
            public const string ProfileChecker = "Người kiểm tra";
            public const string ProfileArchiver = "Người lưu trữ";
        }

        public enum ProfileStatus
        {
            // Tất cả
            All = 0,
            // Mới
            New = 1,
            // Cập nhật
            Update = 2,
            // Đóng
            Close = 3,
            // Lưu trữ
            Archive = 4,
            // Hủy
            Destroy = 5,
            //Mở
            Open = 6,
            // Chờ hủy
            WaitDestroy = 7,
            // Chờ xem xét
            WaitReview = 8
        }
        public enum Sex
        {
            //Nam
            Man = 1,
            // Nữ
            Feman = 2,
            Other = 3
        }
        public class SexText
        {
            public const string Man = "Nam";
            public const string Feman = "Nữ";
            public const string Other = "Khác";
        }
        public enum ProfileBorrowStatus
        {
            // Tất cả
            All = 0,
            // Chờ giao hồ sơ
            WaitAssign = 1,
            // Đang mượn
            Borrowing = 2,
            // Đã trả
            Return = 3,
            // Gia hạn
            Extend = 4,
            // Quá hạn trả
            OverTimeReturn = 5,
            // Hủy
            Destroy = 6
        }
        public enum ProfileTransferStatus
        {
            // Tất cả
            All = 0,
            // Chờ nộp
            WaitTransfer = 1,
            // Chờ lưu trữ
            WaitArchive = 2,
            // Lưu trữ
            Archive = 3,
            // Chờ bổ sung
            WaitAdd = 4,
            // Quá hạn nộp
            OverTimeTransfer = 5,
            // Hủy
            Destroy = 6
        }
        /// <summary>
        /// Loại lưu trữ hồ sơ
        /// </summary>
        public enum ProfileArchiveTypes
        {
            // Tất cả
            All = 0,
            // Có thời hạn
            HasTime = 1,
            // Vĩnh viễn
            NoLimit = 2,
            // Quá hạn
            OverTime = 3,
        }
        public class ProfileArchiveTypeText
        {
            /// <summary>
            /// Tất cả
            /// </summary>
            public const string All = "Tất cả";
            /// <summary>
            /// Có thời hạn
            /// </summary>
            public const string HasTime = "Có thời hạn";
            /// <summary>
            /// Vĩnh viễn
            /// </summary>
            public const string NoLimit = "Vĩnh viễn";
            /// <summary>
            /// Quá hạn
            /// </summary>
            public const string OverTime = "Quá hạn";
        }
        public class ProfileStatusText
        {
            public const string All = "Tất cả";
            /// <summary>
            /// Mới
            /// </summary>
            public const string New = "Mới";
            /// <summary>
            /// Cập nhật
            /// </summary>
            public const string Update = "Cập nhật";
            /// <summary>
            /// Đóng
            /// </summary>
            public const string Close = "Đóng";
            /// <summary>
            /// Lưu trữ
            /// </summary>
            public const string Archive = "Lưu trữ";
            /// <summary>
            /// Hủy
            /// </summary>
            public const string Destroy = "Hủy";
            /// <summary>
            /// Mở
            /// </summary>
            public const string Open = "Mở";
            /// <summary>
            /// Chờ hủy
            /// </summary>
            public const string WaitDestroy = "Chờ hủy";
            /// <summary>
            /// Chờ xem xét
            /// </summary>
            public const string WaitReview = "Chờ xem xét";
        }

        public class ProfileTransferStatusText
        {
            public const string All = "Tất cả";
            /// <summary>
            /// Chờ nộp
            /// </summary>
            public const string WaitTransfer = "Chờ nộp";
            /// <summary>
            /// Chờ lưu trữ
            /// </summary>
            public const string WaitArchive = "Chờ lưu trữ";
            /// <summary>
            /// Lưu trữ
            /// </summary>
            public const string Archive = "Lưu trữ";
            /// <summary>
            /// Chờ bổ sung
            /// </summary>
            public const string WaitAdd = "Chờ bổ sung";
            /// <summary>
            /// Quá hạn nộp
            /// </summary>
            public const string OverTimeTransfer = "Quá hạn nộp";
            /// <summary>
            /// Hủy
            /// </summary>
            public const string Destroy = "Hủy";
        }
        public class ProfileBorrowStatusText
        {
            public const string All = "Tất cả";
            /// <summary>
            /// Chờ giao hồ sơ
            /// </summary>
            public const string WaitAssign = "Chờ giao hồ sơ";
            /// <summary>
            /// Đang mượn
            /// </summary>
            public const string Borrowing = "Đang mượn";
            /// <summary>
            /// Gia hạn
            /// </summary>
            public const string Extend = "Gia hạn";
            /// <summary>
            /// Quá hạn trả
            /// </summary>
            public const string OverTimeReturn = "Quá hạn trả";
            /// <summary>
            /// Đã trả
            /// </summary>
            public const string Return = "Đã trả";
            /// <summary>
            /// Hủy
            /// </summary>
            public const string Destroy = "Hủy";
        }

        public class ProfileComponentStatusText
        {
            /// <summary>
            /// Tất cả
            /// </summary>
            public const string All = "Tất cả";
            /// <summary>
            /// Chờ Cập nhật
            /// </summary>
            public const string WaitUpdate = "Chờ cập nhật";
            /// <summary>
            /// Đã cập nhật
            /// </summary>
            public const string Updating = "Đã cập nhật";
            /// <summary>
            /// Loại bỏ
            /// </summary>
            public const string Eliminate = "Loại bỏ";
        }

        public enum ProfileComponentStatus
        {  //Tất cả
            All = 0,
            // chờ cập nhật
            WaitUpdate = 1,
            // Đã cập nhật
            Updating = 2,
            //Loại bỏ
            Eliminate = 3,
        }


        public static Dictionary<int, string> GetProfileComponentStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ProfileComponentStatus.All, ProfileComponentStatusText.All);
            dic.Add((int)ProfileComponentStatus.WaitUpdate, ProfileComponentStatusText.WaitUpdate);
            dic.Add((int)ProfileComponentStatus.Updating, ProfileComponentStatusText.Updating);
            dic.Add((int)ProfileComponentStatus.Eliminate, ProfileComponentStatusText.Eliminate);
            return dic;
        }

        public class FormalitySaveStatusText
        {
            /// <summary>
            /// Bản mềm
            /// </summary>
            public const string Soft = "Bản mềm";
            /// <summary>
            /// Bản cứng
            /// </summary>
            public const string Hard = "Bản cứng";

        }

        public enum FormalitySaveStatus
        {
            // Bản mềm
            Soft = 1,
            // Bản cứng
            Hard = 2,

        }


        public static Dictionary<int, string> GetFormalitySaveStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)FormalitySaveStatus.Soft, FormalitySaveStatusText.Soft);
            dic.Add((int)FormalitySaveStatus.Hard, FormalitySaveStatusText.Hard);
            return dic;
        }

        public enum ProfileAssignStatus
        {
            All = -1,
            // Chờ phân công
            WaitSend = 0,
            // Chờ cập nhật
            WaitUpdate = 1,
            //Đang cập nhật
            Updating = 2,
            // Hoàn thành   
            Complete = 3,
            // Không đạt
            CompleteFail = 4,
            // Hủy
            Destroy = 5,
        }
        public class ProfileAssignStatusText
        {
            public const string All = "Tất cả";
            // Chờ gửi
            public const string WaitSend = "Chờ gửi";
            // Chờ cập nhật
            public const string WaitUpdate = "Chờ cập nhật";
            //Đang cập nhật
            public const string Updating = "Đang cập nhật";
            // Hoàn thành   
            public const string Complete = "Hoàn thành";
            // Không đạt  
            public const string CompleteFail = "Không đạt";
            // Hủy   
            public const string Destroy = "Hủy";
        }
        public enum ProfileResponsibilityRole
        {
            // Người lập
            Creater = 0,
            // Người kiểm tra 
            Checker = 1,
            // Người duyệt
            Approval = 2
        }
        public class ProfileResponsibilityRoleText
        {
            // Người lập
            public const string Creater = "Người lập";
            // Người kiểm tra 
            public const string Checker = "Người kiểm tra ";
            // Người duyệt
            public const string Approval = "Người duyệt";
        }
        public enum ProfileResponsibilityStatus
        {
            All = -1,
            // Chờ gửi
            WaitSend = 0,
            //Đã gửi
            Sent = 1,
            //Chờ kiểm tra
            WaitCheck = 2,
            //Đạt
            CheckAccept = 3,
            // Không đạt
            CheckFail = 4,
            // Chờ phê duyệt
            WaitApprove = 5,
            // Duyệt
            ApproveAccept = 6,
            // Không duyệt
            ApproveFail = 7
        }
        public class ProfileResponsibilityStatusText
        {
            public const string All = "Tất cả";
            // Chờ gửi
            public const string WaitSend = "Chờ gửi";
            //Đã gửi
            public const string Sent = "Đã gửi";
            //Chờ kiểm tra
            public const string WaitCheck = "Chờ kiểm tra";
            //Đạt
            public const string CheckAccept = "Đạt";
            // Không đạt
            public const string CheckFail = "Không đạt";
            // Chờ phê duyệt
            public const string WaitApprove = "Chờ phê duyệt";
            // Duyệt
            public const string ApproveAccept = "Duyệt";
            // Không duyệt
            public const string ApproveFail = "Không duyệt";
        }
        public enum ProfileResponsibilityResult
        {
            // Đạt
            Accept = 1,
            //Không đạt
            Fail = 2,
        }
        public class ProfileResponsibilityResultText
        {
            // Đạt
            public const string Accept = "Đạt";
            //Không đạt
            public const string Fail = "Không đạt";
        }

        /// <summary>
        /// Thời gian lưu trữ
        /// </summary>
        public enum ProfileArchiveTime
        {
            All = 0,
            /// <summary>
            /// Ngày
            /// </summary>
            Day = 1,
            /// <summary>
            /// Tháng
            /// </summary>
            Month = 2,
            /// <summary>
            /// Năm
            /// </summary>
            Year = 3,
            /// <summary>
            /// Vĩnh viễn
            /// </summary>
            Permanent = 4
        }

        public class ArchiveTimeText
        {
            public const string All = "Tất cả";
            public const string Day = "Ngày";
            public const string Month = "Tháng";
            public const string Year = "Năm";
            public const string Permanent = "Vĩnh viễn";
        }

        public enum ProfileDestroyStatus
        {
            /// <summary>
            /// Tất cả
            /// </summary>
            All = 0,
            /// <summary>
            /// Mới
            /// </summary>
            New = 1,
            /// <summary>
            /// Chờ duyệt
            /// </summary>
            WaitApprove = 2,
            /// <summary>
            /// Chờ thực hiện
            /// </summary>
            WaitPerform = 3,
            /// <summary>
            /// Đã duyệt
            /// </summary>
            Approved = 4,
            /// <summary>
            /// Không đạt
            /// </summary>
            Reject = 5,
            /// <summary>
            /// Đã tạo biên bản
            /// </summary>
            BillCreated = 6
        }

        public class ProfileDestroyStatusText
        {
            public const string All = "Tất cả";
            public const string New = "Mới";
            public const string WaitApprove = "Chờ duyệt";
            public const string WaitPerform = "Chờ thực hiện";
            public const string Approved = "Đã duyệt";
            public const string Reject = "Không đạt";
            public const string BillCreated = "Đã tạo biên bản";
        }

        public enum ProfileDestroyDetail
        {
            /// <summary>
            /// Tất cả
            /// </summary>
            All = 0,
            /// <summary>
            /// Không hủy
            /// </summary>
            NotDestroy = 1,
            /// <summary>
            /// Hủy
            /// </summary>
            Destroy = 2
        }

        public class ProfileDestroyDetailText
        {
            public const string All = "Tất cả";
            public const string NotDestroy = "Không hủy";
            public const string Destroy = "Hủy";
        }
        /// <summary>
        /// Loại lưu trữ hồ sơ
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetProfileArchiveTypes()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ProfileArchiveTypes.All, ProfileArchiveTypeText.All);
            dic.Add((int)ProfileArchiveTypes.HasTime, ProfileArchiveTypeText.HasTime);
            dic.Add((int)ProfileArchiveTypes.NoLimit, ProfileArchiveTypeText.NoLimit);
            dic.Add((int)ProfileArchiveTypes.OverTime, ProfileArchiveTypeText.OverTime);
            return dic;
        }

        public static Dictionary<int, string> GetDestroyReport()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ProfileDestroyDetail.All, ProfileDestroyDetailText.All);
            dic.Add((int)ProfileDestroyDetail.NotDestroy, ProfileDestroyDetailText.NotDestroy);
            dic.Add((int)ProfileDestroyDetail.Destroy, ProfileDestroyDetailText.Destroy);
            return dic;
        }

        /// <summary>
        /// Trạng thái hồ sơ của đề nghị hủy
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetDestroyDetail()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ProfileStatus.All, ProfileStatusText.All);
            dic.Add((int)ProfileStatus.WaitReview, ProfileStatusText.WaitReview);
            dic.Add((int)ProfileStatus.WaitDestroy, ProfileStatusText.WaitDestroy);
            dic.Add((int)ProfileStatus.Archive, ProfileStatusText.Archive);
            dic.Add((int)ProfileStatus.Destroy, ProfileStatusText.Destroy);
            return dic;
        }

        /// <summary>
        /// Trạng thái đề nghị hủy hồ sơ
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetProfileDestroyStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ProfileDestroyStatus.All, ProfileDestroyStatusText.All);
            dic.Add((int)ProfileDestroyStatus.New, ProfileDestroyStatusText.New);
            dic.Add((int)ProfileDestroyStatus.WaitApprove, ProfileDestroyStatusText.WaitApprove);
            dic.Add((int)ProfileDestroyStatus.WaitPerform, ProfileDestroyStatusText.WaitPerform);
            dic.Add((int)ProfileDestroyStatus.Approved, ProfileDestroyStatusText.Approved);
            dic.Add((int)ProfileDestroyStatus.Reject, ProfileDestroyStatusText.Reject);
            dic.Add((int)ProfileDestroyStatus.BillCreated, ProfileDestroyStatusText.BillCreated);
            return dic;
        }

        public static Dictionary<int, string> GetArchiveTime()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ProfileArchiveTime.Day, ArchiveTimeText.Day);
            dic.Add((int)ProfileArchiveTime.Month, ArchiveTimeText.Month);
            dic.Add((int)ProfileArchiveTime.Year, ArchiveTimeText.Year);
            dic.Add((int)ProfileArchiveTime.Permanent, ArchiveTimeText.Permanent);
            return dic;
        }

        public static Dictionary<int, string> GetArchiveTimeForList()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ProfileArchiveTime.All, ArchiveTimeText.All);
            dic.Add((int)ProfileArchiveTime.Day, ArchiveTimeText.Day);
            dic.Add((int)ProfileArchiveTime.Month, ArchiveTimeText.Month);
            dic.Add((int)ProfileArchiveTime.Year, ArchiveTimeText.Year);
            dic.Add((int)ProfileArchiveTime.Permanent, ArchiveTimeText.Permanent);
            return dic;
        }

        public static Dictionary<int, string> GetProfileStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ProfileStatus.All, ProfileStatusText.All);
            dic.Add((int)ProfileStatus.New, ProfileStatusText.New);
            dic.Add((int)ProfileStatus.Update, ProfileStatusText.Update);
            dic.Add((int)ProfileStatus.Close, ProfileStatusText.Close);
            dic.Add((int)ProfileStatus.Archive, ProfileStatusText.Archive);
            dic.Add((int)ProfileStatus.WaitDestroy, ProfileStatusText.WaitDestroy);
            dic.Add((int)ProfileStatus.Destroy, ProfileStatusText.Destroy);
            dic.Add((int)ProfileStatus.Open, ProfileStatusText.Open);
            return dic;
        }
        public static Dictionary<int, string> GetSendTransferResult()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ESendResult.WaitArchive, ESendResultText.WaitArchive);
            dic.Add((int)ESendResult.Archived, ESendResultText.Archived);
            dic.Add((int)ESendResult.WaitUpdate, ESendResultText.WaitUpdate);
            return dic;
        }

        /// <summary>
        /// Trạng thái danh sách hồ sơ chờ hủy
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetProfileWaitDestroyStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ProfileStatus.All, ProfileStatusText.All);
            dic.Add((int)ProfileStatus.WaitDestroy, ProfileStatusText.WaitDestroy);
            dic.Add((int)ProfileStatus.Archive, ProfileStatusText.Archive);
            return dic;
        }
        public static Dictionary<int, string> GetSex()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)Sex.Man, SexText.Man);
            dic.Add((int)Sex.Feman, SexText.Feman);
            dic.Add((int)Sex.Other, SexText.Other);
            return dic;
        }
        public static Dictionary<int, string> GetProfileTransferStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ProfileTransferStatus.All, ProfileStatusText.All);
            dic.Add((int)ProfileTransferStatus.WaitTransfer, ProfileTransferStatusText.WaitTransfer);
            dic.Add((int)ProfileTransferStatus.WaitArchive, ProfileTransferStatusText.WaitArchive);
            dic.Add((int)ProfileTransferStatus.Archive, ProfileTransferStatusText.Archive);
            dic.Add((int)ProfileTransferStatus.WaitAdd, ProfileTransferStatusText.WaitAdd);
            dic.Add((int)ProfileTransferStatus.OverTimeTransfer, ProfileTransferStatusText.OverTimeTransfer);
            dic.Add((int)ProfileTransferStatus.Destroy, ProfileTransferStatusText.Destroy);
            return dic;
        }
        public static Dictionary<int, string> GetProfileBorrowStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)ProfileBorrowStatus.All, ProfileBorrowStatusText.All);
            dic.Add((int)ProfileBorrowStatus.WaitAssign, ProfileBorrowStatusText.WaitAssign);
            dic.Add((int)ProfileBorrowStatus.Borrowing, ProfileBorrowStatusText.Borrowing);
            dic.Add((int)ProfileBorrowStatus.Extend, ProfileBorrowStatusText.Extend);
            dic.Add((int)ProfileBorrowStatus.OverTimeReturn, ProfileBorrowStatusText.OverTimeReturn);
            dic.Add((int)ProfileBorrowStatus.Return, ProfileBorrowStatusText.Return);
            dic.Add((int)ProfileBorrowStatus.Destroy, ProfileBorrowStatusText.Destroy);
            return dic;
        }
        #endregion
        #region Chất lượng
        #region Enum

        public enum RoleAudit
        {
            // Đánh giá trưởng
            Leader = 1,
            // Thành viên tham dự
            Auditer = 2,
            // Đánh giá viên
            Member = 3
        }
        public enum UnitTime
        {
            // Ngày
            Day = 1,
            // Tuần
            Week = 2,
            // Tháng
            Month = 3,
            // Năm
            Year = 4,
        }
        public enum TargetStatus
        {
            // Tất cả
            All = 0,
            // Mới
            New = 1,
            // Cập nhật
            Perform = 2,
            // Duyệt
            Approve = 3,
            // Không duyệt
            NotApprove = 4,
            // Hủy
            Destroy = 5,
            // Quá hạn
            Expire = 6,
            // Đạt
            Pass = 7,
            // Không đạt
            NotPass = 8,
            // Chờ duyệt
            WaitApproval = 9
        }
        public enum PlanAuditStatus
        {
            // Tất cả
            All = 0,
            // Mới
            New = 1,
            // Chờ duyệt
            Pending = 2,
            // Chờ đánh giá
            Approval = 3,
            // Không duyệt
            NotApprove = 4,
            // Đang đánh giá
            Auditing = 5,
            // Hoàn thành
            Finished = 6,
            // Hủy
            Destroy = 7,

        }
        /// <summary>
        /// Trạng thái sự không phù hợp
        /// </summary>
        public enum QualityNCStatus
        {
            // Tất cả
            All = 0,
            // Chờ xác nhận
            WaitConfirm = 1,
            // Đã xác nhận
            Confirm = 2,
            //Không xác nhận
            NotConfirm = 3,
            //Chờ duyệt
            WaitApprove = 4,
            // Duyệt
            Approve = 5,
            // Không duyệt
            NotApprove = 6,
            //Đạt   
            Pass = 7,
            // Không đạt
            NotPass = 8,
            // Hủy
            Destroy = 9
        }

        public enum QualityNCTypeStatus
        {
            M = 1,
            m = 2,
            obs = 3
        }

        public enum TargetDestination
        {
            /// <summary>
            /// Tăng
            /// </summary>
            Up = 1,
            /// <summary>
            /// Giảm
            /// </summary>
            Down = 2,
            /// <summary>
            /// Đạt
            /// </summary>
            Equals = 3
        }

        /// <summary>
        /// Trạng thái cuộc họp
        /// </summary>
        public enum MeetingStatus
        {
            /// <summary>
            /// Mới
            /// </summary>
            New = 1,
            /// <summary>
            /// Chờ duyệt
            /// </summary>
            WaitApprove = 2,
            /// <summary>
            /// Đã duyệt
            /// </summary>
            Approve = 3,
            ///// <summary>
            ///// Thực hiện
            ///// </summary>
            Perform = 4,
            ///// <summary>
            ///// Đang họp
            ///// </summary>
            Finish = 5,
            /// <summary>
            /// Hủy
            /// </summary>
            Cancel = 6
        }

        public enum TargetType
        {
            // Ngắn hạn
            Short = 1,
            // Dài hạn
            Long = 2
        }

        public enum TargetValue
        {
            /// <summary>
            /// phần trăm
            /// </summary>
            Scale = 1,
            /// <summary>
            /// Giá trị
            /// </summary>
            Value = 2
        }

        public enum TargetResponsibility
        {
            /// <summary>
            /// Trách nhiệm tạo mục tiêu
            /// </summary>
            Create = 1,
            /// <summary>
            /// Trách nhiệm phê duyệt mục tiêu
            /// </summary>
            Approval = 2,
            /// <summary>
            /// Kiểm soát
            /// </summary>
            Control = 3
        }


        public enum DispatchResponsibility
        {
            /// <summary>
            /// Trách nhiệm tạo mục tiêu
            /// </summary>
            Create = 1,
            /// <summary>
            /// Trách nhiệm phê duyệt mục tiêu
            /// </summary>
            Approval = 2,
            /// <summary>
            /// Xem xét
            /// </summary>
            Review = 3
        }

        public enum TargetExpression
        {
            /// <summary>
            /// Lớn hơn
            /// </summary>
            Grand = 1,
            /// <summary>
            /// Bé hơn
            /// </summary>
            Less = 2,
            /// <summary>
            /// Bằng
            /// </summary>
            Equal = 3,
            /// <summary>
            /// Lớn hơn hoặc bằng
            /// </summary>
            GrandOrEqual = 4,
            /// <summary>
            /// Bé hơn hoặc bằng
            /// </summary>
            LessOrEqual = 5
        }

        public enum QualityPlan
        {
            // Tất cả
            All = 1,
            // Mới
            New = 2,
            // Chờ duyệt
            WaitApprove = 3,
            // Duyệt
            Approved = 4,
            // Không duyệt
            NotApprove = 5
        }
        #endregion

        #region Text
        public class RoleAuditText
        {
            public const string Leader = "Đánh giá trưởng";
            public const string Auditer = "Đánh giá viên";
            public const string Member = "Thành viên tham dự";
        }
        public class UnitTimeText
        {
            public const string Day = "Ngày";
            public const string Week = "Tuần";
            public const string Month = "Tháng";
            public const string Year = "Năm";
        }
        public class PlanAuditStatusText
        {
            // Tất cả
            public const string All = "Tất cả";
            // Mới
            public const string New = "Mới";
            // Chờ duyệt
            public const string Pending = "Chờ duyệt";
            // Chờ họp
            public const string Approval = "Chờ đánh giá";
            // Không duyệt
            public const string NotApprove = "Không duyệt";
            // Đang họp
            public const string Auditing = "Đang đánh giá";
            // Hoàn thành
            public const string Finished = "Hoàn thành";
            // Hủy
            public const string Destroy = "Hủy";

        }
        public class TargetStatusText
        {
            public const string All = "Tất cả";
            public const string New = "Mới";
            public const string Perform = "Đang thực hiện";
            public const string Approve = "Duyệt";
            public const string NotApprove = "Không duyệt";
            public const string Destroy = "Hủy";
            public const string Expire = "Quá hạn";
            public const string Pass = "Đạt";
            public const string NotPass = "Không đạt";
            public const string WaitApproval = "Chờ duyệt";
        }
        public class QualityNCTypeStatusText
        {
            public const string M = "M";
            public const string m = "m";
            public const string obs = "obs";
        }

        public static Dictionary<int, string> GetQualityNCTypeStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)QualityNCTypeStatus.M, QualityNCTypeStatusText.M);
            dic.Add((int)QualityNCTypeStatus.m, QualityNCTypeStatusText.m);
            dic.Add((int)QualityNCTypeStatus.obs, QualityNCTypeStatusText.obs);
            return dic;
        }
        public class QualityNCStatusText
        {
            public const string All = "Tất cả";
            public const string WaitConfirm = "Chờ xác nhận";
            public const string Confirm = "Đã xác nhận";
            public const string NotConfirm = "Xác nhận không đạt";
            public const string WaitApprove = "Chờ duyệt";
            public const string Approve = "Duyệt đạt";
            public const string NotApprove = "Duyệt không đạt";
            public const string Pass = "Đạt";
            public const string NotPass = "Không đạt";
            public const string Destroy = "Hủy";
        }
        public enum FormatFile
        {
            Image,
            Text,
            Video,
            Audio,
            Word,
            Excel,
            Pdf,
        }
        public static Dictionary<int, string> GetQualityNCStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)QualityNCStatus.All, QualityNCStatusText.All);
            dic.Add((int)QualityNCStatus.WaitConfirm, QualityNCStatusText.WaitConfirm);
            dic.Add((int)QualityNCStatus.Confirm, QualityNCStatusText.Confirm);
            dic.Add((int)QualityNCStatus.NotConfirm, QualityNCStatusText.NotConfirm);
            dic.Add((int)QualityNCStatus.WaitApprove, QualityNCStatusText.WaitApprove);
            dic.Add((int)QualityNCStatus.Approve, QualityNCStatusText.Approve);
            dic.Add((int)QualityNCStatus.NotApprove, QualityNCStatusText.NotApprove);
            dic.Add((int)QualityNCStatus.Pass, QualityNCStatusText.Pass);
            dic.Add((int)QualityNCStatus.NotPass, QualityNCStatusText.NotPass);
            dic.Add((int)QualityNCStatus.Destroy, QualityNCStatusText.Destroy);
            return dic;
        }
        public class MeetingStatusText
        {
            /// <summary>
            /// Mới
            /// </summary>
            public const string New = "Mới";
            /// <summary>
            /// Chờ duyệt
            /// </summary>
            public const string WaitApprove = "Chờ duyệt";
            /// <summary>
            /// Đã duyệt
            /// </summary>
            public const string Approve = "Đã duyệt";
            /// <summary>
            /// Thông báo
            /// </summary>
            public const string Perform = "Thực hiện";
            /// <summary>
            /// Kết thúc
            /// </summary>
            public const string Finish = "Kết thúc";
            /// <summary>
            /// Hủy
            /// </summary>
            public const string Cancel = "Hủy";
        }

        public class MeetingStatusColor
        {
            /// <summary>
            /// Mới
            /// </summary>
            public const string New = "#ff0000";
            /// <summary>
            /// Chờ duyệt
            /// </summary>
            public const string WaitApprove = "#30e0d9";
            /// <summary>
            /// Đã duyệt
            /// </summary>
            public const string Approve = "#01882b";
            /// <summary>
            /// Thông báo
            /// </summary>
            public const string Perform = "#2027ff;";
            /// <summary>
            /// Kết thúc
            /// </summary>
            public const string Finish = "#34519f";
            /// <summary>
            /// Hủy
            /// </summary>
            public const string Cancel = ";#000000;";
        }
        public class TargetDestinationText
        {
            public const string Up = "Tăng";
            public const string Down = "Giảm";
            public const string Equals = "Đạt";
        }

        public class TargetTypeText
        {
            public const string Short = "Ngắn hạn";
            public const string Long = "Dài hạn";
        }

        public class TargetValueText
        {
            public const string Scale = "%";
            public const string Value = "Giá trị";
        }

        public class TargetResponsibilityText
        {
            public const string Create = "Người tạo";
            public const string Approval = "Phê duyệt";
            public const string Control = "Kiểm soát";
        }

        public class DispatchResponsibilityText
        {
            public const string Create = "Người tạo";
            public const string Approval = "Phê duyệt";
            public const string Review = "Xem xét";
        }

        public class TargetExpressionText
        {
            public const string Grand = ">";
            public const string Less = "<";
            public const string Equal = "=";
            public const string GrandOrEqual = ">=";
            public const string LessOrEqual = "<=";
        }

        public class QualityPlanText
        {
            public const string All = "Tất cả";
            public const string New = "Mới";
            public const string WaitApprove = "Chờ duyệt";
            public const string Approved = "Duyệt";
            public const string NotApprove = "Không duyệt";
        }
        #endregion

        #region Dictionary
        public static Dictionary<int, string> GetUnitTime()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)UnitTime.Day, UnitTimeText.Day);
            dic.Add((int)UnitTime.Week, UnitTimeText.Week);
            dic.Add((int)UnitTime.Month, UnitTimeText.Month);
            dic.Add((int)UnitTime.Year, UnitTimeText.Year);
            return dic;
        }
        public static Dictionary<int, string> GetRoleAudit()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)RoleAudit.Leader, RoleAuditText.Leader);
            dic.Add((int)RoleAudit.Auditer, RoleAuditText.Auditer);
            dic.Add((int)RoleAudit.Member, RoleAuditText.Member);
            return dic;
        }
        public static Dictionary<int, string> GetMeetingStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)MeetingStatus.New, MeetingStatusText.New);
            dic.Add((int)MeetingStatus.WaitApprove, MeetingStatusText.WaitApprove);
            dic.Add((int)MeetingStatus.Approve, MeetingStatusText.Approve);
            dic.Add((int)MeetingStatus.Perform, MeetingStatusText.Perform);
            dic.Add((int)MeetingStatus.Finish, MeetingStatusText.Finish);
            dic.Add((int)MeetingStatus.Cancel, MeetingStatusText.Cancel);
            return dic;
        }

        public static Dictionary<int, string> GetTargetStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)TargetStatus.All, TargetStatusText.All);
            dic.Add((int)TargetStatus.New, TargetStatusText.New);
            dic.Add((int)TargetStatus.Perform, TargetStatusText.Perform);
            dic.Add((int)TargetStatus.WaitApproval, TargetStatusText.WaitApproval);
            dic.Add((int)TargetStatus.Approve, TargetStatusText.Approve);
            dic.Add((int)TargetStatus.NotApprove, TargetStatusText.NotApprove);
            dic.Add((int)TargetStatus.Destroy, TargetStatusText.Destroy);
            dic.Add((int)TargetStatus.Expire, TargetStatusText.Expire);
            dic.Add((int)TargetStatus.Pass, TargetStatusText.Pass);
            dic.Add((int)TargetStatus.NotPass, TargetStatusText.NotPass);
            return dic;
        }

        public static Dictionary<int, string> GetTargetType()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)TargetType.Short, TargetTypeText.Short);
            dic.Add((int)TargetType.Long, TargetTypeText.Long);
            return dic;
        }

        public static Dictionary<int, string> GetTargetValue()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)TargetValue.Scale, TargetValueText.Scale);
            dic.Add((int)TargetValue.Value, TargetValueText.Value);
            return dic;
        }

        public static Dictionary<int, string> GetTargetDestination()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)TargetDestination.Up, TargetDestinationText.Up);
            dic.Add((int)TargetDestination.Down, TargetDestinationText.Down);
            dic.Add((int)TargetDestination.Equals, TargetDestinationText.Equals);
            return dic;
        }

        public static Dictionary<int, string> GetTargetResponsibility()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)TargetResponsibility.Approval, TargetResponsibilityText.Approval);
            dic.Add((int)TargetResponsibility.Control, TargetResponsibilityText.Control);
            return dic;
        }

        public static Dictionary<int, string> GetDispatchResponsibility()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)DispatchResponsibility.Approval, DispatchResponsibilityText.Approval);
            dic.Add((int)DispatchResponsibility.Review, DispatchResponsibilityText.Review);
            return dic;
        }

        public static Dictionary<int, string> GetTargetExpression()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)TargetExpression.Grand, TargetExpressionText.Grand);
            dic.Add((int)TargetExpression.Less, TargetExpressionText.Less);
            //dic.Add((int)TargetExpression.Equal, TargetExpressionText.Equal);
            dic.Add((int)TargetExpression.GrandOrEqual, TargetExpressionText.GrandOrEqual);
            dic.Add((int)TargetExpression.LessOrEqual, TargetExpressionText.LessOrEqual);
            return dic;
        }

        public static Dictionary<int, string> GetQualityPlanStatus()
        {
            var dic = new Dictionary<int, string>();
            dic.Add((int)QualityPlan.All, QualityPlanText.All);
            dic.Add((int)QualityPlan.New, QualityPlanText.New);
            dic.Add((int)QualityPlan.WaitApprove, QualityPlanText.WaitApprove);
            dic.Add((int)QualityPlan.Approved, QualityPlanText.Approved);
            dic.Add((int)QualityPlan.NotApprove, QualityPlanText.NotApprove);
            return dic;
        }
        #endregion

        #endregion
        #region Rủi ro
        #region Enum
        public enum MethodHandleRisk
        {
            Giam = 1,
            Chuyen = 2,
            Tranh = 3,
            Accept = 4
        }

        public enum AcceptCriteriaRisk
        {
            Accept = 1,
            Handler = 2
        }
        #endregion
        #region Text

        public class MethodHandleRiskText
        {
            public const string Giam = "Giảm rủi ro";
            public const string Chuyen = "Chuyển rủi ro";
            public const string Tranh = "Tránh rủi ro";
            public const string Accept = "Chấp nhận rủi ro";
        }
        public class AcceptCriteriaRiskText
        {
            public const string Accept = "Chấp nhận rủi ro";
            public const string Handler = "Xử lý rủi ro";
        }
        #endregion
        #region Dictionary
        public static Dictionary<int, string> GetMethodHandleRisk()
        {
            var dic = new Dictionary<int, string>();
            var source = Enum.GetValues(typeof(MethodHandleRisk)).Cast<MethodHandleRisk>();
            dic.Add((int)MethodHandleRisk.Giam, MethodHandleRiskText.Giam);
            dic.Add((int)MethodHandleRisk.Chuyen, MethodHandleRiskText.Chuyen);
            dic.Add((int)MethodHandleRisk.Tranh, MethodHandleRiskText.Tranh);
            dic.Add((int)MethodHandleRisk.Accept, MethodHandleRiskText.Accept);
            return dic;
        }

        public static Dictionary<int, string> GetAcceptCriteriaRisk()
        {
            var dic = new Dictionary<int, string>();
            var source = Enum.GetValues(typeof(AcceptCriteriaRisk)).Cast<AcceptCriteriaRisk>();
            dic.Add((int)AcceptCriteriaRisk.Accept, AcceptCriteriaRiskText.Accept);
            dic.Add((int)AcceptCriteriaRisk.Handler, AcceptCriteriaRiskText.Handler);
            return dic;
        }

        public static string ConvertMethodHandleRisk(int? method)
        {
            switch (method)
            {
                case (int)MethodHandleRisk.Giam:
                    return MethodHandleRiskText.Giam;
                case (int)MethodHandleRisk.Chuyen:
                    return MethodHandleRiskText.Chuyen;
                case (int)MethodHandleRisk.Tranh:
                    return MethodHandleRiskText.Tranh;
                case (int)MethodHandleRisk.Accept:
                    return MethodHandleRiskText.Accept;
                default:
                    return string.Empty;
            }
        }
        public static string ConvertAcceptCriteriaRisk(int? criteria)
        {
            switch (criteria)
            {
                case (int)AcceptCriteriaRisk.Accept:
                    return AcceptCriteriaRiskText.Accept;
                case (int)AcceptCriteriaRisk.Handler:
                    return AcceptCriteriaRiskText.Handler;
                default:
                    return string.Empty;
            }
        }
        #endregion
        #endregion
        #region Định kỳ
        public enum RoutineType
        {
            QualityMeetingPlan = 1,
            QualityAuditPlan = 2,
        }
        #endregion

        #region Quy trình
        public class ShapeTypeString
        {
            public const string Circle = "basic.Circle";
            public const string Rectangle = "basic.Rect";
            public const string Relationship = "erd.Relationship";
            public const string Link = "app.Link";
            public const string Image = "basic.Image";
            public const string StartState = "fsa.StartState";
            public const string Member = "org.Member";


        }
        public enum ShapeTypeEnum
        {
            Circle = 1,
            Rectangle = 2,
            Relationship = 3,
            Link = 4,
            Image = 5,
            StartState = 6,
            Member = 7
        }
        public static Dictionary<string, ShapeTypeEnum> ShapeDict
        {
            get
            {
                var dict = new Dictionary<string, ShapeTypeEnum>();
                dict.Add(ShapeTypeString.Circle, ShapeTypeEnum.Circle);
                dict.Add(ShapeTypeString.Rectangle, ShapeTypeEnum.Rectangle);
                dict.Add(ShapeTypeString.Relationship, ShapeTypeEnum.Relationship);
                dict.Add(ShapeTypeString.Link, ShapeTypeEnum.Link);
                dict.Add(ShapeTypeString.Image, ShapeTypeEnum.Image);
                dict.Add(ShapeTypeString.StartState, ShapeTypeEnum.StartState);
                dict.Add(ShapeTypeString.Member, ShapeTypeEnum.Member);

                return dict;
            }
        }
        #endregion
    }
}