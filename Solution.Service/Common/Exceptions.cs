﻿using System;

namespace iDAS.Service.Common
{
    public class DataHasBeenExistedException : ArgumentException
    {
        public DataHasBeenExistedException() : base(Resource.ExceptionErrorMessage.DataHasBeenExisted) { }
    }

    public class AccessDenyException : ArgumentException
    {
        public AccessDenyException() : base(Resource.ExceptionErrorMessage.AccessDeny) { }
    }
    #region Tài liệu

    public class SuggestAdjustIsNotFinishException : ArgumentException
    {
        public SuggestAdjustIsNotFinishException() : base(Resource.ExceptionErrorMessage.SuggestAdjustIsNotFinish) { }
    }
    public class CategoryHasDocumentException : ArgumentException
    {
        public CategoryHasDocumentException() : base(Resource.ExceptionErrorMessage.CategoryHasDocument) { }
    }

    public class DataIsNotValidException : ArgumentException
    {
        public DataIsNotValidException()
            : base(Resource.ExceptionErrorMessage.DataIsNotValid)
        {

        }
    }

    public class DocumentCanNotSuggestAdjustException : ArgumentException
    {
        public DocumentCanNotSuggestAdjustException()
            : base(Resource.ExceptionErrorMessage.DocumentCanNotSuggestAdjust)
        {

        }
    }

    public class DocumentIsNotInDepartmentException : ArgumentException
    {
        public DocumentIsNotInDepartmentException() : base(Resource.ExceptionErrorMessage.DocumentIsNotInDepartment) { }
    }

    #endregion
    #region Hồ sơ

    public class ProfileCategoryExitsProfile : ArgumentException
    {
        public ProfileCategoryExitsProfile()
            : base(Resource.ExceptionErrorMessage.DeleteProfileCategory)
        {

        }
    }

    public class NotExistDestroySuggest : ArgumentException
    {
        public NotExistDestroySuggest()
            : base(Resource.ExceptionErrorMessage.NotExistDestroySuggest)
        {

        }
    }

    public class ProfileNameHasBeenExistedException : ArgumentException
    {
        public ProfileNameHasBeenExistedException() : base(Resource.ExceptionErrorMessage.ProfileNameHasBeenExisted) { }
    }

    public class NotExistProfileDestroy : ArgumentException
    {
        public NotExistProfileDestroy() : base(Resource.ExceptionErrorMessage.NotExistProfileDestroy) { }
    }
    #endregion

    #region Chất lượng
    public class TargetCategoryExitsTarget : ArgumentException
    {
        public TargetCategoryExitsTarget()
            : base(Resource.ExceptionErrorMessage.DeleteProfileTargetCategory)
        {

        }
    }
    #endregion

    public class NotExistProfileCuriculmViate : ArgumentException
    {
        public NotExistProfileCuriculmViate() : base(Resource.ExceptionErrorMessage.NotExistProfileCuriculmViate) { }
    }
}
