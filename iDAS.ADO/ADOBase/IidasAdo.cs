﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace iDAS.ADO.ADOBase
{
    public interface IidasAdo
    {

        int ExecuteNoquery(string cml, CommandType Type = CommandType.StoredProcedure, object parameter = null);
        SqlParameter[] Addparameter(object ob);
        List<T> ToList<T>(DataTable ob) where T : class, new();
        DataSet ExecuteNoqueryDataSet(string cml, CommandType Type = CommandType.StoredProcedure, object parameter = null);
        DataTable ExecuteNoqueryTable(string cml, CommandType Type = CommandType.StoredProcedure, object parameter = null);
        DataTable ExecuteNoqueryTableOut(string cml, ref object parameter, CommandType Type = CommandType.StoredProcedure);
    }
}
