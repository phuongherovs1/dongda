﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace iDAS.ADO.ADOBase
{
    public class idasAdo : IidasAdo
    {
        public static string connectstring = ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString;

        public virtual int ExecuteNoquery(string cml, System.Data.CommandType Type = CommandType.StoredProcedure, object parameter = null)
        {
            try
            {
                int so;
                using (SqlConnection connect = new SqlConnection(connectstring))
                {
                    connect.Open();
                    using (SqlTransaction trans = connect.BeginTransaction())
                    {
                        try
                        {
                            SqlCommand sqlad = new SqlCommand(cml, connect, trans);
                            switch (Type)
                            {
                                case CommandType.Text:
                                    so = sqlad.ExecuteNonQuery();
                                    break;
                                case CommandType.StoredProcedure:
                                    sqlad.CommandType = CommandType.StoredProcedure;
                                    if (parameter != null)
                                    {
                                        sqlad.Parameters.AddRange(Addparameter(parameter));
                                    }
                                    so = sqlad.ExecuteNonQuery();
                                    break;
                                default:
                                    throw new Exception("lệnh không được thức thi");
                            }
                            trans.Commit();
                            return so;
                        }
                        catch
                        {
                            trans.Rollback();
                            throw;
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual System.Data.SqlClient.SqlParameter[] Addparameter(object ob)
        {
            SqlParameter[] para = new SqlParameter[ob.GetType().GetProperties().Count()];
            int so = 0;
            PropertyInfo[] data = ob.GetType().GetProperties();
            foreach (var pr in data)
            {
                para[so] = new SqlParameter("@" + pr.Name, pr.GetValue(ob, null));
                so++;
            };
            return para;
        }

        public virtual List<T> ToList<T>(DataTable ob) where T : class, new()
        {
            List<T> dt = new List<T>();
            foreach (DataRow r in ob.Rows)
            {
                T CL = new T();
                PropertyInfo[] pr = CL.GetType().GetProperties();
                foreach (var c in pr)
                {
                    try
                    {
                        c.SetValue(CL, r[c.Name]);
                    }
                    catch
                    {
                        continue;
                    }
                }
                dt.Add(CL);
            }
            return dt;
        }

        public virtual System.Data.DataSet ExecuteNoqueryDataSet(string cml, System.Data.CommandType Type = CommandType.StoredProcedure, object parameter = null)
        {
            try
            {
                DataSet Data = new DataSet();
                using (SqlConnection connect = new SqlConnection(connectstring))
                {
                    connect.Open();
                    using (SqlTransaction trans = connect.BeginTransaction())
                    {

                        try
                        {
                            SqlCommand sqlad = new SqlCommand(cml, connect, trans);
                            SqlDataAdapter Adater = new SqlDataAdapter();
                            Adater.SelectCommand = sqlad;
                            switch (Type)
                            {
                                case CommandType.Text:
                                    sqlad.CommandType = CommandType.Text;
                                    Adater.Fill(Data);
                                    break;
                                case CommandType.StoredProcedure:
                                    sqlad.CommandType = CommandType.StoredProcedure;
                                    if (parameter != null)
                                    {
                                        sqlad.Parameters.AddRange(Addparameter(parameter));
                                    }
                                    Adater.Fill(Data);
                                    break;
                            }
                            trans.Commit();
                            return Data;
                        }
                        catch(Exception ex)
                        {
                            trans.Rollback();
                            throw;
                        }

                    };
                }
            }
            catch
            {
                throw;
            }
        }

        public virtual System.Data.DataTable ExecuteNoqueryTable(string cml, System.Data.CommandType Type = CommandType.StoredProcedure, object parameter = null)
        {
            try
            {
                DataTable Data = new DataTable();
                using (SqlConnection connect = new SqlConnection(connectstring))
                {
                    connect.Open();
                    using (SqlTransaction trans = connect.BeginTransaction())
                    {
                        try
                        {
                            SqlCommand sqlad = new SqlCommand(cml, connect, trans);
                            switch (Type)
                            {
                                case CommandType.Text:
                                    Data.Load(sqlad.ExecuteReader());
                                    break;
                                case CommandType.StoredProcedure:
                                    sqlad.CommandType = CommandType.StoredProcedure;
                                    if (parameter != null)
                                    {
                                        sqlad.Parameters.AddRange(Addparameter(parameter));
                                    }
                                    Data.Load(sqlad.ExecuteReader());
                                    break;
                            }
                            trans.Commit();
                            return Data;
                        }
                        catch(Exception e)
                        {
                            trans.Rollback();
                            throw;
                        }

                    };
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
        protected virtual object Outparameter(SqlCommand cm, object obj)
        {
            PropertyInfo[] data = obj.GetType().GetProperties();
            foreach (var pr in data)
            {
                pr.SetValue(obj, cm.Parameters[("@" + pr.Name)], null);
            };
            return obj;
        }
        public virtual System.Data.DataTable ExecuteNoqueryTableOut(string cml, ref object parameter, System.Data.CommandType Type = CommandType.StoredProcedure)
        {
            try
            {
                DataTable Data = new DataTable();
                using (SqlConnection connect = new SqlConnection(connectstring))
                {
                    connect.Open();
                    using (SqlTransaction trans = connect.BeginTransaction())
                    {
                        try
                        {
                            SqlCommand sqlad = new SqlCommand(cml, connect, trans);
                            switch (Type)
                            {
                                case CommandType.Text:
                                    Data.Load(sqlad.ExecuteReader());
                                    break;
                                case CommandType.StoredProcedure:
                                    sqlad.CommandType = CommandType.StoredProcedure;
                                    if (parameter != null)
                                    {
                                        sqlad.Parameters.AddRange(Addparameter(parameter));
                                    }

                                    Data.Load(sqlad.ExecuteReader());
                                    break;
                            }
                            trans.Commit();
                            parameter = Outparameter(sqlad, parameter);
                            return Data;
                        }
                        catch
                        {
                            trans.Rollback();
                            throw;
                        }

                    };
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
