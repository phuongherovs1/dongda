﻿using Ext.Net;
using Ext.Net.MVC;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Controllers
{
    public class NotifiesController : FrontController
    {
        //
        // GET: /Notifies/
        public ActionResult Index()
        {
            return View();
        }
        public Ext.Net.MVC.PartialViewResult LoadMore(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "LoadMore",
                WrapByScriptTag = false
            };
        }
        public ActionResult Read(string id = default(string))
        {
            var notifyNewTotal = SystemNotifyService.UpdateRead(new Guid(id));
            return this.Direct(notifyNewTotal);
        }
        public ActionResult GetNumberNotify()
        {
            var notifyNewTotal = SystemNotifyService.GetTotalNotRead();
            return this.Direct(notifyNewTotal);
        }
        public ActionResult GetData(StoreRequestParameters parameters)
        {
            var notifies = SystemNotifyService.GetByCurrentUser();
            return this.Store(notifies);
        }
        public ActionResult GetDataMore(int start, int limit)
        {
            var count = 0;
            return this.Store(SystemNotifyService.GetMoreByCurrentUser(start, limit, out count), count);
        }
        public ActionResult LoadTaskNotifies()
        {
            try
            {
                var data = TaskResourceService.GetByCurrentUser();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }

        }
        public ActionResult HiddenNotify(Guid? id)
        {
            var result = false;
            if (id == null || id == Guid.Empty)
                return this.Direct();
            try
            {
                SystemNotifyService.Delete(id.Value);
                result = true;
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult Revert(Guid? id)
        {
            var result = false;
            if (id == null || id == Guid.Empty)
                return this.Direct();
            try
            {
                SystemNotifyService.Revert(id.Value);
                result = true;
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult UpdateRead(Guid? id, bool isRead = true)
        {
            var result = false;
            if (id == null || id == Guid.Empty)
                return this.Direct();
            try
            {
                SystemNotifyService.UpdateRead(id.Value, isRead);
                result = true;
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public void Notify(string title, string contents, Guid userId, string url, string param)
        {
            var user = SystemNotifyService.Insert(title, contents, userId, url, param);
            base.notify.NotifyToUsers(user);
        }
    }
}