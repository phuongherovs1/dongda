﻿using Ext.Net.MVC;
using iDAS.Service;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace iDAS.Presentation.Controllers
{
    [AllowAnonymous]
    public class ConfigController : FrontController
    {
        private UserManager<UserBO, Guid> userManager { get; set; }
        private RoleManager<RoleBO, Guid> roleManager { get; set; }

        public ConfigController(IUserService userStoreService, IRoleService roleStoreService)
        {
            userManager = new UserManager<UserBO, Guid>(userStoreService);
            roleManager = new RoleManager<RoleBO, Guid>(roleStoreService);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAdmin()
        {
            var userName = "Admin";
            var password = "Admin@123";
            var user = new UserBO() { Id = Guid.NewGuid(), UserName = userName, FullName = Common.Role.Administrator };
            var result1 = await userManager.CreateAsync(user, password);
            IdentityResult result2 = null;
            if (result1.Succeeded)
            {
                result2 = await userManager.AddToRoleAsync(user.Id, Common.Role.Administrator);
            }
            var result = result1.Errors;
            if (result2 != null)
            {
                result = result.Concat(result2.Errors);
            }
            return View(result);
        }

        public ActionResult SetupRole()
        {
            var data = typeof(iDAS.Presentation.Common.Role).GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
            var roles = new List<RoleBO>();
            foreach (var item in data)
            {
                var attr = item.GetCustomAttributes(typeof(iDAS.Presentation.Common.RoleAttribute), false).Cast<iDAS.Presentation.Common.RoleAttribute>().FirstOrDefault();
                var role = new RoleBO()
                {
                    Code = item.Name,
                    Name = item.GetValue(null).ToString(),
                    Module = attr.Module,
                    Descriptsion = attr.Description,
                };
                roles.Add(role);
            }
            RoleService.InsertOrUpdate(roles);
            return View();
        }

        [HttpGet]
        public ActionResult SetupDatabase()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UpdateDatabase(DatabaseBO item)
        {
            var success = false;
            try
            {
                success = CoreService.UpdateDatabase(item);
                if (!success)
                    throw new Exception();
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(success);
        }
    }
}