﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Controllers
{
    public partial class FrontController : Controller
    {
        private Hubs.ChatHub _notify;
        public Hubs.ChatHub notify { get { return _notify = _notify ?? new Hubs.ChatHub(); } }
        public string UserName { get { return User.Identity.Name; } }

        /// <summary>
        /// Hàm gửi thông báo tới 1 user
        /// </summary>
        /// <param name="title">Tiêu đề của thông báo. Nên tạo trong enum TitleResourceNotifies</param>
        /// <param name="contents">Nội dung của thông báo</param>
        /// <param name="userId">Id nhân sự hay Id của user cần thông báo đến</param>
        /// <param name="url">Đường dẫn form khi người nhận được thông báo click vào. Đường dẫn này được tạo trong enum UrlResourceNotifies có dạng '/Tên Area/Tên Controller/Tên Action' Ví dụ: /Profile/ProfileSuggestBorrow/Approval</param>
        /// <param name="param">tham số truyền vào của url. Ví dụ: new {param1=value,param2=value}</param>
        public void Notify(string title, string contents, Guid userId, string url, object param, int type = 0)
        {
            var user = SystemNotifyService.Insert(title, contents, userId, url, param, type);
            notify.NotifyToUsers(user);
        }
        /// <summary>
        /// Hàm gửi thông báo tới nhiều user
        /// </summary>
        /// <param name="title">Tiêu đề của thông báo. Nên tạo trong enum TitleResourceNotifies</param>
        /// <param name="contents">Nội dung của thông báo</param>
        /// <param name="userIds">Danh sách Id nhân sự hay Id của user cần thông báo đến</param>
        /// <param name="url">Đường dẫn form khi người nhận được thông báo click vào. Đường dẫn này được tạo trong enum UrlResourceNotifies có dạng '/Tên Area/Tên Controller/Tên Action' Ví dụ: /Profile/ProfileSuggestBorrow/Approval</param>
        /// <param name="param">tham số truyền vào của url. Ví dụ: new {param1=value,param2=value}</param>
        /// <param 
        public void Notifies(string title, string contents, List<Guid> userIds, string url, object param)
        {
            var users = SystemNotifyService.Insert(title, contents, userIds, url, param).ToList();
            notify.NotifyToUsers(users);
        }
    }
}