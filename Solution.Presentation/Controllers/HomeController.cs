﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.API.System;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

namespace iDAS.Presentation.Controllers
{
    [Authorize]
    public class HomeController : FrontController
    {
        private IPermissionService permissionService { set; get; }
        public HomeController(IPermissionService _permissionService)
        {
            permissionService = _permissionService;
        }
        public Ext.Net.MVC.PartialViewResult ReportUpdate1(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "ReportUpdate",
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult ReportUpdate2(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {

                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "ReportUpdate",
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult ReportUpdate3(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {

                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "ReportUpdate",
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult ReportUpdate5(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {

                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "ReportUpdate",
                WrapByScriptTag = false
            };
        }
        public ActionResult Index()
        {
            var claim = (ClaimsIdentity)User.Identity;
            var userId = claim.FindFirst(ClaimTypes.UserData).Value;
            if (string.IsNullOrEmpty(userId))
                userId = Guid.Empty.ToString();
            var user = UserService.GetById(new Guid(userId));
            ViewBag.AvatarUrl = user.Avatar.Url;
            ViewBag.FullName = user.FullName;
            ViewBag.EmployeeID = user.Id;
            ViewData["NotifyNewTotal"] = SystemNotifyService.GetTotalNotRead();
            ViewData["MessaggerNewTotal"] = ChatMessengerService.GetTotalNotRead();
            ViewData["dataMenu"] = permissionService.ViewDataMenu(new Guid(userId));
            return View(user);
        }
        public ActionResult CheckHiddenMenu()
        {
            var claim = (ClaimsIdentity)User.Identity;
            var userId = claim.FindFirst(ClaimTypes.UserData).Value;
            if (string.IsNullOrEmpty(userId))
                userId = Guid.Empty.ToString();
            List<string> listrole, listroletile = new List<string>();
            listrole = UserRoleService.CurrentUserRole(new Guid(userId));
            if (listrole != null)
                for (int i = 0; i < listrole.Count; ++i)
                {
                    listrole[i] = Common.Module.ModuleKeyValue[listrole[i]];
                }
            //check roll in TitleRole
            listroletile = TitleRoleService.GetUserRole(new Guid(userId)).ToList();
            if (listroletile != null)
                for (int i = 0; i < listroletile.Count; ++i)
                {
                    listrole.Add(Common.Module.ModuleKeyValue[listroletile[i]]);
                }
            var result = JsonConvert.SerializeObject(listrole.Distinct());
            return this.Direct(result);
        }
        public ActionResult Test()
        {
            return View();
        }
        public ActionResult Menu()
        {
            return PartialView();
        }
        public ActionResult Main(string moduleCode = "Task")
        {
            return PartialView();
        }
        public ActionResult LoadModule(string moduleCode)
        {

            return PartialView();
        }
        public ActionResult Portlet()
        {
            return View();
        }
    }
}