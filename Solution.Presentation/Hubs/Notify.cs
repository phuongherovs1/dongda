﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace iDAS.Presentation.Hubs
{

    [HubName("Nofity")]
    public class Nofity : Hub
    {

        public void NotifyToUsers(List<string> users)
        {
            var hubConext = GlobalHost.ConnectionManager.GetHubContext<Nofity>();
            var proxy = hubConext.Clients.Users(users);
            proxy.ShowTotalNew();
        }
        public void NotifyToUsers(string user)
        {
            var hubConext = GlobalHost.ConnectionManager.GetHubContext<Nofity>();
            var proxy = hubConext.Clients.User(user);
            proxy.ShowTotalNew();
        }

    }

}