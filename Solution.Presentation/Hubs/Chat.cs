﻿using iDAS.Service;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;

namespace iDAS.Presentation.Hubs
{

    [HubName("Chat")]
    public class ChatHub : Hub
    {
        private IHubContext _hubContext;
        private IHubContext hubContext { get { return _hubContext = _hubContext ?? GlobalHost.ConnectionManager.GetHubContext<ChatHub>(); } }

        public static List<string> userConnecteds = new List<string>();

        // private ChatUserOnlineService userOnlineService = new ChatUserOnlineService(); //{ get { return _userOnlineService = _userOnlineService ?? new ChatUserOnlineService(); } }
        /// <summary>
        /// Gửi tin nhắn riêng
        /// </summary>
        /// <param name="content"></param>
        /// <param name="fromId"></param>
        /// <param name="toId"></param>
        /// <param name="toUser"></param>
        public void NewMessage(string content, string fromId, string toId, string fromUser, string toUser)
        {
            var item = new ChatItemBO()
            {
                Content = content,
                From = fromId,
                To = toId,
                FromName = fromUser,
                ToName = toUser,
                CreateAt = DateTime.Now,
            };
            var proxy = hubContext.Clients.User(toUser);
            proxy.NewMessage(item);
        }

        /// <summary>
        /// Gửi tin nhắn chung
        /// </summary>
        /// <param name="content"></param>
        /// <param name="fromId"></param>
        /// <param name="fromUserName"></param>
        /// <param name="fromFullName"></param>
        /// <param name="fromAvatarUrl"></param>
        public void NewPublicMessage(string content, string fromId, string fromUserName, string fromFullName, string fromAvatarUrl)
        {
            var item = new ChatPublicBO()
            {
                Content = content,
                From = new Guid(fromId),
                FromName = fromFullName,
                FromAvatarUrl = fromAvatarUrl,
                CreateAt = DateTime.Now
            };

            var proxy = hubContext.Clients.All;
            proxy.NewPublicMessage(item);
        }

        public override System.Threading.Tasks.Task OnConnected()
        {
            var userConnect = Context.User.Identity.Name;
            userConnecteds.Add(userConnect);
            var proxy = hubContext.Clients.All;
            proxy.UserOnline(userConnect, userConnecteds);
            return base.OnConnected();
        }

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var currentUser = Context.User.Identity.Name;
            userConnecteds.Remove(currentUser);
            var proxy = hubContext.Clients.All;
            proxy.UserOffline(currentUser);
            return base.OnDisconnected(stopCalled);
        }

        public override System.Threading.Tasks.Task OnReconnected()
        {
            var currentUser = Context.User.Identity.Name;
            userConnecteds.Remove(currentUser);
            var proxy = hubContext.Clients.All;
            proxy.UserOnline(currentUser);
            hubContext.Clients.User(currentUser).SetUserOnline(userConnecteds);
            return base.OnReconnected();
        }


        public void NotifyToUsers(List<string> users)
        {
            var proxy = hubContext.Clients.Users(users);
            proxy.ShowTotalNew();
        }
        public void NotifyToUsers(string user)
        {
            var proxy = hubContext.Clients.User(user);
            proxy.ShowTotalNew();
        }
    }

}