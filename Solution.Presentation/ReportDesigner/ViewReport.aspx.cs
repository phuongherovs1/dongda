﻿using iDAS.Service;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

namespace iDAS.Presentation.Views.Shared
{
    public partial class ViewReport : System.Web.UI.Page
    {
        #region Contructor
        VPHumanProfileWorkExperienceService _profileWorkExperienceService;
        VPHumanProfileWorkExperienceService profileWorkExperienceService
        {
            get
            {
                return _profileWorkExperienceService = _profileWorkExperienceService ?? new VPHumanProfileWorkExperienceService();
            }
        }

        VPHumanProfileCuriculmViateService _profileCuriculmViateService;
        VPHumanProfileCuriculmViateService profileCuriculmViateService
        {
            get
            {
                return _profileCuriculmViateService = _profileCuriculmViateService ?? new VPHumanProfileCuriculmViateService();
            }
        }

        VPHumanProfileTrainingService _profileTrainingService;
        VPHumanProfileTrainingService profileTrainingService
        {
            get
            {
                return _profileTrainingService = _profileTrainingService ?? new VPHumanProfileTrainingService();
            }
        }

        VPHumanProfileDiplomaService _profileDiplomaService;
        VPHumanProfileDiplomaService profileDiplomaService
        {
            get
            {
                return _profileDiplomaService = _profileDiplomaService ?? new VPHumanProfileDiplomaService();
            }
        }

        VPHumanProfileCertificateService _profileCertificateService;
        VPHumanProfileCertificateService profileCertificateService
        {
            get
            {
                return _profileCertificateService = _profileCertificateService ?? new VPHumanProfileCertificateService();
            }
        }

        VPHumanProfileRewardService _profileRewardService;
        VPHumanProfileRewardService profileRewardService
        {
            get
            {
                return _profileRewardService = _profileRewardService ?? new VPHumanProfileRewardService();
            }
        }

        VPHumanProfileDisciplineService _profileDisciplineService;
        VPHumanProfileDisciplineService profileDisciplineService
        {
            get
            {
                return _profileDisciplineService = _profileDisciplineService ?? new VPHumanProfileDisciplineService();
            }
        }

        VPHumanProfileAssessService _profileAssessService;
        VPHumanProfileAssessService profileAssessService
        {
            get
            {
                return _profileAssessService = _profileAssessService ?? new VPHumanProfileAssessService();
            }
        }

        VPHumanProfileContractService _profileContractService;
        VPHumanProfileContractService profileContractService
        {
            get
            {
                return _profileContractService = _profileContractService ?? new VPHumanProfileContractService();
            }
        }

        VPHumanProfileRelationshipService _profileRelationshipServicee;
        VPHumanProfileRelationshipService profileRelationshipService
        {
            get
            {
                return _profileRelationshipServicee = _profileRelationshipServicee ?? new VPHumanProfileRelationshipService();
            }
        }

        VPHumanProfileSalaryService _profileSalaryService;
        VPHumanProfileSalaryService profileSalaryService
        {
            get
            {
                return _profileSalaryService = _profileSalaryService ?? new VPHumanProfileSalaryService();
            }
        }

        VPHumanProfileInsuranceService _profileInsuranceService;
        VPHumanProfileInsuranceService profileInsuranceService
        {
            get
            {
                return _profileInsuranceService = _profileInsuranceService ?? new VPHumanProfileInsuranceService();
            }
        }

        VPHumanProfileAttachmentService _profileAttachmentService;
        VPHumanProfileAttachmentService profileAttachmentService
        {
            get
            {
                return _profileAttachmentService = _profileAttachmentService ?? new VPHumanProfileAttachmentService();
            }
        }

        VPHumanProfileWorkTrialService _profileWorkTrialService;
        VPHumanProfileWorkTrialService profileWorkTrialService
        {
            get
            {
                return _profileWorkTrialService = _profileWorkTrialService ?? new VPHumanProfileWorkTrialService();
            }
        }

        EmployeeService _employeeService;
        EmployeeService employeeService
        {
            get
            {
                return _employeeService = _employeeService ?? new EmployeeService();
            }
        }
        const string defaultAvatar = "iVBORw0KGgoAAAANSUhEUgAAAJgAAADjCAIAAAD2RUp0AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAXRSURBVHhe7do9dtpYAEBhPGuxp8jJCvAKsJup0qYTpWnSuXSXBkrTpXWVJmgFZgU5KQJ7Yd6TngTGYIsgGeee+xUxyAIkXf084ZytVque/n7/pJ/6yxkSwpAQhoQwJIQhIQwJYUgIQ0IYEsKQEIaEMCSEISEMCWFICENCGBLCkBCGhDAkhCEhDAlhSAhDQhgSwpAQhoQwJIQhIQwJYUgIQ0IYEsKQEIaEMCSEISEMCWFICENCGBLCkBCGhDAkhCEhDAlhSAhDQhgSwpAQhoQwJIQhIQwJYUgIQ0IYEsKQEIaEMCSEISEMCWFICENCGBLCkBCGhDAkhCEhDAlhSAhDQhgSwpAQhoQwJIQhIQwJYUgIQ0IYEsKQEIaEMCSEISEMCWFIiK5CLifDy7PocrJMk9SljkIufzxMP85Wq1k2f/hhyTdwtlqt0sMO5MPh7y/3N+fpqbrTZcjlZNK7seLb6PaI1Jv5O0aty3wSBk9h9OTQaZ/uQubDs7Nhnp4cI7zR56vRaDqfpwntiMt3mh2j3i2jVjZRFE6tXViM+/Hd++NFmnCkWVYs7bt9v4YW46zcLv1sPFssWvzwjkKmju1tqXZ3jJTxbUNW26QfEqZJLerm1Jp/HYWdLj56j7eR+fCuN672tDeynFxejOa9fjZbPd4MOhjJdxIy/z7Nbh9vi93+3ZWMGW/vr9Ozt5EPY8VeNnu8H6RJbesg5HJy93P8ZdAb/FeWHH1t63regjJjV1tzt3x4NQ0/slmXn9t+yOWPh96n63jySCV70++7Sy7zfOOmYrnM66Hc2eUw338cb855eTk5YDcJm/TAjMXYdkM5zA0nyvQ8eGXcGfbrmLEf9+0upWtla8I4IlwHkmpQsZ5Sm2XlRTQKQ7hxlqUhwCL8ppj4ZBxSDXa25ozTGo9YNhftkMFTPXLbXI/isxuMW9bLGFdsc53bHfK0HXJzYwVpNXaVjNa/3lypXRu52pivz7nP0yU75JVB3TK9RXjerES1hnEXrOZfzNbv1uzjG2g3ZFy/p5um2gK7S+7ZnGntN1/TfM7dnmbc+34vqJKEjR8PrWYv3Lv61S8OWICXtXqNXF8ea+fXn4olnt6d8su1eG08dqgxuC9TTq8uwnjp8aA/BvQ/XKRHlfOblgf1bYYMd4/z+eiiHAJUinF3cMLbkDYyRoP7dBy18l3hxYfyvX4tiqfHai9kHJ3tPMVVa3+iksWocXpV7la1tH9V+92B37lOrxq+4Pzfj+nRMy/86k+0FjKeVnePsKuz64lKnt88pj1q09Y1stGZMh9+/nWbLpbz0edGKcvj7oXD7vlZ98+0FTJ+Kbd1eazVJd/VVwMHCreOdx++xRN0uliGg7nBXy7StXDHrfTy98/wb3/fRjtUOyGL0+rt3r26urKfeMhzhHAwPnz6Vq3getzT4Aw7+BIP/unVVvVwBotf2u3faIcqzixHSbdF2QsD6erWPdi8/arvp8KNdZoUVCPzYqSfJqUvCYo51x+zGFdve/AN2dap9SXl+m3NWS9Qo48u566XPW2x1m49omNDrgsVdizb1hylbPZ8cnztrql1rdq+OQ/YLo1DrneruNTPJpUafHRx85lmj3+OPHTPe43/ZweizftInZAhIQwJYUgIQ0IYEsKQEIaEMCSEISEMCWFICENCGBLCkBCGhDAkhCEhDAlhSAhDQhgSwpAQhoQwJIQhIQwJYUgIQ0IYEsKQEIaEMCSEISEMCWFICENCGBLCkBCGhDAkhCEhDAlhSAhDQhgSwpAQhoQwJIQhIQwJYUgIQ0IYEsKQEIaEMCSEISEMCWFICENCGBLCkBCGhDAkhCEhDAlhSAhDQhgSwpAQhoQwJIQhIQwJYUgIQ0IYEsKQEIaEMCSEISEMCWFICENCGBLCkBCGhDAkhCEhDAlhSAhDQhgSwpAQhoQwJIQhIQwJYUgIQ0IYEsKQEIaEMCSEISEMCWFICENCGBLCkBCGhDAkQq/3P0+eekALhSVjAAAAAElFTkSuQmCC";
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var employeeId = Request.QueryString["emp"];
                if (!string.IsNullOrEmpty(employeeId))
                    SetLocalReport(new Guid(employeeId));
            }
        }

        private void SetLocalReport(Guid employeeId)
        {
            ReportViewer1.SizeToReportContent = true;
            ReportViewer1.Width = Unit.Percentage(100);
            ReportViewer1.Height = Unit.Percentage(100);

            var ms = new ProfileMasterService();

            var profileWorkExperience = profileWorkExperienceService.GetByEmployeeIdNotPaging(employeeId).ToList();
            var profileWorkTraining = profileTrainingService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var work = (new[] { new { StartDate = DateTime.Now, EndDate = DateTime.Now, Content = "" } }).ToList();
            foreach (var v in profileWorkExperience)
            {
                if (v.StartDate.HasValue && v.EndDate.HasValue)
                {
                    var content = string.Join(", ", new[] { v.Position, v.Department, v.PlaceOfWork }.Where(w => !string.IsNullOrEmpty(w)));
                    work.Add(new { StartDate = (DateTime)v.StartDate, EndDate = (DateTime)v.EndDate, Content = content });
                }
            }

            foreach (var v in profileWorkTraining)
            {
                if (v.StartDate.HasValue && v.EndDate.HasValue)
                {
                    var form = ms.GetSingleMasterData("educationType", v.Form).Name;
                    var content = string.Join(", ", new[] { v.Name, v.Content, form }.Where(w => !string.IsNullOrEmpty(w)));

                    var dateSpan = string.Format("dd/MM/yyyy", (DateTime)v.StartDate) + " - " + string.Format("dd/MM/yyyy", (DateTime)v.EndDate);
                    work.Add(new { StartDate = (DateTime)v.StartDate, EndDate = (DateTime)v.EndDate, Content = content });
                }
            }
            work.RemoveAt(0);
            work = work.OrderBy(o => o.StartDate).ToList();

            var profileDiploma = profileDiplomaService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var diplomas = ms.GetMasterDataList("educationLevel");
            var listDiploma = new List<string[]>();
            foreach (DataRow dr in diplomas.Rows)
            {
                var o = new[] { dr["Id"].ToString(), dr["Name"].ToString(), dr["Rank"].ToString() };
                listDiploma.Add(o);
            }

            var listdiploma1 = listDiploma.Except(listDiploma.Where(w => !int.TryParse(w[2], out int z)).ToList()).ToList();
            var highestLevel = "";
            foreach (var v in listdiploma1)
            {
                var x = v[0];
                var level = profileDiploma.FirstOrDefault(f => f.Level.ToString() == v[0]);
                if (level != null)
                {
                    highestLevel = v[1];
                    break;
                }
            }
            foreach (var d in profileDiploma)
            {
                var place = d.Place;
                var major = d.Major;
                var form = d.FormOfTrainning;
                var level = d.Level;

                d.Place = ms.GetSingleMasterData("educationOrg", place).Name;
                d.Major = ms.GetSingleMasterData("educationField", major).Name;
                var timespan = "";
                if (d.StartDate.HasValue && d.EndDate.HasValue)
                {
                    timespan = ((DateTime)d.StartDate).ToString("dd/MM/yyyy") + " - " + ((DateTime)d.EndDate).ToString("dd/MM/yyyy");
                }
                d.Faculty = timespan;
                d.FormOfTrainning = ms.GetSingleMasterData("educationType", form).Name;
                d.Level = ms.GetSingleMasterData("educationLevel", level).Name;
            }

            var profileCertificate = profileCertificateService.GetAllByEmployeeIDNotPaging(employeeId).ToList();
            var profileReward = profileRewardService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var profileDiscipline = profileDisciplineService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var profileAssess = profileAssessService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var profileContract = profileContractService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var profileRelationship = profileRelationshipService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            foreach (var v in profileRelationship)
            {
                v.Relationship = ms.GetSingleMasterData("family", v.Relationship).Name;
                if (v.Age.HasValue)
                {
                    if ((short)v.Age < DateTime.Now.Year) v.Age = (short)((short)DateTime.Now.Year - (short)v.Age);
                    else v.Age = null;
                }
                v.Job = string.Join(", ", new[] { v.Job, v.PlaceOfJob }.Where(w => !string.IsNullOrEmpty(w)));
            }
            var partnerRelationship = profileRelationship.Where(w => w.IsPartnerRelationship == true).ToList();
            var selfRelationship = profileRelationship.Except(partnerRelationship).ToList();
            var profileSalary = profileSalaryService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var profileInsurance = profileInsuranceService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var profileAttach = profileAttachmentService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var auditTickDetail = profileWorkTrialService.GetByEmployeeNotPaging(employeeId).ToList();

            ReportViewer1.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"ReportDesigner\EmployeeProfile_1.rdlc";
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsProfileWorkExperienceDetail", profileWorkExperience));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsProfileTraining", work));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsProfileDiplomaDetail", profileDiploma));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsProfileCertificateDetail", profileCertificate));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsProfileRewardDetail", profileReward));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsProfileDisciplineDetail", profileDiscipline));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsProfileAssessDetail", profileAssess));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsProfileContractDetail", profileContract));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsProfileSelfRelationshipDetail", selfRelationship));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsProfilePartnerRelationshipDetail", partnerRelationship));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsProfileSalaryDetail", profileSalary));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsProfileInsuranceDetail", profileInsurance));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsProfileAttachDetail", profileAttach));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsAuditTickDetail", auditTickDetail));

            var data = profileCuriculmViateService.GetByEmployeeId(employeeId);
            var reportParams = new List<ReportParameter>();
            if (data != null)
            {
                reportParams.Add(new ReportParameter("Aliases", data.Aliases));
                var birthDay = ""; var birthMonth = ""; var birthYear = "";
                if (data.DateOfBirth.HasValue)
                {
                    var dt = (DateTime)data.DateOfBirth;
                    birthDay = dt.Day.ToString();
                    birthMonth = dt.Month.ToString();
                    birthYear = dt.Year.ToString();

                }
                reportParams.Add(new ReportParameter("BirthDay", birthDay));
                reportParams.Add(new ReportParameter("BirthMonth", birthMonth));
                reportParams.Add(new ReportParameter("BirthYear", birthYear));
                reportParams.Add(new ReportParameter("IsMale", data.IsMale == true ? "Nam" : data.IsMale == false ? "Nữ" : ""));
                var bornCommune = ms.GetSingleMasterData("commune", data.PlaceOfBirthCommune).Name;
                reportParams.Add(new ReportParameter("BornCommune", bornCommune));
                var bornDistrict = ms.GetSingleMasterData("district", data.PlaceOfBirthDistrict).Name;
                reportParams.Add(new ReportParameter("BornDistrict", bornDistrict));
                var bornCity = ms.GetSingleMasterData("city", data.PlaceOfBirthCity).Name;
                reportParams.Add(new ReportParameter("BornCity", bornCity));
                var homeCommune = ms.GetSingleMasterData("commune", data.HomeTownCommune).Name;
                reportParams.Add(new ReportParameter("HomeCommune", homeCommune));
                var homeDistrict = ms.GetSingleMasterData("district", data.HomeTownDistrict).Name;
                reportParams.Add(new ReportParameter("HomeDistrict", homeDistrict));
                var homeCity = ms.GetSingleMasterData("city", data.HomeTownCity).Name;
                reportParams.Add(new ReportParameter("HomeCity", homeCity));
                var ethnic = ms.GetSingleMasterData("ethnic", data.People).Name;
                reportParams.Add(new ReportParameter("Ethnic", ethnic));
                var religion = ms.GetSingleMasterData("religion", data.Religion).Name;
                reportParams.Add(new ReportParameter("Religion", religion));
                reportParams.Add(new ReportParameter("PermanentResidence", data.PermanentResidence));
                reportParams.Add(new ReportParameter("CurrentResidence", data.CurrentResidence));
                var political = ms.GetSingleMasterData("political", data.PoliticalTheory).Name;
                reportParams.Add(new ReportParameter("PoliticalTheory", data.PermanentResidence));
                var management = ms.GetSingleMasterData("management", data.GovermentManagement).Name;
                reportParams.Add(new ReportParameter("GovermentManagement", data.PermanentResidence));
                reportParams.Add(new ReportParameter("NumberOfIdentityCard", data.NumberOfIdentityCard));
                reportParams.Add(new ReportParameter("DateIssueOfIdentityCard", data.DateIssueOfIdentityCard.HasValue ? String.Format("{0:dd/MM/yyyy}", data.DateIssueOfIdentityCard) : string.Empty));
                reportParams.Add(new ReportParameter("DateOnGroup", data.DateOnGroup.HasValue ? String.Format("{0:dd/MM/yyyy}", data.DateOnGroup) : string.Empty));
                reportParams.Add(new ReportParameter("PositionGroup", data.PositionGroup));
                reportParams.Add(new ReportParameter("DateAtParty", data.DateAtParty.HasValue ? String.Format("{0:dd/MM/yyyy}", data.DateAtParty) : string.Empty));
                reportParams.Add(new ReportParameter("DateOfJoinParty", data.DateOfJoinParty.HasValue ? String.Format("{0:dd/MM/yyyy}", data.DateOfJoinParty) : string.Empty));
                reportParams.Add(new ReportParameter("DateOnArmy", data.DateOnArmy.HasValue ? String.Format("{0:dd/MM/yyyy}", data.DateOnArmy) : string.Empty));
                reportParams.Add(new ReportParameter("ArmyRank", data.ArmyRank));
                reportParams.Add(new ReportParameter("Forte", data.Forte));
                reportParams.Add(new ReportParameter("HighestLevel", highestLevel));
                var disciplines = new VPHumanProfileDisciplineService().GetAllByEmployeeIdNotPaging(employeeId);
                var disciplineMst = ms.GetMasterDataList("discipline").Rows;
                var listDiscipline = new List<string[]>();
                foreach (DataRow dr in disciplineMst)
                {
                    var o = new[] { dr["Id"].ToString(), dr["Name"].ToString(), dr["Rank"].ToString() };
                    listDiscipline.Add(o);
                }
                listDiscipline = listDiscipline.OrderBy(o => int.Parse(o[2])).ToList();
                var disciplineForm = "";
                var disciplineYear = "";
                foreach (var v in listDiscipline)
                {
                    var highestDiscipline = disciplines.FirstOrDefault(w => w.Form.ToString() == v[0]);
                    if (highestDiscipline != null)
                    {
                        disciplineForm = highestDiscipline.Form.ToString();
                        disciplineYear = highestDiscipline.DateOfDecision.HasValue ? " (" + ((DateTime)highestDiscipline.DateOfDecision).Year.ToString() + ")" : "";
                        break;
                    }
                }
                var disciplineName = "";
                disciplineName = disciplineForm == "" ? "" : ms.GetSingleMasterData("discipline", disciplineForm).Name;
                reportParams.Add(new ReportParameter("Discipline", disciplineName + disciplineYear));

                var rewards = new VPHumanProfileRewardService().GetAllByEmployeeIdNotPaging(employeeId);
                var rewardMst = ms.GetMasterDataList("award").Rows;
                var listReward = new List<string[]>();
                foreach (DataRow dr in rewardMst)
                {
                    var o = new[] { dr["Id"].ToString(), dr["Name"].ToString(), dr["Rank"].ToString() };
                    listReward.Add(o);
                }
                listReward = listReward.OrderBy(o => int.Parse(o[2])).ToList();
                var rewardForm = "";
                var rewardYear = "";
                foreach (var v in listReward)
                {
                    var highestReward = rewards.FirstOrDefault(w => w.Form.ToString() == v[0]);
                    if (highestReward != null)
                    {
                        rewardForm = highestReward.Form.ToString();
                        rewardYear = highestReward.DateOfDecision.HasValue ? " (" + ((DateTime)highestReward.DateOfDecision).Year.ToString() + ")" : "";
                        break;
                    }
                }
                var rewardName = "";
                rewardName = rewardForm == "" ? "" : ms.GetSingleMasterData("award", rewardForm).Name;
                reportParams.Add(new ReportParameter("Reward", rewardName + rewardYear));

            }
            ReportViewer1.LocalReport.EnableExternalImages = true;
            var employee = employeeService.GetById(employeeId);
            reportParams.Add(new ReportParameter("EmployeeName", employee == null ? string.Empty : employee.Name.ToUpper()));
            var avatarUrl = "";
            if (employee.AvatarUrl == "/Content/Images/underfind.jpg") {
                avatarUrl = defaultAvatar;
            }
            else
            {
                avatarUrl = employee.AvatarUrl.Remove(0, 18);
            }
            reportParams.Add(new ReportParameter("Avatar", avatarUrl));

            ReportViewer1.LocalReport.SetParameters(reportParams);
        }
    }
}