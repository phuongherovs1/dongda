﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class NotPerformController : FrontController
    {
        public ActionResult Index(Guid taskId)
        {
            ViewData["TaskID"] = taskId;
            ViewData["AllowCreate"] = TaskResourceService.CheckIsCreate(taskId);
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        public ActionResult LoadData(Guid taskId, StoreRequestParameters parameter)
        {
            try
            {
                var count = 0;
                var data = TaskResourceService.GetListViewAndReview(taskId, parameter.Page, parameter.Limit, out count);
                return this.Store(data, count);
            }
            catch
            {
                return this.Store(null);
            }
        }
        public ActionResult Form(Guid? id, Guid? taskId)
        {
            var model = TaskResourceService.GetFormItem(id, taskId);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        [HttpPost]
        public ActionResult Create(TaskResourceBO taskResource)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    TaskResourceService.Insert(taskResource);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult Update(TaskResourceBO taskResource)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    TaskResourceService.Update(taskResource);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var message = default(string);
            try
            {
                TaskResourceService.Delete(id);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
    }
}