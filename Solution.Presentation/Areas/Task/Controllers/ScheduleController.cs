﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class ScheduleController : FrontController
    {
        public ActionResult Index()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }
        public ActionResult Week()
        {
            var model = EmployeeService.GetCurrentUser();
            return PartialView(model);
        }
        public ActionResult Day(string containerId, DateTime? date, Guid? employeeId)
        {
            var model = TaskScheduleService.GetWeek_Task(employeeId, date);
            return new Ext.Net.MVC.PartialViewResult
            {
                ClearContainer = true,
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                Model = model,
                WrapByScriptTag = false
            };
        }
        public ActionResult LoadData(StoreRequestParameters parameter)
        {
            try
            {
                var count = 0;
                var employeeId = TaskScheduleService.UserId;
                var data = TaskScheduleService.GetListScheduleByEmployee(employeeId, parameter.Page, parameter.Limit, out count);
                return this.Store(data, count);
            }
            catch
            {
                return this.Store(null);
            }
        }
        public ActionResult Form(Guid? id)
        {
            var model = TaskScheduleService.GetFormItem(id);
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(TaskScheduleBO item)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    TaskScheduleService.Create(item);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(TaskScheduleBO item)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    TaskScheduleService.Update(item);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var message = default(string);
            try
            {
                TaskScheduleService.Delete(id);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult Cancel(Guid id)
        {
            var message = default(string);
            try
            {
                object model = null;
                model = TaskScheduleService.GetById(id);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Cancel(TaskScheduleBO item)
        {
            var message = default(string);
            try
            {
                TaskScheduleService.Cancel(item);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult Pause(Guid id)
        {
            var message = default(string);
            try
            {
                object model = null;
                model = TaskScheduleService.GetById(id);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Pause(TaskScheduleBO item)
        {
            var message = default(string);
            try
            {
                TaskScheduleService.Pause(item);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
    }
}