﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class ManageController : FrontController
    {
        public ActionResult Index(bool roleedit = false)
        {
            ViewData["isroleedit"] = roleedit;
            ViewData["TaskStatus"] = iDAS.Service.Common.Resource.Task.GetStatusNew();
            ViewData["TaskRole"] = iDAS.Service.Common.Resource.Task.GetResourceRoles();
            return PartialView();
        }
        public ActionResult LoadEmployees(Guid taskId, StoreRequestParameters parameter, string query = default(string))
        {
            try
            {
                var count = 0;
                var employeeIds = TaskResourceService.GetEmployeeIds(taskId);
                //var data = EmployeeService.GetInfoNotInIds(employeeIds: employeeIds, query: query, allowGetRoleName: true, allowDeleted: false);
                var data = EmployeeService.GetInfo(parameter.Page, parameter.Limit, out count, query: query, allowGetRoleName: true, allowDeleted: false).ToList();
                return this.Store(data, count);
            }
            catch
            {
                return this.Store(null);
            }
        }
        public ActionResult LoadData(int taskRole, int taskStatus, int pageSites = 14, int pageIndex = 0)
        {
            try
            {
                var employeeId = TaskService.UserId;
                var data = TaskResourceService.GetListTaskByEmployee(employeeId, taskRole, taskStatus, pageSites, pageIndex);
                return this.Store(data.OrderByDescending(x => x.StartAt));
            }
            catch (Exception ex)
            {
                return this.Store(new List<TaskResourceBO>());
            }

        }
        public ActionResult LoadDataHistoryTask(string taskId = default(string))
        {
            List<HistoryTaskBO> his = new List<HistoryTaskBO>();
            if (!string.IsNullOrEmpty(taskId))
            {
                DataTable data = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_HistoryTask_GetData", parameter: new
                {
                    taskId = taskId,
                });
                if (data != null && data.Rows.Count > 0)
                {
                    foreach(DataRow item in data.Rows)
                    {
                        HistoryTaskBO hisItem = new HistoryTaskBO();
                        hisItem.Name = item["Name"] != null ? item["Name"].ToString() : "";
                        hisItem.Content = item["Content"] != null ? item["Content"].ToString() : "";
                        hisItem.Percent = item["Percent"] != null ? item["Percent"].ToString() : "";
                        if (item["CreateDate"] != null)
                            hisItem.CreateDate = Convert.ToDateTime(item["CreateDate"].ToString());
                        hisItem.Avatar = item["Avatar"] != null ? Encoding.ASCII.GetBytes(item["Avatar"].ToString()) : new byte[0];
                        his.Add(hisItem);
                    }
                }
            }
            return this.Store(his);
        }

        public ActionResult Form(Guid? id, bool? isView)
        {
            var model = TaskService.GetFormItem(id);
            model.StartAt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 8, 0, 0);
            model.EndAt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 18, 0, 0);
            if (model.KPICatagoryId.HasValue)
            {
                var item = KPICategoryService.GetById(model.KPICatagoryId.Value);
                model.KPICatagoryText = item.Name;
                if (item.Maxscore.HasValue)
                    model.MaxScore = "Điểm tối đa: " + item.Maxscore;
                else
                    model.MaxScore = "";
            }
            if (isView.HasValue)
                ViewData["IsView"] = isView.Value;
            else
                ViewData["IsView"] = false;
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        [HttpPost]
        public ActionResult Create(TaskBO item, string DataResourceEmployees = "")
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    var taskId = TaskService.Create(item);
                    if (item.EmployeeIdOther.HasValue)
                    {
                        TaskPerformBO taskPerformBO = new TaskPerformBO();
                        taskPerformBO.TaskId = taskId;
                        taskPerformBO.Employee.Id = item.EmployeeIdOther.Value;
                        taskPerformBO.StartAt = item.StartAt;
                        taskPerformBO.EndAt = item.EndAt;
                        taskPerformBO.Unit = 100;
                        taskPerformBO.Content = item.Content;
                        TaskPerformService.Send(taskPerformBO);
                        Notify(TitleResourceNotifies.AssignTask + " \"" + item.Name + "\"", item.Content, taskPerformBO.Employee.Id, UrlResourceNotifies.AssignTask, new { id = taskPerformBO.Id, taskId = taskPerformBO.TaskId });
                    }
                    if (!string.IsNullOrEmpty(DataResourceEmployees))
                        ConvertToTaskPerformBO(taskId, DataResourceEmployees, item.Name);
                    return this.Direct(taskId);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        private void ConvertToTaskPerformBO(Guid TaskId, string DataResourceEmployees, string nametask = default(string))
        {
            try
            {
                List<EmployeeNotPerForm> Employees = JSON.Deserialize<List<EmployeeNotPerForm>>(DataResourceEmployees);
                foreach (EmployeeNotPerForm item in Employees)
                {
                    string requets = "";
                    TaskResourceBO taskResourceBO = new TaskResourceBO();
                    taskResourceBO.TaskId = TaskId;
                    taskResourceBO.EmployeeId = item.EmployeeId;
                    if (item.IsRoleView == true)
                    {
                        taskResourceBO.IsRoleView = item.IsRoleView;
                        requets += ",<b>Theo dõi </b>";
                    }
                    if (item.IsRoleReview == true)
                    {
                        taskResourceBO.IsRoleReview = item.IsRoleReview;
                        requets += ",<b>Kiểm soát </b>";
                    }
                    if (item.IsRoleMark == true)
                    {
                        taskResourceBO.IsRoleMark = item.IsRoleMark;
                        requets += ",<b>Cho điểm </b>";
                    }
                    taskResourceBO.Note = item.Content;
                    TaskResourceService.Insert(taskResourceBO);
                    Notify(TitleResourceNotifies.AssignRequetsTask + " " + (!string.IsNullOrEmpty(requets)? requets.Substring(1) : "") + " với công việc \"" + nametask + "\"", item.Content, item.EmployeeId.Value, UrlResourceNotifies.TaskFrom, new { id = TaskId, isView = true });
                }
            }
            catch (Exception e) { }

        }
        [HttpPost]
        public ActionResult Update(TaskBO item)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    TaskService.Update(item);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    TaskService.Delete(id);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        public ActionResult Cancel(Guid id)
        {
            var message = default(string);
            try
            {
                object model = null;
                model = TaskService.GetById(id);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Cancel(TaskBO item)
        {
            var message = default(string);
            try
            {
                TaskService.Cancel(item);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult Pause(Guid id)
        {
            var message = default(string);
            try
            {
                object model = null;
                model = TaskService.GetById(id);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Pause(TaskBO item)
        {
            var message = default(string);
            try
            {
                TaskService.Pause(item);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult Finish(Guid id)
        {
            var message = default(string);
            try
            {
                object model = null;
                model = TaskService.GetById(id);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Finish(TaskBO item)
        {
            var message = default(string);
            try
            {
                TaskService.Finish(item);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult Control(Guid taskId)
        {
            try
            {
                var obj = TaskService.GetById(taskId);
                return new Ext.Net.MVC.PartialViewResult { Model = obj };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult Summary(string containerId, int taskRole)
        {
            var model = TaskService.GetSummary(taskRole);
            return new Ext.Net.MVC.PartialViewResult
            {
                ClearContainer = true,
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                Model = model,
                WrapByScriptTag = false
            };
        }
        public ActionResult GridTaskSummary()
        {
            return new Ext.Net.MVC.PartialViewResult();
        }
        public ActionResult GetMaxScore(Guid? id)
        {
            var result = "";
            if (id.HasValue)
                result = KPICategoryService.GetById(id.Value).Maxscore.ToString();
            return this.Direct(result: result);
        }

    }
    public class EmployeeNotPerForm
    {
        public Guid? EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public bool IsRoleView { get; set; }
        public bool IsRoleReview { get; set; }
        public bool IsRoleMark { get; set; }
        public string Content { get; set; }
    }
}