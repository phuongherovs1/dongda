﻿using iDAS.Presentation.Controllers;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class GanttController : FrontController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetData(Guid? taskID = null)
        {
            var data = GanttResourceService.GetByTask(taskID.Value).ToList();
            return Content(JsonConvert.SerializeObject(data), "application/json");
        }

        public ActionResult ControlResources(Guid taskId)
        {
            try
            {
                return new Ext.Net.MVC.PartialViewResult { Model = taskId };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ActionResult BaselineResources(Guid? taskID = null)
        {
            ViewBag.TaskID = taskID;
            return View();
        }
    }
}