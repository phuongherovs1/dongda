﻿using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using iDAS.Service.Common;
using System;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class SecurityController : FrontController
    {
        //
        // GET: /Task/Security/

        #region Danh sách
        public ActionResult Index()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }
        public ActionResult Form(string alignTo, string renderTo, string storeId)
        {
            ViewData["AlignToId"] = string.IsNullOrEmpty(alignTo) ? "ctDesktop" : alignTo;
            ViewData["RenderToId"] = string.IsNullOrEmpty(renderTo) ? "ctDesktop" : renderTo;
            ViewData["storeId"] = string.IsNullOrEmpty(storeId) ? "storeSecurity" : storeId;
            var model = new TaskSecurityBO();
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = model };
        }

        /// <summary>
        /// Get all Task sercurity
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAll()
        {
            var data = TaskSecurityService.GetAll().ToList();
            return this.Store(data);
        }


        #endregion
        #region Thêm mới
        [ValidateInput(false)]
        public ActionResult Create(TaskSecurityBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    TaskSecurityService.Insert(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion

        #region chi tiết
        public ActionResult Delete(Guid Id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    TaskSecurityService.Delete(Id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);

        }

        [ValidateInput(false)]
        public ActionResult Update(TaskSecurityBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    //TaskSecurityService.UpdateTaskSecurity(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        #endregion


    }
}