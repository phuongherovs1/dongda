﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class SchedulePerformController : FrontController
    {
        public ActionResult Index(Guid taskScheduleId, bool allowCreate)
        {
            var message = default(string);
            try
            {
                ViewData["TaskScheduleID"] = taskScheduleId;
                ViewData["AllowCreate"] = allowCreate;
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult LoadData(Guid taskScheduleId, StoreRequestParameters parameter)
        {
            try
            {
                var count = 0;
                var data = TaskSchedulePerformService.GetBySchedule(taskScheduleId, parameter.Page, parameter.Limit, out count);
                return this.Store(data, count);
            }
            catch
            {
                return this.Store(null);
            }
        }
        public ActionResult Form(Guid? id, Guid? taskScheduleId)
        {
            var model = TaskSchedulePerformService.GetFormItem(id, taskScheduleId);
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        [HttpPost]
        public ActionResult Create(TaskSchedulePerformBO item)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    TaskSchedulePerformService.Create(item);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult Update(TaskSchedulePerformBO item)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    TaskSchedulePerformService.Update(item);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var message = default(string);
            try
            {
                TaskSchedulePerformService.Delete(id);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult SendOnList(string id)
        {
            var message = default(string);
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    id = id.Substring(1);
                    List<string> lstID = id.Split(',').ToList();
                    DateTime startAt = DateTime.MinValue;
                    DateTime EndAt = DateTime.MinValue;
                    Guid EmployeeSetupId = Guid.Empty;
                    foreach (string itemId in lstID)
                    {
                        var item = TaskSchedulePerformService.GetById(new Guid(itemId));
                        if (item.StartAt == DateTime.MinValue || item.EndAt == DateTime.MinValue || item.EmployeeSetupId == Guid.Empty)
                        {
                            item.StartAt = startAt;
                            item.EndAt = EndAt;
                            item.EmployeeSetupId = EmployeeSetupId;
                        }
                        else
                        {
                            startAt = item.StartAt;
                            EndAt = item.EndAt;
                            EmployeeSetupId = item.EmployeeSetupId;
                        }
                        TaskSchedulePerformService.Send(item);
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult Send(TaskSchedulePerformBO item)
        {
            var message = default(string);
            try
            {
                TaskSchedulePerformService.Send(item);
                item.IsAccept = true;
                TaskSchedulePerformService.Confirm(item);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult Confirm(Guid id)
        {
            var message = default(string);
            try
            {
                object model = null;
                var item = TaskSchedulePerformService.GetById(id);
                item.UserId = TaskSchedulePerformService.UserId;
                if (item.ScheduleId.HasValue)
                {
                    var itemSchedule = TaskScheduleService.GetById(item.ScheduleId.Value);
                    itemSchedule.IsConfirm = true;
                    TaskScheduleService.Update(itemSchedule);
                }
                model = item;
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult Confirm(TaskSchedulePerformBO item)
        {
            var message = default(string);
            try
            {
                TaskSchedulePerformService.Confirm(item);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult Cancel(Guid id)
        {
            var message = default(string);
            try
            {
                object model = null;
                model = TaskSchedulePerformService.GetById(id);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Cancel(TaskSchedulePerformBO item)
        {
            var message = default(string);
            try
            {
                TaskSchedulePerformService.Cancel(item);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult Pause(Guid id)
        {
            var message = default(string);
            try
            {
                object model = null;
                model = TaskSchedulePerformService.GetById(id);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Pause(TaskSchedulePerformBO item)
        {
            var message = default(string);
            try
            {
                TaskSchedulePerformService.Pause(item);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
    }
}