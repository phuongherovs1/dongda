﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class CommunicationController : FrontController
    {
        public ActionResult Index()
        {
            ViewData["CommunicationType"] = iDAS.Service.Common.Resource.Task.GetCommunicationType();
            return PartialView();
        }
        public ActionResult LoadDataType()
        {
            var data = iDAS.Service.Common.Resource.Task.GetCommunicationTypeAdd()
                .Select(t => new ComboboxBO
                {
                    Name = t.Value,
                    ID = t.Key
                }).ToList();
            return this.Store(data);
        }
        public ActionResult Report(Guid taskResourceId)
        {
            ViewData["TaskResourceID"] = taskResourceId;
            return PartialView();
        }
        public ActionResult LoadData(Guid taskResourceId, Guid? taskId, int type)
        {
            try
            {
                var userId = UserService.GetUserCurrent().Id;
                var data = TaskCommunicationService.Get(taskResourceId, type, userId).ToList();
                var task = new TaskBO();
                if (taskId.HasValue)
                    task = TaskService.GetById(taskId.Value);
                foreach (TaskCommunicationBO item in data)
                {
                    if (taskId.HasValue)
                    {
                        item.TaskId = task.Id;
                        item.TaskName = task.Name;
                    }
                    var files = new TaskCommunicationFileService().GetByCommunication(item.Id);
                    item.AttachFiles = new FileUploadBO()
                    {
                        Files = files.Select(x => x.FileId.Value)
                    };
                }
                return this.Store(data);
            }
            catch
            {
                return this.Store(null);
            }
        }
        public ActionResult LoadDataByType(int typeId = 0)
        {
            var userId = UserService.GetUserCurrent().Id;
            var data = TaskCommunicationService.GetDataByUser(typeId, userId).ToList();
            return this.Store(data);
        }
        public ActionResult TaskCommunication(Guid taskResourceId, Guid? taskId)
        {
            try
            {
                if(taskId.HasValue)
                    ViewData["TaskId"] = taskId.Value;
                else
                    ViewData["TaskId"] = Guid.Empty;
                ViewData["CommunicationType"] = iDAS.Service.Common.Resource.Task.GetCommunicationType();
                return new Ext.Net.MVC.PartialViewResult { Model = taskResourceId, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult GetResourceShare(string taskResourceId)
        {
            var data = TaskResourceService.GetByTaskResource(new Guid(taskResourceId));
            return this.Store(data);
        }
        public ActionResult UpdateRate(Guid taskId, double rate)
        {
            var obj = TaskPerformService.GetById(taskId);
            obj.CompleteRate = rate;
            TaskPerformService.Update(obj);
            return this.Direct();
        }
        public ActionResult UpdateFinishTask(Guid taskId, bool value)
        {
            var obj = TaskPerformService.GetById(taskId);
            if (value)
                obj.FinishAt = DateTime.Now;
            else
                obj.FinishAt = null;
            obj.IsFinish = value;
            TaskPerformService.Update(obj);
            return this.Direct();
        }
        public ActionResult Form(string id = default(string), string taskResoureId = default(string))
        {
            object model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new TaskCommunicationBO() { TaskResourceId = new Guid(taskResoureId) };

            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = TaskCommunicationService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult Create(TaskCommunicationBO communication, string resourceIds)
        {
            try
            {
                var taskResourceGuidIds = JSON.Deserialize<List<Guid>>(resourceIds);
                if (taskResourceGuidIds.Count() > 0)
                {
                    TaskCommunicationService.InsertCommunication(communication, taskResourceGuidIds);
                }
                else
                {
                    TaskCommunicationService.Create(communication);
                }
                return this.Direct(result: true);
            }
            catch (Exception)
            {
                return this.Direct(result: false);
            }
        }
        [ValidateInput(false)]
        public ActionResult Insert(Guid parentId, string content)
        {
            try
            {
                content = content.Replace("\n", string.Empty).Trim();
                var comment = new TaskCommunicationBO();
                comment.ParentId = parentId;
                comment.Content = HttpUtility.HtmlEncode(content);
                comment.IsPublic = true;
                comment.TaskResourceId = TaskCommunicationService.GetById(parentId).TaskResourceId;
                // comment.TaskCommunicationTypeId = TaskCommunicationTypeService.GetAll().Where(t => t.IsDefault.Value).Select(t => t.Id).FirstOrDefault();
                var commentId = TaskCommunicationService.Insert(comment);
                var currentUser = EmployeeService.GetUserCurrent();
                var resuts = new
                {
                    AvartarUrl = currentUser.AvatarUrl,
                    EmployeeName = currentUser.Name,
                    CommentID = commentId,
                    Success = true
                };
                var parent = TaskCommunicationService.GetById(parentId);
                Notify(TitleResourceNotifies.CommunicationTask, content, parent.CreateBy.Value, UrlResourceNotifies.CommunicationTask, new { taskResourceId = parent.TaskResourceId });
                return this.Direct(result: resuts);
            }
            catch (Exception)
            {
                var currentUser = EmployeeService.GetUserCurrent();
                var resutError = new
                {
                    AvartarUrl = currentUser.AvatarUrl,
                    EmployeeName = currentUser.Name,
                    Success = false
                };
                return this.Direct(result: resutError);
            }
        }
        [ValidateInput(false)]
        public ActionResult Update(Guid id, string content)
        {
            try
            {
                content = content.Replace("\n", string.Empty).Trim();
                var comment = TaskCommunicationService.GetById(id);
                comment.Content = HttpUtility.HtmlEncode(content);
                TaskCommunicationService.Update(comment);
                return this.Direct(result: true);
            }
            catch (Exception)
            {
                return this.Direct(result: false);
            }
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            try
            {
                TaskCommunicationService.Delete(id);
                result = true;
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Revert(Guid id)
        {
            var result = false;
            try
            {
                TaskCommunicationService.Revert(id);
                result = true;
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
    }
}