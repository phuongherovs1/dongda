﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class DashBoardController : FrontController
    {
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        public ActionResult LoadResources()
        {
            var data = EmployeeService.GetAll();
            return this.Store(data);
        }
        public ActionResult GetData(string filterName = default(string))
        {
            var data = TaskService.GetAll();
            if (!string.IsNullOrEmpty(filterName))
                data = data.Where(x => x.Name.ToUpper().Contains(filterName.ToUpper()));
            return this.Store(data);
        }
    }
}