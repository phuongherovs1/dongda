﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Common;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Task.Controllers
{
    //[Authorize(Roles = Common.Role.Administrator)]
    public class CalendarController : FrontController
    {
        #region Lịch làm việc
        public ActionResult Index()
        {
            return PartialView();
        }
        public ActionResult LoadActive()
        {
            return this.Store(CalendarService.GetActive().OrderByDescending(t => t.CreateAt).ToList());
        }
        public ActionResult LoadData()
        {
            return this.Store(CalendarService.GetAll().OrderByDescending(t => t.CreateAt).ToList());
        }
        public ActionResult Form(string id = default(string))
        {
            object model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new CalendarBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = CalendarService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        [HttpPost]
        public ActionResult Create(CalendarBO calendar)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    var calendarID = CalendarService.Insert(calendar);
                    if (calendarID == Guid.Empty)
                    {
                        this.ShowNotify(Common.Resource.SystemFailure);
                    }
                    else
                    {
                        this.ShowNotify(Common.Resource.SystemSuccess);
                        result = true;
                    }
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Update(CalendarBO calendar)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    CalendarService.Update(calendar);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            try
            {
                CalendarService.Delete(id);
                this.ShowNotify(Common.Resource.SystemSuccess);
                result = true;
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion
        #region Ngày thay đổi
        public ActionResult GetDataDayOverride(StoreRequestParameters parameters, string calendarId, DateTime date)
        {
            if (!string.IsNullOrEmpty(calendarId))
            {
                var data = CalendarDayOverrideShiftService.GetByDate(new Guid(calendarId), date);
                return this.Store(data, data.Count());
            }
            else
            {
                return this.Store(new List<object>());
            }
        }

        public ActionResult ShowCalendarDayOverrideShift(string calendarDayOverrideId = default(string))
        {
            try
            {
                var obj = new CalendarDayOverrideShiftBO();
                obj.CalendarDayOverrideId = new Guid(calendarDayOverrideId);
                return new Ext.Net.MVC.PartialViewResult { ViewName = "_CalendarDayOverrideShift", Model = obj };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult CreateCalendarDayOverrideShift(CalendarDayOverrideShiftBO calendarDayOverrideShift)
        {
            try
            {
                CalendarDayOverrideShiftService.Insert(calendarDayOverrideShift);
                return this.Direct(result: true);
            }
            catch (Exception)
            {
                return this.Direct(result: false);
            }
        }
        public ActionResult UpdateCalendarDayOverrideShift(CalendarDayOverrideShiftBO calendarDayOverrideShift)
        {
            try
            {
                CalendarDayOverrideShiftService.Update(calendarDayOverrideShift);
                return this.Direct(result: true);
            }
            catch (Exception)
            {
                return this.Direct(result: false);
            }
        }
        public ActionResult UpdateCalendarDayOverrideShiftWindow(Guid calendarDayOverrideShiftId)
        {
            try
            {
                var obj = CalendarDayOverrideShiftService.GetById(calendarDayOverrideShiftId);
                return new Ext.Net.MVC.PartialViewResult { ViewName = "_UpdateCalendarDayOverrideShift", Model = obj };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ActionResult GetDateHighLight(string calendarId = default(string))
        {
            var dateHighLight = new List<CalendarHighLight>();
            if (calendarId == "null" || calendarId == default(string) || calendarId.Equals("") || calendarId.Equals(new Guid().ToString()))
            {
                // do someting else
            }
            else
            {
                var data = CalendarDayOverrideService.GetByCalendar(new Guid(calendarId)).ToList();
                foreach (var item in data)
                {
                    int styleId = 1;
                    if (item.IsWorkingDay.HasValue && item.IsWorkingDay.Value)
                    {
                        styleId = 2;
                    }
                    else
                    {
                        styleId = 3;
                    }
                    if (item.IsHoliday.HasValue && item.IsHoliday.Value)
                        styleId = 4;
                    dateHighLight.Add(new CalendarHighLight()
                    {
                        StyleID = styleId,
                        Date = item.Date.Value.ToString("MM/dd/yyyy"),
                        Title = item.Note
                    });
                }
            }
            return this.Direct(dateHighLight);
        }

        public ActionResult GetDateOverride(string calendarId, string date)
        {
            var dayOverride = CalendarDayOverrideService.GetByCalendarAndDate(new Guid(calendarId), DateTime.Parse(date));
            return this.Direct(result: dayOverride);
        }
        public ActionResult DeleteDayOverrideShift(Guid id)
        {
            try
            {
                CalendarDayOverrideService.Delete(id);
                return this.Direct(result: true);
            }
            catch
            {
                return this.Direct(result: false);
            }
        }
        #endregion

        public ActionResult GetWorkingDays()
        {
            var calendarId = new Guid("22e4afcb-bee4-4ed1-b5a7-30bd9646f00a");
            var startDate = new DateTime(2017, 7, 17, 08, 30, 0);
            var endDate = new DateTime(2017, 7, 20, 17, 29, 0);
            var result = CalendarService.GetWorkingDay(calendarId, startDate, endDate);
            return View();
        }
    }
}