﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Task.Controllers.Setting
{
    public class CalendarWeekShiftController : FrontController
    {
        public ActionResult Form(Guid calendarId, string id = default(string))
        {
            object model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new CalendarWeekShiftBO()
                {
                    CalendarId = calendarId
                };
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = CalendarWeekShiftService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult LoadData(StoreRequestParameters parameters, string calendarId = default(string))
        {
            if (calendarId != default(string) && !calendarId.Equals(""))
            {
                var data = CalendarWeekShiftService.GetWeekShiftCalendar(new Guid(calendarId));
                return this.Store(data, data.Count());
            }
            else
            {
                return this.Store(new List<object>());
            }
        }
        public ActionResult UpdateCalendarWeekShiftWindow(Guid rowId, DayOfWeek dayType)
        {
            try
            {
                var obj = CalendarWeekShiftService.GetByRowIDAndDayType(rowId, (int)dayType);
                if (obj == null)
                {
                    obj = new CalendarWeekShiftBO();
                    obj.RowId = rowId;
                    obj.CalendarId = CalendarWeekShiftService.GetCalendarByRowID(rowId);
                    obj.DayType = (int)dayType;
                }
                return new Ext.Net.MVC.PartialViewResult { ViewName = "_UpdateCalendarWeekShift", Model = obj };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ActionResult Create(CalendarWeekShiftBO calendarWeekShift, bool ckMo, bool ckTu, bool ckWe, bool ckTh, bool ckFr, bool ckSa, bool ckSu)
        {
            try
            {
                var exits = false;
                var day = new List<string>();
                if (ckMo)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Monday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Thứ Hai");
                    }
                }
                if (ckTu)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Tuesday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Thứ Ba");
                    }

                }
                if (ckWe)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Wednesday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Thứ Tư");
                    }
                }
                if (ckTh)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Thursday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Thứ Năm");
                    }
                }
                if (ckFr)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Friday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Thứ Sáu");
                    }
                }
                if (ckSa)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Saturday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Thứ Bảy");
                    }
                }
                if (ckSu)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Sunday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Chủ nhật");
                    }
                }
                if (!exits)
                {
                    calendarWeekShift.RowId = Guid.NewGuid();
                    if (ckMo)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Monday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    if (ckTu)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Tuesday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);

                    }
                    if (ckWe)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Wednesday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    if (ckTh)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Thursday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    if (ckFr)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Friday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    if (ckSa)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Saturday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    if (ckSu)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Sunday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    return this.Direct(result: true);
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = String.Join(", ", day.ToArray()) + " đã tồn tại ca làm việc từ: " + calendarWeekShift.StartTime + " đến: " + calendarWeekShift.EndTime + ". Vui lòng lựa chọn ca làm việc không được trùng thời gian giữa các ca!"
                    });
                    return this.Direct(result: "Exits");
                }

            }
            catch (Exception)
            {

                return this.Direct(result: false);
            }

        }
        public ActionResult Update(CalendarWeekShiftBO calendarWeekShift)
        {
            try
            {
                if (calendarWeekShift.Id != Guid.Empty)
                {
                    if (!CalendarWeekShiftService.CheckTimeShiftExitsEdit(calendarWeekShift.Id, calendarWeekShift.CalendarId, calendarWeekShift.DayType.Value, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        CalendarWeekShiftService.Update(calendarWeekShift);
                    }
                    else
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING,
                            Title = "Thông báo",
                            Message = "Đã tồn tại ca làm việc từ: " + calendarWeekShift.StartTime + " đến: " + calendarWeekShift.EndTime + ". Vui lòng lựa chọn ca làm việc không được trùng thời gian giữa các ca!"
                        });
                        return this.Direct(result: "Exits");
                    }
                }
                else
                {
                    if (!CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, calendarWeekShift.DayType.Value, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    else
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING,
                            Title = "Thông báo",
                            Message = "Đã tồn tại ca làm việc từ: " + calendarWeekShift.StartTime + " đến: " + calendarWeekShift.EndTime + ". Vui lòng lựa chọn ca làm việc không được trùng thời gian giữa các ca!"
                        });
                        return this.Direct(result: "Exits");
                    }
                }
                return this.Direct(result: true);
            }
            catch (Exception)
            {
                return this.Direct(result: false);
            }
        }
        public ActionResult Delete(Guid id)
        {
            try
            {
                CalendarWeekShiftService.Delete(id);
                return this.Direct(result: true);
            }
            catch
            {
                return this.Direct(result: false);
            }
        }
        public ActionResult CreateCalendarWeekShift(CalendarWeekShiftBO calendarWeekShift, bool ckMo, bool ckTu, bool ckWe, bool ckTh, bool ckFr, bool ckSa, bool ckSu)
        {
            try
            {
                var exits = false;
                var day = new List<string>();
                if (ckMo)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Monday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Thứ Hai");
                    }
                }
                if (ckTu)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Tuesday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Thứ Ba");
                    }

                }
                if (ckWe)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Wednesday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Thứ Tư");
                    }
                }
                if (ckTh)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Thursday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Thứ Năm");
                    }
                }
                if (ckFr)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Friday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Thứ Sáu");
                    }
                }
                if (ckSa)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Saturday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Thứ Bảy");
                    }
                }
                if (ckSu)
                {
                    if (CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, (int)DayOfWeek.Sunday, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        exits = true;
                        day.Add("Chủ nhật");
                    }
                }
                if (!exits)
                {
                    calendarWeekShift.RowId = Guid.NewGuid();
                    if (ckMo)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Monday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    if (ckTu)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Tuesday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);

                    }
                    if (ckWe)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Wednesday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    if (ckTh)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Thursday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    if (ckFr)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Friday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    if (ckSa)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Saturday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    if (ckSu)
                    {
                        calendarWeekShift.DayType = (int)DayOfWeek.Sunday;
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    return this.Direct(result: true);
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = String.Join(", ", day.ToArray()) + " đã tồn tại ca làm việc từ: " + calendarWeekShift.StartTime + " đến: " + calendarWeekShift.EndTime + ". Vui lòng lựa chọn ca làm việc không được trùng thời gian giữa các ca!"
                    });
                    return this.Direct(result: "Exits");
                }

            }
            catch (Exception)
            {

                return this.Direct(result: false);
            }

        }
        public ActionResult UpdateCalendarWeekShift(CalendarWeekShiftBO calendarWeekShift)
        {
            try
            {
                if (calendarWeekShift.Id != new Guid())
                {

                    if (!CalendarWeekShiftService.CheckTimeShiftExitsEdit(calendarWeekShift.Id, calendarWeekShift.CalendarId, calendarWeekShift.DayType.Value, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        CalendarWeekShiftService.Update(calendarWeekShift);
                    }
                    else
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING,
                            Title = "Thông báo",
                            Message = "Đã tồn tại ca làm việc từ: " + calendarWeekShift.StartTime + " đến: " + calendarWeekShift.EndTime + ". Vui lòng lựa chọn ca làm việc không được trùng thời gian giữa các ca!"
                        });
                        return this.Direct(result: "Exits");
                    }
                }
                else
                {
                    if (!CalendarWeekShiftService.CheckTimeShiftExits(calendarWeekShift.CalendarId, calendarWeekShift.DayType.Value, calendarWeekShift.StartTime.Value, calendarWeekShift.EndTime.Value))
                    {
                        CalendarWeekShiftService.Insert(calendarWeekShift);
                    }
                    else
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING,
                            Title = "Thông báo",
                            Message = "Đã tồn tại ca làm việc từ: " + calendarWeekShift.StartTime + " đến: " + calendarWeekShift.EndTime + ". Vui lòng lựa chọn ca làm việc không được trùng thời gian giữa các ca!"
                        });
                        return this.Direct(result: "Exits");
                    }
                }
                return this.Direct(result: true);
            }
            catch (Exception)
            {
                return this.Direct(result: false);
            }
        }
        public ActionResult ViewCalendarWeekShiftWindow(Guid id)
        {
            try
            {
                var obj = CalendarWeekShiftService.GetById(id);
                return new Ext.Net.MVC.PartialViewResult { ViewName = "_DetailCalendarWeekShift", Model = obj };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}