﻿using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Task.Controllers.Setting
{
    public class CalendarOverrideDayController : FrontController
    {
        //
        // GET: /Task/CalendarOverrideDay/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Update(CalendarDayOverrideBO calendarDayOverride)
        {
            try
            {
                var result = CalendarDayOverrideService.InsertUpdate(calendarDayOverride);
                return this.Direct(result: result);
            }
            catch (Exception e)
            {
                return this.Direct(result: null);
            }
        }
    }
}