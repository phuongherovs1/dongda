﻿using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using iDAS.Service.Common;
using System;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class TaskCategoryController : FrontController
    {
        //
        // GET: /Task/Category/

        #region Danh sách
        public ActionResult Index()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }
        public ActionResult Form(string alignTo, string renderTo, string storeId)
        {
            ViewData["AlignToId"] = string.IsNullOrEmpty(alignTo) ? "ctDesktop" : alignTo;
            ViewData["RenderToId"] = string.IsNullOrEmpty(renderTo) ? "ctDesktop" : renderTo;
            ViewData["storeId"] = string.IsNullOrEmpty(storeId) ? "storeCategory" : storeId;
            ViewData["DepartmentIDCategory"] = TaskCategoryService.GetDepartmentByUser();
            var model = new TaskCategoryBO();
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = model };
        }

        /// <summary>
        /// Get all Task sercurity
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAll()
        {
            var data = TaskCategoryService.GetAll().ToList();
            return this.Store(data);
        }

        #endregion
        #region Thêm mới
        [ValidateInput(false)]
        public ActionResult Create(TaskCategoryBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    TaskCategoryService.Insert(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
        #region chi tiết
        public ActionResult Delete(Guid Id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    TaskCategoryService.Delete(Id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);

        }

        [ValidateInput(false)]
        public ActionResult Update(TaskCategoryBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    //TaskCategoryService.UpdateTaskCategory(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion


    }
}