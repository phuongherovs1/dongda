﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class StatisticalController : FrontController
    {
        //
        // GET: /Task/Statistical/
        public ActionResult Index()
        {
            return PartialView();
        }

        //public StoreResult GetTaskCategoriesByDepartment(Guid departmentID)
        //{
        //    var result = TaskCategoryService.Get(i => i.)
        //}

        public StoreResult GetSummaryNearly(int role)
        {
            var summary = TaskSummaryService.GetSummaryNearly(role);
            return new StoreResult(summary);
        }

        public StoreResult GetDataPie(Resource.StatisRole role)
        {

            var summary = TaskSummaryService.CurrentSummary((int)role);
            return new StoreResult(summary);

        }

        public class ChartStatisticalBO
        {
            public string Name
            {
                get;
                set;
            }

            public double Data1
            {
                get;
                set;
            }
        }
        public static List<ChartStatisticalBO> GenerateData(int n, int floor)
        {
            List<ChartStatisticalBO> data = new List<ChartStatisticalBO>(n);
            Random random = new Random();
            double p = (random.NextDouble() * 11) + 1;
            string name = "";
            for (int i = 0; i < n; i++)
            {
                if (i == 0)
                    name = "Chờ thực hiện";
                else if (i == 1)
                    name = "Tạm dừng";
                else if (i == 2)
                    name = "Quá hạn";
                else if (i == 3)
                    name = "Thực hiện";
                else if (i == 4)
                    name = "Hủy";
                data.Add(new ChartStatisticalBO
                {
                    Name = name + " (" + Math.Floor(Math.Max(random.NextDouble() * 100, floor)) + "%)",
                    Data1 = Math.Floor(Math.Max(random.NextDouble() * 100, floor))
                });
            }
            return data;
        }
        public static List<ChartStatisticalBO> GenerateData(int n)
        {
            return GenerateData(n, 20);
        }

        public ActionResult GridStatistical()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }
        public ActionResult TaskStatistical()
        {
            var currentUser = EmployeeService.GetCurrentUser();
            return new Ext.Net.MVC.PartialViewResult() { Model = currentUser };
        }
        public ActionResult ChangeChart(string containerId, string chartName)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                ClearContainer = true,
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                WrapByScriptTag = false,
                ViewName = chartName
            };
        }

        public StoreResult GetDataTaskStatusAnalytic(string departmentId = default(string))
        {

            var summary = TaskSummaryService.TaskStatusAnalytic(iDAS.Service.Common.Utilities.ConvertToGuid(departmentId));
            return new StoreResult(summary);
        }
        public StoreResult GetDataTaskTypeAnalytic(string departmentId = default(string))
        {

            var summary = TaskSummaryService.TaskTypeAnalytic(iDAS.Service.Common.Utilities.ConvertToGuid(departmentId));
            return new StoreResult(summary);
        }
    }
}