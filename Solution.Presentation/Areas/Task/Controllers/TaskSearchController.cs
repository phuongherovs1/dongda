﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using iDAS.Service.API.Task;
using iDAS.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class TaskSearchController : FrontController
    {
        protected ITaskSearchService taskSearchService { set; get; }
        public TaskSearchController(ITaskSearchService _taskSearchService)
        {
            taskSearchService = _taskSearchService;
        }

        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult TaskIndex(string containerId)
        {
            return PartialView();
        }


        public ActionResult GetDataTask(StoreRequestParameters parameter, DateTime dateFrom, DateTime dateTo,
                                    string textSearch = default(string), string employeeInfo = default(string), int? status = null)
        {
            DateTime timeMin = DateTime.Parse("01/01/1945");
            DateTime timeMax = DateTime.MaxValue;
            if (dateFrom < timeMin)
                dateFrom = timeMin;
            if (dateTo > timeMax)
                dateTo = timeMax;

            Guid? employeeId = null;
            Guid? titleId = null;
            Guid? departmentId = null;

            if (!string.IsNullOrEmpty(employeeInfo))
            {
                var arr = employeeInfo.Split('_');
                if (arr.Length > 1)
                {
                    if (arr[0].ToLower().Equals("employee"))
                        employeeId = Guid.Parse(arr[1]);
                    else if (arr[0].ToLower().ToLower().Equals("title"))
                        titleId = Guid.Parse(arr[1]);
                    else if (arr[0].ToLower().ToLower().Equals("department"))
                        departmentId = Guid.Parse(arr[1]);
                }
            }

            var pageIndex = parameter.Page;
            var pageSize = parameter.Limit;
            var count = 0;
            DataTable dt = taskSearchService.GetData(textSearch, dateFrom, dateTo, employeeId, titleId, departmentId, status, pageIndex, pageSize, out count);
            return this.Store(dt, count);
        }

        /// <summary>
        /// Thống kê công việc
        /// </summary>
        /// <param name="timeIndex">thời gian theo tháng, quý, năm</param>
        /// <param name="year">năm</param>
        /// <param name="objectStatistic">Đối tượng: phòng ban, cán bộ</param>
        /// <returns></returns>
        public ActionResult GetTaskStatistic(int timeIndex = 12, int year = 0, string objectStatistic = default(string), string strTaskCategoryID = default(string))
        {
            Session["TaskStatisticDetail"] = null;

            if (year == 0)
                year = DateTime.Today.Year;

            if (string.IsNullOrEmpty(objectStatistic))
                return new StoreResult(new List<Summary1BO>());

            try
            {

                DateTime fromDate = DateTime.Today;
                DateTime toDate = DateTime.Today;
                string months = string.Empty;
                Guid? dataId = null;
                Guid? categoryId = null;
                if (!string.IsNullOrEmpty(strTaskCategoryID) && !strTaskCategoryID.Equals("0"))
                    categoryId = Guid.Parse(strTaskCategoryID);
                //'D': Department,  'E': Employee,  'TC':  Task Category
                string statisticType = string.Empty;

                getDate(timeIndex, year, ref fromDate, ref toDate, ref months);
                // nếu chọn nhóm công việc thì lấy theo nhóm công việc
                if (categoryId != null)
                {
                    statisticType = "TC";
                    dataId = categoryId;
                }
                else if (!string.IsNullOrEmpty(objectStatistic))
                {
                    var arr = objectStatistic.Split('_');
                    if (arr.Length > 1)
                    {
                        dataId = Guid.Parse(arr[1]);

                        if (arr[0].ToLower().Equals("employee"))
                            statisticType = "E";
                        //else if (arr[0].ToLower().ToLower().Equals("title"))
                        //dataId = Guid.Parse(arr[1]);
                        else if (arr[0].ToLower().ToLower().Equals("department"))
                            statisticType = "D";
                    }
                }

                List<Summary1BO> lstSum = new List<Summary1BO>();
                DataTable dt = taskSearchService.GetTaskStatistic(fromDate, toDate, year.ToString(), months, dataId, statisticType);

                if (dt == null || dt.Rows.Count == 0)
                    return new StoreResult(lstSum);

                for (int rowIndex = 0; rowIndex < dt.Rows.Count; rowIndex++)
                {
                    int month = int.Parse(dt.Rows[rowIndex]["Month"].ToString());
                    int index = lstSum.FindIndex(i => i.MonthSummary.Month == month);
                    if (index >= 0)
                    {
                        Summary1BO summary = lstSum[index];

                        //lstSum[index].MonthSummary = Utilities.GetFirstDayOfMonth(month, year);
                        lstSum[index].UnPerForm += getTaskCount(dt.Rows[rowIndex], "CTH");
                        lstSum[index].PerForm1 += getTaskCount(dt.Rows[rowIndex], "DTHTH");
                        lstSum[index].PerForm2 += getTaskCount(dt.Rows[rowIndex], "DTHQH");
                        lstSum[index].Finish1 += getTaskCount(dt.Rows[rowIndex], "KTDH");
                        lstSum[index].Finish2 += getTaskCount(dt.Rows[rowIndex], "KTQH");
                        lstSum[index].Cancel += getTaskCount(dt.Rows[rowIndex], "HUY");

                    }
                    else
                    {
                        Summary1BO summary = new Summary1BO()
                        {
                            MonthSummary = Utilities.GetFirstDayOfMonth(month, year),
                            UnPerForm = getTaskCount(dt.Rows[rowIndex], "CTH"),
                            PerForm1 = getTaskCount(dt.Rows[rowIndex], "DTHTH"),
                            PerForm2 = getTaskCount(dt.Rows[rowIndex], "DTHQH"),
                            Finish1 = getTaskCount(dt.Rows[rowIndex], "KTDH"),
                            Finish2 = getTaskCount(dt.Rows[rowIndex], "KTQH"),
                            Cancel = getTaskCount(dt.Rows[rowIndex], "HUY"),
                        };
                        lstSum.Add(summary);
                    }
                }
                Session["TaskStatisticDetail"] = lstSum;
                return new StoreResult(lstSum);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetTaskStatisticDetail()
        {
            var data = Session["TaskStatisticDetail"];
            return new StoreResult(data); ;
        }

        /// <summary>
        /// Thống kê công việc
        /// </summary>
        /// <param name="timeIndex">thời gian theo tháng, quý, năm</param>
        /// <param name="year">năm</param>
        /// <param name="objectStatistic">Đối tượng: phòng ban, cán bộ</param>
        /// <returns></returns>
        public ActionResult GetTaskStatistic_PieChart(int timeIndex = 12, int year = 0, string objectStatistic = default(string))
        {
            if (year == 0)
                year = DateTime.Today.Year;

            if (string.IsNullOrEmpty(objectStatistic))
                return new StoreResult(new List<TaskPieChartBO>());

            try
            {

                DateTime fromDate = DateTime.Today;
                DateTime toDate = DateTime.Today;
                string months = string.Empty;
                Guid? dataId = null;
                //'D': Department,  'E': Employee,  'TC':  Task Category
                string statisticType = string.Empty;

                getDate(timeIndex, year, ref fromDate, ref toDate, ref months);

                if (!string.IsNullOrEmpty(objectStatistic))
                {
                    var arr = objectStatistic.Split('_');
                    if (arr.Length > 1)
                    {
                        dataId = Guid.Parse(arr[1]);

                        if (arr[0].ToLower().Equals("employee"))
                            statisticType = "E";
                        //else if (arr[0].ToLower().ToLower().Equals("title"))
                        //dataId = Guid.Parse(arr[1]);
                        else if (arr[0].ToLower().ToLower().Equals("department"))
                            statisticType = "D";
                    }
                }

                List<TaskPieChartBO> lstSum = new List<TaskPieChartBO>();
                DataTable dt = taskSearchService.GetTaskStatistic(fromDate, toDate, year.ToString(), months, dataId, statisticType);

                if (dt == null || dt.Rows.Count == 0)
                    return new StoreResult(lstSum);

                int UnPerForm = 0;
                int PerForm1 = 0;
                int PerForm2 = 0;
                int Finish1 = 0;
                int Finish2 = 0;
                int Cancel = 0;

                for (int rowIndex = 0; rowIndex < dt.Rows.Count; rowIndex++)
                {
                    UnPerForm += getTaskCount(dt.Rows[rowIndex], "CTH");
                    PerForm1 += getTaskCount(dt.Rows[rowIndex], "DTHTH");
                    PerForm2 += getTaskCount(dt.Rows[rowIndex], "DTHQH");
                    Finish1 += getTaskCount(dt.Rows[rowIndex], "KTDH");
                    Finish2 += getTaskCount(dt.Rows[rowIndex], "KTQH");
                    Cancel += getTaskCount(dt.Rows[rowIndex], "HUY");
                }
                lstSum.Add(new TaskPieChartBO() { Name = "Chờ thực hiện", Value = UnPerForm });
                lstSum.Add(new TaskPieChartBO() { Name = "Đang thực hiện (trong hạn)", Value = PerForm1 });
                lstSum.Add(new TaskPieChartBO() { Name = "Đang thực hiện (quá hạn)", Value = PerForm2 });
                lstSum.Add(new TaskPieChartBO() { Name = "Kết thúc (đúng hạn)", Value = Finish1 });
                lstSum.Add(new TaskPieChartBO() { Name = "Kết thúc (quá hạn)", Value = Finish2 });
                lstSum.Add(new TaskPieChartBO() { Name = "Hủy", Value = Cancel });

                return new StoreResult(lstSum);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int getTaskCount(DataRow r, string stattisticStatusCode)
        {
            int result = 0;
            if (r["StatisticStatusCode"].ToString().Equals(stattisticStatusCode))
                result = int.Parse(r["TaskCount"].ToString());

            return result;
        }

        private void getDate(int timeIndex, int year, ref DateTime fromDate, ref DateTime toDate, ref string months)
        {
            if (timeIndex == 0)
            {
                fromDate = DateTime.ParseExact("01/01/" + year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                toDate = DateTime.ParseExact("31/12/" + year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                months = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12";
            }
            else if (timeIndex < 13)
            {
                fromDate = Utilities.GetFirstDayOfMonth(timeIndex, year);
                toDate = fromDate.AddMonths(1).AddDays(-1);
                months = "" + timeIndex.ToString() + "";
            }
            else if (timeIndex == 13) // quý 1
            {
                fromDate = DateTime.ParseExact("01/01/" + year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                toDate = DateTime.ParseExact("31/03/" + year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                months = "1, 2, 3";
            }
            else if (timeIndex == 14) // quý 2
            {
                fromDate = DateTime.ParseExact("01/04/" + year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                toDate = DateTime.ParseExact("30/06/" + year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                months = "4, 5, 6";
            }
            else if (timeIndex == 15) // quý 3
            {
                fromDate = DateTime.ParseExact("01/07/" + year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                toDate = DateTime.ParseExact("30/09/" + year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                months = "7, 8, 9";
            }
            else if (timeIndex == 16) // quý 4
            {
                fromDate = DateTime.ParseExact("01/10/" + year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                toDate = DateTime.ParseExact("31/12/" + year, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                months = "10, 11, 12";
            }
        }
    }
}