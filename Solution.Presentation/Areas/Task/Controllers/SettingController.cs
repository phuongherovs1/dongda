﻿using Ext.Net;
using iDAS.Presentation.Controllers;

namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class SettingController : FrontController
    {
        //
        // GET: /Task/Setting/
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                WrapByScriptTag = false
            };
        }
    }
}