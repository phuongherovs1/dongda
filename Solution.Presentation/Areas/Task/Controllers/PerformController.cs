﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Data;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class PerformController : FrontController
    {
        public ActionResult Index(Guid taskId)
        {
            ViewData["TaskID"] = taskId;
            ViewData["AllowCreate"] = TaskPerformService.CheckIsCreate(taskId);
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        public ActionResult LoadData(Guid taskId, StoreRequestParameters parameter)
        {
            try
            {
                var count = 0;
                var data = TaskPerformService.GetAll(taskId, parameter.Page, parameter.Limit, out count);
                return this.Store(data, count);
            }
            catch
            {
                return this.Store(null);
            }
        }
        public ActionResult LoadTask(Guid? employeeId, DateTime? startAt, DateTime? endAt, StoreRequestParameters parameter)
        {
            try
            {
                var count = 0;
                var data = TaskPerformService.GetListPerformByEmployeeAndTime(employeeId, startAt, endAt, parameter.Page, parameter.Limit, out count);
                return this.Store(data, count);
            }
            catch(Exception ex)
            {
                return this.Store(null);
            }
        }
        public ActionResult Form(Guid? id, Guid? taskId)
        {
            if (taskId == null)
            {
                var Task = TaskResourceService.GetById((Guid)id);
                ViewData["TaskID"] = Task.TaskId;
                var model = TaskPerformService.GetFormItem(id, Task.TaskId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                ViewData["TaskID"] = taskId;
                var model = TaskPerformService.GetFormItem(id, taskId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
        }
        [HttpPost]
        public ActionResult Create(TaskPerformBO item)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    Guid check = TaskPerformService.Create(item);
                    if (check == Guid.Empty) message = "Người dùng đã được thêm cho công việc này!";
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult Update(TaskPerformBO item)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    TaskPerformService.Update(item);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    TaskPerformService.Delete(id);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult Send(TaskPerformBO item)
        {
            var message = default(string);
            try
            {
                if (TaskPerformService.Send(item) == true)
                    Notify(TitleResourceNotifies.AssignTask, item.Content, item.Employee.Id, UrlResourceNotifies.AssignTask, new { id = item.Id, taskId = item.TaskId });
                else
                    message = "Người dùng đã được thêm cho công việc này!";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult Report(Guid id, Guid taskId)
        {
            var model = TaskPerformService.GetById(id);
            var task = TaskService.GetById(taskId);
            model.TaskId = taskId;
            model.TaskName = task.Name;
            model.TaskContent = task == null ? string.Empty : task.Content;
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        [HttpPost]
        public ActionResult Report1(TaskPerformBO item)
        {
            var message = default(string);
            try
            {
                TaskPerformService.Report(item);
                Notify(TitleResourceNotifies.ReportTask + " \""+item.TaskName+"\"", item.Report, item.CreateBy.HasValue ? item.CreateBy.Value : Guid.Empty, UrlResourceNotifies.CommunicationTask, new { taskResourceId = item.Id, taskId = item.TaskId });

                DataTable dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_TaskResource_GetListEmployeeNotPerform",
                    parameter: new
                    {
                        taskid = item.TaskId.HasValue? item.TaskId.Value.ToString() : "",
                        performid = item.CreateBy.HasValue ? item.CreateBy.Value.ToString() : ""
                    }
                );
                if(dataTable != null && dataTable.Rows.Count > 0)
                {
                    foreach(DataRow itemresource in dataTable.Rows)
                    {
                        Notify(TitleResourceNotifies.ReportTask + " \"" + item.TaskName + "\"", item.Report, itemresource["EmployeeId"] != null ? new Guid(itemresource["EmployeeId"].ToString()) : Guid.Empty, UrlResourceNotifies.CommunicationTask, new { taskResourceId = item.Id, taskId = item.TaskId });
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult Check(Guid id)
        {
            var obj = TaskPerformService.GetCheckItem(id);
            return new Ext.Net.MVC.PartialViewResult { Model = obj };
        }
        [HttpPost]
        public ActionResult Check(TaskPerformBO item)
        {
            var message = default(string);
            try
            {
                TaskPerformService.Check(item);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult Cancel(Guid id)
        {
            var message = default(string);
            try
            {
                var model = TaskPerformService.GetCancelItem(id);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Cancel(TaskPerformBO item)
        {
            var message = default(string);
            try
            {
                TaskPerformService.Cancel(item);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult Pause(Guid id)
        {
            var message = default(string);
            try
            {
                var model = TaskPerformService.GetPauseItem(id);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Pause(TaskPerformBO item)
        {
            var message = default(string);
            try
            {
                TaskPerformService.Pause(item);
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return this.DirectFormat(message);
        }
        public ActionResult Task(Guid? employeeId, DateTime? startAt, DateTime? endAt)
        {
            ViewData["EmployeeID"] = employeeId;
            ViewData["StartAt"] = startAt;
            ViewData["EndAt"] = endAt;
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
    }
}