﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Task.Controllers
{
    public class ScoreController : FrontController
    {
        public ActionResult Index(Guid taskId)
        {
            ViewData["TaskID"] = taskId;
            ViewData["AllowCreate"] = TaskPerformService.CheckIsCreate(taskId);
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        public ActionResult LoadData(Guid taskId, StoreRequestParameters parameter)
        {
            try
            {
                var count = 0;
                var data = TaskPerformService.GetAll(taskId, parameter.Page, parameter.Limit, out count);
                return this.Store(data, count);
            }
            catch
            {
                return this.Store(null);
            }
        }
        public ActionResult LoadTask(Guid? employeeId, DateTime? startAt, DateTime? endAt, StoreRequestParameters parameter)
        {
            try
            {
                var count = 0;
                var data = TaskPerformService.GetListPerformByEmployeeAndTime(employeeId, startAt, endAt, parameter.Page, parameter.Limit, out count);
                return this.Store(data, count);
            }
            catch
            {
                return this.Store(null);
            }
        }
        public ActionResult Form(Guid? id, Guid? taskId)
        {
            if (taskId == null)
            {
                var Task = TaskResourceService.GetById((Guid)id);
                ViewData["TaskID"] = Task.TaskId;
                var model = TaskPerformService.GetFormItem(id, Task.TaskId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                ViewData["TaskID"] = taskId;
                var model = TaskPerformService.GetFormItem(id, taskId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
        }
        [HttpPost]
        public ActionResult Create(TaskPerformBO item)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    Guid check = TaskPerformService.Create(item);
                    if (check == Guid.Empty) message = "Người dùng đã được thêm cho công việc này!";
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult Update(TaskPerformBO item)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    TaskPerformService.Update(item);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var message = default(string);
            if (ModelState.IsValid)
            {
                try
                {
                    TaskPerformService.Delete(id);
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            }
            return this.DirectFormat(message);
        }
    }
}