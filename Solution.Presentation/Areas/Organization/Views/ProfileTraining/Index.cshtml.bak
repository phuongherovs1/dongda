﻿@using Ext.Net;
@using Ext.Net.MVC;
@model iDAS.Service.VPHumanProfileTrainingBO
@{
    var X = Html.X();
    Layout = "~/Views/Shared/_Layout.cshtml";
    var employeeId = ViewData["EmployeeId"];
    var employeeName = (string)ViewData["EmployeeName"];
}

@(X.Viewport().Layout(LayoutType.Fit).Border(false).Margin(0).Frame(false)
    .Items(
        X.FormPanel()
        .Layout(LayoutType.Fit)
        .Frame(true)
        .Border(false)
        .StyleSpec("border: none;").Padding(0)
        .DefaultAnchor("100%")
        .Items(
            X.GridPanel()
                .Header(false)
                .ID("GrProfileTraining")
                .ColumnLines(true)
                .Border(false)
                .ForceFit(true)
                .Icon(Icon.TextListBullets)
                .TopBar(
                    X.Toolbar()
                    .Layout(LayoutType.VBox)
                    .Items(
                        X.ContainerHBox().Cls("topbarItems").Items(
                            X.Hidden().ID("hdfEmployeeID").Value(employeeId),
                            X.Button().ID("btnCreate").Text("Thêm mới").IconCls("x-fa fa-plus-circle").Handler("AddNew();"),
                            X.Button().ID("btnUpdate").Text("Chi tiết").IconCls("x-fa fa-info-circle").Handler("UpdateItem();").Disabled(true),
                            X.Button().ID("btnDelete").Text("Xóa").IconCls("x-fa fa-times-circle").Handler("onDelete(App.GrProfileTraining.selModel.getSelection()[0].get('Id'));").Disabled(true),
                            X.Label().Width(10),
                            X.Label().Text("Quản lý dữ liệu đào tạo"),
                            X.Label().Width(10),
                            X.ComboBox().ID("cbMasterEducation").Width(300).Editable(false).EmptyText("--Chọn loại dữ liệu--")
                            .Items(
                                new ListItem("Hình thức đào tạo", "educationType"),
                                new ListItem("Kết quả đào tạo", "educationResult")
                            ),
                            X.Label().Width(10),
                            X.Button().ID("btnMasterEducation").IconCls("x-fa fa-cogs").Text("Quản lý").Handler("manageMasterDataEducation()")
                        ),
                        X.ToolbarSpacer().Height(20),
                        X.Label().Cls("topbarItems topbarName").Text("Nhân viên: " + employeeName)
                    )
            )
            .Store(
                X.Store()
                    .ID("StoreProfileTraining")
                    .RemotePaging(true)
                    .PageSize(20)
                    .Proxy(X.AjaxProxy().Url(Url.Action("LoadProfileTraining")).Reader(X.JsonReader().RootProperty("data")).IDParam("Id"))
                    .Parameters(ps =>
                            {
                                ps.Add(new StoreParameter("EmployeeID", "App.hdfEmployeeID.value", ParameterMode.Raw));
                            })
                .Model(
                    X.Model().Fields(
                        X.ModelField().Name("ID"),
                        X.ModelField().Name("Name"),
                        X.ModelField().Name("StartDate"),
                        X.ModelField().Name("EndDate"),
                        X.ModelField().Name("Form"),
                        X.ModelField().Name("Content"),
                        X.ModelField().Name("Certificate"),
                        X.ModelField().Name("Result"),
                        X.ModelField().Name("Reviews"),
                        X.ModelField().Name("FileAttachs")
                    )
                )
            )
            .ColumnModel(
                X.RowNumbererColumnFormat().DataIndex("ID").Text("STT").Width(50).StyleSpec("font-weight: bold;text-align: center"),
                X.Column().DataIndex("Name").Text("Tên khóa học").Flex(1).StyleSpec("font-weight: bold;text-align: center"),
                X.Column().DataIndex("Form").Text("Hình thức đào tạo").Width(140).StyleSpec("font-weight: bold;text-align: center"),
                X.Column().DataIndex("Content").Text("Nội dung đào tạo").Width(140).StyleSpec("font-weight: bold;text-align: center"),
                X.Column().DataIndex("Certificate").Text("Chứng chỉ đào tạo").Width(140).StyleSpec("font-weight: bold;text-align: center"),
                X.Column().DataIndex("Result").Text("Kết quả đào tạo").Width(140).StyleSpec("font-weight: bold;text-align: center"),
                X.ColumnFileFormat(m => m.FileAttachs.Files).Text("<b>Tệp đính kèm</b>").Width(130)
            )
            .SelectionModel(X.CheckboxSelectionModel().ShowHeaderCheckbox(false).Mode(SelectionMode.Single)
                .Listeners(ls => ls.SelectionChange.Fn = "CheckStatus"))
            .Listeners(
                ls =>
                {
                    ls.CellDblClick.Handler = "Ext.getBody().mask('Đang thực hiện....');App.btnUpdate.click()";
                }
            )
            .BottomBar(X.PagingToolbarFormat())
            )
        )
)

<script>
    var deleteUrl = '@(Url.Action("DeleteProfileTraining"))';
    function CheckStatus() {
        var records = App.GrProfileTraining.selModel.getSelection();
        if (records.length > 0) {
            App.btnUpdate.setDisabled(false);
            App.btnDelete.setDisabled(false);
        }
        else {
            App.btnUpdate.setDisabled(true);
            App.btnDelete.setDisabled(true);
        };
    };
    var AddNew = function () {
        var url = '@(Url.Action("UpdateForm", "ProfileTraining", new { Area = "Organization" }, null))';
        var params = {
            EmployeeID: App.hdfEmployeeID.value
        };
        onDirectMethod(url, params);
    };
    var UpdateItem = function () {
        var records = App.GrProfileTraining.selModel.getSelection();
        if (records.length > 0) {
            var url = '@(Url.Action("UpdateForm", "ProfileTraining", new { Area = "Organization" }, null))';
            var params = {
                id: records[0].get('Id'),
                EmployeeID: App.hdfEmployeeID.value
            };
            onDirectMethod(url, params);
        }
        else {
            MessageBox();
        }
    };

    var manageMasterDataEducation = function () {
	    var value = App.cbMasterEducation.getValue();
	    var url = '@Url.Action("ManageMasterDataForm", "ProfileMasterData", new { area = "Organization" })';
	    Ext.net.DirectMethod.request({
		    url: url,
		    params: { type: value }
	    });
    };
</script>

<style>
    .topbarItems {
        left: 0px !important;
    }

    .topbarName {
        font-weight: bold;
        font-size: 16px;
    }
</style>