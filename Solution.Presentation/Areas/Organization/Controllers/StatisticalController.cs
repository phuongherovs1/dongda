﻿using Ext.Net.MVC;
using iDAS.Presentation.Controllers;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class StatisticalController : FrontController
    {
        //
        // GET: /Organization/Statistical/
        public StoreResult GetHumanSexAnalytic()
        {

            var summary = OrganizationSummaryService.HumanSexAnalytic();
            return new StoreResult(summary);

        }
        public StoreResult GetHumanAnalytic()
        {

            var summary = OrganizationSummaryService.HumanAnalyticByDepartment();
            return new StoreResult(summary);

        }
    }
}