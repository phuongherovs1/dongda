﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using systemIO = System.IO;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class DepartmentTitleController : FrontController
    {
        //
        // GET: /Organization/Title/
        #region View
        public ActionResult Index(string departmentId = default(string))
        {
            try
            {
                ViewData["DepartmentID"] = departmentId;
                return new Ext.Net.MVC.PartialViewResult() { ViewData = ViewData };
            }
            catch
            {
            }
            return this.Direct();
        }
        public ActionResult ResponsibilityIndex(string titleid = default(string))
        {
            ViewData["titleid"] = titleid;
            ViewData["IsAdmin"] = Common.Role.IsAdmin();
            return PartialView();
        }
        public ActionResult Form(string id = default(string), string departmentId = default(string))
        {
            object model = null;
            if (id == default(string))
            {
                UserRoleService roleService = new UserRoleService();
                var isAdmin = roleService.RoleExits("Administrator");
                DepartmentTitleRoleService s = new DepartmentTitleRoleService();
                var titleRole = s.CheckCreateForTitles(new Guid(departmentId));
                if (!isAdmin && !titleRole)
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = "Bạn không có quyền thực hiện chức năng này"
                    });
                    return this.Direct();
                }
                ViewData.Add("Operation", Common.Operation.Create);
                model = new TitleBO() { DepartmentId = new Guid(departmentId) };
            }
            else
            {
                UserRoleService roleService = new UserRoleService();
                var isAdmin = roleService.RoleExits("Administrator");
                DepartmentTitleRoleService s = new DepartmentTitleRoleService();
                var titleRole = s.CheckRoleForTitles_View(new Guid(id));
                if (isAdmin || titleRole)
                {
                    ViewData.Add("Operation", Common.Operation.Update);
                    model = DepartmentTitleService.GetById(new Guid(id.ToString()));
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = "Bạn không có quyền truy cập chức năng này!"
                    });
                    return this.Direct();
                }
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult FormView(string id = default(string), string departmentId = default(string))
        {
            var fullpath = AppDomain.CurrentDomain.BaseDirectory + "\\Temp\\Template_InfoTitle.txt";
            //var FullPathImage = AppDomain.CurrentDomain.BaseDirectory + "Content\\images\\Logo.png";
            var FullPathImage = HttpContext.Request.UrlReferrer.OriginalString + "/Content/images/Logo.png";
            string TemplateHTML = systemIO.File.ReadAllText(fullpath);
            TitleBO model = new TitleBO();
            DepartmentBO department = new DepartmentBO();
            EmployeeBO employee = new EmployeeBO();
            EmployeeBO employeeTo = new EmployeeBO();
            model = DepartmentTitleService.GetById(new Guid(id.ToString()));
            if (model.DepartmentId.HasValue)
                department = DepartmentService.GetById(model.DepartmentId.Value);
            if (model.ReportToId.HasValue)
                employee = EmployeeService.GetById(model.ReportToId.Value);
            if (model.AbsenreplaceId.HasValue)
                employeeTo = EmployeeService.GetById(model.AbsenreplaceId.Value);
            DataTable data = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_ResponsibilityTitles_GetData", parameter: new { titleid = model.Id.ToString() });

            TemplateHTML = TemplateHTML.Replace("@FullPathImage@", FullPathImage);
            #region get CompanyName From config
            var deparmentname = ConfigTableService.GetByConfigKey("CompanyName");
            var NameCompany = string.IsNullOrEmpty(deparmentname) ? "Vinh Khoa Company" : deparmentname;
            #endregion
            TemplateHTML = TemplateHTML.Replace("@CompanyName@", NameCompany);
            if (!string.IsNullOrEmpty(model.Name))
                TemplateHTML = TemplateHTML.Replace("@NameTitle@", model.Name);
            else
                TemplateHTML = TemplateHTML.Replace("@NameTitle@", "");
            if (!string.IsNullOrEmpty(model.Name))
                TemplateHTML = TemplateHTML.Replace("@CodeTitle@", employee.Code);
            else
                TemplateHTML = TemplateHTML.Replace("@CodeTitle@", "");
            if (!string.IsNullOrEmpty(department.Name))
                TemplateHTML = TemplateHTML.Replace("@DeparmentTitle@", department.Name);
            else
                TemplateHTML = TemplateHTML.Replace("@DeparmentTitle@", "");
            if (!string.IsNullOrEmpty(model.ReportTo))
                TemplateHTML = TemplateHTML.Replace("@DeparmentManager@", model.ReportTo);
            else
                TemplateHTML = TemplateHTML.Replace("@DeparmentManager@", "");

            if (!string.IsNullOrEmpty(model.Absenreplace))
                TemplateHTML = TemplateHTML.Replace("@Specialize@", model.Absenreplace);
            else
                TemplateHTML = TemplateHTML.Replace("@Specialize@", "");
            if (!string.IsNullOrEmpty(model.WorkingConditions))
                TemplateHTML = TemplateHTML.Replace("@Accusative@", model.WorkingConditions);
            else
                TemplateHTML = TemplateHTML.Replace("@Accusative@", "");
            if (data != null && data.Rows.Count > 0)
            {
                string tableHTML = "";
                foreach (DataRow item in data.Rows)
                {
                    tableHTML += "<tr style=\"border: 1px solid black;\">";
                    tableHTML += "<td style=\"border: 1px solid black;padding: 5px;\">" + (item["Name"] != null ? item["Name"].ToString() : "") + "</td>";
                    tableHTML += "<td style=\"border: 1px solid black;padding: 5px;\">" + (item["Mission"] != null ? item["Mission"].ToString() : "") + "</td>";
                    tableHTML += "<td style=\"border: 1px solid black;padding: 5px;\">" + (item["Request"] != null ? item["Request"].ToString() : "") + "</td>";
                    tableHTML += "<td style=\"border: 1px solid black;padding: 5px;\">" + (item["Criteria"] != null ? item["Criteria"].ToString() : "") + "</td>";
                    tableHTML += "</tr>";
                }
                TemplateHTML = TemplateHTML.Replace("@TableFormResponsibilityTitle@", tableHTML);
            }
            else
            {
                TemplateHTML = TemplateHTML.Replace("@TableFormResponsibilityTitle@", "");
            }
            if (!string.IsNullOrEmpty(model.Purpose))
                TemplateHTML = TemplateHTML.Replace("@Condition@", model.Purpose);
            else
                TemplateHTML = TemplateHTML.Replace("@Condition@", "");
            if (!string.IsNullOrEmpty(model.Power))
                TemplateHTML = TemplateHTML.Replace("@Power@", model.Power);
            else
                TemplateHTML = TemplateHTML.Replace("@Power@", "");
            if (!string.IsNullOrEmpty(model.Capacityrequire))
                TemplateHTML = TemplateHTML.Replace("@Capacityrequire@", model.Capacityrequire);
            else
                TemplateHTML = TemplateHTML.Replace("@Capacityrequire@", "");
            if (!string.IsNullOrEmpty(model.RelationshipIn))
                TemplateHTML = TemplateHTML.Replace("@RelationshipIn@", model.RelationshipIn);
            else
                TemplateHTML = TemplateHTML.Replace("@RelationshipIn@", "");
            if (!string.IsNullOrEmpty(model.RelationshipOut))
                TemplateHTML = TemplateHTML.Replace("@RelationshipOut@", model.RelationshipOut);
            else
                TemplateHTML = TemplateHTML.Replace("@RelationshipOut@", "");
            ViewData.Add("ContentHTML", TemplateHTML);
            ViewData.Add("Id", id);

            //using (StreamWriter file = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Temp\\Log.txt", true))
            //{
            //    file.WriteLine(TemplateHTML);
            //}
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult ExportInfoTitles(string id = default(string))
        {
            var fullpath = AppDomain.CurrentDomain.BaseDirectory + "\\Temp\\Template_InfoTitle.txt";
            //var FullPathImage = AppDomain.CurrentDomain.BaseDirectory + "Content\\images\\Logo.png";
            var FullPathImage = HttpContext.Request.UrlReferrer.OriginalString+ "/Content/images/Logo.png";
            string TemplateHTML = systemIO.File.ReadAllText(fullpath);
            TitleBO model = new TitleBO();
            DepartmentBO department = new DepartmentBO();
            EmployeeBO employee = new EmployeeBO();
            EmployeeBO employeeTo = new EmployeeBO();
            model = DepartmentTitleService.GetById(new Guid(id.ToString()));
            if (model.DepartmentId.HasValue)
                department = DepartmentService.GetById(model.DepartmentId.Value);
            if (model.ReportToId.HasValue)
                employee = EmployeeService.GetById(model.ReportToId.Value);
            if (model.AbsenreplaceId.HasValue)
                employeeTo = EmployeeService.GetById(model.AbsenreplaceId.Value);
            DataTable data = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_ResponsibilityTitles_GetData", parameter: new { titleid = model.Id.ToString() });

            TemplateHTML = TemplateHTML.Replace("@FullPathImage@", FullPathImage);
            #region get CompanyName From config
            var deparmentname = ConfigTableService.GetByConfigKey("CompanyName");
            var NameCompany = string.IsNullOrEmpty(deparmentname) ? "Vinh Khoa Company" : deparmentname;
            #endregion
            TemplateHTML = TemplateHTML.Replace("@CompanyName@", NameCompany);
            if (!string.IsNullOrEmpty(model.Name))
                TemplateHTML = TemplateHTML.Replace("@NameTitle@", model.Name);
            else
                TemplateHTML = TemplateHTML.Replace("@NameTitle@", "");
            if (!string.IsNullOrEmpty(model.Name))
                TemplateHTML = TemplateHTML.Replace("@CodeTitle@", employee.Code);
            else
                TemplateHTML = TemplateHTML.Replace("@CodeTitle@", "");
            if (!string.IsNullOrEmpty(department.Name))
                TemplateHTML = TemplateHTML.Replace("@DeparmentTitle@", department.Name);
            else
                TemplateHTML = TemplateHTML.Replace("@DeparmentTitle@", "");
            if (!string.IsNullOrEmpty(model.ReportTo))
                TemplateHTML = TemplateHTML.Replace("@DeparmentManager@", model.ReportTo);
            else
                TemplateHTML = TemplateHTML.Replace("@DeparmentManager@", "");

            if (!string.IsNullOrEmpty(model.Absenreplace))
                TemplateHTML = TemplateHTML.Replace("@Specialize@", model.Absenreplace);
            else
                TemplateHTML = TemplateHTML.Replace("@Specialize@", "");
            if (!string.IsNullOrEmpty(model.WorkingConditions))
                TemplateHTML = TemplateHTML.Replace("@Accusative@", model.WorkingConditions);
            else
                TemplateHTML = TemplateHTML.Replace("@Accusative@", "");
            if (data != null && data.Rows.Count > 0)
            {
                string tableHTML = "";
                foreach (DataRow item in data.Rows)
                {
                    tableHTML += "<tr style=\"border: 1px solid black;\">";
                    tableHTML += "<td style=\"border: 1px solid black;padding: 5px;\">" + (item["Name"] != null ? item["Name"].ToString() : "") + "</td>";
                    tableHTML += "<td style=\"border: 1px solid black;padding: 5px;\">" + (item["Mission"] != null ? item["Mission"].ToString() : "") + "</td>";
                    tableHTML += "<td style=\"border: 1px solid black;padding: 5px;\">" + (item["Request"] != null ? item["Request"].ToString() : "") + "</td>";
                    tableHTML += "<td style=\"border: 1px solid black;padding: 5px;\">" + (item["Criteria"] != null ? item["Criteria"].ToString() : "") + "</td>";
                    tableHTML += "</tr>";
                }
                TemplateHTML = TemplateHTML.Replace("@TableFormResponsibilityTitle@", tableHTML);
            }
            else
            {
                TemplateHTML = TemplateHTML.Replace("@TableFormResponsibilityTitle@", "");
            }
            if (!string.IsNullOrEmpty(model.Purpose))
                TemplateHTML = TemplateHTML.Replace("@Condition@", model.Purpose);
            else
                TemplateHTML = TemplateHTML.Replace("@Condition@", "");
            if (!string.IsNullOrEmpty(model.Power))
                TemplateHTML = TemplateHTML.Replace("@Power@", model.Power);
            else
                TemplateHTML = TemplateHTML.Replace("@Power@", "");
            if (!string.IsNullOrEmpty(model.Capacityrequire))
                TemplateHTML = TemplateHTML.Replace("@Capacityrequire@", model.Capacityrequire);
            else
                TemplateHTML = TemplateHTML.Replace("@Capacityrequire@", "");
            if (!string.IsNullOrEmpty(model.RelationshipIn))
                TemplateHTML = TemplateHTML.Replace("@RelationshipIn@", model.RelationshipIn);
            else
                TemplateHTML = TemplateHTML.Replace("@RelationshipIn@", "");
            if (!string.IsNullOrEmpty(model.RelationshipOut))
                TemplateHTML = TemplateHTML.Replace("@RelationshipOut@", model.RelationshipOut);
            else
                TemplateHTML = TemplateHTML.Replace("@RelationshipOut@", "");
            if (!string.IsNullOrEmpty(TemplateHTML))
            {
                StringBuilder strHTMLContent = new StringBuilder();
                strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><meta charset=\"UTF-8\"></head><body>");
                strHTMLContent.Append(TemplateHTML);
                strHTMLContent.Append("</body></html>");
                TempData["Info_Title"] = strHTMLContent;
                TempData["Title"] = model.Name;
            }
            //using (StreamWriter file = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Temp\\Log.txt", true))
            //{
            //    file.WriteLine(TemplateHTML);
            //}
            return Json(new { success = true, responseText = "get data successfuly" }, JsonRequestBehavior.AllowGet);
        }
        public void DownloadFileInfoTitles()
        {
            var strHTMLContent = TempData["Info_Title"];
            Response.Clear();
            Response.ContentType = "application/msword";
            Response.Charset = "UTF-8";
            Response.ContentEncoding = UnicodeEncoding.UTF8;
            string strFileName = TempData["Title"] + ".doc";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName);
            Response.Write(strHTMLContent);
            Response.End();
        }
        public ActionResult FormResponsibilityTitles(string id = "", string titleid = "")
        {
            ResponsibilityTitlesBO model = null;
            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                if (!string.IsNullOrEmpty(titleid) && titleid != Guid.Empty.ToString())
                    model = new ResponsibilityTitlesBO() { TitleId = Guid.Parse(titleid) };
                else
                    model = new ResponsibilityTitlesBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = new ResponsibilityTitlesBO();
                if (!string.IsNullOrEmpty(titleid) && titleid != Guid.Empty.ToString())
                {
                    DataTable data = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_DepartmentTitle_GetById", parameter: new { id = id });
                    if (data != null && data.Rows.Count > 0)
                    {
                        model.ResponsibilityId = Guid.Parse(data.Rows[0]["Id"].ToString());
                        model.TitleId = Guid.Parse(data.Rows[0]["TitleId"].ToString());
                        model.NameResponsibility = data.Rows[0]["Name"] != null ? data.Rows[0]["Name"].ToString() : "";
                        model.Loop = data.Rows[0]["Loop"] != null ? data.Rows[0]["Loop"].ToString() : "";
                        model.Mission = data.Rows[0]["Mission"] != null ? data.Rows[0]["Mission"].ToString() : "";
                        model.Request = data.Rows[0]["Request"] != null ? data.Rows[0]["Request"].ToString() : "";
                        model.Result = data.Rows[0]["Result"] != null ? data.Rows[0]["Result"].ToString() : "";
                        model.Criteria = data.Rows[0]["Criteria"] != null ? data.Rows[0]["Criteria"].ToString() : "";
                    }
                }
                else
                {
                    List<ResponsibilityTitlesBO> lst = new List<ResponsibilityTitlesBO>();
                    lst = Session["lstResponsibilityTitlesBO"] as List<ResponsibilityTitlesBO>;
                    model = lst.Where(x => x.Id == Guid.Parse(id)).First();
                    model.ResponsibilityId = model.Id;
                    model.TitleId = Guid.Empty;
                }
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion
        #region Method
        public ActionResult LoadData(StoreRequestParameters parameters, Guid departmentId)
        {
            try
            {
                var roles = DepartmentTitleService.GetRolesByDepartment(departmentId)
                    .ToList();
                return this.Store(roles, roles.Count());
            }
            catch
            {
            }
            return this.Direct();
        }
        public ActionResult LoadDataTrachNhiem(string titleid)
        {
            try
            {
                List<ResponsibilityTitlesBO> lst = new List<ResponsibilityTitlesBO>();
                if (string.IsNullOrEmpty(titleid) || titleid == Guid.Empty.ToString())
                {
                    lst = Session["lstResponsibilityTitlesBO"] as List<ResponsibilityTitlesBO>;
                }
                else
                {
                    DataTable data = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_ResponsibilityTitles_GetData", parameter: new { titleid = titleid });
                    if (data != null && data.Rows.Count > 0)
                    {
                        foreach (DataRow item in data.Rows)
                        {
                            ResponsibilityTitlesBO receiveBO = new ResponsibilityTitlesBO();
                            receiveBO.Id = Guid.Parse(item["Id"].ToString());
                            receiveBO.ResponsibilityId = Guid.Parse(item["Id"].ToString());
                            receiveBO.NameResponsibility = item["Name"] != null ? item["Name"].ToString() : "";
                            receiveBO.Loop = item["Loop"] != null ? item["Loop"].ToString() : "";
                            receiveBO.Mission = item["Mission"] != null ? item["Mission"].ToString() : "";
                            receiveBO.Request = item["Request"] != null ? item["Request"].ToString() : "";
                            receiveBO.Result = item["Result"] != null ? item["Result"].ToString() : "";
                            receiveBO.Criteria = item["Criteria"] != null ? item["Criteria"].ToString() : "";
                            lst.Add(receiveBO);
                        }
                    }
                }
                return this.Store(lst, lst.Count());
            }
            catch
            {
            }
            return this.Direct();
        }
        [ValidateInput(false)]
        public ActionResult Create(TitleBO itemTitle)
        {
            try
            {
                DepartmentTitleService.Insert(itemTitle);
                List<ResponsibilityTitlesBO> lst = new List<ResponsibilityTitlesBO>();
                lst = Session["lstResponsibilityTitlesBO"] as List<ResponsibilityTitlesBO>;
                if (lst != null && lst.Count > 0)
                {
                    foreach (ResponsibilityTitlesBO item in lst)
                    {
                        Sql_ADO.idasAdoService.ExecuteNoquery("sp_ResponsibilityTitles_Insert", parameter: new
                        {
                            Id = Guid.NewGuid(),
                            TitleId = itemTitle.Id,
                            Loop = !string.IsNullOrEmpty(item.Loop) ? item.Loop : "",
                            Request = !string.IsNullOrEmpty(item.Request)? item.Request:"",
                            Result = !string.IsNullOrEmpty(item.Result) ? item.Result : "",
                            Mission = !string.IsNullOrEmpty(item.Mission) ? item.Mission : "",
                            Name = !string.IsNullOrEmpty(item.NameResponsibility) ? item.NameResponsibility : "",
                            Criteria = !string.IsNullOrEmpty(item.Criteria) ? item.Criteria : ""
                        });
                    }
                }
                Session.Remove("lstResponsibilityTitlesBO");
                this.ShowNotify(Common.Resource.SystemSuccess);
                return this.Direct(true);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct(false);
            }

        }
        [ValidateInput(false)]
        public ActionResult CreateResponsibilityTitles(ResponsibilityTitlesBO item)
        {
            try
            {

                if (!item.TitleId.HasValue)
                {
                    List<ResponsibilityTitlesBO> lst = new List<ResponsibilityTitlesBO>();
                    item.Id = Guid.NewGuid();
                    item.ResponsibilityId = item.Id;
                    if (Session["lstResponsibilityTitlesBO"] != null)
                    {
                        lst = Session["lstResponsibilityTitlesBO"] as List<ResponsibilityTitlesBO>;
                        lst.Add(item);
                        Session["lstResponsibilityTitlesBO"] = lst;
                    }
                    else
                    {
                        lst.Add(item);
                        Session["lstResponsibilityTitlesBO"] = lst;
                    }
                }
                else
                {
                    Sql_ADO.idasAdoService.ExecuteNoquery("sp_ResponsibilityTitles_Insert", parameter: new
                    {
                        Id = Guid.NewGuid(),
                        TitleId = item.TitleId,
                        Loop = !string.IsNullOrEmpty(item.Loop) ? item.Loop : "",
                        Request = !string.IsNullOrEmpty(item.Request) ? item.Request : "",
                        Result = !string.IsNullOrEmpty(item.Result) ? item.Result : "",
                        Mission = !string.IsNullOrEmpty(item.Mission) ? item.Mission : "",
                        Name = !string.IsNullOrEmpty(item.NameResponsibility) ? item.NameResponsibility : "",
                        Criteria = !string.IsNullOrEmpty(item.Criteria) ? item.Criteria : ""
                    });
                }
                this.ShowNotify(Common.Resource.SystemSuccess);
                return this.Direct(true);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct(false);
            }

        }
        [ValidateInput(false)]
        public ActionResult Update(TitleBO item)
        {
            UserRoleService roleService = new UserRoleService();
            var isAdmin = roleService.RoleExits("Administrator");
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckRoleForTitles_Edit(item.Id);
            if (!isAdmin && !titleRole)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }
            try
            {
                DepartmentTitleService.Update(item);
                this.ShowNotify(Common.Resource.SystemSuccess);
                return this.Direct(true);
            }
            catch (Exception ex)
            {

                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct(false);
            }
        }
        [ValidateInput(false)]
        public ActionResult UpdateResponsibilityTitles(ResponsibilityTitlesBO item)
        {
            try
            {
                if (!item.TitleId.HasValue)
                {
                    List<ResponsibilityTitlesBO> lst = new List<ResponsibilityTitlesBO>();
                    if (Session["lstResponsibilityTitlesBO"] != null)
                    {
                        lst = Session["lstResponsibilityTitlesBO"] as List<ResponsibilityTitlesBO>;
                        if (item.Id != Guid.Empty)
                        {
                            int index = lst.FindIndex(x => x.ResponsibilityId == item.ResponsibilityId);
                            if (index > -1)
                                lst.RemoveAt(index);
                        }
                        lst.Add(item);
                        Session["lstResponsibilityTitlesBO"] = lst;
                    }
                }
                else
                {
                    Sql_ADO.idasAdoService.ExecuteNoquery("sp_ResponsibilityTitles_Update", parameter: new
                    {
                        Id = item.ResponsibilityId,
                        TitleId = item.TitleId,
                        Loop = !string.IsNullOrEmpty(item.Loop) ? item.Loop : "",
                        Request = !string.IsNullOrEmpty(item.Request) ? item.Request : "",
                        Result = !string.IsNullOrEmpty(item.Result) ? item.Result : "",
                        Mission = !string.IsNullOrEmpty(item.Mission) ? item.Mission : "",
                        Name = !string.IsNullOrEmpty(item.NameResponsibility) ? item.NameResponsibility : "",
                        Criteria = !string.IsNullOrEmpty(item.Criteria) ? item.Criteria : ""
                    });
                }
                this.ShowNotify(Common.Resource.SystemSuccess);
                return this.Direct(true);
            }
            catch (Exception ex)
            {

                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct(false);
            }
        }

        public ActionResult Delete(Guid id)
        {
            UserRoleService roleService = new UserRoleService();
            var isAdmin = roleService.RoleExits("Administrator");
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckRoleForTitles_Edit(id);
            if (!isAdmin && !titleRole)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }
            try
            {
                DepartmentTitleService.DeleteByOrder(id);
                Sql_ADO.idasAdoService.ExecuteNoquery("sp_ResponsibilityTitles_DeleteByTitleId", parameter: new { id = id });
                this.ShowNotify(Common.Resource.SystemSuccess);
                return this.Direct(true);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct(false);
            }

        }

        public ActionResult DeleteResponsibilityTitles(Guid id, Guid titleid)
        {
            try
            {
                if (titleid == Guid.Empty)
                {
                    List<ResponsibilityTitlesBO> lst = new List<ResponsibilityTitlesBO>();
                    if (Session["lstResponsibilityTitlesBO"] != null)
                    {
                        lst = Session["lstResponsibilityTitlesBO"] as List<ResponsibilityTitlesBO>;
                        int index = lst.FindIndex(x => x.ResponsibilityId == id);
                        if (index > -1)
                            lst.RemoveAt(index);
                    }
                }
                else
                {
                    Sql_ADO.idasAdoService.ExecuteNoquery("sp_ResponsibilityTitles_DeleteById", parameter: new { id = id });
                }
                this.ShowNotify(Common.Resource.SystemSuccess);
                return this.Direct(true);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct(false);
            }

        }
        public ActionResult UpdateOrder(bool up, Guid id)
        {
            try
            {
                DepartmentTitleService.UpdateOrder(up, id);
                return this.Direct(result: true);
            }
            catch
            {
                return this.Direct(result: false);
            }
        }

        /// <summary>
        /// Get  department title
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAll()
        {
            var data = DepartmentTitleService.GetAll();
            return this.Store(data);
        }
        #endregion

    }
}