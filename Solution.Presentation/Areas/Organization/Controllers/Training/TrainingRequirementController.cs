﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class TrainingRequirementController : FrontController
    {
        //
        // GET: /Organization/TrainingRequirement/
        #region View
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexNew(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }
        public ActionResult Form(string id = default(string))
        {
            VPHumanTrainingRequirementBO model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new VPHumanTrainingRequirementBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = VPHumanTrainingRequirementService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult SendApproval(string id = default(string))
        {
            var model = VPHumanTrainingRequirementService.GetById(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult FrmApproval(string id = default(string))
        {
            var model = VPHumanTrainingRequirementService.GetById(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult DetailApproval(string id = default(string))
        {
            var model = VPHumanTrainingRequirementService.GetById(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        #endregion
        #region Method
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = VPHumanTrainingRequirementService.GetAll(pageIndex, pageSize, out count);
            return this.Store(data, count);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(VPHumanTrainingRequirementBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    if (!item.StatusRequest.HasValue)
                        item.StatusRequest = 0;
                    VPHumanTrainingRequirementService.Insert(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Send(VPHumanTrainingRequirementBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    VPHumanTrainingRequirementService.SendApproval(item);
                    Notify(TitleResourceNotifies.SendApprovalTrainingSuggest, item.Contents, item.EmployeeApproval.Id, UrlResourceNotifies.ApprovalTrainingSuggest, new { id = item.Id });
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Approval(VPHumanTrainingRequirementBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    VPHumanTrainingRequirementService.Approval(item);
                    Notify(TitleResourceNotifies.ApprovalTrainingSuggest, item.Contents, item.RequireBy.Id, UrlResourceNotifies.DetailTrainingSuggest, new { id = item.Id });
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(VPHumanTrainingRequirementBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    if (!item.StatusRequest.HasValue)
                        item.StatusRequest = 0;
                    VPHumanTrainingRequirementService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    VPHumanTrainingRequirementService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}