﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class TrainingPlanController : FrontController
    {
        //
        // GET: /Organization/TrainingPlan/
        #region View
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexNew(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }
        public ActionResult Form(string id = default(string))
        {
            VPHumanTrainingPlanBO model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new VPHumanTrainingPlanBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = VPHumanTrainingPlanService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult SendApproval(string id = default(string))
        {
            var model = VPHumanTrainingPlanService.GetById(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult FrmApproval(string id = default(string))
        {
            var model = VPHumanTrainingPlanService.GetById(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult DetailApproval(string id = default(string))
        {
            var model = VPHumanTrainingPlanService.GetById(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        #endregion
        #region Method
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = VPHumanTrainingPlanService.GetAll(pageIndex, pageSize, out count);
            return this.Store(data, count);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(VPHumanTrainingPlanBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    VPHumanTrainingPlanService.Insert(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Send(VPHumanTrainingPlanBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    VPHumanTrainingPlanService.SendApproval(item);
                    Notify(TitleResourceNotifies.SendApprovalTrainingPlan, item.Contents, item.EmployeeApproval.Id, UrlResourceNotifies.ApprovalTrainingPlan, new { id = item.Id });
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Approval(VPHumanTrainingPlanBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    VPHumanTrainingPlanService.Approval(item);
                    Notify(TitleResourceNotifies.ApprovalTrainingPlan, item.Contents, item.CreateBy.Value, UrlResourceNotifies.DetailTrainingPlan, new { id = item.Id });
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(VPHumanTrainingPlanBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    VPHumanTrainingPlanService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    VPHumanTrainingPlanService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
        #region Thiết lập yêu cầu đào tạo
        public ActionResult RequirementForm(string id = default(string))
        {
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Requirement", Model = new VPHumanTrainingPlanBO() { Id = new Guid(id) } };
        }
        public ActionResult LoadRequirementByPlan(StoreRequestParameters parameters, string id = default(string))
        {
            var pageIndex = parameters.Page;
            var pageSize = parameters.Limit;
            var count = 0;
            var data = VPHumanTrainingRequirementService.GetByPlan(pageIndex, pageSize, out count, new Guid(id));
            return this.Store(data, count);
        }
        public ActionResult LoadRequirement(StoreRequestParameters parameters, string id = default(string))
        {
            var pageIndex = parameters.Page;
            var pageSize = parameters.Limit;
            var count = 0;
            var data = VPHumanTrainingRequirementService.GetRequirementByPlan(pageIndex, pageSize, out count, new Guid(id));
            return this.Store(data, count);
        }
        public DirectResult UpdateRequirementPlan(string id, bool isSeleted = false, string planId = default(string))
        {
            object result = null;
            try
            {
                VPHumanTrainingPlanRequirementService.UpdateSelected(new Guid(id), isSeleted, new Guid(planId));
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception e)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
        #endregion
    }
}