﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class PractionersController : FrontController
    {
        //
        // GET: /Organization/Practioners/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexNew(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }
        public ActionResult LoadDataPlan(StoreRequestParameters parameters)
        {
            var pageIndex = parameters.Page;
            var pageSize = parameters.Limit;
            var count = 0;
            var data = VPHumanTrainingPlanService.GetPlanIsAccept(pageIndex, pageSize, out count);
            return this.Store(data, count);
        }
        public ActionResult GetDataPlanDetail(StoreRequestParameters parameters, string planId = default(string))
        {
            var pageIndex = parameters.Page;
            var pageSize = parameters.Limit;
            var count = 0;
            var data = VPHumanTrainingPlanDetailService.GetAll(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(planId));
            return this.Store(data, count);
        }
        public ActionResult GetDataPlanDetailResult(StoreRequestParameters parameters, string planId = default(string))
        {

            var pageIndex = parameters.Page;
            var pageSize = parameters.Limit;
            var count = 0;
            var data = VPHumanTrainingPlanDetailService.GetResultTraining(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(planId));
            return this.Store(data, count);
        }
        public ActionResult GetData(StoreRequestParameters parameters, string detailId = default(string))
        {
            var pageIndex = parameters.Page;
            var pageSize = parameters.Limit;
            var count = 0;
            var data = VPHumanTrainingPractionerService.GetAll(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(detailId));
            return this.Store(data, count);
        }
        public ActionResult AddForm(string detailId = default(string))
        {
            VPHumanTrainingPlanDetailBO detail = VPHumanTrainingPlanDetailService.GetById(iDAS.Service.Common.Utilities.ConvertToGuid(detailId));
            var data = new VPHumanTrainingPractionerBO();
            data.ContentCommit = detail.CommitContent;
            data.IsAcceptCommit = detail.IsCommit.HasValue ? detail.IsCommit.Value : false;
            data.VPHumanTrainingPlanDetailId = detail.Id;
            data.Number = detail.Number.HasValue ? detail.Number.Value : 0;
            data.IsCommit = detail.IsCommit.HasValue ? detail.IsCommit.Value : false;
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Insert", Model = data };
        }
        public ActionResult InsertListObject(string stringId, string detailId = default(string))
        {
            try
            {
                VPHumanTrainingPractionerService.InsertObject(stringId, iDAS.Service.Common.Utilities.ConvertToGuid(detailId));
                X.GetCmp<Store>("StorePractioners").Reload();
            }
            catch
            {
                throw;
            }
            return this.Direct();
        }
        /// <summary>
        /// Hàm insert dữ liệu
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="isuse"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult Insert(VPHumanTrainingPractionerBO objNew)
        {

            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    if (objNew.Practioner.Id == Guid.Empty)
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO,
                            Title = "Thông báo",
                            Message = "Bạn phải chọn ứng viên tham gia khóa đào tạo!"
                        });
                        result = false;
                    }
                    else if (objNew.IsAcceptCommit != true && objNew.IsCommit)
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO,
                            Title = "Thông báo",
                            Message = "Để tham gia đợt đào tạo bạn phải đồng ý với cam kết trên!"
                        });
                        result = false;
                    }
                    else if (VPHumanTrainingPractionerService.CheckInvalidEmployees(objNew.Practioner.Id, objNew.VPHumanTrainingPlanDetailId))
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO,
                            Title = "Thông báo",
                            Message = "Học viên đã đăng ký đợt đào tạo vui lòng chọn học viên khác!"
                        });
                        result = false;
                    }
                    else if (VPHumanTrainingPractionerService.CheckOverNumber(objNew.VPHumanTrainingPlanDetailId, objNew.NumberAbsence))
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO,
                            Title = "Thông báo",
                            Message = "Số lượng đăng ký đã hết vui lòng chờ đợt đào tạo lần sau!"
                        });
                        result = false;
                    }
                    else
                    {
                        VPHumanTrainingPractionerService.Insert(objNew);
                        X.Msg.Notify("Thông báo", "Bạn đã thêm mới thành công!").Show();
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        #region Xóa
        /// <summary>
        /// Xóa học viên đăng ký đào tạo
        /// </summary>
        /// <param name="id"></param>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    VPHumanTrainingPractionerService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}