﻿using iDAS.Presentation.Controllers;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class TrainingController : FrontController
    {
        //
        // GET: /Organization/Training/
        public ActionResult Index()
        {
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Index" };
        }
    }
}