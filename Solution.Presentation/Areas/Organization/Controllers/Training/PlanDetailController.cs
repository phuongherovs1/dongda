﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class PlanDetailController : FrontController
    {
        //
        // GET: /Organization/PlanDetail/
        public ActionResult Index(string planId = default(string), string planName = default(string))
        {
            return new Ext.Net.MVC.PartialViewResult { ViewData = new ViewDataDictionary { { "planId", planId }, { "planName", planName } } };
        }
        public ActionResult GetData(StoreRequestParameters parameters, string planId = default(string))
        {
            var pageIndex = parameters.Page;
            var pageSize = parameters.Limit;
            var count = 0;
            var data = VPHumanTrainingPlanDetailService.GetAll(pageIndex, pageSize, out count, new Guid(planId));
            return this.Store(data, count);
        }
        public ActionResult ShowFrmAdd(string planId = default(string))
        {
            return new Ext.Net.MVC.PartialViewResult() { ViewName = "Insert", ViewData = new ViewDataDictionary { { "planId", planId } } };
        }
        public ActionResult ShowFrmUpdate(string id = default(string))
        {
            var obj = VPHumanTrainingPlanDetailService.GetById(new Guid(id));
            return new Ext.Net.MVC.PartialViewResult() { ViewName = "Update", Model = obj };
        }
        public ActionResult ShowFrmCancel(string id = default(string))
        {
            var obj = VPHumanTrainingPlanDetailService.GetById(new Guid(id));
            return new Ext.Net.MVC.PartialViewResult() { ViewName = "FrmCancel", Model = obj };
        }
        public ActionResult ShowFrmDetail(string id = default(string))
        {
            var obj = VPHumanTrainingPlanDetailService.GetById(new Guid(id));
            return new Ext.Net.MVC.PartialViewResult() { ViewName = "Detail", Model = obj };
        }
        public ActionResult UpdateIsCancel(VPHumanTrainingPlanDetailBO objEdit)
        {
            try
            {
                objEdit.IsCancel = !objEdit.IsCancel.HasValue ? false : objEdit.IsCancel.Value;
                VPHumanTrainingPlanDetailService.Update(objEdit);
                X.GetCmp<Store>("stMnPlanDetail").Reload();
            }
            catch (Exception ex)
            {
                throw;
            }
            return this.Direct();
        }
        [ValidateInput(false)]
        public ActionResult Insert(VPHumanTrainingPlanDetailBO objNew)
        {

            try
            {
                if (!objNew.StatusRequest.HasValue)
                    objNew.StatusRequest = 0;
                VPHumanTrainingPlanDetailService.Insert(objNew);
                X.GetCmp<Store>("stMnPlanTrainingDetail").Reload();
            }
            catch (Exception ex)
            {
                throw;
            }
            return this.Direct();

        }
        [ValidateInput(false)]
        public ActionResult Update(VPHumanTrainingPlanDetailBO objEdit)
        {

            try
            {
                if (!objEdit.StatusRequest.HasValue)
                    objEdit.StatusRequest = 0;
                VPHumanTrainingPlanDetailService.Update(objEdit);
                X.GetCmp<Store>("stMnPlanTrainingDetail").Reload();
            }
            catch (Exception ex)
            {
                throw;
            }
            return this.Direct();
        }
        public ActionResult Delete(string id = default(string))
        {
            try
            {
                VPHumanTrainingPlanDetailService.Delete(new Guid(id));
                X.GetCmp<Store>("stMnPlanTrainingDetail").Reload();
            }
            catch (Exception ex)
            {
                throw;
            }
            return this.Direct();
        }
    }
}