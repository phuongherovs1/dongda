﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class ResultTrainingController : FrontController
    {
        //
        // GET: /Organization/ResultTraining/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexNew(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }
        public ActionResult UpdateForm(string id = default(string))
        {
            var obj = VPHumanTrainingPractionerService.GetById(new Guid(id));
            try
            {
                return new Ext.Net.MVC.PartialViewResult { ViewName = "UpdateResult", Model = obj };
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult UpdateFormJoin(string id = default(string))
        {
            var obj = VPHumanTrainingPractionerService.GetById(new Guid(id));
            try
            {
                return new Ext.Net.MVC.PartialViewResult { ViewName = "UpdateJoin", Model = obj };
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult Update(VPHumanTrainingPractionerBO objEdit)
        {

            try
            {
                VPHumanTrainingPractionerService.Update(objEdit);
                return this.Direct();

            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult InsertToProfile(string id = default(string))
        {

            try
            {
                VPHumanTrainingPractionerService.InsertToProfile(new Guid(id));
                X.GetCmp<Store>("StorePractioners").Reload();
                return this.Direct();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult UpdateJoin(VPHumanTrainingPractionerBO objEdit)
        {
            try
            {
                if (objEdit.IsJoin == true)
                    VPHumanTrainingPractionerService.Update(objEdit);
                else
                {
                    objEdit.IsJoin = false;
                    VPHumanTrainingPractionerService.Update(objEdit);
                }
                return this.Direct();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }
    }
}