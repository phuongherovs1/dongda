﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers.Training
{
    public class TrainingRequirementEmployeeController : FrontController
    {
        //
        // GET: /Organization/TrainingRequirementEmployee/
        #region View
        public ActionResult Index(string id = default(string))
        {
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Index", Model = new VPHumanTrainingRequirementBO() { Id = new Guid(id) } };
        }
        public ActionResult Form(string id = default(string), string requirementId = default(string))
        {
            VPHumanTrainingRequirementEmployeeBO model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new VPHumanTrainingRequirementEmployeeBO() { VPHumanTrainingRequirementId = new Guid(requirementId) };
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = VPHumanTrainingRequirementEmployeeService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion
        #region Method
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param, string requirementId = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = VPHumanTrainingRequirementEmployeeService.GetAll(pageIndex, pageSize, out count, new Guid(requirementId));
            return this.Store(data, count);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(VPHumanTrainingRequirementEmployeeBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    VPHumanTrainingRequirementEmployeeService.Insert(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(VPHumanTrainingRequirementEmployeeBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    VPHumanTrainingRequirementEmployeeService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    VPHumanTrainingRequirementEmployeeService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}