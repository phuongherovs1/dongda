﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class QuestionController : FrontController
    {
        //
        // GET: /Organization/Question/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexNew(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }

        public ActionResult Form(Guid? Id)
        {
            if (Id == null)
            {
                var model = new VPHumanTrainingQuestionCategorieBO() { };
                ViewData.Add("Operation", Common.Operation.Create);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                var model = VPHumanTrainingQuestionCategorieService.GetById(Id.Value);
                ViewData.Add("Operation", Common.Operation.Update);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }

        }

        public ActionResult FormQuestion(Guid? Id, Guid? QuestionCategoryId)
        {
            if (Id == null)
            {
                var model = VPHumanTrainingQuestionService.CreateDefaut(QuestionCategoryId.Value);
                ViewData.Add("Operation", Common.Operation.Create);

                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                var model = VPHumanTrainingQuestionService.ViewDetalQuest(Id.Value);
                ViewData.Add("Operation", Common.Operation.Update);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }

        }

        public ActionResult LoadQuestionCate(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = VPHumanTrainingQuestionCategorieService.GetQuestionCategory(pageIndex, pageSize, out count).ToList();
            return this.Store(data, count);
        }


        public ActionResult LoadQuestion(StoreRequestParameters param, Guid QuestionCategorieId)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = VPHumanTrainingQuestionService.GetQuestion(pageIndex, pageSize, out count, QuestionCategorieId).ToList();
            return this.Store(data, count);
        }
        public ActionResult Create(VPHumanTrainingQuestionCategorieBO item)
        {
            var result = false;
            try
            {
                VPHumanTrainingQuestionCategorieService.Insert(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);

        }


        public ActionResult CreateQuest(VPHumanTrainingQuestionBO item)
        {
            var result = false;
            try
            {
                VPHumanTrainingQuestionService.Insert(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);

        }

        public ActionResult UpdateQuestion(VPHumanTrainingQuestionBO item)
        {
            var result = false;
            try
            {
                VPHumanTrainingQuestionService.Update(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);

        }

        public ActionResult DeleteQuestion(Guid Id)
        {
            var result = false;
            try
            {
                VPHumanTrainingQuestionService.Delete(Id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);

        }

        public ActionResult Update(VPHumanTrainingQuestionCategorieBO item)
        {
            var result = false;
            try
            {
                VPHumanTrainingQuestionCategorieService.Update(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);

        }

        public ActionResult Delete(Guid Id)
        {
            var result = false;
            try
            {
                VPHumanTrainingQuestionCategorieService.Delete(Id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);

        }
    }
}