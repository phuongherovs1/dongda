﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;
using iDAS.Service.API.Organization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class EmployeeSearchController : Controller
    {
        // GET: Organization/EmployeeSearch
        protected IEmployeeSearchService employeeSearchService { set; get; }
        public EmployeeSearchController(IEmployeeSearchService _employeeSearchService)
        {
            employeeSearchService = _employeeSearchService;
        }

        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult CertificateIndex(string containerId)
        {
            return PartialView();
        }

        public ActionResult DecisionIndex(string containerId)
        {
            return PartialView();
        }

        public ActionResult ContractIndex(string containerId)
        {
            return PartialView();
        }

        /// <summary>
        /// Tìm kiếm Cán bộ theo hồ sơ Lý lịch/Công tác (tôn giáo, CMT/hộ chiếu, dân tộc, nơi công tác, chức vụ)
        /// </summary>
        public ActionResult GetData(string religion = "", string identityCard = "", string people = "", string placeOfWork = "", string position = "")
        {
            DataTable dt = employeeSearchService.GetData(religion, identityCard, people, placeOfWork, position);
            return this.Store(AddAvatarUrl(dt));
        }

        /// <summary>
        /// Tìm kiếm Cán bộ theo hồ sơ Văn bằng/Chứng chỉ( văn bằng, khoa, chuyên ngành, tên chứng chỉ, nơi đào tạo chứng chỉ)
        /// </summary>
        public ActionResult GetDataByCertificate(string diplomasName, string faculty, string major, string certificatesName, string placeOfTraining)
        {
            DataTable dt = employeeSearchService.GetDataByCertificate(diplomasName, faculty, major, certificatesName, placeOfTraining);
            return this.Store(AddAvatarUrl(dt));
        }

        /// <summary>
        /// Tìm kiếm Cán bộ theo hồ sơ khen thưởng/kỷ luật
        /// </summary>
        public ActionResult GetDataByDecision(string numberOfDecisionReward, string reasonReward, string formReward, string numberOfDecisionDiscipline, string reasonDiscipline, string formDiscipline)
        {
            DataTable dt = employeeSearchService.GetDataByDecision(numberOfDecisionReward, reasonReward, formReward, numberOfDecisionDiscipline, reasonDiscipline, formDiscipline);
            return this.Store(AddAvatarUrl(dt));
        }

        public ActionResult GetDataByContract(string numberOfContracts, string typeOfContracts, string numberOfInsurances, string typeOfInsurances)
        {
            DataTable dt = employeeSearchService.GetDataByContract(numberOfContracts, typeOfContracts, numberOfInsurances, typeOfInsurances);
            return this.Store(AddAvatarUrl(dt));
        }

        private DataTable AddAvatarUrl(DataTable dt)
        {
            if (dt == null)
                return null;

            // Thêm ảnh hiển thị           
            dt.Columns.Add("AvatarUrl");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["Avatar"].ToString().Length > 0)
                    dt.Rows[i]["AvatarUrl"] = string.Format("data:image;base64,{0}", Convert.ToBase64String((byte[])dt.Rows[i]["Avatar"]));
                else
                    dt.Rows[i]["AvatarUrl"] = @"/Content/Images/underfind.jpg";
            }
            return dt;
        }

        /// <summary>
        /// Thống kê theo loại thông tin (dân tộc, tôn giáo, trình độ ...)
        /// </summary>
        /// <param name="statisticType"></param>
        /// <returns></returns>
        public ActionResult GetStatistic(string statisticType = "")
        {
            try
            {
                DataTable dt = employeeSearchService.GetStatistic(statisticType);
                return this.Store(dt);
            }
            catch (Exception ex)
            {
                return this.Store(new DataTable());
            }
        }

        public ActionResult GetStatistic1(string statisticType = "")
        {

            try
            {

                List<TaskPieChartBO> lstSum = new List<TaskPieChartBO>();

                int UnPerForm = 3;
                int PerForm1 = 6;
                int PerForm2 = 4;
                int Finish1 = 9;
                int Finish2 = 5;
                int Cancel = 1;

                lstSum.Add(new TaskPieChartBO() { Name = "name1", Value = UnPerForm });
                lstSum.Add(new TaskPieChartBO() { Name = "name2", Value = PerForm1 });
                lstSum.Add(new TaskPieChartBO() { Name = "name3", Value = PerForm2 });
                lstSum.Add(new TaskPieChartBO() { Name = "name4", Value = Finish1 });
                lstSum.Add(new TaskPieChartBO() { Name = "name5", Value = Finish2 });
                lstSum.Add(new TaskPieChartBO() { Name = "name6", Value = Cancel });

                return new StoreResult(lstSum);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}