﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class EmployeeTitleController : FrontController
    {
        //
        // GET: /Organization/EmployeeTitle/
        public ActionResult Index(string employeeId)
        {
            var exist = UserRoleService.RoleExits("ViewEmployeeProfile");
            var isAdmin = UserRoleService.RoleExits("Administrator");
            var isCurrent = UserRoleService.CurrentUser(new Guid(employeeId));
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckTitleRole_View(new Guid(employeeId));
            if (exist || isCurrent || isAdmin || titleRole)
            {
                try
                {
                    ViewData["EmployeeID"] = employeeId;
                    return new Ext.Net.MVC.PartialViewResult() { ViewData = ViewData };
                }
                catch
                {
                }
                return this.Direct();
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền truy cập chức năng này!"
                });
                return this.Direct();
            }
        }
        public ActionResult Form(string id = default(string), string employeeId = default(string))
        {
            EmployeeTitleBO model = null;
            if (id == default(string))
            {
                var isAdmin = UserRoleService.RoleExits("Administrator");
                var isCurrent = UserRoleService.CurrentUser(new Guid(employeeId));
                DepartmentTitleRoleService s = new DepartmentTitleRoleService();
                var titleRole = s.CheckTitleRole_Edit(new Guid(employeeId));
                if (!isCurrent && !isAdmin && !titleRole)
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = "Bạn không có quyền thực hiện chức năng này"
                    });
                    return this.Direct();
                }
                ViewData.Add("Operation", Common.Operation.Create);
                model = new EmployeeTitleBO();
                model.EmployeeId = new Guid(employeeId);
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                var obj = EmployeeTitleService.GetById(new Guid(id.ToString()));
                obj.DepartmentId = DepartmentTitleService.GetDepartmentByRole(obj.TitleId);
                obj.DepartmentName = DepartmentService.GetById(obj.DepartmentId).Name;
                obj.TitleName = DepartmentTitleService.GetById(obj.TitleId).Name;
                obj.EmployeeId = new Guid(employeeId);
                if (obj.ProfileWorkExperienceId.HasValue)
                {
                    var temp = VPHumanProfileAttachmentFileService.GetFileByProfileId(obj.ProfileWorkExperienceId.Value);
                    obj.FileAttachs = new FileUploadBO()
                    {
                        Files = temp.ToList()
                    };
                }
                obj.DepartmentId_Update = obj.DepartmentId;
                obj.TitleId_Update = obj.TitleId;
                model = obj;
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult GetDepartmentTitle(string departmentId = "")
        {
            var department = !string.IsNullOrEmpty(departmentId) ? new Guid(departmentId) : Guid.Empty;
            var data = DepartmentTitleService.GetRolesByDepartment(department).OrderBy(x => x.Name);
            return this.Store(data, data.ToList().Count);
        }
        public ActionResult LoadData(StoreRequestParameters parameters, Guid employeeId)
        {
            try
            {
                var roles = EmployeeTitleService.GetRolesByEmployeeID(employeeId, allowGetRoleName: true);
                foreach (var item in roles)
                {
                    if (item.ProfileWorkExperienceId.HasValue)
                    {
                        var temp = VPHumanProfileAttachmentFileService.GetFileByProfileId(item.ProfileWorkExperienceId.Value);
                        item.FileAttachs = new FileUploadBO()
                        {
                            Files = temp.ToList()
                        };
                    }
                    else
                    {
                        item.FileAttachs = new FileUploadBO();
                    }
                }
                return this.Store(roles);
            }
            catch(Exception ex)
            {
            }
            return this.Direct();
        }
        public ActionResult SetEmployeeTitle(EmployeeTitleBO item)
        {
            var isAdmin = UserRoleService.RoleExits("Administrator");
            var isCurrent = UserRoleService.CurrentUser(item.EmployeeId);
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckTitleRole_Edit(item.EmployeeId);
            if (!isCurrent && !isAdmin && !titleRole)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }
            try
            {
                if (!item.ProfileWorkExperienceId.HasValue)
                {
                    #region Add qua trinh cong tác
                    VPHumanProfileWorkExperienceBO data = new VPHumanProfileWorkExperienceBO();
                    var titles = DepartmentTitleService.GetById(item.TitleId);
                    var department = DepartmentService.GetById(item.DepartmentId);
                    data.HumanEmployeeId = item.EmployeeId;
                    data.StartDate = item.StartDate;
                    data.EndDate = item.EndDate;
                    data.Position = titles.Name;
                    data.Department = department.Name;
                    data.FileAttachs = item.FileAttachs;
                    data.JobDescription = item.Note;
                    data.PlaceOfWork = ConfigurationManager.AppSettings["PlaceOfWork"]; ;
                    var fileIds = Enumerable.Empty<Guid>();
                    if (data != null && data.FileAttachs != null && data.FileAttachs.FileAttachments.Any())
                    {
                        if (data != null && data.FileAttachs != null)
                        {
                            fileIds = FileService.UploadRange(data.FileAttachs, false);
                        }
                    }
                    data.Id = VPHumanProfileWorkExperienceService.Insert(data, true);
                    foreach (var fileId in fileIds)
                    {
                        var attachment = new VPHumanProfileAttachmentFileBO()
                        {
                            AttachmentFileId = fileId,
                            HumanProfileAttachmentId = data.Id
                        };
                        VPHumanProfileAttachmentFileService.Insert(attachment, true);
                    }
                    #endregion
                    item.ProfileWorkExperienceId = data.Id;
                }
                if (item.Id != null && item.Id != Guid.Empty)
                {
                    item.DepartmentId = item.DepartmentId_Update;
                    item.TitleId = item.TitleId_Update;
                    EmployeeTitleService.Update(item);
                }
                else
                    EmployeeTitleService.Insert(item);
                return this.Direct(result: true);
            }
            catch (Exception ex)
            {
                return this.Direct(result: false);
            }
        }
        public ActionResult GetAllDepartmentByCurrentUser()
        {
            try
            {
                var data = DepartmentService.GetDepartmentsByCurrentUser();
                return this.Store(data);
            }
            catch
            {
            }
            return this.Direct();
        }
        public ActionResult Delete(Guid id)
        {
            try
            {
                var employeeTitle = EmployeeTitleService.GetById(id);
                EmployeeTitleService.Delete(id);
                if(employeeTitle.ProfileWorkExperienceId.HasValue)
                    VPHumanProfileAttachmentFileService.Delete(employeeTitle.ProfileWorkExperienceId.Value);
                return this.Direct(result: true);
            }
            catch
            {
                return this.Direct(result: false);
            }
        }

        public ActionResult LoadDataByRole(Guid roleId)
        {
            try
            {
                var count = 0;
                var data = EmployeeTitleService.GetByTitleId(roleId);
                return this.Store(data, count);
            }
            catch
            {
                return this.Store(null);
            }
        }
    }
}