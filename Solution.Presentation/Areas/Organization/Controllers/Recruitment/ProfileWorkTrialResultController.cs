﻿using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class ProfileWorkTrialResultController : FrontController
    {
        /// <summary>
        /// Thêm mới/sửa yêu cầu tự đánh giá của nhân viên thử việc
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult Create(VPHumanProfileWorkTrialResultBO data)
        {
            try
            {
                VPHumanProfileWorkTrialResultService.Create(data);
                Notify(TitleResourceNotifies.SendProfileWorkTrialResult, data.Note, data.HumanProfileWorkTrialId.Value, UrlResourceNotifies.SendProfileWorkTrialResult, new { id = data.Id });
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }

        /// <summary>
        /// Xóa yêu cầu tự đánh giá
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(Guid id)
        {
            try
            {
                VPHumanProfileWorkTrialResultService.Delete(id);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }
    }
}
