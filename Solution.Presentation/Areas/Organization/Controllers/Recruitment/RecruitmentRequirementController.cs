﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Linq;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class RecruitmentRequirementController : FrontController
    {
        #region Thông tin chung
        public ActionResult Index()
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            return PartialView();
        }
        public ActionResult IndexNew(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                ViewData["departmentName"] = department.Name;
                ViewData["departmentId"] = department.Id;
            }
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
                //check view
            };
        }
        /// <summary>
        /// Load hồ sơ yêu cầu tuyển dụng
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public ActionResult LoadRequirement(StoreRequestParameters param, string DepartmentID = default(string))
        {
            var departmentSplitId = Guid.Empty;
            if (DepartmentID.Contains('_'))
            {
                departmentSplitId = new Guid(DepartmentID.Split('_')[1]);
            }
            else
            {
                if (!string.IsNullOrEmpty(DepartmentID))
                {
                    departmentSplitId = new Guid(DepartmentID);
                }
            }
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            if (departmentSplitId != Guid.Empty)
            {
                var data = VPHumanRecruitmentRequirementService.GetAllByDeptID(pageIndex, pageSize, out count, departmentSplitId);
                return this.Store(data, count);
            }
            else
            {
                var data = VPHumanRecruitmentRequirementService.GetAll(pageIndex, pageSize, out count);
                return this.Store(data, count);
            }
        }
        #endregion

        #region Thêm
        [HttpGet]
        public ActionResult Insert()
        {
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Add" };
        }

        /// <summary>
        /// Thêm mới yêu cầu tuyển dụng
        /// </summary>
        /// <param name="updateData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Insert(VPHumanRecruitmentRequirementBO updateData, bool IsEdit = true, string RoleId = default(string))
        {
            try
            {
                updateData.IsEdit = IsEdit;
                if (RoleId.Contains('_'))
                {
                    string[] RoleIdIdsl = RoleId.Trim().Split('_');
                    RoleId = RoleIdIdsl[1];
                }
                updateData.HumanRoleId = new Guid(RoleId);
                updateData.ApprovalBy = updateData.EmployeeApproveBy.Id;
                updateData.StatusApproval = 0;
                updateData.ApproveNumber = 0;
                updateData.Id = Guid.NewGuid();
                VPHumanRecruitmentRequirementService.Insert(updateData);
                Notify(TitleResourceNotifies.SendApprovalRecruitmentRequest + " \"" + updateData.Name + "\"", updateData.Reason, updateData.EmployeeApproveBy.Id, UrlResourceNotifies.ApprovalRecruitmentRequest, new { ID = updateData.Id });
            }
            catch
            {
            }
            finally
            {

                X.GetCmp<Window>("winRequirementAdd").Close();
                X.GetCmp<Store>("StoreRequirement").Reload();
            }
            return this.Direct();
        }
        #endregion

        #region Sửa
        /// <summary>
        /// Trỏ đến form cập nhật yêu cầu tuyển dụng 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Update(Guid ID)
        {
            var data = new VPHumanRecruitmentRequirementBO();
            if (ID != Guid.Empty)
            {
                data = VPHumanRecruitmentRequirementService.updateRecruitment(ID);
            }
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };

        }
        public ActionResult UpdateApproveNumber(Guid Id, int? ApproveNumber)
        {
            if (Id != Guid.Empty)
            {
                var data = VPHumanRecruitmentRequirementService.GetById(Id);
                data.ApproveNumber = ApproveNumber.HasValue ? ApproveNumber.Value : 0;
                VPHumanRecruitmentRequirementService.Update(data);
            }
            return this.Direct();
        }
        /// <summary>
        /// Cập nhật yêu cầu tuyển dụng
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="exit"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(VPHumanRecruitmentRequirementBO updateData, string RoleId = default(string))
        {
            try
            {
                if (updateData.Id != Guid.Empty)
                {
                    if (RoleId.Contains('_'))
                    {
                        string[] RoleIdIdsl = RoleId.Trim().Split('_');
                        RoleId = RoleIdIdsl[1];
                    }
                    updateData.HumanRoleId = new Guid(RoleId);
                    updateData.ApprovalBy = updateData.EmployeeApproveBy.Id;
                    VPHumanRecruitmentRequirementService.Update(updateData);
                }
            }
            catch
            {
            }
            finally
            {
                X.GetCmp<Window>("winRequirementUpdate").Close();
                X.GetCmp<Store>("StoreRequirement").Reload();

            }
            return this.Direct();
        }
        #endregion

        #region Xóa
        /// <summary>
        /// Xóa yêu cầu tuyển dụng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeleteRequirement(Guid id)
        {
            try
            {
                VPHumanRecruitmentRequirementService.Delete(id);
            }
            catch
            {
            }
            finally
            {
                X.GetCmp<Store>("StoreRequirement").Reload();
            }
            return this.Direct();
        }
        #endregion

        #region Chi tiết
        /// <summary>
        /// Form chi tiết yêu cầu tuyển dụng
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public ActionResult DetailForm(Guid ID)
        {
            var data = VPHumanRecruitmentRequirementService.GetById(ID);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
        }
        /// <summary>
        /// Lấy danh sách tiêu chí theo chức danh
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="roleID"></param>
        /// <returns></returns>
        //public ActionResult LoadCriteria(StoreRequestParameters parameters, int roleID)
        //{
        //    var CriteriaSv = new HumanRecruitmentCriteriaSV();
        //    int totalCount;
        //    var result = CriteriaSv.GetByRole(parameters.Page, parameters.Limit, out totalCount, roleID);
        //    return this.Store(result, totalCount);
        //}
        #endregion

        #region Phê duyệt
        public ActionResult frmApprove(Guid ID)
        {
            var data = new VPHumanRecruitmentRequirementBO();
            if (ID != Guid.Empty)
            {
                data = VPHumanRecruitmentRequirementService.updateRecruitment(ID);
            }
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Approve", Model = data };
        }
        /// <summary>
        /// Phê duyệt yêu cầu
        /// </summary>
        /// <param name="updateData"></param>
        /// <returns></returns>
        public ActionResult Approve(VPHumanRecruitmentRequirementBO updateData)
        {
            try
            {
                if (updateData.Id != Guid.Empty)
                {
                    VPHumanRecruitmentRequirementService.Approve(updateData);
                }
            }
            catch
            {
            }
            finally
            {
                X.GetCmp<Window>("winRequirementApprove").Close();
                X.GetCmp<Store>("StoreRequirement").Reload();

            }
            return this.Direct();
        }
        #endregion
    }
}
