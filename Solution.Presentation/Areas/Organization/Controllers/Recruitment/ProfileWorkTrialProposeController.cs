﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class ProfileWorkTrialProposeController : FrontController
    {
        //
        // GET: /Human/ProfileWorkTrialPropose/
        public ActionResult Index(int focusId = 0)
        {
            ViewBag.FocusId = focusId;
            return View();
        }
        public ActionResult IndexNew(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }
        public ActionResult GetData(StoreRequestParameters parameters)
        {
            var pageIndex = parameters.Page;
            var pageSize = 20;
            var count = 0;
            var data = VPHumanProfileWorkTrialService.GetDataApproval(pageIndex, pageSize, out count);
            return this.Store(data, count);
        }
        public ActionResult GetAuditResult(int humanProfileWorkTrialID = 0)
        {
            //List<HumanProfileWorkTrialResultItem> lstData = new List<HumanProfileWorkTrialResultItem>();
            //if (humanProfileWorkTrialID > 0)
            //{
            //    lstData = service.GetResultAudit(humanProfileWorkTrialID);
            //}

            //return this.Store(lstData, lstData.Count);
            return this.Store(null);
        }
        #region Phê duyệt
        public ActionResult frmApprove(string id)
        {
            var data = new VPHumanProfileWorkTrialBO();
            data = VPHumanProfileWorkTrialService.GetDetail(id);
            if (!data.IsEdit)
            {
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Approve", Model = data };
            }
            return this.Direct();
        }
        [HttpPost]
        public ActionResult Approve(VPHumanProfileWorkTrialBO updateData)
        {
            try
            {
                VPHumanProfileWorkTrialService.Approval(updateData);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }
        #endregion
    }
}
