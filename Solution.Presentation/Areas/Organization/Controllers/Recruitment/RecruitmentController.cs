﻿using Ext.Net;
using iDAS.Presentation.Controllers;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class RecruitmentController : FrontController
    {
        public ActionResult RecruitmentForm()
        {
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Recruitment" };
        }
        public ActionResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit;
            else
                ViewData["isroleedit"] = "0";
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }
    }
}
