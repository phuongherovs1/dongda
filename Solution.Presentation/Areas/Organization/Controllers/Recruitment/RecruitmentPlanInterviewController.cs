﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class RecruitmentPlanInterviewController : FrontController
    {
        public ActionResult InterviewForm(Guid ID)
        {
            var data = new VPHumanRecruitmentPlanInterviewBO();
            data.HumanRecruitmentPlanId = ID;
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Interview", Model = data };
        }
        /// <summary>
        /// Gọi form cập nhật thêm sửa xóa cho vòng thi tuyển
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="action"></param>
        /// <param name="PlanID"></param>
        /// <param name="IsInterview"></param>
        /// <returns></returns>
        public ActionResult InterviewUpdateForm(string ID, string action, Guid PlanID)
        {
            var data = new VPHumanRecruitmentPlanInterviewBO();
            if (!string.IsNullOrEmpty(ID))
            {
                data = VPHumanRecruitmentPlanInterviewService.GetById(new Guid(ID));
            }
            else
            {
                data.HumanRecruitmentPlanId = PlanID;
            };

            return new Ext.Net.MVC.PartialViewResult { ViewName = "InterviewUpdate", Model = data };
        }
        /// <summary>
        /// Cập nhât vòng thi tuyển
        /// </summary>
        /// <param name="updateData"></param>
        /// <returns></returns>
        public ActionResult UpdateInterview(VPHumanRecruitmentPlanInterviewBO updateData)
        {

            try
            {
                VPHumanRecruitmentPlanInterviewService.Update(updateData);
                X.GetCmp<Store>("PlanInterviewStore").Reload();
            }
            catch
            {

            }
            finally
            {
                X.GetCmp<Window>("winInterviewUpdate").Close();
            }
            return this.Direct();
        }

        /// <summary>
        /// Cập nhât vòng thi tuyển
        /// </summary>
        /// <param name="updateData"></param>
        /// <returns></returns>
        public ActionResult CreateInterview(VPHumanRecruitmentPlanInterviewBO updateData)
        {
            try
            {
                VPHumanRecruitmentPlanInterviewService.Insert(updateData);
                X.GetCmp<Store>("PlanInterviewStore").Reload();
            }
            catch
            {

            }
            finally
            {
                X.GetCmp<Window>("winInterviewUpdate").Close();
            }
            return this.Direct();
        }
        /// <summary>
        /// Lấy danh sách vòng thi tuyển của kế hoạch
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public ActionResult LoadPlanInterview(StoreRequestParameters parameters, Guid Id)
        {
            int totalCount;
            var result = VPHumanRecruitmentPlanInterviewService.GetByPlanId(Id, parameters.Page, parameters.Limit, out totalCount);
            return this.Store(result, totalCount);

        }
        public ActionResult GetByProfieInterviewId(StoreRequestParameters parameters, Guid ProfileInterviewID, Guid PlanID)
        {
            int totalCount;
            var result = VPHumanRecruitmentPlanInterviewService.GetByProfieInterviewId(PlanID, ProfileInterviewID, parameters.Page, parameters.Limit, out totalCount);
            return this.Store(result, totalCount);

        }

        /// <summary>
        /// Xóa vòng thi tuyển
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeletePlanInterview(Guid id)
        {
            try
            {
                VPHumanRecruitmentPlanInterviewService.Delete(id);
            }
            catch
            {

            }
            finally
            {
                X.GetCmp<Store>("PlanInterviewStore").Reload();
            }
            return this.Direct();
        }
    }
}
