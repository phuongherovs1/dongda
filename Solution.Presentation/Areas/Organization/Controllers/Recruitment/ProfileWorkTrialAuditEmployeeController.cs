﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Linq;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class ProfileWorkTrialAuditEmployeeController : FrontController
    {
        //
        // GET: /Human/ProfileWorkTrialAuditEmployee/
        //private HumanProfileWorkTrialSV service = new HumanProfileWorkTrialSV();
        #region Lấy dữ liệu
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexNew(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }
        public ActionResult GetData(StoreRequestParameters parameters)
        {
            var pageIndex = parameters.Page;
            var pageSize = 20;
            var count = 0;
            var data = VPHumanProfileWorkTrialService.GetByEmployee(pageIndex, pageSize, out count).ToList();
            return this.Store(data, count);
        }
        public ActionResult GetAuditResult(StoreRequestParameters parameters, string humanProfileWorkTrialID)
        {
            var data = Enumerable.Empty<VPHumanProfileWorkTrialResultBO>();
            var pageIndex = parameters.Page;
            var pageSize = 20;
            var count = 0;
            if (!string.IsNullOrEmpty(humanProfileWorkTrialID))
                data = VPHumanProfileWorkTrialResultService.GetByHumanProfileWorkTrialId(pageIndex, pageSize, out count, new Guid(humanProfileWorkTrialID));
            return this.Store(data, count);
        }
        #endregion
        public ActionResult AuditForm(string id)
        {
            var data = new VPHumanProfileWorkTrialBO();
            data = VPHumanProfileWorkTrialService.GetDetail(id);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Audit", Model = data };
        }
        public ActionResult Detail(string id)
        {
            var data = new VPHumanProfileWorkTrialBO();
            data = VPHumanProfileWorkTrialService.GetDetail(id);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
        }
        public ActionResult UpdateAudit(string strPoint, Guid humanProfileWorkTrialId)
        {
            try
            {
                VPHumanProfileWorkTrialResultService.UpdateProfileWorkTrial(strPoint, humanProfileWorkTrialId);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }
    }
}
