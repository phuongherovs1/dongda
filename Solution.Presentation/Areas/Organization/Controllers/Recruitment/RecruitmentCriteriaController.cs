﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Linq;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{

    public class RecruitmentCriteriaController : FrontController
    {
        public ActionResult Index()
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            return PartialView();
        }
        public ActionResult IndexNew(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                ViewData["departmentName"] = department.Name;
                ViewData["departmentId"] = department.Id;
            }
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }
        public ActionResult LoadCriteria(StoreRequestParameters param, string departmentID = default(string))
        {
            //int totalCount;
            //if (departmentID.Contains('_'))
            //{
            //    string[] DepartmentIdsl = departmentID.Trim().Split('_');
            //    departmentID = DepartmentIdsl[1];
            //}
            //return this.Store(VPHumanRecruitmentCriteriaService.GetByDepartment(parameters.Page, parameters.Limit, out totalCount, new Guid (departmentID)), totalCount);

            var departmentSplitId = Guid.Empty;
            if (departmentID.Contains('_'))
            {
                departmentSplitId = new Guid(departmentID.Split('_')[1]);
            }
            else
            {
                if (!string.IsNullOrEmpty(departmentID))
                {
                    departmentSplitId = new Guid(departmentID);
                }
            }
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = VPHumanRecruitmentCriteriaService.GetByDepartment(pageIndex, pageSize, out count, departmentSplitId).ToList();
            return this.Store(data, count);
        }
        #region Cập nhật
        public ActionResult Update(Guid? ID)
        {
            var data = new VPHumanRecruitmentCriteriaBO();
            if (ID != null)
            {
                data = VPHumanRecruitmentCriteriaService.getDetail(ID.Value);
                ViewData.Add("Operation", Common.Operation.Update);
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Create);
            }
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data, ViewData = ViewData };
        }
        [HttpPost]
        public ActionResult Update(VPHumanRecruitmentCriteriaBO updateData, string RoleId = default(string))
        {
            if (!String.IsNullOrEmpty(RoleId) && RoleId.Contains('_'))
            {
                string[] DepartmentIdsl = RoleId.Trim().Split('_');
                RoleId = DepartmentIdsl[1];
                updateData.HumanRoleId = new Guid(RoleId);
            }
            var result = false;
            try
            {
                if (updateData.Id == Guid.Empty)
                {
                    VPHumanRecruitmentCriteriaService.Insert(updateData);
                }
                else
                {
                    VPHumanRecruitmentCriteriaService.Update(updateData);
                }
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);

        }
        #endregion
        /// <summary>
        /// Thực hiện vào form chi tiết tiêu chí tuyển dụng
        /// Author: Thanhnv. DateTime: 26/12/2014
        /// </summary>
        /// <param name="ID">Mã tiêu chí</param>
        /// <returns></returns>
        public ActionResult DetailForm(Guid ID)
        {
            var data = VPHumanRecruitmentCriteriaService.GetById(ID);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
        }
        public ActionResult DeleteCriteria(Guid id)
        {
            try
            {
                VPHumanRecruitmentCriteriaService.Delete(id);
            }
            catch
            {
            }
            return this.Direct();
        }
        /// <summary>
        /// kích hoạt hoặc hủy kích hoạt tiêu chí
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public ActionResult Active(Guid ID, bool IsActive)
        {
            try
            {
                VPHumanRecruitmentCriteriaService.Active(ID, IsActive);

            }
            catch
            {

            }
            finally
            {
                X.GetCmp<Store>("StoreCriteria").Reload();
            }
            return this.Direct();
        }
        /// <summary>
        /// Xóa trạng thái bản ghi
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public ActionResult reDeleteCriteria(Guid id)
        {
            try
            {
                VPHumanRecruitmentCriteriaService.IsDelete(id);
            }
            catch
            {
            }
            finally
            {
                X.GetCmp<Store>("StoreCriteria").Reload();
            }
            return this.Direct();
        }
    }
}
