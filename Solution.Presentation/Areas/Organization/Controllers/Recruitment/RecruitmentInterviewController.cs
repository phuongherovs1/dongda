﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class RecruitmentInterviewController : FrontController
    {
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult UpdateForm(Guid ID, Guid PlanID)
        {
            var data = new VPHumanRecruitmentProfileInterviewBO();
            data.Id = ID;
            data.HumanRecruitmentPlanId = PlanID;
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
        }
        /// <summary>
        /// Lấy danh sách hồ sơ phỏng vấn của kế hoạch
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult LoadProfileInterview(StoreRequestParameters parameters, int PlanID = 0, int RequirementID = 0)
        {
            int totalCount;
            //var result = InterviewService.GetByPlanIDAndRequirementID(parameters.Page, parameters.Limit, out totalCount, PlanID, RequirementID);
            //return this.Store(result, totalCount);
            return this.Store(null);
        }
        /// <summary>
        /// lấy kết quả phỏng vấn của hồ sơ
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="ProfileInterviewID"></param>
        /// <returns></returns>
        public ActionResult LoadInterview(StoreRequestParameters parameters, Guid ProfileInterviewID, Guid PlanID)
        {
            int totalCount;
            var result = VPHumanRecruitmentInterviewService.GetByProfieInterviewID(parameters.Page, parameters.Limit, out totalCount, ProfileInterviewID, PlanID);
            return this.Store(result, totalCount);
        }
        /// <summary>
        /// Xóa điểm thi
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        //[BaseSystem(Name = "Xóa")]
        //[SystemAuthorize(CheckAuthorize = true)]
        public ActionResult DeleteInterviewItem(int ID)
        {
            //try
            //{
            //    InterviewService.Delete(ID);
            //    Ultility.CreateNotification(message: RequestMessage.DeleteSuccess);
            //}
            //catch
            //{
            //    Ultility.CreateNotification(message: RequestMessage.DeleteError, error: true);
            //}
            //finally
            //{
            //    X.GetCmp<Store>("StoreInterview").Reload();
            //}
            return this.Direct();
        }
        /// <summary>
        /// Cập nhật điểm thi của các vòng thi
        /// </summary>
        /// <param name="updateData"></param>
        /// <returns></returns>
        public ActionResult UpdateInteview(string InterviewItem)
        {
            try
            {
                var updateData = Ext.Net.JSON.Deserialize<VPHumanRecruitmentInterviewBO>(InterviewItem);
                VPHumanRecruitmentInterviewService.UpdateOrInsert(updateData);

            }
            catch
            {

            }
            return this.Direct();
        }
        #region Chi tiết
        //[BaseSystem(Name = "Xem chi tiết")]
        //[SystemAuthorize(CheckAuthorize = true)]
        public ActionResult DetailForm(int ID, string Name)
        {
            //var data = new HumanRecruitmentPlanItem();
            //data.ID = ID;
            //data.Name = Name;
            //return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail" };
        }
        #endregion
    }
}
