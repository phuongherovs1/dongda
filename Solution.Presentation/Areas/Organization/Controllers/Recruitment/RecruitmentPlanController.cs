﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Linq;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class RecruitmentPlanController : FrontController
    {
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult IndexNew(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }
        public ActionResult LoadPlanApproved(StoreRequestParameters parameters)
        {
            int totalCount;
            var result = VPHumanRecruitmentPlanService.GetAllAprovel(parameters.Page, parameters.Limit, out totalCount);
            return this.Store(result, totalCount);
        }
        public ActionResult LoadPlan(StoreRequestParameters parameters)
        {
            int totalCount;
            var data = VPHumanRecruitmentPlanService.GetData(parameters.Page, parameters.Limit, out totalCount).ToList();
            return this.Store(data, totalCount);
        }
        public ActionResult GetRequirementByplanId(StoreRequestParameters parameters, string PlanID)
        {
            var data = VPHumanRecruitmentRequirementService.GetRequirementByplanId(new Guid(PlanID)).ToList();
            return this.Store(data);
        }

        // Lấy danh sách những yêu cầu tuyển dụng đã được phê duyệt
        public ActionResult LoadRequirementApproved(StoreRequestParameters parameters, string planId)
        {
            int totalCount;
            var data = VPHumanRecruitmentRequirementService.GetRequirementApproved(planId, parameters.Page, parameters.Limit, out totalCount).ToList();
            return this.Store(data, totalCount);
        }
        #region Thêm
        [HttpGet]
        public ActionResult Insert()
        {
            var data = new VPHumanRecruitmentPlanBO();
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Add", Model = data };
        }
        [HttpPost]
        public ActionResult Insert(VPHumanRecruitmentPlanBO AddData, string listRequirementId)
        {
            try
            {
                VPHumanRecruitmentPlanService.Save(AddData, listRequirementId);
            }
            catch
            {
                //Ultility.CreateNotification(message: RequestMessage.UpdateError, error: true);
            }
            finally
            {
                X.GetCmp<Window>("winPlanAdd").Close();
                X.GetCmp<Store>("StorePlan").Reload();
            }
            return this.Direct();
        }
        #endregion

        #region Sửa
        [HttpGet]
        public ActionResult Update(Guid ID)
        {

            if (ID == Guid.Empty)
                return this.Direct();
            var data = VPHumanRecruitmentPlanService.GetById(ID);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Add", Model = data };

        }
        [HttpPost]
        public ActionResult Update(VPHumanRecruitmentPlanBO updateData)
        {
            try
            {
                VPHumanRecruitmentPlanService.Update(updateData);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                X.GetCmp<Store>("StorePlan").Reload();
            }
            return this.Direct();
        }

        public ActionResult SelectRequirement(Guid planId, Guid requirementId)
        {
            try
            {
                VPHumanRecruitmentPlanRequirementService.Insert(planId, requirementId);
            }
            catch (Exception)
            {
                throw;
            }
            return this.Direct();
        }
        public ActionResult UnselectRequirement(Guid planId, Guid requirementId)
        {
            try
            {
                VPHumanRecruitmentPlanRequirementService.Delete(planId, requirementId);
            }
            catch (Exception)
            {
                throw;
            }
            return this.Direct();
        }


        //Lấy danh sách yêu cầu của kế hoạch
        public ActionResult LoadRequirementUpdate(StoreRequestParameters parameters, Guid Id)
        {
            //int totalCount;
            //var Data = PlanService.GetRequirementSelect(parameters.Page, parameters.Limit, out totalCount, Id);
            //return this.Store(Data, totalCount);
            return this.Store(null);
        }
        // Cập nhật yêu cầu tuyển dụng
        public ActionResult UpdateRequirement(string data)
        {
            var PlanRequirementdata = Ext.Net.JSON.Deserialize<VPHumanRecruitmentPlanRequirementBO>(data);
            try
            {
                //PlanService.UpdateRequired(PlanRequirementdata);
            }
            catch
            {
                //Ultility.CreateNotification(message: RequestMessage.UpdateError, error: true);
            }
            finally
            {
                X.GetCmp<Store>("StoreRequirement").Reload();
            }
            return this.Direct();
        }
        #endregion

        #region Xóa
        /// <summary>
        /// Xóa kế hoạch
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DeletePlan(Guid id)
        {
            try
            {
                VPHumanRecruitmentPlanService.Delete(id);
                //Ultility.CreateNotification(message: RequestMessage.DeleteSuccess);
            }
            catch
            {
                //Ultility.CreateNotification(message: RequestMessage.DeleteError, error: true);
            }
            finally
            {
                X.GetCmp<Store>("StorePlan").Reload();
            }
            return this.Direct();
        }
        #endregion

        #region Chi tiết

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult LoadRequirementDetail(StoreRequestParameters parameters, Guid Id)
        {
            int totalCount;
            var data = VPHumanRecruitmentPlanRequirementService.GetByPlanId(Id, parameters.Page, parameters.Limit, out totalCount);
            return this.Store(data, totalCount);

        }
        #endregion

        #region Gửi duyệt
        [HttpGet]
        public ActionResult SendApprove(Guid ID)
        {
            var data = new VPHumanRecruitmentPlanBO();
            if (ID != Guid.Empty)
            {
                data = VPHumanRecruitmentPlanService.GetById(ID);
            }
            if (data.IsEdit != false)
            {
                return new Ext.Net.MVC.PartialViewResult { ViewName = "SendApprove", Model = data };
            }
            return this.Direct();
        }
        [HttpPost]
        public ActionResult SendApprove(VPHumanRecruitmentPlanBO updateData, bool IsEdit = false)
        {
            bool success = false;
            try
            {
                if (updateData.Id != Guid.Empty)
                {
                    VPHumanRecruitmentPlanService.SendApprove(updateData);
                    Notify(TitleResourceNotifies.SendApprovalRecruitmentPlan, updateData.Content, updateData.EmployeeApproveBy.Id, UrlResourceNotifies.ApprovalRecruitmentPlan, new { id = updateData.Id }, 1);
                    success = true;
                }
            }
            catch
            {
            }
            finally
            {
                X.GetCmp<Store>("StorePlan").Reload();
            }
            return this.FormPanel(success);
        }
        #endregion

        #region Phê duyệt
        [HttpGet]
        public ActionResult Approve(Guid ID)
        {
            var data = new VPHumanRecruitmentPlanBO();
            if (ID != Guid.Empty)
            {
                data = VPHumanRecruitmentPlanService.GetById(ID);
            }
            if (data.IsEdit != true)
            {
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Approve", Model = data };
            }
            return this.Direct();
        }
        [HttpPost]
        public ActionResult Approve(VPHumanRecruitmentPlanBO updateData)
        {
            bool success = false;
            try
            {
                VPHumanRecruitmentPlanService.Approve(updateData);
                success = true;
            }
            catch
            {

            }
            finally
            {
                X.GetCmp<Window>("WinApprove").Close();
                X.GetCmp<Store>("StorePlan").Reload();
            }
            return this.FormPanel(success);
        }
        #endregion

        #region Giao việc
        public ActionResult TaskForm(int ID)
        {
            ViewData["PlanID"] = ID;
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Task", ViewData = ViewData };
        }
        /// <summary>
        /// Thêm mới công việc kế hoạch
        /// </summary>
        /// <param name="objNew"></param>
        /// <param name="refID"></param>
        /// <returns></returns>
        //public ActionResult InsertTask(VPHumanRecruitmentPlanTaskBO objNew, int planId, string arrObject = null)
        //{
        //    bool success = false;
        //    try
        //    {

        //        if (objNew.ParentID != 0 && taskSV.GetByID(objNew.ParentID).IsPause)
        //        {
        //            X.Msg.Show(new MessageBoxConfig
        //            {
        //                Buttons = MessageBox.Button.OK,
        //                Icon = MessageBox.Icon.WARNING,
        //                Title = "Thông báo",
        //                Message = "Công việc đã tạm dừng không được phép thêm công việc con!"
        //            });
        //            return this.Direct();
        //        }
        //        if (taskSV.CheckNameTaskExist(objNew.Name.Trim()))
        //        {
        //            X.Msg.Show(new MessageBoxConfig
        //            {
        //                Buttons = MessageBox.Button.OK,
        //                Icon = MessageBox.Icon.WARNING,
        //                Title = "Thông báo",
        //                Message = "Tên công việc đã tồn tại vui lòng nhập tên khác!"
        //            });
        //            return this.Direct();
        //        }
        //        else
        //        {
        //            string nameOld = objNew.Name;
        //            if (!string.IsNullOrEmpty(arrObject))
        //            {
        //                objNew.Perform = employeeSV.GetEmployeeView(objNew.Perform.ID);
        //                objNew.Name = nameOld + " (" + objNew.Perform.Name + ")";
        //            }
        //            objNew.CreateBy = User.ID;
        //            int taskId = taskSV.Insert(objNew, User.ID, User.EmployeeID);
        //            objNew.TaskID = taskId;
        //            objNew.PlanID = planId;
        //            PlanService.InsertTask(objNew, User.ID);
        //            if (!objNew.IsEdit && !objNew.IsNew)
        //            {
        //                NotifyController notify = new NotifyController();
        //                notify.Notify(this.ModuleCode, "Yêu cầu thực hiện công việc", objNew.Name, objNew.Perform.ID, User, Common.Task, "RecuitmentPlanTaskID:" + taskId.ToString());
        //            }
        //            if (!string.IsNullOrEmpty(arrObject))
        //            {
        //                var ids = arrObject.Split(',').Select(n => (object)int.Parse(n)).ToList();
        //                ids.Remove(objNew.Perform.ID);
        //                foreach (var ide in ids)
        //                {
        //                    objNew.Perform = employeeSV.GetEmployeeView((int)ide);
        //                    objNew.Name = nameOld + " (" + objNew.Perform.Name + ")";
        //                    int taskIds = taskSV.Insert(objNew, User.ID, User.EmployeeID);
        //                    if (!objNew.IsEdit && !objNew.IsNew)
        //                    {
        //                        NotifyController notify = new NotifyController();
        //                        notify.Notify(this.ModuleCode, "Yêu cầu thực hiện công việc", objNew.Name, objNew.Perform.ID, User, Common.Task, "RecuitmentPlanTaskID:" + taskIds.ToString());
        //                    }
        //                    objNew.TaskID = taskIds;
        //                    objNew.PlanID = planId;
        //                    PlanService.InsertTask(objNew, User.ID);
        //                }
        //            }
        //            Ultility.ShowNotification(message: RequestMessage.CreateSuccess);
        //            success = true;
        //        }
        //    }
        //    catch
        //    {
        //        Ultility.ShowMessageBox(message: RequestMessage.CreateError, icon: MessageBox.Icon.ERROR);
        //    }
        //    return this.FormPanel(success);
        //}

        //private Node createNodeTask(TaskItem dataNode, int planId)
        //{
        //    Node nodeItem = new Node();
        //    nodeItem.NodeID = dataNode.ID.ToString();
        //    //if (dataNode. != planId)
        //    //{
        //    //    nodeItem.Cls = "clsUnView";
        //    //}
        //    nodeItem.Text = !dataNode.Leaf ? dataNode.Name.ToUpper() : dataNode.Name;
        //    nodeItem.Icon = !dataNode.Leaf ? Icon.Folder : Icon.TagBlue;
        //    nodeItem.CustomAttributes.Add(new ConfigItem { Name = "ParentID", Value = dataNode.ParentID.ToString(), Mode = ParameterMode.Value });
        //    nodeItem.CustomAttributes.Add(new ConfigItem { Name = "Rate", Value = JSON.Serialize(dataNode.Rate), Mode = ParameterMode.Value });
        //    nodeItem.CustomAttributes.Add(new ConfigItem { Name = "IsNew", Value = JSON.Serialize(dataNode.IsNew), Mode = ParameterMode.Value });
        //    nodeItem.CustomAttributes.Add(new ConfigItem { Name = "IsEdit", Value = JSON.Serialize(dataNode.IsEdit), Mode = ParameterMode.Value });
        //    nodeItem.CustomAttributes.Add(new ConfigItem { Name = "IsComplete", Value = JSON.Serialize(dataNode.IsComplete), Mode = ParameterMode.Value });
        //    nodeItem.CustomAttributes.Add(new ConfigItem { Name = "IsPause", Value = JSON.Serialize(dataNode.IsPause), Mode = ParameterMode.Value });
        //    nodeItem.CustomAttributes.Add(new ConfigItem { Name = "IsPause", Value = JSON.Serialize(dataNode.IsPause), Mode = ParameterMode.Value });
        //    nodeItem.CustomAttributes.Add(new ConfigItem { Name = "CategoryName", Value = dataNode.CategoryName.ToString(), Mode = ParameterMode.Value });
        //    nodeItem.CustomAttributes.Add(new ConfigItem { Name = "Status", Value = dataNode.Status.ToString(), Mode = ParameterMode.Value });
        //    nodeItem.CustomAttributes.Add(new ConfigItem { Name = "LevelName", Value = dataNode.LevelName.ToString(), Mode = ParameterMode.Value });
        //    nodeItem.CustomAttributes.Add(new ConfigItem { Name = "LevelColor", Value = dataNode.LevelColor.ToString(), Mode = ParameterMode.Value });
        //    nodeItem.CustomAttributes.Add(new ConfigItem { Name = "StartTime", Value = dataNode.StartTime.ToString("dd/MM/yyyy HH:mm"), Mode = ParameterMode.Value });
        //    nodeItem.CustomAttributes.Add(new ConfigItem { Name = "EndTime", Value = dataNode.EndTime.ToString("dd/MM/yyyy HH:mm"), Mode = ParameterMode.Value });
        //    nodeItem.Leaf = dataNode.Leaf;
        //    return nodeItem;
        //}
        //public ActionResult LoadPlanTask(string node, int planId = 0)
        //{
        //    try
        //    {
        //        NodeCollection nodes = new NodeCollection();
        //        var taskId = node == "root" ? 0 : System.Convert.ToInt32(node);
        //        var tasks = PlanService.GetTreeTask(taskId, planId);
        //        foreach (var task in tasks)
        //        {
        //            nodes.Add(createNodeTask(task, planId));
        //        }
        //        return this.Content(nodes.ToJson());
        //    }
        //    catch
        //    {
        //        Ultility.ShowMessageRequest(RequestStatus.Error);
        //    }
        //    return this.Direct();
        //}
        #endregion
    }
}
