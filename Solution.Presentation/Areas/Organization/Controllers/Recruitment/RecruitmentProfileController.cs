﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class RecruitmentProfileController : FrontController
    {
        public ActionResult Index()
        {
            VPHumanRecruitmentProfileBO model = new VPHumanRecruitmentProfileBO();
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                model.IsRoleEdit = RoleEdit == "1" ? true : false;
            else
                model.IsRoleEdit = false;
            return View(model);
        }
        public ActionResult IndexNew(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }
        public ActionResult LoadProfile(StoreRequestParameters parameters)
        {
            int totalCount;
            var result = VPHumanRecruitmentProfileService.GetAll(parameters.Page, parameters.Limit, out totalCount);
            return this.Store(result, totalCount);
        }

        public ActionResult GetAllProfileInterview(StoreRequestParameters parameters, string PlanId, string filterName = default(string))
        {
            int totalCount;
            var result = VPHumanRecruitmentProfileService.GetAllProfileInterview(parameters.Page, parameters.Limit, out totalCount, new Guid(PlanId), filterName);
            return this.Store(result, totalCount);
        }



        public ActionResult LoadProfileInterview(StoreRequestParameters parameters, string PlanID)
        {
            int totalCount;
            var result = VPHumanRecruitmentProfileService.LoadProfileInterviewByPlan(parameters.Page, parameters.Limit, out totalCount, new Guid(PlanID));
            return this.Store(result, totalCount);
        }
        #region Cập nhật hồ sơ ứng viên
        public ActionResult UpdateForm(Guid? ID)
        {
            var data = new VPHumanRecruitmentProfileBO();

            if (ID != null)
            {
                data = VPHumanRecruitmentProfileService.GetById(ID.Value);
                List<Guid> fileID = new List<Guid>();
                if (data.FileId.HasValue)
                {
                    fileID.Add(data.FileId.Value);
                    data.FileAttachs = new FileUploadBO()
                    {
                        Files = fileID
                    };
                }
                data.FileAvatar.Data = data.Avatar;
                ViewData.Add("Operation", Common.Operation.Update);
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Create);
            }
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data, ViewData = ViewData };
        }

        /// <summary>
        /// Thêm mới dữ liệu
        /// </summary>
        /// <param name="recruitmentProfile"></param>
        /// <returns></returns>
        public ActionResult Insert(VPHumanRecruitmentProfileBO recruitmentProfile)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    if (recruitmentProfile.Id == Guid.Empty)
                    {
                        recruitmentProfile.Avatar = recruitmentProfile.FileAvatar.Data;
                        VPHumanRecruitmentProfileService.Insert(recruitmentProfile);
                    }
                    else
                    {
                        recruitmentProfile.Avatar = recruitmentProfile.FileAvatar.Data;
                        VPHumanRecruitmentProfileService.Update(recruitmentProfile);
                    }
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }

                finally
                {

                    X.GetCmp<Window>("winRecruitmentProfile").Close();
                    X.GetCmp<Store>("StoreRecruitmentProfile").Reload();
                }
            }
            return this.Direct(result: result);
        }


        #endregion

        #region Xóa
        /// <summary>
        /// Thực hiện xóa hồ sơ
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public ActionResult DeleteProfile(Guid ID)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    VPHumanRecruitmentProfileService.Delete(ID);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }

                finally
                {

                    X.GetCmp<Store>("StoreRecruitmentProfile").Reload();
                }
            }
            return this.Direct(result: result);

        }
        /// <summary>
        /// Xóa trạng thái bản ghi
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public ActionResult reDeleteProfile(int id)
        {
            //try
            //{
            //    RecruitmentProfileService.IsDelete(id);
            //    Ultility.CreateNotification(message: RequestMessage.DeleteSuccess);
            //}
            //catch
            //{
            //    Ultility.CreateNotification(message: RequestMessage.DeleteError, error: true);
            //}
            //finally
            //{
            //    X.GetCmp<Store>("StoreProfile").Reload();
            //}
            return this.Direct();
        }

        #endregion

        #region Chi tiết
        public ActionResult DetailForm(int ID)
        {
            //var data = RecruitmentProfileService.GetById(ID);
            //return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
            return new Ext.Net.MVC.PartialViewResult { };
        }
        #endregion
    }
}