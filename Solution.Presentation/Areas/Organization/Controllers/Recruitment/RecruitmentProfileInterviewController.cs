﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class RecruitmentProfileInterviewController : FrontController
    {
        public ActionResult Index()
        {
            VPHumanRecruitmentPlanBO model = new VPHumanRecruitmentPlanBO();
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                model.IsRoleEdit = RoleEdit == "1" ? true : false;
            else
                model.IsRoleEdit = false;
            return View(model);
        }
        public ActionResult IndexNew(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }
        public ActionResult CheckInsert(Guid? ProfileId, Guid? PlanId, Guid? RequirementId)
        {
            var result = "";
            var itemExsit = VPHumanRecruitmentProfileInterviewService.Get(x => x.HumanRecruitmentPlanId == PlanId && x.HumanRecruitmentProfileId == ProfileId);
            if (itemExsit != null && itemExsit.ToList().Count > 0)
                result = itemExsit.ToList()[0].Id.ToString();
            return this.Direct(result: result);
        }
        public ActionResult Insert(Guid? ProfileId, Guid? PlanId, Guid? RequirementId)
        {
            var item = new VPHumanRecruitmentProfileInterviewBO()
            {
                HumanRecruitmentPlanId = PlanId,
                HumanRecruitmentProfileId = ProfileId,
                HumanRecruitmentRequirementId = RequirementId
            };

            var result = false;
            try
            {
                VPHumanRecruitmentProfileInterviewService.Insert(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileInterview").Reload();
            }
            return this.Direct(result: result);

        }
        public ActionResult UpdateMSg(Guid? ProfileId, Guid? PlanId, Guid? RequirementId, Guid Id)
        {
            var item = new VPHumanRecruitmentProfileInterviewBO()
            {
                Id = Id,
                HumanRecruitmentPlanId = PlanId,
                HumanRecruitmentProfileId = ProfileId,
                HumanRecruitmentRequirementId = RequirementId
            };

            var result = false;
            try
            {
                VPHumanRecruitmentProfileInterviewService.Update(item);
                this.ShowNotify(Common.Resource.SystemSuccess);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileInterview").Reload();
            }
            return this.Direct(result: result);
        }
        #region Thêm
        public ActionResult AddForm(Guid ID)
        {
            var data = new VPHumanRecruitmentProfileBO();
            data.HumanRecruitmentPlanId = ID;

            var ProfileIds = VPHumanRecruitmentProfileInterviewService.Get(m => m.HumanRecruitmentPlanId == ID).Select(i => i.HumanRecruitmentProfileId);
            var Profiles = VPHumanRecruitmentProfileService.Get(i => ProfileIds.Contains(i.Id)).OrderByDescending(i => i.CreateAt);
            Dictionary<Guid, string> lstUV = new Dictionary<Guid, string>();
            foreach (VPHumanRecruitmentProfileBO item in Profiles)
                lstUV.Add(item.Id, item.Name);
            ViewData["lstUV"] = lstUV;
            var titleIds = DepartmentTitleService.GetQuery().Select(i => i.Id);
            var RecruitmentRequirement = VPHumanRecruitmentRequirementService.Get(i => i.HumanRoleId.HasValue && titleIds.Contains(i.HumanRoleId.Value)).OrderByDescending(i => i.CreateAt);
            Dictionary<Guid, string> lstYC = new Dictionary<Guid, string>();
            foreach (VPHumanRecruitmentRequirementBO item in RecruitmentRequirement)
                lstYC.Add(item.Id, item.Name);
            ViewData["lstYC"] = lstYC;
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Add", Model = data, ViewData = ViewData };
        }
        /// <summary>
        /// Lấy danh sách yêu cầu theo mã kế hoạch
        /// </summary>
        /// <param name="PlanID"></param>
        /// <returns></returns>
        public ActionResult GetRequirement(int PlanID = 0)
        {
            var Requirements = new List<VPHumanRecruitmentPlanRequirementBO>();
            //if (PlanID != 0)
            //{
            //    Requirements = recruitmentProfileInterviewService.GetRequirementByPlan(PlanID);
            //}
            return this.Store(Requirements);
        }
        /// <summary>
        /// Lấy danh sách hồ sơ
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public ActionResult LoadProfile(StoreRequestParameters parameters, Guid PlanID)
        {
            if (PlanID != Guid.Empty)
            {
                int totalCount;
                //var result = recruitmentProfileInterviewService.GetAllNotIsEmpByPlanID(parameters.Page, parameters.Limit, out totalCount, PlanID);
                //return this.Store(result, totalCount);
                return this.Store(null);
            }
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult UpdateProfileInterView(string data, Guid PlanID)
        {
            //var ProfileInterviewdata = Ext.Net.JSON.Deserialize<RecruitmentProfileIneterviewSelectItem>(data);
            //try
            //{
            //    if (ProfileInterviewdata.IsSelect == true)
            //    {
            //        var ProfileInterviewtem = new HumanRecruitmentProfileInterviewItem();
            //        if (ProfileInterviewdata.RequirementID == null)
            //        {

            //            ProfileInterviewtem.ProfileID = ProfileInterviewdata.ProfileID;
            //            ProfileInterviewtem.PlanID = PlanID;
            //            ProfileInterviewtem.RequirementID = 0;
            //            recruitmentProfileInterviewService.InsertOrDefaulRequirment(ProfileInterviewtem, User.ID, PlanID);
            //        }
            //        else
            //        {
            //            ProfileInterviewtem.ProfileID = ProfileInterviewdata.ProfileID;
            //            ProfileInterviewtem.RequirementID = (int)ProfileInterviewdata.RequirementID;
            //            recruitmentProfileInterviewService.UpdateRequirement(ProfileInterviewtem.ProfileID, ProfileInterviewtem.RequirementID);
            //        }
            //    }
            //    else
            //    {
            //        if (ProfileInterviewdata.ID != null)
            //        {
            //            recruitmentProfileInterviewService.Delete((int)ProfileInterviewdata.ID);
            //        }
            //        else
            //        {
            //            var ProfileInterviewtem = new HumanRecruitmentProfileInterviewItem();
            //            ProfileInterviewtem.ProfileID = ProfileInterviewdata.ProfileID;
            //            ProfileInterviewtem.PlanID = PlanID;
            //            ProfileInterviewtem.RequirementID = (int)ProfileInterviewdata.RequirementID;
            //            recruitmentProfileInterviewService.InsertOrDefaulRequirment(ProfileInterviewtem, User.ID, PlanID);
            //        }
            //    }
            //}
            //catch
            //{
            //    Ultility.CreateNotification(message: RequestMessage.UpdateError, error: true);
            //}
            //finally
            //{
            //    X.GetCmp<Store>("ProfileAddStore").Reload();
            //    X.GetCmp<Store>("StoreProfile").Reload();
            //}
            return this.Direct();
        }
        #endregion



        #region Chi tiết
        public ActionResult DetailForm(Guid ProfileInterviewID)
        {
            //var data = new HumanRecruitmentResultItem();
            //recruitmentProfileInterviewService.GetResultByProfileInterViewID(ProfileInterviewID, out data);
            //return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail" };
        }

        /// <summary>
        /// hiển thị chi tiết cho kết quả tuyển dụng 
        /// </summary>
        /// <param name="ProfileResultID"></param>
        /// <returns></returns>
        public ActionResult DetailResultForm(Guid ProfileResultID)
        {
            var data = VPHumanRecruitmentProfileResultService.GetDetailResultFormItem(ProfileResultID);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
        }

        /// <summary>
        /// Lấy danh sách hồ sơ phỏng vấn của kế hoạch
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult LoadProfileInterview(StoreRequestParameters parameters, Guid PlanID, Guid focusId)
        {
            //ModelFilter filter = ExtExtend.ExtExtendFilter.ConvertToFilter(parameters);
            //var result = recruitmentProfileInterviewService.GetProfileByPlan(filter, focusId, PlanID);
            //return this.Store(result, filter.PageTotal);
            return this.Store(null);
        }
        #endregion

        #region Phê duyệt
        /// <summary>
        /// Trỏ đến form  gửi duyệt
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult SendApproveForm(Guid ProfileInterviewID)
        {
            var data = VPHumanRecruitmentProfileInterviewService.GetResultByProfileInterViewID(ProfileInterviewID);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "SendApprove", Model = data };
            //return new Ext.Net.MVC.PartialViewResult { ViewName = "SendApprove" };
        }

        public ActionResult InsertProfileResult(VPHumanRecruitmentProfileResultBO ProfileResult)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {

                    var item = new VPHumanRecruitmentProfileResultBO()
                    {
                        ApprovalBy = ProfileResult.EmployeeApproveBy.Id,
                        ApprovalAt = DateTime.Now,
                        HumanRecruitmentProfileInterviewId = ProfileResult.HumanRecruitmentProfileInterviewId,
                        TotalPoint = ProfileResult.TotalPoint,
                        StartTime = ProfileResult.StartTime,
                        Salary = ProfileResult.Salary
                    };
                    VPHumanRecruitmentProfileInterviewService.InsertProfileResult(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }

                finally
                {

                    X.GetCmp<Window>("winSendApprove").Close();
                    X.GetCmp<Store>("StoreProfileInterview").Reload();
                }
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// Trỏ đến form phê duyệt
        /// </summary>
        /// <param name="ProfileInterviewID"></param>
        /// <param name="RequirementID"></param>
        /// <returns></returns>
        public ActionResult ApproveForm(Guid ProfileResultID)
        {
            var model = VPHumanRecruitmentProfileResultService.GetDetailResultFormItem(ProfileResultID);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Approve", Model = model };
        }

        public ActionResult Approve(VPHumanRecruitmentProfileResultBO data)
        {
            try
            {
                VPHumanRecruitmentProfileResultService.Approve(data);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                {

                }
                else
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                    //Common.WriteLog.WriteLogEntry("RecruitmentProfileInterviewController-Approve", ex);
                }
            }
            return this.Direct();
        }


        /// <summary>
        /// Xử lý thông tin gửi duyệt
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="exit"></param>
        /// <returns></returns>
        //public ActionResult SendApprove(VpHumanRecruitmentResultBo updateData)
        //{
        //    bool success = false;
        //    try
        //    {
        //        if (updateData.TotalPoint == null)
        //        {
        //            Ultility.ShowMessageBox(title: RequestMessage.Notify, message: RequestMessage.NotReview);
        //            return this.Direct();
        //        }
        //        recruitmentProfileInterviewService.SendProfile(updateData, User.ID);
        //        Ultility.CreateNotification(message: RequestMessage.SendSuccess);
        //        NotifyController notify = new NotifyController();
        //        notify.Notify(this.ModuleCode, "Có một hồ sơ ứng tuyển cần phê duyệt", updateData.CreateByName, updateData.Approval.ID, User, Common.RecruitmentProfileInterview, "RecruitmentProfileInterviewID:" + updateData.ID.ToString());

        //        success = true;
        //    }
        //    catch
        //    {
        //        Ultility.CreateNotification(message: RequestMessage.SendError, error: true);
        //    }
        //    finally
        //    {
        //        X.GetCmp<Store>("StoreProfile").Reload();
        //    }
        //    return this.FormPanel(success);
        //}
        /// <summary>
        /// Thực hiện phê duyệt kế hoạch tuyển dụng
        /// </summary>
        /// <param name="updateData"></param>
        /// <returns></returns>
        //public ActionResult ApproveReview(VPHumanRecruitmentResultBO updateData)
        //{
        //    bool success = false;
        //    try
        //    {
        //        if (updateData.ID != 0)
        //        {
        //            updateData.IsAccept = updateData.IsResult == true ? true : false;
        //            updateData.IsApproval = true;
        //            recruitmentProfileInterviewService.UpdateResult(updateData, User.ID);
        //            NotifyController notify = new NotifyController();
        //            notify.Notify(this.ModuleCode, "Hồ sơ ứng tuyển của bạn đã được phê duyệt", updateData.CreateByName, updateData.CreateUserID, User, Common.RecruitmentProfileInterview, "RecruitmentProfileInterviewID:" + updateData.ID.ToString());
        //            Ultility.CreateNotification(message: RequestMessage.ApproveSuccess);
        //            success = true;
        //        }
        //    }
        //    catch
        //    {
        //        Ultility.CreateNotification(message: RequestMessage.ApproveError, error: true);
        //    }
        //    finally
        //    {
        //        X.GetCmp<Store>("StoreProfileResult").Reload();

        //    }
        //    return this.FormPanel(success);
        //}
        #endregion


    }
}
