﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class RecruitmentReviewController : FrontController
    {
        public ActionResult Index()
        {
            return View();
        }
        #region Thêm
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult UpdateForm(Guid ID, Guid RequirementID)
        {
            var data = new VPHumanRecruitmentProfileInterviewBO();
            data.Id = ID;
            data.HumanRecruitmentRequirementId = RequirementID;
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
        }
        /// <summary>
        /// Lấy danh sách hồ sơ xét tuyển của kế hoạch
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult LoadProfileInterview(StoreRequestParameters parameters, int PlanID = 0, int RequirementID = 0)
        {
            //int totalCount;
            //var result = ReviewService.GetByPlanIDAndRequirementID(parameters.Page, parameters.Limit, out totalCount, PlanID, RequirementID);
            //return this.Store(result, totalCount);
            return this.Store(null);
        }

        public ActionResult GetCriteria(StoreRequestParameters parameters, Guid profileInterviewID, Guid RequirementID)
        {
            int totalCount;
            var result = VPHumanRecruitmentCriteriaService.GetCriteria(parameters.Page, parameters.Limit, out totalCount, profileInterviewID, RequirementID);
            return this.Store(result, totalCount);
        }


        /// <summary>
        /// Cập nhật kết quả cho các tiêu chí
        /// </summary>
        /// <param name="updateData"></param>
        /// <returns></returns>
        public ActionResult UpdateReview(string ReviewItem)
        {
            try
            {
                var updateData = Ext.Net.JSON.Deserialize<VPHumanRecruitmentReviewBO>(ReviewItem);
                VPHumanRecruitmentReviewService.UpdateOrInsert(updateData);

            }
            catch
            {

            }
            return this.Direct();
        }
        #endregion



    }
}
