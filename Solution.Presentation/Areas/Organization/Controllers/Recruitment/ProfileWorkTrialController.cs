﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class ProfileWorkTrialController : FrontController
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexNew(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }
        public ActionResult Employee(string Id)
        {
            ViewData["EmployeeId"] = Id;
            return View();
        }
        public ActionResult GetDataEmployee(StoreRequestParameters parameters, Guid empID)
        {
            //ModelFilter filter = ExtExtend.ExtExtendFilter.ConvertToFilter(parameters);
            var pageIndex = parameters.Page;
            var pageSize = parameters.Limit;
            var count = 0;
            var data = VPHumanProfileWorkTrialService.GetByEmployee(pageIndex, pageSize, out count, empID);
            //var result =  .GetByEmployee(filter, empID,0);
            return this.Store(data, count);
            //return this.Store(null);
        }

        public ActionResult GetDataEmployeeNotPaging(Guid empID)
        {
            var data = VPHumanProfileWorkTrialService.GetByEmployeeNotPaging(empID);
            return this.Store(data);
        }

        public ActionResult LoadList(StoreRequestParameters parameters)
        {
            var pageIndex = parameters.Page;
            var pageSize = 20;
            var count = 0;
            var data = VPHumanProfileWorkTrialService.GetAll(pageIndex, pageSize, out count);
            return this.Store(data, count);
        }
        public ActionResult GetQualityCriteria(StoreRequestParameters parameters, string categoryID)
        {
            //List<QualityCriteriaItem> lstData = new List<QualityCriteriaItem>();
            //int total;
            //if (!string.IsNullOrEmpty(categoryID))
            //{
            //    lstData = new QualityCriteriaSV().GetCriteriaUsedByCateIds(parameters.Page, parameters.Limit, out total, categoryID);
            //}
            //return this.Store(lstData, lstData.Count);
            return this.Store(null);
        }
        public ActionResult TaskForm(int id)
        {
            var data = new VPHumanProfileWorkTrialBO();
            //data = ptwSV.getByID(id);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "TaskEmployee", Model = data };
        }
        public ActionResult ListComentForm(int id)
        {
            //var data = new TaskItem();
            //data.ID = id;
            //return new Ext.Net.MVC.PartialViewResult { ViewName = "TaskComent", Model = data };
            return new Ext.Net.MVC.PartialViewResult { ViewName = "TaskComent" };
        }
        public ActionResult LoadListComent(StoreRequestParameters parameters, int ID)
        {
            //ModelFilter filter = ExtExtend.ExtExtendFilter.ConvertToFilter(parameters);
            //var result = cmSV.GetAll(filter, ID, User.ID);
            //return this.Store(result, filter.PageTotal);
            return this.Store(null);
        }
        public ActionResult UpdateCommentForm(int ID, string action, int TaskId)
        {
            //var data = new TaskCommentItem();
            //if (ID != 0)
            //{
            //    data = new TaskCommentSV().GetById(ID);
            //}
            //else
            //{
            //    data.TaskID = TaskId;
            //    data.EmployeeID = User.EmployeeID;
            //    data.Employee.ID = User.EmployeeID;
            //};
            //data.ActionForm = action;
            //return new Ext.Net.MVC.PartialViewResult { ViewName = "UpdateTaskComent", Model = data };
            return new Ext.Net.MVC.PartialViewResult { ViewName = "UpdateTaskComent" };
        }
        //public ActionResult UpdateComment(TaskCommentItem updateData)
        //{
        //    try
        //    {
        //        if (updateData.ID != 0)
        //        {
        //            cmSV.Update(updateData, User.ID);
        //            Ultility.CreateNotification(message: RequestMessage.UpdateSuccess);
        //        }
        //        else
        //        {
        //            cmSV.Insert(updateData, User.ID);
        //            Ultility.CreateNotification(message: RequestMessage.CreateSuccess);
        //        }
        //        X.GetCmp<Store>("PlanInterviewStore").Reload();
        //    }
        //    catch
        //    {
        //        Ultility.CreateNotification(message: RequestMessage.UpdateError, error: true);
        //    }
        //    finally
        //    {
        //        X.GetCmp<Window>("winInterviewUpdate").Close();
        //    }
        //    return this.Direct();
        //}
        //public ActionResult DeleteComent(int id)
        //{
        //    try
        //    {

        //        cmSV.Delete(id);
        //        Ultility.CreateNotification(message: RequestMessage.DeleteSuccess);
        //    }
        //    catch
        //    {
        //        Ultility.CreateNotification(message: RequestMessage.DeleteError, error: true);
        //    }
        //    finally
        //    {
        //        X.GetCmp<Store>("PlanInterviewStore").Reload();
        //    }
        //    return this.Direct();
        //}

        /// <summary>
        /// Xóa hồ sơ nhân sự thử việc
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteProfile(Guid id)
        {
            try
            {
                VPHumanProfileWorkTrialService.Delete(id);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }
        //[BaseSystem(Name = "Cập nhật", IsActive = true, IsShow = true)]
        //[SystemAuthorize(CheckAuthorize = true)]
        public ActionResult UpdateForm(string Id, string action)
        {
            var data = new VPHumanProfileWorkTrialBO();
            ViewData["ActionForm"] = action;
            data = VPHumanProfileWorkTrialService.GetDetail(Id);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data, ViewData = ViewData };
        }
        public ActionResult Update(VPHumanProfileWorkTrialBO updateData, string manegeId, string roleId)
        {
            try
            {
                VPHumanProfileWorkTrialService.Create(updateData, manegeId, roleId);
                var employeename = EmployeeService.GetById(updateData.HumanEmployeeId.Value).Name;
                Notify(TitleResourceNotifies.SendProfileWorkTrial + " \"" + employeename + "\"", updateData.Note, updateData.ManagerId.Value, UrlResourceNotifies.SendProfileWorkTrial, new { id = updateData.Id });
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }
        /// <summary>
        /// Form tạo yêu cầu tự đánh giá
        /// </summary>
        /// <param name="humanProfileWorkTrialId"></param>
        /// <returns></returns>
        public ActionResult SendTickForm(Guid humanProfileWorkTrialId)
        {
            var data = new VPHumanProfileWorkTrialResultBO();
            data = VPHumanProfileWorkTrialResultService.CreateDefault(humanProfileWorkTrialId);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "SelectCriteria", Model = data };
        }

        public ActionResult SaveCriteria(string catID, int humanProfileWorkTrialID)
        {
            //try
            //{
            //    rsSV.InsertListCri(catID, humanProfileWorkTrialID, User.ID);
            //    Ultility.CreateNotification(message: RequestMessage.CreateSuccess);
            //    NotifyController notify = new NotifyController();
            //    var data = new HumanProfileWorkTrialItem();
            //    if (humanProfileWorkTrialID != 0)
            //    {
            //        data = ptwSV.getByID(humanProfileWorkTrialID);
            //    }
            //    notify.Notify(this.ModuleCode, "Bạn có một phiếu đánh giá cá nhân cần thực hiện", User.Name, data.HumanEmployeeID, User, Common.ProfileWorkTrialEmployee, "HumanProfileWorkTrialID:" + humanProfileWorkTrialID.ToString());
            //    notify.Notify(this.ModuleCode, "Bạn có một phiếu đánh giá nhân sự cần thực hiện", User.Name, data.ManagerID.Value, User, Common.ProfileWorkTrialManager, "HumanProfileWorkTrialID:" + humanProfileWorkTrialID.ToString());
            //}
            //catch
            //{
            //    Ultility.CreateNotification(message: RequestMessage.CreateSuccess, error: true);
            //}
            //finally
            //{
            //    //X.GetCmp<Store>("StorePlanIndex").Reload();
            //    X.GetCmp<Window>("frmUpdate").Close();
            //}
            return this.Direct();
        }
    }
}
