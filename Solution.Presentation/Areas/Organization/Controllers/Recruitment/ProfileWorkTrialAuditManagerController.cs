﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class ProfileWorkTrialAuditManagerController : FrontController
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexNew(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }
        public ActionResult GetData(StoreRequestParameters parameters)
        {
            var pageIndex = parameters.Page;
            var pageSize = 20;
            var count = 0;
            var data = VPHumanProfileWorkTrialService.GetByManege(pageIndex, pageSize, out count);
            return this.Store(data, count);
        }
        public ActionResult GetAuditResult(int humanProfileWorkTrialID = 0)
        {
            //List<HumanProfileWorkTrialResultItem> lstData = new List<HumanProfileWorkTrialResultItem>();
            //if (humanProfileWorkTrialID > 0)
            //{
            //    lstData = service.GetResultAudit(humanProfileWorkTrialID);
            //}

            //return this.Store(lstData, lstData.Count);
            return this.Store(null);
        }
        public ActionResult AuditForm(int id)
        {
            //var data = new HumanProfileWorkTrialItem();
            //data = service.getByID(id);
            //return new Ext.Net.MVC.PartialViewResult { ViewName = "Audit", Model = data };
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Audit" };
        }
        public ActionResult Detail(string id)
        {
            var data = new VPHumanProfileWorkTrialBO();
            if (!string.IsNullOrEmpty(id))
                data = VPHumanProfileWorkTrialService.GetDetail(id);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
        }
        public ActionResult EditAuditForm(string id)
        {
            var data = new VPHumanProfileWorkTrialBO();
            if (!string.IsNullOrEmpty(id))
                data.Id = new Guid(id);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "EditAudit", Model = data };
        }
        public ActionResult DetailAuditForm(string id)
        {
            var data = new VPHumanProfileWorkTrialBO();
            if (!string.IsNullOrEmpty(id))
                data.Id = new Guid(id);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "DetailAudit", Model = data };
        }
        public ActionResult GetQualityCriteria(StoreRequestParameters parameters, string categoryID)
        {
            //List<QualityCriteriaItem> lstData = new List<QualityCriteriaItem>();
            //int total;
            //if (!string.IsNullOrEmpty(categoryID))
            //{
            //    lstData = new QualityCriteriaSV().GetCriteriaUsedByCateIds(parameters.Page, parameters.Limit, out total, categoryID);
            //}

            //return this.Store(lstData, lstData.Count);
            return this.Store(null);
        }
        public ActionResult SaveCriteria(string humanProfileWorkTrialID, string strPoint)
        {
            try
            {
                //List<QualityCriteriaItem> lstPointItem = new List<QualityCriteriaItem>();
                //lstPointItem = Ext.Net.JSON.Deserialize<QualityCriteriaItem[]>(strPoint).ToList();
                //int hptID = int.Parse(humanProfileWorkTrialID);
                //foreach (var item in lstPointItem)
                //{
                //    var data = new HumanProfileWorkTrialResultItem()
                //    {
                //        HumanProfileWorkTrialID = hptID,
                //        QualityCriteriaID = item.ID,
                //        ManagerPoint = item.MaxPoint,
                //        Note = item.Note
                //    };
                //    service.InsertCriterialResult(data);
                //}
                return this.Direct();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult UpdateAudit(string strPoint, Guid humanProfileWorkTrialId)
        {
            try
            {
                VPHumanProfileWorkTrialResultService.UpdateProfileWorkTrial(strPoint, humanProfileWorkTrialId);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }
        #region Gửi duyệt
        [HttpGet]
        public ActionResult frmSendApprove(string Id)
        {
            try
            {
                var data = new VPHumanProfileWorkTrialBO();
                data = VPHumanProfileWorkTrialService.GetDetail(Id);
                return new Ext.Net.MVC.PartialViewResult { ViewName = "SendApprove", Model = data };
            }
            catch
            {
                return this.Direct();
            }
        }
        [HttpPost]
        public ActionResult SendApprove(VPHumanProfileWorkTrialBO updateData, bool IsEdit = false)
        {
            try
            {
                updateData.ApprovalBy = updateData.EmployeeApprover.Id;
                VPHumanProfileWorkTrialService.Update(updateData);
                Notify(TitleResourceNotifies.SendApproveWorkTrial + " \""+ updateData.EmployeeTrialName + "\"", updateData.Note, updateData.EmployeeApprover.Id, UrlResourceNotifies.SendProfileWorkTrialPropose, new { id = updateData.Id });
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }
        #endregion
    }
}
