﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class RecruitmentResultController : FrontController
    {


        private UserManager<UserBO, Guid> userManager { get; set; }
        private RoleManager<RoleBO, Guid> roleManager { get; set; }

        public RecruitmentResultController(IUserService userStoreService, IRoleService roleStoreService)
        {
            userManager = new UserManager<UserBO, Guid>(userStoreService);
            roleManager = new RoleManager<RoleBO, Guid>(roleStoreService);
        }

        public ActionResult Index()
        {
            VPHumanRecruitmentPlanBO model = new VPHumanRecruitmentPlanBO();
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                model.IsRoleEdit = RoleEdit == "1" ? true : false;
            else
                model.IsRoleEdit = false;
            return View(model);
        }
        public ActionResult IndexNew(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexNew",
                WrapByScriptTag = false
            };
        }
        /// <summary>
        /// danh sách hồ sơ trúng tuyển
        /// </summary>
        /// <returns></returns>
        public ActionResult ProfilePassForm(Guid ID, string Name)
        {
            //var data = new HumanRecruitmentPlanItem();
            //data.ID = ID;
            //data.Name = Name;
            //return new Ext.Net.MVC.PartialViewResult { ViewName = "ProfilePass", Model = data };
            return new Ext.Net.MVC.PartialViewResult { ViewName = "ProfilePass" };
        }
        /// <summary>
        /// Lấy danh sách hồ sơ trúng tuyển đã được phê duyệt
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult LoadProfilePass(StoreRequestParameters parameters, Guid PlanID, Guid RequirementID)
        {
            //int totalCount;
            //var result = ResultService.GetProfilePass(parameters.Page, parameters.Limit, out totalCount, PlanID, RequirementID);
            //return this.Store(result, totalCount);
            return this.Store(null);
        }
        /// <summary>
        /// Lấy danh sách các hồ sơ được đề xuất
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="PlanID"></param>
        /// <param name="RequirementID"></param>
        /// <returns></returns> 
        public ActionResult LoadProfileResult(StoreRequestParameters parameters, Guid PlanID)
        {
            var pageIndex = parameters.Page;
            var pageSize = 20;
            int count = 0;
            var data = VPHumanRecruitmentProfileResultService.GetProfileResultByPlan(pageIndex, pageSize, out count, PlanID).ToList();

            return this.Store(data, count);
        }

        public ActionResult LoadCriteria(StoreRequestParameters parameters)
        {
            return this.Direct();
        }

        ////<summary>
        ////Thêm mới nhân sự vào hệ thống
        ////</summary>
        ////<param name="ProfileID"></param>
        ////<returns></returns>
        public ActionResult AddNewEmployee(VPHumanProfileWorkTrialBO data)
        {
            bool success = true;
            try
            {
                VPHumanProfileWorkTrialService.CreateEmployee(data);
            }
            catch
            {

            }
            return this.Direct(success);
        }

        public ActionResult AddNewEmployeeTrialForm(Guid ProfileID, Guid RecruitmentResultId, bool IsEmployee, bool IsTrial)
        {
            var model = VPHumanProfileWorkTrialService.GetNewEmployeeTrialForm(ProfileID, RecruitmentResultId);
            ViewData["IsEmployee"] = IsEmployee;
            ViewData["IsTrial"] = IsTrial;
            return new Ext.Net.MVC.PartialViewResult { ViewName = "AddEmployeeTrial", Model = model, ViewData = ViewData };
        }

        public ActionResult AddNewEmployeeTrial(VPHumanProfileWorkTrialBO data)
        {
            bool success = true;
            try
            {
                var result = userManager.Create(data.User, data.User.Password);
                if (result.Succeeded)
                {
                    VPHumanProfileWorkTrialService.CreateEmployeeTrial(data);
                }
            }
            catch
            {

            }
            return this.Direct(success);
        }
    }
}
