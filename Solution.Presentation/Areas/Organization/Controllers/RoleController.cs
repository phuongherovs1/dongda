﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class RoleController : Controller
    {

        protected IDepartmentTitleRoleService roleService { set; get; }
        public RoleController(IDepartmentTitleRoleService _roleService)
        {
            roleService = _roleService;
        }

        // GET: Organization/Role
        public Ext.Net.MVC.PartialViewResult IndexPosition(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "IndexPosition",
                WrapByScriptTag = false
                //check view
            };
        }
        public ActionResult Form(string containerId, string titleId)
        {
            UserRoleService roleService = new UserRoleService();
            var isAdmin = roleService.RoleExits("Administrator");
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckRoleForTitles_View(new Guid(titleId));
            if (isAdmin || titleRole)
            {
                try
                {
                    Session["TitleID"] = titleId;
                    return new Ext.Net.MVC.PartialViewResult();
                }
                catch
                {
                    return this.Direct();
                }
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền truy cập chức năng này!"
                });
                return this.Direct();
            }
        }

        private Node createNodeDepartment(DepartmentBO department)
        {
            Node node = new Node();
            node.NodeID = department.Id.ToString();
            node.Text = " " + department.Name;
            node.IconCls = "";

            node.CustomAttributes.Add(new ConfigItem { Name = "IsView", Value = department.IsView.ToString(), Mode = ParameterMode.Value });
            node.CustomAttributes.Add(new ConfigItem { Name = "IsUpdate", Value = department.IsUpdate.ToString(), Mode = ParameterMode.Value });
            node.Leaf = !department.IsParent;
            return node;
        }

        public ActionResult LoadDataTree(string node)
        {
            try
            {
                Guid titleId = Guid.Parse(Session["TitleID"].ToString());
                DepartmentService departmentService = new DepartmentService();
                DepartmentTitleRoleService roleService = new DepartmentTitleRoleService();

                NodeCollection nodes = new NodeCollection();
                var departments = new List<DepartmentBO>();
                //var departmentID = node == "root" ? (Guid?)null : new Guid(Convert.ToString(node));
                //if(departmentID != null)
                //    departments = departmentService.GetAll().Where(i=>i.ParentId == departmentID).OrderBy(o => o.Level).ThenBy(o => o.Name).ToList();
                //else
                //    departments = departmentService.GetAll().Where(i=>i.Level == 0).OrderBy(o => o.Level).ThenBy(o => o.Name).ToList();
                departments = departmentService.GetAll().OrderBy(o => o.Level).ThenBy(o => o.Name).ToList();
                var test = roleService.GetRolesByTitle(titleId);
                var departmentTitleRoles = new List<DepartmentTitleRoleBO>();
                if (test != null)
                    departmentTitleRoles = test.ToList();

                for (int i = 0; i < departments.Count; i++)
                {
                    int index = departmentTitleRoles.FindIndex(c => c.DepartmentId == departments[i].Id);
                    if (index >= 0)
                    {
                        departments[i].IsView = departmentTitleRoles[index].ViewAll;
                        departments[i].IsUpdate = departmentTitleRoles[index].UpdateAll;
                    }

                    nodes.Add(createNodeDepartment(departments[i]));
                }
                return this.Content(nodes.ToJson());
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }

        public ActionResult LoadDataTree1(string node)
        {
            try
            {
                Guid titleId = Guid.Parse(Session["TitleID"].ToString());
                DepartmentService departmentService = new DepartmentService();
                NodeCollection nodes = new NodeCollection();
                var departments = new List<DepartmentBO>();
                var departmentID = node == "root" ? (Guid?)null : new Guid(Convert.ToString(node));
                departments = departmentService.GetTreeDepartment(departmentID).ToList();
                departments.OrderByDescending(x => x.Name);

                var test = roleService.GetRolesByTitle(titleId);
                var departmentTitleRoles = new List<DepartmentTitleRoleBO>();
                if (test != null)
                    departmentTitleRoles = test.ToList();

                for (int i = 0; i < departments.Count; i++)
                {
                    int index = departmentTitleRoles.FindIndex(c => c.DepartmentId == departments[i].Id);
                    if (index >= 0)
                    {
                        departments[i].IsView = departmentTitleRoles[index].ViewAll;
                        departments[i].IsUpdate = departmentTitleRoles[index].UpdateAll;
                    }

                    nodes.Add(createNodeDepartment(departments[i]));
                }

                return this.Content(nodes.ToJson());
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }
        //public NodeCollection BuildNodeCollection(Guid Id, NodeCollection nodes)
        //{
        //    Guid titleId = Guid.Parse(Session["TitleID"].ToString());
        //    DepartmentService departmentService = new DepartmentService();
        //    var departments = new List<DepartmentBO>();
        //    departments = departmentService.GetAll().Where(i => i.ParentId == Id).OrderBy(o => o.Level).ThenBy(o => o.Name).ToList();
        //    departments.OrderByDescending(x => x.Name);

        //    var test = roleService.GetRolesByTitle(titleId);
        //    var departmentTitleRoles = new List<DepartmentTitleRoleBO>();
        //    if (test != null)
        //        departmentTitleRoles = test.ToList();

        //    for (int i = 0; i < departments.Count; i++)
        //    {
        //        int index = departmentTitleRoles.FindIndex(c => c.DepartmentId == departments[i].Id);
        //        if (index >= 0)
        //        {
        //            departments[i].IsView = departmentTitleRoles[index].ViewAll;
        //            departments[i].IsUpdate = departmentTitleRoles[index].UpdateAll;
        //        }
        //        nodes.Add(createNodeDepartment(departments[i]));
        //    }
        //    return nodes;
        //}
        public ActionResult UpdateTitleRole(string dataRole = null)
        {
            var result = false;
            if (string.IsNullOrWhiteSpace(dataRole) || !dataRole.Contains("Updated"))
            {
                this.ShowNotify("Không có dữ liệu cập nhật");
                return this.Direct(result: result);
            }

            string titleId = string.Empty;
            if (Session["TitleID"] != null)
                titleId = Session["TitleID"].ToString();

            if (string.IsNullOrEmpty(titleId))
            {
                this.ShowNotify("Không xác định được chức danh");
                return this.Direct(result: result);
            }

            //Check quyền
            UserRoleService roleService = new UserRoleService();
            var isAdmin = roleService.RoleExits("Administrator");
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckRoleForTitles_Edit(new Guid(titleId));
            if (!isAdmin && !titleRole)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }
            var dataUpdated = JSON.Deserialize<Dictionary<string, object>>(dataRole ?? string.Empty)["Updated"];
            List<DepartmentBO> lstDepartment = JSON.Deserialize<List<DepartmentBO>>(dataUpdated.ToString().Replace("checked", "ABC").Replace("\"root\"", "null"));
            DepartmentTitleRoleService service = new DepartmentTitleRoleService();
            try
            {
                for (int i = 0; i < lstDepartment.Count; i++)
                {
                    var role = service.GetRole(Guid.Parse(titleId), lstDepartment[i].Id);

                    if (role != null)
                    {
                        role.ViewAll = lstDepartment[i].IsView;
                        role.UpdateAll = lstDepartment[i].IsUpdate;
                        service.Update(role);
                    }
                    else
                    {
                        role = new DepartmentTitleRoleBO();
                        role.Id = Guid.NewGuid();
                        role.TitleId = Guid.Parse(titleId);
                        role.DepartmentId = lstDepartment[i].Id;
                        role.ViewAll = lstDepartment[i].IsView;
                        role.UpdateAll = lstDepartment[i].IsUpdate;
                        service.Insert(role);
                    }
                }
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
    }
}