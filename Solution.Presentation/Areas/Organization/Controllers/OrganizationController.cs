﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Common;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Text;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using iDAS.ADO;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    //[Authorize(Roles = Common.Role.Administrator)]
    public class OrganizationController : FrontController
    {

        private UserManager<UserBO, Guid> userManager { get; set; }
        private readonly IVPHumanProfileCuriculmViateService _vPHumanProfileCuriculmViateService;
        public OrganizationController(IUserService userStoreService, IVPHumanProfileCuriculmViateService vPHumanProfileCuriculmViateService)
        {
            userManager = new UserManager<UserBO, Guid>(userStoreService);
            _vPHumanProfileCuriculmViateService = vPHumanProfileCuriculmViateService;
        }
        //
        // GET: /Organization/Organization/
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            //var RoleEdit = Request.Params["isroleedit"];
            //if (RoleEdit != null)
            //    ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            //else
            //    ViewData["isroleedit"] = false;
            //ViewData["IsAdmin"] = Common.Role.IsAdmin();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult IndexOrganization(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            //ViewData["IsAdmin"] = Common.Role.IsAdmin();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexOrganization",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult IndexPosition(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            //ViewData["IsAdmin"] = Common.Role.IsAdmin();
            var tap = Request.Params["IsRole"];
            if (tap == "1")
            {
                ViewData["IsRole"] = true;
            }
            else
            {
                ViewData["IsRole"] = false;
            }
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexPosition",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }

        public ActionResult EmployeeTitleForm(string id)
        {
            var employeeName = EmployeeService.GetById(Guid.TryParse(id, out Guid gid) ? gid : Guid.Empty).Name;
            ViewData["EmployeeName"] = employeeName;
            ViewData["EmployeeId"] = id;

            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = "pnMainSystem",
                ViewName = "EmployeeTitleForm",
                ViewData = ViewData
            };
        }

        public ActionResult GetEmployeeJobTitles(string id)
        {
            var currentJobIds = (from t in EmployeeJobTitleService.GetByEmployeeId(Guid.TryParse(id, out Guid gid) ? gid : Guid.Empty) select t.JobTitleId).ToList();
            var allJobIds = JobTitleService.GetAllNotPaging();
            var result = from t in allJobIds select new { t.Id, t.Name, IsJob = currentJobIds.Contains(t.Id) };

            return this.Store(result);
        }

        public ActionResult UpdateJobTitles(string employeeId, string titleId, string isJob)
        {
            bool b = bool.Parse(isJob);
            if (b)
            {
                EmployeeJobTitleService.Insert(new EmployeeJobTitleBO { Id = Guid.NewGuid(), HumanEmployeeId = Guid.Parse(employeeId), JobTitleId = Guid.Parse(titleId) });
            }
            else
            {
                var toRemove = from t in EmployeeJobTitleService.GetByEmployeeId(Guid.Parse(employeeId)).Where(t => t.JobTitleId == Guid.Parse(titleId)) select t.Id;
                EmployeeJobTitleService.RemoveRange(toRemove);
            }
            return this.Direct(true);
        }

        public void Export(List<string> IDEmployee = null)
        {
            this.ExportSummaryRelityEmployee(IDEmployee);
        }
        public Ext.Net.MVC.PartialViewResult EmployeeManage(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "EmployeeManage",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        public ActionResult Form(string id = default(string))
        {
            EmployeeBO model = null;
            ViewData["DataSex"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetSex());
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                ViewData.Add("ChangePass", true);
                model = new EmployeeBO();
                var character = ConfigurationManager.AppSettings["CharacterInCodeEmployee"];
                var KeyCodeEmployee = ConfigurationManager.AppSettings["KeyCodeEmployee"];
                DataTable dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryTable("GetMaxNumberOfCodeEmployee",
                    parameter: new
                    {
                        character = !string.IsNullOrEmpty(character) ? character : "."
                    }
                );
                if(dataTable != null && dataTable.Rows.Count > 0)
                {
                    int numberOfCode = (dataTable.Rows[0]["Number"] != null && !string.IsNullOrEmpty(dataTable.Rows[0]["Number"].ToString())) ? Int32.Parse(dataTable.Rows[0]["Number"].ToString()) + 1 : 1;
                    if (!string.IsNullOrEmpty(KeyCodeEmployee))
                    {
                        if (numberOfCode < 10)
                            model.Code = KeyCodeEmployee + character + "00" + numberOfCode.ToString();
                        else if(numberOfCode < 100)
                            model.Code = KeyCodeEmployee + character + "0" + numberOfCode.ToString();
                        else
                            model.Code = KeyCodeEmployee + character + numberOfCode.ToString();
                    }
                }
            }
            else
            {
                var exist = UserRoleService.RoleExits("ViewEmployeeProfile");
                var isAdmin = UserRoleService.RoleExits("Administrator");
                var isCurrent = UserRoleService.CurrentUser(new Guid(id));
                DepartmentTitleRoleService s = new DepartmentTitleRoleService();
                var titleRole = s.CheckTitleRole_View(new Guid(id));
                if (exist || isCurrent || isAdmin || titleRole)
                {
                    ViewData.Add("Operation", Common.Operation.Update);
                    ViewData.Add("ChangePass", (UserName == "Admin" ? false : true));
                    model = EmployeeService.GetDetail(new Guid(id.ToString()));
                    if (model.Parent.HasValue)
                        model.Employee = Common.DBConnect.EmployeeExtGetById(model.Parent.Value.ToString());
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = "Bạn không có quyền truy cập chức năng này!"
                    });
                    return this.Direct();
                }
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult Detail(string id = default(string))
        {
            object model = null;
            if (id == default(string))
            {
            }
            else
            {
                model = EmployeeService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        #region Nhân sự
        public List<Guid> GetDepartmentsListParent(Guid ID, List<Guid> LstOut)
        {
            if (LstOut == null || LstOut.Count == 0)
                LstOut = new List<Guid>();
            if (LstOut.IndexOf(ID) == -1)
                LstOut.Add(ID);
            List<DepartmentBO> department = DepartmentService.Get(x => x.ParentId.HasValue && x.ParentId.Value.ToString() == ID.ToString()).ToList();
            if (department.Count > 0)
            {
                foreach (DepartmentBO item in department)
                    GetDepartmentsListParent(item.Id, LstOut);
            }
            return LstOut;
        }
        /// <summary>
        /// Load nhân sự
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="departmentId">id phòng ban</param>
        /// <param name="query">chuỗi filter</param>
        /// <param name="emptyRole">chưa có chức danh</param>
        /// <param name="isAll">tất cả</param>
        /// <returns></returns>
        public ActionResult LoadEmployees(StoreRequestParameters parameter, string departmentId, string query, string emptyRole, List<string> lstEmployeeTitles)
        {
            try
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                List<Guid> lstDepartment = new List<Guid>();
                lstDepartment = GetDepartmentsListParent(new Guid(departmentId), lstDepartment);
                if (emptyRole == "Chưa có chức danh")
                {
                    var data = EmployeeService.GetAllRoleEmpty(query).ToList();
                    data = data.GroupBy(x => x.Id).Select(y => y.First()).AsEnumerable().Select(i => { i.LastName = i.Name.Split(' ').Last(); return i; }).ToList();
                    return this.Store(EmployeeService.Page(data.OrderBy(x => x.LastName, StringComparer.Create(culture, false)), pageIndex, pageSize), data.Count);
                }
                else if (emptyRole == "Có chức danh")
                {
                    var data = new List<EmployeeBO>();
                    foreach (Guid item in lstDepartment)
                    {
                        var lst = EmployeeService.GetByDepartmentIndex(item, query, allowGetRoleName: true, lstEmployeeTitles).ToList();
                        data.AddRange(lst);
                    }
                    data = data.GroupBy(x => x.Id).Select(y => y.First()).AsEnumerable().Select(i => { i.LastName = i.Name.Split(' ').Last(); return i; }).ToList();
                    return this.Store(EmployeeService.Page(data.OrderBy(x => x.LastName, StringComparer.Create(culture, false)), pageIndex, pageSize), data.Count);
                }
                else if (emptyRole == "Tất cả")
                {
                    List<EmployeeBO> data = EmployeeService.GetAllRoleEmpty(query).ToList();
                    var dataHas = new List<EmployeeBO>();
                    foreach (Guid item in lstDepartment)
                    {
                        var lst = EmployeeService.GetByDepartmentIndex(item, query, allowGetRoleName: true, lstEmployeeTitles).ToList();
                        dataHas.AddRange(lst);
                    }
                    data.AddRange(dataHas);
                    data = data.GroupBy(x => x.Id).Select(y => y.First()).AsEnumerable().Select(i => { i.LastName = i.Name.Split(' ').Last(); return i; }).ToList();
                    return this.Store(EmployeeService.Page(data.OrderBy(x => x.LastName, StringComparer.Create(culture, false)), pageIndex, pageSize), data.Count);
                }
            }
            catch
            {
            }
            return this.Direct();
        }
        public ActionResult LoadAll(StoreRequestParameters parameter, string query = "", string emptyRole = "Tất cả")
        {
            try
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var count = 0;
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                if (emptyRole == "Chưa có chức danh")
                {
                    var data = EmployeeService.GetAllRoleEmpty(query).AsEnumerable().Select(i => { i.LastName = i.Name.Split(' ').Last(); return i; }).ToList();
                    return this.Store(data.OrderBy(x => x.LastName, StringComparer.Create(culture, false)), count);
                }
                else if (emptyRole == "Có chức danh")
                {
                    var data = EmployeeService.GetData(pageIndex, pageSize, out count, query, allowGetRoleName: true).AsEnumerable().Select(i => { i.LastName = i.Name.Split(' ').Last(); return i; }).ToList();
                    return this.Store(data.OrderBy(x => x.LastName, StringComparer.Create(culture, false)), count);
                }
                else if (emptyRole == "Tất cả")
                {
                    List<EmployeeBO> data = EmployeeService.GetAllRoleEmpty(query).AsEnumerable().Select(i => { i.LastName = i.Name.Split(' ').Last(); return i; }).ToList();
                    List<EmployeeBO> dataHas = EmployeeService.GetData(pageIndex, pageSize, out count, query, allowGetRoleName: true).AsEnumerable().Select(i => { i.LastName = i.Name.Split(' ').Last(); return i; }).ToList();
                    data.AddRange(dataHas);
                    return this.Store(data.OrderBy(x => x.LastName, StringComparer.Create(culture, false)));
                }
            }
            catch
            {

            }
            return this.Direct();
        }
        public ActionResult LoadAll1(StoreRequestParameters parameter,
                                    string name = "", string code = "", DateTime? birthDayFrom = null, DateTime? birthDayTo = null, string native = "", string religion = "", string ethnic = "",
                                    string placeOfWork = "", string position = "", string departmentName = "",
                                    string eduName = "", string educationType = "", string educationResult = "",
                                    string diplomaName = "", string major = "", string diplomaEucationType = "", string diplomaEducationOrg = "", string educationLevel = "", string certificateLevel = "",
                                    string certificateName = "", string level = "", string certificatetype = "",
                                    string numberOfDecision = "", string reason = "", string form = "", string diplomaEducationLevel = "", string diplomacertificateLevel = "")
        {
            List<EmployeeBO> data = new List<EmployeeBO>();
            CultureInfo culture = new CultureInfo("vi-VN");
            try
            {
                DataTable dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employee_GetListData",
                    parameter: new
                    {
                        name = !string.IsNullOrEmpty(name) ? name : "",
                        code = !string.IsNullOrEmpty(code) ? code : "",
                        birthDayFrom = birthDayFrom.HasValue ? birthDayFrom.Value.ToString("yyyyMMdd") : "",
                        birthDayTo = birthDayTo.HasValue ? birthDayTo.Value.ToString("yyyyMMdd") : "",
                        native = !string.IsNullOrEmpty(native) ? native : "",
                        religion = !string.IsNullOrEmpty(religion) ? religion : "",
                        ethnic = !string.IsNullOrEmpty(ethnic) ? ethnic : ""
                    }
                );
                DataTable dataTable_WorkExperiences = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employee_GetListData_WorkExperiences",
                    parameter: new
                    {
                        placeOfWork = !string.IsNullOrEmpty(placeOfWork) ? placeOfWork : "",
                        position = !string.IsNullOrEmpty(position) ? position : "",
                        departmentName = !string.IsNullOrEmpty(departmentName) ? departmentName : ""
                    }
                );
                List<string> lst_WorkExperiences = Common.Utilities.ConvertDataIDToString(dataTable_WorkExperiences);

                DataTable dataTable_Training = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employee_GetListData_Training",
                    parameter: new
                    {
                        eduName = !string.IsNullOrEmpty(eduName) ? eduName : "",
                        educationType = !string.IsNullOrEmpty(educationType) ? educationType : "",
                        educationResult = !string.IsNullOrEmpty(educationResult) ? educationResult : ""
                    }
                );
                List<string> lst_Training = Common.Utilities.ConvertDataIDToString(dataTable_Training);

                DataTable dataTable_Diplomas = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employee_GetListData_Diplomas",
                    parameter: new
                    {
                        diplomaName = !string.IsNullOrEmpty(diplomaName) ? diplomaName : "",
                        major = !string.IsNullOrEmpty(major) ? major : "",
                        diplomaEucationType = !string.IsNullOrEmpty(diplomaEucationType) ? diplomaEucationType : "",
                        diplomaEducationOrg = !string.IsNullOrEmpty(diplomaEducationOrg) ? diplomaEducationOrg : "",
                        diplomaEducationLevel = !string.IsNullOrEmpty(diplomaEducationLevel) ? diplomaEducationLevel : "",
                        diplomacertificateLevel = !string.IsNullOrEmpty(diplomacertificateLevel) ? diplomacertificateLevel : ""
                    }
                );
                List<string> lst_Diplomas = Common.Utilities.ConvertDataIDToString(dataTable_Diplomas);

                DataTable dataTable_Certificates = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employee_GetListData_Certificates",
                    parameter: new
                    {
                        certificateName = !string.IsNullOrEmpty(certificateName) ? certificateName : "",
                        level = !string.IsNullOrEmpty(level) ? level : "",
                        certificatetype = !string.IsNullOrEmpty(certificatetype) ? certificatetype : ""
                    }
                );
                List<string> lst_Certificates = Common.Utilities.ConvertDataIDToString(dataTable_Certificates);

                DataTable dataTable_Rewards = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employee_GetListData_Rewards",
                    parameter: new
                    {
                        numberOfDecision = !string.IsNullOrEmpty(numberOfDecision) ? numberOfDecision : "",
                        reason = !string.IsNullOrEmpty(reason) ? reason : "",
                        form = !string.IsNullOrEmpty(form) ? form : ""
                    }
                );
                List<string> lst_Rewards = Common.Utilities.ConvertDataIDToString(dataTable_Rewards);

                if (dataTable.Rows.Count > 0)
                {
                    foreach (DataRow item in dataTable.Rows)
                    {
                        EmployeeBO employeeBO = new EmployeeBO();
                        employeeBO.Id = item["Id"] != null ? Guid.Parse(item["Id"].ToString()) : Guid.Empty;
                        employeeBO.Code = item["Code"] != null ? item["Code"].ToString() : "";
                        employeeBO.Name = item["Name"] != null ? item["Name"].ToString() : "";
                        employeeBO.RoleNames = item["Titles"] != null ? item["Titles"].ToString() : "";
                        employeeBO.FileAvatar.Data = item["Avatar"] != null ? Encoding.ASCII.GetBytes(item["Avatar"].ToString()) : new byte[0];
                        employeeBO.Email = item["Email"] != null ? item["Email"].ToString() : "";
                        employeeBO.PhoneNumber = item["PhoneNumber"] != null ? item["PhoneNumber"].ToString() : "";
                        if (item["Avatar"] != null)
                            employeeBO.Avatar = Encoding.ASCII.GetBytes(item["Avatar"].ToString());
                        if (item["Sex"] != null && !string.IsNullOrEmpty(item["Sex"].ToString()))
                            employeeBO.Sex = Int32.Parse(item["Sex"].ToString());
                        data.Add(employeeBO);
                    }
                }
                if (!string.IsNullOrEmpty(placeOfWork) || !string.IsNullOrEmpty(position) || !string.IsNullOrEmpty(departmentName))
                    data = data.Where(x => lst_WorkExperiences.Contains(x.Id.ToString())).ToList();

                if (!string.IsNullOrEmpty(eduName) || !string.IsNullOrEmpty(educationType) || !string.IsNullOrEmpty(educationResult))
                    data = data.Where(x => lst_Training.Contains(x.Id.ToString())).ToList();

                if (!string.IsNullOrEmpty(diplomaName)
                    || !string.IsNullOrEmpty(major)
                    || !string.IsNullOrEmpty(diplomaEucationType)
                    || !string.IsNullOrEmpty(diplomaEducationOrg)
                    || !string.IsNullOrEmpty(diplomaEducationLevel)
                    || !string.IsNullOrEmpty(diplomacertificateLevel))
                {
                    data = data.Where(x => lst_Diplomas.Contains(x.Id.ToString())).ToList();
                }

                if (!string.IsNullOrEmpty(certificateName) || !string.IsNullOrEmpty(level) || !string.IsNullOrEmpty(certificatetype))
                    data = data.Where(x => lst_Certificates.Contains(x.Id.ToString())).ToList();

                if (!string.IsNullOrEmpty(numberOfDecision) || !string.IsNullOrEmpty(reason) || !string.IsNullOrEmpty(form))
                    data = data.Where(x => lst_Rewards.Contains(x.Id.ToString())).ToList();
                data = data.Select(i => { i.LastName = i.Name.Split(' ').Last(); return i; }).ToList();
                TempData["EmployeeList"] = data.OrderBy(x => x.LastName, StringComparer.Create(culture, false));
                return this.Store(data.OrderBy(x => x.LastName, StringComparer.Create(culture, false)));
            }
            catch (Exception e) { }
            return this.Direct();
        }

        public ActionResult FormProfileFieldExcel()
        {
            // Thông tin cơ bản
            var cv = new string[] { "Code","Mã", "Name","Tên", "Sex", "Giới tính", "DateOfBirth","Ngày sinh", "BornAddress", "Nơi sinh", "HomeAddress", "Quê quán",
                "People", "Dân tộc", "PermanentResidence", "Hộ khẩu", "CurrentResidence", "Địa chỉ hiện tại", "OfficePhone","Điện thoại", "NumberOfIdentityCard","Số CMT",
                "DateIssueOfIdentityCard","Ngày cấp CMT", "PlaceIssueOfIdentityCard","Nơi cấp CMT", "TaxCode","Mã số thuế", "NumberOfBankAccounts","Số tài khoản",
                "Bank","Ngân hàng", "NumberOfPassport","Số hộ chiếu", "PlaceOfPassport","Nơi cấp hộ chiếu", "DateOfIssuePassport","Ngày cấp hộ chiếu",
                "PassportExpirationDate","Ngày hết hạn hộ chiếu", "Religion","Tôn giáo"};

            // Chính trị
            var politic = new string[] { "DateOnGroup","Ngày vào đoàn", "PositionGroup","Chức vụ đoàn", "PlaceOfLoadedGroup","Nơi kết nạp đoàn",
                "DateJoinRevolution", "Ngày tham gia cách mạng", "DateOfJoinParty", "Ngày vào đảng chính thức", "PlaceOfLoadedParty", "Nơi kết nạp đảng",
                "PosititonParty", "Chức vụ đảng" , "NumberOfPartyCard", "Số thẻ đảng" , "DateOnArmy", "Ngày nhập ngũ", "PositionArmy", "Chức vụ quân đội",
                "ArmyRank", "Cập bậc quân đội", "PoliticalTheory", "Trình độ lý luận chính trị", "GovermentManagement", "Trình độ quản lý nhà nước"};
            // quá trình công tác
            var workExp = new string[] { "StartDate","Bắt đầu", "EndDate","Kết thúc", "PlaceOfWork","Nơi công tác",
                "Position", "Chức vụ đảm nhiệm", "Department", "Phòng ban", "JobDescription", "Công việc"};

            // quá trình đào tạo
            var training = new string[] { "Name","Tên khóa học", "Form","Hình thức đào tạo", "Content","Nội dung đào tạo",
                "Certificate", "Chứng chỉ đào tạo", "Result", "Kết quả đào tạo"};

            // văn bằng
            var diploma = new string[] { "Name", "Tên văn bằng", "Faculty", "Khoa", "Major", "Chuyên ngành", "Place", "Nơi đào tạo", "Level", "Trình độ" };
            // chứng chỉ
            var certificate = new string[] { "Name", "Tên chứng chỉ", "Level", "Xếp loại", "CertificateType", "Loại chứng chỉ", "PlaceOfTraining", "Nơi cấp",
                "DateIssuance", "Ngày cấp", "DateExpiration", "Ngày hết hạn"};
            // khen thưởng
            var reward = new string[] { "NumberOfDecision", "Số quyết định", "DateOfDecision", "Ngày quyết định", "Reason", "Lý do", "Form", "Hình thức khen thưởng" };

            var result = new GroupCheckBoxBO();
            // Tạo danh sách checkbox
            for (int i = 0; i < cv.Length - 1; i += 2)
                result.groupCV.Add(new CheckBoxBO(cv[i], false, cv[i + 1]));

            for (int i = 0; i < politic.Length - 1; i += 2)
                result.groupPolitic.Add(new CheckBoxBO(politic[i], false, politic[i + 1]));

            for (int i = 0; i < workExp.Length - 1; i += 2)
                result.groupWorkExp.Add(new CheckBoxBO(workExp[i], false, workExp[i + 1]));

            for (int i = 0; i < training.Length - 1; i += 2)
                result.groupTraining.Add(new CheckBoxBO(training[i], false, training[i + 1]));

            for (int i = 0; i < diploma.Length - 1; i += 2)
                result.groupDiploma.Add(new CheckBoxBO(diploma[i], false, diploma[i + 1]));

            for (int i = 0; i < certificate.Length - 1; i += 2)
                result.groupCertificate.Add(new CheckBoxBO(certificate[i], false, certificate[i + 1]));

            for (int i = 0; i < reward.Length - 1; i += 2)
                result.groupReward.Add(new CheckBoxBO(reward[i], false, reward[i + 1]));

            return new Ext.Net.MVC.PartialViewResult { ViewName = "FormProfileFieldExcel", Model = result };
            //return new Ext.Net.MVC.PartialViewResult { ViewName = "FormProfileFieldExcel"};
        }

        private string ConvertAddress(string cityId, string districtId, string CommuneId)
        {
            var masterService = new ProfileMasterService();

            var city = "";
            var bornDistrict = "";
            var bornCommune = "";
            if (cityId != null && cityId != "" && Guid.TryParse(cityId, out Guid gid1))
                city = masterService.GetSingleMasterData("city", cityId).Name;
            if (districtId != null && districtId != "" && Guid.TryParse(districtId, out Guid gid2))
                bornDistrict = masterService.GetSingleMasterData("district", districtId).Name;
            if (CommuneId != null && CommuneId != "" && Guid.TryParse(CommuneId, out Guid gid4))
                bornCommune = masterService.GetSingleMasterData("commune", CommuneId).Name;

            return string.Join(" - ", new[] { bornCommune, bornDistrict, city }.Where(s => !string.IsNullOrEmpty(s)));
        }

        private string getValueCell_CV(EmployeeBO emp, string fieldName, IEnumerable<VPHumanProfileCuriculmViateBO> lstCV)
        {
            if (fieldName.Equals("Code"))
                return emp.Code;
            else if (fieldName.Equals("Name"))
                return emp.Name;

            if (lstCV == null || lstCV.Count() == 0)
                return string.Empty;

            VPHumanProfileCuriculmViateBO cv = lstCV.FirstOrDefault(i => i.HumanEmployeeId == emp.Id);
            if (cv == null)
                return string.Empty;

            switch (fieldName)
            {
                case "Sex":
                    if (cv.IsMale == true)
                        return "Nam";
                    else
                        return "Nữ";
                case "BornAddress":
                    return ConvertAddress(cv.PlaceOfBirthCity, cv.PlaceOfBirthDistrict, cv.PlaceOfBirthCommune);
                case "HomeAddress":
                    return ConvertAddress(cv.HomeTownCity, cv.HomeTownDistrict, cv.HomeTownCommune);
                case "People":
                    var ethnicId = cv.People;
                    var ethnicName = "";
                    if (ethnicId != null && ethnicId != "" && Guid.TryParse(ethnicId, out Guid gid2))
                    {
                        var profileMasterService = new ProfileMasterService();
                        ethnicName = profileMasterService.GetSingleMasterData("ethnic", ethnicId).Name;
                    }
                    return ethnicName;
                case "Religion":
                    var religionId = cv.Religion;
                    var religionName = "";
                    if (religionId != null && religionId != "" && Guid.TryParse(religionId, out Guid gid1))
                    {
                        var profileMasterService1 = new ProfileMasterService();
                        religionName = profileMasterService1.GetSingleMasterData("religion", religionId).Name;
                    }
                    return religionName;
                case "PositionGroup":
                    var masterService = new ProfileMasterService();
                    return masterService.GetSingleMasterData("positionGroup", cv.PositionGroup).Name;
                case "PosititonParty":
                    var masterService1 = new ProfileMasterService();
                    return masterService1.GetSingleMasterData("positionParty", cv.PosititonParty).Name;
                case "ArmyRank":
                    var masterService2 = new ProfileMasterService();
                    return masterService2.GetSingleMasterData("rankArmy", cv.ArmyRank).Name;
                case "PoliticalTheory":
                    var masterService3 = new ProfileMasterService();
                    return masterService3.GetSingleMasterData("political", cv.PoliticalTheory).Name;
                case "GovermentManagement":
                    var masterService4 = new ProfileMasterService();
                    return masterService4.GetSingleMasterData("management", cv.GovermentManagement).Name;
                default:
                    var obj = typeof(VPHumanProfileCuriculmViateBO).GetProperty(fieldName).GetValue(cv);
                    if (obj == null)
                        return string.Empty;
                    else if (obj.GetType() == DateTime.Now.GetType())
                        return ((DateTime)obj).ToString("dd/MM/yyyy");
                    else
                        return obj.ToString();
            }
        }
        private string getValueCell_WorksExp(EmployeeBO emp, string fieldName, IEnumerable<VPHumanProfileWorkExperienceBO> lstWorkExp)
        {
            if (lstWorkExp == null || lstWorkExp.Count() == 0)
                return string.Empty;

            var w = lstWorkExp.FirstOrDefault(i => i.HumanEmployeeId == emp.Id);
            if (w == null)
                return string.Empty;

            var obj = typeof(VPHumanProfileWorkExperienceBO).GetProperty(fieldName).GetValue(w);

            if (obj == null)
                return string.Empty;
            else if (obj.GetType() == DateTime.Now.GetType())
                return ((DateTime)obj).ToString("dd/MM/yyyy");
            else
                return obj.ToString();
        }
        private string getValueCell_Training(EmployeeBO emp, string fieldName, IEnumerable<VPHumanProfileTrainingBO> lstTraining)
        {
            if (lstTraining == null || lstTraining.Count() == 0)
                return string.Empty;

            var masterService = new ProfileMasterService();
            var objs = lstTraining.Where(i => i.HumanEmployeeId == emp.Id);
            if (objs == null || objs.Count() == 0)
                return string.Empty;

            switch (fieldName)
            {
                case "Form":
                    var eduTypeName = "";
                    try
                    {
                        foreach (var o in objs)
                        {

                            // tách dòng
                            if (string.IsNullOrWhiteSpace(eduTypeName))
                                eduTypeName += masterService.GetSingleMasterData("educationType", o.Form).Name;
                            else
                                eduTypeName += "&#10;" + masterService.GetSingleMasterData("educationType", o.Form).Name;

                        }
                    }
                    catch
                    {
                        return eduTypeName;
                    }
                    return eduTypeName;
                case "Result":
                    var resultEdu = "";
                    try
                    {
                        foreach (var o in objs)
                        {
                            // tách dòng
                            if (string.IsNullOrWhiteSpace(resultEdu))
                                resultEdu += masterService.GetSingleMasterData("educationResult", o.Form).Name;
                            else
                                resultEdu += "&#10;" + masterService.GetSingleMasterData("educationResult", o.Form).Name;
                        }
                    }
                    catch
                    {
                        return resultEdu;
                    }
                    return resultEdu;
                default:
                    var cellValue = "";
                    foreach (var o in objs)
                    {
                        // tách dòng
                        if (string.IsNullOrWhiteSpace(cellValue))
                            cellValue += typeof(VPHumanProfileTrainingBO).GetProperty(fieldName).GetValue(o);
                        else
                            cellValue += "&#10;" + typeof(VPHumanProfileTrainingBO).GetProperty(fieldName).GetValue(o);
                    }
                    return cellValue;
            }
        }

        private string getValueCell_Diploma(EmployeeBO emp, string fieldName, IEnumerable<VPHumanProfileDiplomaBO> lstDiploma)
        {
            if (lstDiploma == null || lstDiploma.Count() == 0)
                return string.Empty;

            var masterService = new ProfileMasterService();
            var objs = lstDiploma.Where(i => i.HumanEmployeeId == emp.Id);
            if (objs == null || objs.Count() == 0)
                return string.Empty;

            switch (fieldName)
            {
                // chuyên ngành
                case "Major":
                    var eduTypeName = "";
                    try
                    {
                        foreach (var o in objs)
                        {

                            // tách dòng
                            if (string.IsNullOrWhiteSpace(eduTypeName))
                                eduTypeName += masterService.GetSingleMasterData("educationField", o.Major).Name;
                            else
                                eduTypeName += "&#10;" + masterService.GetSingleMasterData("educationField", o.Major).Name;

                        }
                    }
                    catch
                    {
                        return eduTypeName;
                    }
                    return eduTypeName;
                case "Place":
                    var pla = "";
                    try
                    {
                        foreach (var o in objs)
                        {
                            // tách dòng
                            if (string.IsNullOrWhiteSpace(pla))
                                pla += masterService.GetSingleMasterData("educationOrg", o.Place).Name;
                            else
                                pla += "&#10;" + masterService.GetSingleMasterData("educationOrg", o.Place).Name;
                        }
                    }
                    catch
                    {
                        return pla;
                    }
                    return pla;
                case "Level":
                    var lev = "";
                    try
                    {
                        foreach (var o in objs)
                        {
                            // tách dòng
                            if (string.IsNullOrWhiteSpace(lev))
                                lev += masterService.GetSingleMasterData("educationLevel", o.Place).Name;
                            else
                                lev += "&#10;" + masterService.GetSingleMasterData("educationLevel", o.Place).Name;
                        }
                    }
                    catch
                    {
                        return lev;
                    }
                    return lev;
                default:
                    var cellValue = "";
                    foreach (var o in objs)
                    {
                        // tách dòng
                        if (string.IsNullOrWhiteSpace(cellValue))
                            cellValue += typeof(VPHumanProfileDiplomaBO).GetProperty(fieldName).GetValue(o);
                        else
                            cellValue += "&#10;" + typeof(VPHumanProfileDiplomaBO).GetProperty(fieldName).GetValue(o);
                    }
                    return cellValue;
            }
        }

        private string getValueCell_Certificate(EmployeeBO emp, string fieldName, IEnumerable<VPHumanProfileCertificateBO> lstCertificate)
        {
            if (lstCertificate == null || lstCertificate.Count() == 0)
                return string.Empty;

            var masterService = new ProfileMasterService();
            var objs = lstCertificate.Where(i => i.HumanEmployeeId == emp.Id);
            if (objs == null || objs.Count() == 0)
                return string.Empty;

            switch (fieldName)
            {
                case "Level":
                    var eduTypeName = "";
                    try
                    {
                        foreach (var o in objs)
                        {
                            // tách dòng
                            if (string.IsNullOrWhiteSpace(eduTypeName))
                                eduTypeName += masterService.GetSingleMasterData("certificateLevel", o.Level).Name;
                            else
                                eduTypeName += "&#10;" + masterService.GetSingleMasterData("certificateLevel", o.Level).Name;
                        }
                    }
                    catch
                    {
                        return eduTypeName;
                    }
                    return eduTypeName;
                case "CertificateType":
                    var typ = "";
                    try
                    {
                        foreach (var o in objs)
                        {
                            // tách dòng
                            if (string.IsNullOrWhiteSpace(typ))
                                typ += masterService.GetSingleMasterData("certificateType", o.CertificateType).Name;
                            else
                                typ += "&#10;" + masterService.GetSingleMasterData("certificateType", o.CertificateType).Name;
                        }
                    }
                    catch
                    {
                        return typ;
                    }
                    return typ;
                default:
                    var cellValue = "";
                    foreach (var o in objs)
                    {
                        var obj1 = typeof(VPHumanProfileCertificateBO).GetProperty(fieldName).GetValue(o);
                        // format ngày tháng nếu có
                        if (obj1 != null && obj1.GetType() == DateTime.Now.GetType())
                            obj1 = ((DateTime)obj1).ToString("dd/MM/yyyy");
                        // tách dòng
                        if (string.IsNullOrWhiteSpace(cellValue))
                            cellValue += obj1;
                        else
                            cellValue += "&#10;" + obj1;
                    }
                    return cellValue;
            }
        }

        private string getValueCell_Reward(EmployeeBO emp, string fieldName, IEnumerable<VPHumanProfileRewardBO> lstReward)
        {
            if (lstReward == null || lstReward.Count() == 0)
                return string.Empty;

            var masterService = new ProfileMasterService();
            var objs = lstReward.Where(i => i.HumanEmployeeId == emp.Id);
            if (objs == null || objs.Count() == 0)
                return string.Empty;

            switch (fieldName)
            {
                case "Form":
                    var typeRe = "";
                    try
                    {
                        foreach (var o in objs)
                        {
                            // tách dòng
                            if (string.IsNullOrWhiteSpace(typeRe))
                                typeRe += masterService.GetSingleMasterData("award", o.Form).Name;
                            else
                                typeRe += "&#10;" + masterService.GetSingleMasterData("award", o.Form).Name;
                        }
                    }
                    catch
                    {
                        return typeRe;
                    }
                    return typeRe;
                default:
                    var cellValue = "";
                    foreach (var o in objs)
                    {
                        var obj1 = typeof(VPHumanProfileRewardBO).GetProperty(fieldName).GetValue(o);
                        // format ngày tháng nếu có
                        if (obj1 != null && obj1.GetType() == DateTime.Now.GetType())
                            obj1 = ((DateTime)obj1).ToString("dd/MM/yyyy");
                        // tách dòng
                        if (string.IsNullOrWhiteSpace(cellValue))
                            cellValue += obj1;
                        else
                            cellValue += "&#10;" + obj1;
                    }
                    return cellValue;
            }
        }

        private string getDataExportExcel(GroupCheckBoxBO grpCheck)
        {
            try
            {
                if (grpCheck == null)
                    return null;
                if (TempData["EmployeeList"] == null)
                    return null;
                TempData.Keep("EmployeeList");
                var emps = (IEnumerable<EmployeeBO>)TempData["EmployeeList"];
                if (emps.Count() == 0)
                    return null;

                IEnumerable<VPHumanProfileCuriculmViateBO> lstCV = VPHumanProfileCuriculmViateService.GetAll();
                IEnumerable<VPHumanProfileWorkExperienceBO> lstWorkExp = null;
                IEnumerable<VPHumanProfileTrainingBO> lstTraining = null;
                IEnumerable<VPHumanProfileDiplomaBO> lstDiploma = null;
                IEnumerable<VPHumanProfileCertificateBO> lstCertificate = null;
                IEnumerable<VPHumanProfileRewardBO> lstReward = null;

                // luôn check họ tên
                grpCheck.groupCV[1].FieldValue = true;
                List<CheckBoxBO> grpCV = grpCheck.groupCV.FindAll(i => i.FieldValue == true);
                List<CheckBoxBO> grpPolitic = grpCheck.groupPolitic.FindAll(i => i.FieldValue == true);
                List<CheckBoxBO> grpWorkExp = grpCheck.groupWorkExp.FindAll(i => i.FieldValue == true);
                List<CheckBoxBO> grpTraining = grpCheck.groupTraining.FindAll(i => i.FieldValue == true);
                List<CheckBoxBO> grpDiploma = grpCheck.groupDiploma.FindAll(i => i.FieldValue == true);
                List<CheckBoxBO> grpCerfificate = grpCheck.groupCertificate.FindAll(i => i.FieldValue == true);
                List<CheckBoxBO> grpReward = grpCheck.groupReward.FindAll(i => i.FieldValue == true);

                // Chỉ lấy dữ liệu về khi người dùng chọn
                if (grpWorkExp.Count > 0)
                    lstWorkExp = VPHumanProfileWorkExperienceService.GetAll();
                if (grpTraining.Count > 0)
                    lstTraining = VPHumanProfileTrainingService.GetAll();
                if (grpDiploma.Count > 0)
                    lstDiploma = VPHumanProfileDiplomaService.GetAll();
                if (grpCerfificate.Count > 0)
                    lstCertificate = VPHumanProfileCertificateService.GetAll();
                if (grpReward.Count > 0)
                    lstReward = VPHumanProfileRewardService.GetAll();

                var rowCount = (emps.Count() + 2).ToString();
                var colCount = (1 + grpCV.Count + grpPolitic.Count + grpWorkExp.Count + grpTraining.Count + grpDiploma.Count + grpCerfificate.Count + grpReward.Count).ToString();

                StringBuilder stringBuilder = new StringBuilder(@"<?xml version=""1.0""?>
                <?mso-application progid=""Excel.Sheet""?>
                <Workbook xmlns=""urn:schemas-microsoft-com:office:spreadsheet""
                 xmlns:o=""urn:schemas-microsoft-com:office:office""
                 xmlns:x=""urn:schemas-microsoft-com:office:excel""
                 xmlns:ss=""urn:schemas-microsoft-com:office:spreadsheet""
                 xmlns:html=""http://www.w3.org/TR/REC-html40"">
                 <DocumentProperties xmlns=""urn:schemas-microsoft-com:office:office"">
                  <Author>vncert</Author>
                  <LastAuthor>vncert</LastAuthor>
                  <Created>2019-10-25T04:14:19Z</Created>
                  <LastSaved>2019-10-25T06:45:54Z</LastSaved>
                  <Version>16.00</Version>
                 </DocumentProperties>
                 <OfficeDocumentSettings xmlns=""urn:schemas-microsoft-com:office:office"">
                  <AllowPNG/>
                 </OfficeDocumentSettings>
                 <ExcelWorkbook xmlns=""urn:schemas-microsoft-com:office:excel"">
                  <WindowHeight>7320</WindowHeight>
                  <WindowWidth>20490</WindowWidth>
                  <WindowTopX>0</WindowTopX>
                  <WindowTopY>0</WindowTopY>
                  <ProtectStructure>False</ProtectStructure>
                  <ProtectWindows>False</ProtectWindows>
                 </ExcelWorkbook>
                 <Styles>
                  <Style ss:ID=""Default"" ss:Name=""Normal"">
                   <Alignment ss:Vertical=""Bottom""/>
                   <Borders/>
                   <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""11"" ss:Color=""#000000""/>
                   <Interior/>
                   <NumberFormat/>
                   <Protection/>
                  </Style>
                  <Style ss:ID=""s62"">
                   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Bottom""/>
                   <Font ss:FontName=""Times New Roman"" x:Family=""Roman"" ss:Size=""11"" ss:Color=""#000000"" ss:Bold=""1""/>
                  </Style>
                  <Style ss:ID=""s64"">
                   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Bottom""/>
                   <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""12"" ss:Color=""#000000"" ss:Bold=""1""/>
                  </Style>
                  <Style ss:ID=""s65"">
                   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center""/>
                   <Font ss:FontName=""Times New Roman"" x:Family=""Roman"" ss:Size=""11"" ss:Color=""#000000"" ss:Bold=""1""/>
                  </Style>
                  <Style ss:ID=""s66"">
                   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center""/>
                  </Style>
                  <Style ss:ID=""s67"">
                   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center""/>
                   <Font ss:FontName=""Times New Roman"" x:Family=""Roman"" ss:Size=""11"" ss:Color=""#000000""/>
                  </Style>
                  <Style ss:ID=""s68"">
                   <Font ss:FontName=""Times New Roman"" x:Family=""Roman"" ss:Size=""11"" ss:Color=""#000000""/>
                  </Style>
                  <Style ss:ID=""s69"">
                   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Bottom""/>
                   <Font ss:FontName=""Times New Roman"" x:Family=""Roman"" ss:Size=""12"" ss:Color=""#000000"" ss:Bold=""1""/>
                  </Style>
                  <Style ss:ID=""s70"">
                   <Alignment ss:Vertical=""Bottom"" ss:WrapText=""1""/>
                   <Font ss:FontName=""Times New Roman"" x:Family=""Roman"" ss:Size=""11"" ss:Color=""#000000""/>
                  </Style >
                 </Styles>
                 <Worksheet ss:Name=""Sheet1"">
                  <Table ss:ExpandedColumnCount=""" + colCount + @""" ss:ExpandedRowCount=""" + rowCount + @""" x:FullColumns=""1"" x:FullRows=""1"" ss:DefaultRowHeight=""15"">
                   <Row ss:Height=""15.75"" ss:StyleID=""s64"">");
                if (grpCV.Count > 0)
                    stringBuilder.Append(@"<Cell ss:MergeAcross=""" + (grpCV.Count).ToString() + @""" ss:StyleID=""s69""><Data ss:Type=""String"">Thông tin chung</Data></Cell>");
                if (grpPolitic.Count > 0)
                    stringBuilder.Append(@"<Cell ss:MergeAcross=""" + (grpPolitic.Count - 1).ToString() + @""" ss:StyleID=""s69""><Data ss:Type=""String"">Lý lịch chính trị</Data></Cell>");
                if (grpWorkExp.Count > 0)
                    stringBuilder.Append(@"<Cell ss:MergeAcross=""" + (grpWorkExp.Count - 1).ToString() + @""" ss:StyleID=""s69""><Data ss:Type=""String"">Quá trình công tác</Data></Cell>");
                if (grpTraining.Count > 0)
                    stringBuilder.Append(@"<Cell ss:MergeAcross=""" + (grpTraining.Count - 1).ToString() + @""" ss:StyleID=""s69""><Data ss:Type=""String"">Quá trình đào tạo</Data></Cell>");
                if (grpDiploma.Count > 0)
                    stringBuilder.Append(@"<Cell ss:MergeAcross=""" + (grpDiploma.Count - 1).ToString() + @""" ss:StyleID=""s69""><Data ss:Type=""String"">Văn bằng</Data></Cell>");
                if (grpCerfificate.Count > 0)
                    stringBuilder.Append(@"<Cell ss:MergeAcross=""" + (grpCerfificate.Count - 1).ToString() + @""" ss:StyleID=""s69""><Data ss:Type=""String"">Chứng chỉ</Data></Cell>");
                if (grpReward.Count > 0)
                    stringBuilder.Append(@"<Cell ss:MergeAcross=""" + (grpReward.Count - 1).ToString() + @""" ss:StyleID=""s69""><Data ss:Type=""String"">Khen thưởng</Data></Cell>");
                stringBuilder.Append(@"</Row>");
                stringBuilder.Append(@"<Row ss:Height=""14.25"" ss:StyleID=""s62"">");
                stringBuilder.Append(@"<Cell ss:StyleID=""s65""><Data ss:Type=""String"">STT</Data></Cell>");

                //Tiêu đề cột
                foreach (var item in grpCV)
                    stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + item.Title + "</Data></Cell>");

                foreach (var item in grpPolitic)
                    stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + item.Title + "</Data></Cell>");

                foreach (var item in grpWorkExp)
                    stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + item.Title + "</Data></Cell>");

                foreach (var item in grpTraining)
                    stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + item.Title + "</Data></Cell>");

                foreach (var item in grpDiploma)
                    stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + item.Title + "</Data></Cell>");
                foreach (var item in grpCerfificate)
                    stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + item.Title + "</Data></Cell>");

                foreach (var item in grpReward)
                    stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + item.Title + "</Data></Cell>");

                stringBuilder.Append(@"</Row>");
                int count = 0;
                // danh sách row
                foreach (var emp in emps)
                {
                    count += 1;
                    stringBuilder.Append(@"<Row ss:AutoFitHeight=""0"" ss:StyleID=""s68"">");
                    stringBuilder.Append(@"<Cell ss:StyleID=""s67""><Data ss:Type=""Number"">" + count.ToString() + @"</Data></Cell>");

                    foreach (var item in grpCV)
                        stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + getValueCell_CV(emp, item.FieldLabel, lstCV) + @"</Data></Cell>");

                    foreach (var item in grpPolitic)
                        stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + getValueCell_CV(emp, item.FieldLabel, lstCV) + @"</Data></Cell>");

                    foreach (var item in grpWorkExp)
                        stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + getValueCell_WorksExp(emp, item.FieldLabel, lstWorkExp) + @"</Data></Cell>");

                    foreach (var item in grpTraining)
                        stringBuilder.Append(@"<Cell ss:StyleID=""s70""><Data ss:Type=""String"">" + getValueCell_Training(emp, item.FieldLabel, lstTraining) + @"</Data></Cell>");

                    foreach (var item in grpDiploma)
                        stringBuilder.Append(@"<Cell ss:StyleID=""s70""><Data ss:Type=""String"">" + getValueCell_Diploma(emp, item.FieldLabel, lstDiploma) + @"</Data></Cell>");
                    foreach (var item in grpCerfificate)
                        stringBuilder.Append(@"<Cell ss:StyleID=""s70""><Data ss:Type=""String"">" + getValueCell_Certificate(emp, item.FieldLabel, lstCertificate) + @"</Data></Cell>");

                    foreach (var item in grpReward)
                        stringBuilder.Append(@"<Cell ss:StyleID=""s70""><Data ss:Type=""String"">" + getValueCell_Reward(emp, item.FieldLabel, lstReward) + @"</Data></Cell>");

                    stringBuilder.Append(@"</Row>");
                }


                stringBuilder.Append(@"</Table>
                  <WorksheetOptions xmlns=""urn:schemas-microsoft-com:office:excel"">
                   <PageSetup>
                    <Header x:Margin=""0.3""/>
                    <Footer x:Margin=""0.3""/>
                    <PageMargins x:Bottom=""0.75"" x:Left=""0.7"" x:Right=""0.7"" x:Top=""0.75""/>
                   </PageSetup>
                   <Selected/>
                   <Panes>
                    <Pane>
                     <Number>3</Number>
                     <ActiveRow>8</ActiveRow>
                     <ActiveCol>6</ActiveCol>
                    </Pane>
                   </Panes>
                   <ProtectObjects>False</ProtectObjects>
                   <ProtectScenarios>False</ProtectScenarios>
                  </WorksheetOptions>
                 </Worksheet>
                </Workbook>
                ");

                return stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult ExportExcelProfile(GroupCheckBoxBO data = null)
        {
            TempData["DownloadExcel_Profile"] = getDataExportExcel(data);
            return Json(new { success = true, responseText = "get data successfuly" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportInfoDepartment(Guid? departmentId)
        {
            if (departmentId.HasValue && departmentId.Value != Guid.Empty)
            {
                var department = DepartmentService.GetById(departmentId.Value);
                StringBuilder strHTMLContent = new StringBuilder();
                strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><meta charset=\"UTF-8\"></head><body>");
                strHTMLContent.Append("<center><b style=\font-size:40px;\">" + department.Name + "<b></center><br>" + department.Functions + department.Task + department.Note);
                strHTMLContent.Append("</body></html>");
                TempData["Info_Department"] = strHTMLContent;
                TempData["Department_Name"] = department.Name;
            }
            return Json(new { success = true, responseText = "get data successfuly" }, JsonRequestBehavior.AllowGet);
        }
        private void CreateDocument(string Name, string Functions, string Task, string Note)
        {
            try
            {
                //Create an instance for word app  
                Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();
                //Set animation status for word application  
                winword.ShowAnimation = false;
                //Set status for word application is to be visible or not.  
                winword.Visible = false;
                //Create a missing variable for missing value  
                object missing = Missing.Value;
                //Create a new document  
                Microsoft.Office.Interop.Word.Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);
                //Add header into the document  
                //foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
                //{
                //    //Get the header range and add the header details.  
                //    Microsoft.Office.Interop.Word.Range headerRange = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                //    headerRange.Fields.Add(headerRange, Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage);
                //    headerRange.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
                //    headerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdBlue;
                //    headerRange.Font.Size = 10;
                //    headerRange.Text = "Header text goes here";
                //}
                //Add the footers into the document  
                //foreach (Microsoft.Office.Interop.Word.Section wordSection in document.Sections)
                //{
                //    //Get the footer range and add the footer details.  
                //    Microsoft.Office.Interop.Word.Range footerRange = wordSection.Footers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                //    footerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdDarkRed;
                //    footerRange.Font.Size = 10;
                //    footerRange.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
                //    footerRange.Text = "Footer text goes here";
                //}
                //adding text to document  
                document.Content.SetRange(0, 0);
                document.Content.Text = Name + Environment.NewLine;
                //Add paragraph with Heading 1 style  
                Microsoft.Office.Interop.Word.Paragraph para1 = document.Content.Paragraphs.Add(ref missing);
                object styleHeading1 = "Heading 1";
                para1.Range.set_Style(ref styleHeading1);
                para1.Range.Text = Functions;
                para1.Range.InsertParagraphAfter();
                //Add paragraph with Heading 2 style  
                Microsoft.Office.Interop.Word.Paragraph para2 = document.Content.Paragraphs.Add(ref missing);
                object styleHeading2 = "Heading 2";
                para2.Range.set_Style(ref styleHeading2);
                para2.Range.Text = Task;
                para2.Range.InsertParagraphAfter();
                //Add paragraph with Heading 3 style  
                Microsoft.Office.Interop.Word.Paragraph para3 = document.Content.Paragraphs.Add(ref missing);
                object styleHeading3 = "Heading 3";
                para2.Range.set_Style(ref styleHeading3);
                para2.Range.Text = Note;
                para2.Range.InsertParagraphAfter();
                //Create a 5X5 table and insert some dummy record  
                Microsoft.Office.Interop.Word.Table firstTable = document.Tables.Add(para1.Range, 5, 5, ref missing, ref missing);
                firstTable.Borders.Enable = 1;
                foreach (Microsoft.Office.Interop.Word.Row row in firstTable.Rows)
                {
                    foreach (Microsoft.Office.Interop.Word.Cell cell in row.Cells)
                    {
                        //Header row  
                        if (cell.RowIndex == 1)
                        {
                            cell.Range.Text = "Column " + cell.ColumnIndex.ToString();
                            cell.Range.Font.Bold = 1;
                            //other format properties goes here  
                            cell.Range.Font.Name = "verdana";
                            cell.Range.Font.Size = 10;
                            //cell.Range.Font.ColorIndex = WdColorIndex.wdGray25;                              
                            cell.Shading.BackgroundPatternColor = Microsoft.Office.Interop.Word.WdColor.wdColorGray25;
                            //Center alignment for the Header cells  
                            cell.VerticalAlignment = Microsoft.Office.Interop.Word.WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                            cell.Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;

                        }
                        //Data row  
                        else
                        {
                            cell.Range.Text = (cell.RowIndex - 2 + cell.ColumnIndex).ToString();
                        }
                    }
                }
                //Save the document  
                object filename = @"D:\temp1.docx";
                document.SaveAs2(ref filename);
                document.Close(ref missing, ref missing, ref missing);
                document = null;
                winword.Quit(ref missing, ref missing, ref missing);
                winword = null;
            }
            catch (Exception ex)
            {

            }
        }
        public void DownloadFileInfoDepartment()
        {
            var strHTMLContent = "<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><meta charset=\"UTF-8\"></head><body><center><table style=\"width: 600px;padding: 20px 0px\" cellpadding=\"0\" cellspacing=\"0\"><tr><td style=\"width: 300px;text-align: center;font-size: 15px;\"><b>@CompanyName@</b></td><td style=\"width: 300px;\"><center><img src=\"@FullPathImage@\" style=\"width: 100px\"></center></td></tr></table></center> <br> " + TempData["Info_Department"] + " </body></html>";
            #region get CompanyName From config
            var deparmentname = ConfigTableService.GetByConfigKey("CompanyName");
            var NameCompany = string.IsNullOrEmpty(deparmentname) ? "Vinh Khoa Company" : deparmentname;
            #endregion
            var FullPathImage = HttpContext.Request.UrlReferrer.OriginalString + "/Content/images/Logo.png";
            strHTMLContent = strHTMLContent.Replace("@CompanyName@", NameCompany);
            strHTMLContent = strHTMLContent.Replace("@FullPathImage@", FullPathImage);
            Response.Clear();
            Response.ContentType = "application/msword";
            Response.Charset = "UTF-8";
            Response.ContentEncoding = UnicodeEncoding.UTF8;
            string strFileName = TempData["Department_Name"] + ".doc";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName);
            Response.Write(strHTMLContent);
            Response.End();
        }
        public void DownloadProfile()
        {
            var data = TempData["DownloadExcel_Profile"];
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=DanhSachNhanSu.xls");
            Response.Write(data);
            Response.End();
        }
        public ActionResult LoadEmployeesQuery(StoreRequestParameters parameter, string query = default(string))
        {
            try
            {
                var data = new List<EmployeeBO>();
                var count = 0;
                data = EmployeeService.GetAllHasRole(parameter.Page, parameter.Limit, out count, query).ToList();
                return this.Store(data, count);
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult Create(EmployeeBO item)
        {
            var isAdmin = UserRoleService.RoleExits("Administrator");
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckRoleForDeparment_Edit(item.DepartmentId);
            if (!isAdmin && !titleRole)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thêm mới nhân viên trên đơn vị đã chọn"
                });
                return this.Direct();
            }
            try
            {
                var resultOut = true;
                item.Avatar = item.FileAvatar.Data;
                //validate password
                string outputvalidate = "";
                if (!Common.Utilities.ValidatePassword(item.Password, out outputvalidate))
                {
                    this.ShowNotify(outputvalidate);
                    return this.Direct(result: false);
                }
                if (!Common.Utilities.IsPhoneNumber(item.PhoneNumber, out outputvalidate))
                {
                    this.ShowNotify(outputvalidate);
                    return this.Direct(result: false);
                }
                if (item.Employee != null && item.Employee.Id != Guid.Empty)
                    item.Parent = item.Employee.Id;
                var id = EmployeeService.Insert(item);
                if (item.UserName != "" && item.Password.Trim() != "")
                {
                    var user = new UserBO()
                    {
                        Id = id,
                        FullName = item.Name,
                        UserName = item.UserName,
                        Password = item.Password,
                        IsExistEmployee = true,
                    };
                    VPHumanProfileCuriculmViateBO vPHumanProfileCuriculmViateBO = new VPHumanProfileCuriculmViateBO
                    {
                        HumanEmployeeId = id
                    };
                    _vPHumanProfileCuriculmViateService.Insert(vPHumanProfileCuriculmViateBO);
                    //var check = userManager.FindById(id).Id != Guid.Empty;
                    var check = userManager.FindByName(user.UserName) == null ? false : true;
                    IdentityResult result = new IdentityResult();
                    if (!check)
                    {
                        result = userManager.Create(user, user.Password);
                        var success = result.Succeeded;
                        if (!success)
                        {
                            EmployeeService.Delete(id);
                            this.ShowNotify(Common.Resource.SystemFailure);
                        }
                        SetEmployeeTitle(id, item.TitleId);
                    }
                    else
                    {
                        EmployeeService.Delete(id);
                        this.ShowNotify(Common.Resource.AccountExist);
                        resultOut = false;
                    }
                }
                return this.Direct(result: resultOut);
            }
            catch (Exception ex)
            {
                return this.Direct(result: false);
            }
        }
        public ActionResult validateExist(string username)
        {
            var check = userManager.FindByName(username) == null ? false : true;
            return this.Direct(result: check);
        }
        public bool Checkupdate(EmployeeBO info, EmployeeBO infonew)
        {
            if (info.Name != infonew.Name)
            {
                return true;
            }
            if (info.PhoneNumber != infonew.PhoneNumber)
            {
                return true;
            }
            if (info.Code != infonew.Code)
            {
                return true;
            }
            if (info.Sex != infonew.Sex)
            {
                return true;
            }
            if (infonew.AvatarUrl != "/Content/Images/underfind.jpg")
            {
                return true;
            }
            if (info.Note != infonew.Note)
            {
                return true;
            }
            if (info.Email != infonew.Email)
            {
                return true;
            }
            if (info.Parent != infonew.Employee.Id)
            {
                return true;
            }
            return false;
        }
        public ActionResult GetDepartmentTitle(string departmentId = "")
        {
            var department = !string.IsNullOrEmpty(departmentId) ? new Guid(departmentId) : Guid.Empty;
            var data = DepartmentTitleService.GetRolesByDepartment(department).OrderBy(x => x.Name);
            return this.Store(data, data.ToList().Count);
        }
        public ActionResult Update(EmployeeBO item)
        {
            var isAdmin = UserRoleService.RoleExits("Administrator");
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckRoleForDeparment_Edit(item.DepartmentId);
            if (!isAdmin && !titleRole)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền chỉnh sửa thông tin nhân viên trên đơn vị đã chọn"
                });
                return this.Direct();
            }
            try
            {
                var model = EmployeeService.GetDetail(item.Id);
                if (!Checkupdate(model, item))
                {
                    return this.Direct(result: true);
                }
                item.Avatar = item.FileAvatar.Data;
                //validate password
                string outputvalidate = "";
                if (UserName == "Admin" && item.Password.Trim() != "")
                {
                    if (!Common.Utilities.ValidatePassword(item.Password, out outputvalidate))
                    {
                        this.ShowNotify(outputvalidate);
                        return this.Direct(result: false);
                    }
                }
                if (!Common.Utilities.IsPhoneNumber(item.PhoneNumber, out outputvalidate))
                {
                    this.ShowNotify(outputvalidate);
                    return this.Direct(result: false);
                }
                var user = new UserBO()
                {
                    Id = item.Id,
                    FullName = item.Name,
                    UserName = item.UserName,
                    Password = item.Password,
                    IsExistEmployee = true,
                };
                var check = userManager.FindById(item.Id) != null;
                IdentityResult result;
                if (check)
                {
                    result = userManager.Update(user);
                    //if (UserName == "Admin" && item.Password.Trim() != "")
                    //{
                    //    userManager.ChangePassword(new Guid(User.Identity.GetUserId()), user.Password, user.NewPassword);
                    //}
                    userManager.RemovePassword(user.Id);
                    userManager.AddPassword(user.Id, user.Password);
                    //userManager.ChangePassword(user.Id, user.Password, user.NewPassword);
                }
                else
                {
                    result = userManager.Create(user, user.Password);
                }
                var success = result.Succeeded;
                if (!success)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
                EmployeeService.Update(item);
                // update hồ sơ
                var profile = VPHumanProfileCuriculmViateService.GetByEmployeeId(item.Id);
                if (profile != null && profile.Id != null && profile.Id != Guid.Empty)
                {
                    profile.OfficePhone = item.PhoneNumber;
                    profile.IsMale = false;
                    if (item.Sex == 1)
                        profile.IsMale = true;
                    VPHumanProfileCuriculmViateService.Update(profile);
                }

                return this.Direct(result: true);
            }
            catch (Exception ex)
            {
                return this.Direct(result: false);
            }
        }
        public bool SetEmployeeTitle(Guid employeeId, Guid titeId)
        {
            try
            {
                var employeeRole = new EmployeeTitleBO();
                employeeRole.EmployeeId = employeeId;
                employeeRole.TitleId = titeId;
                EmployeeTitleService.Insert(employeeRole);
                //return this.Direct(result: true);
                return true;
            }
            catch
            {
                //return this.Direct(result: false);
                return true;
            }
        }
        public ActionResult UpdateEmployeeRole(EmployeeTitleBO employeeRole)
        {
            try
            {
                EmployeeTitleService.Update(employeeRole);
                return this.Direct(result: true);
            }
            catch (Exception ex)
            {

                return this.Direct(result: false);
            }
        }
        public ActionResult Delete(Guid id)
        {
            var isAdmin = UserRoleService.RoleExits("Administrator");
            var isCurrent = UserRoleService.CurrentUser(id);
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckTitleRole_Edit(id);
            if (!isCurrent && !isAdmin && !titleRole)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }
            try
            {
                EmployeeService.Delete(id);
                var user = new UserBO()
                {
                    Id = id
                };
                userManager.Delete(user);
                return this.Direct(result: true);
            }
            catch
            {
                return this.Direct(result: false);
            }
        }
        #endregion

        #region form dùng chung
        public ActionResult GetEmployeeByRoleType(StoreRequestParameters parameter, int roleType = 0, string query = default(string))
        {
            try
            {
                var count = 0;
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var data = EmployeeService.GetEmployeeByRoleType(parameter.Page, parameter.Limit, out count, query, roleType);
                return this.Store(data, count);
            }
            catch
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Danh sách nhân sự cùng chức danh thiết lập trong quy trình để Chuyển thực hiện
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="roleType"></param>
        /// <param name="query"></param>
        /// <param name="departmentId"></param>
        /// <param name="typeProcess"></param>
        /// <returns></returns>
        public ActionResult GetEmployeeByProcess(StoreRequestParameters parameter, int roleType = 0, string query = default(string), string departmentId = default(string), int typeProcess = 0)
        {
            try
            {
                var data = new List<EmployeeBO>();
                var count = 0;
                data = EmployeeService.GetEmployeeByProcess(parameter.Page, parameter.Limit, out count, query, roleType, typeProcess, departmentId).ToList();
                return this.Store(data, count);
            }
            catch
            {
                return this.Direct();
            }
        }

        public ActionResult LoadProcessEmployeeQuery(StoreRequestParameters parameter, string query = default(string), iDAS.Service.Common.Resource.DocumentTypeProcess? process = null, string departmentId = default(string), int order = 0, int? startRole = null, int? singlerole = null, string documentrequest = default(string))
        {
            try
            {
                var data = new List<EmployeeBO>();
                var count = 0;
                if (!string.IsNullOrEmpty(documentrequest))
                {
                    var documentRequest = DocumentRequestService.GetById(new Guid(documentrequest));
                    switch (singlerole.Value)
                    {
                        case 5:
                            if (documentRequest.CheckerBy.HasValue)
                                data.Add(EmployeeService.GetEmployeeGeneralInfo(documentRequest.CheckerBy.Value));
                            break;
                        case 6:
                            if (documentRequest.ApproverBy.HasValue)
                                data.Add(EmployeeService.GetEmployeeGeneralInfo(documentRequest.ApproverBy.Value));
                            break;
                        case 7:
                            if (documentRequest.PromulgaterBy.HasValue)
                                data.Add(EmployeeService.GetEmployeeGeneralInfo(documentRequest.PromulgaterBy.Value));
                            break;
                    }
                }
                if (departmentId != default(string) && process != null && !string.IsNullOrEmpty(departmentId) && data.Count == 0)
                {
                    data = EmployeeService.GetEmployeeInProcessDocument(parameter.Page, parameter.Limit, out count, query, departmentId, (int)process, order, startRole).ToList();
                }
                return this.Store(data, count);
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult LoadCommonEmployees(StoreRequestParameters parameter, string departmentId, string query, bool isFilterDepartment = false)
        {
            try
            {
                var count = 0;
                if (!isFilterDepartment)
                {
                    var employees = EmployeeService.GetAll(parameter.Page, parameter.Limit, out count).ToList();
                    return this.Store(employees, count);
                }
                else
                {
                    var employeebyDepartments = EmployeeService.GetByDepartment(parameter.Page, parameter.Limit, out count, new Guid(departmentId), query).ToList();
                    return this.Store(employeebyDepartments, count);
                }
            }
            catch
            {
            }
            return this.Direct();
        }
        public ActionResult EmployeeView(bool multi = true, string urlLoadData = "")
        {
            ViewData["ModeMulti"] = multi;
            ViewData["UrlLoadData"] = urlLoadData;
            return PartialView();
        }
        public ActionResult EmployeeWindow(bool multi = true, string urlLoadData = "")
        {
            ViewData["ModeMulti"] = multi;
            ViewData["UrlLoadData"] = urlLoadData;
            return new Ext.Net.MVC.PartialViewResult
            {
                ViewData = ViewData,
            };
        }

        public ActionResult LoadEmployeesTitle(string departmentId)
        {
            var department = !string.IsNullOrEmpty(departmentId) ? new Guid(departmentId) : Guid.Empty;
            //List<Guid> lstDepartment = new List<Guid>();
            //lstDepartment = GetDepartmentsListParent(new Guid(departmentId), lstDepartment);
            //var data = new List<TitleBO>();
            //foreach (Guid item in lstDepartment)
            //{
            //    data.AddRange(DepartmentTitleService.GetRolesByDepartment(item));
            //}
            var data = DepartmentTitleService.GetRolesByDepartment(department);
            return this.Store(data, data.ToList().Count);
        }
        #endregion

        public ActionResult GetEmployeeToCreateProfile(StoreRequestParameters parameter)
        {
            try
            {
                var data = new List<EmployeeBO>();
                data = EmployeeService.GetAll().OrderBy(x => x.Name).ToList();
                return this.Store(data);
            }
            catch
            {
                return this.Direct();
            }
        }

    }
}