﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class ProfileContractController : FrontController
    {
        #region   Hồ sơ Hợp đồng
        public ActionResult Index(string id)
        {
            ViewData["EmployeeId"] = id;
            ViewData["EmployeeName"] = EmployeeService.GetById(Guid.Parse(id)).Name;
            return View();
        }
        public PartialViewResult IndexAll(string containerId)
        {

            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult StatisticIndex(string containerId)
        {
            return PartialView();
        }

        public ActionResult LoadProfileContract(StoreRequestParameters parameters, Guid EmployeeID)
        {
            int totalCount;
            var result = VPHumanProfileContractService.GetAllByEmployeeId(parameters.Page, parameters.Limit, out totalCount, EmployeeID);
            foreach (var data in result)
            {
                var typeId = data.Type;
                if (typeId != null && typeId != "" && Guid.TryParse(typeId, out Guid id))
                {
                    data.Type = new ProfileMasterService().GetSingleMasterData("contractType", typeId).Name;
                }
            }
            return this.Store(result, totalCount);
        }

        public ActionResult LoadProfileContractAll(StoreRequestParameters param, string name = "", string contractNo = "", string type = "", string condition = "", string DeparmentId = "")
        {
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileContract_LoadProfileContractAll",
               parameter: new
               {
                   name = !string.IsNullOrEmpty(name) ? name : "",
                   DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                   contractNo = !string.IsNullOrEmpty(contractNo) ? (contractNo == "Tất cả" ? "" : contractNo) : "",
                   type = !string.IsNullOrEmpty(type) ? (type == "Tất cả" ? "" : type) : "",
                   condition = !string.IsNullOrEmpty(condition) ? (condition == "Tất cả" ? "" : condition) : "",
                   pageIndex = param.Page,
                   pageSize = param.Limit
               }
            );
            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            NumberOfContracts = "",
                                            StartDate = "",
                                            EndDate = "",
                                            TypeId = "",
                                            Type = "",
                                            ConditionId = "",
                                            Condition = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            int count = 0;
            if (dataTable != null && dataTable.Tables[0] != null && dataTable.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(dataTable.Tables[1].Rows[0][0].ToString());
                foreach (DataRow data in dataTable.Tables[0].Rows)
                {
                    var employeeId = data["HumanEmployeeId"] != null && !string.IsNullOrEmpty(data["HumanEmployeeId"].ToString()) ? new Guid(data["HumanEmployeeId"].ToString()) : Guid.Empty;
                    var Id = data["Id"] != null && !string.IsNullOrEmpty(data["Id"].ToString()) ? new Guid(data["Id"].ToString()) : Guid.Empty;
                    var files = new VPHumanProfileAttachmentFileService().GetByFile(Id);
                    var FileAttachs = new FileUploadBO()
                    {
                        Files = files.ToList()
                    };
                    customResult.Add(new { 
                        Id = Id,
                        Code = (data["Code"] != null && !string.IsNullOrEmpty(data["Code"].ToString())) ? data["Code"].ToString() : "",
                        RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId)),
                        EmployeeId = employeeId,
                        EmployeeName = (data["employeeName"] != null && !string.IsNullOrEmpty(data["employeeName"].ToString())) ? data["employeeName"].ToString() : "",
                        NumberOfContracts = (data["NumberOfContracts"] != null && !string.IsNullOrEmpty(data["NumberOfContracts"].ToString())) ? data["NumberOfContracts"].ToString() : "",
                        StartDate = (data["StartDate"] != null && !string.IsNullOrEmpty(data["StartDate"].ToString())) ? DateTime.Parse(data["StartDate"].ToString()).ToString("dd/MM/yyyy") : "",
                        EndDate = (data["EndDate"] != null && !string.IsNullOrEmpty(data["EndDate"].ToString())) ? DateTime.Parse(data["EndDate"].ToString()).ToString("dd/MM/yyyy") : "",
                        TypeId = (data["Type"] != null && !string.IsNullOrEmpty(data["Type"].ToString())) ? data["Type"].ToString() : "",
                        Type = (data["typeName"] != null && !string.IsNullOrEmpty(data["typeName"].ToString())) ? data["typeName"].ToString() : "",
                        ConditionId = (data["Condition"] != null && !string.IsNullOrEmpty(data["Condition"].ToString())) ? data["Condition"].ToString() : "",
                        Condition = (data["conditionName"] != null && !string.IsNullOrEmpty(data["conditionName"].ToString())) ? data["conditionName"].ToString() : "",
                        FileAttachs = FileAttachs
                    });
                }
            }
            customResult.RemoveAt(0);
            //customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult, count);
        }

        public void exportexcel(string name = "", string contractNo = "", string type = "", string condition = "", string DeparmentId = "")
        {
            if (DeparmentId == "null")
                DeparmentId = "";
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileContract_LoadProfileContractAll",
               parameter: new
               {
                   name = !string.IsNullOrEmpty(name) ? name : "",
                   DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                   contractNo = !string.IsNullOrEmpty(contractNo) ? (contractNo == "Tất cả" || contractNo == "null" ? "" : contractNo) : "",
                   type = !string.IsNullOrEmpty(type) ? (type == "Tất cả" || type == "null" ? "" : type) : "",
                   condition = !string.IsNullOrEmpty(condition) ? (condition == "Tất cả" || condition == "null" ? "" : condition) : "",
                   pageIndex = -1,
                   pageSize = -1
               }
            );
            foreach (DataRow row in dataTable.Tables[0].Rows)
            {
                row["RoleName"] = string.Join(", ", EmployeeService.GetDeparmentRoleNames(new Guid(row["HumanEmployeeId"].ToString())));
            }
            DataTable tablecopy = dataTable.Tables[0].Copy();
            foreach (DataColumn col in tablecopy.Columns)
            {
                if (col.ColumnName != "employeeName"
                    && col.ColumnName != "NumberOfContracts"
                      && col.ColumnName != "Code"
                    && col.ColumnName != "RoleName"
                    && col.ColumnName != "StartDate"
                    && col.ColumnName != "typeName"
                    && col.ColumnName != "EndDate"
                    && col.ColumnName != "conditionName")
                {
                    dataTable.Tables[0].Columns.Remove(col.ColumnName);
                }
                else
                {
                    if (col.DataType.Name == "DateTime")
                    {
                        dataTable.Tables[0].Columns.Add(col.ColumnName + "_Extra", typeof(String));
                        foreach (DataRow data in dataTable.Tables[0].Rows)
                        {
                            data[col.ColumnName + "_Extra"] = (data[col.ColumnName] != null && !string.IsNullOrEmpty(data[col.ColumnName].ToString())) ? DateTime.Parse(data[col.ColumnName].ToString()).ToString("dd/MM/yyyy") : "";
                        }
                        dataTable.Tables[0].Columns.Remove(col.ColumnName);
                        dataTable.Tables[0].Columns[col.ColumnName + "_Extra"].ColumnName = col.ColumnName;
                    }
                }
            }
            dataTable.Tables[0].Columns["Code"].SetOrdinal(0);
            dataTable.Tables[0].Columns["employeeName"].SetOrdinal(1);
            dataTable.Tables[0].Columns["RoleName"].SetOrdinal(2);
            dataTable.Tables[0].Columns["NumberOfContracts"].SetOrdinal(3);
            dataTable.Tables[0].Columns["typeName"].SetOrdinal(4);
            dataTable.Tables[0].Columns["conditionName"].SetOrdinal(5);
            dataTable.Tables[0].Columns["StartDate"].SetOrdinal(6);
            dataTable.Tables[0].Columns["EndDate"].SetOrdinal(7);

            dataTable.Tables[0].Columns["Code"].ColumnName = "Mã nhân viên";
            dataTable.Tables[0].Columns["employeeName"].ColumnName = "Tên nhân viên";
            dataTable.Tables[0].Columns["RoleName"].ColumnName = "Chức vụ";
            dataTable.Tables[0].Columns["NumberOfContracts"].ColumnName = "Số hợp đồng";
            dataTable.Tables[0].Columns["typeName"].ColumnName = "Loại hợp đồng";
            dataTable.Tables[0].Columns["conditionName"].ColumnName = "Tình trạng hợp đồng";
            dataTable.Tables[0].Columns["StartDate"].ColumnName = "Ngày bắt đầu";
            dataTable.Tables[0].Columns["EndDate"].ColumnName = "Ngày kết thúc";
            Common.Utilities.ExportDataTableToExcel(dataTable.Tables[0], "Danh sách hồ sơ hợp đồng lao động");
        }

        public ActionResult LoadProfileContractNotPaging(Guid EmployeeID)
        {
            var result = VPHumanProfileContractService.GetAllByEmployeeIdNotPaging(EmployeeID);
            var profileMasterService = new ProfileMasterService();
            var employeeService = new EmployeeService();

            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            NumberOfContracts = "",
                                            StartDate = (DateTime?)DateTime.Now,
                                            EndDate = (DateTime?)DateTime.Now,
                                            TypeId = "",
                                            Type = "",
                                            ConditionId = "",
                                            Condition = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            foreach (var data in result)
            {
                var typeId = data.Type;
                var typeName = "";
                if (typeId != null && typeId != "" && Guid.TryParse(typeId, out Guid id))
                {
                    typeName = profileMasterService.GetSingleMasterData("contractType", typeId).Name;
                }

                var conditionId = data.Condition;
                var conditionName = "";
                if (conditionId != null && conditionId != "" && Guid.TryParse(conditionId, out Guid id1))
                {
                    conditionName = profileMasterService.GetSingleMasterData("contractStatus", conditionId).Name;
                }
                var employeeId = data.HumanEmployeeId ?? Guid.Empty;
                var employeeName = "";

                if (employeeId != Guid.Empty)
                {
                    employeeName = employeeService.GetById(employeeId).Name;
                }
                customResult.Add(new { Id = data.Id, EmployeeId = employeeId, EmployeeName = employeeName, NumberOfContracts = data.NumberOfContracts, StartDate = data.StartDate, EndDate = data.EndDate, TypeId = typeId, Type = typeName, ConditionId = data.Condition, Condition = conditionName, FileAttachs = data.FileAttachs });
            }
            customResult.RemoveAt(0);

            return this.Store(customResult);
        }

        public ActionResult DeleteProfileContract(Guid id)
        {
            try
            {
                VPHumanProfileContractService.Delete(id);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileContract").Reload();
            }
            return this.Direct();
        }
        #endregion


        public ActionResult UpdateForm(string ID, Guid EmployeeID)
        {
            var data = new VPHumanProfileContractBO();
            if (!string.IsNullOrEmpty(ID))
            {
                data = VPHumanProfileContractService.GetdataById(new Guid(ID));
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
            else
            {
                data.HumanEmployeeId = EmployeeID;
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="exit"></param>
        /// <returns></returns>
        public ActionResult Update(VPHumanProfileContractBO updateData)
        {
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            // Kiểm tra quyền edit
            if (updateData.HumanEmployeeId != null && s.CheckTitleRole_Edit((Guid)updateData.HumanEmployeeId) == false)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }

            try
            {
                if (updateData.Id == Guid.Empty)
                {
                    VPHumanProfileContractService.HumanProfileContract(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    VPHumanProfileContractService.HumanProfileContract(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                X.GetCmp<Window>("WinContract").Close();
                X.GetCmp<Store>("StoreProfileContract").Reload();
                return this.Direct();
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return RedirectToAction("Index");
            }
        }


        public ActionResult DetailForm(string ID, Guid EmployeeID)
        {
            var data = VPHumanProfileContractService.GetById(new Guid(ID));
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
        }
    }
}
