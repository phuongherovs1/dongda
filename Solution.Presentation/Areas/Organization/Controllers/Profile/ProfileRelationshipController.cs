﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;

namespace iDAS.Presentation.Areas.Organization.Controllers
{

    public class ProfileRelationshipController : FrontController
    {

        #region   Hồ sơ Quan hệ gia đình


        public ActionResult Index(string id)
        {
            ViewData["EmployeeId"] = id;
            ViewData["EmployeeName"] = EmployeeService.GetById(Guid.Parse(id)).Name;
            return View();
        }
        public PartialViewResult IndexAll(string containerId)
        {
            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult LoadProfileRelationshipAll(StoreRequestParameters param, string name = "", string relation = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileRelationship_LoadProfileRelationshipAll",
               parameter: new
               {
                   name = !string.IsNullOrEmpty(name) ? name : "",
                   relation = !string.IsNullOrEmpty(relation) ? (relation == "Tất cả" ? "" : relation) : "",
                   DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                   pageIndex = param.Page,
                   pageSize = param.Limit
               }
            );
            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                             Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            Name = "",
                                            Age = "",
                                            IsMale = "",
                                            RelationId = "",
                                            Relationship = "",
                                            Job = "",
                                            PlaceOfJob = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            int count = 0;
            if (dataTable != null && dataTable.Tables[0] != null && dataTable.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(dataTable.Tables[1].Rows[0][0].ToString());
                foreach (DataRow data in dataTable.Tables[0].Rows)
                {
                    var employeeId = data["HumanEmployeeId"] != null && !string.IsNullOrEmpty(data["HumanEmployeeId"].ToString()) ? new Guid(data["HumanEmployeeId"].ToString()) : Guid.Empty;
                    var Id = data["Id"] != null && !string.IsNullOrEmpty(data["Id"].ToString()) ? new Guid(data["Id"].ToString()) : Guid.Empty;
                    var files = new VPHumanProfileAttachmentFileService().GetByFile(Id);
                    var FileAttachs = new FileUploadBO()
                    {
                        Files = files.ToList()
                    };
                    customResult.Add(new { 
                        Id = Id,
                        Code = (data["Code"] != null && !string.IsNullOrEmpty(data["Code"].ToString())) ? data["Code"].ToString() : "",
                        RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId)),
                        EmployeeId = employeeId,
                        EmployeeName = (data["employeeName"] != null && !string.IsNullOrEmpty(data["employeeName"].ToString())) ? data["employeeName"].ToString() : "",
                        Name = (data["Name"] != null && !string.IsNullOrEmpty(data["Name"].ToString())) ? data["Name"].ToString() : "",
                        Age = (data["Age"] != null && !string.IsNullOrEmpty(data["Age"].ToString())) ? data["Age"].ToString() : "",
                        IsMale = (data["IsMale"] != null && !string.IsNullOrEmpty(data["IsMale"].ToString())) ? data["IsMale"].ToString() : "",
                        RelationId = (data["Relationship"] != null && !string.IsNullOrEmpty(data["Relationship"].ToString())) ? data["Relationship"].ToString() : "",
                        Relationship = (data["relationName"] != null && !string.IsNullOrEmpty(data["relationName"].ToString())) ? data["relationName"].ToString() : "",
                        Job = (data["Job"] != null && !string.IsNullOrEmpty(data["Job"].ToString())) ? data["Job"].ToString() : "",
                        PlaceOfJob = (data["PlaceOfJob"] != null && !string.IsNullOrEmpty(data["PlaceOfJob"].ToString())) ? data["PlaceOfJob"].ToString() : "",
                        FileAttachs = FileAttachs
                    });
                }
            }
            customResult.RemoveAt(0);
            //Sắp xếp theo tên nhân viên, nếu trùng thì xếp theo Id
            //customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult,count);
        }
        public void exportexcel(string name = "", string relation = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            if (DeparmentId == "null")
                DeparmentId = "";
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileRelationship_LoadProfileRelationshipAll",
               parameter: new
               {
                   name = !string.IsNullOrEmpty(name) ? name : "",
                   DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                   relation = !string.IsNullOrEmpty(relation) ? (relation == "Tất cả" || relation  == "null" ? "" : relation) : "",
                   pageIndex = -1,
                   pageSize = -1
               }
            );
            foreach (DataRow row in dataTable.Tables[0].Rows)
            {
                row["RoleName"] = string.Join(", ", EmployeeService.GetDeparmentRoleNames(new Guid(row["HumanEmployeeId"].ToString())));
            }
            DataTable tablecopy = dataTable.Tables[0].Copy();
            foreach (DataColumn col in tablecopy.Columns)
            {
                if (col.ColumnName != "employeeName"
                    && col.ColumnName != "Name"
                     && col.ColumnName != "Code"
                    && col.ColumnName != "RoleName"
                    && col.ColumnName != "Age"
                    && col.ColumnName != "IsMale"
                    && col.ColumnName != "relationName"
                    && col.ColumnName != "Job"
                    && col.ColumnName != "PlaceOfJob")
                {
                    dataTable.Tables[0].Columns.Remove(col.ColumnName);
                }
                else
                {
                    if (col.DataType.Name == "DateTime")
                    {
                        dataTable.Tables[0].Columns.Add(col.ColumnName + "_Extra", typeof(String));
                        foreach (DataRow data in dataTable.Tables[0].Rows)
                        {
                            data[col.ColumnName + "_Extra"] = (data[col.ColumnName] != null && !string.IsNullOrEmpty(data[col.ColumnName].ToString())) ? DateTime.Parse(data[col.ColumnName].ToString()).ToString("dd/MM/yyyy") : "";
                        }
                        dataTable.Tables[0].Columns.Remove(col.ColumnName);
                        dataTable.Tables[0].Columns[col.ColumnName + "_Extra"].ColumnName = col.ColumnName;
                    }
                }
            }
            dataTable.Tables[0].Columns["Code"].SetOrdinal(0);
            dataTable.Tables[0].Columns["employeeName"].SetOrdinal(1);
            dataTable.Tables[0].Columns["RoleName"].SetOrdinal(2);
            dataTable.Tables[0].Columns["Name"].SetOrdinal(3);
            dataTable.Tables[0].Columns["Age"].SetOrdinal(4);
            dataTable.Tables[0].Columns["IsMale"].SetOrdinal(5);
            dataTable.Tables[0].Columns["relationName"].SetOrdinal(6);
            dataTable.Tables[0].Columns["Job"].SetOrdinal(7);
            dataTable.Tables[0].Columns["PlaceOfJob"].SetOrdinal(8);

            dataTable.Tables[0].Columns["Code"].ColumnName = "Mã nhân viên";
            dataTable.Tables[0].Columns["employeeName"].ColumnName = "Tên nhân viên";
            dataTable.Tables[0].Columns["RoleName"].ColumnName = "Chức vụ";
            dataTable.Tables[0].Columns["Name"].ColumnName = "Họ tên người thân";
            dataTable.Tables[0].Columns["Age"].ColumnName = "Tuổi";
            dataTable.Tables[0].Columns["IsMale"].ColumnName = "Giới tính";
            dataTable.Tables[0].Columns["relationName"].ColumnName = "Quan hệ";
            dataTable.Tables[0].Columns["Job"].ColumnName = "Nghề nghiệp";
            dataTable.Tables[0].Columns["PlaceOfJob"].ColumnName = "Nơi công tác";
            Common.Utilities.ExportDataTableToExcel(dataTable.Tables[0], "Danh sách hồ sơ quan hệ gia đình");
        }
        public ActionResult LoadProfileRelationship(StoreRequestParameters parameters, Guid EmployeeID)
        {
            int totalCount;
            var result = VPHumanProfileRelationshipService.GetAllByEmployeeId(parameters.Page, parameters.Limit, out totalCount, EmployeeID);

            foreach (var data in result)
            {
                var relationId = data.Relationship;
                if (relationId != null && relationId != "" && Guid.TryParse(relationId, out Guid id))
                {
                    data.Relationship = new ProfileMasterService().GetSingleMasterData("family", relationId).Name;
                }
            }
            return this.Store(result, totalCount);
        }

        public ActionResult LoadProfileRelationshipNotPaging(Guid EmployeeID)
        {
            var result = VPHumanProfileRelationshipService.GetAllByEmployeeIdNotPaging(EmployeeID);
            var profileMasterService = new ProfileMasterService();
            var employeeService = new EmployeeService();

            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            Name = "",
                                            Age = (short?)0,
                                            IsMale = (bool?)false,
                                            RelationId = "",
                                            Relationship = "",
                                            Job = "",
                                            PlaceOfJob = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            foreach (var data in result)
            {
                var relationId = data.Relationship;
                var relationName = "";
                if (relationId != null && relationId != "" && Guid.TryParse(relationId, out Guid id))
                {
                    relationName = profileMasterService.GetSingleMasterData("family", relationId).Name;
                }

                var employeeId = data.HumanEmployeeId ?? Guid.Empty;
                var employeeName = "";

                if (employeeId != Guid.Empty)
                {
                    employeeName = employeeService.GetById(employeeId).Name;
                }
                customResult.Add(new { Id = data.Id, EmployeeId = employeeId, EmployeeName = employeeName, Name = data.Name, Age = data.Age, IsMale = data.IsMale, RelationId = relationId, Relationship = relationName, Job = data.Job, PlaceOfJob = data.PlaceOfJob, FileAttachs = data.FileAttachs });
            }
            customResult.RemoveAt(0);
            return this.Store(customResult);
        }

        public ActionResult DeleteProfileRelationship(Guid id)
        {
            try
            {
                VPHumanProfileRelationshipService.Delete(id);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileRelationship").Reload();
            }
            return this.Direct();
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        public ActionResult UpdateForm(string ID, Guid EmployeeID)
        {
            var data = new VPHumanProfileRelationshipBO();
            if (!string.IsNullOrEmpty(ID))
            {
                data = VPHumanProfileRelationshipService.GetdataById(new Guid(ID));
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
            else
            {
                data.HumanEmployeeId = EmployeeID;
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="exit"></param>
        /// <returns></returns>
        public ActionResult Update(VPHumanProfileRelationshipBO updateData)
        {
            try
            {
                if (updateData.Id == Guid.Empty)
                {
                    VPHumanProfileRelationshipService.HumanProfileRelationship(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    VPHumanProfileRelationshipService.HumanProfileRelationship(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                X.GetCmp<Window>("winRelationship").Close();
                X.GetCmp<Store>("StoreProfileRelationship").Reload();
                return this.Direct();

            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return RedirectToAction("Index");
            }
        }

    }
}
