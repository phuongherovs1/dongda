﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;
namespace iDAS.Presentation.Areas.Organization.Controllers
{

    public class ProfileSalaryController : FrontController
    {

        #region   Hồ sơ lương
        public ActionResult Index(string id)
        {
            ViewData["EmployeeId"] = id;
            ViewData["EmployeeName"] = EmployeeService.GetById(Guid.Parse(id)).Name;
            return View();
        }
        public PartialViewResult IndexAll(string containerId)
        {
            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                WrapByScriptTag = false
                //check view
            };
        }
        public ActionResult LoadProfileSalaryAll(StoreRequestParameters param, string name = "", string Codejob = "", string DeparmentId = "")
      {
            //Query dữ liệu thô
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileSalaries_LoadProfileSalaryAll",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    Codejob = !string.IsNullOrEmpty(Codejob) ? Codejob : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    pageIndex = param.Page,
                    pageSize = param.Limit
                }
            );
            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                             Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            Level = "",
                                            CodeJob = "",
                                            Wage = "",
                                            Extra = "",
                                            DateOfApp = "",
                                            DateOfOff = "",
                                            Note = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            int count = 0;
            if (dataTable != null && dataTable.Tables[0] != null && dataTable.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(dataTable.Tables[1].Rows[0][0].ToString());
                foreach (DataRow data in dataTable.Tables[0].Rows)
                {
                    var employeeId = data["HumanEmployeeId"] != null && !string.IsNullOrEmpty(data["HumanEmployeeId"].ToString())? new Guid(data["HumanEmployeeId"].ToString()) : Guid.Empty;
                    var Id = data["Id"] != null && !string.IsNullOrEmpty(data["Id"].ToString()) ? new Guid(data["Id"].ToString()) : Guid.Empty;
                    var files = new VPHumanProfileAttachmentFileService().GetByFile(Id);
                    var FileAttachs = new FileUploadBO()
                    {
                        Files = files.ToList()
                    };
                    customResult.Add(new {
                        Id = Id,
                        Code = (data["Code"] != null && !string.IsNullOrEmpty(data["Code"].ToString())) ? data["Code"].ToString() : "",
                        RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId)),
                        EmployeeId = employeeId,
                        EmployeeName = (data["Name"] != null && !string.IsNullOrEmpty(data["Name"].ToString())) ? data["Name"].ToString() : "",
                        Level = (data["Level"] != null && !string.IsNullOrEmpty(data["Level"].ToString())) ? data["Level"].ToString() : "",
                        CodeJob = (data["CodeJob"] != null && !string.IsNullOrEmpty(data["CodeJob"].ToString())) ? data["CodeJob"].ToString() : "",
                        Wage = (data["Wage"] != null && !string.IsNullOrEmpty(data["Wage"].ToString())) ? data["Wage"].ToString() : "",
                        Extra = (data["Extra"] != null && !string.IsNullOrEmpty(data["Extra"].ToString())) ? data["Extra"].ToString() : "",
                        DateOfApp = (data["DateOfApp"] != null && !string.IsNullOrEmpty(data["DateOfApp"].ToString())) ? DateTime.Parse(data["DateOfApp"].ToString()).ToString("dd/MM/yyyy") : "",
                        DateOfOff = (data["DateOfOff"] != null && !string.IsNullOrEmpty(data["DateOfOff"].ToString())) ? DateTime.Parse(data["DateOfOff"].ToString()).ToString("dd/MM/yyyy") : "",
                        Note = (data["Note"] != null && !string.IsNullOrEmpty(data["Note"].ToString())) ? data["Note"].ToString() : "",
                        FileAttachs = FileAttachs
                    });
                }
            }
            customResult.RemoveAt(0);
            //Sắp xếp theo tên nhân viên, nếu trùng thì xếp theo Id
            //customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult,count);
        }

        public void exportexcel(string name = "", string Codejob = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            if (DeparmentId == "null")
                DeparmentId = "";
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileSalaries_LoadProfileSalaryAll",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    Codejob = !string.IsNullOrEmpty(Codejob) ? Codejob : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    pageIndex = -1,
                    pageSize = -1
                }
            );
            foreach (DataRow row in dataTable.Tables[0].Rows)
            {
                row["RoleName"] = string.Join(", ", EmployeeService.GetDeparmentRoleNames(new Guid(row["HumanEmployeeId"].ToString())));
            }
            DataTable tablecopy = dataTable.Tables[0].Copy();
            foreach (DataColumn col in tablecopy.Columns)
            {
                if (col.ColumnName != "Name"
                    && col.ColumnName != "Level"
                     && col.ColumnName != "Code"
                    && col.ColumnName != "RoleName"
                    && col.ColumnName != "CodeJob"
                    && col.ColumnName != "Wage"
                    && col.ColumnName != "Extra"
                    && col.ColumnName != "DateOfApp"
                    && col.ColumnName != "Note"
                    && col.ColumnName != "DateOfOff")
                {
                    dataTable.Tables[0].Columns.Remove(col.ColumnName);
                }
                else
                {
                    if (col.DataType.Name == "DateTime")
                    {
                        dataTable.Tables[0].Columns.Add(col.ColumnName + "_Extra", typeof(String));
                        foreach (DataRow data in dataTable.Tables[0].Rows)
                        {
                            data[col.ColumnName + "_Extra"] = (data[col.ColumnName] != null && !string.IsNullOrEmpty(data[col.ColumnName].ToString())) ? DateTime.Parse(data[col.ColumnName].ToString()).ToString("dd/MM/yyyy") : "";
                        }
                        dataTable.Tables[0].Columns.Remove(col.ColumnName);
                        dataTable.Tables[0].Columns[col.ColumnName + "_Extra"].ColumnName = col.ColumnName;
                    }
                }
            }
            dataTable.Tables[0].Columns["Code"].SetOrdinal(0);
            dataTable.Tables[0].Columns["Name"].SetOrdinal(1);
            dataTable.Tables[0].Columns["RoleName"].SetOrdinal(2);
            dataTable.Tables[0].Columns["Level"].SetOrdinal(3);
            dataTable.Tables[0].Columns["CodeJob"].SetOrdinal(4);
            dataTable.Tables[0].Columns["Wage"].SetOrdinal(5);
            dataTable.Tables[0].Columns["Extra"].SetOrdinal(6);
            dataTable.Tables[0].Columns["DateOfApp"].SetOrdinal(7);
            dataTable.Tables[0].Columns["DateOfOff"].SetOrdinal(8);
            dataTable.Tables[0].Columns["Note"].SetOrdinal(9);

            dataTable.Tables[0].Columns["Code"].ColumnName = "Mã nhân viên";
            dataTable.Tables[0].Columns["Name"].ColumnName = "Tên nhân viên";
            dataTable.Tables[0].Columns["RoleName"].ColumnName = "Chức vụ";
            dataTable.Tables[0].Columns["Level"].ColumnName = "Mức lương";
            dataTable.Tables[0].Columns["CodeJob"].ColumnName = "Mã chức danh nghề";
            dataTable.Tables[0].Columns["Wage"].ColumnName = "Bậc lương";
            dataTable.Tables[0].Columns["Extra"].ColumnName = "Phụ cấp thâm niên vượt khung";
            dataTable.Tables[0].Columns["DateOfApp"].ColumnName = "Ngày áp dụng";
            dataTable.Tables[0].Columns["DateOfOff"].ColumnName = "Ngày kết thúc";
            dataTable.Tables[0].Columns["Note"].ColumnName = "Ghi chú";
            Common.Utilities.ExportDataTableToExcel(dataTable.Tables[0], "Danh sách hồ sơ lương");
        }

        public ActionResult LoadProfileSalary(StoreRequestParameters parameters, Guid EmployeeID)
        {
            int totalCount;
            var result = VPHumanProfileSalaryService.GetAllByEmployeeId(parameters.Page, parameters.Limit, out totalCount, EmployeeID);
            return this.Store(result, totalCount);
        }

        public ActionResult LoadProfileSalaryNotPaging(Guid EmployeeID)
        {
            var result = VPHumanProfileSalaryService.GetAllByEmployeeIdNotPaging(EmployeeID);
            return this.Store(result);
        }

        public ActionResult DeleteProfileSalary(Guid id)
        {
            try
            {
                VPHumanProfileSalaryService.Delete(id);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileSalary").Reload();
            }
            return this.Direct();
        }
        #endregion

        public ActionResult UpdateForm(string ID, Guid EmployeeID)
        {
            var data = new VPHumanProfileSalaryBO();
            if (!string.IsNullOrEmpty(ID))
            {
                data = VPHumanProfileSalaryService.GetdataById(new Guid(ID));
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
            else
            {
                data.HumanEmployeeId = EmployeeID;
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }

        }
        public ActionResult Update(VPHumanProfileSalaryBO updateData)
        {
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            // Kiểm tra quyền edit
            if (updateData.HumanEmployeeId != null && s.CheckTitleRole_Edit((Guid)updateData.HumanEmployeeId) == false)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }

            try
            {
                if (updateData.Id == Guid.Empty)
                {
                    VPHumanProfileSalaryService.HumanProfileSalary(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    VPHumanProfileSalaryService.HumanProfileSalary(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                X.GetCmp<Window>("winSalary").Close();
                X.GetCmp<Store>("StoreProfileSalary").Reload();
                return this.Direct();

            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return RedirectToAction("Index");
            }
        }

    }
}
