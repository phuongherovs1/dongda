﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using io = System.IO;
using sysnet = System.Net;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using iDAS.Presentation.Common;
using System.Configuration;
using System.Security.Claims;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;
using iDAS.ADO;
using systemIO = System.IO;
using System.IO;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class ProfileCuriculmViateController : FrontController
    {
        public PartialViewResult IndexAll(string containerId)
        {
            var TypeCombobox = new List<Ext.Net.ListItem>();
            TypeCombobox.Add(new Ext.Net.ListItem() { Text = "Nơi sinh", Value = "NS" });
            TypeCombobox.Add(new Ext.Net.ListItem() { Text = "Hộ khẩu", Value = "HK" });
            TypeCombobox.Add(new Ext.Net.ListItem() { Text = "Tôn giáo", Value = "TG" });
            TypeCombobox.Add(new Ext.Net.ListItem() { Text = "Dân tộc", Value = "DT" });
            TypeCombobox.Add(new Ext.Net.ListItem() { Text = "Quốc tịch", Value = "QT" });
            TypeCombobox.Add(new Ext.Net.ListItem() { Text = "Độ tuổi", Value = "KT" });
            Session["cboStatisType_data1"] = TypeCombobox;

            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                WrapByScriptTag = false
                //check view
            };
        }
        public PartialViewResult Index1(Guid Id)
        {
            var data = VPHumanProfileCuriculmViateService.GetById(Id);
            return new PartialViewResult
            {
                ContainerId = "pnMainSystem",
                ViewName = "Index1",
                Model = data,
                //check view
            };
        }

        public ActionResult StatisticIndex(string containerId)
        {
            //var TypeCombobox = new List<Ext.Net.ListItem>();
            //TypeCombobox.Add(new Ext.Net.ListItem() { Text = "Nơi sinh", Value = "NS" });
            //TypeCombobox.Add(new Ext.Net.ListItem() { Text = "Hộ khẩu", Value = "HK" });
            //TypeCombobox.Add(new Ext.Net.ListItem() { Text = "Tôn giáo", Value = "TG" });
            //TypeCombobox.Add(new Ext.Net.ListItem() { Text = "Dân tộc", Value = "DT" });
            //TypeCombobox.Add(new Ext.Net.ListItem() { Text = "Quốc tịch", Value = "QT" });
            //TypeCombobox.Add(new Ext.Net.ListItem() { Text = "Độ tuổi", Value = "KT" });
            //ViewData["cboStatisType_data"] = TypeCombobox;

            return PartialView();
        }

        public ActionResult LoadProfileCVAll(StoreRequestParameters param, string name = "", string nation = "", string religion = "", string ethnic = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileCuriculmViates_LoadProfileSalaryAll",
               parameter: new
               {
                   name = !string.IsNullOrEmpty(name) ? name : "",
                   nation = !string.IsNullOrEmpty(nation) ? (nation == "Tất cả" ? "" : nation) : "",
                   religion = !string.IsNullOrEmpty(religion) ? (religion == "Tất cả" ? "" : religion) : "",
                   ethnic = !string.IsNullOrEmpty(ethnic) ? (ethnic == "Tất cả" ? "" : ethnic) : "",
                   DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                   pageIndex = param.Page,
                   pageSize = param.Limit
               }
           );
            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            NationId = "",
                                            Nationality = "",
                                            ReligionId = "",
                                            Religion = "",
                                            EthnicId = "",
                                            Ethnic = "",
                                            DOB = "",
                                            PlaceOfBirth = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            int count = 0;
            if (dataTable != null && dataTable.Tables[0] != null && dataTable.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(dataTable.Tables[1].Rows[0][0].ToString());
                foreach (DataRow data in dataTable.Tables[0].Rows)
                {
                    var employeeId = data["HumanEmployeeId"] != null && !string.IsNullOrEmpty(data["HumanEmployeeId"].ToString()) ? new Guid(data["HumanEmployeeId"].ToString()) : Guid.Empty;
                    var Id = data["Id"] != null && !string.IsNullOrEmpty(data["Id"].ToString()) ? new Guid(data["Id"].ToString()) : Guid.Empty;
                    var files = new VPHumanProfileAttachmentFileService().GetByFile(Id);
                    var FileAttachs = new FileUploadBO()
                    {
                        Files = files.ToList()
                    };
                    VPHumanProfileCuriculmViateBO BOItem = new VPHumanProfileCuriculmViateBO();
                    BOItem.PlaceOfBirthCity = data["PlaceOfBirthCity"] != null && !string.IsNullOrEmpty(data["PlaceOfBirthCity"].ToString()) ? data["PlaceOfBirthCity"].ToString() : "";
                    BOItem.PlaceOfBirthDistrict = data["PlaceOfBirthDistrict"] != null && !string.IsNullOrEmpty(data["PlaceOfBirthDistrict"].ToString()) ? data["PlaceOfBirthDistrict"].ToString() : "";
                    BOItem.PlaceOfBirthCommune = data["PlaceOfBirthCommune"] != null && !string.IsNullOrEmpty(data["PlaceOfBirthCommune"].ToString()) ? data["PlaceOfBirthCommune"].ToString() : "";
                    BOItem.HomeTownCity = data["HomeTownCity"] != null && !string.IsNullOrEmpty(data["HomeTownCity"].ToString()) ? data["HomeTownCity"].ToString() : "";
                    BOItem.HomeTownDistrict = data["HomeTownDistrict"] != null && !string.IsNullOrEmpty(data["HomeTownDistrict"].ToString()) ? data["HomeTownDistrict"].ToString() : "";
                    BOItem.HomeTownCommune = data["HomeTownCommune"] != null && !string.IsNullOrEmpty(data["HomeTownCommune"].ToString()) ? data["HomeTownCommune"].ToString() : "";
                    customResult.Add(new
                    {
                        Id = Id,
                        Code = (data["Code"] != null && !string.IsNullOrEmpty(data["Code"].ToString())) ? data["Code"].ToString() : "",
                        RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId)),
                        EmployeeId = employeeId,
                        EmployeeName = (data["EmployeeName"] != null && !string.IsNullOrEmpty(data["EmployeeName"].ToString())) ? data["EmployeeName"].ToString() : "",
                        NationId = (data["Nationality"] != null && !string.IsNullOrEmpty(data["Nationality"].ToString())) ? data["Nationality"].ToString() : "",
                        Nationality = (data["nationName"] != null && !string.IsNullOrEmpty(data["nationName"].ToString())) ? data["nationName"].ToString() : "",
                        ReligionId = (data["Religion"] != null && !string.IsNullOrEmpty(data["Religion"].ToString())) ? data["Religion"].ToString() : "",
                        Religion = (data["religionName"] != null && !string.IsNullOrEmpty(data["religionName"].ToString())) ? data["religionName"].ToString() : "",
                        EthnicId = (data["People"] != null && !string.IsNullOrEmpty(data["People"].ToString())) ? data["People"].ToString() : "",
                        Ethnic = (data["ethnicName"] != null && !string.IsNullOrEmpty(data["ethnicName"].ToString())) ? data["ethnicName"].ToString() : "",
                        DOB = (data["DateOfBirth"] != null && !string.IsNullOrEmpty(data["DateOfBirth"].ToString())) ? DateTime.Parse(data["DateOfBirth"].ToString()).ToString("dd/MM/yyyy") : "",
                        PlaceOfBirth = ConvertAddress(BOItem)[0],
                        FileAttachs = FileAttachs
                    });
                }
            }
            customResult.RemoveAt(0);
            //Sắp xếp theo tên nhân viên, nếu trùng tên thì xếp theo Id
            //customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult, count);
        }
        public void exportexcel(string name = "", string nation = "", string religion = "", string ethnic = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            //var result = VPHumanProfileCuriculmViateService.GetAll();
            if (DeparmentId == "null")
                DeparmentId = "";
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileCuriculmViates_LoadProfileSalaryAll",
               parameter: new
               {
                   name = !string.IsNullOrEmpty(name) ? name : "",
                   nation = !string.IsNullOrEmpty(nation) ? (nation == "Tất cả" ? "" : nation) : "",
                   religion = !string.IsNullOrEmpty(religion) ? (religion == "Tất cả" ? "" : religion) : "",
                   ethnic = !string.IsNullOrEmpty(ethnic) ? (ethnic == "Tất cả" ? "" : ethnic) : "",
                   DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                   pageIndex = -1,
                   pageSize = -1
               }
           );
            foreach (DataRow row in dataTable.Tables[0].Rows)
            {
                row["RoleName"] = string.Join(", ", EmployeeService.GetDeparmentRoleNames(new Guid(row["HumanEmployeeId"].ToString())));
            }
            dataTable.Tables[0].Columns.Add("PlaceOfBirth", typeof(string));
            foreach (DataRow data in dataTable.Tables[0].Rows)
            {
                VPHumanProfileCuriculmViateBO BOItem = new VPHumanProfileCuriculmViateBO();
                BOItem.PlaceOfBirthCity = data["PlaceOfBirthCity"] != null && !string.IsNullOrEmpty(data["PlaceOfBirthCity"].ToString()) ? data["PlaceOfBirthCity"].ToString() : "";
                BOItem.PlaceOfBirthDistrict = data["PlaceOfBirthDistrict"] != null && !string.IsNullOrEmpty(data["PlaceOfBirthDistrict"].ToString()) ? data["PlaceOfBirthDistrict"].ToString() : "";
                BOItem.PlaceOfBirthCommune = data["PlaceOfBirthCommune"] != null && !string.IsNullOrEmpty(data["PlaceOfBirthCommune"].ToString()) ? data["PlaceOfBirthCommune"].ToString() : "";
                BOItem.HomeTownCity = data["HomeTownCity"] != null && !string.IsNullOrEmpty(data["HomeTownCity"].ToString()) ? data["HomeTownCity"].ToString() : "";
                BOItem.HomeTownDistrict = data["HomeTownDistrict"] != null && !string.IsNullOrEmpty(data["HomeTownDistrict"].ToString()) ? data["HomeTownDistrict"].ToString() : "";
                BOItem.HomeTownCommune = data["HomeTownCommune"] != null && !string.IsNullOrEmpty(data["HomeTownCommune"].ToString()) ? data["HomeTownCommune"].ToString() : "";
                data["PlaceOfBirth"] = ConvertAddress(BOItem)[0];
            }
            DataTable tablecopy = dataTable.Tables[0].Copy();
            foreach (DataColumn col in tablecopy.Columns)
            {
                if (col.ColumnName != "EmployeeName"
                    && col.ColumnName != "nationName"
                     && col.ColumnName != "Code"
                    && col.ColumnName != "RoleName"
                    && col.ColumnName != "religionName"
                    && col.ColumnName != "ethnicName"
                    && col.ColumnName != "DateOfBirth"
                    && col.ColumnName != "PlaceOfBirth")
                {
                    dataTable.Tables[0].Columns.Remove(col.ColumnName);
                }
                else
                {
                    if (col.DataType.Name == "DateTime")
                    {
                        dataTable.Tables[0].Columns.Add(col.ColumnName + "_Extra", typeof(String));
                        foreach (DataRow data in dataTable.Tables[0].Rows)
                        {
                            data[col.ColumnName + "_Extra"] = (data[col.ColumnName] != null && !string.IsNullOrEmpty(data[col.ColumnName].ToString())) ? DateTime.Parse(data[col.ColumnName].ToString()).ToString("dd/MM/yyyy") : "";
                        }
                        dataTable.Tables[0].Columns.Remove(col.ColumnName);
                        dataTable.Tables[0].Columns[col.ColumnName + "_Extra"].ColumnName = col.ColumnName;
                    }
                }
            }
            dataTable.Tables[0].Columns["Code"].SetOrdinal(0);
            dataTable.Tables[0].Columns["Code"].ColumnName = "Mã nhân viên";
            dataTable.Tables[0].Columns["RoleName"].SetOrdinal(2);
            dataTable.Tables[0].Columns["RoleName"].ColumnName = "Chức vụ";
            dataTable.Tables[0].Columns["EmployeeName"].SetOrdinal(1);
            dataTable.Tables[0].Columns["EmployeeName"].ColumnName = "Tên nhân viên";
            dataTable.Tables[0].Columns["nationName"].SetOrdinal(3);
            dataTable.Tables[0].Columns["nationName"].ColumnName = "Quốc tịch";
            dataTable.Tables[0].Columns["religionName"].SetOrdinal(4);
            dataTable.Tables[0].Columns["religionName"].ColumnName = "Tôn giáo";
            dataTable.Tables[0].Columns["ethnicName"].SetOrdinal(5);
            dataTable.Tables[0].Columns["ethnicName"].ColumnName = "Dân tộc";
            dataTable.Tables[0].Columns["DateOfBirth"].SetOrdinal(6);
            dataTable.Tables[0].Columns["DateOfBirth"].ColumnName = "Ngày sinh";
            dataTable.Tables[0].Columns["PlaceOfBirth"].SetOrdinal(7);
            dataTable.Tables[0].Columns["PlaceOfBirth"].ColumnName = "Nơi sinh";
            Common.Utilities.ExportDataTableToExcel(dataTable.Tables[0], "Danh sách hồ sơ lý lịch");
            ////Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            //var customResult = (new[] { new {
            //                                Id = Guid.Empty,
            //                                EmployeeId = Guid.Empty,
            //                                EmployeeName = "",
            //                                NationId = "",
            //                                Nationality = "",
            //                                ReligionId = "",
            //                                Religion = "",
            //                                EthnicId = "",
            //                                Ethnic = "",
            //                                DOB = DateTime.Now,
            //                                PlaceOfBirth = "",
            //                                FileAttachs = new FileUploadBO()
            //                                }}).ToList();
            ////Chuyển các trường guid sang string
            //if (dataTable != null && dataTable.Tables[0] != null && dataTable.Tables[0].Rows.Count > 0)
            //{
            //    foreach (DataRow data in dataTable.Tables[0].Rows)
            //    {
            //        var employeeId = data["HumanEmployeeId"] != null && !string.IsNullOrEmpty(data["HumanEmployeeId"].ToString()) ? new Guid(data["HumanEmployeeId"].ToString()) : Guid.Empty;
            //        var Id = data["Id"] != null && !string.IsNullOrEmpty(data["Id"].ToString()) ? new Guid(data["Id"].ToString()) : Guid.Empty;
            //        var files = new VPHumanProfileAttachmentFileService().GetByFile(Id);
            //        var FileAttachs = new FileUploadBO()
            //        {
            //            Files = files.ToList()
            //        };
            //        VPHumanProfileCuriculmViateBO BOItem = new VPHumanProfileCuriculmViateBO();
            //        BOItem.PlaceOfBirthCity = data["PlaceOfBirthCity"] != null && !string.IsNullOrEmpty(data["PlaceOfBirthCity"].ToString()) ? data["PlaceOfBirthCity"].ToString() : "";
            //        BOItem.PlaceOfBirthDistrict = data["PlaceOfBirthDistrict"] != null && !string.IsNullOrEmpty(data["PlaceOfBirthDistrict"].ToString()) ? data["PlaceOfBirthDistrict"].ToString() : "";
            //        BOItem.PlaceOfBirthCommune = data["PlaceOfBirthCommune"] != null && !string.IsNullOrEmpty(data["PlaceOfBirthCommune"].ToString()) ? data["PlaceOfBirthCommune"].ToString() : "";
            //        BOItem.HomeTownCity = data["HomeTownCity"] != null && !string.IsNullOrEmpty(data["HomeTownCity"].ToString()) ? data["HomeTownCity"].ToString() : "";
            //        BOItem.HomeTownDistrict = data["HomeTownDistrict"] != null && !string.IsNullOrEmpty(data["HomeTownDistrict"].ToString()) ? data["HomeTownDistrict"].ToString() : "";
            //        BOItem.HomeTownCommune = data["HomeTownCommune"] != null && !string.IsNullOrEmpty(data["HomeTownCommune"].ToString()) ? data["HomeTownCommune"].ToString() : "";
            //        customResult.Add(new
            //        {
            //            Id = Id,
            //            EmployeeId = employeeId,
            //            EmployeeName = (data["EmployeeName"] != null && !string.IsNullOrEmpty(data["EmployeeName"].ToString())) ? data["EmployeeName"].ToString() : "",
            //            NationId = (data["Nationality"] != null && !string.IsNullOrEmpty(data["Nationality"].ToString())) ? data["Nationality"].ToString() : "",
            //            Nationality = (data["nationName"] != null && !string.IsNullOrEmpty(data["nationName"].ToString())) ? data["nationName"].ToString() : "",
            //            ReligionId = (data["Religion"] != null && !string.IsNullOrEmpty(data["Religion"].ToString())) ? data["Religion"].ToString() : "",
            //            Religion = (data["religionName"] != null && !string.IsNullOrEmpty(data["religionName"].ToString())) ? data["religionName"].ToString() : "",
            //            EthnicId = (data["People"] != null && !string.IsNullOrEmpty(data["People"].ToString())) ? data["People"].ToString() : "",
            //            Ethnic = (data["ethnicName"] != null && !string.IsNullOrEmpty(data["ethnicName"].ToString())) ? data["ethnicName"].ToString() : "",
            //            DOB = (data["DateOfBirth"] != null && !string.IsNullOrEmpty(data["DateOfBirth"].ToString())) ? DateTime.Parse(data["DateOfBirth"].ToString()) : DateTime.Now,
            //            PlaceOfBirth = ConvertAddress(BOItem)[0],
            //            FileAttachs = FileAttachs
            //        });
            //    }
            //}
            //customResult.RemoveAt(0);

            ////Filter dữ liệu 
            ////if (name != "") customResult = customResult.FindAll(f => f.EmployeeName.ToLower().Contains(name.ToLower()));
            ////if (nation != "" && nation != "Tất cả") customResult = customResult.FindAll(f => f.NationId!= null && f.NationId.ToLower() == nation.ToLower());
            ////if (religion != "" && religion != "Tất cả") customResult = customResult.FindAll(f => f.ReligionId != null && f.ReligionId.ToLower() == religion.ToLower());
            ////if (ethnic != "" && ethnic != "Tất cả") customResult = customResult.FindAll(f => f.EthnicId != null && f.EthnicId.ToLower() == ethnic.ToLower());

            ////Sắp xếp theo tên nhân viên, nếu trùng tên thì xếp theo Id
            //customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            //List<string> idemployee = new List<string>();
            //foreach (var item in customResult)
            //{
            //    idemployee.Add(item.EmployeeId.ToString());
            //}
            //this.ExportSummaryRelityEmployee(idemployee);
        }

        public ActionResult ProfileForm(Guid id)
        {
            var exist = UserRoleService.RoleExits("ViewEmployeeProfile");
            var isAdmin = UserRoleService.RoleExits("Administrator");
            var isCurrent = UserRoleService.CurrentUser(id);
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckTitleRole_View(id);

            if (exist || isCurrent || isAdmin || titleRole)
            {
                var result = new EmployeeBO { Id = id };
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Profile", Model = result };
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền truy cập chức năng này!"
                });
                return this.Direct();
            }
        }

        public ActionResult FormPreviewProfileEmployee(Guid employeeId)
        {
            ViewData["EmployeeId"] = employeeId;
            DataTable dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryTable("GEtProfile_ExportWord",
               parameter: new
               {
                   Id = employeeId
               }
            );
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                var fullpath = AppDomain.CurrentDomain.BaseDirectory + "\\Temp\\2CTemplate.html";
                string ContentHTML = systemIO.File.ReadAllText(fullpath);
                foreach (DataColumn col in dataTable.Columns)
                {
                    if (dataTable.Rows[0][col.ColumnName] != null && !string.IsNullOrEmpty(dataTable.Rows[0][col.ColumnName].ToString()))
                    {
                        ContentHTML = ContentHTML.Replace("@" + col.ColumnName + "@", "<b>" + dataTable.Rows[0][col.ColumnName].ToString() + "</b>");
                    }
                    else
                    {
                        ContentHTML = ContentHTML.Replace("@" + col.ColumnName + "@", "……");
                    }
                }
                EmployeeBO employee = EmployeeService.GetById(employeeId);
                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\Content\\Avatar"))
                {
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\Content\\Avatar");
                }
                if (employee.Avatar != null && employee.Avatar.Length > 0)
                {
                    using (var fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "\\Content\\Avatar\\" + employeeId.ToString() + ".png", FileMode.Create, FileAccess.Write))
                    {
                        fs.Write(employee.Avatar, 0, employee.Avatar.Length);
                        ContentHTML = ContentHTML.Replace("@IMAGEAVATAR@", HttpContext.Request.UrlReferrer.OriginalString + "/Content/Avatar/" + employeeId.ToString() + ".png");
                    }
                }
                else
                {
                    ContentHTML = ContentHTML.Replace("@IMAGEAVATAR@", HttpContext.Request.UrlReferrer.OriginalString + "/Content/images/underfind.jpg");
                }
                string RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId));
                string Department = string.Join("<br>", DepartmentService.GetDepartmentsByEmployeeID(employeeId).Select(x => x.Name).ToList());
                ContentHTML = ContentHTML.Replace("@EmployeeTitle_Now@", !string.IsNullOrEmpty(RoleName) ? "<b>" + RoleName + "</b>" : "……");
                ContentHTML = ContentHTML.Replace("@DepartmentNameCBCC@", !string.IsNullOrEmpty(Department) ? "<b>" + Department + "</b>" : "……");
                ViewData["ContentHTML"] = ContentHTML;
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Không có thông tin hồ sơ nhân sự của nhân viên này, vui lòng thiết lập thông tin hồ sơ nhân sự !"
                });
                return this.Direct();
            }

        }

        public ActionResult Index(Guid Id)
        {
            var data = VPHumanProfileCuriculmViateService.GetByEmployeeId(Id);
            if (data != null)
                return View(data);
            else return View(new VPHumanProfileCuriculmViateBO { HumanEmployeeId = Id });
        }

        public ActionResult FormSummaryProfile(Guid Id)
        {
            var masterService = new ProfileMasterService();
            var data = VPHumanProfileCuriculmViateService.GetByEmployeeId(Id);

            if (data != null)
            {
                data.DepartmentId = DepartmentService.GetDepartmentsByEmployeeID(Id).FirstOrDefault().Id;
                var nationality = masterService.GetSingleMasterData("nation", data.Nationality).Name;
                var ethnic = masterService.GetSingleMasterData("ethnic", data.People).Name;
                var religion = masterService.GetSingleMasterData("religion", data.Religion).Name;
                var positionGroup = masterService.GetSingleMasterData("positionGroup", data.PositionGroup).Name;
                var positionParty = masterService.GetSingleMasterData("positionParty", data.PosititonParty).Name;
                var rankArmy = masterService.GetSingleMasterData("rankArmy", data.ArmyRank).Name;

                data.Id = data.Id;
                data.Nationality = nationality;
                data.People = ethnic;
                data.Religion = religion;
                data.PositionGroup = positionGroup;
                data.PosititonParty = positionParty;
                data.ArmyRank = rankArmy;
                data.PlaceOfBirthCity = ConvertAddress(data)[0];
                return View(data);
            }
            else return View(new VPHumanProfileCuriculmViateBO
            {
                HumanEmployeeId = Id,
                DepartmentId = DepartmentService.GetDepartmentsByEmployeeID(Id).FirstOrDefault().Id
            });
        }

        public ActionResult PrintCuriculmViate(Guid employeeId)
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }

        public ActionResult Update(VPHumanProfileCuriculmViateBO updateData)
        {
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            // Kiểm tra quyền edit
            if (updateData.HumanEmployeeId != null && s.CheckTitleRole_Edit((Guid)updateData.HumanEmployeeId) == false)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }

            try
            {
                VPHumanProfileCuriculmViateService.InsertHumanProfileCuriculmViate(updateData);

                // update employee
                if (updateData.HumanEmployeeId != null && updateData.HumanEmployeeId != Guid.Empty)
                {
                    var emp = EmployeeService.GetById((Guid)updateData.HumanEmployeeId);
                    if (emp != null)
                    {
                        emp.Sex = 2;//nữ
                        if (updateData.IsMale != null && updateData.IsMale.Value == true)
                            emp.Sex = 1;

                        emp.PhoneNumber = updateData.OfficePhone;
                        EmployeeService.Update(emp);
                    }
                }

                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }
        public ActionResult SetName(string id)
        {
            var name = "";
            if (Guid.TryParse(id, out Guid eid))
            {
                var service = new EmployeeService();
                name = service.GetById(eid).Name;
            }
            return this.Direct(name);
        }

        public List<string> ConvertAddress(VPHumanProfileCuriculmViateBO data)
        {
            var bornCityId = data.PlaceOfBirthCity;
            var bornDistrictId = data.PlaceOfBirthDistrict;
            var bornCommuneId = data.PlaceOfBirthCommune;
            var homeCityId = data.HomeTownCity;
            var homeDistrictId = data.HomeTownDistrict;
            var homeCommuneId = data.HomeTownCommune;
            var masterService = new ProfileMasterService();
            var bornCity = "";
            var bornDistrict = "";
            var bornCommune = "";
            var homeCity = "";
            var homeDistrict = "";
            var homeCommune = "";
            if (bornCityId != null && bornCityId != "" && Guid.TryParse(bornCityId, out Guid gid)) bornCity = masterService.GetSingleMasterData("city", bornCityId).Name;
            if (homeCityId != null && homeCityId != "" && Guid.TryParse(homeCityId, out Guid gid1)) homeCity = masterService.GetSingleMasterData("city", homeCityId).Name;
            if (bornDistrictId != null && bornDistrictId != "" && Guid.TryParse(bornDistrictId, out Guid gid2)) bornDistrict = masterService.GetSingleMasterData("district", bornDistrictId).Name;
            if (homeDistrictId != null && homeDistrictId != "" && Guid.TryParse(homeDistrictId, out Guid gid3)) homeDistrict = masterService.GetSingleMasterData("district", homeDistrictId).Name;
            if (bornCommuneId != null && bornCommuneId != "" && Guid.TryParse(bornCommuneId, out Guid gid4)) bornCommune = masterService.GetSingleMasterData("commune", bornCommuneId).Name;
            if (homeCommuneId != null && homeCommuneId != "" && Guid.TryParse(homeCommuneId, out Guid gid5)) homeCommune = masterService.GetSingleMasterData("commune", homeCommuneId).Name;

            var bornAddress = string.Join(" - ", new[] { bornCommune, bornDistrict, bornCity }.Where(s => !string.IsNullOrEmpty(s)));
            var homeAddress = string.Join(" - ", new[] { homeCommune, homeDistrict, homeCity }.Where(s => !string.IsNullOrEmpty(s)));
            return new List<string> { bornAddress, homeAddress };
        }

        #region Export Profile Employee
        public ActionResult InformationProfile(Guid employeeId)
        {
            var data = VPHumanProfileCuriculmViateService.GetByEmployeeId(employeeId);
            var experience = VPHumanProfileWorkExperienceService.GetByEmployeeIdNotPaging(employeeId).ToList();
            ViewBag.experience = experience;
            return PartialView(data);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Export(string data)
        {
            using (var stream = new io.MemoryStream())
            {
                var sr = new io.StringReader(Encoding.UTF8.GetBytes(data).ToString());
                //var htmlStream = new io.MemoryStream(Encoding.UTF8.GetBytes(data));
                var pdfDoc = new iTextSharp.text.Document();
                var writer = PdfWriter.GetInstance(pdfDoc, stream);
                var htmlWorker = new HTMLWorker(pdfDoc);
                pdfDoc.Open();

                BaseFont bf = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\ARIAL.TTF", BaseFont.IDENTITY_H, true);
                Font NormalFont = new Font(bf, 12, Font.NORMAL, BaseColor.BLACK);
                //XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);

                htmlWorker.StartDocument();
                htmlWorker.Parse(sr);
                htmlWorker.EndDocument();
                htmlWorker.Close();
                pdfDoc.Close();
                var fileName = AppDomain.CurrentDomain.BaseDirectory + "1.pdf";
                io.File.WriteAllBytes(fileName, stream.ToArray());
                //File(stream.ToArray(), "application/pdf", "1.pdf");
                ////stream.write

                //var file = new FileBO()
                //    {
                //        Name = DateTime.Now.ToString("ddMMyyyyhhmmss") + ".pdf",
                //        Stream = stream
                //    };
                //FileUploadService.Upload(file);
            }
            //var fileUpload = new FileUploadBO()
            //{
            //     FileAttachments = new List<HttpPostedFileBase>()
            //     {
            //         new HttpPostedFileBase()
            //     }
            //};
            //var file = new HttpPostedFile
            //FileService.Upload()

            return this.Direct();
        }
        #endregion
    }

}
