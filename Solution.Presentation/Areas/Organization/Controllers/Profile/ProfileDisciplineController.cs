﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;

namespace iDAS.Presentation.Areas.Organization.Controllers
{


    public class ProfileDisciplineController : FrontController
    {


        #region   Hồ sơ Kỉ luật


        public ActionResult Index(string id)
        {
            ViewData["EmployeeId"] = id;
            ViewData["EmployeeName"] = EmployeeService.GetById(Guid.Parse(id)).Name;
            return View();
        }
        public PartialViewResult IndexAll(string containerId)
        {

            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult StatisticIndex(string containerId)
        {
            return PartialView();
        }

        public ActionResult Form(string containerId)
        {
            try
            {
                return new Ext.Net.MVC.PartialViewResult();
            }
            catch
            {
                return this.Direct();
            }
        }

        public ActionResult DetailForm(string ID, Guid EmployeeID)
        {
            var data = VPHumanProfileDisciplineService.GetById(new Guid(ID));
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
        }

        public ActionResult LoadProfileDiscipline(StoreRequestParameters parameters, Guid EmployeeID)
        {
            int totalCount;
            var result = VPHumanProfileDisciplineService.GetAllByEmployeeId(parameters.Page, parameters.Limit, out totalCount, EmployeeID);
            foreach (var data in result)
            {
                var id = data.Form;
                if (id != null && id != "" && Guid.TryParse(id, out Guid gid))
                {
                    data.Form = new ProfileMasterService().GetSingleMasterData("discipline", id).Name;
                }
            }
            return this.Store(result, totalCount);
        }

        public ActionResult LoadProfileDisciplineAll(StoreRequestParameters param, string name = "", string form = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileDiscipline_LoadProfileDisciplineAll",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    form = !string.IsNullOrEmpty(form) ? form : "",
                    pageIndex = param.Page,
                    pageSize = param.Limit
                }
            );

            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                              Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            NumberOfDecision = "",
                                            DateOfDecision = "",
                                            Reason = "",
                                            FormId = "",
                                            Form = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            int count = 0;
            if (dataTable != null && dataTable.Tables[0] != null && dataTable.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(dataTable.Tables[1].Rows[0][0].ToString());
                foreach (DataRow data in dataTable.Tables[0].Rows)
                {
                    var employeeId = data["HumanEmployeeId"] != null && !string.IsNullOrEmpty(data["HumanEmployeeId"].ToString()) ? new Guid(data["HumanEmployeeId"].ToString()) : Guid.Empty;
                    var Id = data["Id"] != null && !string.IsNullOrEmpty(data["Id"].ToString()) ? new Guid(data["Id"].ToString()) : Guid.Empty;
                    var files = new VPHumanProfileAttachmentFileService().GetByFile(Id);
                    var FileAttachs = new FileUploadBO()
                    {
                        Files = files.ToList()
                    };
                    customResult.Add(new
                    {
                        Id = Id,
                        Code = (data["Code"] != null && !string.IsNullOrEmpty(data["Code"].ToString())) ? data["Code"].ToString() : "",
                        RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId)),
                        EmployeeId = employeeId,
                        EmployeeName = (data["employeeName"] != null && !string.IsNullOrEmpty(data["employeeName"].ToString())) ? data["employeeName"].ToString() : "",
                        NumberOfDecision = (data["NumberOfDecision"] != null && !string.IsNullOrEmpty(data["NumberOfDecision"].ToString())) ? data["NumberOfDecision"].ToString() : "",
                        DateOfDecision = (data["DateOfDecision"] != null && !string.IsNullOrEmpty(data["DateOfDecision"].ToString())) ? DateTime.Parse(data["DateOfDecision"].ToString()).ToString("dd/MM/yyyy") : "",
                        Reason = (data["Reason"] != null && !string.IsNullOrEmpty(data["Reason"].ToString())) ? data["Reason"].ToString() : "",
                        FormId = (data["Form"] != null && !string.IsNullOrEmpty(data["Form"].ToString())) ? data["Form"].ToString() : "",
                        Form = (data["formName"] != null && !string.IsNullOrEmpty(data["formName"].ToString())) ? data["formName"].ToString() : "",
                        FileAttachs = FileAttachs
                    });
                }
            }
            customResult.RemoveAt(0);
            //Sắp xếp theo tên nhân viên, nếu trùng thì xếp theo Id
            //customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult, count);
        }
        public void exportexcel(string name = "", string form = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            if (DeparmentId == "null")
                DeparmentId = "";
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileDiscipline_LoadProfileDisciplineAll",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    form = !string.IsNullOrEmpty(form) ? (form == "Tất cả" || form == "null" ? "" : form) : "",
                    pageIndex = -1,
                    pageSize = -1
                }
            );
            foreach (DataRow row in dataTable.Tables[0].Rows)
            {
                row["RoleName"] = string.Join(", ", EmployeeService.GetDeparmentRoleNames(new Guid(row["HumanEmployeeId"].ToString())));
            }
            DataTable tablecopy = dataTable.Tables[0].Copy();
            foreach (DataColumn col in tablecopy.Columns)
            {
                if (col.ColumnName != "employeeName"
                    && col.ColumnName != "NumberOfDecision"
                       && col.ColumnName != "Code"
                    && col.ColumnName != "RoleName"
                    && col.ColumnName != "DateOfDecision"
                    && col.ColumnName != "Reason"
                    && col.ColumnName != "formName")
                {
                    dataTable.Tables[0].Columns.Remove(col.ColumnName);
                }
                else
                {
                    if (col.DataType.Name == "DateTime")
                    {
                        dataTable.Tables[0].Columns.Add(col.ColumnName + "_Extra", typeof(String));
                        foreach (DataRow data in dataTable.Tables[0].Rows)
                        {
                            data[col.ColumnName + "_Extra"] = (data[col.ColumnName] != null && !string.IsNullOrEmpty(data[col.ColumnName].ToString())) ? DateTime.Parse(data[col.ColumnName].ToString()).ToString("dd/MM/yyyy") : "";
                        }
                        dataTable.Tables[0].Columns.Remove(col.ColumnName);
                        dataTable.Tables[0].Columns[col.ColumnName + "_Extra"].ColumnName = col.ColumnName;
                    }
                }
            }
            dataTable.Tables[0].Columns["Code"].SetOrdinal(0);
            dataTable.Tables[0].Columns["employeeName"].SetOrdinal(1);
            dataTable.Tables[0].Columns["RoleName"].SetOrdinal(2);
            dataTable.Tables[0].Columns["NumberOfDecision"].SetOrdinal(3);
            dataTable.Tables[0].Columns["DateOfDecision"].SetOrdinal(4);
            dataTable.Tables[0].Columns["Reason"].SetOrdinal(5);
            dataTable.Tables[0].Columns["formName"].SetOrdinal(6);

            dataTable.Tables[0].Columns["Code"].ColumnName = "Mã nhân viên";
            dataTable.Tables[0].Columns["employeeName"].ColumnName = "Tên nhân viên";
            dataTable.Tables[0].Columns["RoleName"].ColumnName = "Chức vụ";
            dataTable.Tables[0].Columns["NumberOfDecision"].ColumnName = "Số quyết định";
            dataTable.Tables[0].Columns["DateOfDecision"].ColumnName = "Ngày quyết định";
            dataTable.Tables[0].Columns["Reason"].ColumnName = "Lý do kỷ luật";
            dataTable.Tables[0].Columns["formName"].ColumnName = "Hình thức kỷ luật";
            Common.Utilities.ExportDataTableToExcel(dataTable.Tables[0], "Danh sách hồ kỷ luật");
        }

        public ActionResult LoadProfileDisciplineNotPaging(Guid EmployeeID)
        {
            var result = VPHumanProfileDisciplineService.GetAllByEmployeeIdNotPaging(EmployeeID);
            var profileMasterService = new ProfileMasterService();

            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            EmployeeId = Guid.Empty,
                                            NumberOfDecision = "",
                                            DateOfDecision = (DateTime?)DateTime.Now,
                                            Reason = "",
                                            Form = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            foreach (var data in result)
            {
                var formId = data.Form;
                var formName = "";
                if (formId != null && formId != "" && Guid.TryParse(formId, out Guid id))
                {
                    formName = profileMasterService.GetSingleMasterData("discipline", formId).Name;
                }

                customResult.Add(new { Id = data.Id, EmployeeId = EmployeeID, NumberOfDecision = data.NumberOfDecision, DateOfDecision = data.DateOfDecision, Reason = data.Reason, Form = formName, FileAttachs = data.FileAttachs });
            }
            customResult.RemoveAt(0);
            return this.Store(customResult);
        }

        public ActionResult DeleteProfileDiscipline(Guid id)
        {
            try
            {
                VPHumanProfileDisciplineService.Delete(id);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileDiscipline").Reload();
            }
            return this.Direct();
        }
        #endregion


        public ActionResult UpdateForm(string ID, Guid EmployeeID)
        {
            var data = new VPHumanProfileDisciplineBO();
            if (!string.IsNullOrEmpty(ID))
            {
                data = VPHumanProfileDisciplineService.GetdataById(new Guid(ID));
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
            else
            {
                data.HumanEmployeeId = EmployeeID;
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="exit"></param>
        /// <returns></returns>
        public ActionResult Update(VPHumanProfileDisciplineBO updateData)
        {
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            // Kiểm tra quyền edit
            if (updateData.HumanEmployeeId != null && s.CheckTitleRole_Edit((Guid)updateData.HumanEmployeeId) == false)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }

            try
            {
                if (updateData.Id == Guid.Empty)
                {
                    VPHumanProfileDisciplineService.HumanProfileDiscipline(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    VPHumanProfileDisciplineService.HumanProfileDiscipline(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                X.GetCmp<Window>("winDiscipline").Close();
                X.GetCmp<Store>("StoreProfileDiscipline").Reload();
                return this.Direct();

            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return RedirectToAction("Index");
            }
        }

        public ActionResult InsertToAllEmployee(VPHumanProfileDisciplineBO profile = null, string dataEmpChanged = null)
        {
            var result = false;
            if (string.IsNullOrWhiteSpace(dataEmpChanged) || dataEmpChanged.Length < 6)
            {
                this.ShowNotify("Chưa chọn nhân sự");
                return this.Direct(result: result);
            }

            var dataUpdated = JSON.Deserialize<Dictionary<string, object>>(dataEmpChanged ?? string.Empty)["Updated"];
            List<EmployeeBO> lstEmp = JSON.Deserialize<List<EmployeeBO>>(dataUpdated.ToString().Replace("id", "ABC"));

            try
            {
                for (int i = 0; i < lstEmp.Count; i++)
                {
                    profile.HumanEmployeeId = lstEmp[i].Id;
                    profile.Id = Guid.Empty;
                    VPHumanProfileDisciplineService.HumanProfileDiscipline(profile);
                }
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
    }
}
