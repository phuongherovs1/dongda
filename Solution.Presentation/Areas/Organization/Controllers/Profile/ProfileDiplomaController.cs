﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class ProfileDiplomaController : FrontController
    {

        #region   Hồ sơ Văn bằng

        public ActionResult Index(string id)
        {
            ViewData["EmployeeId"] = id;
            ViewData["EmployeeName"] = EmployeeService.GetById(Guid.Parse(id)).Name;
            return View();
        }

        public PartialViewResult IndexAll(string containerId)
        {
            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult StatisticIndex(string containerId)
        {
            return PartialView();
        }

        public ActionResult LoadProfileDiplomaAll(StoreRequestParameters param, string name, string major = "", string level = "", string form = "", string rank = "", string place = "", string DeparmentId = "")
        {
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileDiploma_LoadProfileDiplomaAll",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    major = !string.IsNullOrEmpty(major) ? (major == "Tất cả" ? "" : major) : "",
                    level = !string.IsNullOrEmpty(level) ? (level == "Tất cả" ? "" : level) : "",
                    form = !string.IsNullOrEmpty(form) ? (form == "Tất cả" ? "" : form) : "",
                    rank = !string.IsNullOrEmpty(rank) ? (rank == "Tất cả" ? "" : rank) : "",
                    place = !string.IsNullOrEmpty(place) ? (place == "Tất cả" ? "" : place) : "",
                    pageIndex = param.Page,
                    pageSize = param.Limit
                }
             );
            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            Name = "",
                                            MajorId = "",
                                            Major = "",
                                            LevelId = "",
                                            Level = "",
                                            FormId = "",
                                            FormOfTraining = "",
                                            RankId = "",
                                            Rank = "",
                                            PlaceId = "",
                                            Place = "",
                                            DateOfGraduation = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            int count = 0;
            if (dataTable != null && dataTable.Tables[0] != null && dataTable.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(dataTable.Tables[1].Rows[0][0].ToString());
                foreach (DataRow data in dataTable.Tables[0].Rows)
                {
                    var employeeId = data["HumanEmployeeId"] != null && !string.IsNullOrEmpty(data["HumanEmployeeId"].ToString()) ? new Guid(data["HumanEmployeeId"].ToString()) : Guid.Empty;
                    var Id = data["Id"] != null && !string.IsNullOrEmpty(data["Id"].ToString()) ? new Guid(data["Id"].ToString()) : Guid.Empty;
                    var files = new VPHumanProfileAttachmentFileService().GetByFile(Id);
                    var FileAttachs = new FileUploadBO()
                    {
                        Files = files.ToList()
                    };
                    customResult.Add(new { 
                        Id = Id,
                        Code = (data["Code"] != null && !string.IsNullOrEmpty(data["Code"].ToString())) ? data["Code"].ToString() : "",
                        RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId)),
                        EmployeeId = employeeId, 
                        EmployeeName = (data["employeeName"] != null && !string.IsNullOrEmpty(data["employeeName"].ToString())) ? data["employeeName"].ToString() : "",
                        Name = (data["Name"] != null && !string.IsNullOrEmpty(data["Name"].ToString())) ? data["Name"].ToString() : "", 
                        MajorId = (data["Major"] != null && !string.IsNullOrEmpty(data["Major"].ToString())) ? data["Major"].ToString() : "",
                        Major = (data["majorName"] != null && !string.IsNullOrEmpty(data["majorName"].ToString())) ? data["majorName"].ToString() : "",
                        LevelId = (data["Level"] != null && !string.IsNullOrEmpty(data["Level"].ToString())) ? data["Level"].ToString() : "",
                        Level = (data["levelName"] != null && !string.IsNullOrEmpty(data["levelName"].ToString())) ? data["levelName"].ToString() : "",
                        FormId = (data["FormOfTrainning"] != null && !string.IsNullOrEmpty(data["FormOfTrainning"].ToString())) ? data["FormOfTrainning"].ToString() : "",
                        FormOfTraining = (data["formName"] != null && !string.IsNullOrEmpty(data["formName"].ToString())) ? data["formName"].ToString() : "",
                        RankId = (data["Rank"] != null && !string.IsNullOrEmpty(data["Rank"].ToString())) ? data["Rank"].ToString() : "",
                        Rank = (data["rankName"] != null && !string.IsNullOrEmpty(data["rankName"].ToString())) ? data["rankName"].ToString() : "",
                        PlaceId = (data["Place"] != null && !string.IsNullOrEmpty(data["Place"].ToString())) ? data["Place"].ToString() : "",
                        Place = (data["placeName"] != null && !string.IsNullOrEmpty(data["placeName"].ToString())) ? data["placeName"].ToString() : "",
                        DateOfGraduation = (data["DateOfGraduation"] != null && !string.IsNullOrEmpty(data["DateOfGraduation"].ToString())) ? DateTime.Parse(data["DateOfGraduation"].ToString()).ToString("dd/MM/yyyy") : "",
                        FileAttachs = FileAttachs
                    });
                }
            }
            customResult.RemoveAt(0);
            //Sắp xếp theo tên nhân viên, nếu trùng thì xếp theo Id
            //customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult, count);
        }
        public void exportexcel(string name, string major = "", string level = "", string form = "", string rank = "", string place = "", string DeparmentId = "")
        {
            if (DeparmentId == "null")
                DeparmentId = "";
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileDiploma_LoadProfileDiplomaAll",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    major = !string.IsNullOrEmpty(major) ? (major == "Tất cả" || major == "null" ? "" : major) : "",
                    level = !string.IsNullOrEmpty(level) ? (level == "Tất cả" || level == "null" ? "" : level) : "",
                    form = !string.IsNullOrEmpty(form) ? (form == "Tất cả" || form == "null" ? "" : form) : "",
                    rank = !string.IsNullOrEmpty(rank) ? (rank == "Tất cả" || rank == "null" ? "" : rank) : "",
                    place = !string.IsNullOrEmpty(place) ? (place == "Tất cả" || place == "null" ? "" : place) : "",
                    pageIndex = -1,
                    pageSize = -1
                }
             );
            foreach (DataRow row in dataTable.Tables[0].Rows)
            {
                row["RoleName"] = string.Join(", ", EmployeeService.GetDeparmentRoleNames(new Guid(row["HumanEmployeeId"].ToString())));
            }
            DataTable tablecopy = dataTable.Tables[0].Copy();
            foreach (DataColumn col in tablecopy.Columns)
            {
                if (col.ColumnName != "employeeName"
                    && col.ColumnName != "Name"
                     && col.ColumnName != "Code"
                    && col.ColumnName != "RoleName"
                    && col.ColumnName != "majorName"
                    && col.ColumnName != "formName"
                    && col.ColumnName != "placeName"
                    && col.ColumnName != "levelName"
                    && col.ColumnName != "rankName"
                    && col.ColumnName != "DateOfGraduation")
                {
                    dataTable.Tables[0].Columns.Remove(col.ColumnName);
                }
                else
                {
                    if (col.DataType.Name == "DateTime")
                    {
                        dataTable.Tables[0].Columns.Add(col.ColumnName + "_Extra", typeof(String));
                        foreach (DataRow data in dataTable.Tables[0].Rows)
                        {
                            data[col.ColumnName + "_Extra"] = (data[col.ColumnName] != null && !string.IsNullOrEmpty(data[col.ColumnName].ToString())) ? DateTime.Parse(data[col.ColumnName].ToString()).ToString("dd/MM/yyyy") : "";
                        }
                        dataTable.Tables[0].Columns.Remove(col.ColumnName);
                        dataTable.Tables[0].Columns[col.ColumnName + "_Extra"].ColumnName = col.ColumnName;
                    }
                }
            }
            dataTable.Tables[0].Columns["Code"].SetOrdinal(0);
            dataTable.Tables[0].Columns["employeeName"].SetOrdinal(1);
            dataTable.Tables[0].Columns["RoleName"].SetOrdinal(2);
            dataTable.Tables[0].Columns["Name"].SetOrdinal(3);
            dataTable.Tables[0].Columns["majorName"].SetOrdinal(4);
            dataTable.Tables[0].Columns["formName"].SetOrdinal(5);
            dataTable.Tables[0].Columns["placeName"].SetOrdinal(6);
            dataTable.Tables[0].Columns["levelName"].SetOrdinal(7);
            dataTable.Tables[0].Columns["rankName"].SetOrdinal(8);
            dataTable.Tables[0].Columns["DateOfGraduation"].SetOrdinal(9);

            dataTable.Tables[0].Columns["Code"].ColumnName = "Mã nhân viên";
            dataTable.Tables[0].Columns["employeeName"].ColumnName = "Tên nhân viên";
            dataTable.Tables[0].Columns["RoleName"].ColumnName = "Chức vụ";
            dataTable.Tables[0].Columns["Name"].ColumnName = "Tên văn bằng";
            dataTable.Tables[0].Columns["majorName"].ColumnName = "Chuyên ngành";
            dataTable.Tables[0].Columns["formName"].ColumnName = "Hình thức đào tạo";
            dataTable.Tables[0].Columns["placeName"].ColumnName = "Nơi đào tạo";
            dataTable.Tables[0].Columns["levelName"].ColumnName = "Trình độ";
            dataTable.Tables[0].Columns["rankName"].ColumnName = "Xếp loại văn bằng";
            dataTable.Tables[0].Columns["DateOfGraduation"].ColumnName = "Ngày nhận bằng";
            Common.Utilities.ExportDataTableToExcel(dataTable.Tables[0], "Danh sách hồ sơ văn bằng");
        }
        public ActionResult LoadProfileDiploma(StoreRequestParameters parameters, Guid EmployeeID)
        {
            int totalCount;

            var result = VPHumanProfileDiplomaService.GetAllByEmployeeId(EmployeeID, parameters.Page, parameters.Limit, out totalCount);
            foreach (var data in result)
            {
                var majorId = data.Major;
                if (majorId != null && majorId != "" && Guid.TryParse(majorId, out Guid id1))
                {
                    data.Major = new ProfileMasterService().GetSingleMasterData("educationField", majorId).Name;
                }
                var formId = data.FormOfTrainning;
                if (formId != null && formId != "" && Guid.TryParse(formId, out Guid id2))
                {
                    data.FormOfTrainning = new ProfileMasterService().GetSingleMasterData("educationType", formId).Name;
                }
                var placeId = data.Place;
                if (placeId != null && placeId != "" && Guid.TryParse(placeId, out Guid id3))
                {
                    data.Place = new ProfileMasterService().GetSingleMasterData("educationOrg", placeId).Name;
                }
                var levelId = data.Level;
                if (levelId != null && levelId != "" && Guid.TryParse(levelId, out Guid id4))
                {
                    data.Level = new ProfileMasterService().GetSingleMasterData("educationLevel", levelId).Name;
                }
                var rankId = data.Rank;
                if (rankId != null && rankId != "" && Guid.TryParse(rankId, out Guid id5))
                {
                    data.Rank = new ProfileMasterService().GetSingleMasterData("certificateLevel", rankId).Name;
                }
            }
            return this.Store(result, totalCount);
        }

        public ActionResult LoadProfileDiplomaNotPaging(Guid EmployeeID)
        {
            var result = VPHumanProfileDiplomaService.GetAllByEmployeeIdNotPaging(EmployeeID);
            var profileMasterService = new ProfileMasterService();
            var employeeService = new EmployeeService();

            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            Name = "",
                                            MajorId = "",
                                            Major = "",
                                            LevelId = "",
                                            Level = "",
                                            FormId = "",
                                            FormOfTraining = "",
                                            RankId = "",
                                            Rank = "",
                                            PlaceId = "",
                                            Place = "",
                                            DateOfGraduation = (DateTime?)DateTime.Now,
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            foreach (var data in result)
            {
                var majorId = data.Major;
                var majorName = "";
                if (majorId != null && majorId != "" && Guid.TryParse(majorId, out Guid id1))
                {
                    majorName = profileMasterService.GetSingleMasterData("educationField", majorId).Name;
                }

                var formId = data.FormOfTrainning;
                var formName = "";
                if (formId != null && formId != "" && Guid.TryParse(formId, out Guid id2))
                {
                    formName = profileMasterService.GetSingleMasterData("educationType", formId).Name;
                }

                var placeId = data.Place;
                var placeName = "";
                if (placeId != null && placeId != "" && Guid.TryParse(placeId, out Guid id3))
                {
                    placeName = profileMasterService.GetSingleMasterData("educationOrg", placeId).Name;
                }
                var levelId = data.Level;
                var levelName = "";
                if (levelId != null && levelId != "" && Guid.TryParse(levelId, out Guid id4))
                {
                    levelName = profileMasterService.GetSingleMasterData("educationLevel", levelId).Name;
                }
                var rankId = data.Rank;
                var rankName = "";
                if (rankId != null && rankId != "" && Guid.TryParse(rankId, out Guid id5))
                {
                    rankName = profileMasterService.GetSingleMasterData("certificateLevel", rankId).Name;
                }

                var employeeId = data.HumanEmployeeId ?? Guid.Empty;
                var employeeName = "";
                if (employeeId != Guid.Empty)
                {
                    employeeName = employeeService.GetById(employeeId).Name;
                }

                customResult.Add(new { Id = data.Id, EmployeeId = employeeId, EmployeeName = employeeName, Name = data.Name, MajorId = data.Major, Major = majorName, LevelId = data.Level, Level = levelName, FormId = data.FormOfTrainning, FormOfTraining = formName, RankId = data.Rank, Rank = rankName, PlaceId = data.Place, Place = placeName, DateOfGraduation = data.DateOfGraduation, FileAttachs = data.FileAttachs });
            }
            customResult.RemoveAt(0);
            return this.Store(customResult);
        }

        public ActionResult DeleteProfileDiploma(Guid id)
        {
            try
            {
                VPHumanProfileDiplomaService.Delete(id);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileDiploma").Reload();
            }
            return this.Direct();
        }
        #endregion

        public ActionResult UpdateForm(string ID, Guid EmployeeID)
        {
            var data = new VPHumanProfileDiplomaBO();
            if (!string.IsNullOrEmpty(ID))
            {
                data = VPHumanProfileDiplomaService.GetdataById(new Guid(ID));
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
            else
            {
                data.HumanEmployeeId = EmployeeID;
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="exit"></param>
        /// <returns></returns>
        public ActionResult Update(VPHumanProfileDiplomaBO updateData)
        {
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            // Kiểm tra quyền edit
            if (updateData.HumanEmployeeId != null && s.CheckTitleRole_Edit((Guid)updateData.HumanEmployeeId) == false)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }

            try
            {
                if (updateData.Id == Guid.Empty)
                {
                    VPHumanProfileDiplomaService.InsertHumanProfileDiploma(updateData);

                }
                else
                {
                    VPHumanProfileDiplomaService.InsertHumanProfileDiploma(updateData);

                }
                this.ShowNotify(Common.Resource.SystemSuccess);
                X.GetCmp<Window>("winDiploma").Close();
                X.GetCmp<Store>("StoreProfileDiploma").Reload();
                return this.Direct();

            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return RedirectToAction("Index");
            }
        }

    }
}
