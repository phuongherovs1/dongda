﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class ProfileInsuranceController : FrontController
    {
        #region   Hồ sơ bảo hiểm y tế

        public ActionResult Index(string id)
        {
            ViewData["EmployeeId"] = id;
            ViewData["EmployeeName"] = EmployeeService.GetById(Guid.Parse(id)).Name;
            return View();
        }
        public PartialViewResult IndexAll(string containerId)
        {
            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                WrapByScriptTag = false
                //check view
            };
        }
        public ActionResult LoadProfileInsuranceAll(StoreRequestParameters param, string name = "", string DeparmentId = "")
        {
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileInsurance_LoadProfileInsurance",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    pageIndex = param.Page,
                    pageSize = param.Limit
                }
            );
            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            Number = "",
                                            Type = "",
                                            StartDate = "",
                                            EndDate = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            int count = 0;
            if (dataTable != null && dataTable.Tables[0] != null && dataTable.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(dataTable.Tables[1].Rows[0][0].ToString());
                foreach (DataRow data in dataTable.Tables[0].Rows)
                {
                    var employeeId = data["HumanEmployeeId"] != null && !string.IsNullOrEmpty(data["HumanEmployeeId"].ToString()) ? new Guid(data["HumanEmployeeId"].ToString()) : Guid.Empty;
                    var Id = data["Id"] != null && !string.IsNullOrEmpty(data["Id"].ToString()) ? new Guid(data["Id"].ToString()) : Guid.Empty;
                    var files = new VPHumanProfileAttachmentFileService().GetByFile(Id);
                    var FileAttachs = new FileUploadBO()
                    {
                        Files = files.ToList()
                    };
                    customResult.Add(new
                    {
                        Id = Id,
                        Code = (data["Code"] != null && !string.IsNullOrEmpty(data["Code"].ToString())) ? data["Code"].ToString() : "",
                        RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId)),
                        EmployeeId = employeeId,
                        EmployeeName = (data["EmployeeName"] != null && !string.IsNullOrEmpty(data["EmployeeName"].ToString())) ? data["EmployeeName"].ToString() : "",
                        Number = (data["Number"] != null && !string.IsNullOrEmpty(data["Number"].ToString())) ? data["Number"].ToString() : "",
                        Type = (data["Type"] != null && !string.IsNullOrEmpty(data["Type"].ToString())) ? data["Type"].ToString() : "",
                        StartDate = (data["StartDate"] != null && !string.IsNullOrEmpty(data["StartDate"].ToString())) ? DateTime.Parse(data["StartDate"].ToString()).ToString("dd/MM/yyyy") : "",
                        EndDate = (data["EndDate"] != null && !string.IsNullOrEmpty(data["EndDate"].ToString())) ? DateTime.Parse(data["EndDate"].ToString()).ToString("dd/MM/yyyy") : "",
                        FileAttachs = FileAttachs
                    });
                }
            }
            customResult.RemoveAt(0);
            //Sắp xếp theo tên nhân viên, nếu trùng thì xếp theo Id
            //customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult,count);
        }
        public void exportexcel(string name = "", string DeparmentId = "")
        {
            if (DeparmentId == "null")
                DeparmentId = "";
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileInsurance_LoadProfileInsurance",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    pageIndex = -1,
                    pageSize = -1
                }
            );
            foreach (DataRow row in dataTable.Tables[0].Rows)
            {
                row["RoleName"] = string.Join(", ", EmployeeService.GetDeparmentRoleNames(new Guid(row["HumanEmployeeId"].ToString())));
            }
            DataTable tablecopy = dataTable.Tables[0].Copy();
            foreach (DataColumn col in tablecopy.Columns)
            {
                if (col.ColumnName != "EmployeeName"
                    && col.ColumnName != "Number"
                     && col.ColumnName != "Code"
                    && col.ColumnName != "RoleName"
                    && col.ColumnName != "Type"
                    && col.ColumnName != "StartDate"
                    && col.ColumnName != "EndDate")
                {
                    dataTable.Tables[0].Columns.Remove(col.ColumnName);
                }
                else
                {
                    if (col.DataType.Name == "DateTime")
                    {
                        dataTable.Tables[0].Columns.Add(col.ColumnName + "_Extra", typeof(String));
                        foreach (DataRow data in dataTable.Tables[0].Rows)
                        {
                            data[col.ColumnName + "_Extra"] = (data[col.ColumnName] != null && !string.IsNullOrEmpty(data[col.ColumnName].ToString())) ? DateTime.Parse(data[col.ColumnName].ToString()).ToString("dd/MM/yyyy") : "";
                        }
                        dataTable.Tables[0].Columns.Remove(col.ColumnName);
                        dataTable.Tables[0].Columns[col.ColumnName + "_Extra"].ColumnName = col.ColumnName;
                    }
                }
            }
            dataTable.Tables[0].Columns["Code"].SetOrdinal(0);
            dataTable.Tables[0].Columns["EmployeeName"].SetOrdinal(1);
            dataTable.Tables[0].Columns["RoleName"].SetOrdinal(2);
            dataTable.Tables[0].Columns["Number"].SetOrdinal(3);
            dataTable.Tables[0].Columns["Type"].SetOrdinal(4);
            dataTable.Tables[0].Columns["StartDate"].SetOrdinal(5);
            dataTable.Tables[0].Columns["EndDate"].SetOrdinal(6);

            dataTable.Tables[0].Columns["Code"].ColumnName = "Mã nhân viên";
            dataTable.Tables[0].Columns["EmployeeName"].ColumnName = "Tên nhân viên";
            dataTable.Tables[0].Columns["RoleName"].ColumnName = "Chức vụ";
            dataTable.Tables[0].Columns["Number"].ColumnName = "Số bảo hiểm";
            dataTable.Tables[0].Columns["Type"].ColumnName = "Loại bảo hiểm";
            dataTable.Tables[0].Columns["StartDate"].ColumnName = "Ngày bắt đầu";
            dataTable.Tables[0].Columns["EndDate"].ColumnName = "Ngày kết thúc";
            Common.Utilities.ExportDataTableToExcel(dataTable.Tables[0], "Danh sách hồ sơ bảo hiểm");
        }
        public ActionResult LoadProfileInsurance(StoreRequestParameters parameters, Guid EmployeeID)
        {
            int totalCount;
            var result = VPHumanProfileInsuranceService.GetAllByEmployeeId(parameters.Page, parameters.Limit, out totalCount, EmployeeID);
            return this.Store(result, totalCount);
        }

        public ActionResult LoadProfileInsuranceNotPaging(Guid EmployeeID)
        {
            var result = VPHumanProfileInsuranceService.GetAllByEmployeeIdNotPaging(EmployeeID);
            return this.Store(result);
        }

        public ActionResult DeleteProfileInsurance(Guid id)
        {
            try
            {
                VPHumanProfileInsuranceService.Delete(id);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileInsurance").Reload();
            }
            return this.Direct();
        }
        #endregion


        public ActionResult UpdateForm(string ID, Guid EmployeeID)
        {
            var data = new VPHumanProfileInsuranceBO();
            if (!string.IsNullOrEmpty(ID))
            {
                data = VPHumanProfileInsuranceService.GetdataById(new Guid(ID));
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
            else
            {
                data.HumanEmployeeId = EmployeeID;
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="exit"></param>
        /// <returns></returns>
        public ActionResult Update(VPHumanProfileInsuranceBO updateData)
        {
            try
            {
                if (updateData.Id == Guid.Empty)
                {
                    VPHumanProfileInsuranceService.HumanProfileInsuranceBO(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    VPHumanProfileInsuranceService.HumanProfileInsuranceBO(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                X.GetCmp<Window>("winInsurance").Close();
                X.GetCmp<Store>("StoreProfileInsurance").Reload();
                return this.Direct();

            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return RedirectToAction("Index");
            }
        }


        public ActionResult DetailForm(string ID, Guid EmployeeID)
        {
            var data = VPHumanProfileInsuranceService.GetById(new Guid(ID));
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
        }

    }
}
