﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using io = System.IO;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;
using webcontrol = System.Web.UI.WebControls;

namespace iDAS.Presentation.Areas.Organization.Controllers
{

    public class ProfileWorkExperienceController : FrontController
    {
        #region   Hồ sơ kinh nghiệm làm việc

        public ActionResult Index(string id)
        {
            ViewData["EmployeeId"] = id;
            ViewData["EmployeeName"] = EmployeeService.GetById(Guid.Parse(id)).Name;
            return View();
        }
        public PartialViewResult IndexAll(string containerId)
        {
            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                WrapByScriptTag = false
                //check view
            };
        }
        public ActionResult LoadProfileWorkExperienceAll(StoreRequestParameters param, string name = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileWorkExperience_LoadProfileWorkExperienceAll",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    pageIndex = param.Page,
                    pageSize = param.Limit
                }
            );
            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                             Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            StartDate = "",
                                            EndDate = "",
                                            PlaceOfWork="",
                                            Position="",
                                            Department="",
                                            JobDescription="",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            int count = 0;
            if (dataTable != null && dataTable.Tables[0] != null && dataTable.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(dataTable.Tables[1].Rows[0][0].ToString());
                foreach (DataRow data in dataTable.Tables[0].Rows)
                {
                    var employeeId = data["HumanEmployeeId"] != null && !string.IsNullOrEmpty(data["HumanEmployeeId"].ToString()) ? new Guid(data["HumanEmployeeId"].ToString()) : Guid.Empty;
                    var Id = data["Id"] != null && !string.IsNullOrEmpty(data["Id"].ToString()) ? new Guid(data["Id"].ToString()) : Guid.Empty;
                    var files = new VPHumanProfileAttachmentFileService().GetByFile(Id);
                    var FileAttachs = new FileUploadBO()
                    {
                        Files = files.ToList()
                    };
                    customResult.Add(new
                    {
                        Id = Id,
                        Code = (data["Code"] != null && !string.IsNullOrEmpty(data["Code"].ToString())) ? data["Code"].ToString() : "",
                        RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId)),
                        EmployeeId = employeeId,
                        EmployeeName = (data["Name"] != null && !string.IsNullOrEmpty(data["Name"].ToString())) ? data["Name"].ToString() : "",
                        StartDate = (data["StartDate"] != null && !string.IsNullOrEmpty(data["StartDate"].ToString())) ? DateTime.Parse(data["StartDate"].ToString()).ToString("dd/MM/yyyy") : "",
                        EndDate = (data["EndDate"] != null && !string.IsNullOrEmpty(data["EndDate"].ToString())) ? DateTime.Parse(data["EndDate"].ToString()).ToString("dd/MM/yyyy") : "",
                        PlaceOfWork = (data["PlaceOfWork"] != null && !string.IsNullOrEmpty(data["PlaceOfWork"].ToString())) ? data["PlaceOfWork"].ToString() : "",
                        Position = (data["Position"] != null && !string.IsNullOrEmpty(data["Position"].ToString())) ? data["Position"].ToString() : "",
                        Department = (data["Department"] != null && !string.IsNullOrEmpty(data["Department"].ToString())) ? data["Department"].ToString() : "",
                        JobDescription = (data["JobDescription"] != null && !string.IsNullOrEmpty(data["JobDescription"].ToString())) ? data["JobDescription"].ToString() : "",
                        FileAttachs = FileAttachs
                    });
                }
            }
            customResult.RemoveAt(0);

            //Sắp xếp theo tên nhân viên, nếu trùng thì xếp theo Id
            //customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult, count);
        }
        public void exportexcel(string name = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            if (DeparmentId == "null")
                DeparmentId = "";
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileWorkExperience_LoadProfileWorkExperienceAll",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    pageIndex = -1,
                    pageSize = -1
                }
            );
            foreach (DataRow row in dataTable.Tables[0].Rows)
            {
                row["RoleName"] = string.Join(", ", EmployeeService.GetDeparmentRoleNames(new Guid(row["HumanEmployeeId"].ToString())));
            }
            DataTable tablecopy = dataTable.Tables[0].Copy();
            foreach (DataColumn col in tablecopy.Columns)
            {
                if (col.ColumnName != "StartDate"
                    && col.ColumnName != "EndDate"
                     && col.ColumnName != "Code"
                    && col.ColumnName != "RoleName"
                    && col.ColumnName != "Name"
                    && col.ColumnName != "PlaceOfWork"
                    && col.ColumnName != "Position"
                    && col.ColumnName != "Department"
                    && col.ColumnName != "JobDescription")
                {
                    dataTable.Tables[0].Columns.Remove(col.ColumnName);
                }
                else
                {
                    if (col.DataType.Name == "DateTime")
                    {
                        dataTable.Tables[0].Columns.Add(col.ColumnName + "_Extra", typeof(String));
                        foreach (DataRow data in dataTable.Tables[0].Rows)
                        {
                            data[col.ColumnName + "_Extra"] = (data[col.ColumnName] != null && !string.IsNullOrEmpty(data[col.ColumnName].ToString())) ? DateTime.Parse(data[col.ColumnName].ToString()).ToString("dd/MM/yyyy") : "";
                        }
                        dataTable.Tables[0].Columns.Remove(col.ColumnName);
                        dataTable.Tables[0].Columns[col.ColumnName + "_Extra"].ColumnName = col.ColumnName;
                    }
                }
            }
            dataTable.Tables[0].Columns["StartDate"].SetOrdinal(0);
            dataTable.Tables[0].Columns["StartDate"].ColumnName = "Thời gian bắt đầu công tác";
            dataTable.Tables[0].Columns["EndDate"].SetOrdinal(1);
            dataTable.Tables[0].Columns["EndDate"].ColumnName = "Thời gian kết thúc công tác";
            dataTable.Tables[0].Columns["Code"].SetOrdinal(2);
            dataTable.Tables[0].Columns["Code"].ColumnName = "Mã nhân sự";
            dataTable.Tables[0].Columns["Name"].SetOrdinal(3);
            dataTable.Tables[0].Columns["Name"].ColumnName = "Tên nhân sự";
            dataTable.Tables[0].Columns["RoleName"].SetOrdinal(4);
            dataTable.Tables[0].Columns["RoleName"].ColumnName = "Chức vụ";
            dataTable.Tables[0].Columns["PlaceOfWork"].SetOrdinal(5);
            dataTable.Tables[0].Columns["PlaceOfWork"].ColumnName = "Nơi công tác";
            dataTable.Tables[0].Columns["Position"].SetOrdinal(6);
            dataTable.Tables[0].Columns["Position"].ColumnName = "Chức vụ đảm nhiệm";
            dataTable.Tables[0].Columns["Department"].SetOrdinal(7);
            dataTable.Tables[0].Columns["Department"].ColumnName = "Phòng ban";
            dataTable.Tables[0].Columns["JobDescription"].SetOrdinal(8);
            dataTable.Tables[0].Columns["JobDescription"].ColumnName = "Công việc";
            Common.Utilities.ExportDataTableToExcel(dataTable.Tables[0], "Danh sách hồ sơ quá trình công tác");
        }
        public ActionResult LoadProfileWorkExperience(StoreRequestParameters parameters, Guid employeeId)
        {
            int totalCount;
            var result = VPHumanProfileWorkExperienceService.GetByEmployeeId(employeeId, parameters.Page, parameters.Limit, out totalCount).ToList();
            return this.Store(result, totalCount);
        }

        public ActionResult LoadProfileWorkExperienceNotPaging(Guid employeeId)
        {
            var result = VPHumanProfileWorkExperienceService.GetByEmployeeIdNotPaging(employeeId).ToList();
            return this.Store(result);
        }

        public ActionResult DeleteProfileWorkExperience(Guid id)
        {
            try
            {
                VPHumanProfileWorkExperienceService.Delete(id);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileWorkExperience").Reload();
            }
            return this.Direct();
        }
        #endregion

        public ActionResult UpdateForm(string id, Guid employeeId)
        {
            var data = VPHumanProfileWorkExperienceService.GetFormItem(id, employeeId);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="exit"></param>
        /// <returns></returns>
        public ActionResult Update(VPHumanProfileWorkExperienceBO updateData)
        {
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            // Kiểm tra quyền edit
            if (updateData.HumanEmployeeId != null && s.CheckTitleRole_Edit((Guid)updateData.HumanEmployeeId) == false)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }

            try
            {
                if (updateData.Id == Guid.Empty)
                {
                    VPHumanProfileWorkExperienceService.HumanProfileExperience(updateData);
                }
                else
                {
                    VPHumanProfileWorkExperienceService.HumanProfileExperience(updateData); ;
                }
                X.GetCmp<Window>("winWorkExperience").Close();
                X.GetCmp<Store>("StoreProfileWorkExperience").Reload();
                return this.Direct();
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        [ValidateInput(false)]
        public ActionResult DownloadPDF(string content)
        {
            if (!string.IsNullOrEmpty(content))
            {
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=1.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(ExportToPDF(content));
                Response.End();
                //io.File.WriteAllBytes(AppDomain.CurrentDomain.BaseDirectory + "\\Temp\\temp.pdf", ExportToPDF(content));
            }

            return this.Direct();
        }

        public byte[] ExportToPDF(string pHTML)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                byte[] bPDF = null;
                TextReader reader = new StringReader(HttpUtility.HtmlDecode(pHTML));
                iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                HTMLWorker htmlWorker = new HTMLWorker(pdfDoc);
                pdfDoc.Open();
                htmlWorker.StartDocument();
                htmlWorker.Parse(reader);
                htmlWorker.EndDocument();
                htmlWorker.Close();
                pdfDoc.Close();
                bPDF = stream.ToArray();
                return bPDF;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ExportToWord(string pHtml, Guid employeeId)
        {
            var employee = EmployeeService.GetById(employeeId);
            var employeeName = employee == null ? string.Empty : employee.Name;
            var filename = string.Format(AppDomain.CurrentDomain.BaseDirectory + "Temp\\Hồ sơ nhân sự " + employeeName + ".docx");
            if (io.File.Exists(filename))
                io.File.Delete(filename);
            using (var generate = new MemoryStream())
            {
                //haudv
                //using (var package = WordprocessingDocument.Create(generate, WordprocessingDocumentType.Document))
                //{
                //    var main = package.MainDocumentPart;
                //    if (main == null)
                //    {
                //        main = package.AddMainDocumentPart();
                //        new DocumentFormat.OpenXml.Wordprocessing.Document(new Body()).Save(main);
                //    }
                //    var converter = new HtmlConverter(main);
                //    converter.BaseImageUrl = new Uri(Request.Url.Scheme + "://" + Request.Url.Authority);
                //    var body = main.Document.Body;
                //    var paragraphs = converter.Parse(HttpUtility.HtmlDecode(pHtml));
                //    for (int i = 0; i < paragraphs.Count; i++)
                //    {
                //        body.Append(paragraphs[i]);
                //    }
                //    main.Document.Save();
                //}
                io.File.WriteAllBytes(filename, generate.ToArray());
            }
            this.ShowNotify(Common.Resource.SystemSuccess);
            return this.Direct();
        }

        public ActionResult FormPreviewProfile(Guid employeeId)
        {
            SetLocalReport(employeeId);
            return View();
        }

        private void SetLocalReport(Guid employeeId)
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = webcontrol.Unit.Percentage(100);
            reportViewer.Height = webcontrol.Unit.Percentage(100);

            var ms = new ProfileMasterService();

            var profileWorkExperience = new VPHumanProfileWorkExperienceService().GetByEmployeeIdNotPaging(employeeId).ToList();
            var profileWorkTraining = new VPHumanProfileTrainingService().GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var work = (new[] { new { StartDate = DateTime.Now, EndDate = DateTime.Now, Content = "" } }).ToList();
            foreach (var v in profileWorkExperience)
            {
                if (v.StartDate.HasValue && v.EndDate.HasValue)
                {
                    var content = string.Join(", ", new[] { v.Position, v.Department, v.PlaceOfWork });
                    work.Add(new { StartDate = (DateTime)v.StartDate, EndDate = (DateTime)v.EndDate, Content = "111111" });
                }
            }

            foreach (var v in profileWorkTraining)
            {
                if (v.StartDate.HasValue && v.EndDate.HasValue)
                {
                    var content = string.Join(", ", new[] { v.Content, v.Certificate, v.Form });
                    work.Add(new { StartDate = (DateTime)v.StartDate, EndDate = (DateTime)v.EndDate, Content = "2222222" });
                }
            }
            work.RemoveAt(0);
            work = work.OrderBy(o => o.StartDate).ToList();

            var profileDiploma = new VPHumanProfileDiplomaService().GetAllByEmployeeIdNotPaging(employeeId).ToList();
            foreach (var d in profileDiploma)
            {
                var place = d.Place;
                var major = d.Major;
                var form = d.FormOfTrainning;
                var level = d.Level;

                d.Place = ms.GetSingleMasterData("educationOrg", place).Name;
                d.Major = ms.GetSingleMasterData("educationField", major).Name;
                var timespan = "";
                if (d.StartDate.HasValue && d.EndDate.HasValue)
                {
                    timespan = ((DateTime)d.StartDate).ToString("dd/MM/yyyy") + " - " + ((DateTime)d.EndDate).ToString("dd/MM/yyyy");
                }
                d.Faculty = timespan;
                d.FormOfTrainning = ms.GetSingleMasterData("educationType", form).Name;
                d.Level = ms.GetSingleMasterData("educationLevel", level).Name;
            }
            var profileCertificate = VPHumanProfileCertificateService.GetAllByEmployeeIDNotPaging(employeeId).ToList();
            var profileReward = VPHumanProfileRewardService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var profileDiscipline = VPHumanProfileDisciplineService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var profileAssess = VPHumanProfileAssessService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var profileContract = VPHumanProfileContractService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var profileRelationship = VPHumanProfileRelationshipService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var profileSalary = VPHumanProfileSalaryService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var profileInsurance = VPHumanProfileInsuranceService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var profileAttach = VPHumanProfileAttachmentService.GetAllByEmployeeIdNotPaging(employeeId).ToList();
            var auditTickDetail = VPHumanProfileWorkTrialService.GetByEmployeeNotPaging(employeeId).ToList();

            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"ReportDesigner\EmployeeProfile_1.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsProfileWorkExperienceDetail", profileWorkExperience));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsProfileTraining", work));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsProfileDiplomaDetail", profileDiploma));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsProfileCertificateDetail", profileCertificate));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsProfileRewardDetail", profileReward));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsProfileDisciplineDetail", profileDiscipline));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsProfileAssessDetail", profileAssess));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsProfileContractDetail", profileContract));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsProfileRelationshipDetail", profileRelationship));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsProfileSalaryDetail", profileSalary));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsProfileInsuranceDetail", profileInsurance));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsProfileAttachDetail", profileAttach));
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("dsAuditTickDetail", auditTickDetail));

            var data = VPHumanProfileCuriculmViateService.GetByEmployeeId(employeeId);
            var reportParams = new List<ReportParameter>();
            reportParams.Add(new ReportParameter("Aliases", data.Aliases));
            var birthDay = ""; var birthMonth = ""; var birthYear = "";
            if (data.DateOfBirth != null)
            {
                var dt = (DateTime)data.DateOfBirth;
                birthDay = dt.Day.ToString();
                birthMonth = dt.Month.ToString();
                birthYear = dt.Year.ToString();

            }
            reportParams.Add(new ReportParameter("BirthDay", birthDay));
            reportParams.Add(new ReportParameter("BirthMonth", birthMonth));
            reportParams.Add(new ReportParameter("BirthYear", birthYear));
            reportParams.Add(new ReportParameter("IsMale", data.IsMale == true ? "Nam" : data.IsMale == false ? "Nữ" : ""));
            var bornCommune = ms.GetSingleMasterData("commune", data.PlaceOfBirthCommune).Name;
            reportParams.Add(new ReportParameter("BornCommune", bornCommune));
            var bornDistrict = ms.GetSingleMasterData("district", data.PlaceOfBirthDistrict).Name;
            reportParams.Add(new ReportParameter("BornDistrict", bornDistrict));
            var bornCity = ms.GetSingleMasterData("city", data.PlaceOfBirthCity).Name;
            reportParams.Add(new ReportParameter("BornCity", bornCity));
            var homeCommune = ms.GetSingleMasterData("commune", data.HomeTownCommune).Name;
            reportParams.Add(new ReportParameter("HomeCommune", homeCommune));
            var homeDistrict = ms.GetSingleMasterData("district", data.HomeTownDistrict).Name;
            reportParams.Add(new ReportParameter("HomeDistrict", homeDistrict));
            var homeCity = ms.GetSingleMasterData("city", data.HomeTownCity).Name;
            reportParams.Add(new ReportParameter("HomeCity", homeCity));
            var ethnic = ms.GetSingleMasterData("ethnic", data.People).Name;
            reportParams.Add(new ReportParameter("Ethnic", ethnic));
            var religion = ms.GetSingleMasterData("religion", data.Religion).Name;
            reportParams.Add(new ReportParameter("Religion", religion));
            reportParams.Add(new ReportParameter("PermanentResidence", data.PermanentResidence));
            reportParams.Add(new ReportParameter("CurrentResidence", data.CurrentResidence));
            reportParams.Add(new ReportParameter("NumberOfIdentityCard", data.NumberOfIdentityCard));
            reportParams.Add(new ReportParameter("DateIssueOfIdentityCard", data.DateIssueOfIdentityCard.HasValue ? String.Format("{0:dd/MM/yyyy}", data.DateIssueOfIdentityCard) : string.Empty));
            reportParams.Add(new ReportParameter("DateOnGroup", data.DateOnGroup.HasValue ? String.Format("{0:dd/MM/yyyy}", data.DateOnGroup) : string.Empty));
            reportParams.Add(new ReportParameter("PositionGroup", data.PositionGroup));
            reportParams.Add(new ReportParameter("DateAtParty", data.DateAtParty.HasValue ? String.Format("{0:dd/MM/yyyy}", data.DateAtParty) : string.Empty));
            reportParams.Add(new ReportParameter("DateOfJoinParty", data.DateOfJoinParty.HasValue ? String.Format("{0:dd/MM/yyyy}", data.DateOfJoinParty) : string.Empty));
            reportParams.Add(new ReportParameter("DateOnArmy", data.DateOnArmy.HasValue ? String.Format("{0:dd/MM/yyyy}", data.DateOnArmy) : string.Empty));
            reportParams.Add(new ReportParameter("ArmyRank", data.ArmyRank));
            reportParams.Add(new ReportParameter("Forte", data.Forte));

            var disciplines = new VPHumanProfileDisciplineService().GetAllByEmployeeIdNotPaging(employeeId);
            var disciplineMst = ms.GetMasterDataList("discipline").Rows;
            var listDiscipline = new List<string[]>();
            foreach (DataRow dr in disciplineMst)
            {
                var o = new[] { dr.ItemArray[0].ToString(), dr.ItemArray[1].ToString(), dr.ItemArray[2].ToString() };
                listDiscipline.Add(o);
            }
            listDiscipline = listDiscipline.OrderBy(o => int.Parse(o[2])).ToList();
            var disciplineForm = "";
            var disciplineYear = "";
            foreach (var v in listDiscipline)
            {
                var highestDiscipline = disciplines.FirstOrDefault(w => w.Form.ToString() == v[0]);
                if (highestDiscipline != null)
                {
                    disciplineForm = highestDiscipline.Form.ToString();
                    disciplineYear = highestDiscipline.DateOfDecision.HasValue ? " (" + ((DateTime)highestDiscipline.DateOfDecision).Year.ToString() + ")" : "";
                    break;
                }
            }
            var disciplineName = "";
            disciplineName = disciplineForm == "" ? "" : ms.GetSingleMasterData("discipline", disciplineForm).Name;
            reportParams.Add(new ReportParameter("Discipline", disciplineName + disciplineYear));

            var rewards = new VPHumanProfileRewardService().GetAllByEmployeeIdNotPaging(employeeId);
            var rewardMst = ms.GetMasterDataList("award").Rows;
            var listReward = new List<string[]>();
            foreach (DataRow dr in rewardMst)
            {
                var o = new[] { dr.ItemArray[0].ToString(), dr.ItemArray[1].ToString(), dr.ItemArray[2].ToString() };
                listReward.Add(o);
            }
            listReward = listReward.OrderBy(o => int.Parse(o[2])).ToList();
            var rewardForm = "";
            var rewardYear = "";
            foreach (var v in listReward)
            {
                var highestReward = rewards.FirstOrDefault(w => w.Form.ToString() == v[0]);
                if (highestReward != null)
                {
                    rewardForm = highestReward.Form.ToString();
                    rewardYear = highestReward.DateOfDecision.HasValue ? " (" + ((DateTime)highestReward.DateOfDecision).Year.ToString() + ")" : "";
                    break;
                }
            }
            var rewardName = "";
            rewardName = rewardForm == "" ? "" : ms.GetSingleMasterData("award", rewardForm).Name;
            reportParams.Add(new ReportParameter("Reward", rewardName + rewardYear));

            //reportParams.Add(new ReportParameter("HomePhone", data.HomePhone));
            //reportParams.Add(new ReportParameter("OfficePhone", data.OfficePhone));
            //reportParams.Add(new ReportParameter("PlaceIssueOfIdentityCard", data.PlaceIssueOfIdentityCard));
            //reportParams.Add(new ReportParameter("TaxCode", data.TaxCode));
            //reportParams.Add(new ReportParameter("NumberOfBankAccounts", data.NumberOfBankAccounts));
            //reportParams.Add(new ReportParameter("Bank", data.Bank));
            //reportParams.Add(new ReportParameter("NumberOfPassport", data.NumberOfPassport));
            //reportParams.Add(new ReportParameter("PlaceOfPassport", data.PlaceOfPassport));
            //reportParams.Add(new ReportParameter("DateOfIssuePassport", data.DateOfIssuePassport.HasValue ? String.Format("{0:dd/MM/yyyy}", data.DateOfIssuePassport) : string.Empty));
            //reportParams.Add(new ReportParameter("PassportExpirationDate", data.PassportExpirationDate.HasValue ? String.Format("{0:dd/MM/yyyy}", data.PassportExpirationDate) : string.Empty));
            //reportParams.Add(new ReportParameter("PlaceOfLoadedGroup", data.PlaceOfLoadedGroup));
            //reportParams.Add(new ReportParameter("DateJoinRevolution", data.DateJoinRevolution.HasValue ? String.Format("{0:dd/MM/yyyy}", data.DateJoinRevolution) : string.Empty));
            //reportParams.Add(new ReportParameter("PlaceOfLoadedParty", data.PlaceOfLoadedParty));
            //reportParams.Add(new ReportParameter("NumberOfPartyCard", data.NumberOfPartyCard));
            //reportParams.Add(new ReportParameter("PositionArmy", data.PositionArmy));
            //reportParams.Add(new ReportParameter("Likes", data.Likes));
            //reportParams.Add(new ReportParameter("Defect", data.Defect));

            var employee = EmployeeService.GetById(employeeId);
            reportParams.Add(new ReportParameter("EmployeeName", employee.Name.ToUpper()));

            reportViewer.LocalReport.SetParameters(reportParams);
            ViewBag.ReportViewer = reportViewer;
        }

    }
}