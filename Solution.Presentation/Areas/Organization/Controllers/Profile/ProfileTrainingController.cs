﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;

namespace iDAS.Presentation.Areas.Organization.Controllers
{

    public class ProfileTrainingController : FrontController
    {
        #region   Hồ sơ quá trình đào tạo
        public ActionResult Index(string id)
        {
            ViewData["EmployeeId"] = id;
            ViewData["EmployeeName"] = EmployeeService.GetById(Guid.Parse(id)).Name;
            return View();
        }
        public PartialViewResult IndexAll(string containerId)
        {
            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult StatisticIndex(string containerId)
        {
            return PartialView();
        }

        public ActionResult LoadProfileTraining(StoreRequestParameters parameters, Guid EmployeeID)
        {
            int totalCount;
            var result = VPHumanProfileTrainingService.GetAllByEmployeeId(EmployeeID, parameters.Page, parameters.Limit, out totalCount);

            foreach (var data in result)
            {
                var typeId = data.Form;
                if (typeId != null && typeId != "" && Guid.TryParse(typeId, out Guid gid))
                {
                    data.Form = new ProfileMasterService().GetSingleMasterData("educationType", typeId).Name;
                }

                var resultId = data.Result;
                if (resultId != null && resultId != "" && Guid.TryParse(resultId, out Guid gid1))
                {
                    data.Result = new ProfileMasterService().GetSingleMasterData("educationResult", resultId).Name;
                }
            }
            return this.Store(result, totalCount);
        }
        public ActionResult LoadProfileTrainingAll(StoreRequestParameters param, string name = "", string educationType = "", string educationResult = "", string DeparmentId = "", string TrainingName = "")
        {
            //Query dữ liệu thô
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileTraining_LoadProfileTrainingAll",
               parameter: new
               {
                   name = !string.IsNullOrEmpty(name) ? name : "",
                   TrainingName = !string.IsNullOrEmpty(TrainingName) ? TrainingName : "",
                   educationType = !string.IsNullOrEmpty(educationType) ? ((educationType == "Tất cả" || educationType == "null") ? "" : educationType) : "",
                   DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                   educationResult = !string.IsNullOrEmpty(educationResult) ? ((educationResult == "Tất cả" || educationResult == "null") ? "" : educationResult) : "",
                   pageIndex = param.Page,
                   pageSize = param.Limit
               }
            );
            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            Name = "",
                                            StartDate = DateTime.Now,
                                            EndDate = DateTime.Now,
                                            TypeId = "",
                                            Type = "",
                                            Content = "",
                                            Certificate = "",
                                            ResultId = "",
                                            Result = "",
                                            Reviews = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            int count = 0;
            if (dataTable != null && dataTable.Tables[0] != null && dataTable.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(dataTable.Tables[1].Rows[0][0].ToString());
                foreach (DataRow data in dataTable.Tables[0].Rows)
                {
                    var employeeId = data["HumanEmployeeId"] != null && !string.IsNullOrEmpty(data["HumanEmployeeId"].ToString()) ? new Guid(data["HumanEmployeeId"].ToString()) : Guid.Empty;
                    var Id = data["Id"] != null && !string.IsNullOrEmpty(data["Id"].ToString()) ? new Guid(data["Id"].ToString()) : Guid.Empty;
                    var files = new VPHumanProfileAttachmentFileService().GetByFile(Id);
                    var FileAttachs = new FileUploadBO()
                    {
                        Files = files.ToList()
                    };
                    customResult.Add(new
                    {
                        Id = Id,
                        Code = (data["Code"] != null && !string.IsNullOrEmpty(data["Code"].ToString())) ? data["Code"].ToString() : "",
                        RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId)),
                        EmployeeId = employeeId,
                        EmployeeName = (data["EmployeeName"] != null && !string.IsNullOrEmpty(data["EmployeeName"].ToString())) ? data["EmployeeName"].ToString() : "",
                        Name = (data["Name"] != null && !string.IsNullOrEmpty(data["Name"].ToString())) ? data["Name"].ToString() : "",
                        StartDate = (data["StartDate"] != null && !string.IsNullOrEmpty(data["StartDate"].ToString())) ? DateTime.Parse(data["StartDate"].ToString()) : DateTime.Now,
                        EndDate = (data["EndDate"] != null && !string.IsNullOrEmpty(data["EndDate"].ToString())) ? DateTime.Parse(data["EndDate"].ToString()) : DateTime.Now,
                        TypeId = (data["TypeId"] != null && !string.IsNullOrEmpty(data["TypeId"].ToString())) ? data["TypeId"].ToString() : "",
                        Type = (data["Type"] != null && !string.IsNullOrEmpty(data["Type"].ToString())) ? data["Type"].ToString() : "",
                        Content = (data["Content"] != null && !string.IsNullOrEmpty(data["Content"].ToString())) ? data["Content"].ToString() : "",
                        Certificate = (data["Certificate"] != null && !string.IsNullOrEmpty(data["Certificate"].ToString())) ? data["Certificate"].ToString() : "",
                        ResultId = (data["ResultId"] != null && !string.IsNullOrEmpty(data["ResultId"].ToString())) ? data["ResultId"].ToString() : "",
                        Result = (data["Result"] != null && !string.IsNullOrEmpty(data["Result"].ToString())) ? data["Result"].ToString() : "",
                        Reviews = (data["Reviews"] != null && !string.IsNullOrEmpty(data["Reviews"].ToString())) ? data["Reviews"].ToString() : "",
                        FileAttachs = FileAttachs
                    });

                }
            }
            customResult.RemoveAt(0);
            //Sắp xếp theo tên nhân viên, nếu trùng tên thì xếp theo Id
            //customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult, count);
        }
        public void exportexcel(string name = "", string educationType = "", string educationResult = "", string DeparmentId = "", string TrainingName = "")
        {
            //Query dữ liệu thô
            if (DeparmentId == "null")
                DeparmentId = "";
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileTraining_LoadProfileTrainingAll",
              parameter: new
              {
                  name = !string.IsNullOrEmpty(name) ? name : "",
                  TrainingName = !string.IsNullOrEmpty(TrainingName) ? TrainingName : "",
                  educationType = !string.IsNullOrEmpty(educationType) ? ((educationType == "Tất cả" || educationType == "null") ? "" : educationType) : "",
                  DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                  educationResult = !string.IsNullOrEmpty(educationResult) ? ((educationResult == "Tất cả" || educationResult == "null") ? "" : educationResult) : "",
                  pageIndex = -1,
                  pageSize = -1
              }
           );
            foreach (DataRow row in dataTable.Tables[0].Rows)
            {
                row["RoleName"] = string.Join(", ", EmployeeService.GetDeparmentRoleNames(new Guid(row["HumanEmployeeId"].ToString())));
            }
            DataTable tablecopy = dataTable.Tables[0].Copy();
            foreach (DataColumn col in tablecopy.Columns)
            {
                if (col.ColumnName != "EmployeeName"
                    && col.ColumnName != "Name"
                    && col.ColumnName != "Code"
                    && col.ColumnName != "RoleName"
                    && col.ColumnName != "Type"
                    && col.ColumnName != "Content"
                    && col.ColumnName != "Certificate"
                    && col.ColumnName != "Result")
                {
                    dataTable.Tables[0].Columns.Remove(col.ColumnName);
                }
                else
                {
                    if (col.DataType.Name == "DateTime")
                    {
                        dataTable.Tables[0].Columns.Add(col.ColumnName + "_Extra", typeof(String));
                        foreach (DataRow data in dataTable.Tables[0].Rows)
                        {
                            data[col.ColumnName + "_Extra"] = (data[col.ColumnName] != null && !string.IsNullOrEmpty(data[col.ColumnName].ToString())) ? DateTime.Parse(data[col.ColumnName].ToString()).ToString("dd/MM/yyyy") : "";
                        }
                        dataTable.Tables[0].Columns.Remove(col.ColumnName);
                        dataTable.Tables[0].Columns[col.ColumnName + "_Extra"].ColumnName = col.ColumnName;
                    }
                }
            }
            //foreach (DataRow row in dataTable.Tables[0].Rows)
            //{
            //    var a = row[0].GetType().Name;
            //}
            dataTable.Tables[0].Columns["Code"].SetOrdinal(0);
            dataTable.Tables[0].Columns["EmployeeName"].SetOrdinal(1);
            dataTable.Tables[0].Columns["RoleName"].SetOrdinal(2);
            dataTable.Tables[0].Columns["Name"].SetOrdinal(3);
            dataTable.Tables[0].Columns["Type"].SetOrdinal(4);
            dataTable.Tables[0].Columns["Content"].SetOrdinal(5);
            dataTable.Tables[0].Columns["Certificate"].SetOrdinal(6);
            dataTable.Tables[0].Columns["Result"].SetOrdinal(7);

            dataTable.Tables[0].Columns["Code"].ColumnName = "Mã nhân viên";
            dataTable.Tables[0].Columns["EmployeeName"].ColumnName = "Tên nhân viên";
            dataTable.Tables[0].Columns["RoleName"].ColumnName = "Chức vụ";
            dataTable.Tables[0].Columns["Name"].ColumnName = "Tên khóa học";
            dataTable.Tables[0].Columns["Type"].ColumnName = "Hình thức đào tạo";
            dataTable.Tables[0].Columns["Content"].ColumnName = "Nội dung đào tạo";
            dataTable.Tables[0].Columns["Certificate"].ColumnName = "Chứng chỉ đào tạo";
            dataTable.Tables[0].Columns["Result"].ColumnName = "Kết quả đào tạo";
            Common.Utilities.ExportDataTableToExcel(dataTable.Tables[0], "Danh sách hồ sơ quá trình đào tạo");
        }
        public Guid toGuid(Guid? source)
        {
            return source ?? Guid.Empty;
        }
        public ActionResult LoadProfileTrainingNotPaging(Guid EmployeeID)
        {
            var result = VPHumanProfileTrainingService.GetAllByEmployeeIdNotPaging(EmployeeID);
            var profileMasterService = new ProfileMasterService();
            var employeeService = new EmployeeService();

            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            EmployeeId = Guid.Empty,
                                            Name = "",
                                            StartDate = (DateTime?)DateTime.Now,
                                            EndDate = (DateTime?)DateTime.Now,
                                            Form = "",
                                            Content = "",
                                            Certificate = "",
                                            Result = "",
                                            Reviews = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            foreach (var data in result)
            {
                var typeId = data.Form;
                var typeName = "";
                if (typeId != null && typeId != "" && Guid.TryParse(typeId, out Guid gid))
                {
                    typeName = profileMasterService.GetSingleMasterData("educationType", typeId).Name;
                }

                var resultId = data.Result;
                var resultName = "";
                if (resultId != null && resultId != "" && Guid.TryParse(resultId, out Guid gid1))
                {
                    resultName = profileMasterService.GetSingleMasterData("educationResult", resultId).Name;
                }

                var employeeId = data.HumanEmployeeId ?? Guid.Empty;
                var employeeName = "";

                if (employeeId != Guid.Empty)
                {
                    employeeName = employeeService.GetById(employeeId).Name;
                }
                customResult.Add(new { Id = data.Id, EmployeeId = employeeId, Name = data.Name, StartDate = data.StartDate, EndDate = data.EndDate, Form = typeName, Content = data.Content, Certificate = data.Certificate, Result = resultName, Reviews = data.Reviews, FileAttachs = data.FileAttachs });
            }
            customResult.RemoveAt(0);
            return this.Store(customResult);
        }

        public ActionResult DeleteProfileTraining(Guid id)
        {
            try
            {
                VPHumanProfileTrainingService.Delete(id);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileTraining").Reload();
            }
            return this.Direct();
        }
        #endregion

        public ActionResult UpdateForm(string id, Guid EmployeeID)
        {
            VPHumanProfileTrainingBO data;
            if (!string.IsNullOrEmpty(id))
            {
                data = VPHumanProfileTrainingService.GetdataById(new Guid(id));
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
            else
            {
                data = new VPHumanProfileTrainingBO() { HumanEmployeeId = EmployeeID };
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="exit"></param>
        /// <returns></returns>
        public ActionResult Update(VPHumanProfileTrainingBO updateData)
        {
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            // Kiểm tra quyền edit
            if (updateData.HumanEmployeeId != null && s.CheckTitleRole_Edit((Guid)updateData.HumanEmployeeId) == false)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }

            try
            {
                if (updateData.Id == Guid.Empty)
                {
                    VPHumanProfileTrainingService.HumanProfileTraining(updateData);
                }
                else
                {
                    VPHumanProfileTrainingService.HumanProfileTraining(updateData);

                }
                this.ShowNotify(Common.Resource.SystemSuccess);
                X.GetCmp<Window>("winTraining").Close();
                X.GetCmp<Store>("StoreProfileTraining").Reload();
                return this.Direct();

            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return RedirectToAction("Index");
            }
        }

    }
}
