﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;
namespace iDAS.Presentation.Areas.Organization.Controllers.Profile
{
    public class ProfileAllowanceController : FrontController
    {
        #region   Hồ sơ chợ cấp
        public ActionResult Index(string id)
        {
            ViewData["EmployeeId"] = id;
            ViewData["EmployeeName"] = EmployeeService.GetById(Guid.Parse(id)).Name;
            return View();
        }
        public PartialViewResult IndexAll(string containerId)
        {
            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                WrapByScriptTag = false
                //check view
            };
        }
        public ActionResult LoadProfileAllowancesAll(StoreRequestParameters param, string name = "", string DeparmentId = "", string NumberDecisions = "")
        {
            //Query dữ liệu thô
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileAllowances_LoadProfileAllowancesAll",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    NumberDecisions = !string.IsNullOrEmpty(NumberDecisions) ? NumberDecisions : "",
                    pageIndex = param.Page,
                    pageSize = param.Limit
                }
            );
            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            NumberDecisions = "",
                                            Note = "",
                                            PCCV = "",
                                            Position = "",
                                            DateAppoint = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            int count = 0;
            if (dataTable != null && dataTable.Tables[0] != null && dataTable.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(dataTable.Tables[1].Rows[0][0].ToString());
                foreach (DataRow data in dataTable.Tables[0].Rows)
                {
                    var employeeId = data["EmployeeId"] != null && !string.IsNullOrEmpty(data["EmployeeId"].ToString()) ? new Guid(data["EmployeeId"].ToString()) : Guid.Empty;
                    var Id = data["Id"] != null && !string.IsNullOrEmpty(data["Id"].ToString()) ? new Guid(data["Id"].ToString()) : Guid.Empty;
                    var files = new VPHumanProfileAttachmentFileService().GetByFile(Id);
                    var FileAttachs = new FileUploadBO()
                    {
                        Files = files.ToList()
                    };
                    customResult.Add(new
                    {
                        Id = Id,
                        Code = (data["Code"] != null && !string.IsNullOrEmpty(data["Code"].ToString())) ? data["Code"].ToString() : "",
                        RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId)),
                        EmployeeId = employeeId,
                        EmployeeName = (data["Name"] != null && !string.IsNullOrEmpty(data["Name"].ToString())) ? data["Name"].ToString() : "",
                        NumberDecisions = (data["NumberDecisions"] != null && !string.IsNullOrEmpty(data["NumberDecisions"].ToString())) ? data["NumberDecisions"].ToString() : "",
                        Note = (data["Note"] != null && !string.IsNullOrEmpty(data["Note"].ToString())) ? data["Note"].ToString() : "",
                        PCCV = (data["PCCV"] != null && !string.IsNullOrEmpty(data["PCCV"].ToString())) ? data["PCCV"].ToString() : "",
                        Position = (data["Position"] != null && !string.IsNullOrEmpty(data["Position"].ToString())) ? data["Position"].ToString() : "",
                        DateAppoint = (data["DateAppoint"] != null && !string.IsNullOrEmpty(data["DateAppoint"].ToString())) ? DateTime.Parse(data["DateAppoint"].ToString()).ToString("dd/MM/yyyy") : "",
                        FileAttachs = FileAttachs
                    });
                }
            }
            customResult.RemoveAt(0);
            //Sắp xếp theo tên nhân viên, nếu trùng thì xếp theo Id
            //customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult, count);
        }
        public void exportexcel(string name = "", string DeparmentId = "", string NumberDecisions = "")
        {
            //Query dữ liệu thô
            if (DeparmentId == "null")
                DeparmentId = "";
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileAllowances_LoadProfileAllowancesAll",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    NumberDecisions = !string.IsNullOrEmpty(NumberDecisions) ? NumberDecisions : "",
                    pageIndex = -1,
                    pageSize = -1
                }
            );
            foreach (DataRow row in dataTable.Tables[0].Rows)
            {
                row["RoleName"] = string.Join(", ", EmployeeService.GetDeparmentRoleNames(new Guid(row["EmployeeId"].ToString())));
            }
            DataTable tablecopy = dataTable.Tables[0].Copy();
            foreach (DataColumn col in tablecopy.Columns)
            {
                if (col.ColumnName != "Name"
                    && col.ColumnName != "Code"
                    && col.ColumnName != "RoleName"
                    && col.ColumnName != "NumberDecisions"
                    && col.ColumnName != "Note"
                    && col.ColumnName != "PCCV"
                    && col.ColumnName != "Position"
                    && col.ColumnName != "DateAppoint")
                {
                    dataTable.Tables[0].Columns.Remove(col.ColumnName);
                }
                else
                {
                    if (col.DataType.Name == "DateTime")
                    {
                        dataTable.Tables[0].Columns.Add(col.ColumnName + "_Extra", typeof(String));
                        foreach (DataRow data in dataTable.Tables[0].Rows)
                        {
                            data[col.ColumnName + "_Extra"] = (data[col.ColumnName] != null && !string.IsNullOrEmpty(data[col.ColumnName].ToString())) ? DateTime.Parse(data[col.ColumnName].ToString()).ToString("dd/MM/yyyy") : "";
                        }
                        dataTable.Tables[0].Columns.Remove(col.ColumnName);
                        dataTable.Tables[0].Columns[col.ColumnName + "_Extra"].ColumnName = col.ColumnName;
                    }
                }
            }
            dataTable.Tables[0].Columns["Code"].SetOrdinal(0);
            dataTable.Tables[0].Columns["Name"].SetOrdinal(1);
            dataTable.Tables[0].Columns["RoleName"].SetOrdinal(2);
            dataTable.Tables[0].Columns["NumberDecisions"].SetOrdinal(3);
            dataTable.Tables[0].Columns["Note"].SetOrdinal(4);
            dataTable.Tables[0].Columns["PCCV"].SetOrdinal(5);
            dataTable.Tables[0].Columns["Position"].SetOrdinal(6);
            dataTable.Tables[0].Columns["DateAppoint"].SetOrdinal(7);

            dataTable.Tables[0].Columns["Code"].ColumnName = "Mã Nhân sự";
            dataTable.Tables[0].Columns["Name"].ColumnName = "Tên Nhân sự";
            dataTable.Tables[0].Columns["RoleName"].ColumnName = "Chức vụ";
            dataTable.Tables[0].Columns["NumberDecisions"].ColumnName = "Số quyết định";
            dataTable.Tables[0].Columns["Note"].ColumnName = "Ghi chú";
            dataTable.Tables[0].Columns["PCCV"].ColumnName = "Hệ số PCCV";
            dataTable.Tables[0].Columns["Position"].ColumnName = "Chức vụ đảm nhận";
            dataTable.Tables[0].Columns["DateAppoint"].ColumnName = "Thời gian bắt đầu bổ nhiệm";
            Common.Utilities.ExportDataTableToExcel(dataTable.Tables[0], "Danh sách hồ sơ phụ cấp chức vụ");
        }

        public ActionResult LoadProfileAllowance(StoreRequestParameters parameters, Guid EmployeeID)
        {
            int totalCount;
            var result = VPHumanProfileAllowanceService.GetAllByEmployeeId(EmployeeID, parameters.Page, parameters.Limit, out totalCount);
            return this.Store(result, totalCount);
        }

        public ActionResult LoadProfileAllowanceNotPaging(Guid EmployeeID)
        {
            var result = VPHumanProfileAllowanceService.GetAllByEmployeeIdNotPaging(EmployeeID);
            return this.Store(result);
        }

        public ActionResult DeleteProfileAllowance(Guid id)
        {
            try
            {
                VPHumanProfileAllowanceService.Delete(id);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileAllowance").Reload();
            }
            return this.Direct();
        }
        #endregion

        public ActionResult UpdateForm(string ID, Guid EmployeeID)
        {
            var data = new VPHumanProfileAllowanceBO();
            if (!string.IsNullOrEmpty(ID))
            {
                data = VPHumanProfileAllowanceService.GetdataById(new Guid(ID));
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
            else
            {
                data.EmployeeId = EmployeeID;
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }

        }
        public ActionResult Update(VPHumanProfileAllowanceBO updateData)
        {
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            // Kiểm tra quyền edit
            if (updateData.EmployeeId != null && s.CheckTitleRole_Edit((Guid)updateData.EmployeeId) == false)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }

            try
            {
                if (updateData.Id == Guid.Empty)
                {
                    VPHumanProfileAllowanceService.VPHumanProfileAllowance(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    VPHumanProfileAllowanceService.VPHumanProfileAllowance(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                X.GetCmp<Window>("winAllowance").Close();
                X.GetCmp<Store>("StoreProfileAllowance").Reload();
                return this.Direct();

            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return RedirectToAction("Index");
            }
        }
    }
}