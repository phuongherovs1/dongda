﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class ProfileAssessController : FrontController
    {

        #region Hồ sơ đánh giá
        public ActionResult Index(string id)
        {
            ViewData["EmployeeId"] = id;
            ViewData["EmployeeName"] = EmployeeService.GetById(Guid.Parse(id)).Name;
            return View();
        }

        public PartialViewResult IndexAll(string containerId)
        {
            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult LoadProfileAssess(StoreRequestParameters parameters, Guid EmployeeID)
        {
            int totalCount;
            var result = VPHumanProfileAssessService.GetAllByEmployeeId(EmployeeID, parameters.Page, parameters.Limit, out totalCount);
            return this.Store(result, totalCount);
        }

        public ActionResult LoadProfileAssessNotPaging(Guid EmployeeID)
        {
            var result = VPHumanProfileAssessService.GetAllByEmployeeIdNotPaging(EmployeeID);
            var profileMasterService = new ProfileMasterService();
            var employeeService = new EmployeeService();

            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            Name = "",
                                            StartDate = (DateTime?)DateTime.Now,
                                            EndDate = (DateTime?)DateTime.Now,
                                            Score = "",
                                            Result = "",
                                            Note = ""
                                            }}).ToList();
            //Chuyển các trường guid sang string
            foreach (var data in result)
            {
                var employeeId = data.HumanEmployeeId ?? Guid.Empty;
                var employeeName = "";

                if (employeeId != Guid.Empty)
                {
                    employeeName = employeeService.GetById(employeeId).Name;
                }
                customResult.Add(new { Id = data.Id, EmployeeId = employeeId, EmployeeName = employeeName, Name = data.Name, StartDate = data.StartDate, EndDate = data.EndDate, Score = data.Score, Result = data.Result, Note = data.Note });
            }
            customResult.RemoveAt(0);

            //Sắp xếp theo tên nhân viên, nếu trùng thì xếp theo Id
            //customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult);
        }

        public ActionResult LoadProfileAssessAll(StoreRequestParameters param, string name = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            if (DeparmentId == "null")
                DeparmentId = "";
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileAssesses_LoadProfileAssesses",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    pageIndex = param.Page,
                    pageSize = param.Limit
                }
            );
            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            Name = "",
                                            StartDate = "",
                                            EndDate = "",
                                            Score = "",
                                            Result = "",
                                            Note = ""
                                            }}).ToList();
            //Chuyển các trường guid sang string
            int count = 0;
            if (dataTable != null && dataTable.Tables[0] != null && dataTable.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(dataTable.Tables[1].Rows[0][0].ToString());
                foreach (DataRow data in dataTable.Tables[0].Rows)
                {
                    var employeeId = data["HumanEmployeeId"] != null && !string.IsNullOrEmpty(data["HumanEmployeeId"].ToString()) ? new Guid(data["HumanEmployeeId"].ToString()) : Guid.Empty;
                    var Id = data["Id"] != null && !string.IsNullOrEmpty(data["Id"].ToString()) ? new Guid(data["Id"].ToString()) : Guid.Empty;
                    customResult.Add(new
                    {
                        Id = Id,
                        Code = (data["Code"] != null && !string.IsNullOrEmpty(data["Code"].ToString())) ? data["Code"].ToString() : "",
                        RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId)),
                        EmployeeId = employeeId,
                        EmployeeName = (data["EmployeeName"] != null && !string.IsNullOrEmpty(data["EmployeeName"].ToString())) ? data["EmployeeName"].ToString() : "",
                        Name = (data["Name"] != null && !string.IsNullOrEmpty(data["Name"].ToString())) ? data["Name"].ToString() : "",
                        StartDate = (data["StartDate"] != null && !string.IsNullOrEmpty(data["StartDate"].ToString())) ? DateTime.Parse(data["StartDate"].ToString()).ToString("dd/MM/yyyy") : "",
                        EndDate = (data["EndDate"] != null && !string.IsNullOrEmpty(data["EndDate"].ToString())) ? DateTime.Parse(data["EndDate"].ToString()).ToString("dd/MM/yyyy") : "",
                        Score = (data["Score"] != null && !string.IsNullOrEmpty(data["Score"].ToString())) ? data["Score"].ToString() : "",
                        Result = (data["Result"] != null && !string.IsNullOrEmpty(data["Result"].ToString())) ? data["Result"].ToString() : "",
                        Note = (data["Note"] != null && !string.IsNullOrEmpty(data["Note"].ToString())) ? data["Note"].ToString() : ""
                    });
                }
            }
            customResult.RemoveAt(0);
            //Sắp xếp theo tên nhân viên, nếu trùng thì xếp theo Id
            customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult,count);
        }
        public void exportexcel(string name = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileAssesses_LoadProfileAssesses",
                parameter: new
                {
                    name = !string.IsNullOrEmpty(name) ? name : "",
                    DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                    pageIndex = -1,
                    pageSize = -1
                }
            );
            foreach (DataRow row in dataTable.Tables[0].Rows)
            {
                row["RoleName"] = string.Join(", ", EmployeeService.GetDeparmentRoleNames(new Guid(row["HumanEmployeeId"].ToString())));
            }
            DataTable tablecopy = dataTable.Tables[0].Copy();
            foreach (DataColumn col in tablecopy.Columns)
            {
                if (col.ColumnName != "EmployeeName"
                    && col.ColumnName != "Name"
                    && col.ColumnName != "Code"
                    && col.ColumnName != "RoleName"
                    && col.ColumnName != "StartDate"
                    && col.ColumnName != "Result"
                    && col.ColumnName != "EndDate"
                    && col.ColumnName != "Note"
                    && col.ColumnName != "Score")
                {
                    dataTable.Tables[0].Columns.Remove(col.ColumnName);
                }
                else
                {
                    if (col.DataType.Name == "DateTime")
                    {
                        dataTable.Tables[0].Columns.Add(col.ColumnName + "_Extra", typeof(String));
                        foreach (DataRow data in dataTable.Tables[0].Rows)
                        {
                            data[col.ColumnName + "_Extra"] = (data[col.ColumnName] != null && !string.IsNullOrEmpty(data[col.ColumnName].ToString())) ? DateTime.Parse(data[col.ColumnName].ToString()).ToString("dd/MM/yyyy") : "";
                        }
                        dataTable.Tables[0].Columns.Remove(col.ColumnName);
                        dataTable.Tables[0].Columns[col.ColumnName + "_Extra"].ColumnName = col.ColumnName;
                    }
                }
            }
            dataTable.Tables[0].Columns["Code"].SetOrdinal(0);
            dataTable.Tables[0].Columns["EmployeeName"].SetOrdinal(1);
            dataTable.Tables[0].Columns["RoleName"].SetOrdinal(2);
            dataTable.Tables[0].Columns["Name"].SetOrdinal(3);
            dataTable.Tables[0].Columns["StartDate"].SetOrdinal(4);
            dataTable.Tables[0].Columns["EndDate"].SetOrdinal(5);
            dataTable.Tables[0].Columns["Score"].SetOrdinal(6);
            dataTable.Tables[0].Columns["Result"].SetOrdinal(7);
            dataTable.Tables[0].Columns["Note"].SetOrdinal(8);

            dataTable.Tables[0].Columns["Code"].ColumnName = "Mã nhân viên";
            dataTable.Tables[0].Columns["EmployeeName"].ColumnName = "Tên nhân viên";
            dataTable.Tables[0].Columns["RoleName"].ColumnName = "Chức vụ";
            dataTable.Tables[0].Columns["Name"].ColumnName = "Đợt đánh giá";
            dataTable.Tables[0].Columns["StartDate"].ColumnName = "Ngày bắt đầu";
            dataTable.Tables[0].Columns["EndDate"].ColumnName = "Ngày kết thúc";
            dataTable.Tables[0].Columns["Score"].ColumnName = "Điểm";
            dataTable.Tables[0].Columns["Result"].ColumnName = "Kết quả";
            dataTable.Tables[0].Columns["Note"].ColumnName = "Ghi chú";
            Common.Utilities.ExportDataTableToExcel(dataTable.Tables[0], "Danh sách hồ sơ dánh giá");
        }
        public ActionResult DeleteProfileAssess(Guid id)
        {
            try
            {
                VPHumanProfileAssessService.Delete(id);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileAssess").Reload();
            }
            return this.Direct();
        }

        public ActionResult DetailForm(string ID, Guid EmployeeID)
        {
            var data = VPHumanProfileAssessService.GetById(new Guid(ID));
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
        }

        public ActionResult UpdateForm(string ID, Guid EmployeeID)
        {
            var data = new VPHumanProfileAssessBO();
            if (!string.IsNullOrEmpty(ID))
            {
                data = VPHumanProfileAssessService.GetById(new Guid(ID));
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
            else
            {
                data.HumanEmployeeId = EmployeeID;
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="exit"></param>
        /// <returns></returns>
        public ActionResult Update(VPHumanProfileAssessBO updateData)
        {
            try
            {
                if (updateData.Id == Guid.Empty)
                {
                    VPHumanProfileAssessService.Insert(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    VPHumanProfileAssessService.Update(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                X.GetCmp<Window>("WinAssess").Close();
                X.GetCmp<Store>("StoreProfileAssess").Reload();
                return this.Direct();

            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return RedirectToAction("Index");
            }
        }
        #endregion

    }
}
