﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;
namespace iDAS.Presentation.Areas.Organization.Controllers
{


    public class ProfileAttachController : FrontController
    {

        #region Hồ sơ đính kèm

        public ActionResult Index(string id)
        {
            ViewData["EmployeeId"] = id;
            ViewData["EmployeeName"] = EmployeeService.GetById(Guid.Parse(id)).Name;
            return View();
        }

        public PartialViewResult IndexAll(string containerId)
        {
            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                WrapByScriptTag = false
                //check view
            };
        }
        public ActionResult LoadProfileAttachAll(string name = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            int totalCount;
            var result = VPHumanProfileAttachmentService.GetAll();
            var profileMasterService = new ProfileMasterService();
            var employeeService = new EmployeeService();

            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            Name = "",
                                            Note = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            foreach (var data in result)
            {
                //var relationId = data.Relationship;
                //var relationName = "";
                //if (relationId != null && relationId != "" && Guid.TryParse(relationId, out Guid id))
                //{
                //    relationName = profileMasterService.GetSingleMasterData("family", relationId).Name;
                //}

                var employeeId = data.HumanEmployeeId ?? Guid.Empty;
                var employeeName = "";
                var Code = "";

                if (employeeId != Guid.Empty)
                {
                    var employee = employeeService.GetById(employeeId);
                    employeeName = employee.Name;
                    Code = employee.Code;
                }
                customResult.Add(new { Id = data.Id, Code = Code, RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId)), EmployeeId = employeeId, EmployeeName = employeeName, Name = data.Name, Note = data.Note, FileAttachs = data.FileAttachs });
            }
            customResult.RemoveAt(0);

            //Filter dữ liệu 
            if (name != "") customResult = customResult.FindAll(f => f.EmployeeName.ToLower().Contains(name.ToLower()));
            //if (relation != "" && relation != "Tất cả") customResult = customResult.FindAll(f => f.RelationId == relation);

            //Sắp xếp theo tên nhân viên, nếu trùng thì xếp theo Id
            customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult, customResult.Count);
        }
        public void exportexcel(string name = "", string DeparmentId = "")
        {
            var result = VPHumanProfileAttachmentService.GetAll();
            var profileMasterService = new ProfileMasterService();
            var employeeService = new EmployeeService();
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            Name = "",
                                            Note = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            foreach (var data in result)
            {
                var employeeId = data.HumanEmployeeId ?? Guid.Empty;
                var employeeName = "";
                var Code = "";
                if (employeeId != Guid.Empty)
                {
                    var employee = employeeService.GetById(employeeId);
                    employeeName = employee.Name;
                    Code = employee.Code;
                }
                customResult.Add(new { 
                    Id = data.Id, 
                    Code = Code,
                    RoleName = string.Join(", ", EmployeeService.GetDeparmentRoleNames(employeeId)),
                    EmployeeId = employeeId, 
                    EmployeeName = employeeName, 
                    Name = data.Name, 
                    Note = data.Note, 
                    FileAttachs = data.FileAttachs 
                });
            }
            customResult.RemoveAt(0);
            if (name != "") customResult = customResult.FindAll(f => f.EmployeeName.ToLower().Contains(name.ToLower()));
            customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("Mã Nhân sự", typeof(string));
            dataTable.Columns.Add("Tên Nhân sự", typeof(string));
            dataTable.Columns.Add("Chức vụ", typeof(string));
            dataTable.Columns.Add("Tên tài liệu", typeof(string));
            dataTable.Columns.Add("Ghi chú", typeof(string));
            foreach(var data in customResult)
            {
                DataRow rowitem = dataTable.NewRow();
                rowitem["Mã Nhân sự"] = data.Code;
                rowitem["Tên Nhân sự"] = data.EmployeeName;
                rowitem["Chức vụ"] = data.RoleName;
                rowitem["Tên tài liệu"] = data.Name;
                rowitem["Ghi chú"] = data.Note;
                dataTable.Rows.Add(rowitem);
            }
            Common.Utilities.ExportDataTableToExcel(dataTable, "Danh sách hồ sơ đính kèm");
        }
        public ActionResult LoadProfileAttach(StoreRequestParameters parameters, Guid EmployeeID)
        {
            int totalCount;
            var result = VPHumanProfileAttachmentService.GetAllByEmployeeId(parameters.Page, parameters.Limit, out totalCount, EmployeeID);
            return this.Store(result, totalCount);
        }

        public ActionResult LoadProfileAttachNotPaging(Guid EmployeeID)
        {
            var result = VPHumanProfileAttachmentService.GetAllByEmployeeIdNotPaging(EmployeeID);
            return this.Store(result);
        }

        public ActionResult DeleteProfileAttach(Guid id)
        {
            try
            {
                VPHumanProfileAttachmentService.Delete(id);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileAttach").Reload();
            }
            return this.Direct();
        }


        public ActionResult DetailForm(string ID, Guid EmployeeID)
        {
            var data = VPHumanProfileAttachmentService.GetById(new Guid(ID));
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
        }


        public ActionResult UpdateForm(string ID, Guid EmployeeID)
        {
            var data = new VPHumanProfileAttachmentBO();
            if (!string.IsNullOrEmpty(ID))
            {
                data = VPHumanProfileAttachmentService.GetdataById(new Guid(ID));
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
            else
            {
                data.HumanEmployeeId = EmployeeID;
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="exit"></param>
        /// <returns></returns>
        public ActionResult Update(VPHumanProfileAttachmentBO updateData)
        {
            try
            {
                if (updateData.Id == Guid.Empty)
                {
                    VPHumanProfileAttachmentService.HumanProfileAttachment(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    VPHumanProfileAttachmentService.HumanProfileAttachment(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                X.GetCmp<Window>("WinAttachUpdate").Close();
                X.GetCmp<Store>("StoreProfileAttach").Reload();
                return this.Direct();

            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return RedirectToAction("Index");
            }
        }
        #endregion

    }
}
