﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;
using System;
using System.Security.Claims;
using System.Web.Mvc;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class ProfileMasterDataController : Controller
    {
        protected IProfileMasterService profileMasterService;

        public ProfileMasterDataController()
        {
        }

        public ProfileMasterDataController(IProfileMasterService _profileMasterService)
        {
            profileMasterService = _profileMasterService;
        }

        public ActionResult GetMasterData(string dataType, string superId = "", string query = "")
        {
            return this.Store(profileMasterService.GetMasterDataList(dataType, superId, query));
        }

        public ProfileMasterBO GetMasterDataFromId(string dataType, string id)
        {
            return profileMasterService.GetSingleMasterData(dataType, id);
        }
        public PartialViewResult IndexAll(string containerId, string type)
        {
            ViewData["type"] = type;
            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                ViewData = ViewData,
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult ManageMasterDataForm(string type)
        {
            ViewData["type"] = type;
            return new Ext.Net.MVC.PartialViewResult { ViewName = "MasterDataForm", ViewData = ViewData };
        }
        public ActionResult MasterDataDetail(int actionType, string dataType, string id, string name, string superId)
        {
            ViewData["actionType"] = actionType;
            ViewData["dataType"] = dataType;
            ViewData["SuperId"] = superId;
            if (id != null)
            {
                ViewData["Id"] = id;
                ViewData["Name"] = name;
            }
            return new Ext.Net.MVC.PartialViewResult { ViewName = "MasterDataDetail", ViewData = ViewData };
        }

        public ActionResult MasterDataModify(int actionType, string dataType, string id, string name, int rank = default, string superId = "")
        {
            Guid newid = Guid.NewGuid();
            var claim = (ClaimsIdentity)User.Identity;
            var userId = claim.FindFirst(ClaimTypes.UserData).Value;
            switch (actionType)
            {
                case 1: profileMasterService.Insert(newid.ToString(), name, dataType, userId, rank, superId); break;
                case 2: profileMasterService.Update(id, name, dataType, userId, rank); break;
                case 3: profileMasterService.Delete(id, dataType, userId); break;
                default: break;
            }
            return this.Direct(true);
        }
    }
}
