﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class ProfileCertificateController : FrontController
    {
        public ActionResult Index(string id)
        {
            ViewData["EmployeeId"] = id;
            ViewData["EmployeeName"] = EmployeeService.GetById(Guid.Parse(id)).Name;
            return View();
        }

        public PartialViewResult IndexAll(string containerId)
        {

            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexAll",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult StatisticIndex(string containerId)
        {
            return PartialView();
        }

        public ActionResult LoadProfileCertificate(StoreRequestParameters parameters, Guid EmployeeID)
        {
            int totalCount;
            var result = VPHumanProfileCertificateService.GetAllByEmployeeID(EmployeeID, parameters.Page, parameters.Limit, out totalCount);
            foreach (var data in result)
            {
                var x = data.Type;
                var typeId = data.CertificateType;
                if (typeId != null && typeId != "" && Guid.TryParse(typeId, out Guid gid))
                {
                    data.CertificateType = new ProfileMasterService().GetSingleMasterData("certificateType", typeId).Name;
                }

                var levelId = data.Level;
                if (levelId != null && levelId != "" && Guid.TryParse(typeId, out Guid gid1))
                {
                    data.Level = new ProfileMasterService().GetSingleMasterData("certificateLevel", levelId).Name;
                }
            }
            return this.Store(result, totalCount);
        }

        public ActionResult LoadProfileCertificateAll(StoreRequestParameters param, string name = "", string level = "", string type = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileCertificate_LoadProfileCertificateAll",
               parameter: new
               {
                   name = !string.IsNullOrEmpty(name) ? name : "",
                   DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                   level = !string.IsNullOrEmpty(level) ? (level == "Tất cả" ? "" : level) : "",
                   type = !string.IsNullOrEmpty(type) ? (type == "Tất cả" ? "" : type) : "",
                   pageIndex = param.Page,
                   pageSize = param.Limit
               }
            );
            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            Code = "",
                                            RoleName = "",
                                            EmployeeId = Guid.Empty,
                                            EmployeeName = "",
                                            Name = "",
                                            LevelId = "",
                                            Level = "",
                                            TypeId = "",
                                            Type = "",
                                            PlaceOfTraining = "",
                                            DateIssuance = "",
                                            DateExpiration = "",
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            int count = 0;
            if (dataTable != null && dataTable.Tables[0] != null && dataTable.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(dataTable.Tables[1].Rows[0][0].ToString());
                foreach (DataRow data in dataTable.Tables[0].Rows)
                {
                    var employeeId = data["HumanEmployeeId"] != null && !string.IsNullOrEmpty(data["HumanEmployeeId"].ToString()) ? new Guid(data["HumanEmployeeId"].ToString()) : Guid.Empty;
                    var Id = data["Id"] != null && !string.IsNullOrEmpty(data["Id"].ToString()) ? new Guid(data["Id"].ToString()) : Guid.Empty;
                    var files = new VPHumanProfileAttachmentFileService().GetByFile(Id);
                    var FileAttachs = new FileUploadBO()
                    {
                        Files = files.ToList()
                    };
                    customResult.Add(new
                    {
                        Id = Id,
                        Code = (data["Code"] != null && !string.IsNullOrEmpty(data["Code"].ToString())) ? data["Code"].ToString() : "",
                        RoleName = string.Join("<br>", EmployeeService.GetDeparmentRoleNames(employeeId)),
                        EmployeeId = employeeId,
                        EmployeeName = (data["employeeName"] != null && !string.IsNullOrEmpty(data["employeeName"].ToString())) ? data["employeeName"].ToString() : "",
                        Name = (data["Name"] != null && !string.IsNullOrEmpty(data["Name"].ToString())) ? data["Name"].ToString() : "",
                        LevelId = (data["Level"] != null && !string.IsNullOrEmpty(data["Level"].ToString())) ? data["Level"].ToString() : "",
                        Level = (data["levelName"] != null && !string.IsNullOrEmpty(data["levelName"].ToString())) ? data["levelName"].ToString() : "",
                        TypeId = (data["CertificateType"] != null && !string.IsNullOrEmpty(data["CertificateType"].ToString())) ? data["CertificateType"].ToString() : "",
                        Type = (data["typeName"] != null && !string.IsNullOrEmpty(data["typeName"].ToString())) ? data["typeName"].ToString() : "",
                        PlaceOfTraining = (data["PlaceOfTraining"] != null && !string.IsNullOrEmpty(data["PlaceOfTraining"].ToString())) ? data["PlaceOfTraining"].ToString() : "",
                        DateIssuance = (data["DateIssuance"] != null && !string.IsNullOrEmpty(data["DateIssuance"].ToString())) ? DateTime.Parse(data["DateIssuance"].ToString()).ToString("dd/MM/yyyy") : "",
                        DateExpiration = (data["DateExpiration"] != null && !string.IsNullOrEmpty(data["DateExpiration"].ToString())) ? DateTime.Parse(data["DateExpiration"].ToString()).ToString("dd/MM/yyyy") : "",
                        FileAttachs = FileAttachs
                    });
                }
            }
            customResult.RemoveAt(0);
            //Sắp xếp theo tên nhân viên, nếu trùng thì xếp theo Id
            //customResult = customResult.OrderBy(o => o.EmployeeName).ThenBy(o => o.EmployeeId).ToList();
            return this.Store(customResult, count);
        }
        public void exportexcel(string name = "", string level = "", string type = "", string DeparmentId = "")
        {
            //Query dữ liệu thô
            if (DeparmentId == "null")
                DeparmentId = "";
            DataSet dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_VPHumanProfileCertificate_LoadProfileCertificateAll",
               parameter: new
               {
                   name = !string.IsNullOrEmpty(name) ? name : "",
                   DeparmentId = !string.IsNullOrEmpty(DeparmentId) ? DeparmentId : "",
                   level = !string.IsNullOrEmpty(level) ? (level == "Tất cả" || level == "null" ? "" : level) : "",
                   type = !string.IsNullOrEmpty(type) ? (type == "Tất cả" || type == "null" ? "" : type) : "",
                   pageIndex = -1,
                   pageSize = -1
               }
            );
            foreach (DataRow row in dataTable.Tables[0].Rows)
            {
                row["RoleName"] = string.Join(", ", EmployeeService.GetDeparmentRoleNames(new Guid(row["HumanEmployeeId"].ToString())));
            }
            DataTable tablecopy = dataTable.Tables[0].Copy();
            foreach (DataColumn col in tablecopy.Columns)
            {
                if (col.ColumnName != "employeeName"
                    && col.ColumnName != "Name"
                    && col.ColumnName != "Code"
                    && col.ColumnName != "RoleName"
                    && col.ColumnName != "levelName"
                    && col.ColumnName != "typeName"
                    && col.ColumnName != "PlaceOfTraining"
                    && col.ColumnName != "DateIssuance")
                {
                    dataTable.Tables[0].Columns.Remove(col.ColumnName);
                }
                else
                {
                    if (col.DataType.Name == "DateTime")
                    {
                        dataTable.Tables[0].Columns.Add(col.ColumnName + "_Extra", typeof(String));
                        foreach (DataRow data in dataTable.Tables[0].Rows)
                        {
                            data[col.ColumnName + "_Extra"] = (data[col.ColumnName] != null && !string.IsNullOrEmpty(data[col.ColumnName].ToString())) ? DateTime.Parse(data[col.ColumnName].ToString()).ToString("dd/MM/yyyy") : "";
                        }
                        dataTable.Tables[0].Columns.Remove(col.ColumnName);
                        dataTable.Tables[0].Columns[col.ColumnName + "_Extra"].ColumnName = col.ColumnName;
                    }
                }
            }
            dataTable.Tables[0].Columns["Code"].SetOrdinal(0);
            dataTable.Tables[0].Columns["employeeName"].SetOrdinal(1);
            dataTable.Tables[0].Columns["RoleName"].SetOrdinal(2);
            dataTable.Tables[0].Columns["Name"].SetOrdinal(3);
            dataTable.Tables[0].Columns["levelName"].SetOrdinal(4);
            dataTable.Tables[0].Columns["typeName"].SetOrdinal(5);
            dataTable.Tables[0].Columns["PlaceOfTraining"].SetOrdinal(6);
            dataTable.Tables[0].Columns["DateIssuance"].SetOrdinal(7);

            dataTable.Tables[0].Columns["Code"].ColumnName = "Mã nhân viên";
            dataTable.Tables[0].Columns["employeeName"].ColumnName = "Tên nhân viên";
            dataTable.Tables[0].Columns["RoleName"].ColumnName = "Chức vụ";
            dataTable.Tables[0].Columns["Name"].ColumnName = "Tên chứng chỉ";
            dataTable.Tables[0].Columns["levelName"].ColumnName = "Xếp loại";
            dataTable.Tables[0].Columns["typeName"].ColumnName = "Loại chứng chỉ";
            dataTable.Tables[0].Columns["PlaceOfTraining"].ColumnName = "Nơi cấp";
            dataTable.Tables[0].Columns["DateIssuance"].ColumnName = "Ngày cấp";
            Common.Utilities.ExportDataTableToExcel(dataTable.Tables[0], "Danh sách hồ sơ chứng chỉ");
        }

        public ActionResult LoadProfileCertificateNotPaging(Guid EmployeeID)
        {
            var result = VPHumanProfileCertificateService.GetAllByEmployeeIDNotPaging(EmployeeID);
            var profileMasterService = new ProfileMasterService();

            //Tạo anonymous class để chuyển EmployeeID (Guid) thành EmployeeName (string)
            var customResult = (new[] { new {
                                            Id = Guid.Empty,
                                            EmployeeId = Guid.Empty,
                                            Name = "",
                                            Level = "",
                                            Type = "",
                                            PlaceOfTraining = "",
                                            DateIssuance = (DateTime?)DateTime.Now,
                                            DateExpiration = (DateTime?)DateTime.Now,
                                            FileAttachs = new FileUploadBO()
                                            }}).ToList();
            //Chuyển các trường guid sang string
            foreach (var data in result)
            {
                var levelId = data.Level;
                var levelName = "";
                if (levelId != null && levelId != "" && Guid.TryParse(levelId, out Guid id))
                {
                    levelName = profileMasterService.GetSingleMasterData("certificateLevel", levelId).Name;
                }

                var typeId = data.CertificateType;
                var typeName = "";
                if (typeId != null && typeId != "" && Guid.TryParse(levelId, out Guid id1))
                {
                    typeName = profileMasterService.GetSingleMasterData("certificateType", typeId).Name;
                }
                customResult.Add(new { Id = data.Id, EmployeeId = EmployeeID, Name = data.Name, Level = levelName, Type = typeName, PlaceOfTraining = data.PlaceOfTraining, DateIssuance = data.DateIssuance, DateExpiration = data.DateExpiration, FileAttachs = data.FileAttachs });
            }
            customResult.RemoveAt(0);
            return this.Store(customResult);
        }

        public ActionResult DeleteProfileCertificate(Guid id)
        {
            try
            {
                VPHumanProfileCertificateService.Delete(id);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            finally
            {
                X.GetCmp<Store>("StoreProfileCertificate").Reload();
            }
            return this.Direct();
        }

        public ActionResult UpdateForm(string ID, Guid EmployeeID)
        {
            var data = new VPHumanProfileCertificateBO();
            if (!string.IsNullOrEmpty(ID))
            {
                data = VPHumanProfileCertificateService.GetdataById(new Guid(ID));
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
            else
            {
                data.HumanEmployeeId = EmployeeID;
                return new Ext.Net.MVC.PartialViewResult { ViewName = "Update", Model = data };
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="updateData"></param>
        /// <param name="exit"></param>
        /// <returns></returns>
        public ActionResult Update(VPHumanProfileCertificateBO updateData)
        {
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            // Kiểm tra quyền edit
            if (updateData.HumanEmployeeId != null && s.CheckTitleRole_Edit((Guid)updateData.HumanEmployeeId) == false)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }
            try
            {
                if (updateData.Id == Guid.Empty)
                {
                    VPHumanProfileCertificateService.HumanProfileCertificate(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    VPHumanProfileCertificateService.HumanProfileCertificate(updateData);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                X.GetCmp<Window>("WinCertificate").Close();
                X.GetCmp<Store>("StoreProfileCertificate").Reload();
                return this.Direct();
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return RedirectToAction("Index");
            }
        }

        public ActionResult DetailForm(string ID, Guid EmployeeID)
        {
            var data = VPHumanProfileCertificateService.GetById(new Guid(ID));
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Detail", Model = data };
        }

    }
}