﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class DashboardEmployeesController : FrontController
    {
        // GET: Organization/DashboardEmployees
        public ActionResult Dashboard()
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = "pnMainSystem",
                WrapByScriptTag = false
            };
        }
        public ActionResult ChartGrowthEmployee()
        {
            return PartialView();
        }
        public ActionResult ChartAreaSex()
        {
            return PartialView();
        }
        public ActionResult ChartRangerAgeEmployee()
        {
            return PartialView();
        }
        public ActionResult ChartDeparmentEmployee()
        {
            return PartialView();
        }
        public ActionResult ChartPieDeparmentEmployee()
        {
            return PartialView();
        }
        public ActionResult ChartTitleEmployee()
        {
            return PartialView();
        }
        public ActionResult ChartLevelEmployee()
        {
            return PartialView();
        }
        public ActionResult EmployeeListsFromChart(string type = "", string param1 = "", string param2 = "", string param3 = "")
        {
            ViewData["type"] = type;
            ViewData["param1"] = param1;
            ViewData["param2"] = param2;
            ViewData["param3"] = param3;
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        public ActionResult LoadDataDashboardEmployees(string type = "", string param1 = "", string param2 = "", string param3 = "", string query = "", int status = 0)
        {
            if(Session["DataExcel"] != null)
                Session.Remove("DataExcel");
            DataTable dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employee_LoadDataDashboardEmployees",
                parameter: new
                {
                    type = type,
                    param1 = (!string.IsNullOrEmpty(param1) && param1 != Guid.Empty.ToString()) ? param1 : "",
                    param2 = (!string.IsNullOrEmpty(param2) && param2 != Guid.Empty.ToString()) ? param2 : "",
                    param3 = (!string.IsNullOrEmpty(param3) && param3 != Guid.Empty.ToString()) ? param3 : "",
                    query = query
                }
            );
            List<EmployeeBO> listEmployee = new List<EmployeeBO>();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                foreach (DataRow item in dataTable.Rows)
                {
                    EmployeeBO boitem = new EmployeeBO();
                    boitem.Id = new Guid(item["Id"].ToString());
                    boitem.Name = (item["Name"] != null && !string.IsNullOrEmpty(item["Name"].ToString())) ? item["Name"].ToString() : "";
                    boitem.PhoneNumber = (item["PhoneNumber"] != null && !string.IsNullOrEmpty(item["PhoneNumber"].ToString())) ? item["PhoneNumber"].ToString() : "";
                    boitem.Email = (item["Email"] != null && !string.IsNullOrEmpty(item["Email"].ToString())) ? item["Email"].ToString() : "";
                    boitem.Status = (item["Status"] != null && !string.IsNullOrEmpty(item["Status"].ToString())) ? item["Status"].ToString() : "";
                    listEmployee.Add(boitem);
                }
            }
            listEmployee = listEmployee.GroupBy(x => x.Id).Select(y => y.First()).ToList();
            Session["DataExcel"] = listEmployee;
            return this.Store(listEmployee);
        }
        private string GetDataExportExcel(List<EmployeeBO> listEmployee)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder(@"<?xml version=""1.0""?>
                <?mso-application progid=""Excel.Sheet""?>
                <Workbook xmlns=""urn:schemas-microsoft-com:office:spreadsheet""
                 xmlns:o=""urn:schemas-microsoft-com:office:office""
                 xmlns:x=""urn:schemas-microsoft-com:office:excel""
                 xmlns:ss=""urn:schemas-microsoft-com:office:spreadsheet""
                 xmlns:html=""http://www.w3.org/TR/REC-html40"">
                 <DocumentProperties xmlns=""urn:schemas-microsoft-com:office:office"">
                  <Author>vncert</Author>
                  <LastAuthor>vncert</LastAuthor>
                  <Created>2019-10-25T04:14:19Z</Created>
                  <LastSaved>2019-10-25T06:45:54Z</LastSaved>
                  <Version>16.00</Version>
                 </DocumentProperties>
                 <OfficeDocumentSettings xmlns=""urn:schemas-microsoft-com:office:office"">
                  <AllowPNG/>
                 </OfficeDocumentSettings>
                 <ExcelWorkbook xmlns=""urn:schemas-microsoft-com:office:excel"">
                  <WindowHeight>7320</WindowHeight>
                  <WindowWidth>20490</WindowWidth>
                  <WindowTopX>0</WindowTopX>
                  <WindowTopY>0</WindowTopY>
                  <ProtectStructure>False</ProtectStructure>
                  <ProtectWindows>False</ProtectWindows>
                 </ExcelWorkbook>
                 <Styles>
                  <Style ss:ID=""Default"" ss:Name=""Normal"">
                   <Alignment ss:Vertical=""Bottom""/>
                   <Borders/>
                   <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""11"" ss:Color=""#000000""/>
                   <Interior/>
                   <NumberFormat/>
                   <Protection/>
                  </Style>
                  <Style ss:ID=""s62"">
                   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Bottom""/>
                   <Font ss:FontName=""Times New Roman"" x:Family=""Roman"" ss:Size=""11"" ss:Color=""#000000"" ss:Bold=""1""/>
                  </Style>
                  <Style ss:ID=""s64"">
                   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Bottom""/>
                   <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""12"" ss:Color=""#000000"" ss:Bold=""1""/>
                  </Style>
                  <Style ss:ID=""s65"">
                   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center""/>
                   <Font ss:FontName=""Times New Roman"" x:Family=""Roman"" ss:Size=""11"" ss:Color=""#000000"" ss:Bold=""1""/>
                  </Style>
                  <Style ss:ID=""s66"">
                   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center""/>
                  </Style>
                  <Style ss:ID=""s67"">
                   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center""/>
                   <Font ss:FontName=""Times New Roman"" x:Family=""Roman"" ss:Size=""11"" ss:Color=""#000000""/>
                  </Style>
                  <Style ss:ID=""s68"">
                   <Font ss:FontName=""Times New Roman"" x:Family=""Roman"" ss:Size=""11"" ss:Color=""#000000""/>
                  </Style>
                  <Style ss:ID=""s69"">
                   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Bottom""/>
                   <Font ss:FontName=""Times New Roman"" x:Family=""Roman"" ss:Size=""12"" ss:Color=""#000000"" ss:Bold=""1""/>
                  </Style>
                  <Style ss:ID=""s70"">
                   <Alignment ss:Vertical=""Bottom"" ss:WrapText=""1""/>
                   <Font ss:FontName=""Times New Roman"" x:Family=""Roman"" ss:Size=""11"" ss:Color=""#000000""/>
                  </Style >
                 </Styles>
                 <Worksheet ss:Name=""Sheet1"">
                  <Table ss:ExpandedColumnCount=""5"" ss:ExpandedRowCount=""" + (listEmployee.Count + 2).ToString() + @""" x:FullColumns=""1"" x:FullRows=""1"" ss:DefaultRowHeight=""15"">");
                stringBuilder.Append(@"<Row ss:Height=""14.25"" ss:StyleID=""s62"">");
                stringBuilder.Append(@"<Cell ss:StyleID=""s65""><Data ss:Type=""String"">STT</Data></Cell>");
                //Tiêu đề cột
                stringBuilder.Append(@"<Cell><Data ss:Type=""String"">Tên nhân sự</Data></Cell>");
                stringBuilder.Append(@"<Cell><Data ss:Type=""String"">Địa chỉ Email</Data></Cell>");
                stringBuilder.Append(@"<Cell><Data ss:Type=""String"">Số điện thoại</Data></Cell>");
                stringBuilder.Append(@"<Cell><Data ss:Type=""String"">Trạng thái tài khoản</Data></Cell>");
                stringBuilder.Append(@"</Row>");
                int count = 0;
                // danh sách row
                foreach (var emp in listEmployee)
                {
                    count += 1;
                    stringBuilder.Append(@"<Row ss:AutoFitHeight=""0"" ss:StyleID=""s68"">");
                    stringBuilder.Append(@"<Cell ss:StyleID=""s67""><Data ss:Type=""Number"">" + count.ToString() + @"</Data></Cell>");
                    stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + emp.Name + @"</Data></Cell>");
                    stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + emp.Email + @"</Data></Cell>");
                    stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + emp.PhoneNumber + @"</Data></Cell>");
                    stringBuilder.Append(@"<Cell><Data ss:Type=""String"">" + emp.Status + @"</Data></Cell>");
                    stringBuilder.Append(@"</Row>");
                }
                stringBuilder.Append(@"</Table>
                  <WorksheetOptions xmlns=""urn:schemas-microsoft-com:office:excel"">
                   <PageSetup>
                    <Header x:Margin=""0.3""/>
                    <Footer x:Margin=""0.3""/>
                    <PageMargins x:Bottom=""0.75"" x:Left=""0.7"" x:Right=""0.7"" x:Top=""0.75""/>
                   </PageSetup>
                   <Selected/>
                   <Panes>
                    <Pane>
                     <Number>3</Number>
                     <ActiveRow>8</ActiveRow>
                     <ActiveCol>6</ActiveCol>
                    </Pane>
                   </Panes>
                   <ProtectObjects>False</ProtectObjects>
                   <ProtectScenarios>False</ProtectScenarios>
                  </WorksheetOptions>
                 </Worksheet>
                </Workbook>
                ");
                TempData["DownloadExcel"] = stringBuilder.ToString();
                return stringBuilder.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DownloadExcel()
        {
            List<EmployeeBO> listEmployee = (List<EmployeeBO>)Session["DataExcel"];
            GetDataExportExcel(listEmployee);
            var data = TempData["DownloadExcel"];
            TempData.Keep("DownloadExcel");
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=DanhSachNhanSu.xls");
            Response.Write(data);
            Response.End();
        }
        public StoreResult GetDataRangerAgeEmployees()
        {
            List<PieChartBO> listBO = new List<PieChartBO>();
            var summaryData = new List<PieChartBO>();
            DataSet dataset = Sql_ADO.idasAdoService.ExecuteNoqueryDataSet("sp_Employees_GetCountByRangerAge");
            if (dataset != null && dataset.Tables.Count > 0)
            {
                summaryData.Add(new PieChartBO()
                {
                    Name = "20 - 24",
                    Value = Int32.Parse(dataset.Tables[0].Rows[0][0].ToString())
                });
                summaryData.Add(new PieChartBO()
                {
                    Name = "25 - 30",
                    Value = Int32.Parse(dataset.Tables[1].Rows[0][0].ToString())
                });
                summaryData.Add(new PieChartBO()
                {
                    Name = "31 - 40",
                    Value = Int32.Parse(dataset.Tables[2].Rows[0][0].ToString())
                });
                summaryData.Add(new PieChartBO()
                {
                    Name = "Hơn 40",
                    Value = Int32.Parse(dataset.Tables[3].Rows[0][0].ToString())
                });
            }
            return new StoreResult(summaryData);
        }
        public StoreResult GetDataGrowthEmployee()
        {
            List<PieChartBO> listBO = new List<PieChartBO>();
            var summaryData = new List<PieChartBO>();
            for (int i = 0; i < 12; i++)
            {
                DataTable data = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employees_GetDataGrowthEmployee", parameter: new
                {
                    Date = DateTime.Now.AddMonths(0 - i),
                });
                if (data != null && data.Rows.Count > 0)
                {
                    summaryData.Add(new PieChartBO()
                    {
                        Rate = i,
                        Name = data.Rows[0]["Name"].ToString(),
                        Value = Int32.Parse(data.Rows[0]["Value"].ToString()),
                        ValueOther = Int32.Parse(data.Rows[0]["ValueOther"].ToString())
                    });
                }
            }
            return new StoreResult(summaryData.OrderByDescending(x => x.Rate));
        }
        public StoreResult GetDataTitleEmployee()
        {
            List<PieChartBO> listBO = new List<PieChartBO>();
            var summaryData = new List<PieChartBO>();

            DataTable data = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employees_GetDataTitleEmployee");
            if (data != null && data.Rows.Count > 0)
            {
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    summaryData.Add(new PieChartBO()
                    {
                        Name = data.Rows[i]["Name"].ToString(),
                        Value = Int32.Parse(data.Rows[i]["Total"].ToString())
                    });
                }
            }
            return new StoreResult(summaryData.OrderByDescending(x => x.Rate));
        }
        public StoreResult GetDataLevelEmployee()
        {
            List<PieChartBO> listBO = new List<PieChartBO>();
            var summaryData = new List<PieChartBO>();

            //DataTable data = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employees_GetDataLevelEmployee");
            //if (data != null && data.Rows.Count > 0)
            //{
            //    for (int i = 0; i < data.Rows.Count; i++)
            //    {
            //        summaryData.Add(new PieChartBO()
            //        {
            //            Name = data.Rows[i]["Name"].ToString(),
            //            Value = Int32.Parse(data.Rows[i]["Value"].ToString())
            //        });
            //    }
            //}
            for (int i = 1; i < 13; i++)
            {
                summaryData.Add(new PieChartBO()
                {
                    Name = "T" + i,
                    Value = VPHumanProfileCuriculmViateService.Get(x => x.DateOfBirth.HasValue && x.DateOfBirth.Value.Month == i).Count()
                });
            }
            return new StoreResult(summaryData.OrderByDescending(x => x.Rate));
        }
        public StoreResult GetDataDeparmentEmployees()
        {
            var summaryData = new List<PieChartBO>();
            DataTable data = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employees_GetDataDeparmentEmployees");
            if (data != null && data.Rows.Count > 0)
            {
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    summaryData.Add(new PieChartBO()
                    {
                        DeparmentId = new Guid(data.Rows[i]["Id"].ToString()),
                        Name = data.Rows[i]["Name"].ToString(),
                        Value = Int32.Parse(data.Rows[i]["Value"].ToString()),
                        ValueOther = Int32.Parse(data.Rows[i]["ValueOther"].ToString()),
                        ValueThree = Int32.Parse(data.Rows[i]["ValueThree"].ToString()),
                    });
                }
            }
            return new StoreResult(summaryData.OrderByDescending(x => x.Name));
        } 
        public StoreResult GetDataDeparmentEmployeesPie()
        {
            List<PieChartBO> listBO = new List<PieChartBO>();
            var summaryData = new List<PieChartBO>();
            DataTable data = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employees_GetDataDeparmentEmployees");
            if (data != null && data.Rows.Count > 0)
            {
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    listBO.Add(new PieChartBO()
                    {
                        Name = data.Rows[i]["Name"].ToString(),
                        Value = Int32.Parse(data.Rows[i]["Value"].ToString()),
                        ValueOther = Int32.Parse(data.Rows[i]["ValueOther"].ToString()),
                        ValueThree = Int32.Parse(data.Rows[i]["ValueThree"].ToString()),
                    });
                }
                summaryData.Add(new PieChartBO()
                {
                    Name = "Thực tập sinh",
                    Value = listBO.Sum(x => x.Value),
                    ValueOther = 1
                });
                summaryData.Add(new PieChartBO()
                {
                    Name = "Thử việc",
                    Value = listBO.Sum(x => x.ValueOther),
                    ValueOther = 2
                });
                summaryData.Add(new PieChartBO()
                {
                    Name = "Chính thức",
                    Value = listBO.Sum(x => x.ValueThree),
                    ValueOther = 3
                });
            }
            return new StoreResult(summaryData);
        }
    }
}