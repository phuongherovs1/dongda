﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class DepartmentController : FrontController
    {
        //
        // GET: /Organization/Department/
        #region View
        public ActionResult Index()
        {
            ViewData["CheckRoot"] = DepartmentService.CheckHasRoot();
            return PartialView();
        }
        public ActionResult Form(string id, string parentId)
        {
            if(!string.IsNullOrEmpty(id) && id != Guid.Empty.ToString())
            {
                UserRoleService roleService = new UserRoleService();
                var isAdmin = roleService.RoleExits("Administrator");
                DepartmentTitleRoleService s = new DepartmentTitleRoleService();
                var titleRole = s.CheckCreateForTitles(new Guid(id));
                if (!isAdmin && !titleRole)
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = "Bạn không có quyền thực hiện chức năng này"
                    });
                    return this.Direct();
                }
            }
            else if (!string.IsNullOrEmpty(parentId) && parentId != Guid.Empty.ToString())
            {
                UserRoleService roleService = new UserRoleService();
                var isAdmin = roleService.RoleExits("Administrator");
                DepartmentTitleRoleService s = new DepartmentTitleRoleService();
                var titleRole = s.CheckCreateForTitles(new Guid(parentId));
                if (!isAdmin && !titleRole)
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = "Bạn không có quyền thực hiện chức năng này"
                    });
                    return this.Direct();
                }
            }
            try
            {
                var model = DepartmentService.GetFormItem(id, parentId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception)
            {
                return this.Direct();
            }

        }

        public ActionResult Test()
        {
            try
            {
                var model = DepartmentService.GetAll();
                return this.Store(model);
            }
            catch (Exception)
            {
                return this.Direct();
            }

        }
        #endregion
        #region Method
        public ActionResult GetDepartment()
        {
            var listDepartment = DepartmentService.GetAll().ToList().OrderBy(x => x.Name);
            return this.Store(listDepartment);
        }

        public ActionResult GetDepartmentDetailsByID(string id)
        {
            var isValid = Guid.TryParse(id, out Guid gid);
            if (isValid)
            {
                var department = DepartmentService.GetById(gid);
                var model = new[] { department.Functions, department.Task, department.Note };
                return this.Direct(model);
            }
            return this.Direct("false");
        }
        public ActionResult GetDepartmentEmployeeWithAllRole(string id)
        {
            var isValid = Guid.TryParse(id, out Guid gid);
            if (isValid)
            {
                List<Guid> lstDeparmentId = new List<Guid>();
                lstDeparmentId = GetDeparmentIdByParent(id, lstDeparmentId);
                List<EmployeeBO> employees = new List<EmployeeBO>();
                foreach(Guid item in lstDeparmentId)
                {
                    employees.AddRange(EmployeeService.GetByDepartment(item).ToList());
                }
                employees = employees.GroupBy(x => x.Id).Select(y => y.First()).ToList();
                foreach (var e in employees)
                {
                    var roleList = new List<string>();
                    var departments = DepartmentService.GetDepartmentsByEmployeeID(e.Id).ToList();
                    foreach (var d in departments)
                    {
                        var roleNames = EmployeeTitleService.GetRoleNamesByEmployeeIDAndDepartmentID(e.Id, d.Id);
                        roleList.Add(d.Name + " - " + roleNames);
                    }
                    e.RoleNames = string.Join("<br/>", roleList.Where(rl => !string.IsNullOrEmpty(rl)));
                }

                return this.Store(employees);
            }
            return this.Store(null);
        }
        public List<Guid> GetDeparmentIdByParent(string id, List<Guid> res)
        {
            if (res != null && res.Count == 0)
                res.Add(new Guid(id));
            DataTable dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Deparment_GetDeparmentIdByParent",
                    parameter: new
                    {
                        id = new Guid(id)
                    }
                );
            if(dataTable != null && dataTable.Rows.Count > 0)
            {
                foreach(DataRow item in dataTable.Rows)
                {
                    res.Add(new Guid(item["id"].ToString()));
                }
                foreach (DataRow item in dataTable.Rows)
                {
                    res = GetDeparmentIdByParent(item["id"].ToString(), res);
                }
            }
            return res;
        }
        public ActionResult GetCurrentDepartment()
        {
            var data = DepartmentService.GetDepartmentsByCurrentUser();
            return this.Store(data);
        }
        private Node createNodeDepartment(DepartmentBO department)
        {
            Node node = new Node();
            node.NodeID = department.Id.ToString();
            node.Text = " " + department.Name;
            node.IconCls = "x-fa fa-home";
            node.CustomAttributes.Add(new ConfigItem { Name = "ParentId", Value = department.ParentId.ToString(), Mode = ParameterMode.Value });
            node.Leaf = !department.IsParent;
            return node;
        }
        public ActionResult LoadData(string node)
        {
            try
            {
                NodeCollection nodes = new NodeCollection();
                var departmentID = node == "root" ? (Guid?)null : new Guid(Convert.ToString(node));
                var departments = DepartmentService.GetTreeDepartment(departmentID);
                departments.OrderByDescending(x => x.Name);
                foreach (var department in departments)
                {
                    nodes.Add(createNodeDepartment(department));
                }
                return this.Content(nodes.ToJson());
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }
        [ValidateInput(false)]
        public ActionResult Create(DepartmentBO item)
        {
            DepartmentService.Insert(item);
            return this.Direct(result: true);
        }
        [ValidateInput(false)]
        public ActionResult Update(DepartmentBO item)
        {
            DepartmentService.Update(item);
            return this.Direct(result: true);
        }

        public ActionResult Delete(Guid id)
        {
            if (!EmployeeService.GetByDepartmentEmployee(id))
            {
                List<Guid> lstDepartment = new List<Guid>();
                lstDepartment = GetDepartmentsListParent(id, lstDepartment);
                DepartmentService.DeleteRange(lstDepartment);
                return this.Direct(result: true);
            }
            else
            {
                return this.Direct(result: false);
            }
        }
     
        public ActionResult GetOrganizationalcharts(string id)
        {
            try
            {
                var isValid = Guid.TryParse(id, out Guid gid);
                if (isValid)
                {
                    List<Guid> lstDepartment = new List<Guid>();
                    lstDepartment = GetDepartmentsListParent(new Guid(id), lstDepartment);
                    var department = DepartmentService.GetByIds(lstDepartment);
                    var model = department.OrderBy(x => x.Name);
                    return this.Direct(model);
                }
            }
            catch (Exception e)
            {
                return this.Direct("false");
            }
            return this.Direct("false");
        }
        public List<Guid> GetDepartmentsListParent(Guid ID, List<Guid> LstOut)
        {
            if (LstOut == null || LstOut.Count == 0)
                LstOut = new List<Guid>();
            if (LstOut.IndexOf(ID) == -1)
                LstOut.Add(ID);
            List<DepartmentBO> department = DepartmentService.Get(x => x.ParentId.HasValue && x.ParentId.Value.ToString() == ID.ToString()).ToList();
            if (department.Count > 0)
            {
                foreach (DepartmentBO item in department)
                    GetDepartmentsListParent(item.Id, LstOut);
            }
            return LstOut;
        }
        #endregion
    }
}