﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    //[Authorize(Roles = Common.Role.Administrator)]
    public class OrganizationTreeController : FrontController
    {
        public ActionResult GetData(string node)
        {
            var data = OrganizationTreeService.GetByNode(node);
            return this.Store(data);
        }
        private Node createTreeNode(OrganizationTreeBO nodeItem, bool disableSelectDepartment = false, bool disableSelectTitle = false, bool disableSelectEmployee = false)
        {
            var node = new Node();
            node.NodeID = nodeItem.Id.ToString();
            node.Text = " " + nodeItem.Name;

            switch (nodeItem.Type)
            {
                case iDAS.Service.Common.Resource.OrganizationTreeType.Department:
                    node.IconCls = "x-fa fa-home";
                    if (!disableSelectDepartment)
                        node.Checked = false;
                    break;
                case iDAS.Service.Common.Resource.OrganizationTreeType.Title:
                    node.IconCls = "x-fa fa-suitcase";
                    if (!disableSelectTitle)
                        node.Checked = false;
                    break;
                case iDAS.Service.Common.Resource.OrganizationTreeType.Employee:
                    node.IconFile = nodeItem.AvatarUrl;
                    node.IconCls = "image_avatar";
                    if (!disableSelectEmployee)
                        node.Checked = false;
                    break;
            }
            node.Leaf = nodeItem.IsLeaf;
            return node;
        }
        public StoreResult LoadTreeData(string node, bool showTitle = false, bool showEmployee = false, string onlyTitle = default(string), bool disableSelectDepartment = false, bool disableSelectTitle = false, bool disableSelectEmployee = false)
        {
            var nodes = new NodeCollection();
            var departments = DepartmentService.GetByNode(node, showTitle, showEmployee, onlyTitle);
            foreach (var department in departments)
            {
                nodes.Add(createTreeNode(department, disableSelectDepartment, disableSelectTitle, disableSelectEmployee));
            }
            return this.Store(nodes);
        }

    }
}