﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Organization.Controllers
{
    public class JobTitleController : FrontController
    {
        //
        // GET: /Organization/Title/
        #region View
        public ActionResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }

        public ActionResult Form(string operation, string type, string titleId = default, string rankId = default)
        {
            if (type == "title")
            {
                ViewData["Title"] = "vị trí việc làm";
                if (titleId != default)
                {
                    ViewData["Name"] = JobTitleService.GetById(Guid.Parse(titleId)).Name;
                }
            }
            else
            {
                ViewData["Title"] = "ngạch / hạng chức danh";
                if (rankId != default)
                {
                    ViewData["Name"] = new ProfileMasterService().GetSingleMasterData("jobTitle", rankId).Name;
                }
            }

            ViewData["Type"] = type;
            ViewData["operation"] = operation;
            if (operation == "Update") ViewData["Id"] = type == "title" ? titleId : rankId;
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Form", ViewData = ViewData };
        }

        #endregion
        #region Method
        public ActionResult LoadData(StoreRequestParameters parameters)
        {
            var ms = new ProfileMasterService();
            var titles = JobTitleService.GetAllNotPaging();
            var masterTitles = ms.ToEnumerable("jobTitle");
            var result = titles.Join(masterTitles, title => title.Rank, master => master.Id, (first, second) => new { first.Id, first.Name, RankId = second.Id, Rank = second.Name + " hoặc tương đương", Level = toRoman((int)second.Rank) });
            return this.Store(result);
        }
        public ActionResult LoadComboData(string type = "")
        {
            if (type == "title") return GetTitles();
            else if (type == "rank") return GetRanks();
            else return this.Store(new List<JobTitleBO>());
        }

        string toRoman(int i)
        {
            var s = "";
            switch (i)
            {
                case 1: s = "I"; break;
                case 2: s = "II"; break;
                case 3: s = "III"; break;
                case 4: s = "IV"; break;
                case 5: s = "V"; break;
                default: break;
            }
            return "Hạng " + s;
        }

        public ActionResult GetTitles()
        {
            var data = JobTitleService.GetAllNotPaging();
            return this.Store(data);
        }
        public ActionResult GetRanks()
        {
            var ms = new ProfileMasterService();
            var data = ms.ToEnumerable("jobTitle");
            return this.Store(data);
        }

        [ValidateInput(false)]
        public ActionResult Create(string type, string name, string rank)
        {
            var id = Guid.NewGuid();
            var rankId = Guid.Empty;
            Guid.TryParse(rank, out rankId);
            if (type == "title") JobTitleService.Insert(new JobTitleBO() { Id = id, Name = name, Rank = rankId });
            else
            {
                var ms = new ProfileMasterService();
                ms.Insert(id.ToString(), name, "jobTitle", UserService.GetCurrentUser().Id.ToString(), int.Parse(rank));
            }
            return this.Direct(result: true);
        }
        [ValidateInput(false)]
        public ActionResult Update(string type, string name, string rank, string id = default)
        {
            if (type == "title")
            {
                var jobTitle = JobTitleService.GetById(Guid.Parse(id));
                jobTitle.Name = name;
                jobTitle.Rank = Guid.Parse(rank);
                JobTitleService.Update(jobTitle);
            }
            else
            {
                var ms = new ProfileMasterService();
                var mstJobTitle = ms.GetSingleMasterData("jobTitle", id);
                ms.Update(id, name, "jobTitle", UserService.GetCurrentUser().Id.ToString(), int.Parse(rank));
            }
            return this.Direct(result: true);
        }

        public ActionResult Delete(string type, string id)
        {
            if (type == "title")
            {
                JobTitleService.Delete(Guid.Parse(id));
            }
            else
            {
                var jobs = JobTitleService.GetAllNotPaging().Where(t => t.Rank == Guid.Parse(id)).ToList();
                if (jobs.Count > 0) {
                    X.Msg.Notify("Cảnh báo", "Không thể xóa hạng ngạch đang được chọn bởi ít nhất 1 công việc!").Show();
                    return this.Direct(result: true);
                }
                var ms = new ProfileMasterService();
                ms.Delete(id, "jobTitle", UserService.GetCurrentUser().Id.ToString());
            }
            return this.Direct(result: true);
        }
        public ActionResult UpdateOrder(bool up, Guid id)
        {
            try
            {
                DepartmentTitleService.UpdateOrder(up, id);
                return this.Direct(result: true);
            }
            catch
            {
                return this.Direct(result: false);
            }
        }

        /// <summary>
        /// Get  department title
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAll()
        {
            var data = DepartmentTitleService.GetAll();
            return this.Store(data);
        }
        #endregion

    }
}