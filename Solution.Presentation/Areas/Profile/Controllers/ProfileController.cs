﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class ProfileController : FrontController
    {
        // Danh sách hồ sơ lấy theo danh mục hồ sơ
        public ActionResult GetDataProfile(StoreRequestParameters param, string filterName = default(string), string categoryId = default(string), iDAS.Service.Common.Resource.ProfileStatus status = iDAS.Service.Common.Resource.ProfileStatus.All)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            object data = null;
            if (!string.IsNullOrEmpty(categoryId))
                data = ProfileService.GetByCategory(pageIndex, pageSize, out count, new Guid(categoryId), filterName, status).ToList();
            return this.Store(data, count);
        }
        #region Thêm, sửa, xóa danh mục hồ sơ
        public ActionResult FormAddProfileFromHuman(string departmentId = default(string), string curiculmviateId = default(string))
        {
            try
            {
                ViewData["DepartmentId"] = departmentId;
                if (departmentId.Contains('_'))
                {
                    string[] DepartmentIdsl = departmentId.Trim().Split('_');
                    departmentId = DepartmentIdsl[1];
                }
                var model = ProfileService.CreateDefaultForHuman(new Guid(departmentId), curiculmviateId);
                ViewData.Add("Operation", Common.Operation.Create);
                ViewData["Status"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetProfileStatus());
                ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
                ViewData["FormalitySave"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.NotExistProfileCuriculmViate)
                    this.ShowAlert(ex.Message);
                return this.Direct();
            }
        }

        public ActionResult Form(string Id = default(string), bool hideButtonSave = false, string CategoryId = default(string), string departmentId = default(string))
        {
            ViewData["HideButtonSave"] = hideButtonSave;
            if (Id == null)
            {
                if (departmentId.Contains('_'))
                {
                    string[] DepartmentIdsl = departmentId.Trim().Split('_');
                    departmentId = DepartmentIdsl[1];
                }
                ViewData["HideButtonSaveProfile"] = false;
                var model = ProfileService.CreateDefault(new Guid(CategoryId), new Guid(departmentId));
                ViewData.Add("Operation", Common.Operation.Create);
                ViewData["Status"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetProfileStatus());
                ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
                ViewData["FormalitySave"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                var model = ProfileService.GetDetailDataProfile(new Guid(Id));
                ViewData.Add("Operation", Common.Operation.Update);
                ViewData["HideButtonSaveProfile"] = model.IsHiddenButton;
                ViewData["Status"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetProfileStatus());
                ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
                ViewData["FormalitySave"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
        }

        /// <summary>
        /// View xem thông tin chi tiết hồ sơ nhân sự
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult ViewProfileEmployee(string Id)
        {
            var model = !string.IsNullOrEmpty(Id) ? ProfileService.GetById(new Guid(Id)) : new ProfileBO();
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        public ActionResult Create(ProfileBO item)
        {
            var result = false;
            try
            {
                ProfileService.InsertProfile(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.ProfileNameHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Thêm mới hồ sơ nhân sự
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult InsertProfileEmployee(ProfileBO item)
        {
            var result = false;
            try
            {
                ProfileService.InsertProfileEmployee(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.ProfileNameHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult Update(ProfileBO item)
        {
            var result = false;
            try
            {
                ProfileService.UpdateProfile(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        [ValidateInput(false)]
        public ActionResult UpdateProfileEmployee(ProfileBO item)
        {
            var result = false;
            try
            {
                ProfileService.UpdateProfileEmployee(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult DeleteProfile(Guid? id)
        {
            var result = false;
            if (id == null || id == Guid.Empty)
                return this.Direct();

            try
            {
                ProfileService.DeleteProfile(id.Value);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }
        #endregion

        #region Hồ sơ lưu trữ
        public ActionResult ProfileArchive(string containerId)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetProfileStatus();
            ViewData["ArchiveType"] = iDAS.Service.Common.Resource.GetProfileArchiveTypes();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "ProfileArchive",
                WrapByScriptTag = false,
                ViewData = ViewData
            };
        }
        public ActionResult GetDataArchive(StoreRequestParameters parameter, string filterName = default(string), iDAS.Service.Common.Resource.ProfileStatus status = 0, iDAS.Service.Common.Resource.ProfileArchiveTypes archiveType = 0, string objectId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = ProfileService.GetProfileArchives(pageIndex, pageSize, out count, filterName, archiveType, status, objectId);
                return this.Store(data, count);
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }
        #endregion

        #region Hồ sơ lưu chuyển
        public ActionResult ProfileTransfer(string containerId)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetProfileTransferStatus();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "ProfileTransfer",
                WrapByScriptTag = false,
                ViewData = ViewData
            };
        }
        public ActionResult GetDataTransfers(StoreRequestParameters parameter, string filterName = default(string), iDAS.Service.Common.Resource.ProfileTransferStatus status = 0, string objectId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = ProfileService.GetProfileTransfers(pageIndex, pageSize, out count, filterName, status, objectId);
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        #endregion

        #region Hồ sơ chờ hủy
        public ActionResult ProfileWaitDestroy(string containerId)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetProfileWaitDestroyStatus();
            ViewData["ArchiveTypeWaitDestroy"] = iDAS.Service.Common.Resource.GetProfileArchiveTypes();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "ProfileWaitDestroy",
                WrapByScriptTag = false,
                ViewData = ViewData
            };
        }
        /// <summary>
        /// Danh sách hồ sơ chờ hủy
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="filterName"></param>
        /// <param name="status"></param>
        /// <param name="archiveType"></param>
        /// <param name="objectId"></param>
        /// <returns></returns>
        public ActionResult GetDataWaitDestroy(StoreRequestParameters parameter, string filterName = default(string), iDAS.Service.Common.Resource.ProfileStatus status = 0, iDAS.Service.Common.Resource.ProfileArchiveTypes archiveType = 0, string objectId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = ProfileService.GetProfileWaitDestroy(pageIndex, pageSize, out count, filterName, archiveType, status, objectId);
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        #endregion

        #region Hồ sơ đã hủy
        public ActionResult ProfileDestroy(string containerId)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetProfileStatus();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "ProfileDestroy",
                WrapByScriptTag = false,
                ViewData = ViewData
            };
        }
        public ActionResult GetDataDestroys(StoreRequestParameters parameter, string filterName = default(string), string objectId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = ProfileService.GetProfileDestroys(pageIndex, pageSize, out count, filterName, objectId);
                var listdt = data.ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        #endregion

        #region Hồ sơ mượn trả
        public ActionResult ProfileBorrowes(string containerId)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetProfileBorrowStatus();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "ProfileBorrowes",
                WrapByScriptTag = false,
                ViewData = ViewData
            };
        }
        public ActionResult GetDataBorrows(StoreRequestParameters parameter, string filterName = default(string), iDAS.Service.Common.Resource.ProfileBorrowStatus status = 0, string objectId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = ProfileService.GetProfileBorrows(pageIndex, pageSize, out count, filterName, status, objectId);
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        #endregion

        #region Kiểm tra hồ sơ

        public ActionResult FormReview(string profileId = default(string), string ResponsibilityId = default(string))
        {
            try
            {
                var model = ProfileService.GetDetailDataProfile(new Guid(profileId));
                ViewData["ResponsibilityId"] = ResponsibilityId;
                ViewData["StatusComponent"] = iDAS.Service.Common.Resource.GetProfileComponentStatus();
                ViewData["Status"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetProfileStatus());
                ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
                ViewData["FormalitySave"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }
        #endregion
    }
}