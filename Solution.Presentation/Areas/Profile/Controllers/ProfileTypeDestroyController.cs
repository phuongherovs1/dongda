﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using Ext.Net;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class ProfileTypeDestroyController : FrontController
    {
        public ActionResult Form(string id = default(string))
        {
            object model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new ProfileTypeDestroyBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                var Profileinfo = ProfileTypeDestroyService.GetById(new Guid(id.ToString()));
                model = Profileinfo;
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult Index()
        {
            return PartialView();
        }

        public ActionResult GetAll()
        {
            var data = ProfileTypeDestroyService.GetAll();
            return this.Store(data);
        }
        [HttpPost]
        public ActionResult Create(ProfileTypeDestroyBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ProfileTypeDestroyService.Insert(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Update(ProfileTypeDestroyBO item)
        {
            try
            {
                var ProfileSecuritUpdate = new ProfileTypeDestroyBO()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Color = item.Color,
                    Description = item.Description,
                };
                ProfileTypeDestroyService.Update(ProfileSecuritUpdate);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: true);
        }
        public ActionResult Delete(Guid id)
        {
            try
            {
                ProfileTypeDestroyService.Delete(id);
                return this.Direct(result: true);
            }
            catch
            {
                return this.Direct(result: false);
            }
        }
    }
}