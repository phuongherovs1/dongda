﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class ProfileAssignController : FrontController
    {
        public ActionResult Index(string id = default(string))
        {
            try
            {
                return new Ext.Net.MVC.PartialViewResult { Model = id };
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult LoadByProfiles(StoreRequestParameters param, iDAS.Service.Common.Resource.ProfileAssignStatus status = iDAS.Service.Common.Resource.ProfileAssignStatus.All,
                                            string profileId = "", string query = "")
        {
            if (string.IsNullOrEmpty(profileId)) return this.Store(null);
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = ProfileAssignService.GetByProfile(pageIndex, pageSize, out count, new Guid(profileId), status, query);
            return this.Store(data, count);
        }
        public ActionResult Form(string id = "", string profileId = "")
        {
            try
            {
                var model = new ProfileAssignBO();
                if (!string.IsNullOrEmpty(id))
                {
                    ViewData.Add("Operation", Common.Operation.Update);
                    model = ProfileAssignService.GetDetail(new Guid(id));
                }
                else
                {
                    model.ProfileId = new Guid(profileId);
                    var profile = ProfileAssignService.GetProfileInfo(model.ProfileId.Value);
                    model.ProfileName = profile.Name;
                    model.DepartmentId = profile.DepartmentId;
                    model.Department = new DepartmentBO()
                    {
                        Name = profile.DepartmentName
                    };
                    ViewData.Add("Operation", Common.Operation.Create);
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                return this.Direct();
            }
        }
        public ActionResult Create(ProfileAssignBO data)
        {
            var result = false;
            try
            {
                ProfileAssignService.InsertProfile(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult Update(ProfileAssignBO data)
        {
            var result = false;
            try
            {
                ProfileAssignService.UpdateProfile(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {

                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult SendAssign(ProfileAssignBO data)
        {
            var result = false;
            try
            {
                ProfileAssignService.SendProfileAssign(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
                Notify(TitleResourceNotifies.DeliveredAssign, data.ContentOfSuggest, data.EmployeePerformBy.Id, UrlResourceNotifies.DeliveredAssign, new { Id = data.ProfileId });
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult SendAssignById(string id = "")
        {
            var result = false;
            try
            {
                ProfileAssignService.SendProfileAssignById(new Guid(id));
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {

                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult RevertAssign(string id = "")
        {
            var result = false;
            try
            {
                ProfileAssignService.RevertAssign(new Guid(id));
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {

                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult DestroyAssign(string id = "")
        {
            var result = false;
            try
            {
                ProfileAssignService.DestroyAssign(new Guid(id));
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {

                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult DeleteAssign(string id = "")
        {
            var result = false;
            try
            {
                ProfileAssignService.DeleteAssign(new Guid(id));
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {

                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
    }
}