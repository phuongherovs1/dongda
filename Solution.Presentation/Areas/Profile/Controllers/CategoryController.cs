﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using Ext.Net;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class CategoryController : FrontController
    {
        //
        // GET: /Profile/Category/
        private Node createNodeCategory(ProfileCategoryBO category)
        {
            var node = new Node();
            node.NodeID = category.Id.ToString();
            node.Text = " " + category.NameFormat;
            node.IconCls = !category.ParentId.HasValue ? "x-fa fa-folder" : "x-fa fa-folder-o";
            node.CustomAttributes.Add(new ConfigItem { Name = "ParentId", Value = category.ParentId.ToString(), Mode = ParameterMode.Value });
            node.Leaf = !category.IsParent;
            node.Expanded = false;
            return node;
        }

        public ActionResult GetData(string node, string departmentId)
        {
            try
            {
                var nodes = new NodeCollection();
                if (departmentId.Contains('_'))
                {
                    string[] DepartmentIdsl = departmentId.Trim().Split('_');
                    departmentId = DepartmentIdsl[1];
                }
                var documentCategoryID = node == "root" ? (Guid?)null : new Guid(Convert.ToString(node));
                var documentCategories = ProfileCategoryService.GetTreeProfileCategory(documentCategoryID, new Guid(departmentId)).ToList();
                foreach (var category in documentCategories)
                {
                    nodes.Add(createNodeCategory(category));
                }

                return this.Content(nodes.ToJson());
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Form thêm mới/sửa danh mục
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <param name="IsNoParent"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public ActionResult Form(string id = default(string), string departmentId = default(string), bool IsNoParent = false, string categoryId = default(string))
        {
            try
            {
                ProfileCategoryBO model = null;
                ViewData.Add("NoParent", IsNoParent);
                ViewData["ArchiveTime"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
                if (id == default(string))
                {
                    ViewData.Add("Operation", Common.Operation.Create);
                }
                else
                {
                    ViewData.Add("Operation", Common.Operation.Update);
                }
                model = ProfileCategoryService.GetDetail(id, departmentId, categoryId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                return this.Direct();
            }
        }

        /// <summary>
        /// Danh sách danh mục theo phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public ActionResult GetByDepartment(string departmentId = default(string), string categoryId = default(string))
        {
            try
            {
                var data = ProfileCategoryService.GetByDepartment(departmentId, categoryId).ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Thêm mới danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(ProfileCategoryBO item, string tranferDepartment = default(string))
        {
            var result = false;
            try
            {
                if (!String.IsNullOrEmpty(tranferDepartment))
                {
                    var department = tranferDepartment.Split('_').ToList();
                    if (department != null && department.Count == 2)
                    {
                        item.TransferDepartmentId = new Guid(department[1]);
                    }
                }
                ProfileCategoryService.Insert(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Sửa danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(ProfileCategoryBO item, string tranferDepartment = default(string))
        {
            var result = false;
            try
            {
                if (!String.IsNullOrEmpty(tranferDepartment))
                {
                    var department = tranferDepartment.Split('_').ToList();
                    if (department != null && department.Count == 2)
                    {
                        item.TransferDepartmentId = new Guid(department[1]);
                    }
                }
                ProfileCategoryService.Update(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ProfileCategoryService.DeleteCategory(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.ProfileCategoryExitsProfile || ex is iDAS.Service.Common.AccessDenyException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}