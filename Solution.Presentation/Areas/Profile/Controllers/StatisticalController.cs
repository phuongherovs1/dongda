﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class StatisticalController : FrontController
    {
        //
        // GET: /Profile/Statistical/
        public StoreResult GetDataProfileStatusAnalytic(string departmentId = default(string))
        {

            var summary = ProfileSummaryService.ProfileStatusAnalytic(iDAS.Service.Common.Utilities.ConvertToGuid(departmentId));
            return new StoreResult(summary);

        }
        public StoreResult GetDataProfileBorrowAnalytic(string departmentId = default(string))
        {

            var summary = ProfileSummaryService.ProfileBorrowAnalytic(iDAS.Service.Common.Utilities.ConvertToGuid(departmentId));
            return new StoreResult(summary);

        }
	}
}