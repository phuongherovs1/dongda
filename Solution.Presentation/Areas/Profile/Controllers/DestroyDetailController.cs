﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using Ext.Net;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class DestroyDetailController : FrontController
    {
        /// <summary>
        /// Danh sách hồ sơ của đề nghị hủy
        /// </summary>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public ActionResult LoadDetail(Guid? destroySuggestId, string query = "",
                                        iDAS.Service.Common.Resource.ProfileStatus status = iDAS.Service.Common.Resource.ProfileStatus.All)
        {
            object data = null;
            if (destroySuggestId != null && destroySuggestId != Guid.Empty)
                data = ProfileDestroyDetailService.GetByProfileDestroySuggestId(destroySuggestId.Value, query, status);
            return this.Store(data);
        }

        /// <summary>
        /// Danh sách hồ sơ chờ hủy mà chưa được tạo đề nghị hủy
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult GetProfileWaitDestroyForAddDestroySuggest(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = ProfileService.GetProfileWaitDestroyForAddDestroySuggest(pageIndex, pageSize, out count);
            return this.Store(data);
        }

        /// <summary>
        /// Form chọn thêm hồ sơ chờ hủy
        /// </summary>
        /// <returns></returns>
        public ActionResult FormAddProfileWaitDestroy()
        {
            try
            {
                return new Ext.Net.MVC.PartialViewResult { };
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Form xem xét hồ sơ đề nghị hủy
        /// </summary>
        /// <returns></returns>
        public ActionResult FormReview(string id = default(string))
        {
            try
            {
                object model = null;
                if(!String.IsNullOrEmpty(id))
                {
                    model = ProfileDestroyDetailService.Review(new Guid(id));
                }
                else
                {
                    model = new ProfileDestroyDetailBO()
                    {
                        Id = Guid.Empty,
                        Profile = new ProfileBO()
                    };
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Cập nhật hồ sơ đề nghị hủy
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult Update(ProfileDestroyDetailBO data)
        {
            var result = false;
            try
            {
                ProfileDestroyDetailService.UpdateDetroyDetail(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Cập nhật kết quả hủy
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult UpdateDestroyResult(ProfileDestroyDetailBO data)
        {
            var result = false;
            try
            {
                ProfileDestroyDetailService.UpdateDestroyResult(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Xóa hồ sơ của đề nghị
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ProfileDestroyDetailService.Delete(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}