﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class ProfileBorrowController : FrontController
    {
        //
        // GET: /Profile/ProfileBorrow/
        public ActionResult ExtendBorrow(string id = default(string))
        {
            var model = ProfileBorrowService.GetBorrowById(new Guid(id));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult Borrow(string profileId = default(string))
        {
            var model = ProfileBorrowService.GetByProfile(new Guid(profileId));
            if (model.AssignEmployee == null)
            {
                var assignEmployee = EmployeeService.GetEmployeeGeneralInfo(EmployeeService.GetUserCurrent().Id);
                model.AssignEmployee = assignEmployee;
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult Return(string profileId = default(string))
        {
            var model = ProfileBorrowService.GetByProfile(new Guid(profileId));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(ProfileBorrowBO item)
        {
            var result = false;
            try
            {
                ProfileBorrowService.Borrow(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AssignProfile(ProfileBorrowBO item)
        {
            var result = false;
            try
            {
                ProfileBorrowService.AssignProfile(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ReturnProfile(ProfileBorrowBO item)
        {
            var result = false;
            try
            {
                ProfileBorrowService.ReturnProfile(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(ProfileBorrowBO item)
        {
            var result = false;
            try
            {
                ProfileBorrowService.Update(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ProfileBorrowService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}