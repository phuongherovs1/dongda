﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class DashBoardController : FrontController
    {
        public ActionResult Test()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }
        public ActionResult ProfileByMe(string TypeList)
        {
            ViewData["TypeProfile"] = TypeList;
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        // GET: /Profile/DashBoard/
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            var employee = EmployeeService.GetCurrentUser();
            ViewData["EmployeeName"] = employee.Name;
            ViewData["EmployeeID"] = employee.Id;

            ViewData["Status"] = iDAS.Service.Common.Resource.GetProfileStatus();
            ViewData["StatusBorrow"] = iDAS.Service.Common.Resource.GetProfileBorrowStatus();
            ViewData["ArchiveType"] = iDAS.Service.Common.Resource.GetProfileArchiveTypes();
            ViewData["StatusDestroy"] = iDAS.Service.Common.Resource.GetProfileStatus();
            ViewData["StatusTransfer"] = iDAS.Service.Common.Resource.GetProfileTransferStatus();
            ViewData["StatusWaitDestroy"] = iDAS.Service.Common.Resource.GetProfileWaitDestroyStatus();
            ViewData["ArchiveTypeWaitDestroy"] = iDAS.Service.Common.Resource.GetProfileArchiveTypes();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        // Danh sách mục hồ sơ
        public ActionResult ProfileList()
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetProfileStatus();
            return PartialView();
        }
        // Hồ sơ lưu trữ
        public ActionResult ArchiveProfile(string containerId)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetProfileStatus();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "ArchiveProfile",
                WrapByScriptTag = false,
                ViewData = ViewData
            };
        }
        // Lấy danh sách phân công, cập nhật
        public ActionResult LoadProfileAssignUpdate()
        {
            var data = ProfileSummaryService.GetDasboardAssignUpdate();
            return this.Store(data);
        }
        // Lấy danh sách đề nghị kiểm tra
        public ActionResult LoadProfileSuggestCheck()
        {
            var data = ProfileSummaryService.GetDasboardSuggestCheck();
            return this.Store(data);
        }
        // Lấy danh sách hồ sơ phê duyệt
        public ActionResult LoadProfileApproval()
        {
            var data = ProfileSummaryService.GetDasboardApproval();
            return this.Store(data);
        }
        // Lấy danh sách hồ sơ lưu trữ
        public ActionResult LoadProfileArchire()
        {
            var data = ProfileSummaryService.GetDasboardArchire();
            return this.Store(data);
        }

       
	}
}