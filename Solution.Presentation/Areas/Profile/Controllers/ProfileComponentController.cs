﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class ProfileComponentController : FrontController
    {
        public ActionResult Index(string id = default(string))
        {
            try
            {
                ViewData["Status"] = iDAS.Service.Common.Resource.GetProfileComponentStatus();
                if (!string.IsNullOrEmpty(id))
                {
                    ProfileComponentService.UpdateProfileComponentPrinciple(new Guid(id));
                    ViewData["ShowOrHideButtonComponent"] = ProfileComponentService.CheckRoleAddOrDelete(new Guid(id));
                    ViewData["ShowOrHideButtonUpdate"] = ProfileComponentService.CheckRoleUpdate(new Guid(id));
                }
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = id };
            }
            catch
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Form xem chi tiết danh sách thành phần hồ sơ
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult FormViewDetail(string Id = default(string))
        {
            try
            {
                ViewData["Status"] = iDAS.Service.Common.Resource.GetProfileComponentStatus();
                if (!string.IsNullOrEmpty(Id)) { ProfileComponentService.UpdateProfileComponentPrinciple(new Guid(Id)); }
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = Id };
            }
            catch
            {
                return this.Direct();
            }
        }

        public ActionResult LoadByProfiles(StoreRequestParameters param, iDAS.Service.Common.Resource.ProfileComponentStatus status, string profileId = "", string query = "")
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            if (string.IsNullOrEmpty(profileId)) return this.Store(null);
            var data = ProfileComponentService.GetByProfile(pageIndex, pageSize, out count, new Guid(profileId), status, query).ToList();
            return this.Store(data);
        }
        public ActionResult Form(string id = "", string profileId = "")
        {
            try
            {
                ViewData["ProfileComponentArchiveType"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
                ViewData["ShowOrHideButtonSave"] = ProfileComponentService.CheckRoleSaveProfileComponent(new Guid(profileId));
                var model = new ProfileComponentBO();
                if (!string.IsNullOrEmpty(id))
                {
                    ViewData.Add("Operation", Common.Operation.Update);
                    model = ProfileComponentService.GetDetail(new Guid(id));
                    if (model.ProfilePrincipleId.HasValue)
                    {
                        return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData, ViewName = "FormUpdate" };
                    }
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
                }
                else
                {
                    model.ProfileId = new Guid(profileId);
                    var profile = ProfileComponentService.GetProfileInfo(model.ProfileId.Value);
                    model.ProfileName = profile.Name;
                    model.ProfileCategoryName = profile.CategoryName;
                    model.ProfileUpdateAt = DateTime.Now;
                    ViewData.Add("Operation", Common.Operation.Create);
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
                }
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                return this.Direct();
            }
        }
        public ActionResult Create(ProfileComponentBO data)
        {
            var result = false;
            try
            {
                ProfileComponentService.InsertProfilecomponent(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.ProfileNameHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult Update(ProfileComponentBO data)
        {
            var result = false;
            try
            {
                ProfileComponentService.UpdateProfilecomponent(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.ProfileNameHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult CancelComponent(string id = "")
        {
            var result = false;
            try
            {
                ProfileComponentService.CancelComponent(new Guid(id));
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult UseComponent(string id = "")
        {
            var result = false;
            try
            {
                ProfileComponentService.UseComponent(new Guid(id));
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {

                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult DeleteComponent(string id = "")
        {
            var result = false;
            try
            {
                ProfileComponentService.Delete(new Guid(id));
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {

                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
    }
}