﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using Ext.Net;
using iDAS.Service;
using iDAS.Service.Common;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class DestroyReportController : FrontController
    {
        /// <summary>
        /// Form thêm mới/sửa danh mục
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <param name="IsNoParent"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public ActionResult Form(string departmentId = default(string),string ProfileDestroySuggest = default(string))
        {
            try
            {
                if (string.IsNullOrEmpty(ProfileDestroySuggest))
                {
                    this.ShowAlert(Resource.ExceptionErrorMessage.NotExistDestroySuggestChoose);
                    return this.Direct();
                }                   
                ProfileDestroyReportBO model = null;
                model = ProfileDestroyReportService.CreateDefault(departmentId, ProfileDestroySuggest);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.NotExistDestroySuggest)
                    this.ShowAlert(ex.Message);
                else if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }

        /// <summary>
        /// Form danh sách hồ sơ trong biên bản hủy
        /// </summary>
        /// <param name="strProfile"></param>
        /// <param name="destroySuggestId"></param>
        /// <param name="isHideButton"></param>
        /// <returns></returns>
        public ActionResult FormListProfileReport(string strProfile = "", string destroySuggestId = "", bool isHideButton = false)
        {
            //ViewData["ProfileIds"] = strProfile;
            ViewData["HideButton"] = isHideButton;
            ViewData["ProfileDestroySuggestId"] = destroySuggestId;
            ViewData["DestroyDetailStatus"] = iDAS.Service.Common.Resource.GetDestroyReport();
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }

        /// <summary>
        /// Form kết quả hủy
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult FormDestroyResult(string id = default(string))
        {
            try
            {
                object model = null;
                if (!String.IsNullOrEmpty(id))
                {
                    model = ProfileDestroyDetailService.Review(new Guid(id));
                }
                else
                {
                    model = new ProfileDestroyDetailBO()
                    {
                        Id = Guid.Empty,
                        Profile = new ProfileBO()
                    };
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Thêm mới danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(ProfileDestroyReportBO item, string employeeIds = default(string))
        {
            var result = false;
            try
            {
                ProfileDestroyReportService.Save(item, employeeIds);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Sửa danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(ProfileCategoryBO item)
        {
            var result = false;
            try
            {
                ProfileCategoryService.Update(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ProfileCategoryService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// Danh sách phương thức hủy
        /// </summary>
        /// <returns></returns>
        public ActionResult GetProfileTypeDestroy()
        {
            try
            {
                var data = ProfileTypeDestroyService.GetAll();
                return this.Store(data);
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }
    }
}