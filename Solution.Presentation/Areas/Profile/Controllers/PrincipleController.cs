﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using Ext.Net;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class PrincipleController : FrontController
    {
        public ActionResult Index(string categoryId = default(string))
        {
            try
            {
                ViewData["ShowOrHideButton"] = ProfilePrincipleService.ShowOrHideButton(categoryId);
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Form thêm mới/sửa danh mục
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <param name="IsNoParent"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public ActionResult Form(string id = default(string), string departmentId = default(string), string categoryId = default(string))
        {
            ProfilePrincipleBO model = null;
            ViewData["ArchiveType"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
            }
            model = ProfilePrincipleService.GetDetail(id, departmentId, categoryId);
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        /// <summary>
        /// Danh sách tài liệu nguyên tắc theo danh mục
        /// </summary>
        /// <param name="param"></param>
        /// <param name="categoryId"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public ActionResult GetDataByCategory(StoreRequestParameters param, string categoryId = default(string), string query = default(string))
        {
            try
            {
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                object data = null;
                if (!String.IsNullOrEmpty(categoryId))
                    data = ProfilePrincipleService.GetByCategory(pageIndex, pageSize, out count, new Guid(categoryId), query);
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Danh sách tài liệu ban hành
        /// </summary>
        /// <returns></returns>
        public ActionResult GetDocumentPublish()
        {
            try
            {
                var data = ProfilePrincipleService.GetDocumentPublish();
                return this.Store(data);
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Thêm mới danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(ProfilePrincipleBO item)
        {
            var result = false;
            try
            {
                ProfilePrincipleService.InsertProfile(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Sửa danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(ProfilePrincipleBO item)
        {
            var result = false;
            try
            {
                ProfilePrincipleService.UpdateProfile(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ProfilePrincipleService.DeleteProfile(id);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Sắp xếp giảm dần
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SortASC(Guid id)
        {
            var result = false;
            try
            {
                ProfilePrincipleService.SortASC(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Sắp xếp tăng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SortDESC(Guid id)
        {
            var result = false;
            try
            {
                ProfilePrincipleService.SortDESC(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Sử dụng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Using(Guid id)
        {
            var result = false;
            try
            {
                ProfilePrincipleService.Using(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Không sử dụng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult NotUsing(Guid id)
        {
            var result = false;
            try
            {
                ProfilePrincipleService.NotUse(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
    }
}