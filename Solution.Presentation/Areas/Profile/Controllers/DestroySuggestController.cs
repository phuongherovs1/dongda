﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using Ext.Net;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class DestroySuggestController : FrontController
    {
        public ActionResult Index()
        {
            try
            {
                ViewData["DestroySuggestStatus"] = iDAS.Service.Common.Resource.GetProfileDestroyStatus();
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult ListSuggest()
        {
            try
            {
                ViewData["DestroySuggestStatus"] = iDAS.Service.Common.Resource.GetProfileDestroyStatus();
                return PartialView(ViewData = ViewData);
            }
            catch
            {
                return this.Direct();
            }
        }
        /// <summary>
        /// Form chọn hồ sơ từ những hồ sơ chờ hủy
        /// </summary>
        /// <returns></returns>
        public ActionResult FormListProfileDestroy(string strProfile = "", string destroySuggestId = "", bool hideButtonReview = false, bool isHideButton = false)
        {
            //ViewData["ProfileIds"] = strProfile;
            ViewData["HideButton"] = isHideButton;
            ViewData["ProfileDestroySuggestId"] = destroySuggestId;
            ViewData["HideButtonReview"] = hideButtonReview;
            ViewData["DestroyDetailStatus"] = iDAS.Service.Common.Resource.GetDestroyDetail();
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }

        /// <summary>
        /// Danh sách đề nghị hủy
        /// </summary>
        /// <param name="param"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public ActionResult GetData(StoreRequestParameters param, iDAS.Service.Common.Resource.ProfileDestroyStatus status = iDAS.Service.Common.Resource.ProfileDestroyStatus.All)
        {
            try
            {
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                var data = ProfileDestroySuggestService.GetData(pageIndex, pageSize, out count, status);
                return this.Store(data);
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Form thêm mới/sửa danh mục
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <param name="IsNoParent"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public ActionResult Form(string profileIds = default(string), string departmentId = default(string))
        {
            try
            {
                //ViewData["ProfileIds"] = profileIds;
                ProfileDestroySuggestBO model = null;
                model = ProfileDestroySuggestService.CreateDefault(departmentId);
                profileIds = String.IsNullOrEmpty(model.strProfile) ? profileIds : model.strProfile;
                if (String.IsNullOrEmpty(profileIds))
                    throw new iDAS.Service.Common.NotExistProfileDestroy();
                ViewData["ProfileIds"] = profileIds;
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException || ex is iDAS.Service.Common.NotExistProfileDestroy)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }

        /// <summary>
        /// Form phê duyệt đề nghị
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult FormApproval(string id = default(string))
        {
            try
            {
                ProfileDestroySuggestBO model = null;
                model = ProfileDestroySuggestService.GetForApproval(id);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Danh sách danh mục theo phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public ActionResult GetByDepartment(string departmentId = default(string), string categoryId = default(string))
        {
            try
            {
                var data = ProfileCategoryService.GetByDepartment(departmentId, categoryId).ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Thêm mới danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(ProfileDestroySuggestBO item, string departmentId = default(string))
        {
            var result = false;
            try
            {
                ProfileDestroySuggestService.Save(item, item.strProfile, departmentId);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Sửa danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Send(ProfileDestroySuggestBO item, string departmentId = default(string))
        {
            var result = false;
            try
            {
                ProfileDestroySuggestService.Send(item, item.strProfile, departmentId);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
                // Gửi thông báo phê duyệt đề nghị hủy đến người phê duyệt
                Notify(TitleResourceNotifies.SendProfileDestroy, item.ReasonOfSuggest, item.EmployeeApproveBy.Id, UrlResourceNotifies.SendProfileDestroy, new { id = item.Id });
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ProfileDestroySuggestService.DeleteProfileDestroy(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Phê duyệt đề nghị hủy
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Approve(ProfileDestroySuggestBO item)
        {
            var result = false;
            try
            {
                ProfileDestroySuggestService.Approve(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
                // Gửi thông báo lập biên bản hủy khi phê duyệt Đạt
                if (item.IsApprove == true && item.IsAccept == true && item.DepartmentId != null && item.DepartmentId != Guid.Empty)
                    Notify(TitleResourceNotifies.ApprovedProfileDestroy, item.ReasonOfReject, item.CreateBy.Value, UrlResourceNotifies.ApprovedProfileDestroy, new { departmentId = item.DepartmentId });
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
    }
}