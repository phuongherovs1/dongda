﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class ProfileSuggestBorrowController : FrontController
    {
        //
        // GET: /Profile/ProfileSuggestBorrow/
        #region View
        public ActionResult Form(string id = default(string), string profileId = default(string))
        {
            try
            {
                if (!String.IsNullOrEmpty(profileId) && ProfileBorrowService.ExitBorrow(new Guid(profileId)))
                {
                    iDAS.Presentation.Common.Utilities.ShowMessageBox("Thông báo", "Hồ sơ hiện tại đã có người mượn.Vui lòng liên hệ trực tiếp với người lưu trữ để biết chi tiết!", MessageBox.Icon.WARNING);
                    return this.Direct(result: false);
                }
                else
                {
                    ProfileSuggestBorrowBO model = null;
                    if (id == default(string))
                    {
                        ViewData.Add("Operation", Common.Operation.Create);
                        var profile = ProfileService.GetDetailDataProfile(new Guid(profileId));
                        model = new ProfileSuggestBorrowBO() { Profile = profile, Approval = profile.EmployeeArchive };
                    }
                    else
                    {
                        ViewData.Add("Operation", Common.Operation.Update);
                        model = ProfileSuggestBorrowService.GetById(new Guid(id.ToString()));
                        model.Profile = model.ProfileId.HasValue ? ProfileService.GetDetailDataProfile(model.ProfileId.Value) : new ProfileBO();
                        model.Approval = model.Profile.EmployeeArchive;
                    }
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
                }
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult Approval(string id = default(string))
        {
            try
            {
                var data = ProfileSuggestBorrowService.GetDetail(new Guid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = data };
            }
            catch
            {
                return this.Direct();
            }
        }
        #endregion
        #region Method
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(ProfileSuggestBorrowBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    var id = ProfileSuggestBorrowService.Insert(item);
                    result = true;
                    Notify(TitleResourceNotifies.ApprovalSuggestBorrow, item.ReasonBorrow, item.Approval.Id, UrlResourceNotifies.ApprovalSuggestBorrow, new { id = id });
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ApprovalSuggest(ProfileSuggestBorrowBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ProfileSuggestBorrowService.ApprovalSuggest(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(ProfileSuggestBorrowBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ProfileSuggestBorrowService.Update(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ProfileSuggestBorrowService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}