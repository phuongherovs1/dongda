﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class ProfileTransferController : FrontController
    {
        //
        // GET: /Profile/ProfileTransfer/
        #region View
        public ActionResult Form(string id = default(string), string profileId = default(string))
        {
            ProfileTransferBO model = null;
            ViewData["SendTransferResult"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetSendTransferResult());
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new ProfileTransferBO() { SendEmployee = EmployeeService.GetById(EmployeeService.GetCurrentUser().Id), Profile = ProfileService.GetDetailDataProfile(new Guid(profileId)) };
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = ProfileTransferService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult Receive(string profileId = default(string))
        {
            ProfileTransferBO model = null;
            model = ProfileTransferService.GetByProfile(new Guid(profileId.ToString()));
            model.ArchiveEmployee = EmployeeService.GetById(EmployeeService.GetCurrentUser().Id);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        #endregion
        #region Method
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = AssetGroupService.Page(out count, pageIndex, pageSize).ToList();
            return this.Store(data, count);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(ProfileTransferBO item)
        {
            var result = false;
            try
            {
                ProfileTransferService.TransferProfile(item);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ReceiveProfile(ProfileTransferBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ProfileTransferService.ReceiveProfile(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(ProfileTransferBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ProfileTransferService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ProfileTransferService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}