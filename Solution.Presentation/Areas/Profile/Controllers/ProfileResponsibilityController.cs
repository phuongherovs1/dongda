﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Profile.Controllers
{
    public class ProfileResponsibilityController : FrontController
    {
        public ActionResult Index(string id = default(string))
        {
            try
            {
                return new Ext.Net.MVC.PartialViewResult { Model = id };
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult LoadByProfiles(StoreRequestParameters param, iDAS.Service.Common.Resource.ProfileResponsibilityStatus status = iDAS.Service.Common.Resource.ProfileResponsibilityStatus.All
                                            , string profileId = "", string query = "")
        {
            if (string.IsNullOrEmpty(profileId)) return this.Store(null);
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = ProfileResponsibilityService.GetByProfile(pageIndex, pageSize, out count, new Guid(profileId), status, query);
            return this.Store(data, count);
        }
        public ActionResult Form(string id = "", string profileId = "")
        {
            try
            {
                var model = new ProfileResponsibilityBO();
                if (!string.IsNullOrEmpty(id))
                {
                    ViewData.Add("Operation", Common.Operation.Update);
                    model = ProfileResponsibilityService.GetDetail(new Guid(id));
                    if (model.StatusType == (int)iDAS.Service.Common.Resource.ProfileResponsibilityStatus.WaitSend)
                        return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData, ViewName = "FormCreate" };
                    else
                    {
                        return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData, ViewName = "FormDetail" };
                    }
                }
                else
                {
                    model = ProfileResponsibilityService.CreateDefault(profileId);
                    ViewData.Add("Operation", Common.Operation.Create);

                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData, ViewName = "FormCreate" };
                }
            }
            catch(Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }

        /// <summary>
        /// Form kiểm tra/phê duyệt hồ sơ
        /// </summary>
        /// <param name="id"></param>
        /// <param name="profileId"></param>
        /// <returns></returns>
        public ActionResult FormApprove(string id = "", string profileId = "")
        {
            try
            {
                var model = new ProfileResponsibilityBO();
                if (!string.IsNullOrEmpty(id))
                {
                    ViewData.Add("Operation", Common.Operation.Update);
                    model = ProfileResponsibilityService.GetDetail(new Guid(id));
                    if (model.RoleType == (int)iDAS.Service.Common.Resource.ProfileResponsibilityRole.Approval)
                        return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData, ViewName = "FormAppove" };
                    else if (model.RoleType == (int)iDAS.Service.Common.Resource.ProfileResponsibilityRole.Checker)
                        return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData, ViewName = "FormReview" };
                    else
                        return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData, ViewName = "FormDetail" };
                }
            }
            catch
            {
                this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct();
        }

        public ActionResult FormUpdate(string id = "")
        {
            try
            {
                var model = new ProfileResponsibilityBO();
                if (!string.IsNullOrEmpty(id))
                {
                    model = ProfileResponsibilityService.GetDetail(new Guid(id));
                    return new Ext.Net.MVC.PartialViewResult { Model = model };
                }
            }
            catch
            {
                this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct();
        }

        public ActionResult Create(ProfileResponsibilityBO data)
        {
            var result = false;
            try
            {
                data.Id = ProfileResponsibilityService.InsertProfile(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult Update(ProfileResponsibilityBO data)
        {
            var result = false;
            try
            {
                ProfileResponsibilityService.UpdateProfile(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {

                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult SendResponsibilityById(string id = "")
        {
            var result = false;
            try
            {
                ProfileResponsibilityService.SendProfileResponsibilityById(new Guid(id));
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult RevertResponsibility(string id = "")
        {
            var result = false;
            try
            {
                ProfileResponsibilityService.RevertResponsibility(new Guid(id));
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult DeleteResponsibility(string id = "")
        {
            var result = false;
            try
            {
                ProfileResponsibilityService.DeleteResponsibility(new Guid(id));
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
    }
}