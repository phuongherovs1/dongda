﻿using Ext.Net;
using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Asset.Controllers
{
    public class SettingController : FrontController
    {
        //
        // GET: /Asset/Setting/
       
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {

                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                WrapByScriptTag = false
            };
        }
	}
}