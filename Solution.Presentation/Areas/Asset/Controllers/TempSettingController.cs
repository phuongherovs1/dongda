﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using Ext.Net;

namespace iDAS.Presentation.Areas.Asset.Controllers
{
    public class TempSettingController : FrontController
    {
        #region View
        public ActionResult Index()
        {
            return PartialView();
        }
        public ActionResult Form(string id = "", bool isDetail = false)
        {
            object model = null;
            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new AssetTempSettingBO();
            }
            else
            {
                ViewData.Add("Operation", isDetail ? Common.Operation.Read : Common.Operation.Update);
                model = AssetTempSettingService.GetById(new Guid(id.ToString()));

            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion
        #region Method
        public ActionResult GetData(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = AssetTempSettingService.Page(out count, pageIndex, pageSize).ToList();
            return this.Store(data, count);
        }
        [ValidateInput(false)]
        public ActionResult Create(AssetTempSettingBO item)
        {
            try
            {
                AssetTempSettingService.Insert(item);
                return this.Direct(result: true);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                return this.Direct(result: ex);
            }
        }
        [ValidateInput(false)]
        public ActionResult Update(AssetTempSettingBO item)
        {
            try
            {
                AssetTempSettingService.Update(item);
                return this.Direct(result: true);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                return this.Direct(result: ex);
            }
        }
        public ActionResult Delete(Guid id)
        {
            AssetTempSettingService.Delete(id);
            return this.Direct(result: true);
        }
        #endregion
    }
}