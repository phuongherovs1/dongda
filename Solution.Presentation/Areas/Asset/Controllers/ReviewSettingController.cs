﻿using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;

using Ext.Net;
using iDAS.Service.Common;
namespace iDAS.Presentation.Areas.Asset.Controllers
{
    public class ReviewSettingController : FrontController
    {
        #region View
        public ActionResult Index()
        {
          //  var selectReview = iDAS.Presentation.Common.Utilities.GetSelectItem(AssetReviewSettingService.GetLogicReview());
          //  ViewData["SelectReview"] = selectReview;
            return PartialView();
        }
        public ActionResult GetdataselectReview(StoreParameter parame)
        {
            var selectReview = iDAS.Presentation.Common.Utilities.GetSelectItemToObject(AssetReviewSettingService.GetLogicReview());
            return this.Store(selectReview);
        }

        public ActionResult Form(iDAS.Service.Common.Resource.QualityReview selectReview, string id = "")
        {
            AssetReviewSettingBO model = null;
            var limitValue = AssetReviewSettingService.GetLimitImportantValue();
            ViewData.Add("LimitValue", limitValue);
            ViewData.Add("SelectReview", selectReview);

            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new AssetReviewSettingBO()
                {
                    QualityReview = (int)selectReview
                };
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = AssetReviewSettingService.GetById(new Guid(id.ToString()));
                model.QualityReview = (int)selectReview;
            }
            return new Ext.Net.MVC.PartialViewResult() { Model = model, ViewData = ViewData };
        }
        #endregion
        #region Method
        public ActionResult FillColor()
        {
            return this.Store(iDAS.Service.Common.Utilities.RenderColor());
        }
        public ActionResult GetByValue(int value, iDAS.Service.Common.Resource.QualityReview selectReview)
        {
            var data = AssetReviewSettingService.GetByValue(value, selectReview);
            return Json(data);
        }

        public ActionResult GetData(StoreRequestParameters param, int selectReview = 0)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = AssetReviewSettingService.SelectByReview(out count, pageIndex, pageSize, (Service.Common.Resource.QualityReview)selectReview).ToList();
            return this.Store(data, count);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(AssetReviewSettingBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    AssetReviewSettingService.Insert(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (!string.IsNullOrEmpty(ex.Message))
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(AssetReviewSettingBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    AssetReviewSettingService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (!string.IsNullOrEmpty(ex.Message))
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(string id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    AssetReviewSettingService.Delete(new Guid(id));
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);

        }
        #endregion
    }
}