﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using systemIO = System.IO;
using systemnet = System.Net;

namespace iDAS.Presentation.Areas.Asset.Controllers
{
    public class DashboardController : FrontController
    {
        #region View
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
            };
        }
        public ActionResult AssetInfoIndex(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return PartialView();
        }       
        public ActionResult Form(string id = "")
        {
            var selectReview = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetLogicReview());
            var selectObject = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetObjectRelative());
            ViewData.Add("SelectReview", selectReview);
            ViewData.Add("SelectObject", selectObject);
            AssetBO model;
            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = AssetService.GetFormItem();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = AssetService.GetById(new Guid(id));
                if (model.TitleId.HasValue)
                {
                    if (model.TypeTitleId == "Title")
                    {
                        var DepartmentTitle =  DepartmentTitleService.GetById(model.TitleId.Value);
                        if(DepartmentTitle != null)
                            model.TitleIdText = DepartmentTitle.Name;
                    }
                    else
                    {
                        var Department = DepartmentService.GetById(model.TitleId.Value);
                        if (Department != null)
                            model.TitleIdText = Department.Name;
                    }
                }
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult History(string id)
        {

            if (string.IsNullOrEmpty(id))
                return this.Direct();
            var model = AssetService.GetById(iDAS.Service.Common.Utilities.ConvertToGuid(id));

            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion
        #region Method
        public ActionResult GetData(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = AssetService.GetAll(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString ,pageIndex, pageSize, out count);
            return this.Store(data, count);
        }
        public ActionResult GetDataInfo(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = AssetResourceRelateService.GetAssetInfo(pageIndex, pageSize, out count);
            return this.Store(data, count);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(AssetBO asset, string TitleId = "", string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            try
            {
                if (!string.IsNullOrEmpty(TitleId))
                {
                    if (TitleId.Contains('_'))
                    {
                        asset.TitleId = new Guid(TitleId.Split('_')[1]);
                        asset.TypeTitleId = TitleId.Split('_')[0];
                    }
                    else
                        asset.TitleId = new Guid(TitleId);
                }
                AssetService.Insert(asset, dataPropertySettings, dataResourceRelates);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(AssetBO asset, string TitleId = "", string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            try
            {
                if (!string.IsNullOrEmpty(TitleId))
                {
                    if (TitleId.Contains('_'))
                    {
                        asset.TitleId = new Guid(TitleId.Split('_')[1]);
                        asset.TypeTitleId = TitleId.Split('_')[0];
                    }
                    else
                        asset.TitleId = new Guid(TitleId);
                }
                AssetService.Update(asset, dataPropertySettings, dataResourceRelates);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    AssetService.Delete(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

       
        #endregion
    }
}