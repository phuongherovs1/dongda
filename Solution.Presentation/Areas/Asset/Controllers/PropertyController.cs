﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using Ext.Net;
using iDAS.Service;
using iDAS.Service.Common;
using Newtonsoft.Json;
using System.Diagnostics;

namespace iDAS.Presentation.Areas.Asset.Controllers
{
    public class PropertyController : FrontController
    {
        public ActionResult Index(string assetId)
        {
            if (!string.IsNullOrEmpty(assetId))
            {
                ViewBag.AssetId = assetId;
                var type = AssetTypeService.GetByAsset(iDAS.Service.Common.Utilities.ConvertToGuid(assetId));
                return PartialView(type);
            }
            
            return this.Direct();
        }

        #region Method
        public ActionResult GetData()
        {
            var data = AssetPropertyService.GetAll().ToList();
            return this.Store(data);
        }

        [HttpPost]
        [ValidateInput(false)]
        public object Create(AssetPropertyBO data)
        {
            try
            {
                var result = AssetPropertyService.Insert(data);
                return JsonConvert.SerializeObject(new { code = 200, data = result.ToString(), message = Common.Resource.SystemSuccess });
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException)
                    return JsonConvert.SerializeObject(new { code = 201, message = ex.Message });
                return JsonConvert.SerializeObject(new { code = 201, message = Common.Resource.SystemFailure });
            }

        }
        [ValidateInput(false)]
        public ActionResult GetTypeFilter(List<Guid> propertyIds, string Id, string query = "")
        {
            var data = AssetPropertyService.GetPropertyFilterType(propertyIds, query);
            if(!string.IsNullOrEmpty(Id))
            {
                List<Guid> lstGuid = AssetTypePropertyService.GetByAssetType(new Guid(Id)).Select(x => x.PropertyId.Value).ToList();
                data = AssetPropertyService.GetByIds(lstGuid);
            }
            return this.Store(data);
        }

        [ValidateInput(false)]
        public ActionResult GetQuery(string query = "")
        {
            var data = AssetPropertyService.GetContainName(query).ToList();// AssetPropertyService.GetQuery(pageIndex, pageSize, query).ToList();
            return this.Store(data);
        }

        public ActionResult GetByAsset(string assetId)
        {
            var data = AssetPropertySettingService.GetByAsset(iDAS.Service.Common.Utilities.ConvertToGuid(assetId)).ToList();
            return this.Store(data);
        }
        #endregion
    }
}