﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Asset.Controllers
{
    public class PropertySettingController : FrontController
    {
        //
        // GET: /Asset/PropertySetting/
        public ActionResult Index(string assetId)
        {
            if (!string.IsNullOrEmpty(assetId))
            {
                ViewBag.AssetId = assetId;
                var type = AssetTypeService.GetByAsset(iDAS.Service.Common.Utilities.ConvertToGuid(assetId));
                var typeName = type == null ? "Chưa phân loại" : HttpUtility.HtmlEncode(type.Name);
                ViewBag.TypeName = typeName;
                return PartialView();
            }
            return this.Direct();
        }
        public ActionResult GetByAsset(string assetId)
        {
            var data = AssetPropertySettingService.GetByAsset(new Guid(assetId)).ToList();
            return this.Store(data);
        }
	}
}