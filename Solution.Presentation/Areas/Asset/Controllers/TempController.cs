﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Asset.Controllers
{
    public class TempController : FrontController
    {
        #region View
        public ActionResult Index(string assetId = "")
        {
            ViewData.Add("assetId", assetId);
            return PartialView();
        }
        public ActionResult Form(string assetId = "")
        {
            if (string.IsNullOrEmpty(assetId))
                return this.Direct();
            AssetTempBO model = null;
            var dataAssetTemp = AssetTempService.Get(x => x.AssetID.ToString() == assetId);
            if (dataAssetTemp != null && dataAssetTemp.ToList().Count > 0)
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = AssetTempService.GetById(new Guid(dataAssetTemp.ToList()[0].Id.ToString()));
                if(model.Paster != null && model.Paster.HasValue)
                    model.Creater = EmployeeService.GetById(model.Paster.Value);
                model.Template = new FileUploadBO()
                {
                    Files = model.TempFile.HasValue ? new List<Guid> { model.TempFile.Value } : new List<Guid>()
                };
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new AssetTempBO()
                {
                    AssetID = iDAS.Service.Common.Utilities.ConvertToGuid(assetId)
                };
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion
        #region Method
        public ActionResult GetData(string assetId = "")
        {
            var data = AssetTempService.GetByAssetId(iDAS.Service.Common.Utilities.ConvertToGuid(assetId)).ToList();
            return this.Store(data);
        }
        [HttpPost]
        public ActionResult Create(AssetTempBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    AssetTempService.Insert(item);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
            
        }
        [HttpPost]
        public ActionResult Update(AssetTempBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    AssetTempService.Update(item);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    AssetTempService.Delete(id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
	}
}