﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using iDAS.Service;
using iDAS.Service.Common;

namespace iDAS.Presentation.Areas.Asset.Controllers
{
    public class ReviewController : FrontController
    {
        #region View
        public ActionResult Index()
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return PartialView();
        }
        public ActionResult RelateTo()
        {
            return PartialView();
        }
        public ActionResult ReviewHistory(string assetId = "")
        {
            ViewData["AssetID"] = assetId;
            return PartialView();
        }
        public ActionResult History(string assetResourceId = "", string assetId = "")
        {
            ViewData["AssetResourceId"] = assetResourceId;
            ViewData["AssetId"] = assetId;
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        public ActionResult Form(string assetResourceId = "", string id = "")
        {
            var limitValue = AssetReviewSettingService.GetLimitValue();
            ViewData.Add("LimitValue", limitValue);
            if (string.IsNullOrEmpty(assetResourceId))
                return this.Direct();
            object model = null;
            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                var Reviewer = AssetReviewService.GetUserCurrent();
                Reviewer.RoleNames = string.Join(",", EmployeeService.GetRoleNames(Reviewer.Id));
                model = new AssetReviewBO()
                {
                    AssetResourceRelateId = new Guid(assetResourceId),
                    Reviewer = Reviewer
                };
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = AssetReviewService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion
        #region Method

        public ActionResult GetData(string assetId)
        {
            try
            {
                var data = AssetReviewService.GetByAsset(iDAS.Service.Common.Utilities.ConvertToGuid(assetId)).ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        public ActionResult GetByAssetResource(string assetResourceId)
        {
            try
            {
                var data = AssetReviewService.GetByAssetResource(iDAS.Service.Common.Utilities.ConvertToGuid(assetResourceId)).ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(AssetReviewBO item)
        {
            var result = false;

            try
            {
                AssetReviewService.Insert(item);
                result = true;
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(AssetReviewBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    AssetReviewService.Update(item);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    AssetReviewService.Delete(id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }


        #endregion
    }
}