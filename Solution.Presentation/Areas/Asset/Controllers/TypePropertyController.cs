﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
namespace iDAS.Presentation.Areas.Asset.Controllers
{
    public class TypePropertyController : FrontController
    {
        public ActionResult Index(string typeId = default(string))
        {
            ViewData["AssetTypeID"] = typeId;
            //   return new Ext.Net.MVC.PartialViewResult() { ViewData = ViewData };
            return PartialView();
        }

        [ValidateInput(false)]
        public ActionResult Form(string id = "", string assetTypeId = "", string propertyId = "", string propertyName = "", string description = "")
        {
            ViewData["DataType"] = iDAS.Presentation.Common.Utilities.GetSelectItemText(iDAS.Service.Common.Utilities.GetTypeData());
            ViewData["Operation"] = string.IsNullOrEmpty(id) ? Common.Operation.Create : Common.Operation.Update;
            var model = new AssetTypePropertyBO()
            {
                Id = iDAS.Service.Common.Utilities.ConvertToGuid(id),
                AssetTypeId = iDAS.Service.Common.Utilities.ConvertToGuid(assetTypeId),
                PropertyId = iDAS.Service.Common.Utilities.ConvertToGuid(propertyId),
                PropertyName = propertyName,
               
                Description = description
            };
            return new Ext.Net.MVC.PartialViewResult() { Model = model, ViewData = ViewData };
        }

        public ActionResult LoadData(string typeId = "")
        {
            List<AssetTypePropertyBO> data;
            if (string.IsNullOrEmpty(typeId))
            {
                data = new List<AssetTypePropertyBO>();
            }
            else
            {
                data = AssetTypePropertyService.GetByAssetType(new Guid(typeId)).ToList();
            }
            return this.Store(data);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(AssetTypePropertyBO property)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    AssetTypePropertyService.Insert(property);
                    result = true;

                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(AssetTypePropertyBO property)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    AssetTypePropertyService.Update(property);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    AssetTypePropertyService.Delete(id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}