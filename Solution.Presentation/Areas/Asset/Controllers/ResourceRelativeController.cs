﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using System.Diagnostics;
using System.Configuration;

namespace iDAS.Presentation.Areas.Asset.Controllers
{
    public class ResourceRelativeController : FrontController
    {
        //
        // GET: /Asset/ResourceRelative/    
        #region View
        //string assetId
        public ActionResult Index(string id = default(string))
        {
            try
            {
                var obj = AssetService.GetById(new Guid(id.ToString()));
                return new Ext.Net.MVC.PartialViewResult { Model = obj };
            }
            catch (Exception ex)
            {
            }
            return this.Direct();
        }
        #endregion

        #region Method
        public ActionResult GetSelectObjectRelate(Resource.ObjectRelative selectObject)
        {
            var data = AssetResourceRelateService.GetSelectObject(selectObject).ToList();
            return this.Store(data);
        }
        public ActionResult GetData(string assetId = "")
        {
            try
            {
                if (string.IsNullOrEmpty(assetId))
                    return this.Direct();
                var data = AssetResourceRelateService.GetByAssetId(new Guid(assetId)).ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult GetDataReview(StoreRequestParameters param, Guid? assetId)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = AssetResourceRelateService.GetReview(pageIndex, pageSize, out count, assetId).ToList();
            return this.Store(data, count);
        }
        public ActionResult GetRoleRelateTo(StoreRequestParameters param, string filterName = "")
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = AssetResourceRelateService.GetRoleRelateTo(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString , pageIndex, pageSize, out count, filterName).ToList();
            return this.Store(data, count);
        }
        #endregion
    }
}