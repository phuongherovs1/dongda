﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;
using Newtonsoft.Json;
namespace iDAS.Presentation.Areas.Asset.Controllers
{
    public class TypeController : FrontController
    {
        #region View
        public ActionResult Index()
        {
            return PartialView();
        }
        public ActionResult Form(string id = default(string))
        {
            object model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new AssetTypeBO() { Id = Guid.NewGuid() };
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = AssetTypeService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult() { Model = model, ViewData = ViewData };
        }
        #endregion
        #region Method
        public ActionResult GetData(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = AssetTypeService.Page(out count, pageIndex, pageSize);
            return this.Store(data, count);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(AssetTypeBO item, string dataProperties = "")
        {
            var result = false;
            var error = new Exception();
            if (ModelState.IsValid)
            {
                try
                {
                    AssetTypeService.Insert(item, dataProperties);
                    result = true;
                }
                catch (Exception ex)
                {

                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    return this.Direct(result: error);
                }
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="dataProperties"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(AssetTypeBO item, string dataProperties = "")
        {
            var result = false;
            var error = new Exception();
            if (ModelState.IsValid)
            {
                try
                {
                    AssetTypeService.Update(item, dataProperties);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    return this.Direct(result: error);
                }
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    AssetTypeService.Delete(id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}