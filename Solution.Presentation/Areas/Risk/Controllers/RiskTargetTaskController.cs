﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;


namespace iDAS.Presentation.Areas.Risk.Controllers
{
    public class RiskTargetTaskController : FrontController
    {
        //
        // GET: /Risk/RiskTargetTask/
        public ActionResult Index(string RiskTargetPlanId=default(string))
        {
            var model = RiskTargetPlanService.GetById(iDAS.Service.Common.Utilities.ConvertToGuid(RiskTargetPlanId));
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };

        }

        public ActionResult FormTask(string RiskTaskId = default(string), string RiskTargetPlanId = default(string))
        
        {
            //var model = TaskService.GetFormItem(id);
            if (RiskTaskId == default(string))
            {
                var model = new TaskBO { };
                model.RiskTargetPlanId = iDAS.Service.Common.Utilities.ConvertToGuid(RiskTargetPlanId);
                ViewData.Add("Operation", Common.Operation.Create);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                var model = TaskService.GetFormItem(new Guid(RiskTaskId));
                model.RiskTargetPlanId = iDAS.Service.Common.Utilities.ConvertToGuid(RiskTargetPlanId);
                ViewData.Add("Operation", Common.Operation.Update);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
        }
        public ActionResult GetDataRiskTargetTask(StoreRequestParameters parameter, string RiskPlanId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = TaskService.GetDataRiskTargetTask(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(RiskPlanId)).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        public ActionResult CreateRiskTargetTask(TaskBO item)
        {
            var message = default(string);
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    var taskId = TaskService.CreateTaskRiskTarget(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.DirectFormat(message);
        }

        public ActionResult UpdateRiskTargetTask(TaskBO item)
        {
            var message = default(string);
            var result = false;

            try
            {
                TaskService.Update(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.DirectFormat(message);
        }


        public ActionResult DeleteTask(Guid Id)
        {
            var message = default(string);
            var result = false;

            try
            {
                RiskTargetTaskService.DeleteTask(Id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.DirectFormat(message);
        }
	}
}