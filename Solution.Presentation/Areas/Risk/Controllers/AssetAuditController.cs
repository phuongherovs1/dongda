﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Risk.Controllers
{
    public class AssetAuditController : FrontController
    {
        //
        // GET: /Risk/AssetAudit/
        public ActionResult GetData(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = RiskAssetAuditService.GetAll(pageIndex, pageSize, out count);
            return this.Store(data, count);
        }
        public ActionResult GetCriteria(StoreParameter parame)
        {
            var data = iDAS.Presentation.Common.Utilities.GetSelectItemToObject(iDAS.Service.Common.Resource.GetAcceptCriteriaRisk());
            return this.Store(data);
        }
        public ActionResult Form(string id = default(string))
        {
            RiskAssetAuditBO model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new RiskAssetAuditBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = RiskAssetAuditService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(RiskAssetAuditBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskAssetAuditService.Insert(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(RiskAssetAuditBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskAssetAuditService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskAssetAuditService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}