﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Risk.Controllers
{
    public class HandleController : FrontController
    {
        //
        // GET: /Risk/Handle/
        public ActionResult GetData(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = RiskHandleService.GetAll(pageIndex, pageSize, out count);
            return this.Store(data, count);
        }
        public ActionResult GetMethod(StoreParameter parame)
        {
            var data = iDAS.Presentation.Common.Utilities.GetSelectItemToObject(iDAS.Service.Common.Resource.GetMethodHandleRisk());
            return this.Store(data);
        }
        public ActionResult Form(string id = default(string))
        {
            RiskHandleBO model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new RiskHandleBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = RiskHandleService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult Report(string id = default(string))
        {
            var model = RiskHandleService.GetById(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(RiskHandleBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskHandleService.Insert(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(RiskHandleBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskHandleService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}