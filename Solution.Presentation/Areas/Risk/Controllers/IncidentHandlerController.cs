﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Risk.Controllers
{
    public class IncidentHandlerController : FrontController
    {
        //
        // GET: /Risk/IncidentHandler/
        public ActionResult Index(string id = default(string))
        {
            var model = RiskIncidentService.GetById(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult Form(string id = default(string), string riskIncidentId = default(string))
        {
            RiskIncidentHandlerBO model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new RiskIncidentHandlerBO();
                model.RiskIncidentId = new Guid(riskIncidentId);
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = RiskIncidentHandlerService.GetById(new Guid(id.ToString()));
                var department = DepartmentService.GetById(model.DepartmentId.HasValue ? model.DepartmentId.Value : Guid.Empty);
                model.DepartmentName = department != null ? department.Name : string.Empty;
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param, string incidentId = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = RiskIncidentHandlerService.GetAll(pageIndex, pageSize, out count, incidentId).ToList();
            return this.Store(data, count);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(RiskIncidentHandlerBO item, string cbTreeHandlerDepartment)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskIncidentHandlerService.Insert(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(RiskIncidentHandlerBO item, string cbTreeHandlerDepartment)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskIncidentHandlerService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskIncidentHandlerService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}