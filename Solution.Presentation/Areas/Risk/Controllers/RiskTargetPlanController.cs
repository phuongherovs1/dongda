﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Risk.Controllers
{
    public class RiskTargetPlanController : FrontController
    {
        //
        // GET: /Risk/RiskTargetPlan/
        public ActionResult Index(string departmentId = default(string), string RiskTargetId = default(string))
        {
            var model = new RiskTargetPlanBO() { };
            if (departmentId.Contains('_'))
            {
                string[] DepartmentIdsl = departmentId.Trim().Split('_');
                departmentId = DepartmentIdsl[1];
            }
            else if (string.IsNullOrEmpty(departmentId))
            {
                var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
                if (department != null)
                    departmentId = department.Id.ToString();
            }
            model.DepartmentId = new Guid(departmentId);
            model.RiskTargetId = new Guid(RiskTargetId);
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        public ActionResult Form(string Id = default(string),string departmentId = default(string), string RiskTargetId = default(string))
        {
            if (Id == default(string))
            {
                var model = new RiskTargetPlanBO() { };
                model.DepartmentId = new Guid(departmentId);
                model.RiskTargetId = new Guid(RiskTargetId);
                ViewData.Add("Operation", Common.Operation.Create);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            {
                var model = RiskTargetPlanService.GetById(iDAS.Service.Common.Utilities.ConvertToGuid(Id));
                ViewData.Add("Operation", Common.Operation.Update);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };

            }
        }


        public ActionResult GetDataRiskTargetPlan(StoreRequestParameters parameter, string RiskTargetId = default(string), string filterName = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = RiskTargetPlanService.GetDataRiskTargetPlan(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(RiskTargetId), filterName).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult Create(RiskTargetPlanBO item)
        {
            var result = false;
            try
            {
                RiskTargetPlanService.Insert(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult Update(RiskTargetPlanBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskTargetPlanService.Update(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }


        public ActionResult Delete(Guid Id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskTargetPlanService.Delete(Id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

	}
}