﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Risk.Controllers
{
    public class StatisticalController : FrontController
    {
        //
        // GET: /Risk/Statistical/
        public StoreResult GetDataChartArea()
        {
            var data = new object[]
            {
                new { month = "7/2016", data1 = 40, data2 =40, data3=90},
                new { month = "8/2016", data1 = 20, data2 =20, data3=80 },
                new { month = "9/2016", data1 = 69, data2 =90, data3=70 },
                new { month = "10/2016", data1 = 58, data2 =70, data3=60 },
                new { month = "11/2016", data1 = 38, data2 =0, data3=90 },
                new { month = "12/2016", data1 = 77, data2 =80, data3=30 },
                new { month = "1/2017", data1 = 46, data2 =60, data3=20 },
                new { month = "2/2017", data1 = 86, data2 =50, data3=10 },
                new { month = "3/2017", data1 = 15, data2 =30, data3=9 },
                new { month = "4/2017", data1 = 19, data2 =20, data3=8 },
                new { month = "5/2017", data1 = 75, data2 =30, data3=98 },
                new { month = "6/2017", data1 = 15, data2 =10, data3=96 }   
            };
            return new StoreResult(data);
        }
        public StoreResult GetDataPie()
        {
            return new StoreResult(GenerateData(3));
        }
        public StoreResult GetDataChartColumn()
        {
            var data = new List<ChartStatisticalBO>(8);
            Random random = new Random();
            double p = (random.NextDouble() * 11) + 1;
            for (int i = 0; i < 8; i++)
            {
                data.Add(new ChartStatisticalBO
                {
                    Name = "Phòng ban " + i,
                    Data1 = Math.Floor(Math.Max(random.NextDouble() * 100, 20)),
                });
            }
            return new StoreResult(data);
        }
        public class ChartStatisticalBO
        {
            public string Name
            {
                get;
                set;
            }

            public double Data1
            {
                get;
                set;
            }
        }
        public static List<ChartStatisticalBO> GenerateData(int n, int floor)
        {
            List<ChartStatisticalBO> data = new List<ChartStatisticalBO>(n);
            Random random = new Random();
            double p = (random.NextDouble() * 11) + 1;
            string name = "";
            for (int i = 0; i < n; i++)
            {
                if (i == 0)
                    name = "Chưa xử lý";
                else if (i == 1)
                    name = "Đang xử lý";
                else if (i == 2)
                    name = "Đã xử lý";
                data.Add(new ChartStatisticalBO
                {
                    Name = name + " (" + Math.Floor(Math.Max(random.NextDouble() * 100, floor)) + "%)",
                    Data1 = Math.Floor(Math.Max(random.NextDouble() * 100, floor))
                });
            }
            return data;
        }
        public static List<ChartStatisticalBO> GenerateData(int n)
        {
            return GenerateData(n, 20);
        }
	}
}