﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Risk.Controllers
{
    public class RiskTargetController : FrontController
    {
        public ActionResult getlistyear(string departmentId = default(string))
        {

            if (departmentId != default(string))
            {
                if (departmentId.Contains('_'))
                {
                    string[] DepartmentIdsl = departmentId.Trim().Split('_');
                    departmentId = DepartmentIdsl[1];
                }
            }
            var year = RiskTargetService.GetYear(new Guid(departmentId));
            return this.Store(year);
        }
        //
        // GET: /Risk/RiskTarget/
        public ActionResult Target(string containerId)
        {

            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;

            }
            return new Ext.Net.MVC.PartialViewResult
            {

                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Target",
                WrapByScriptTag = false
            };
        }
        public ActionResult Policy(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Policy",
                WrapByScriptTag = false
            };
        }
        public ActionResult Scene(string containerId)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Scene",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        public ActionResult Form(string id = default(string), bool IsTarget = false, bool IsPolicy = false, bool IsScene = false, string departmentId = default(string))
        {
            RiskTargetBO model = null;
            if (id == default(string))
            {
                if (departmentId != default(string))
                {
                    if (departmentId.Contains('_'))
                    {
                        string[] DepartmentIdsl = departmentId.Trim().Split('_');
                        departmentId = DepartmentIdsl[1];
                    }

                    model = RiskTargetService.CreateDefault(new Guid(departmentId));
                    model.IsTarget = IsTarget;
                    model.IsPolicy = IsPolicy;
                    model.IsScene = IsScene;
                }
                else
                {
                    model = new RiskTargetBO() { IsTarget = IsTarget, IsPolicy = IsPolicy, IsScene = IsScene };
                }

                ViewData.Add("Operation", Common.Operation.Create);

            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = RiskTargetService.GetdatabyId(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #region Method
        [ValidateInput(false)]
        public ActionResult GetDataPolicy(StoreRequestParameters param)
        {
            var data = RiskTargetService.GetPolicy();
            return this.Store(data);
        }
        public ActionResult GetDataTarget(StoreRequestParameters param, string departmentId = default(string), string Year = default(string))
        {
            if (departmentId.Contains('_'))
            {
                string[] DepartmentIdsl = departmentId.Trim().Split('_');
                departmentId = DepartmentIdsl[1];
            }
            else if(string.IsNullOrEmpty(departmentId))
            {
                var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
                if (department != null)
                    departmentId = department.Id.ToString();
            }
            object data = null;
            if (!string.IsNullOrEmpty(Year))
                data = RiskTargetService.GetTarget(new Guid(departmentId), int.Parse(Year));
            else
                data = RiskTargetService.GetTarget(new Guid(departmentId), DateTime.Now.Year);
            return this.Store(data);
        }
        public ActionResult GetDataScene(StoreRequestParameters param, string departmentId = default(string), string Year = default(string))
        {
            if (departmentId.Contains('_'))
            {
                string[] DepartmentIdsl = departmentId.Trim().Split('_');
                departmentId = DepartmentIdsl[1];
            }
            else if (string.IsNullOrEmpty(departmentId))
            {
                var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
                if (department != null)
                    departmentId = department.Id.ToString();
            }
            object data = null;
            if (!string.IsNullOrEmpty(Year))
                data = RiskTargetService.GetScene(new Guid(departmentId),int.Parse(Year));
            else
                data = RiskTargetService.GetScene(new Guid(departmentId),DateTime.Now.Year);
            return this.Store(data);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(RiskTargetBO item)
        {
            var result = false;
            try
            {
                RiskTargetService.Insert(item);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(RiskTargetBO item)
        {
            var result = false;
            try
            {
                RiskTargetService.Update(item);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            try
            {
                RiskTargetService.Delete(id);
                result = true;
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}