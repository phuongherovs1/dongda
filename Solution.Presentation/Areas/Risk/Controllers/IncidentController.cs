﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;
using systemIO = System.IO;
using System.Data;
using System.IO;
using System.Dynamic;
using systemnet = System.Net;

namespace iDAS.Presentation.Areas.Risk.Controllers
{
    public class IncidentController : FrontController
    {
        //
        // GET: /Risk/Incident/
        public ActionResult Form(string id = default(string))
        {
            RiskIncidentBO model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new RiskIncidentBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = RiskIncidentService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param, string departmentId = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = RiskIncidentService.GetAll(pageIndex, pageSize, out count, departmentId).ToList();
            return this.Store(data, count);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(RiskIncidentBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskIncidentService.Insert(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(RiskIncidentBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskIncidentService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskIncidentService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        #region ImportData
        #region Các hàm cứng không sửa đổi
        public ActionResult Import()
        {
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Import", ViewData = ViewData };
        }
        private DataTable ReadFileExcel(string directory)
        {
            FileStream fStream = new FileStream(directory, FileMode.Open);
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook(fStream);
            Aspose.Cells.Worksheet worksheet = workbook.Worksheets[0];
            DataTable datatable = worksheet.Cells.ExportDataTable(0, 0, worksheet.Cells.Rows.Count, worksheet.Cells.Columns.Count, true);
            fStream.Close();
            return datatable;
        }
        public ActionResult DataObjectImport(StoreRequestParameters parameters, string direction)
        {
            var result = new List<object>();
            if (!string.IsNullOrEmpty(direction))
            {
                var table = ReadFileExcel(Server.MapPath(direction));
                for (var i = 0; i < table.Rows.Count; i++)
                {
                    var obj = new ExpandoObject() as IDictionary<string, object>;
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        obj.Add("Column" + j, table.Rows[i][j]);
                    }
                    result.Add(obj);
                }
            }
            return this.Store(result);
        }
        public ActionResult SelectImportFile()
        {
            var fileUploadField = X.GetCmp<FileUploadField>("FileImportField");

            var direction = Common.Resource.FileImportUrl + Guid.NewGuid().ToString() + ".xlsx";
            if (fileUploadField.HasFile)
            {
                if (fileUploadField.PostedFile.ContentLength < 300 * 1024 + 1)
                {
                    if (fileUploadField.FileName.Split('.').LastOrDefault().ToUpper().Equals("XLSX") || fileUploadField.FileName.Split('.').LastOrDefault().ToUpper().Equals("XLS"))
                    {
                        if (!systemIO.File.Exists(Server.MapPath(direction)))
                        {
                            fileUploadField.PostedFile.SaveAs(Server.MapPath(direction));
                        }
                    }
                    else
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING,
                            Title = "Thông báo",
                            Message = "Hệ thống chỉ hỗ trợ file có đuôi .xls và .xlsx!"
                        });
                        return this.Direct();
                    }
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = "Chỉ cho phép dung lượng import tối đa là 300KB!"
                    });
                    return this.Direct();
                }
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Không có thông tin cho file upload!"
                });
            }
            return this.Direct(direction);
        }
        [HttpGet]
        public ActionResult SettingImport(string direction)
        {
            ViewData["Direction"] = direction;
            try
            {
                if (direction != null)
                {
                    var table = ReadFileExcel(Server.MapPath(direction));
                    ViewData["NumberColumn"] = table.Columns.Count;
                }
                else
                {
                    ViewData["NumberColumn"] = 0;
                }
                return new Ext.Net.MVC.PartialViewResult { ViewName = "SettingImport", ViewData = ViewData };
            }
            catch (Exception)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Vui lòng lựa chọn tệp cần nhập liệu vào hệ thống!"
                });
                return this.Direct();
            }
        }
        #endregion

        #region Hàm thay đổi với các đối tượng bảng khác nhau
        [HttpPost]
        public ActionResult SettingImport(RiskIncidentImport data, Guid departmentId, string direction)
        {
            try
            {
                if (!string.IsNullOrEmpty(direction))
                {
                    var table = ReadFileExcel(Server.MapPath(direction));
                    var objectImport = new List<RiskIncidentBO>();
                    for (var i = 0; i < table.Rows.Count; i++)
                    {
                        objectImport.Add(new RiskIncidentBO
                        {
                            DepartmentId = departmentId,
                            CodeIncident = table.Rows[i][data.CodeIncident].ToString(),
                            Date = DateTime.Parse(table.Rows[i][data.Date].ToString()),
                            Description = table.Rows[i][data.Description].ToString(),
                            Reason = table.Rows[i][data.Reason].ToString(),
                            AnotherIdea = table.Rows[i][data.AnotherIdea].ToString(),
                            Conclude = table.Rows[i][data.Conclude].ToString(),
                        });
                    }
                    var numberInportFail = RiskIncidentService.Import(objectImport);
                    systemIO.File.Delete(Server.MapPath(direction));
                    if (numberInportFail > 0)
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO,
                            Title = "Thông báo",
                            Message = "Có " + numberInportFail + " bản ghi không được nhập liệu do mã đã tồn tại. Vui lòng kiểm tra lại dữ liệu trước khi nhập liệu tự động!"
                        });
                        return this.Direct();
                    }
                    else
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO,
                            Title = "Thông báo",
                            Message = "Nhập liệu vào hệ thống thành công!"
                        });
                        return this.Direct();
                    }
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = "Vui lòng lựa chọn tệp cần nhập liệu vào hệ thống!"
                    });
                    return this.Direct();
                }
            }
            catch
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Có lỗi trong quá trình nhập dữ liệu!"
                });
                return this.Direct();
            }
        }
        public ActionResult Download()
        {
            try
            {
                var fileName = "template_import_riskincident.xlsx";
                var fullpath = AppDomain.CurrentDomain.BaseDirectory + "/Upload/TemplateImport/" + fileName;
                byte[] filedata = systemIO.File.ReadAllBytes(fullpath);
                string contentType = MimeMapping.GetMimeMapping(fullpath);
                var cd = new systemnet.Mime.ContentDisposition
                {
                    FileName = fileName,
                    Inline = true
                };
                Response.AddHeader("Content-Disposition", cd.ToString());
                return File(filedata, contentType);
            }
            catch (Exception)
            {
                return new EmptyResult();
            }
        }
        #endregion
        #endregion
    }
}