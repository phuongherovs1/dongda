﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using System.Configuration;

namespace iDAS.Presentation.Areas.Risk.Controllers
{
    public class PlanAuditController : FrontController
    {
        //
        // GET: /Risk/PlanAudit/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Form(string id = default(string))
        {
            RiskPlanAuditBO model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new RiskPlanAuditBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = RiskPlanAuditService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult GetAssetReview(StoreRequestParameters param, Guid? cateId, string stroperator = default(string), decimal? assetValue = null)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = AssetResourceRelateService.GetAssetReview(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString, pageIndex, pageSize, out count, cateId, stroperator, assetValue).ToList();
            return this.Store(data, count);
        }
        public ActionResult GetCate(StoreRequestParameters param)
        {
            var data = new List<AssetGroupBO>();
            data.Add(new AssetGroupBO { Id = Guid.Empty, Name = "Tất cả" });
            data.AddRange(AssetGroupService.GetAll().ToList());
            return this.Store(data);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(RiskPlanAuditBO item, string assetResourceRelateIds = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskPlanAuditService.Save(item, assetResourceRelateIds);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(RiskPlanAuditBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskPlanAuditService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}