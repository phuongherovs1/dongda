﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Risk.Controllers
{
    public class LikelihoodController : FrontController
    {
        #region View
        public ActionResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,

                WrapByScriptTag = false
            };
        }
        public ActionResult Form(string id = default(string))
        {
            RiskLikelihoodBO model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new RiskLikelihoodBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = RiskLikelihoodService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion
        #region Method
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = RiskLikelihoodService.Page(out count, pageIndex, pageSize).ToList();
            return this.Store(data, count);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(RiskLikelihoodBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskLikelihoodService.Insert(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(RiskLikelihoodBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskLikelihoodService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    RiskLikelihoodService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
	}
}