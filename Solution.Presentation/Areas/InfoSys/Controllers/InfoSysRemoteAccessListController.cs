﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.InfoSys.Controllers
{
    public class InfoSysRemoteAccessListController : Controller
    {
        protected IInfoSysService infoSysService;
        public InfoSysRemoteAccessListController(IInfoSysService _infoSysService)
        {
            infoSysService = _infoSysService;
        }
        public ActionResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
            };
        }
        public ActionResult Form(string id = default(string))
        {
            InfoSysBO model;
            if (id == default(string))
            {
                model = new InfoSysBO();
                ViewData.Add("Operation", Common.Operation.Create);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = infoSysService.GetInfoSysDetail(Convert.ToInt32(id));
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
        }

        public ActionResult Create(InfoSysBO item)
        {
            return this.Direct(result: infoSysService.Insert(item));
        }

        public ActionResult Update(InfoSysBO item)
        {
            return this.Direct(result: infoSysService.Update(item));
        }

        public ActionResult GetAllInfoSystems()
        {
            return this.Store(infoSysService.GetAllInfoSystems());
        }

        public ActionResult GetInfoSystems(StoreRequestParameters param, string filterName = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = infoSysService.GetInfoSystems(pageIndex, pageSize, out count);

            //if (!string.IsNullOrEmpty(filterName))
            //    data = InfoSysService.GetByFilterName(pageIndex, pageSize, filterName, out count);
            return this.Store(data, count);
        }
    }
}