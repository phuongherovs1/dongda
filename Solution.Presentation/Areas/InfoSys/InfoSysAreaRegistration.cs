﻿using System.Web.Mvc;

namespace iDAS.Presentation.Areas.InfoSys
{
    public class InfoSysAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "InfoSys";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "InfoSys_default",
                "InfoSys/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}