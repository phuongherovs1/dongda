﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using Ext.Net;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class TargetController : FrontController
    {
        public ActionResult GetByDepartment(StoreRequestParameters parameter, string departmentId, string filterName, Guid? issueId, DateTime? dateSearch, iDAS.Service.Common.Resource.TargetStatus status)
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = 20;
                var count = 0;
                object data = null;
                if (!string.IsNullOrEmpty(departmentId))
                    data = QualityTargetService.GetByDepartment(pageIndex, pageSize, out count, departmentId, issueId, status, filterName, dateSearch).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        /// <summary>
        /// Form thêm mới/sửa danh mục
        /// </summary>
        /// <param name="id"></param>
        /// <param name="IsNoParent"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public ActionResult Form(string id = default(string), string departmentId = default(string))
        {
            try
            {
                ViewData["TargetType"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetTargetType());
                ViewData["TargetValue"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetTargetValue());
                ViewData["TargetDestination"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetTargetDestination());
                ViewData["TargetResponsibility"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetTargetResponsibility());
                ViewData["TargetExpression"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetTargetExpression());
                QualityTargetBO model = null;
                model = QualityTargetService.GetDetail(id, departmentId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                return this.Direct();
            }
        }

        /// <summary>
        /// Form phê duyệt mục tiêu
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public ActionResult FormApproval(string id = default(string), string departmentId = default(string))
        {
            try
            {
                ViewData["TargetType"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetTargetType());
                ViewData["TargetValue"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetTargetValue());
                ViewData["TargetDestination"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetTargetDestination());
                ViewData["TargetResponsibility"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetTargetResponsibility());
                ViewData["TargetExpression"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetTargetExpression());
                ViewData["QualityPlanStatus"] = iDAS.Service.Common.Resource.GetQualityPlanStatus();
                QualityTargetBO model = null;
                model = QualityTargetService.GetDetail(id, departmentId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                return this.Direct();
            }
        }

        /// <summary>
        /// Form cập nhật kết quả thực hiện
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public ActionResult FormUpdateResult(string id = default(string), string departmentId = default(string))
        {
            try
            {
                QualityTargetBO model = null;
                model = QualityTargetService.GetDetail(id, departmentId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                return this.Direct();
            }
        }

        /// <summary>
        /// Thêm mới danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(QualityTargetBO item, string jsonData = default(string), string strExpression = default(string))
        {
            var result = false;
            try
            {
                if (!string.IsNullOrEmpty(strExpression) && strExpression != "[]")
                    item.TargetValueExpression = strExpression;
                QualityTargetService.InsertTarget(item, jsonData);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Cập nhật kết quả thực hiện
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateResult(QualityTargetBO item)
        {
            var result = false;
            try
            {
                QualityTargetService.UpdateResult(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Phê duyệt mục tiêu
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Approval(QualityTargetBO item)
        {
            var result = false;
            try
            {
                QualityTargetService.Approval(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityTargetService.DeleteTarget(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.TargetCategoryExitsTarget || ex is iDAS.Service.Common.AccessDenyException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Hủy mục tiêu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DestroyTarget(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityTargetService.DestroyTarget(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.TargetCategoryExitsTarget || ex is iDAS.Service.Common.AccessDenyException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Khôi phục mục tiêu đã hủy
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RevertTarget(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityTargetService.RevertTarget(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.TargetCategoryExitsTarget || ex is iDAS.Service.Common.AccessDenyException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}