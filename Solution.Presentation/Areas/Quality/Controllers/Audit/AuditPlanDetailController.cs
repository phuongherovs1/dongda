﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class AuditPlanDetailController : FrontController
    {
        //
        // GET: /Quality/AuditPlanDetail/
        #region View
        public ActionResult TotalAudit(string qualityAuditPlanDetailId = default(string))
        {
            var model = QualityAuditPlanDetailService.GetById(new Guid(qualityAuditPlanDetailId));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult Form(string id = default(string))
        {
            QualityAuditPlanDetailBO model = null;
            ViewData["RoleAudit"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetRoleAudit());
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new QualityAuditPlanDetailBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = QualityAuditPlanDetailService.GetById(new Guid(id.ToString()));
            }
            if (model.Status == (int)iDAS.Service.Common.Resource.PlanAuditStatus.New || model.Status == (int)iDAS.Service.Common.Resource.PlanAuditStatus.NotApprove)
            {
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "Form", ViewData = ViewData };
            }
            else
            {
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "Detail", ViewData = ViewData };
            }
        }
        public ActionResult Add(string planId = default(string))
        {
            var result = false;
            try
            {
                var plan = QualityAuditPlanService.GetById(new Guid(planId));
                QualityAuditPlanDetailService.Copy(new Guid(planId), plan.TimeValue, plan.UnitTime, true);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult Report(string id = default(string))
        {
            var model = QualityAuditPlanDetailService.GetReportById(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult Detail(string id = default(string))
        {
            ViewData["RoleAudit"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetRoleAudit());
            var model = QualityAuditPlanDetailService.GetById(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult Approval(string id = default(string))
        {
            ViewData["RoleAudit"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetRoleAudit());
            var model = QualityAuditPlanDetailService.GetById(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult Audit(string id = default(string))
        {
            ViewData["RoleAudit"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetRoleAudit());
            var model = QualityAuditPlanDetailService.GetById(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion
        #region Method
        public ActionResult GetData(StoreRequestParameters param, string planId = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = QualityAuditPlanDetailService.GetAll(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(planId));
            return this.Store(data, count);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(QualityAuditPlanDetailBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityAuditPlanDetailService.Insert(item);
                    if (item.IsSend == true)
                        Notify(TitleResourceNotifies.ApprovalQualiyPlanAudit, item.Name, item.EmployeeApproval.Id, UrlResourceNotifies.ApprovalAuditPlanDetail, new { id = item.Id });
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(QualityAuditPlanDetailBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityAuditPlanDetailService.Update(item);
                    if (item.IsSend == true)
                        Notify(TitleResourceNotifies.ApprovalQualiyPlanAudit, item.Scope, item.EmployeeApproval.Id, UrlResourceNotifies.ApprovalAuditPlanDetail, new { id = item.Id });
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveApproval(QualityAuditPlanDetailBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityAuditPlanDetailService.Approval(item);
                    var qapl = QualityAuditPlanDetailService.GetById(item.Id);
                    Notify(TitleResourceNotifies.ApprovalPlanAudit, item.Scope, item.CreateBy.HasValue ? item.CreateBy.Value : qapl.UpdateBy.Value, UrlResourceNotifies.AuditPlanDetail, new { id = item.Id });
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityAuditPlanDetailService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Destroy(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityAuditPlanDetailService.Destroy(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveReport(QualityAuditPlanDetailBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityAuditPlanDetailService.Report(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}