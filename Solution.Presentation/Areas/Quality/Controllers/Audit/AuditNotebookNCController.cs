﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class AuditNotebookNCController : FrontController
    {
        //
        // GET: /Quality/AuditNotebookNC/
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param, string notebookId = default(string))
        {
            var ncIds = QualityAuditNotebookNCService.GetNC(new Guid(notebookId));
            var data = QualityNCService.GetByIds(ncIds);
            return this.Store(data);
        }
        public ActionResult GetTotalData(StoreRequestParameters parameter, string qualityAuditPlanDetailId = default(string))
        {
            var pageIndex = parameter.Page;
            var pageSize = parameter.Limit;
            var count = 0;
            var ncIds = QualityAuditNotebookNCService.GetNCByPlanDetail(new Guid(qualityAuditPlanDetailId));
            var data = QualityNCService.GetDataByIds(ncIds, pageIndex, pageSize, out count);
            return this.Store(data, count);
        }
        public ActionResult GetTotalNC(StoreRequestParameters parameter, string qualityAuditPlanDetailId = default(string))
        {
            var pageIndex = parameter.Page;
            var pageSize = parameter.Limit;
            var count = 0;
            var ncIds = QualityAuditNotebookNCService.GetNCIsConfirmByPlanDetail(new Guid(qualityAuditPlanDetailId));
            var data = QualityNCService.GetTotalNCs(ncIds, pageIndex, pageSize, out count);
            return this.Store(data, count);
        }

        public ActionResult ConfirmNC(string qualityNCSelecteds = default(string))
        {
            object result = null;
            try
            {
                if (qualityNCSelecteds == default(string) || qualityNCSelecteds.Equals("[]"))
                {
                    iDAS.Presentation.Common.Utilities.ShowMessageBox("Thông báo", "Bạn phải chọn sự không phù hợp cần xác nhận", MessageBox.Icon.WARNING);
                    result = false;
                }
                else
                {
                    QualityNCService.ConfirmNCs(qualityNCSelecteds);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
    }
}