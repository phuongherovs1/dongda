﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class AuditNotebookController : FrontController
    {
        //
        // GET: /Quality/AuditNotebook/
        #region View
        public ActionResult Form(string notebookId = default(string), string programId = default(string))
        {
            QualityAuditNotebookBO model = null;
            ViewData.Add("Operation", Common.Operation.Update);
            var program = QualityAuditProgramService.GetById(new Guid(programId));
            ViewData.Add("AuditName", program.ObjectName);
            if (notebookId == default(string))
            {
                model = new QualityAuditNotebookBO() { QualityAuditProgramId = new Guid(programId), AuditBy = program.AuditBy, AuditAt = DateTime.Now };
                model.Id = QualityAuditNotebookService.Insert(model);
                QualityAuditPlanDetailService.ChangeStatus(program.QualityAuditPlanDetailId, (int)iDAS.Service.Common.Resource.PlanAuditStatus.Auditing);
            }
            else
            {
                model = QualityAuditNotebookService.GetById(new Guid(notebookId.ToString()));
                model.QualityAuditProgramId = new Guid(programId);
                model.AuditBy = program.AuditBy;
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult Detail(string notebookId = default(string), string programId = default(string))
        {
            QualityAuditNotebookBO model = null;
            var program = QualityAuditProgramService.GetById(new Guid(programId));
            ViewData.Add("AuditName", program.ObjectName);
            model = QualityAuditNotebookService.GetById(new Guid(notebookId.ToString()));
            model.QualityAuditProgramId = new Guid(programId);
            model.AuditBy = program.AuditBy;
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion
        #region Method
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param, string programId = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = QualityAuditNotebookService.GetAll(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(programId));
            return this.Store(data, count);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(QualityAuditNotebookBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityAuditNotebookService.Insert(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(QualityAuditNotebookBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityAuditNotebookService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityAuditNotebookService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}