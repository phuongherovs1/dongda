﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class AuditPlanController : FrontController
    {
        //
        // GET: /Quality/Plan/
        public ActionResult Index()
        {
            return PartialView();
        }

        public ActionResult Form(string id = default(string))
        {
            QualityAuditPlanBO model = null;
            ViewData["UnitTime"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetUnitTime());
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new QualityAuditPlanBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = QualityAuditPlanService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult GetISOStandard(string planId = default(string))
        {
            var data = ISOStandardService.GetByPlan(iDAS.Service.Common.Utilities.ConvertToGuid(planId));
            return this.Store(data);
        }
        public ActionResult GetData()
        {
            var data = QualityAuditPlanService.GetAll();
            return this.Store(data);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(QualityAuditPlanBO item, string qualityAuditPlanStandards = default(string))
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    if (qualityAuditPlanStandards == default(string) || qualityAuditPlanStandards.Equals("[]"))
                    {
                        iDAS.Presentation.Common.Utilities.ShowMessageBox("Thông báo", "Bạn phải chọn tiêu chuẩn đánh giá", MessageBox.Icon.WARNING);
                        result = false;
                    }
                    else
                    {
                        QualityAuditPlanService.InsertPlan(item, qualityAuditPlanStandards);
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(QualityAuditPlanBO item, string qualityAuditPlanStandards = default(string))
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    if (qualityAuditPlanStandards == default(string) || qualityAuditPlanStandards.Equals("[]"))
                    {
                        iDAS.Presentation.Common.Utilities.ShowMessageBox("Thông báo", "Bạn phải chọn tiêu chuẩn đánh giá", MessageBox.Icon.WARNING);
                        result = false;
                    }
                    else
                    {
                        QualityAuditPlanService.UpdatePlan(item, qualityAuditPlanStandards);
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityAuditPlanService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}