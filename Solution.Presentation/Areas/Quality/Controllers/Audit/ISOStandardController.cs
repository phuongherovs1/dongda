﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class ISOStandardController : FrontController
    {
        //
        // GET: /Quality/ISOStandard/
        #region View
        public ActionResult Index()
        {
            return PartialView();
        }
        public ActionResult Form(string id = default(string))
        {
            ISOStandardBO model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new ISOStandardBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = ISOStandardService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion
        #region Method

        public ActionResult GetData(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = ISOStandardService.Page(out count, pageIndex, pageSize);
            return this.Store(data, count);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(ISOStandardBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ISOStandardService.Insert(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(ISOStandardBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ISOStandardService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ISOStandardService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}