﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class AuditProgramController : FrontController
    {
        //
        // GET: /Quality/AuditProgram/
        #region View
        public ActionResult ProgramForm(string programId = default(string), string qualityAuditPlanDetailId = default(string))
        {
            QualityAuditProgramBO model = null;
            var detail = QualityAuditPlanDetailService.GetById(new Guid(qualityAuditPlanDetailId));
            if (programId == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new QualityAuditProgramBO() { QualityAuditPlanDetailId = new Guid(qualityAuditPlanDetailId), QualityAuditPlanId = detail.QualityAuditPlanId };
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = QualityAuditProgramService.GetById(new Guid(programId.ToString()));
                model.QualityAuditPlanId = detail.QualityAuditPlanId;
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion
        #region Method
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param, string qualityAuditPlanDetailId = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = QualityAuditProgramService.GetAll(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(qualityAuditPlanDetailId));
            return this.Store(data, count);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(QualityAuditProgramBO item, string qualityAuditProgramTerms = default(string), string qualityAuditProgramDepartments = default(string))
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    if (qualityAuditProgramTerms == default(string) || qualityAuditProgramTerms.Equals("[]"))
                    {
                        iDAS.Presentation.Common.Utilities.ShowMessageBox("Thông báo", "Bạn phải chọn điều khoản đánh giá", MessageBox.Icon.WARNING);
                        result = false;
                    }
                    else if (qualityAuditProgramDepartments == default(string) || qualityAuditProgramDepartments.Equals("[]"))
                    {
                        iDAS.Presentation.Common.Utilities.ShowMessageBox("Thông báo", "Bạn phải chọn phòng ban/bộ phận đánh giá", MessageBox.Icon.WARNING);
                        result = false;
                    }
                    else
                    {
                        QualityAuditProgramService.InsertProgram(item, qualityAuditProgramTerms, qualityAuditProgramDepartments);
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(QualityAuditProgramBO item, string qualityAuditProgramTerms = default(string), string qualityAuditProgramDepartments = default(string))
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    if (qualityAuditProgramTerms == default(string) || qualityAuditProgramTerms.Equals("[]"))
                    {
                        iDAS.Presentation.Common.Utilities.ShowMessageBox("Thông báo", "Bạn phải chọn điều khoản đánh giá", MessageBox.Icon.WARNING);
                        result = false;
                    }
                    else if (qualityAuditProgramDepartments == default(string) || qualityAuditProgramDepartments.Equals("[]"))
                    {
                        iDAS.Presentation.Common.Utilities.ShowMessageBox("Thông báo", "Bạn phải chọn phòng ban/bộ phận đánh giá", MessageBox.Icon.WARNING);
                        result = false;
                    }
                    else
                    {
                        QualityAuditProgramService.UpdateProgram(item, qualityAuditProgramTerms, qualityAuditProgramDepartments);
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityAuditProgramService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}