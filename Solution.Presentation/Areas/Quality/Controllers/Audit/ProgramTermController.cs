﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class ProgramTermController : FrontController
    {
        //
        // GET: /Quality/ProgramTerm/
        public ActionResult GetData(StoreRequestParameters param, string planId = default(string), string programId= default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = ISOTermService.GetByISOStandards(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(planId), iDAS.Service.Common.Utilities.ConvertToGuid(programId));
            return this.Store(data, count);
        }
        
	}
}