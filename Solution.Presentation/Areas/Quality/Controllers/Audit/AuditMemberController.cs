﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class AuditMemberController : FrontController
    {
        //
        // GET: /Quality/AuditMember/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetData(StoreRequestParameters param, string qualityAuditPlanDetailId = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = QualityAuditMemberService.GetAll(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(qualityAuditPlanDetailId));
            return this.Store(data, count);
        }
        public ActionResult Create(string qualityAuditMembers = default(string))
        {
            object result = null;
            try
            {
                if (qualityAuditMembers == default(string) || qualityAuditMembers.Equals("[]"))
                {
                    iDAS.Presentation.Common.Utilities.ShowMessageBox("Thông báo", "Bạn phải chọn nhân sự tham gia đánh giá", MessageBox.Icon.WARNING);
                    result = false;
                }
                else
                {
                    QualityAuditMemberService.InsertMember(qualityAuditMembers);
                    result = true;
                }
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
        public DirectResult HandleChanges(string id, string roleType)
        {
            object result = null;
            try
            {
                if (int.Parse(roleType) == (int)iDAS.Service.Common.Resource.RoleAudit.Leader && QualityAuditMemberService.ExitsLeader(new Guid(id)))
                {
                    iDAS.Presentation.Common.Utilities.ShowMessageBox("Thông báo", "Bạn đã tồn tại trưởng nhóm đánh giá!", MessageBox.Icon.WARNING);
                    result = false;
                }
                else
                {
                    QualityAuditMemberService.UpdateRoleType(new Guid(id), int.Parse(roleType));
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
        public DirectResult HandleNotAvailable(string id, bool isNotAvailable)
        {
            object result = null;
            try
            {
                QualityAuditMemberService.UpdateNotAvailable(new Guid(id), isNotAvailable);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityAuditMemberService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}