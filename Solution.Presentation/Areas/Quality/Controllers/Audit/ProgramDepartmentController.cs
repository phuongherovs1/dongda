﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class ProgramDepartmentController : FrontController
    {
        //
        // GET: /Quality/ProgramDepartment/

        private Node createNodeDepartment(DepartmentBO department)
        {
            Node node = new Node();
            node.NodeID = department.Id.ToString();
            node.Text = " " + department.Name;
            node.IconCls = "x-fa fa-home";
            node.Checked = department.Checked;
            node.CustomAttributes.Add(new ConfigItem { Name = "ParentId", Value = department.ParentId.ToString(), Mode = ParameterMode.Value });
            node.Leaf = !department.IsParent;
            return node;
        }
        public ActionResult LoadData(string node, string programId = default(string))
        {
            try
            {
                NodeCollection nodes = new NodeCollection();
                var departmentID = node == "root" ? (Guid?)null : new Guid(Convert.ToString(node));
                var departments = DepartmentService.GetTreeDepartmentChecked(departmentID, new Guid(programId));
                foreach (var department in departments)
                {
                    nodes.Add(createNodeDepartment(department));
                }
                return this.Content(nodes.ToJson());
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }

        public ActionResult GetDepartmentAudit(string programId = default(string))
        {
            var departmentIds = QualityAuditProgramDepartmentService.GetDepartmentByProgram(new Guid(programId));
            var data = DepartmentService.GetByIds(departmentIds);
            return this.Store(data);
        }

    }
}