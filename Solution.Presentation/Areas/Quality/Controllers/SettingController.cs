﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class SettingController : FrontController
    {
        //
        // GET: /Quality/Setting/
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {

                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                WrapByScriptTag = false
            };
        }
	}
}