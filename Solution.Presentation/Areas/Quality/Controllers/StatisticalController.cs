﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class StatisticalController : FrontController
    {
        //
        // GET: /Quality/Statistical/
        public StoreResult GetDataQualityTargetStatusAnalytic(string departmentId = default(string))
        {

            var summary = QualitySummaryService.TargetStatusAnalytic(iDAS.Service.Common.Utilities.ConvertToGuid(departmentId));
            return new StoreResult(summary);

        }
        public StoreResult GetDataQualityNCAnalytic(string departmentId = default(string))
        {

            var summary = QualitySummaryService.QualityNCAnalytic(iDAS.Service.Common.Utilities.ConvertToGuid(departmentId));
            return new StoreResult(summary);

        }
	}
}