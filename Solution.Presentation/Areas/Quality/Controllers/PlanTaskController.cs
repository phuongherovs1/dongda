﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using Ext.Net;
using iDAS.Service;
using iDAS.Service.Common;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class PlanTaskController : FrontController
    {
        /// <summary>
        /// Form thêm mới/sửa đơn vị
        /// </summary>
        /// <param name="id"></param>
        /// <param name="alignTo"></param>
        /// <param name="renderTo"></param>
        /// <param name="storeId"></param>
        /// <returns></returns>
        public ActionResult FormTask(string id = default(string), string QualityPlanId = default(string))
        {
            var model = new TaskBO();
            if (!String.IsNullOrEmpty(id))
            {
                model = TaskService.GetFormItem(new Guid(id));
                model.QualityPlanId = iDAS.Service.Common.Utilities.ConvertToGuid(QualityPlanId);
            }
            else
                model.QualityPlanId = String.IsNullOrEmpty(QualityPlanId) ? Guid.Empty : iDAS.Service.Common.Utilities.ConvertToGuid(QualityPlanId);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        public ActionResult GetDataTaskByQualityPlan(StoreRequestParameters parameter, string QualityPlanId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = TaskService.GetDataTaskByQualityPlan(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(QualityPlanId)).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Thêm mới danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateTask(TaskBO item)
        {

            try
            {
                var taskId = TaskService.CreatePlanTask(item);
                item.Id = taskId;
                var TaskResource = TaskResourceService.Get(p => p.TaskId == item.Id).FirstOrDefault();
                var employees = EmployeeService.GetEmployeeGeneralInfo(TaskResource.EmployeeId.Value);
                item.EmployeePerformQuality = employees;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: item);
        }

        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityTaskService.DeleteTask(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.AccessDenyException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}