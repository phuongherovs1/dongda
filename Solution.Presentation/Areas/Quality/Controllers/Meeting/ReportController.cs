﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class ReportController : FrontController
    {

        public ActionResult Form(Guid id)
        {
            var model = QualityMeetingPlanReportService.GetById(id);
            return new Ext.Net.MVC.PartialViewResult { Model = model};
        }
        public ActionResult Save(QualityMeetingPlanReportBO data)
        {
            object result;
            try
            {
                QualityMeetingPlanReportService.SaveReport(data);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }

	}
}