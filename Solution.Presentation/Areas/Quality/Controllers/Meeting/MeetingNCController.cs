﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class MeetingNCController : FrontController
    {
        public ActionResult Index(Guid issueId, Guid meetingPlanReportIssueId)
        {
            ViewData["IssueId"] = issueId;
            ViewData["MeetingPlanReportIssueId"] = meetingPlanReportIssueId;
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }

        public ActionResult GetData(Guid issueId, Guid meetingPlanReportIssueId)
        {
            var data = QualityMeetingPlanNCService.GetByMeetingIssue(issueId, meetingPlanReportIssueId).ToList();
            return this.Store(data);
        }

        public ActionResult Form(Guid issueId, Guid meetingPlanReportIssueId)
        {
            try
            {
                var model = new QualityNCBO(); // QualityMeetingPlanNCService.GetFormItem(issueId, meetingPlanReportId);
                ViewData["IssueId"] = issueId;
                ViewData["MeetingPlanReportIssueId"] = meetingPlanReportIssueId;
                ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetQualityNCTypeStatus());
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult Save(QualityNCBO data, Guid issueId, Guid meetingPlanReportIssueId, string datajson)
        {
            object result;
            try
            {
                QualityMeetingPlanNCService.Save(data, issueId, meetingPlanReportIssueId, datajson);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }

    }
}