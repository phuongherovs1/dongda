﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers.Meeting
{
    public class DetailController : FrontController
    {
        //
        // GET: /Quality/Detail/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetData(Guid planId)
        {
            var data = QualityMeetingPlanDetailService.GetByPlanId(planId);
            return this.Store(data);
        }
	}
}