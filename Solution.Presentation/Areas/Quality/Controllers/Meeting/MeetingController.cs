﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;
namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class MeetingController : FrontController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var sources = iDAS.Service.Common.Resource.GetMeetingStatus();
            var listComboboxItems = new ListItemCollection();
            foreach (var item in sources)
            {
                listComboboxItems.Add(new ListItem() { Text = item.Value, Value = item.Key.ToString() });
            }
            ViewData["MeetingStatus"] = listComboboxItems;
            return PartialView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="status"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public ActionResult GetData(StoreRequestParameters param, List<int> status, string key)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = QualityMeetingPlanService.GetData(pageIndex, pageSize, out count, key, status).ToList();
            return this.Store(data, count);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Form(string id)
        {
            var listUnitTime = new ListItemCollection();
            listUnitTime.Add(new ListItem() { Value = Convert.ToInt16(Service.Common.Resource.UnitTime.Day).ToString(), Text = Service.Common.Resource.UnitTimeText.Day });
            listUnitTime.Add(new ListItem() { Value = Convert.ToInt16(Service.Common.Resource.UnitTime.Month).ToString(), Text = Service.Common.Resource.UnitTimeText.Month });
            listUnitTime.Add(new ListItem() { Value = Convert.ToInt16(Service.Common.Resource.UnitTime.Week).ToString(), Text = Service.Common.Resource.UnitTimeText.Week });
            listUnitTime.Add(new ListItem() { Value = Convert.ToInt16(Service.Common.Resource.UnitTime.Year).ToString(), Text = Service.Common.Resource.UnitTimeText.Year });
            var model = QualityMeetingPlanService.GetFormItem(id);
            ViewData["ListUnitTime"] = listUnitTime;
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetDepartment()
        {
            var data = DepartmentService.GetAll();
            return this.Store(data);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(QualityMeetingPlanBO data)
        {
            object result;
            try
            {
                QualityMeetingPlanService.Save(data);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Update(QualityMeetingPlanBO data)
        {
            object result;
            try
            {
                QualityMeetingPlanService.Save(data);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }
        
        /// <summary>
        /// Thực hiện họp
        /// </summary>
        /// <param name="meetingId"></param>
        /// <returns></returns>
        public ActionResult Perform(Guid meetingId)
        {
            var model = QualityMeetingPlanDetailService.GetFullInfo(meetingId);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Phê duyệt
        /// </summary>
        /// <param name="meetingId"></param>
        /// <returns></returns>
        public ActionResult Approve(Guid meetingId)
        {
            object result;
            try
            {
                QualityMeetingPlanDetailService.Approve(meetingId);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }

        /// <summary>
        /// Tạo biên bản cuộc họp
        /// </summary>
        /// <param name="meetingId"></param>
        /// <returns></returns>
        public ActionResult Record(Guid meetingId)
        {
            var model = QualityMeetingRecordService.GetFormItem(meetingId); //QualityMeetingPlanDetailService.GetFullInfo(meetingId);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Kết thúc cuộc họp
        /// </summary>
        /// <param name="meetingId"></param>
        /// <returns></returns>
        public ActionResult Finish(Guid meetingId)
        {
            object result;
            try
            {
                QualityMeetingRecordService.Finish(meetingId); 
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);

        }

        /// <summary>
        /// Tạo biên bản cuộc họp
        /// </summary>
        /// <param name="meetingId"></param>
        /// <returns></returns>
        public ActionResult UpdateRecord(QualityMeetingRecordBO data)
        {
            object result;
            try
            {
                QualityMeetingRecordService.Save(data);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);

        }
        /// <summary>
        /// Cập nhật trạng thái vắng mặt
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="meetingId"></param>
        /// <returns></returns>
        public ActionResult AvailableChange(Guid id)
        {
            try
            {
                QualityMeetingRecordMemberService.ChangeAvailable(id);
            }
            catch (Exception)
            {
            }
            return this.Direct();
        }

        /// <summary>
        /// View tạo mới cuộc họp
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public ActionResult NewMeeting(Guid planId)
        {
            var model = QualityMeetingPlanDetailService.GetNewMeetingForm(planId);
            var listUnitTime = new ListItemCollection();
            listUnitTime.Add(new ListItem() { Value = Convert.ToInt16(Service.Common.Resource.UnitTime.Day).ToString(), Text = Service.Common.Resource.UnitTimeText.Day });
            listUnitTime.Add(new ListItem() { Value = Convert.ToInt16(Service.Common.Resource.UnitTime.Month).ToString(), Text = Service.Common.Resource.UnitTimeText.Month });
            listUnitTime.Add(new ListItem() { Value = Convert.ToInt16(Service.Common.Resource.UnitTime.Week).ToString(), Text = Service.Common.Resource.UnitTimeText.Week });
            listUnitTime.Add(new ListItem() { Value = Convert.ToInt16(Service.Common.Resource.UnitTime.Year).ToString(), Text = Service.Common.Resource.UnitTimeText.Year });
            ViewData["ListUnitTime"] = listUnitTime;
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        /// <summary>
        /// Tạo mới cuộc họp
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult CreateMeeting(QualityMeetingPlanDetailBO data)
        {
            object result;
            try
            {
                QualityMeetingPlanDetailService.Insert(data);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public ActionResult ChangeRoutine(Guid planId)
        {
            try
            {
                var model = QualityMeetingPlanService.GetById(planId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception)
            {

            }
            return this.Direct();
        }

        /// <summary>
        /// View Gửi phê duyệt
        /// </summary>
        /// <param name="meetingId"></param>
        /// <returns></returns>
        public ActionResult SendApprove(Guid meetingId)
        {
            try
            {
                var model = QualityMeetingPlanDetailService.GetById(meetingId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception)
            {
            }
            return this.Direct();
        }

        /// <summary>
        /// Gửi phê duyệt
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendApprove(QualityMeetingPlanDetailBO data)
        {
            object result;
            try
            {
                QualityMeetingPlanDetailService.SendApprove(data);
                Notify(TitleResourceNotifies.SendApprovalApprovalMeeting, data.PlanName, data.EmployeeApprove.Id, UrlResourceNotifies.ApprovalMeeting, new { id = data.QualityMeetingPlanId },1);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }
        /// <summary>
        /// Xóa cuộc họp
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult DeleteMeeting(Guid meetingId)
        {
            object result;
            try
            {
                QualityMeetingPlanService.Delete(meetingId);
                result = true;
            }
            catch (Exception e)
            {
                result = false;
            }
            return this.Direct(result);
        }
    }
}