﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class SubjectController : FrontController
    {
        public ActionResult Index(Guid meetingId)
        {
            ViewData["MeetingId"] = meetingId;
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }

        public ActionResult Form(string id, Guid meetingId)
        {
            var model = QualityMeetingPlanReportService.GetFormItem(id,meetingId);
            ViewData["DepartmentName"] = model.DepartmentName;
            return new Ext.Net.MVC.PartialViewResult { Model = model,ViewData = ViewData};
        }

        public ActionResult GetData(Guid meetingId)
        {
            try
            {
                var data = QualityMeetingPlanReportService.GetSubjectByMeeting(meetingId).ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
            }
            return this.Direct();
        }

        public ActionResult Create(QualityMeetingPlanReportBO data, string issueIds)
        {
            object result;
            try
            {
                QualityMeetingPlanReportService.Save(data,issueIds);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }
        public ActionResult Delete(Guid id)
        {
            object result;
            try
            {
                QualityMeetingPlanReportService.Delete(id);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }

        public ActionResult AddIssue(Guid issueId, Guid meetingPlanReportId)
        {
            object result;
            try
            {
                QualityMeetingPlanReportIssueService.AddIssue(issueId, meetingPlanReportId);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }
            
	}
}