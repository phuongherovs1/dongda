﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers.Meeting
{
    /// <summary>
    /// Thành phần tham gia
    /// </summary>
    public class MemberController : FrontController
    {
        //
        // GET: /Quality/MeetingMember/
        public ActionResult Index(Guid meetingId)
        {
            ViewData["MeetingId"] = meetingId;
            return new Ext.Net.MVC.PartialViewResult {  ViewData = ViewData };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="meetingId"></param>
        /// <returns></returns>
        public ActionResult GetData(Guid meetingId)
        {
            var data = QualityMeetingRecordMemberService.GetData(meetingId).ToList();
            return this.Store(data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="meetingId"></param>
        /// <returns></returns>
        public ActionResult GetMemberJoinMeeting(Guid meetingId)
        {
            var data = QualityMeetingRecordMemberService.GetMember(meetingId).ToList();
            return this.Store(data);
        }
        /// <summary>
        /// Danh sách người tham dự cuộc họp
        /// </summary>
        /// <param name="meetingId"></param>
        /// <returns></returns>
        public ActionResult GetAvailableMemberJoinMeeting(Guid meetingId)
        {
            var data = QualityMeetingRecordMemberService.GetAvailableMember(meetingId).ToList();
            return this.Store(data);
        }

        [HttpPost]
        public ActionResult AddMember(string memberIds, Guid meetingId)
        {
            object result;
            try
            {
                QualityMeetingRecordMemberService.AddMember(memberIds, meetingId);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }

        [HttpPost]
        public ActionResult Delete(Guid meetingId, Guid memberId)
        {
            object result;
            try
            {
                QualityMeetingRecordMemberService.Delete(meetingId, memberId);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }
	}
}