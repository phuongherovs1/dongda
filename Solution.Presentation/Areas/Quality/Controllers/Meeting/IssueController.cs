﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class IssueController : FrontController
    {
        public ActionResult Index(Guid meetingId)
        {
            ViewData["MeetingId"] = meetingId;
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }

        public ActionResult Form(string alignTo, string renderTo, string storeId)
        {
            ViewData["AlignToId"] = string.IsNullOrEmpty(alignTo) ? "ctDesktop" : alignTo;
            ViewData["RenderToId"] = string.IsNullOrEmpty(renderTo) ? "ctDesktop" : renderTo;
            ViewData["storeId"] = string.IsNullOrEmpty(storeId) ? "storeIssues" : storeId;
            var model = new QualityIssueBO();
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = model };
        }

        public ActionResult GetData(Guid meetingId)
        {
            var data = QualityIssueService.GetAll().ToList();
            return this.Store(data);
        }

        public ActionResult Create(QualityIssueBO data)
        {
            object result;
            try
            {
                QualityIssueService.Insert(data);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }



        public ActionResult GetIssueReport(Guid meetingPlanReportId)
        {
            var data = QualityMeetingPlanReportIssueService.GetIssueReport(meetingPlanReportId).ToList();
            return this.Store(data);
        }

        public ActionResult GetIssueMeetingReport(Guid meetingPlanReportId)
        {
            var data = QualityMeetingPlanReportIssueService.GetIssueMeetingReport(meetingPlanReportId).ToList();
            return this.Store(data);
        }


	}
}