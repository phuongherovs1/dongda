﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers.Meeting
{
    public class MeetingRoutineController : FrontController
    {
        public ActionResult Create(Guid planId)
        {
            var data = QualityMeetingPlanDetailService.CreateByRoutine(planId);
            return this.Store(data);
        }
	}
}