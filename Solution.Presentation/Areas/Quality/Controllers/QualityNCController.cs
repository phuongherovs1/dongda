﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;
using System.Diagnostics;
namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class QualityNCController : FrontController
    {
        //
        // GET: /Quality/QualityNC/
        public ActionResult Index()
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetQualityNCStatus();

            return PartialView();
        }


        // GET: /Quality/QualityNC/
        public ActionResult SettingQualityNC()
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetQualityNCStatus();

            return PartialView();
        }


        public ActionResult FormUpdateSettingQualityNC(string id = default(string))
        {
            object model = null;
            model = QualityNCConfirmPermissionService.GetDetail(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }


        public ActionResult GetDataPermission(StoreRequestParameters param, string departmentId = default(string))
        {
            try
            {
                var departmentSplitId = Guid.Empty;
                if (departmentId.Contains('_'))
                {
                    departmentSplitId = new Guid(departmentId.Split('_')[1]);
                }
                else
                {
                    if (!string.IsNullOrEmpty(departmentId))
                    {
                        departmentSplitId = new Guid(departmentId);
                    }
                }
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                var data = QualityNCConfirmPermissionService.GetAllNCConfirmPermission(pageSize, pageIndex, out count, departmentSplitId);
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }

        }



        public ActionResult InsertSettingQualityNC(string departmentId = default(string))
        {
            object model = null;
            var departmentSplitId = Guid.Empty;
            if (departmentId.Contains('_'))
            {
                departmentSplitId = new Guid(departmentId.Split('_')[1]);
            }
            else
            {
                if (!string.IsNullOrEmpty(departmentId))
                {
                    departmentSplitId = new Guid(departmentId);
                }
            }
            model = new QualityNCConfirmPermissionBO() { DepartmentId = departmentSplitId, DepartmentName = DepartmentService.GetById(departmentSplitId).Name };
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        /// <summary>
        /// Danh sách sự không phù hợp theo mục tiêu
        /// </summary>
        /// <param name="targetId"></param>
        /// <returns></returns>
        public ActionResult FormQualityNCByTarget(string targetId = default(string))
        {
            try
            {
                ViewData["TargetId"] = targetId;
                ViewData["Status"] = iDAS.Service.Common.Resource.GetQualityNCStatus();
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult Form(string idNC = default(string), string notebookId = default(string), string targetId = default(string))
        {
            if (idNC == default(string))
            {
                var model = new QualityNCBO()
                {
                    QualityAuditNotebookId = notebookId != default(string) ? new Guid(notebookId) : Guid.Empty,
                    QualityTargetId = targetId != default(string) ? new Guid(targetId) : Guid.Empty,

                };
                ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetQualityNCTypeStatus());
                ViewData.Add("Operation", Common.Operation.Create);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                var model = QualityNCService.DetailQualityNC(iDAS.Service.Common.Utilities.ConvertToGuid(idNC));
                ViewData.Add("Operation", Common.Operation.Update);
                ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetQualityNCTypeStatus());
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }

        }


        public ActionResult FormApproval(string QualityNCId = default(string))
        {
         
                var model = QualityNCService.DetailQualityNC(iDAS.Service.Common.Utilities.ConvertToGuid(QualityNCId));
                ViewData.Add("Operation", Common.Operation.Update);
                ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetQualityNCTypeStatus());
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            

        }

        public ActionResult SendApproval(string QualityNCId = default(string))
        {

            var model = QualityNCService.DetailQualityNC(iDAS.Service.Common.Utilities.ConvertToGuid(QualityNCId));
            ViewData.Add("Operation", Common.Operation.Update);
            ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetQualityNCTypeStatus());
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };


        }
        public ActionResult CreateNCFromNotebook(string qualityAuditProgramId = default(string), string notebookId = default(string), string departmentId = default(string))
        {
            var model = new QualityNCBO()
            {
                EmployeeConfirmBy = QualityAuditMemberService.GetLeader(new Guid(qualityAuditProgramId)),
                QualityAuditNotebookId = notebookId != default(string) ? new Guid(notebookId) : Guid.Empty,
                DepartmentId = departmentId != default(string) ? new Guid(departmentId) : Guid.Empty,
            };
            ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetQualityNCTypeStatus());
            ViewData.Add("Operation", Common.Operation.Create);
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Form", Model = model, ViewData = ViewData };
        }

        public ActionResult FormRequestCAPA(string id = default(string))
        {
            var model = QualityCAPAService.CreateRequestQualityCAPA(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            model.QualityNCId = iDAS.Service.Common.Utilities.ConvertToGuid(id);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        public ActionResult FormQualityNCRelation(string QualityNCId = default(string))
        {
            var model = new QualityNCRelationBO { };
            model.QualityNCId = iDAS.Service.Common.Utilities.ConvertToGuid(QualityNCId);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }


        public ActionResult GetDataTaskQualityCAPA(StoreRequestParameters parameter, string QualityCAPAId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = TaskService.GetDataTaskQualityCAPA(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(QualityCAPAId)).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult GetDataByTarget(StoreRequestParameters parameter, string targetId = default(string), string filter = default(string),
                                            iDAS.Service.Common.Resource.QualityNCStatus status = iDAS.Service.Common.Resource.QualityNCStatus.All)
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                object data = null;
                if (!String.IsNullOrEmpty(targetId))
                    data = QualityNCService.GetByTargetId(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(targetId), filter, status).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult GetDataQualityNCRelation(StoreRequestParameters parameter, string QualiyNCId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = QualityNCRelationService.GetDataQualityNCRelation(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(QualiyNCId)).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult GetDataTaskPerformQualityCAPA(StoreRequestParameters parameter, string QualityNCId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = TaskService.GetDataTaskPerformQualityCAPA(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(QualityNCId)).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }


        public ActionResult FormTask(string id = default(string), string QualityCAPAId = default(string), string QualityNCId = default(string))
        {
            //var model = TaskService.GetFormItem(id);
            if (id == default(string))
            {
                var model = new TaskBO { };
                model.QualityCAPAId = iDAS.Service.Common.Utilities.ConvertToGuid(QualityCAPAId);
                model.QualityNCId = iDAS.Service.Common.Utilities.ConvertToGuid(QualityNCId);
                ViewData.Add("Operation", Common.Operation.Create);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                var model = TaskService.GetFormItem(new Guid(id));
                model.QualityCAPAId = iDAS.Service.Common.Utilities.ConvertToGuid(QualityCAPAId);
                model.QualityNCId = iDAS.Service.Common.Utilities.ConvertToGuid(QualityNCId);
                ViewData.Add("Operation", Common.Operation.Update);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
        }

        public ActionResult CreateTask(TaskBO item)
        {
            var message = default(string);
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    var taskId = TaskService.CreateTask(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.DirectFormat(message);
        }

        public ActionResult UpdateTask(TaskBO item)
        {
            var message = default(string);
            var result = false;
        
                try
                {
                    TaskService.Update(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            
            return this.DirectFormat(message);
        }

        public ActionResult InsertRequestCAPA(QualityCAPABO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    item.AssignBy = item.EmployeePerformBy.Id;
                    if (item.Id == Guid.Empty)
                    {
                        QualityCAPAService.Insert(item);
                    }
                    else
                    {
                        QualityCAPAService.Update(item);
                    };
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        public ActionResult FormPerformCAPA(string id = default(string))
        {
            var model = QualityNCService.CreatePerformQualityCAPA(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            model.Id = iDAS.Service.Common.Utilities.ConvertToGuid(id);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        public ActionResult FormConfirmQualityNC()
        {
            var model = new QualityNCBO() { };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

    

        public ActionResult GetDataQualityNC(StoreRequestParameters parameter,string departmentId=default(string), string filterName = default(string), iDAS.Service.Common.Resource.QualityNCStatus status = iDAS.Service.Common.Resource.QualityNCStatus.All)
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                if (departmentId.Contains('_'))
                {
                    string[] DepartmentIdsl = departmentId.Trim().Split('_');
                    departmentId = DepartmentIdsl[1];
                    var data = QualityNCService.GetDataQualityNC(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(departmentId), filterName, status).ToList();
                    return this.Store(data, count);
                }
                else
                {
                    var data = QualityNCService.GetAllDataQualityNC(pageIndex, pageSize, out count, filterName, status).ToList();
                    return this.Store(data, count);
                }                            
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult GetDataRowExpander(StoreRequestParameters parameter, string Id = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = QualityNCService.GetDataRowExpander(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(Id)).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }



        public ActionResult GetDataWaitConfirmNC(StoreRequestParameters parameter)
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = QualityNCService.GetDataWaitConfirmNC(pageIndex, pageSize, out count).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }


        public ActionResult GetDataNotConfirmNC(StoreRequestParameters parameter)
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = QualityNCService.GetDataNotConfirmNC(pageIndex, pageSize, out count).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

          public ActionResult GetDepartment(StoreRequestParameters parameter)
        {
            try
            {
                var data = DepartmentService.GetAll();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        


        public ActionResult GetDataConfirmNC(StoreRequestParameters parameter)
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = QualityNCService.GetDataConfirmNC(pageIndex, pageSize, out count).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }


        public ActionResult Create(QualityNCBO item, string datajson = "")
        {
            var result = false;
            try
            {
               var id = QualityNCService.InsertQualityNC(item, datajson);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }


        public ActionResult InsertPermissionQualityNC(QualityNCConfirmPermissionBO item, string datajson = "")
        {
            var result = false;
            try
            {
                QualityNCConfirmPermissionService.InsertPermissionQualityNC(item, datajson);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult DestroyQualityNC(Guid Id)
        {
            var result = false;
            try
            {

                QualityNCService.DestroyQualityNC(Id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult Update(QualityNCBO item, string datajson = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityNCService.UpdateQualityNC(item, datajson);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }


        public ActionResult UpdatePermission(QualityNCConfirmPermissionBO item)
        {
            var result = false;
            
                try
                {
                    QualityNCConfirmPermissionService.UpdatePermission(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            
            return this.Direct(result: result);
        }


        public ActionResult DeletePermission(Guid id)
        {
            var result = false;

            try
            {
                QualityNCConfirmPermissionService.Delete(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }

       public ActionResult SendApprovalNC(QualityNCBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityNCService.SendApprovalNC(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        public ActionResult ApprovalNC(QualityNCBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityNCService.ApprovalNC(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }



        public ActionResult DeleteQualityNC(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityNCService.DeleteQualityNC(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.AccessDenyException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }


        public ActionResult DeleteRelationNC(string Id=default(string))
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    if (Id=="null")
                    {
                        result = true;
                        this.ShowNotify(Common.Resource.SystemSuccess);
                    }
                    else
                    {
                        QualityNCRelationService.Delete(iDAS.Service.Common.Utilities.ConvertToGuid(Id));
                        result = true;
                        this.ShowNotify(Common.Resource.SystemSuccess);
                    }
                   
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.AccessDenyException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        public ActionResult DeleteTask(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityCAPADetailService.DeleteTask(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.AccessDenyException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        public ActionResult DeletePerformTask(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityCAPADetailService.DeletePerformTask(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.AccessDenyException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }


        /// <summary>
        ///   Gửi đến khung chưa xác nhận
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ToWaitConfirm(Guid id)
        {
            var result = false;

            try
            {
                QualityNCService.FromWaitConfirm(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }



        /// <summary>
        ///   Gửi đền khung  đạt
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ToConfirm(Guid id)
        {
            var result = false;

            try
            {
                QualityNCService.ToConfirm(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }

        /// <summary>
        ///    Gửi đền khung không đạt
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ToNotPass(Guid id)
        {
            var result = false;
            try
            {
                QualityNCService.ToNotPass(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }

        
      /// <summary>
        ///    Kéo vào nhau
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ToFromQuality(Guid toId, Guid formId)
        {
            var result = false;
            try
            {
                QualityNCService.ToFromQuality(toId, formId);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }

    }
}