﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using Ext.Net;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class TargetUnitController : FrontController
    {
        /// <summary>
        /// Form thêm mới/sửa đơn vị
        /// </summary>
        /// <param name="id"></param>
        /// <param name="alignTo"></param>
        /// <param name="renderTo"></param>
        /// <param name="storeId"></param>
        /// <returns></returns>
        public ActionResult Form(string id = default(string), string alignTo = default(string), string renderTo = default(string), string storeId = default(string))
        {
            try
            {
                QualityUnitBO model = null;
                ViewData["AlignToId"] = string.IsNullOrEmpty(alignTo) ? "ctDesktop" : alignTo;
                ViewData["RenderToId"] = string.IsNullOrEmpty(renderTo) ? "ctDesktop" : renderTo;
                ViewData["storeId"] = string.IsNullOrEmpty(storeId) ? "storeQualityUnit" : storeId;
                model = QualityUnitService.GetDetail(id);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                return this.Direct();
            }
        }

        /// <summary>
        /// Danh sách đơn vị
        /// </summary>
        /// <returns></returns>
        public ActionResult GetData()
        {
            try
            {
                var data = QualityUnitService.GetUnit().ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Thêm mới danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(QualityUnitBO item)
        {
            var result = false;
            try
            {
                QualityUnitService.InsertUnit(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityUnitService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.TargetCategoryExitsTarget || ex is iDAS.Service.Common.AccessDenyException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}