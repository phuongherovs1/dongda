﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class DashBoardController : FrontController
    {
        // GET: /Quality/DashBoard/
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }

        /// <summary>
        /// Danh mục mục tiêu
        /// </summary>
        /// <returns></returns>
        public ActionResult ListTarget()
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetTargetStatus();
            return PartialView();
        }

        public ActionResult LoadData(string node, string departmentId)
        {
            try
            {
                NodeCollection nodes = new NodeCollection();
                var depId = new Guid();
                if (node == "root" && departmentId != "")
                {
                    if (departmentId.Contains('_'))
                    {
                        var department = departmentId.Split('_').ToList();
                        if (department != null && department.Count == 2)
                        {
                            depId = new Guid(department[1]);
                        }
                    }
                    else
                    {
                        depId = new Guid(departmentId);
                    }
                    var departmentRoot = DepartmentService.GetDepartmentDetail(depId, checkParent: true);
                    nodes.Add(createNodeDepartment(departmentRoot));
                }
                else
                {
                    var departments = node == "root" ? DepartmentService.GetTreeDepartment(null)
                                    : DepartmentService.GetTreeDepartment(new Guid(Convert.ToString(node)));
                    foreach (var department in departments)
                    {
                        nodes.Add(createNodeDepartment(department));
                    }
                }
                return this.Content(nodes.ToJson());
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }
        private Node createNodeDepartment(iDAS.Service.DepartmentBO department)
        {
            Node node = new Node();
            node.NodeID = department.Id.ToString();
            node.Text = " " + department.Name;
            node.IconCls = "x-fa fa-home";
            node.CustomAttributes.Add(new ConfigItem { Name = "ParentId", Value = department.ParentId.ToString(), Mode = ParameterMode.Value });
            node.Leaf = !department.IsParent;
            return node;
        }
    }
}