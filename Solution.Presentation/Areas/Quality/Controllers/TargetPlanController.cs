﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net.MVC;
using Ext.Net;
using iDAS.Service;
using iDAS.Service.Common;

namespace iDAS.Presentation.Areas.Quality.Controllers
{
    public class TargetPlanController : FrontController
    {
        /// <summary>
        /// Form thêm mới/sửa đơn vị
        /// </summary>
        /// <param name="id"></param>
        /// <param name="alignTo"></param>
        /// <param name="renderTo"></param>
        /// <param name="storeId"></param>
        /// <returns></returns>
        public ActionResult Form(string id = default(string), string departmentId = default(string), string targetId = default(string))
        {
            try
            {
                if (!String.IsNullOrEmpty(id))
                    ViewData.Add("Operation", Common.Operation.Create);
                else
                    ViewData.Add("Operation", Common.Operation.Update);
                QualityPlanBO model = null;
                model = QualityPlanService.GetDetail(id, departmentId, new Guid(targetId));
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                return this.Direct();
            }
        }

        /// <summary>
        /// Form phê duyệt
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <param name="targetId"></param>
        /// <returns></returns>
        public ActionResult FormApproval(string id = default(string), string departmentId = default(string), string targetId = default(string))
        {
            try
            {
                QualityPlanBO model = null;
                model = QualityPlanService.GetDetail(id, departmentId, new Guid(targetId));
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                return this.Direct();
            }
        }

        public ActionResult Index(string targetId = default(string))
        {
            try
            {
                ViewData["TargetId"] = targetId;
                ViewData["QualityPlanStatus"] = iDAS.Service.Common.Resource.GetQualityPlanStatus();
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Danh sách kế hoạch
        /// </summary>
        /// <returns></returns>
        public ActionResult GetData(StoreRequestParameters parameter, Guid targetId, string filter = default(string), Resource.QualityPlan status = Resource.QualityPlan.All)
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = QualityPlanService.GetByTargetId(pageIndex, pageSize, out count, targetId, filter, status).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult GetDataTaskByQualityPlan(StoreRequestParameters parameter, string QualityPlanId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = TaskService.GetDataTaskByQualityPlan(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(QualityPlanId)).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Thêm mới/sửa kế hoạch
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(QualityPlanBO item,string TaskId="")
        {
            var result = false;
            try
            {
                QualityPlanService.Create(item, TaskId);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Gửi kế hoach đi phê duyệt
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Send(QualityPlanBO item, string TaskId = "")
        {
            var result = false;
            try
            {
                QualityPlanService.Send(item, TaskId);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Phê duyệt kế hoạch
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Approval(QualityPlanBO item)
        {
            var result = false;
            try
            {
                QualityPlanService.Approval(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    QualityPlanService.DeletePlan(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.AccessDenyException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}