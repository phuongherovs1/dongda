﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Common;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.TimeKeeping.Controllers
{
    public class EmployeeWorkOutController : FrontController
    {
        #region View
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult Form(string id = default)
        {
            var userId = EmployeeService.GetCurrentUser().Id;

            EmployeeWorkOutBO model;
            if (id == default)
            {
                ViewData.Add("Operation", Operation.Create);
                model = new EmployeeWorkOutBO();
            }
            else
            {
                ViewData.Add("Operation", Operation.Update);
                model = EmployeeWorkOutService.GetById(Guid.Parse(id));
                model.EmployeePerform = EmployeeService.GetById((Guid)model.ApprovedBy);
                model.Status = GetCurrentStatus(model.Id);
                var tracking = FormTracking(model.Id, out FormApproveStatusBO currentStatus);
                if (tracking)
                {
                    ViewData["isFinal"] = currentStatus.isFinal;
                    ViewData["sentBy"] = EmployeeService.GetById((Guid)currentStatus.SentBy).Id;
                }
            }
            model.UserId = userId;
            
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion

        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param, int Status = -1)
        {
            if (param is null)
            {
                throw new ArgumentNullException(nameof(param));
            }

            var userId = EmployeeService.GetCurrentUser().Id;
            var data = EmployeeWorkOutService.GetAll().Where(u => u.EmployeeId == userId);
            if (Status != -1) data = data.Where(u => u.Status == Status);

            if (data.ToList().Count > 0)
            {
                var result = (from EmployeeWorkOutBO bo in data
                              select new
                              {
                                  bo.Id,
                                  bo.EmployeeId,
                                  EmployeeName = EmployeeService.GetById(bo.EmployeeId).Name,
                                  DepartmentTitle = EmployeeTitleService.GetStringDepartmentRole(bo.EmployeeId),
                                  DepartmentName = "",
                                  bo.StartTime,
                                  bo.EndTime,
                                  bo.Reason,
                                  bo.Address,
                                  bo.ApprovedBy,
                                  EmployeeApproveName = EmployeeService.GetById((Guid)bo.ApprovedBy).Name,
                                  Status = GetCurrentStatus(bo.Id),
                                  StatusForView = GetCurrentStatus(bo.Id) == 0 ? Resource.Wait : GetCurrentStatus(bo.Id) == 1 ? Resource.Approved : Resource.Rejected
                              });
                return this.Store(result, result.ToList().Count);
            }
            return this.Store(null, 0);
        }

        [ValidateInput(false)]
        public ActionResult GetDataApproveWorkOut(StoreRequestParameters param, int Status = 0)
        {
            var userId = EmployeeService.GetCurrentUser().Id;
            var data = EmployeeWorkOutService.GetAll().Where(u => u.ApprovedBy == userId);
            if (Status != -1) data = data.Where(u => u.Status == Status);
            if (data.ToList().Count > 0)
            {
                var result = (from EmployeeWorkOutBO bo in data
                              select new
                              {
                                  bo.Id,
                                  bo.EmployeeId,
                                  EmployeeName = EmployeeService.GetById(bo.EmployeeId).Name,
                                  mployeeName = EmployeeService.GetById(bo.EmployeeId).Name,
                                  DepartmentTitle = EmployeeTitleService.GetStringDepartmentRole(bo.EmployeeId),
                                  bo.StartTime,
                                  bo.EndTime,
                                  bo.Reason,
                                  bo.Address,
                                  bo.ApprovedBy,
                                  EmployeeApproveName = EmployeeService.GetById((Guid)bo.ApprovedBy).Name,
                                  Status = GetCurrentStatus(bo.Id),
                                  StatusForView = GetCurrentStatus(bo.Id) == 0 ? Resource.Wait : GetCurrentStatus(bo.Id) == 1 ? Resource.Approved : Resource.Rejected
                              });
                return this.Store(result, result.ToList().Count);
            }
            return this.Store(null, 0);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(EmployeeWorkOutBO item)
        {
            var result = false;
            if (ModelState.IsValid && !DBConnect.CheckEsxitItem("sp_EmployeeWorkOuts_CheckEsxit", "@StartTime", item.StartTime, "@EndTime", item.EndTime))
            {
                try
                {
                    var userId = EmployeeService.GetCurrentUser().Id;
                    var ID = Guid.NewGuid();
                    item.Id = ID;
                    item.EmployeeId = userId;
                    item.ApprovedBy = item.EmployeePerform.Id;
                    item.CreateBy = item.EmployeeId;
                    item.UpdateAt = null;
                    item.IsDelete = false;
                    EmployeeWorkOutService.Insert(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            else
            {
                this.ShowNotify("Thời gian đăng ký đã tồn tại, đang chờ duyệt hoặc đã được duyệt!");
            }
            return this.Direct(result: result);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(EmployeeWorkOutBO item, string forwardId = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    item.ApprovedBy = item.EmployeePerform.Id;
                    UpdateApproveState(item, forwardId);

                    var tracking = FormTracking(item.Id, out FormApproveStatusBO bo);
                    item.ApprovedBy = bo.ApprovedBy;
                    if (bo.isFinal) item.Status = bo.Status;
                    else item.Status = 0;
                    EmployeeWorkOutService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        [HttpPost]
        public ActionResult Delete(EmployeeWorkOutBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    EmployeeWorkOutService.Delete(item.Id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        [HttpPost]
        public ActionResult Approve(EmployeeWorkOutBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
                    {
                        if (connection.State != ConnectionState.Open)
                            connection.Open();

                        using (var cmd = connection.CreateCommand())
                        {
                            cmd.CommandText = "sp_EmployeeWorkOut_Approve";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 200;
                            cmd.Parameters.AddWithValue("@id", item.Id);
                            cmd.Parameters.AddWithValue("@Status", item.Status);

                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        public bool FormTracking(Guid id, out FormApproveStatusBO tracking)
        {
            var stateList = FormApproveStatusService.GetByFormId(id);
            if (stateList.ToList().Count > 0)
            {
                tracking = stateList.OrderByDescending(u => u.CreateAt).First();
                return true;
            }
            tracking = new FormApproveStatusBO();
            return false;
        }

        public int GetCurrentStatus(Guid id)
        {
            var tracking = FormTracking(id, out FormApproveStatusBO currentStatus);
            if (tracking && currentStatus.isFinal) return currentStatus.Status;
            else return 0;
        }

        public ActionResult UpdateApproveState(EmployeeWorkOutBO item, string forwardId = "")
        {
            try
            {
                var tableName = "EmployeeWorkOuts";
                //cập nhật trạng thái cho bảng theo dõi trước
                var tracking = FormTracking(item.Id, out FormApproveStatusBO currentStatus);
                if (tracking)
                {
                    currentStatus.Status = item.Status;
                    currentStatus.SentBy = item.ApprovedBy;
                    //nếu đồng ý hoặc từ chối đơn và ko chuyển cho ai khác thì đóng theo dõi
                    if (item.Status != 0 && forwardId == "")
                    {
                        currentStatus.isFinal = true;
                        FormApproveStatusService.Update(currentStatus);
                        return this.Direct(true);
                    }

                    FormApproveStatusService.Update(currentStatus);
                }
                //thêm 1 record theo dõi mới
                var state = new FormApproveStatusBO()
                {
                    Id = Guid.NewGuid(),
                    FormId = item.Id,
                    TableName = tableName,
                    isFinal = (item.Status != 0 && forwardId == ""),
                    Status = item.Status,
                    SentBy = item.ApprovedBy ?? Guid.Empty,
                    ApprovedBy = Guid.TryParse(forwardId, out Guid gid) ? gid : item.ApprovedBy,
                    ApprovedAt = item.Status != 0 ? DateTime.Now : (DateTime?)null,
                    Reason = item.Reason,
                    CreateBy = item.EmployeeId,
                    CreateAt = DateTime.Now
                };

                FormApproveStatusService.Insert(state);
            }
            catch (Exception)
            {
                throw;
            }
            return this.Direct(true);
        }
    }
}