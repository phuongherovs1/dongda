﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Common;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.TimeKeeping.Controllers
{
    public class ExplanationController : Controller
    {
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }
        public ActionResult ResultListExplanation(string id, string month)
        {
            EmployeeBO item = Common.DBConnect.EmployeeGetById(id);
            ViewData.Add("Code", item.Code);
            ViewData.Add("Month", month);
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        public ActionResult Form(string id = "")
        {
            TimekeepingBO model = new TimekeepingBO();
            ViewData.Add("Operation", Common.Operation.Update);
            model = GetById(id);
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public TimekeepingBO GetById(string Id)
        {
            TimekeepingBO data = new TimekeepingBO();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    var dataResult = new DataTable();
                    cmd.CommandText = "TimeKeeping_GetById_Explanation";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    var da = new SqlDataAdapter(cmd);
                    da.Fill(dataResult);
                    data.Id = new Guid(dataResult.Rows[0][0].ToString());
                    data.Code = dataResult.Rows[0][1].ToString();
                    data.Name = dataResult.Rows[0][2].ToString();
                    data.Date = DateTime.Parse(dataResult.Rows[0][3].ToString());
                    data.TimeIn1 = dataResult.Rows[0][4].ToString();
                    data.TimeOut1 = dataResult.Rows[0][5].ToString();
                    data.Status = !string.IsNullOrEmpty(dataResult.Rows[0][6].ToString()) ? (int)dataResult.Rows[0][6] : 0;
                    data.ReasonExplanation = dataResult.Rows[0][7].ToString();
                    data.ApproveId = !string.IsNullOrEmpty(dataResult.Rows[0][8].ToString()) ? new Guid(dataResult.Rows[0][8].ToString()) : Guid.Empty;
                    return data;
                }
            }
        }
        public ActionResult GetDataExplanation(StoreRequestParameters param, string Code = default(string), string Status = default(string), string Month = default(string))
        {
            List<TimekeepingBO> data = new List<TimekeepingBO>();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    var dataResult = new DataTable();
                    cmd.CommandText = "TimeKeeping_GetData_Explanation";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Code", !string.IsNullOrEmpty(Code) ? Code : "");
                    cmd.Parameters.AddWithValue("@Status", !string.IsNullOrEmpty(Status) ? Status : "");
                    cmd.Parameters.AddWithValue("@FromDate", !string.IsNullOrEmpty(Month) ? DateTime.ParseExact(Month, "dd/MM/yyyy", null).AddMonths(-1).ToString("MM/23/yyyy") : "");
                    cmd.Parameters.AddWithValue("@ToDate", !string.IsNullOrEmpty(Month) ? DateTime.ParseExact(Month, "dd/MM/yyyy", null).ToString("MM/22/yyyy") : "");
                    var da = new SqlDataAdapter(cmd);
                    da.Fill(dataResult);
                    for (int i = 0; i < dataResult.Rows.Count; i++)
                    {
                        TimekeepingBO item = new TimekeepingBO();
                        item.Id = new Guid(dataResult.Rows[i][0].ToString());
                        item.Code = dataResult.Rows[i][1].ToString();
                        item.Name = dataResult.Rows[i][2].ToString();
                        item.Date = DateTime.Parse(dataResult.Rows[i][3].ToString());
                        item.TimeIn1 = dataResult.Rows[i][4].ToString();
                        item.TimeOut1 = dataResult.Rows[i][5].ToString();
                        item.Status = !string.IsNullOrEmpty(dataResult.Rows[i][6].ToString()) ? (int)dataResult.Rows[i][6] : 0;
                        item.ReasonExplanation = dataResult.Rows[i][7].ToString();
                        item.ApproveId = !string.IsNullOrEmpty(dataResult.Rows[i][8].ToString()) ? new Guid(dataResult.Rows[i][8].ToString()) : Guid.Empty;
                        item.ApproveName = item.ApproveId != Guid.Empty ? DBConnect.EmployeeGetById(item.ApproveId.Value.ToString()).Name : "";

                        DateTime RoleIn = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 08:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                        DateTime RoleOut = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 17:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                        //Vào trễ
                        if (item.TimeIn1 != "V" && item.TimeIn1 != "F" && item.TimeIn1 != "--" && !string.IsNullOrEmpty(item.TimeIn1))
                        {
                            DateTime In = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeIn1 + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                            if (In.Subtract(RoleIn).Minutes > 0)
                            {
                                item.Reason += ",Vào trễ " + In.Subtract(RoleIn).TotalMinutes + " phút";
                            }
                        }
                        //Ra sớm
                        if (!string.IsNullOrEmpty(item.TimeOut1))
                        {
                            DateTime Out = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeOut1 + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                            if (RoleOut.Subtract(Out).Minutes > 0)
                            {
                                item.Reason += ", Ra sớm " + RoleOut.Subtract(Out).TotalMinutes + " phút";
                            }
                        }
                        //Vắng KP
                        if (item.TimeIn1 == "V")
                            item.Reason += ",Vắng không phép";
                        if (!string.IsNullOrEmpty(item.Reason))
                            item.Reason = item.Reason.Substring(1);
                        data.Add(item);
                    }
                    return this.Store(data);
                }
            }
        }
        public ActionResult RequestExplanation_Result(TimekeepingBO item)
        {
            var result = false;
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "TimeKeeping_RequestExplanation_Result";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@Status", 2);
                    cmd.Parameters.AddWithValue("@TimeIn", item.TimeIn1);
                    cmd.Parameters.AddWithValue("@TimeOut", item.TimeOut1);
                    cmd.Parameters.AddWithValue("@ReasonExplanation", item.ReasonExplanation);
                    var da = cmd.ExecuteNonQuery();
                    if (da > 0)
                        result = true;
                    return this.Direct(result: result);
                }
            }
        }
        public ActionResult Update(TimekeepingBO item)
        {
            var result = false;
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "TimeKeeping_UPDATE";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@Status", item.Status);
                    cmd.Parameters.AddWithValue("@TimeIn", item.TimeIn);
                    cmd.Parameters.AddWithValue("@TimeOut", item.TimeOut);
                    cmd.Parameters.AddWithValue("@ReasonExplanation", item.ReasonExplanation);
                    var da = cmd.ExecuteNonQuery();
                    if (da > 0)
                        result = true;
                    return this.Direct(result: result);
                }
            }
        }

        private DataTable Employees;
        private DataTable EmployeeDepartment;
        [ValidateInput(false)]
        public ActionResult GetDataEmployeeExplanation(StoreRequestParameters param)
        {
            DBConnect dbconnect = new DBConnect();
            List<TimekeepingBO> lstData = new List<TimekeepingBO>();
            List<EmployeeDepartmentTitle> employeeDepartmentTitles = new List<EmployeeDepartmentTitle>();

            DateTime Month = DateTime.Now;
            if (Month.Day > 22)
                Month = Month.AddMonths(1);
            //Test với dữ liệu tháng 5
            Month = new DateTime(2019, 5, 1);

            DataSet getDataAll;
            getDataAll = dbconnect.myDataset("TimeKeeping_Employee_Explanation", "@FromDate", new DateTime(Month.Year, Month.Month - 1, 23, 0, 0, 0).ToString("MM/dd/yyyy"), "@ToDate", new DateTime(Month.Year, Month.Month, 22, 0, 0, 0).ToString("MM/dd/yyyy"));
            Employees = getDataAll.Tables[0];
            EmployeeDepartment = getDataAll.Tables[1];

            for (int i = 0; i < EmployeeDepartment.Rows.Count; i++)
            {
                EmployeeDepartmentTitle item = new EmployeeDepartmentTitle();
                item.EmployeeId = EmployeeDepartment.Rows[i]["EmployeeId"].ToString();
                item.DepartmentName = EmployeeDepartment.Rows[i]["DepartmentName"].ToString();
                item.DepartmentTitle = EmployeeDepartment.Rows[i]["DepartmentTitle"].ToString();
                employeeDepartmentTitles.Add(item);
            }
            for (int i = 0; i < Employees.Rows.Count; i++)
            {
                TimekeepingBO item = new TimekeepingBO();
                item.Id = new Guid(Employees.Rows[i][0].ToString());
                item.EmployeeId = new Guid(Employees.Rows[i][1].ToString());
                item.TimeIn = Employees.Rows[i][2].ToString();
                item.TimeOut = Employees.Rows[i][3].ToString();
                item.Date = DateTime.Parse(Employees.Rows[i][4].ToString());
                item.Code = Employees.Rows[i][5].ToString();
                item.Name = Employees.Rows[i][6].ToString();

                item.DepartmentName = string.Join(", ", employeeDepartmentTitles.Where(x => x.EmployeeId == item.EmployeeId.ToString()).Select(x => x.DepartmentName).ToArray());
                item.DepartmentTitle = string.Join(", ", employeeDepartmentTitles.Where(x => x.EmployeeId == item.EmployeeId.ToString()).Select(x => x.DepartmentTitle).ToArray());

                DateTime RoleIn = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 08:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                DateTime RoleOut = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 17:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                //Vào trễ
                if (item.TimeIn != "V" && item.TimeIn != "F" && item.TimeIn != "--" && !string.IsNullOrEmpty(item.TimeIn))
                {
                    DateTime In = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeIn + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                    if (In.Subtract(RoleIn).Minutes > 0)
                    {
                        item.Reason += ",Vào trễ " + In.Subtract(RoleIn).TotalMinutes + " phút";
                    }
                }
                //Ra sớm
                if (!string.IsNullOrEmpty(item.TimeOut))
                {
                    DateTime Out = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeOut + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                    if (RoleOut.Subtract(Out).Minutes > 0)
                    {
                        item.Reason += ", Ra sớm " + RoleOut.Subtract(Out).TotalMinutes + " phút";
                    }
                }
                //Vắng KP
                if (item.TimeIn == "V")
                    item.Reason += ",Vắng không phép";
                if (!string.IsNullOrEmpty(item.Reason))
                {
                    var index = lstData.FindIndex(x => x.Code == item.Code);
                    if (index == -1)
                    {
                        item.NumberExplanation = 1;
                        lstData.Add(item);
                    }
                    else
                    {
                        lstData[index].NumberExplanation = 1;
                    }
                }
            }
            return this.Store(lstData, lstData.Count);
        }

        [HttpPost]
        public ActionResult Approve(TimekeepingBO item)
        {
            var claim = (ClaimsIdentity)User.Identity;
            var userId = claim.FindFirst(ClaimTypes.UserData).Value;

            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
                    {
                        if (connection.State != ConnectionState.Open)
                            connection.Open();

                        using (var cmd = connection.CreateCommand())
                        {
                            cmd.CommandText = "TimeKeeping_RequestExplanation_Approve";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 200;
                            cmd.Parameters.AddWithValue("@id", item.Id);
                            cmd.Parameters.AddWithValue("@Status", item.Status);
                            cmd.Parameters.AddWithValue("@ApproveId", new Guid(userId));

                            cmd.ExecuteNonQuery();
                            this.ShowNotify(Common.Resource.SystemSuccess);
                        }
                    }
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}