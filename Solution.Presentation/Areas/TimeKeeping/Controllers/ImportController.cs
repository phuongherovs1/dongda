﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using systemIO = System.IO;

namespace iDAS.Presentation.Areas.TimeKeeping.Controllers
{
    public class ImportController : Controller
    {
        // GET: TimeKeeping/Import
        public ActionResult Import()
        {
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Import", ViewData = ViewData };
        }
        private DataTable ReadFileExcel(string directory)
        {
            FileStream fStream = new FileStream(directory, FileMode.Open);
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook(fStream);
            Aspose.Cells.Worksheet worksheet = workbook.Worksheets[0];
            DataTable datatable = worksheet.Cells.ExportDataTable(0, 0, worksheet.Cells.Rows.Count + 1, worksheet.Cells.Columns.Count == 0 ? worksheet.Cells.MaxColumn : worksheet.Cells.Columns.Count, true);
            fStream.Close();
            return datatable;
        }
        public ActionResult DataObjectImport(StoreRequestParameters parameters, string direction)
        {
            var result = new List<object>();
            if (!string.IsNullOrEmpty(direction))
            {
                try
                {
                    var table = ReadFileExcel(Server.MapPath(direction));
                    for (var i = 0; i < table.Rows.Count; i++)
                    {
                        var obj = new ExpandoObject() as IDictionary<string, object>;
                        for (int j = 0; j < table.Columns.Count; j++)
                        {
                            obj.Add("Column" + j, table.Rows[i][j]);
                        }
                        result.Add(obj);
                    }
                }
                catch (Exception e)
                {

                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = e.Message
                    });
                    return this.Direct();
                }
            }
            return this.Store(result);
        }
        public ActionResult SelectImportFile()
        {
            var fileUploadField = X.GetCmp<FileUploadField>("FileImportField");

            var direction = Common.Resource.FileImportUrl + Guid.NewGuid().ToString() + ".xlsx";
            if (fileUploadField.HasFile)
            {
                if (fileUploadField.PostedFile.ContentLength < 30 * 1024 * 1024 + 1)
                {
                    if (fileUploadField.FileName.Split('.').LastOrDefault().ToUpper().Equals("XLSX") || fileUploadField.FileName.Split('.').LastOrDefault().ToUpper().Equals("XLS"))
                    {
                        if (!systemIO.File.Exists(Server.MapPath(direction)))
                        {
                            fileUploadField.PostedFile.SaveAs(Server.MapPath(direction));
                        }
                    }
                    else
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING,
                            Title = "Thông báo",
                            Message = "Hệ thống chỉ hỗ trợ file có đuôi .xls và .xlsx!"
                        });
                        return this.Direct();
                    }
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = "Chỉ cho phép dung lượng import tối đa là 30MB!"
                    });
                    return this.Direct();
                }
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Không có thông tin cho file upload!"
                });
            }
            return this.Direct(direction);
        }
        public ActionResult SettingImport(string direction)
        {
            if (direction != null)
            {
                var table = ReadFileExcel(Server.MapPath(direction));
                List<string> lst_timefrom_to = table.Rows[0][0].ToString().Split(' ').ToList();
                DateTime FromDate = DateTime.ParseExact(lst_timefrom_to[2], "dd/MM/yyyy", null);
                DateTime ToDate = DateTime.ParseExact(lst_timefrom_to[5], "dd/MM/yyyy", null);
                int CountTime = ToDate.Subtract(FromDate).Days + 1;
                for (int row = 5; row < table.Rows.Count; row += 2)
                {
                    if (!string.IsNullOrEmpty(table.Rows[row][1].ToString()))
                    {
                        for (int col = 0; col < CountTime; col++)
                        {
                            TimekeepingBO item = new TimekeepingBO();
                            item.Id = Guid.NewGuid();
                            item.Code = table.Rows[row][1].ToString();
                            item.Name = table.Rows[row][2].ToString();
                            item.TimeIn = table.Rows[row][col + 4].ToString();
                            item.TimeOut = table.Rows[row + 1][col + 4].ToString();
                            item.Date = FromDate.AddDays(col);
                            Insert(item);
                        }
                    }
                    else
                    {
                        row -= 1;
                    }
                }
            }
            X.Msg.Show(new MessageBoxConfig
            {
                Buttons = MessageBox.Button.OK,
                Icon = MessageBox.Icon.INFO,
                Title = "Thông báo",
                Message = "Nhập liệu vào hệ thống thành công!"
            });
            return this.Direct();
        }
        public void Insert(TimekeepingBO item)
        {
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
                {
                    if (connection.State != ConnectionState.Open)
                        connection.Open();
                    using (var cmd = connection.CreateCommand())
                    {
                        cmd.CommandText = "TimeKeeping_Insert";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 200;
                        cmd.Parameters.AddWithValue("@Id", item.Id);
                        cmd.Parameters.AddWithValue("@Code", item.Code);
                        cmd.Parameters.AddWithValue("@Name", item.Name);
                        cmd.Parameters.AddWithValue("@Date", item.Date);
                        cmd.Parameters.AddWithValue("@TimeIn", item.TimeIn);
                        cmd.Parameters.AddWithValue("@TimeOut", item.TimeOut);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}