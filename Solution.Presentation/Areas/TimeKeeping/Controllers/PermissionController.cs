﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Common;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using iDAS.Service.API.TimeKeeping;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Claims;
using System.Web.Mvc;
using PartialViewResult = Ext.Net.MVC.PartialViewResult;

namespace iDAS.Presentation.Areas.TimeKeeping.Controllers
{
    public class PermissionController : FrontController
    {
        #region View
        public PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }
        #endregion
        protected IPermissionService _IPermissionService { set; get; }
        public PermissionController(IPermissionService permissionService)
        {
            _IPermissionService = permissionService;
        }

        private DataTable Permissions;
        [ValidateInput(false)]
        public ActionResult GetData(int type, string name = "", string code = "")
        {
            var claim = (ClaimsIdentity)User.Identity;
            var userId = claim.FindFirst(ClaimTypes.UserData).Value;
            var dbConnect = new DBConnect();
            if (code != "")
            {
                Permissions = _IPermissionService.Permission_GetData(type, name, userId);
            }
            else
            {
                Permissions = _IPermissionService.Permission_GetData(type, name, "");
            }
            return this.Store(Permissions, Permissions.Rows.Count);
        }

        public ActionResult Delete(string id)
        {
            var idList = JsonConvert.DeserializeObject<List<string>>(id);
            foreach (var i in idList)
            {
                _IPermissionService.Delete(Convert.ToString(i));
            }
            return this.Direct(true);
        }

        public ActionResult Form(string id = default)
        {
            PermissionBO model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new PermissionBO();
            }
            return new PartialViewResult { Model = model, ViewData = ViewData };
        }

        public ActionResult AddEmployee(string id)
        {
            var idList = JsonConvert.DeserializeObject<List<string>>(id);
            foreach (var i in idList)
            {
                _IPermissionService.Insert(Convert.ToString(i));
            }
            return this.Direct(true);
        }

    }
}