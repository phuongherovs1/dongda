﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Common;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.TimeKeeping.Controllers
{
    public class EmployeeOTController : FrontController
    {
        // GET: TimeKeeping/EmployeeOT
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
            };
        }
        public ActionResult Form(string id = default)
        {
            var userId = EmployeeService.GetCurrentUser().Id;
            EmployeeOTBO model;
            if (id == default)
            {
                ViewData.Add("Operation", Operation.Create);
                model = new EmployeeOTBO();
            }
            else
            {
                ViewData.Add("Operation", Operation.Update);
                model = EmployeeOTService.GetById(Guid.Parse(id));
                model.EmployeePerform = EmployeeService.GetById((Guid)model.ApprovedBy);

                model.Status = GetCurrentStatus(model.Id);
                var tracking = FormTracking(model.Id, out FormApproveStatusBO currentStatus);

                if (tracking)
                {
                    ViewData["isFinal"] = currentStatus.isFinal;
                    ViewData["sentBy"] = EmployeeService.GetById((Guid)currentStatus.SentBy).Id;
                }
                Dispose();
            }
            model.UserId = userId;
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param, int Status = -1)
        {
            if (param is null)
            {
                throw new ArgumentNullException(nameof(param));
            }

            var userId = EmployeeService.GetCurrentUser().Id;
            var data = EmployeeOTService.GetAll().Where(u => u.EmployeeId == userId);
            if (Status != -1) data = data.Where(u => u.Status == Status);

            if (data.ToList().Count > 0)
            {
                var result = (from EmployeeOTBO bo in data
                              select new
                              {
                                  bo.Id,
                                  bo.EmployeeId,
                                  EmployeeName = EmployeeService.GetById(bo.EmployeeId).Name,
                                  DepartmentTitle = EmployeeTitleService.GetStringDepartmentRole(bo.EmployeeId),
                                  DepartmentName = "",
                                  bo.StartTime,
                                  bo.EndTime,
                                  bo.Reason,
                                  bo.Level,
                                  bo.ApprovedBy,
                                  EmployeeApproveName = EmployeeService.GetById((Guid)bo.ApprovedBy).Name,
                                  Status = GetCurrentStatus(bo.Id),
                                  StatusForView = GetCurrentStatus(bo.Id) == 0 ? Resource.Wait : GetCurrentStatus(bo.Id) == 1 ? Resource.Approved : Resource.Rejected
                              });
                var r = result.ToList();
                return this.Store(result, result.ToList().Count);
            }
            return this.Store(null, 0);
        }

        [ValidateInput(false)]
        public ActionResult GetDataWaitAprove(StoreRequestParameters param, int Status = 0)
        {
            var userId = EmployeeService.GetCurrentUser().Id;
            var data = EmployeeOTService.GetAll().Where(u => u.ApprovedBy == userId);
            if (Status != -1) data = data.Where(u => u.Status == Status);

            if (data.ToList().Count > 0)
            {
                var result = (from EmployeeOTBO bo in data
                              select new
                              {
                                  bo.Id,
                                  bo.EmployeeId,
                                  EmployeeName = EmployeeService.GetById(bo.EmployeeId).Name,
                                  DepartmentTitle = EmployeeTitleService.GetStringDepartmentRole(bo.EmployeeId),
                                  DepartmentName = "",
                                  bo.StartTime,
                                  bo.EndTime,
                                  bo.Reason,
                                  bo.Level,
                                  bo.ApprovedBy,
                                  EmployeeApproveName = EmployeeService.GetById((Guid)bo.ApprovedBy).Name,
                                  Status = GetCurrentStatus(bo.Id),
                                  StatusForView = GetCurrentStatus(bo.Id) == 0 ? Resource.Wait : GetCurrentStatus(bo.Id) == 1 ? Resource.Approved : Resource.Rejected
                              });
                Dispose();
                return this.Store(result, result.ToList().Count);
            }
            return this.Store(null, 0);
        }
        bool CheckExist(EmployeeOTBO item)
        {
            var st = item.StartTime;
            var et = item.EndTime;
            var data = EmployeeOTService.GetAll().Where(u => ((u.StartTime >= st && u.StartTime <= et) || (u.EndTime >= st && u.EndTime <= et)) && u.Status != 2);
            return data.ToList().Count > 0;
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(EmployeeOTBO item)
        {
            var result = false;
            if (ModelState.IsValid && !CheckExist(item))
            {
                try
                {
                    var userId = EmployeeService.GetCurrentUser().Id;
                    var ID = Guid.NewGuid();
                    item.Id = ID;
                    item.EmployeeId = userId;
                    item.ApprovedBy = item.EmployeePerform.Id;
                    item.CreateBy = item.EmployeeId;
                    item.UpdateAt = null;
                    item.IsDelete = false;
                    EmployeeOTService.Insert(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            else
            {
                this.ShowNotify("Thời gian đăng ký đã tồn tại, đang chờ duyệt hoặc đã được duyệt!");
            }
            return this.Direct(result: result);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(EmployeeOTBO item, string forwardId = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    item.ApprovedBy = item.EmployeePerform.Id;
                    UpdateApproveState(item, forwardId);

                    var tracking = FormTracking(item.Id, out FormApproveStatusBO bo);
                    item.ApprovedBy = bo.ApprovedBy;
                    if (bo.isFinal) item.Status = bo.Status;
                    else item.Status = 0;
                    EmployeeOTService.Update(item);
                    result = true;

                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        [HttpPost]
        public ActionResult Delete(EmployeeOTBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    EmployeeOTService.Delete(item.Id);

                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        public bool FormTracking(Guid id, out FormApproveStatusBO tracking)
        {
            var stateList = FormApproveStatusService.GetByFormId(id);
            if (stateList.ToList().Count > 0)
            {
                tracking = stateList.OrderByDescending(u => u.CreateAt).First();
                return true;
            }
            tracking = new FormApproveStatusBO();
            return false;
        }

        public int GetCurrentStatus(Guid id)
        {
            var tracking = FormTracking(id, out FormApproveStatusBO currentStatus);
            if (tracking && currentStatus.isFinal) return currentStatus.Status;
            else return 0;
        }

        public ActionResult UpdateApproveState(EmployeeOTBO item, string forwardId = "")
        {
            try
            {
                var tableName = "EmployeeOTs";
                //cập nhật trạng thái cho bảng theo dõi trước
                var tracking = FormTracking(item.Id, out FormApproveStatusBO currentStatus);
                if (tracking)
                {
                    currentStatus.Status = item.Status;
                    currentStatus.SentBy = item.ApprovedBy;
                    //nếu đồng ý hoặc từ chối đơn và ko chuyển cho ai khác thì đóng theo dõi
                    if (item.Status != 0 && forwardId == "")
                    {
                        currentStatus.isFinal = true;
                        FormApproveStatusService.Update(currentStatus);
                        return this.Direct(true);
                    }

                    FormApproveStatusService.Update(currentStatus);
                }
                //thêm 1 record theo dõi mới
                var state = new FormApproveStatusBO()
                {
                    Id = Guid.NewGuid(),
                    FormId = item.Id,
                    TableName = tableName,
                    isFinal = (item.Status != 0 && forwardId == ""),
                    Status = item.Status,
                    SentBy = item.ApprovedBy ?? Guid.Empty,
                    ApprovedBy = Guid.TryParse(forwardId, out Guid gid) ? gid : item.ApprovedBy,
                    ApprovedAt = item.Status != 0 ? DateTime.Now : (DateTime?)null,
                    Reason = item.Reason,
                    CreateBy = item.EmployeeId,
                    CreateAt = DateTime.Now
                };

                FormApproveStatusService.Insert(state);
                Dispose();
            }
            catch (Exception)
            {
                throw;
            }
            return this.Direct(true);
        }
    }
}