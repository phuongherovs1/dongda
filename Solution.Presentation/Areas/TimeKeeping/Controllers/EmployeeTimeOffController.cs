﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Common;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.TimeKeeping.Controllers
{
    public class EmployeeTimeOffController : FrontController
    {
        #region View
        // GET: TimeKeeping/EmployeeTimeOffs
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult Form(string id = default)
        {
            var userId = EmployeeService.GetCurrentUser().Id;
            EmployeeTimeOffBO model;
            if (id == default)
            {
                
                ViewData.Add("Operation", Common.Operation.Create);
                model = new EmployeeTimeOffBO();
            }
            else
            {

                ViewData.Add("Operation", Common.Operation.Update);
                model = EmployeeTimeOffService.GetById(Guid.Parse(id));
                model.EmployeePerform = EmployeeService.GetById((Guid)model.ApprovedBy);
                model.Status = GetCurrentStatus(model.Id);

                var tracking = FormTracking(model.Id, out FormApproveStatusBO currentStatus);
                if (tracking)
                {
                    ViewData["isFinal"] = currentStatus.isFinal;
                    ViewData["sentBy"] = EmployeeService.GetById((Guid)currentStatus.SentBy).Id;
                }
            }
            model.UserId = userId;

            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion

        #region Method
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param, int Status = -1)
        {
            if (param is null)
            {
                throw new ArgumentNullException(nameof(param));
            }

            var userId = EmployeeService.GetCurrentUser().Id;
            var data = EmployeeTimeOffService.GetAll().Where(u => u.EmployeeId == userId);
            if (Status != -1) data = data.Where(u => u.Status == Status);

            if (data.ToList().Count > 0)
            {
                var result = (from EmployeeTimeOffBO bo in data
                              select new
                              {
                                  bo.Id,
                                  bo.EmployeeId,
                                  EmployeeName = EmployeeService.GetById(bo.EmployeeId).Name,
                                  DepartmentTitle = EmployeeTitleService.GetStringDepartmentRole(bo.EmployeeId),
                                  DepartmentName = "",
                                  bo.StartTime,
                                  bo.EndTime,
                                  bo.Reason,
                                  bo.EmployeeReplace,
                                  bo.ApprovedBy,
                                  EmployeeApproveName = EmployeeService.GetById((Guid)bo.ApprovedBy).Name,
                                  Status = GetCurrentStatus(bo.Id),
                                  StatusForView = GetCurrentStatus(bo.Id) == 0 ? Resource.Wait : GetCurrentStatus(bo.Id) == 1 ? Resource.Approved : Resource.Rejected
                              });
                return this.Store(result, result.ToList().Count);
            }
            return this.Store(null, 0);
        }

        [ValidateInput(false)]
        public ActionResult GetDataWaitApprove(StoreRequestParameters param, int Status = 0)
        {
            if (param is null)
            {
                throw new ArgumentNullException(nameof(param));
            }

            var userId = EmployeeService.GetCurrentUser().Id;
            var data = EmployeeTimeOffService.GetAll().Where(u => u.ApprovedBy == userId);
            if (Status != -1) data = data.Where(u => u.Status == Status);

            if (data.ToList().Count > 0)
            {
                var result = (from EmployeeTimeOffBO bo in data
                              select new
                              {
                                  bo.Id,
                                  bo.EmployeeId,
                                  EmployeeName = EmployeeService.GetById(bo.EmployeeId).Name,
                                  DepartmentTitle = EmployeeTitleService.GetStringDepartmentRole(bo.EmployeeId),
                                  DepartmentName = "",
                                  bo.StartTime,
                                  bo.EndTime,
                                  bo.Reason,
                                  bo.EmployeeReplace,
                                  bo.ApprovedBy,
                                  EmployeeApproveName = EmployeeService.GetById((Guid)bo.ApprovedBy).Name,
                                  Status = GetCurrentStatus(bo.Id),
                                  StatusForView = GetCurrentStatus(bo.Id) == 0 ? Resource.Wait : GetCurrentStatus(bo.Id) == 1 ? Resource.Approved : Resource.Rejected
                              });
                return this.Store(result, result.ToList().Count);
            }
            return this.Store(null, 0);
        }

        public ActionResult GetDataPluginsTimeOff(string id, int Status = 1)
        {
            if (string.IsNullOrEmpty(id))
                id = Guid.Empty.ToString();
            DataTable dataTable = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_EmployeeTimeOff_GetListData",
                    parameter: new
                    {
                        id = new Guid(id),
                        Status = Status
                    }
                );
            //var data = EmployeeTimeOffService.GetAll().Where(u => u.EmployeeId == Guid.Parse(id) && u.Status == Status);
            List<EmployeeTimeOffBO> data = new List<EmployeeTimeOffBO>();
            if(dataTable != null && dataTable.Rows.Count > 0)
            {
                foreach(DataRow item in dataTable.Rows)
                {
                    EmployeeTimeOffBO boitem = new EmployeeTimeOffBO();
                    boitem.Id = new Guid(item["Id"].ToString());
                    boitem.ApprovedBy = item["ApprovedBy"] != null && !string.IsNullOrEmpty(item["ApprovedBy"].ToString()) ? new Guid(item["ApprovedBy"].ToString()): Guid.Empty;
                    boitem.EmployeeId = new Guid(item["EmployeeId"].ToString());
                    if (item["StartTime"] != null && !string.IsNullOrEmpty(item["StartTime"].ToString()))
                        boitem.StartTime = DateTime.Parse(item["StartTime"].ToString());
                    if (item["EndTime"] != null && !string.IsNullOrEmpty(item["EndTime"].ToString()))
                        boitem.EndTime = DateTime.Parse(item["EndTime"].ToString());
                    boitem.Reason = item["Reason"].ToString();
                    boitem.EmployeeReplace = item["EmployeeReplace"].ToString();
                }
            }
            if (data.ToList().Count > 0)
            {
                var result = (from EmployeeTimeOffBO bo in data
                              select new
                              {
                                  bo.Id,
                                  bo.EmployeeId,
                                  EmployeeName = EmployeeService.GetById(bo.EmployeeId).Name,
                                  DepartmentTitle = EmployeeTitleService.GetStringDepartmentRole(bo.EmployeeId),
                                  DepartmentName = "",
                                  bo.StartTime,
                                  bo.EndTime,
                                  bo.Reason,
                                  bo.EmployeeReplace,
                                  bo.ApprovedBy,
                                  EmployeeApproveName = EmployeeService.GetById((Guid)bo.ApprovedBy).Name,
                                  Status = GetCurrentStatus(bo.Id),
                                  StatusForView = GetCurrentStatus(bo.Id) == 0 ? Resource.Wait : GetCurrentStatus(bo.Id) == 1 ? Resource.Approved : Resource.Rejected
                              });
                return this.Store(result, result.ToList().Count);
            }
            return this.Store(null, 0);
        }
        
        //public EmployeeTimeOffBO GetById(string id)
        //{
        //    var result = EmployeeTimeOffService.GetById(Guid.Parse(id));
        //    return result;
        //}

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(EmployeeTimeOffBO item)
        {
            var result = false;
            if (ModelState.IsValid && !DBConnect.CheckEsxitItem("sp_EmployeeTimeOff_CheckEsxit", "@Id", item.UserId.ToString(), "@StartTime", item.StartTime, "@EndTime", item.EndTime))
            {
                try
                {
                    var userId = EmployeeService.GetCurrentUser().Id;
                    OffDayBO offDayBO = OffDayController.GetByEmploueeId(userId.ToString());
                    if (offDayBO != null && offDayBO.OffDays - offDayBO.Using <= 0)
                    {
                        this.ShowNotify("Bạn đã hết số ngày được đăng ký nghỉ phép. Liên hệ bô phận nhân sự để biết thêm chi tiết!");
                    }
                    else
                    {
                        var ID = Guid.NewGuid();
                        item.Id = ID;
                        item.EmployeeId = userId;
                        item.ApprovedBy = item.EmployeePerform.Id;
                        item.CreateBy = item.EmployeeId;
                        item.UpdateAt = null;
                        item.IsDelete = false;
                        EmployeeTimeOffService.Insert(item);
                        Notify(TitleResourceNotifies.TimeOff, "Xin duyệt yêu cầu nghỉ phép ", item.EmployeePerform.Id, UrlResourceNotifies.TimeOff, new { id = ID.ToString() });
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            else
            {
                this.ShowNotify("Thời gian đăng ký đã tồn tại, đang chờ duyệt hoặc đã được duyệt!");
            }

            return this.Direct(result: result);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(EmployeeTimeOffBO item, string forwardId)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    item.ApprovedBy = item.EmployeePerform.Id;
                    UpdateApproveState(item, forwardId);

                    var tracking = FormTracking(item.Id, out FormApproveStatusBO bo);
                    item.ApprovedBy = bo.ApprovedBy;
                    if (bo.isFinal) item.Status = bo.Status;
                    else item.Status = 0;
                    EmployeeTimeOffService.Update(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        [HttpPost]
        public ActionResult Delete(EmployeeTimeOffBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    EmployeeTimeOffService.Delete(item.Id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        [HttpPost]
        public ActionResult Approve(EmployeeTimeOffBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
                    {
                        if (connection.State != ConnectionState.Open)
                            connection.Open();

                        using (var cmd = connection.CreateCommand())
                        {
                            cmd.CommandText = "sp_EmployeeTimeOff_Approve";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 200;
                            cmd.Parameters.AddWithValue("@id", item.Id);
                            cmd.Parameters.AddWithValue("@Status", item.Status);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        public bool FormTracking(Guid id, out FormApproveStatusBO tracking)
        {
            var stateList = FormApproveStatusService.GetByFormId(id);
            if (stateList.ToList().Count > 0)
            {
                tracking = stateList.OrderByDescending(u => u.CreateAt).First();
                return true;
            }
            tracking = new FormApproveStatusBO();
            return false;
        }

        public int GetCurrentStatus(Guid id)
        {
            var tracking = FormTracking(id, out FormApproveStatusBO currentStatus);
            if (tracking && currentStatus.isFinal) return currentStatus.Status;
            else return 0;
        }

        public ActionResult UpdateApproveState(EmployeeTimeOffBO item, string forwardId = "")
        {
            try
            {
                var tableName = "EmployeeTimeOffs";
                    //cập nhật trạng thái cho bảng theo dõi trước
                    var tracking = FormTracking(item.Id, out FormApproveStatusBO currentStatus);
                if (tracking)
                {
                    currentStatus.Status = item.Status;
                    currentStatus.SentBy = item.ApprovedBy;
                    //nếu đồng ý hoặc từ chối đơn và ko chuyển cho ai khác thì đóng theo dõi
                    if (item.Status != 0 && forwardId == "")
                    {
                        currentStatus.isFinal = true;
                        FormApproveStatusService.Update(currentStatus);
                        return this.Direct(true);
                    }

                    FormApproveStatusService.Update(currentStatus);
                }
                //thêm 1 record theo dõi mới
                var state = new FormApproveStatusBO()
                {
                    Id = Guid.NewGuid(),
                    FormId = item.Id,
                    TableName = tableName,
                    isFinal = (item.Status != 0 && forwardId == ""),
                    Status = item.Status,
                    SentBy = item.ApprovedBy ?? Guid.Empty,
                    ApprovedBy = Guid.TryParse(forwardId, out Guid gid) ? gid : item.ApprovedBy,
                    ApprovedAt = item.Status != 0 ? DateTime.Now : (DateTime?)null,
                    Reason = item.Reason,
                    CreateBy = item.EmployeeId,
                    CreateAt = DateTime.Now
                };

                FormApproveStatusService.Insert(state);
            }
            catch (Exception)
            {
                throw;
            }
            
            return this.Direct(true);
        }
        #endregion


    }
    public class EmployeeDepartmentTitle
    {
        public string EmployeeId { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentTitle { get; set; }
    }
}