﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using iDAS.Service.API.TimeKeeping;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.TimeKeeping.Controllers
{
    public class DashBoardController : FrontController
    {
        public string CurrentMonth { get; set; }
        public ActionResult DataHandling(bool isroleedit = false)
        {
            TimekeepingBO model = new TimekeepingBO();
            model.RoleEdit = isroleedit;
            return PartialView(model);
        }
        protected IPermissionService _IPermissionService { set; get; }
        public DashBoardController(IPermissionService permissionService)
        {
            _IPermissionService = permissionService;
        }
        public ActionResult FormList(string Code)
        {
            ViewData.Add("Code", Code);
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }
        public ActionResult Form(string id = "", int flag = 0 /* 1 = duyệt/chuyển tiếp yêu cầu giải trình*/)
        {
            TimekeepingBO model = new TimekeepingBO();
            ViewData.Add("Operation", Common.Operation.Update);
            model = GetById(id);
            model.ApproveEmployee = EmployeeService.GetById((Guid)model.ApproveId);
            if(GetCurrentStatus(model.Id) != -1) model.Status = GetCurrentStatus(model.Id);

            var tracking = FormTracking(model.Id, out FormApproveStatusBO currentStatus);
            if (tracking)
            {
                ViewData["isFinal"] = currentStatus.isFinal;
                ViewData["sentBy"] = EmployeeService.GetById((Guid)currentStatus.SentBy).Id;
            }
            if (flag != 0 || (model.Status == 2 && EmployeeService.GetCurrentUser().Id == model.ApproveId))
            {
                ViewData.Add("AllowForward", true);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult Update(TimekeepingBO item, string forwardTo = "")
        {
            var result = false;
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (item.Status == 3 || item.Status == 4)
                {
                    UpdateApproveState(item, forwardTo);

                    var tracking = FormTracking(item.Id, out FormApproveStatusBO bo);
                    item.ApproveId = bo.ApprovedBy;
                    if (bo.isFinal) item.Status = bo.Status;
                    else item.Status = 2;
                }

                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "TimeKeeping_UPDATE";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Id", item.Id);
                    cmd.Parameters.AddWithValue("@TimeIn", item.TimeIn);
                    cmd.Parameters.AddWithValue("@TimeOut", item.TimeOut);
                    cmd.Parameters.AddWithValue("@Status", item.Status);
                    cmd.Parameters.AddWithValue("@ReasonExplanation", !string.IsNullOrEmpty(item.ReasonExplanation) ? item.ReasonExplanation : "");
                    cmd.Parameters.AddWithValue("@ApprovedBy", (item.ApproveId != null && item.ApproveId != Guid.Empty) ? item.ApproveId : item.ApproveEmployee != new EmployeeBO()? item.ApproveEmployee.Id : Guid.Empty);
                    cmd.ExecuteNonQuery();
                    result = true;
                    Notify(item, forwardTo);
                    return this.Direct(result: result);
                }
            }
        }

        private void Notify(TimekeepingBO item, string forwardTo = "")
        {
            var url = "/TimeKeeping/DashBoard/Form?";
            var content = "Xin giải trình thời gian chấm công";
            if (item.Status == 2 && forwardTo == "")
            {
                Notify(TitleResourceNotifies.Explanation, content, item.ApproveEmployee.Id, url, new { id = item.Id.ToString(), flag = 1 });
            }
            else if (item.Status == 2 && Guid.TryParse(forwardTo, out Guid fId))
            {
                Notify(TitleResourceNotifies.Explanation, content, fId, url, new { id = item.Id.ToString(), flag = 1 });
            }

        }

        public ActionResult UpdateRow(string EmployeeCode, string cMonth, string timeList)
        {
            var timeJson = JsonConvert.DeserializeObject<List<dynamic>>(timeList);
            foreach (var t in timeJson)
            {
                var td = t.ToObject<Dictionary<string, string>>();
                string date = Convert.ToString(td["date"]);
                string tIn = Convert.ToString(td["timeIn"]);
                string tOut = Convert.ToString(td["timeOut"]);
                _IPermissionService.Update(EmployeeCode, cMonth, date, tIn, tOut);


            }
            return this.Direct(result: true);

        }
        public ActionResult RequestExplanation_Multi(string Code)
        {
            DateTime Month = DateTime.Now;
            if (Month.Day > 22)
                Month = Month.AddMonths(1);
            //Test với dữ liệu tháng 5
            Month = new DateTime(2019, 5, 1);
            Session["CurrentMonth"] = Month.ToString("dd/MM/yyyy");

            var claim = (ClaimsIdentity)User.Identity;
            var userId = claim.FindFirst(ClaimTypes.UserData).Value;
            if (string.IsNullOrEmpty(userId))
                userId = Guid.Empty.ToString();
            var user = UserService.GetById(new Guid(userId));

            var result = false;
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    var dataResult = new DataTable();
                    cmd.CommandText = "TimeKeeping_GetData_RequestExplanation";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Code", !string.IsNullOrEmpty(Code) ? Code : "");
                    cmd.Parameters.AddWithValue("@FromDate", new DateTime(Month.Year, Month.Month - 1, 23, 0, 0, 0).ToString("MM/dd/yyyy"));
                    cmd.Parameters.AddWithValue("@ToDate", new DateTime(Month.Year, Month.Month, 22, 0, 0, 0).ToString("MM/dd/yyyy"));
                    var da = new SqlDataAdapter(cmd);
                    da.Fill(dataResult);
                    if (dataResult.Rows.Count > 0)
                    {
                        for (int i = 0; i < dataResult.Rows.Count; i++)
                        {
                            TimekeepingBO item = new TimekeepingBO();
                            item.Id = new Guid(dataResult.Rows[i][0].ToString());
                            item.TimeIn = dataResult.Rows[i][1].ToString();
                            item.TimeOut = dataResult.Rows[i][2].ToString();
                            item.Date = DateTime.Parse(dataResult.Rows[i][3].ToString());

                            DateTime RoleIn = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 08:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                            DateTime RoleOut = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 17:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                            //Vào trễ
                            if (item.TimeIn != "V" && item.TimeIn != "F" && item.TimeIn != "--" && !string.IsNullOrEmpty(item.TimeIn))
                            {
                                DateTime In = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeIn + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                                if (In.Subtract(RoleIn).Minutes > 0)
                                {
                                    item.Reason += ",Vào trễ " + In.Subtract(RoleIn).TotalMinutes + " phút";
                                }
                            }
                            //Ra sớm
                            if (!string.IsNullOrEmpty(item.TimeOut))
                            {
                                DateTime Out = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeOut + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                                if (RoleOut.Subtract(Out).Minutes > 0)
                                {
                                    item.Reason += ", Ra sớm " + RoleOut.Subtract(Out).TotalMinutes + " phút";
                                }
                            }
                            //Vắng KP
                            if (item.TimeIn == "V")
                                item.Reason += ",Vắng không phép";

                            if (!string.IsNullOrEmpty(item.Reason))
                                RequestExplanation(item.Id.ToString(), Code, item.TimeIn, item.TimeOut);
                        }
                        using (var cmdUser = connection.CreateCommand())
                        {
                            var data = new DataTable();
                            cmdUser.CommandText = "sp_EmployeesId_GetByCode";
                            cmdUser.CommandType = CommandType.StoredProcedure;
                            cmdUser.CommandTimeout = 200;
                            cmdUser.Parameters.AddWithValue("@Code", Code);
                            var daOut = new SqlDataAdapter(cmdUser);
                            daOut.Fill(data);
                            Notify(TitleResourceNotifies.Explanation, user.Name + " yêu cầu anh/chị giải trình chấm công tháng " + Month.ToString("MM/yyyy"), new Guid(data.Rows[0][0].ToString()), UrlResourceNotifies.Explanation, new { id = data.Rows[0][0].ToString(), month = Month.ToString("dd/MM/yyyy") });
                            this.ShowNotify("Yêu cầu giải trình thành công");
                        }
                    }
                    this.ShowNotify("Không có vi phạm để yêu cầu giải trình hoặc đã yêu cầu giải trình với nhân viên này!");
                    return this.Direct(result: result);
                }
            }
        }
        public void RequestExplanation(string id, string Code, string TimeIn, string TimeOut)
        {
            if (string.IsNullOrEmpty(Code))
                Code = Session["Code"].ToString();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var cmdT = connection.CreateCommand())
                {
                    cmdT.CommandText = "TimeKeeping_RequestExplanation";
                    cmdT.CommandType = CommandType.StoredProcedure;
                    cmdT.CommandTimeout = 200;
                    cmdT.Parameters.AddWithValue("@Id", id);
                    cmdT.Parameters.AddWithValue("@Status", 1);
                    cmdT.Parameters.AddWithValue("@TimeIn", TimeIn);
                    cmdT.Parameters.AddWithValue("@TimeOut", TimeOut);
                    cmdT.ExecuteNonQuery();
                }
            }
        }
        public TimekeepingBO GetById(string Id)
        {
            TimekeepingBO data = new TimekeepingBO();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    var dataResult = new DataTable();
                    cmd.CommandText = "TimeKeeping_GetById";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Id", Guid.TryParse(Id, out Guid gid) ? Id : Guid.Empty.ToString());
                    var da = new SqlDataAdapter(cmd);
                    da.Fill(dataResult);
                    data.Id = new Guid(dataResult.Rows[0][0].ToString());
                    data.Code = dataResult.Rows[0][1].ToString();
                    data.Name = dataResult.Rows[0][2].ToString();
                    data.Date = DateTime.Parse(dataResult.Rows[0][3].ToString());
                    data.TimeIn = dataResult.Rows[0][4].ToString();
                    data.TimeOut = dataResult.Rows[0][5].ToString();
                    data.Status = !string.IsNullOrEmpty(dataResult.Rows[0][6].ToString()) ? (int)dataResult.Rows[0][6] : 0;
                    data.ReasonExplanation = dataResult.Rows[0][7].ToString();
                    data.ApproveId = !string.IsNullOrEmpty(dataResult.Rows[0][8].ToString()) ? new Guid(dataResult.Rows[0][8].ToString()) : Guid.Empty;
                    return data;
                }
            }
        }
        public ActionResult GetDataPluginsTimeKeeping(StoreRequestParameters param, string Code = default(string))
        {
            DateTime Month = DateTime.MinValue;
            Session["Code"] = Code;
            if (!string.IsNullOrEmpty(Session["CurrentMonth"].ToString()))
            {
                Month = DateTime.Parse(Session["CurrentMonth"].ToString());
            }
            List<TimekeepingBO> data = new List<TimekeepingBO>();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    var dataResult = new DataTable();
                    cmd.CommandText = "TimeKeeping_GetData";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Code", !string.IsNullOrEmpty(Code) ? Code : "");
                    cmd.Parameters.AddWithValue("@FromDate", Month.AddMonths(-1).ToString("MM/23/yyyy"));
                    cmd.Parameters.AddWithValue("@ToDate", Month.ToString("MM/22/yyyy"));
                    var da = new SqlDataAdapter(cmd);
                    da.Fill(dataResult);
                    for (int i = 0; i < dataResult.Rows.Count; i++)
                    {
                        TimekeepingBO item = new TimekeepingBO();
                        item.Id = new Guid(dataResult.Rows[i][0].ToString());
                        item.Code = dataResult.Rows[i][1].ToString();
                        item.Name = dataResult.Rows[i][2].ToString();
                        item.Date = DateTime.Parse(dataResult.Rows[i][3].ToString());
                        item.TimeIn = dataResult.Rows[i][4].ToString();
                        item.TimeOut = dataResult.Rows[i][5].ToString();
                        item.Status = !string.IsNullOrEmpty(dataResult.Rows[i][6].ToString()) ? (int)dataResult.Rows[i][6] : 0;
                        item.ReasonExplanation = dataResult.Rows[i][7].ToString();
                        item.ApproveId = !string.IsNullOrEmpty(dataResult.Rows[i][8].ToString()) ? new Guid(dataResult.Rows[i][8].ToString()) : Guid.Empty;

                        DateTime RoleIn = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 08:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                        DateTime RoleOut = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 17:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                        //Vào trễ
                        if (item.TimeIn != "V" && item.TimeIn != "F" && item.TimeIn != "--" && !string.IsNullOrEmpty(item.TimeIn))
                        {
                            DateTime In = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeIn + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                            if (In.Subtract(RoleIn).Minutes > 0)
                            {
                                item.Reason += ",Vào trễ " + In.Subtract(RoleIn).TotalMinutes + " phút";
                            }
                        }
                        //Ra sớm
                        if (!string.IsNullOrEmpty(item.TimeOut))
                        {
                            DateTime Out = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeOut + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                            if (RoleOut.Subtract(Out).Minutes > 0)
                            {
                                item.Reason += ", Ra sớm " + RoleOut.Subtract(Out).TotalMinutes + " phút";
                            }
                        }
                        //Vắng KP
                        if (item.TimeIn == "V")
                            item.Reason += ",Vắng không phép";
                        if (!string.IsNullOrEmpty(item.Reason))
                            item.Reason = item.Reason.Substring(1);
                        data.Add(item);
                    }
                    return this.Store(data);
                }
            }
        }
        public ActionResult GetDataNotHanding(StoreRequestParameters param, string curMonth = default(string), string Code = default(string))
        {
            DateTime Month = DateTime.MinValue;
            if (!string.IsNullOrEmpty(curMonth))
            {
                Month = DateTime.Parse(curMonth);
                Session["CurrentMonth"] = curMonth;
            }
            List<TimekeepingBO> data = new List<TimekeepingBO>();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    var dataResult = new DataTable();
                    cmd.CommandText = "TimeKeeping_GetData";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Code", !string.IsNullOrEmpty(Code) ? Code : "");
                    cmd.Parameters.AddWithValue("@FromDate", Month.AddMonths(-1).ToString("yyyy-MM-23"));
                    cmd.Parameters.AddWithValue("@ToDate", Month.ToString("yyyy-MM-22"));
                    var da = new SqlDataAdapter(cmd);
                    da.Fill(dataResult);
                    for (int i = 0; i < dataResult.Rows.Count; i++)
                    {
                        TimekeepingBO item = new TimekeepingBO();
                        item.Id = new Guid(dataResult.Rows[i][0].ToString());
                        item.Code = dataResult.Rows[i][1] != null && !string.IsNullOrEmpty(dataResult.Rows[i][1].ToString()) ? dataResult.Rows[i][1].ToString():"";
                        item.Name = dataResult.Rows[i][2] != null && !string.IsNullOrEmpty(dataResult.Rows[i][2].ToString()) ? dataResult.Rows[i][2].ToString(): "";
                        item.Date = dataResult.Rows[i][3] != null && !string.IsNullOrEmpty(dataResult.Rows[i][3].ToString()) ? DateTime.Parse(dataResult.Rows[i][3].ToString()) : DateTime.MinValue;
                        item.TimeIn = dataResult.Rows[i][4] != null && !string.IsNullOrEmpty(dataResult.Rows[i][4].ToString()) ? dataResult.Rows[i][4].ToString(): "";
                        item.TimeOut = dataResult.Rows[i][5] != null && !string.IsNullOrEmpty(dataResult.Rows[i][5].ToString()) ? dataResult.Rows[i][5].ToString(): "";
                        data.Add(item);
                    }
                    List<string> LstCode = data.Select(x => x.Code).Distinct().ToList();
                    List<DataView> dataViews = new List<DataView>();
                    foreach (string item in LstCode)
                        dataViews.Add(new DataView() { CodeView = item, NameOnView = data.First(x => x.Code == item).Name });
                    foreach (DataView item in dataViews)
                    {
                        try
                        {
                            List<TimekeepingBO> timekeepingitem = data.Where(x => x.Code == item.CodeView).OrderBy(x => x.Date.Value).ToList();

                            for (int i = 0; i < timekeepingitem.Count; i++)
                            {
                                switch (timekeepingitem[i].Date.Value.Day)
                                {
                                    case 1:
                                        item._01_In = timekeepingitem[i].TimeIn;
                                        item._01_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 2:
                                        item._02_In = timekeepingitem[i].TimeIn;
                                        item._02_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 3:
                                        item._03_In = timekeepingitem[i].TimeIn;
                                        item._03_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 4:
                                        item._04_In = timekeepingitem[i].TimeIn;
                                        item._04_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 5:
                                        item._05_In = timekeepingitem[i].TimeIn;
                                        item._05_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 6:
                                        item._06_In = timekeepingitem[i].TimeIn;
                                        item._06_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 7:
                                        item._07_In = timekeepingitem[i].TimeIn;
                                        item._07_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 8:
                                        item._08_In = timekeepingitem[i].TimeIn;
                                        item._08_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 9:
                                        item._09_In = timekeepingitem[i].TimeIn;
                                        item._09_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 10:
                                        item._10_In = timekeepingitem[i].TimeIn;
                                        item._10_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 11:
                                        item._11_In = timekeepingitem[i].TimeIn;
                                        item._11_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 12:
                                        item._12_In = timekeepingitem[i].TimeIn;
                                        item._12_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 13:
                                        item._13_In = timekeepingitem[i].TimeIn;
                                        item._13_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 14:
                                        item._14_In = timekeepingitem[i].TimeIn;
                                        item._14_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 15:
                                        item._15_In = timekeepingitem[i].TimeIn;
                                        item._15_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 16:
                                        item._16_In = timekeepingitem[i].TimeIn;
                                        item._16_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 17:
                                        item._17_In = timekeepingitem[i].TimeIn;
                                        item._17_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 18:
                                        item._18_In = timekeepingitem[i].TimeIn;
                                        item._18_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 19:
                                        item._19_In = timekeepingitem[i].TimeIn;
                                        item._19_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 20:
                                        item._20_In = timekeepingitem[i].TimeIn;
                                        item._20_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 21:
                                        item._21_In = timekeepingitem[i].TimeIn;
                                        item._21_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 22:
                                        item._22_In = timekeepingitem[i].TimeIn;
                                        item._22_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 23:
                                        item._23_In = timekeepingitem[i].TimeIn;
                                        item._23_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 24:
                                        item._24_In = timekeepingitem[i].TimeIn;
                                        item._24_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 25:
                                        item._25_In = timekeepingitem[i].TimeIn;
                                        item._25_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 26:
                                        item._26_In = timekeepingitem[i].TimeIn;
                                        item._26_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 27:
                                        item._27_In = timekeepingitem[i].TimeIn;
                                        item._27_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 28:
                                        item._28_In = timekeepingitem[i].TimeIn;
                                        item._28_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 29:
                                        item._29_In = timekeepingitem[i].TimeIn;
                                        item._29_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 30:
                                        item._30_In = timekeepingitem[i].TimeIn;
                                        item._30_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 31:
                                        item._31_In = timekeepingitem[i].TimeIn;
                                        item._31_Out = timekeepingitem[i].TimeOut;
                                        break;
                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                    return this.Store(dataViews, dataViews.Count);
                }
            }
        }

        public ActionResult GetData(StoreRequestParameters param, string curMonth = default(string), string Code = default(string))
        {
            DateTime Month = DateTime.MinValue;
            if (!string.IsNullOrEmpty(curMonth))
            {
                Month = DateTime.Parse(curMonth);
                Session["CurrentMonth"] = curMonth;
            }
            List<TimekeepingBO> data = new List<TimekeepingBO>();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    var dataResult = new DataTable();
                    cmd.CommandText = "TimeKeeping_GetData";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Code", !string.IsNullOrEmpty(Code) ? Code : "");
                    cmd.Parameters.AddWithValue("@FromDate", Month.AddMonths(-1).ToString("MM/23/yyyy"));
                    cmd.Parameters.AddWithValue("@ToDate", Month.ToString("MM/22/yyyy"));
                    var da = new SqlDataAdapter(cmd);
                    da.Fill(dataResult);
                    for (int i = 0; i < dataResult.Rows.Count; i++)
                    {
                        TimekeepingBO item = new TimekeepingBO();
                        item.Id = new Guid(dataResult.Rows[i][0].ToString());
                        item.Code = dataResult.Rows[i][1].ToString();
                        item.Name = dataResult.Rows[i][2].ToString();
                        item.Date = DateTime.Parse(dataResult.Rows[i][3].ToString());
                        item.TimeIn = dataResult.Rows[i][4].ToString();
                        item.TimeOut = dataResult.Rows[i][5].ToString();
                        data.Add(item);
                    }
                    List<DataView> dataViews = GetDataViews(data);
                    foreach (DataView item in dataViews)
                    {
                        try
                        {
                            List<TimekeepingBO> timekeepingitem = data.Where(x => x.Code == item.CodeView).OrderBy(x => x.Date.Value).ToList();
                            for (int i = 0; i < timekeepingitem.Count; i++)
                            {
                                switch (timekeepingitem[i].Date.Value.Day)
                                {
                                    case 1:
                                        item._01_In = timekeepingitem[i].TimeIn;
                                        item._01_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 2:
                                        item._02_In = timekeepingitem[i].TimeIn;
                                        item._02_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 3:
                                        item._03_In = timekeepingitem[i].TimeIn;
                                        item._03_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 4:
                                        item._04_In = timekeepingitem[i].TimeIn;
                                        item._04_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 5:
                                        item._05_In = timekeepingitem[i].TimeIn;
                                        item._05_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 6:
                                        item._06_In = timekeepingitem[i].TimeIn;
                                        item._06_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 7:
                                        item._07_In = timekeepingitem[i].TimeIn;
                                        item._07_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 8:
                                        item._08_In = timekeepingitem[i].TimeIn;
                                        item._08_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 9:
                                        item._09_In = timekeepingitem[i].TimeIn;
                                        item._09_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 10:
                                        item._10_In = timekeepingitem[i].TimeIn;
                                        item._10_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 11:
                                        item._11_In = timekeepingitem[i].TimeIn;
                                        item._11_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 12:
                                        item._12_In = timekeepingitem[i].TimeIn;
                                        item._12_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 13:
                                        item._13_In = timekeepingitem[i].TimeIn;
                                        item._13_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 14:
                                        item._14_In = timekeepingitem[i].TimeIn;
                                        item._14_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 15:
                                        item._15_In = timekeepingitem[i].TimeIn;
                                        item._15_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 16:
                                        item._16_In = timekeepingitem[i].TimeIn;
                                        item._16_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 17:
                                        item._17_In = timekeepingitem[i].TimeIn;
                                        item._17_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 18:
                                        item._18_In = timekeepingitem[i].TimeIn;
                                        item._18_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 19:
                                        item._19_In = timekeepingitem[i].TimeIn;
                                        item._19_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 20:
                                        item._20_In = timekeepingitem[i].TimeIn;
                                        item._20_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 21:
                                        item._21_In = timekeepingitem[i].TimeIn;
                                        item._21_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 22:
                                        item._22_In = timekeepingitem[i].TimeIn;
                                        item._22_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 23:
                                        item._23_In = timekeepingitem[i].TimeIn;
                                        item._23_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 24:
                                        item._24_In = timekeepingitem[i].TimeIn;
                                        item._24_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 25:
                                        item._25_In = timekeepingitem[i].TimeIn;
                                        item._25_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 26:
                                        item._26_In = timekeepingitem[i].TimeIn;
                                        item._26_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 27:
                                        item._27_In = timekeepingitem[i].TimeIn;
                                        item._27_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 28:
                                        item._28_In = timekeepingitem[i].TimeIn;
                                        item._28_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 29:
                                        item._29_In = timekeepingitem[i].TimeIn;
                                        item._29_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 30:
                                        item._30_In = timekeepingitem[i].TimeIn;
                                        item._30_Out = timekeepingitem[i].TimeOut;
                                        break;
                                    case 31:
                                        item._31_In = timekeepingitem[i].TimeIn;
                                        item._31_Out = timekeepingitem[i].TimeOut;
                                        break;
                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }

                    }
                    return this.Store(dataViews, dataViews.Count);
                }
            }
        }
        public List<DataView> GetDataViews(List<TimekeepingBO> data)
        {
            List<DataView> dataViews = new List<DataView>();
            string code = "";
            DataView view = new DataView();
            int i = 0;
            foreach (TimekeepingBO item in data)
            {
                i++;
                if (string.IsNullOrEmpty(code) || (!string.IsNullOrEmpty(code) && item.Code == code))
                {
                    if (i == data.Count)
                    {
                        code = item.Code;
                        view.CodeView = code;
                        view.NameOnView = item.Name;
                        DateTime RoleIn = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 08:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                        DateTime RoleOut = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 17:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                        //Ngày công and Giờ công
                        if (item.TimeIn != "V" && item.TimeIn != "F" && item.TimeIn != "--" && !string.IsNullOrEmpty(item.TimeIn) && !string.IsNullOrEmpty(item.TimeOut))
                        {
                            DateTime In = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeIn + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                            DateTime Out = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeOut + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                            //Ngày công
                            if (Common.Utilities.GetNameDayOfWeek(In) == "Chủ nhật" || Common.Utilities.GetNameDayOfWeek(In) == "Thứ bảy")
                                view.NgayCong_CT += 1;
                            else
                                view.NgayCong_NT += 1;
                            //Giờ công
                            if (Common.Utilities.GetNameDayOfWeek(In) == "Chủ nhật" || Common.Utilities.GetNameDayOfWeek(In) == "Thứ bảy")
                                view.GioCong_CT = (int)Out.Subtract(In).TotalHours;
                            else
                                view.GioCong_NT = (int)Out.Subtract(In).TotalHours;
                        }
                        //Vào trễ
                        if (item.TimeIn != "V" && item.TimeIn != "F" && item.TimeIn != "--" && !string.IsNullOrEmpty(item.TimeIn))
                        {
                            DateTime In = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeIn + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                            if (In.Subtract(RoleIn).TotalMinutes > 0)
                            {
                                view.VaoTre_Lan += 1;
                                view.VaoTre_Phut += (int)In.Subtract(RoleIn).TotalMinutes;
                            }
                        }
                        //Ra sớm
                        if (!string.IsNullOrEmpty(item.TimeOut))
                        {
                            DateTime Out = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeOut + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                            if (RoleOut.Subtract(Out).TotalMinutes > 0)
                            {
                                view.RaSom_Lan += 1;
                                view.RaSom_Phut += (int)RoleOut.Subtract(Out).TotalMinutes;
                            }
                        }
                        //Vắng KP
                        if (item.TimeIn == "V")
                            view.VangKP += 1;
                        //tăng ca
                        if (item.TimeIn == "TC1")
                            view.TangCa_TC1 += 1;
                        else if (item.TimeIn == "TC2")
                            view.TangCa_TC2 += 1;
                        else if (item.TimeIn == "TC3")
                            view.TangCa_TC3 += 1;

                        if (item.TimeIn == "OM")
                            view.OM += 1;
                        if (item.TimeIn == "TS")
                            view.TS += 1;
                        if (item.TimeIn == "R")
                            view.R += 1;
                        if (item.TimeIn == "Ro")
                            view.Ro += 1;
                        if (item.TimeIn == "P")
                            view.P += 1;
                        if (item.TimeIn == "F")
                            view.F += 1;
                        if (item.TimeIn == "CO")
                            view.CO += 1;
                        if (item.TimeIn == "CD")
                            view.CD += 1;
                        if (item.TimeIn == "H")
                            view.H += 1;
                        if (item.TimeIn == "CT")
                            view.CT += 1;
                        if (item.TimeIn == "Le")
                            view.Le += 1;
                        dataViews.Add(view);
                    }
                    else
                    {
                        code = item.Code;
                        view.CodeView = code;
                        view.NameOnView = item.Name;
                        DateTime RoleIn = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 08:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                        DateTime RoleOut = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 17:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                        //Ngày công and Giờ công
                        if (item.TimeIn != "V" && item.TimeIn != "F" && item.TimeIn != "--" && !string.IsNullOrEmpty(item.TimeIn) && !string.IsNullOrEmpty(item.TimeOut))
                        {
                            DateTime In = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeIn + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                            DateTime Out = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeOut + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                            //Ngày công
                            if (Common.Utilities.GetNameDayOfWeek(In) == "Chủ nhật" || Common.Utilities.GetNameDayOfWeek(In) == "Thứ bảy")
                                view.NgayCong_CT += 1;
                            else
                                view.NgayCong_NT += 1;
                            //Giờ công
                            if (Common.Utilities.GetNameDayOfWeek(In) == "Chủ nhật" || Common.Utilities.GetNameDayOfWeek(In) == "Thứ bảy")
                                view.GioCong_CT = (int)Out.Subtract(In).TotalHours;
                            else
                                view.GioCong_NT = (int)Out.Subtract(In).TotalHours;
                        }
                        //Vào trễ
                        if (item.TimeIn != "V" && item.TimeIn != "F" && item.TimeIn != "--" && !string.IsNullOrEmpty(item.TimeIn))
                        {
                            DateTime In = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeIn + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                            if (In.Subtract(RoleIn).TotalMinutes > 0)
                            {
                                view.VaoTre_Lan += 1;
                                view.VaoTre_Phut += (int)In.Subtract(RoleIn).TotalMinutes;
                            }
                        }
                        //Ra sớm
                        if (!string.IsNullOrEmpty(item.TimeOut))
                        {
                            DateTime Out = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeOut + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                            if (RoleOut.Subtract(Out).TotalMinutes > 0)
                            {
                                view.RaSom_Lan += 1;
                                view.RaSom_Phut += (int)RoleOut.Subtract(Out).TotalMinutes;
                            }
                        }
                        //Vắng KP
                        if (item.TimeIn == "V")
                            view.VangKP += 1;
                        //tăng ca
                        if (item.TimeIn == "TC1")
                            view.TangCa_TC1 += 1;
                        else if (item.TimeIn == "TC2")
                            view.TangCa_TC2 += 1;
                        else if (item.TimeIn == "TC3")
                            view.TangCa_TC3 += 1;

                        if (item.TimeIn == "OM")
                            view.OM += 1;
                        if (item.TimeIn == "TS")
                            view.TS += 1;
                        if (item.TimeIn == "R")
                            view.R += 1;
                        if (item.TimeIn == "Ro")
                            view.Ro += 1;
                        if (item.TimeIn == "P")
                            view.P += 1;
                        if (item.TimeIn == "F")
                            view.F += 1;
                        if (item.TimeIn == "CO")
                            view.CO += 1;
                        if (item.TimeIn == "CD")
                            view.CD += 1;
                        if (item.TimeIn == "H")
                            view.H += 1;
                        if (item.TimeIn == "CT")
                            view.CT += 1;
                        if (item.TimeIn == "Le")
                            view.Le += 1;
                    }
                }
                else
                {
                    code = item.Code;
                    dataViews.Add(view);
                    view = new DataView();
                    view.CodeView = code;
                    view.NameOnView = item.Name;
                    DateTime RoleIn = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 08:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                    DateTime RoleOut = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy 17:00:00"), "dd/MM/yyyy HH:mm:ss", null);
                    //Ngày công and Giờ công
                    if (item.TimeIn != "V" && item.TimeIn != "F" && item.TimeIn != "--" && !string.IsNullOrEmpty(item.TimeIn) && !string.IsNullOrEmpty(item.TimeOut))
                    {
                        DateTime In = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeIn + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                        DateTime Out = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeOut + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                        //Ngày công
                        if (Common.Utilities.GetNameDayOfWeek(In) == "Chủ nhật" || Common.Utilities.GetNameDayOfWeek(In) == "Thứ bảy")
                            view.NgayCong_CT += 1;
                        else
                            view.NgayCong_NT += 1;
                        //Giờ công
                        if (Common.Utilities.GetNameDayOfWeek(In) == "Chủ nhật" || Common.Utilities.GetNameDayOfWeek(In) == "Thứ bảy")
                            view.GioCong_CT = (int)Out.Subtract(In).TotalHours;
                        else
                            view.GioCong_NT = (int)Out.Subtract(In).TotalHours;
                    }
                    //Vào trễ
                    if (item.TimeIn != "V" && item.TimeIn != "F" && item.TimeIn != "--" && !string.IsNullOrEmpty(item.TimeIn))
                    {
                        DateTime In = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeIn + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                        if (In.Subtract(RoleIn).TotalMinutes > 0)
                        {
                            view.VaoTre_Lan += 1;
                            view.VaoTre_Phut += (int)In.Subtract(RoleIn).TotalMinutes;
                        }
                    }
                    //Ra sớm
                    if (!string.IsNullOrEmpty(item.TimeOut))
                    {
                        DateTime Out = DateTime.ParseExact(item.Date.Value.ToString("dd/MM/yyyy " + item.TimeOut + ":00"), "dd/MM/yyyy HH:mm:ss", null);
                        if (RoleOut.Subtract(Out).TotalMinutes > 0)
                        {
                            view.RaSom_Lan += 1;
                            view.RaSom_Phut += (int)RoleOut.Subtract(Out).TotalMinutes;
                        }
                    }
                    //Vắng KP
                    if (item.TimeIn == "V")
                        view.VangKP += 1;
                    //tăng ca
                    if (item.TimeIn == "TC1")
                        view.TangCa_TC1 += 1;
                    else if (item.TimeIn == "TC2")
                        view.TangCa_TC2 += 1;
                    else if (item.TimeIn == "TC3")
                        view.TangCa_TC3 += 1;

                    if (item.TimeIn == "OM")
                        view.OM += 1;
                    if (item.TimeIn == "TS")
                        view.TS += 1;
                    if (item.TimeIn == "R")
                        view.R += 1;
                    if (item.TimeIn == "Ro")
                        view.Ro += 1;
                    if (item.TimeIn == "P")
                        view.P += 1;
                    if (item.TimeIn == "F")
                        view.F += 1;
                    if (item.TimeIn == "CO")
                        view.CO += 1;
                    if (item.TimeIn == "CD")
                        view.CD += 1;
                    if (item.TimeIn == "H")
                        view.H += 1;
                    if (item.TimeIn == "CT")
                        view.CT += 1;
                    if (item.TimeIn == "Le")
                        view.Le += 1;
                }
            }
            return dataViews;
        }
        public bool FormTracking(Guid id, out FormApproveStatusBO tracking)
        {
            var stateList = FormApproveStatusService.GetByFormId(id);
            if (stateList.ToList().Count > 0)
            {
                tracking = stateList.OrderByDescending(u => u.CreateAt).First();
                return true;
            }
            tracking = new FormApproveStatusBO();
            return false;
        }

        public int GetCurrentStatus(Guid id)
        {
            var tracking = FormTracking(id, out FormApproveStatusBO currentStatus);
            if (tracking && currentStatus.isFinal) return currentStatus.Status;
            else return -1;
        }

        public ActionResult UpdateApproveState(TimekeepingBO item, string forwardId = "")
        {
            try
            {
                var tableName = "Timekeeping";
                //cập nhật trạng thái cho bảng theo dõi trước
                var tracking = FormTracking(item.Id, out FormApproveStatusBO currentStatus);
                if (tracking)
                {
                    currentStatus.Status = item.Status;
                    currentStatus.SentBy = item.ApproveEmployee.Id;
                    //nếu đồng ý hoặc từ chối đơn và ko chuyển cho ai khác thì đóng theo dõi
                    if ((item.Status == 3 && forwardId == "") || item.Status == 4)
                    {
                        currentStatus.isFinal = true;
                        FormApproveStatusService.Update(currentStatus);
                        return this.Direct(true);
                    }

                    FormApproveStatusService.Update(currentStatus);
                }
                //thêm 1 record theo dõi mới
                var state = new FormApproveStatusBO()
                {
                    Id = Guid.NewGuid(),
                    FormId = item.Id,
                    TableName = tableName,
                    isFinal = ((item.Status == 3 && forwardId == "")|| item.Status == 4),
                    Status = item.Status,
                    SentBy = (Guid?)item.ApproveEmployee.Id ?? Guid.Empty,
                    ApprovedBy = Guid.TryParse(forwardId, out Guid gid) ? gid : item.ApproveEmployee.Id,
                    ApprovedAt = (item.Status == 3 || item.Status == 4) ? DateTime.Now : (DateTime?)null,
                    Reason = item.ReasonExplanation,
                    CreateBy = item.EmployeeId,
                    CreateAt = DateTime.Now
                };

                FormApproveStatusService.Insert(state);
            }
            catch (Exception)
            {
                throw;
            }

            return this.Direct(true);
        }
    }
    public class DataView
    {
        public string _23_In = "";
        public string _24_In = "";
        public string _25_In = "";
        public string _26_In = "";
        public string _27_In = "";
        public string _28_In = "";
        public string _29_In = "";
        public string _30_In = "";
        public string _31_In = "";
        public string _01_In = "";
        public string _02_In = "";
        public string _03_In = "";
        public string _04_In = "";
        public string _05_In = "";
        public string _06_In = "";
        public string _07_In = "";
        public string _08_In = "";
        public string _09_In = "";
        public string _10_In = "";
        public string _11_In = "";
        public string _12_In = "";
        public string _13_In = "";
        public string _14_In = "";
        public string _15_In = "";
        public string _16_In = "";
        public string _17_In = "";
        public string _18_In = "";
        public string _19_In = "";
        public string _20_In = "";
        public string _21_In = "";
        public string _22_In = "";

        public string _23_Out = "";
        public string _24_Out = "";
        public string _25_Out = "";
        public string _26_Out = "";
        public string _27_Out = "";
        public string _28_Out = "";
        public string _29_Out = "";
        public string _30_Out = "";
        public string _31_Out = "";
        public string _01_Out = "";
        public string _02_Out = "";
        public string _03_Out = "";
        public string _04_Out = "";
        public string _05_Out = "";
        public string _06_Out = "";
        public string _07_Out = "";
        public string _08_Out = "";
        public string _09_Out = "";
        public string _10_Out = "";
        public string _11_Out = "";
        public string _12_Out = "";
        public string _13_Out = "";
        public string _14_Out = "";
        public string _15_Out = "";
        public string _16_Out = "";
        public string _17_Out = "";
        public string _18_Out = "";
        public string _19_Out = "";
        public string _20_Out = "";
        public string _21_Out = "";
        public string _22_Out = "";

        public string NameOnView = "";
        public string CodeView = "";
        public int NgayCong_NT = 0;
        public int NgayCong_CT = 0;
        public int GioCong_NT = 0;
        public int GioCong_CT = 0;
        public int VaoTre_Lan = 0;
        public int VaoTre_Phut = 0;
        public int RaSom_Lan = 0;
        public int RaSom_Phut = 0;
        public int TangCa_TC1 = 0;
        public int TangCa_TC2 = 0;
        public int TangCa_TC3 = 0;
        public int VangKP = 0;
        public int OM = 0;
        public int TS = 0;
        public int R = 0;
        public int Ro = 0;
        public int P = 0;
        public int F = 0;
        public int CO = 0;
        public int CD = 0;
        public int H = 0;
        public int CT = 0;
        public int Le = 0;
    }
}