﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Common;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.TimeKeeping.Controllers
{
    public class OffDayController : FrontController
    {
        #region View
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult Form(string id = default(string))
        {
            OffDayBO model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new OffDayBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = GetById(id.ToString());
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion

        private DataTable Employees;
        private DataTable EmployeeDepartment;
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param)
        {
            DBConnect dbconnect = new DBConnect();
            List<OffDayBO> employeeTimeOffs = new List<OffDayBO>();
            List<EmployeeDepartmentTitle> employeeDepartmentTitles = new List<EmployeeDepartmentTitle>();
            DataSet getDataAll;
            getDataAll = dbconnect.myDataset("sp_OffDays_GetData");
            Employees = getDataAll.Tables[0];
            EmployeeDepartment = getDataAll.Tables[1];
            for (int i = 0; i < EmployeeDepartment.Rows.Count; i++)
            {
                EmployeeDepartmentTitle item = new EmployeeDepartmentTitle();
                item.EmployeeId = EmployeeDepartment.Rows[i]["EmployeeId"].ToString();
                item.DepartmentName = EmployeeDepartment.Rows[i]["DepartmentName"].ToString();
                item.DepartmentTitle = EmployeeDepartment.Rows[i]["DepartmentTitle"].ToString();
                employeeDepartmentTitles.Add(item);
            }
            //string departmentName = string.Join(", ", employeeDepartmentTitles.Select(x => x.DepartmentName).ToArray());
            //string departmentTitle = string.Join(", ", employeeDepartmentTitles.Select(x => x.DepartmentTitle).ToArray());

            for (int i = 0; i < Employees.Rows.Count; i++)
            {
                OffDayBO item = new OffDayBO();
                item.Id = new Guid(Employees.Rows[i]["Id"].ToString());
                item.EmployeeId = new Guid(Employees.Rows[i]["EmployeeId"].ToString());
                item.EmployeeName = Employees.Rows[i]["EmployeeName"].ToString();
                //item.DepartmentName = string.Join(", ", employeeDepartmentTitles.Where(x => x.EmployeeId == item.EmployeeId.Value.ToString()).Select(x => x.DepartmentName).ToArray());
                item.DepartmentTitle = string.Join("<br/>", employeeDepartmentTitles.Where(x => x.EmployeeId == item.EmployeeId.Value.ToString()).Select(x => x.DepartmentName + " - " + x.DepartmentTitle).ToArray());
                item.Using = (int)Employees.Rows[i]["Using"];
                item.OffDays = (int)Employees.Rows[i]["OffDays"];
                employeeTimeOffs.Add(item);
            }
            return this.Store(employeeTimeOffs, employeeTimeOffs.Count);
        }

        public OffDayBO GetById(string id)
        {
            OffDayBO OffDayBO = new OffDayBO();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "sp_OffDays_GetById";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Id", id);

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            OffDayBO.Id = (Guid)dr["Id"];
                            OffDayBO.EmployeeId = (Guid)dr["EmployeeId"];
                            OffDayBO.Using = (int)dr["Using"];
                            OffDayBO.OffDays = (int)dr["OffDays"];
                            OffDayBO.EmployeePerform = DBConnect.EmployeeGetById(OffDayBO.EmployeeId.Value.ToString());
                        }
                    }
                }
            }
            return OffDayBO;
        }

        public static OffDayBO GetByEmploueeId(string id)
        {
            OffDayBO OffDayBO = new OffDayBO();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "sp_OffDays_GetByEmploueeId";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Id", id);

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            OffDayBO.Id = (Guid)dr["Id"];
                            OffDayBO.EmployeeId = (Guid)dr["EmployeeId"];
                            OffDayBO.Using = (int)dr["Using"];
                            OffDayBO.OffDays = (int)dr["OffDays"];
                            OffDayBO.EmployeePerform = DBConnect.EmployeeGetById(OffDayBO.EmployeeId.Value.ToString());
                        }
                    }
                }
            }
            return OffDayBO;
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(OffDayBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
                    {
                        if (connection.State != ConnectionState.Open)
                            connection.Open();

                        using (var cmd = connection.CreateCommand())
                        {
                            cmd.CommandText = "sp_OffDays_Insert";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 200;
                            cmd.Parameters.AddWithValue("@EmployeeId", item.EmployeePerform.Id);
                            cmd.Parameters.AddWithValue("@Using", item.Using);
                            cmd.Parameters.AddWithValue("@OffDays", item.OffDays);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(OffDayBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
                    {
                        if (connection.State != ConnectionState.Open)
                            connection.Open();

                        using (var cmd = connection.CreateCommand())
                        {
                            cmd.CommandText = "sp_OffDays_Update";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 200;
                            cmd.Parameters.AddWithValue("@Id", item.Id);
                            cmd.Parameters.AddWithValue("@Using", item.Using);
                            cmd.Parameters.AddWithValue("@OffDays", item.OffDays);
                            cmd.ExecuteNonQuery();
                        }
                    }

                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        [HttpPost]
        public ActionResult Delete(OffDayBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
                    {
                        if (connection.State != ConnectionState.Open)
                            connection.Open();

                        using (var cmd = connection.CreateCommand())
                        {
                            cmd.CommandText = "sp_OffDays_Delete";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = 200;
                            cmd.Parameters.AddWithValue("@id", item.Id);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}