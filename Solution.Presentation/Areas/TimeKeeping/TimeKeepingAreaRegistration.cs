﻿using System.Web.Mvc;

namespace iDAS.Presentation.Areas.TimeKeeping
{
    public class TimeKeepingAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TimeKeeping";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TimeKeeping_default",
                "TimeKeeping/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}