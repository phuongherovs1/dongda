﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Dispatch.Controllers
{
    public class DispatchRecipientsExternalController : FrontController
    {
        //
        // GET: /Dispatch/DispatchRecipientsExternal/
        public ActionResult Create(DispatchRecipientsExternalBO item)
        {
            var result = false;
            try
            {
                DispatchRecipientsExternalService.Insert(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.ProfileNameHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }


        public ActionResult Update(DispatchRecipientsExternalBO item)
        {
            var result = false;
            try
            {
                DispatchRecipientsExternalService.Update(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.ProfileNameHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult Delete(Guid Id)
        {
            var result = false;
            try
            {
                DispatchRecipientsExternalService.Delete(Id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.ProfileNameHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult GetDataExternal(StoreRequestParameters parameter, string DispatchId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = DispatchRecipientsExternalService.GetDataExternal(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(DispatchId)).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult Form(string Id = default(string), string DispatchId = default(string))
        {
            if (Id == null)
            {
                var model = new DispatchRecipientsExternalBO();
                model.DispatchId = new Guid(DispatchId);
                ViewData.Add("Operation", Common.Operation.Create);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                var model = DispatchRecipientsExternalService.GetById(new Guid(Id));
                ViewData.Add("Operation", Common.Operation.Update);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }

        }
    }
}