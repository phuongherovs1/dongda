﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Dispatch.Controllers
{
    public class DashBoardController : FrontController
    {
        // GET: /Profile/DashBoard/
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser();
            ViewData["DepartmentName"] = department.FirstOrDefault().Name;
            ViewData["DepartmentID"] = department.FirstOrDefault().Id;

            ViewData["ArchiveType"] = iDAS.Service.Common.Resource.GetArchiveTimeForList();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult IndexTo(string containerId)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser();
            ViewData["DepartmentName"] = department.FirstOrDefault().Name;
            ViewData["DepartmentID"] = department.FirstOrDefault().Id;

            ViewData["ArchiveType"] = iDAS.Service.Common.Resource.GetArchiveTimeForList();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexTo",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult IndexGo(string containerId)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser();
            ViewData["DepartmentName"] = department.FirstOrDefault().Name;
            ViewData["DepartmentID"] = department.FirstOrDefault().Id;

            ViewData["ArchiveType"] = iDAS.Service.Common.Resource.GetArchiveTimeForList();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexGo",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult IndexSave(string containerId)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser();
            ViewData["DepartmentName"] = department.FirstOrDefault().Name;
            ViewData["DepartmentID"] = department.FirstOrDefault().Id;

            ViewData["ArchiveType"] = iDAS.Service.Common.Resource.GetArchiveTimeForList();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "IndexSave",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        // Danh sách mục hồ sơ
        public ActionResult DispatchList()
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetDispatchStatus();
            return PartialView();
        }

        public ActionResult DispatchFormList(string depId)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetDispatchStatus();

            return PartialView();
        }
        public ActionResult GetTotal(string departmentId)
        {
            return this.Direct(DispatchRecipientsInternalService.CountDispatchInternal(departmentId));
        }

        // Hồ sơ lưu trữ
        public ActionResult ArchiveProfile(string containerId)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetProfileStatus();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "ArchiveProfile",
                WrapByScriptTag = false,
                ViewData = ViewData
            };
        }
        // Lấy danh sách phân công, cập nhật
        public ActionResult LoadProfileAssignUpdate()
        {
            var data = ProfileSummaryService.GetDasboardAssignUpdate();
            return this.Store(data);
        }
        // Lấy danh sách đề nghị kiểm tra
        public ActionResult LoadProfileSuggestCheck()
        {
            var data = ProfileSummaryService.GetDasboardSuggestCheck();
            return this.Store(data);
        }
        // Lấy danh sách hồ sơ phê duyệt
        public ActionResult LoadProfileApproval()
        {
            var data = ProfileSummaryService.GetDasboardApproval();
            return this.Store(data);
        }
        // Lấy danh sách hồ sơ lưu trữ
        public ActionResult LoadProfileArchire()
        {
            var data = ProfileSummaryService.GetDasboardArchire();
            return this.Store(data);
        }


    }
}