﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Dispatch.Controllers
{
    public class DispatchTaskController : FrontController
    {
        //
        // GET: /Dispatch/DispatchTask/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateDispatchTask(TaskBO item)
        {
            var message = default(string);
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    var taskId = TaskService.CreateDispatchTask(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.DirectFormat(message);
        }

        public ActionResult FormTask(string id = default(string), string DispatchId = default(string))
        {
            //var model = TaskService.GetFormItem(id);
            if (id == default(string))
            {
                var model = new TaskBO { };
                model.DispatchId = iDAS.Service.Common.Utilities.ConvertToGuid(DispatchId);
                ViewData.Add("Operation", Common.Operation.Create);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                var model = TaskService.GetFormItem(new Guid(id));
                model.DispatchId = iDAS.Service.Common.Utilities.ConvertToGuid(DispatchId);
                ViewData.Add("Operation", Common.Operation.Update);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
        }

        public ActionResult DeleteDispatchTask(Guid Id)
        {
            var message = default(string);
            var result = false;

            try
            {
                DispatchTaskService.DeleteTask(Id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.DirectFormat(message);
        }

        public ActionResult UpdateDispatchTask(TaskBO item)
        {
            var message = default(string);
            var result = false;

            try
            {
                TaskService.Update(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.DirectFormat(message);
        }
    }
}