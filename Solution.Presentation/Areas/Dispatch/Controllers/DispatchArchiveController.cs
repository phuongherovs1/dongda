﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Linq;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Dispatch.Controllers
{
    public class DispatchArchiveController : FrontController
    {
        //
        // GET: /Dispatch/DispatchArchive/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Form(string DispatchId = default(string), string DepartmentArchiveId = default(string))
        {
            var model = new DispatchArchiveBO() { };
            ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
            ViewData["FormalitySave"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
            model = DispatchArchiveService.CreateDefault(DispatchId, DepartmentArchiveId);
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        /// <summary>
        /// Form thêm công văn đã lưu trữ vào hệ thống
        /// </summary>
        /// <param name="departmentArchiveId"></param>
        /// <returns></returns>
        public ActionResult FormAddDispatchArchive(string departmentArchiveId)
        {
            try
            {
                ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
                ViewData["FormalitySave"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
                var model = DispatchArchiveService.CreateDefault(departmentArchiveId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }

        public ActionResult InsertDispatchArchiveOld(DispatchArchiveBO item)
        {
            var result = false;
            try
            {
                DispatchArchiveService.InsertDispatchArchiveOld(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult InsertOrUpdate(DispatchArchiveBO item)
        {
            var result = false;
            try
            {
                DispatchArchiveService.InsertOrUpdate(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult GetData(StoreRequestParameters parameter, string filter, string department,
            iDAS.Service.Common.Resource.ProfileArchiveTime archiveType = iDAS.Service.Common.Resource.ProfileArchiveTime.All)
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = DispatchArchiveService.GetDispatchArchive(pageIndex, pageSize, out count, filter, archiveType, department).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Xóa công văn lưu trữ
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(Guid? id)
        {
            var result = false;
            if (id == null || id == Guid.Empty)
                return this.Direct();

            try
            {
                DispatchArchiveService.Delete(id.Value);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }
    }
}