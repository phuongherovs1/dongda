﻿using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Dispatch.Controllers
{
    public class ArchiveSettingController : FrontController
    {
        //
        // GET: /Profile/Setting/
        public ActionResult Index()
        {
            return PartialView();
        }

        public ActionResult GetData(string departmentId)
        {
            try
            {
                if (string.IsNullOrEmpty(departmentId) || departmentId == "root")
                    return this.Direct();
                var data = ProfileDepartmentArchiveRoleService.GetByDepartment(new Guid(departmentId));
                return this.Store(data);
            }
            catch (Exception)
            {
            }
            return this.Direct();
        }

        public ActionResult Form(bool titleOnly, Guid departmentId, string departmentName)
        {

            var model = new ProfileDepartmentArchiveRoleBO()
            {
                DepartmentId = departmentId,
                DepartmentName = departmentName
            };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        [HttpPost]
        public ActionResult Create(string jsonData)
        {
            try
            {
                ProfileDepartmentArchiveRoleService.Save(jsonData);
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);

            }
            return this.Direct();
        }

        public ActionResult Delete(Guid id)
        {
            try
            {
                ProfileDepartmentArchiveRoleService.Delete(id);
            }
            catch (Exception)
            {

            }
            return this.Direct();
        }
    }
}