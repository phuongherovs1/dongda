﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service.API.Dispatch;
using iDAS.Service.Common;
using System;
using System.Data;
using System.Web.Mvc;


namespace iDAS.Presentation.Areas.Dispatch.Controllers
{
    public class DispatchSearchController : FrontController
    {
        // GET: Dispatch/DispatchSearch
        protected IDispatchSearchService dispatchSearchService { set; get; }
        public DispatchSearchController(IDispatchSearchService _dispatchSearchService)
        {
            dispatchSearchService = _dispatchSearchService;
        }

        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            //var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            //if (department != null)
            //{
            //    var departmentName = department.Name;
            //    var departmentId = department.Id;
            //    ViewData["departmentName"] = departmentName;
            //    ViewData["departmentId"] = departmentId;
            //}
            ViewData["Status"] = iDAS.Service.Common.Resource.GetDispatchStatus();

            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult GetData(StoreRequestParameters parameter, DateTime sentDateFrom, DateTime sentDateTo, string dispatchType = default(string), string numberDispatch = default(string), string name = default(string), string content = default(string), int dispatchStatus = 0, Guid? securityID = null, Guid? urgencyID = null)
        {
            DateTime timeMin = DateTime.Parse("01/01/1900");
            DateTime timeMax = DateTime.MaxValue;
            if (sentDateFrom < timeMin)
                sentDateFrom = timeMin;
            if (sentDateTo > timeMax)
                sentDateTo = timeMax;
            int statusArchived = 0;
            bool? isGo = null;

            // tìm kiếm theo loại công văn
            if (dispatchType.Equals("go"))  // cv đến
                isGo = true;
            else if (dispatchType.Equals("arrive")) // cv đi
                isGo = false;
            else if (dispatchType.Equals("archive"))    // cv lưu trữ
            {
                statusArchived = Convert.ToInt32(Resource.DispatchStatus.Archived);
                if (dispatchStatus == 0)
                    dispatchStatus = statusArchived;
                else if (dispatchStatus > 0 && dispatchStatus != statusArchived)
                    return null;
            }

            var pageIndex = parameter.Page;
            var pageSize = parameter.Limit;
            var count = 0;
            DataTable dt = dispatchSearchService.GetData(numberDispatch, name, sentDateFrom, sentDateTo, content, securityID, urgencyID, isGo, dispatchStatus, pageIndex, pageSize, out count);
            return this.Store(AddStatusNameToTable(dt), count);
        }


        private DataTable AddStatusNameToTable(DataTable dtSource)
        {
            if (dtSource == null || dtSource.Rows.Count == 0)
                return dtSource;

            dtSource.Columns.Add("StatusText");
            for (int i = 0; i < dtSource.Rows.Count; i++)
            {
                int status = 0;
                if (string.IsNullOrEmpty(dtSource.Rows[i]["Status"].ToString()))
                    continue;

                status = Convert.ToInt32(dtSource.Rows[i]["Status"]);
                dtSource.Rows[i]["StatusText"] = GetStatusName(status);
            }
            return dtSource;
        }

        private string GetStatusName(int status)
        {
            var type = string.Empty;
            switch (status)
            {
                case (int)iDAS.Service.Common.Resource.DispatchStatus.All:
                    type = iDAS.Service.Common.Resource.DispatchStatusText.All;
                    break;
                case (int)iDAS.Service.Common.Resource.DispatchStatus.WaitReview:
                    type = iDAS.Service.Common.Resource.DispatchStatusText.WaitReview;
                    break;
                case (int)iDAS.Service.Common.Resource.DispatchStatus.Review:
                    type = iDAS.Service.Common.Resource.DispatchStatusText.Review;
                    break;
                case (int)iDAS.Service.Common.Resource.DispatchStatus.NotReview:
                    type = iDAS.Service.Common.Resource.DispatchStatusText.NotReview;
                    break;
                case (int)iDAS.Service.Common.Resource.DispatchStatus.WaitApprove:
                    type = iDAS.Service.Common.Resource.DispatchStatusText.WaitApprove;
                    break;
                case (int)iDAS.Service.Common.Resource.DispatchStatus.Approve:
                    type = iDAS.Service.Common.Resource.DispatchStatusText.Approve;
                    break;
                case (int)iDAS.Service.Common.Resource.DispatchStatus.NotApprove:
                    type = iDAS.Service.Common.Resource.DispatchStatusText.NotApprove;
                    break;
                case (int)iDAS.Service.Common.Resource.DispatchStatus.Delete:
                    type = iDAS.Service.Common.Resource.DispatchStatusText.Delete;
                    break;
                case (int)iDAS.Service.Common.Resource.DispatchStatus.WaitSend:
                    type = iDAS.Service.Common.Resource.DispatchStatusText.WaitSend;
                    break;
                case (int)iDAS.Service.Common.Resource.DispatchStatus.Send:
                    type = iDAS.Service.Common.Resource.DispatchStatusText.Send;
                    break;
                case (int)iDAS.Service.Common.Resource.DispatchStatus.Archived:
                    type = iDAS.Service.Common.Resource.DispatchStatusText.Archived;
                    break;
            }
            return type;
        }
    }
}