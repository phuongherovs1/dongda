﻿using Ext.Net;
using iDAS.Presentation.Controllers;

namespace iDAS.Presentation.Areas.Dispatch.Controllers
{
    public class SettingController : FrontController
    {
        //
        // GET: /Profile/Setting/
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {

                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                WrapByScriptTag = false
            };
        }
    }
}