﻿using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Dispatch.Controllers
{
    public class DispatchCategoriToController : FrontController
    {
        //
        // GET: /Dispatch/DispatchCategoriTo/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Form(string DepartmentId = default(string), string DispatchId = default(string))
        {
            var model = new DispatchCategoriToBO();
            model.DepartmentId = new Guid(DepartmentId);
            model.DispatchId = new Guid(DispatchId);
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        public ActionResult Insert(DispatchCategoriToBO item)
        {
            var result = false;
            try
            {
                DispatchCategoriToService.Insert(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
    }
}