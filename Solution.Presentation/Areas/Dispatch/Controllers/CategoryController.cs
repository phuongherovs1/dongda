﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Dispatch.Controllers
{
    public class CategoryController : FrontController
    {
        //
        // GET: /Profile/Category/
        private Node createNodeCategory(DispatchCategorieBO category)
        {
            var node = new Node();
            node.NodeID = category.Id.ToString();
            node.Text = " " + category.NameFormat;
            node.IconCls = !category.ParentId.HasValue ? "x-fa fa-folder" : "x-fa fa-folder-o";
            node.CustomAttributes.Add(new ConfigItem { Name = "ParentId", Value = category.ParentId.ToString(), Mode = ParameterMode.Value });
            node.Leaf = !category.IsParent;
            node.Expanded = false;
            return node;
        }

        public ActionResult GetData(string node, string departmentId)
        {
            try
            {
                var nodes = new NodeCollection();
                if (departmentId.Contains('_'))
                {
                    string[] DepartmentIdsl = departmentId.Trim().Split('_');
                    departmentId = DepartmentIdsl[1];
                }
                var documentCategoryID = node == "root" ? (Guid?)null : new Guid(Convert.ToString(node));
                var documentCategories = DispatchCategoryService.GetTreeDispatchCategory(documentCategoryID, new Guid(departmentId)).ToList();
                foreach (var category in documentCategories)
                {
                    nodes.Add(createNodeCategory(category));
                }

                return this.Content(nodes.ToJson());
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }

        public ActionResult GetDataTo(string node, string departmentId)
        {
            try
            {
                var nodes = new NodeCollection();
                if (departmentId.Contains('_'))
                {
                    string[] DepartmentIdsl = departmentId.Trim().Split('_');
                    departmentId = DepartmentIdsl[1];
                }
                var documentCategoryID = node == "root" ? (Guid?)null : new Guid(Convert.ToString(node));
                var documentCategories = DispatchCategoryService.GetTreeDispatchCategoryTo(documentCategoryID, new Guid(departmentId)).ToList();
                foreach (var category in documentCategories)
                {
                    nodes.Add(createNodeCategory(category));
                }

                return this.Content(nodes.ToJson());
            }

            catch (Exception ex)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Form thêm mới/sửa danh mục
        /// </summary>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <param name="IsNoParent"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public ActionResult Form(string id = default(string), string departmentId = default(string), bool IsNoParent = false, string categoryId = default(string))
        {
            try
            {
                DispatchCategorieBO model = null;
                ViewData.Add("NoParent", IsNoParent);
                if (id == default(string))
                {
                    ViewData.Add("Operation", Common.Operation.Create);
                }
                else
                {
                    ViewData.Add("Operation", Common.Operation.Update);
                }
                model = DispatchCategoryService.GetDetail(id, departmentId, categoryId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                return this.Direct();
            }
        }


        public ActionResult FormTo(string id = default(string), string departmentId = default(string), bool IsNoParent = false, string categoryId = default(string))
        {
            try
            {
                DispatchCategorieBO model = null;
                ViewData.Add("NoParent", IsNoParent);
                if (id == default(string))
                {
                    ViewData.Add("Operation", Common.Operation.Create);
                }
                else
                {
                    ViewData.Add("Operation", Common.Operation.Update);
                }
                model = DispatchCategoryService.GetDetail(id, departmentId, categoryId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                return this.Direct();
            }
        }
        /// <summary>
        /// Danh sách danh mục theo phòng ban
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public ActionResult GetByDepartment(string departmentId = default(string), string categoryId = default(string))
        {
            try
            {
                var data = DispatchCategoryService.GetByDepartment(departmentId, categoryId).ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult GetCategoryToOrFormByDepartment(string departmentId, bool dispatchForm)
        {
            try
            {
                var data = DispatchCategoryService.GetCategoryToOrFormByDepartment(departmentId, dispatchForm).ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult GetDataCategoryTo(string departmentId = default(string))
        {
            try
            {
                var data = DispatchCategoryService.GetDataCategoryTo(new Guid(departmentId)).ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Thêm mới danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(DispatchCategorieBO item)
        {
            var result = false;
            try
            {
                DispatchCategoryService.Insert(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Sửa danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(DispatchCategorieBO item)
        {
            var result = false;
            try
            {

                DispatchCategoryService.Update(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DispatchCategoryService.DeleteCategory(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.ProfileCategoryExitsProfile || ex is iDAS.Service.Common.AccessDenyException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}