﻿using Ext.Net.MVC;
using iDAS.Presentation.Controllers;

namespace iDAS.Presentation.Areas.Dispatch.Controllers
{
    public class StatisticalController : FrontController
    {
        //
        // GET: /Profile/Statistical/
        public StoreResult GetDataProfileStatusAnalytic(string departmentId = default(string))
        {

            var summary = ProfileSummaryService.ProfileStatusAnalytic(iDAS.Service.Common.Utilities.ConvertToGuid(departmentId));
            return new StoreResult(summary);

        }
        public StoreResult GetDataProfileBorrowAnalytic(string departmentId = default(string))
        {

            var summary = ProfileSummaryService.ProfileBorrowAnalytic(iDAS.Service.Common.Utilities.ConvertToGuid(departmentId));
            return new StoreResult(summary);

        }
    }
}