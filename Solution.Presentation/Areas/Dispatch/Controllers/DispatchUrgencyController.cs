﻿using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Dispatch.Controllers
{
    public class DispatchUrgencyController : FrontController
    {

        public ActionResult Form(string alignTo, string renderTo, string storeId)
        {
            ViewData["AlignToId"] = string.IsNullOrEmpty(alignTo) ? "ctDesktop" : alignTo;
            ViewData["RenderToId"] = string.IsNullOrEmpty(renderTo) ? "ctDesktop" : renderTo;
            ViewData["StoreId"] = string.IsNullOrEmpty(storeId) ? "storeSecurity" : storeId;
            var model = new DispatchUrgencyBO();
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        public ActionResult GetAll()
        {
            var data = DispatchUrgencyService.GetAll();
            return this.Store(data);
        }

        public ActionResult Create(DispatchUrgencyBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DispatchUrgencyService.Insert(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}