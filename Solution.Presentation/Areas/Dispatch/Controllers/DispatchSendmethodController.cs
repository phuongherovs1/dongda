﻿using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Dispatch.Controllers
{
    public class DispatchSendmethodController : FrontController
    {
        //
        // GET: /Dispatch/DispatchSendmethod/
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Form(string alignTo, string renderTo, string storeId)
        {
            ViewData["AlignToId"] = string.IsNullOrEmpty(alignTo) ? "ctDesktop" : alignTo;
            ViewData["RenderToId"] = string.IsNullOrEmpty(renderTo) ? "ctDesktop" : renderTo;
            ViewData["StoreId"] = string.IsNullOrEmpty(storeId) ? "storeSecurity" : storeId;
            var model = new DispatchSendmethodBO();
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        public ActionResult GetAllData()
        {
            try
            {
                var data = DispatchSendmethodService.GetAll().ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult Create(DispatchSendmethodBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DispatchSendmethodService.Insert(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}