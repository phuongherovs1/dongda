﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Dispatch.Controllers
{
    public class DispatchController : FrontController
    {


        public ActionResult GetDataDispatchInternal(StoreRequestParameters parameter, string DepartmentId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                object data = null;
                if (!string.IsNullOrEmpty(DepartmentId))
                    data = DispatchRecipientsInternalService.GetDataDispatchInternal(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(DepartmentId)).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }


        public ActionResult GetDataDispatch(StoreRequestParameters parameter, string categoryId = default(string), string filterName = default(string), iDAS.Service.Common.Resource.DispatchStatus status = iDAS.Service.Common.Resource.DispatchStatus.All)
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                object data = null;
                if (!string.IsNullOrEmpty(categoryId))
                    data = DispatchService.GetDataDispatch(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(categoryId), filterName, status).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }


        public ActionResult GetDataDispatchTo(StoreRequestParameters parameter, string categoryId = default(string), string filterName = default(string), iDAS.Service.Common.Resource.DispatchStatus status = iDAS.Service.Common.Resource.DispatchStatus.All)
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = DispatchCategoriToService.GetDataDispatchTo(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(categoryId), filterName, status).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        public ActionResult GetDataResponsibility(StoreRequestParameters parameter, string DispatchId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = DispatchResponsibilityService.GetByDispatch(pageIndex, pageSize, out count, DispatchId).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }


        public ActionResult Form(string Id = default(string), bool hideButtonSave = false, string CategoryId = default(string), string departmentId = default(string))
        {
            ViewData["HideButtonSave"] = hideButtonSave;
            if (Id == null)
            {
                if (departmentId != null)
                {
                    if (departmentId.Contains('_'))
                    {
                        string[] DepartmentIdsl = departmentId.Trim().Split('_');
                        departmentId = DepartmentIdsl[1];
                    }
                }
                ViewData["HideButtonSaveProfile"] = false;
                var model = DispatchService.CreateDefault(new Guid(CategoryId), new Guid(departmentId));
                ViewData.Add("Operation", Common.Operation.Create);
                ViewData["DispatchResponsibility"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetDispatchResponsibility());
                // ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
                // ViewData["FormalitySave"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                var model = DispatchService.GetDataById(new Guid(Id));
                ViewData.Add("Operation", Common.Operation.Update);
                // ViewData["HideButtonSaveProfile"] = model.IsHiddenButton;
                ViewData["HideButtonSaveProfile"] = false;
                ViewData["DispatchResponsibility"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetDispatchResponsibility());
                // ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
                // ViewData["FormalitySave"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
        }


        public ActionResult FormTo(string Id = default(string), bool hideButtonSave = false, string CategoryId = default(string), string departmentId = default(string))
        {
            ViewData["HideButtonSave"] = hideButtonSave;
            if (Id == null)
            {
                if (departmentId.Contains('_'))
                {
                    string[] DepartmentIdsl = departmentId.Trim().Split('_');
                    departmentId = DepartmentIdsl[1];
                }
                ViewData["HideButtonSaveProfile"] = false;
                var model = DispatchService.CreateDefault(new Guid(CategoryId), new Guid(departmentId));
                ViewData.Add("Operation", Common.Operation.Create);
                ViewData["DispatchResponsibility"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetDispatchResponsibility());
                // ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
                // ViewData["FormalitySave"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                var model = DispatchService.GetDataById(new Guid(Id));
                ViewData.Add("Operation", Common.Operation.Update);
                // ViewData["HideButtonSaveProfile"] = model.IsHiddenButton;
                ViewData["HideButtonSaveProfile"] = false;
                ViewData["DispatchResponsibility"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetDispatchResponsibility());
                // ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
                // ViewData["FormalitySave"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
        }

        public ActionResult SendInternal(string DispatchId = default(string))
        {
            var model = new DispatchRecipientsInternalBO();
            model.DispatchId = new Guid(DispatchId);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult FormApproval(string Id = default(string))
        {
            try
            {
                var model = DispatchService.GetDataByIdCheckAproval(new Guid(Id));
                // ViewData["HideButtonSaveProfile"] = model.IsHiddenButton;
                ViewData["HideButtonSaveProfile"] = false;
                ViewData["DispatchResponsibility"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetDispatchResponsibility());
                // ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
                // ViewData["FormalitySave"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct();
            }

        }

        public ActionResult FormSendExternal(string DispatchId = default(string))
        {

            var model = new DispatchRecipientsExternalBO();
            model.DispatchId = new Guid(DispatchId);
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };

        }


        public ActionResult FormListArchive(string DispatchId = default(string))
        {

            var model = new DispatchBO();
            model.Id = new Guid(DispatchId);
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };

        }

        public ActionResult FormReview(string Id = default(string))
        {

            try
            {
                var model = DispatchService.GetDataByIdCheckReview(new Guid(Id));
                // ViewData["HideButtonSaveProfile"] = model.IsHiddenButton;
                ViewData["HideButtonSaveProfile"] = false;
                ViewData["DispatchResponsibility"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetDispatchResponsibility());
                // ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
                // ViewData["FormalitySave"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct();
            }

        }
        public ActionResult GetDataDispatchTask(StoreRequestParameters parameter, string DispatchId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = TaskService.GetDataDispatchTask(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(DispatchId)).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult FormTask(string Id = default(string))
        {

            var model = DispatchService.GetDataById(new Guid(Id));
            // ViewData["HideButtonSaveProfile"] = model.IsHiddenButton;
            ViewData["HideButtonSaveProfile"] = false;
            ViewData["DispatchResponsibility"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetDispatchResponsibility());
            // ViewData["Type"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetArchiveTime());
            // ViewData["FormalitySave"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetFormalitySaveStatus());
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };

        }


        public ActionResult ReView(DispatchBO item)
        {
            var result = false;
            try
            {
                DispatchService.ReView(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }



        public ActionResult Approval(DispatchBO item)
        {
            var result = false;
            try
            {
                DispatchService.Approval(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult FormSendTo(string departmentId = default(string))
        {
            var model = new DispatchBO() { };
            var depId = Guid.Empty;
            if (!string.IsNullOrEmpty(departmentId))
            {
                if (departmentId.Contains('_'))
                    depId = new Guid(departmentId.Split('_').ToList()[1]);
                else
                    depId = new Guid(departmentId);
            }
            model.DepartmentId = depId;
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        public ActionResult GetDataInternal(StoreRequestParameters parameter, string DispatchId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = DispatchRecipientsInternalService.GetDataInternal(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(DispatchId)).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        public ActionResult DeleteInternal(string Id = default(string))
        {
            var result = false;
            try
            {
                if (Id == "null")
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    DispatchRecipientsInternalService.Delete(new Guid(Id));
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.ProfileNameHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult InsertInternal(DispatchRecipientsInternalBO item, string jsonData = default(string))
        {
            var result = false;
            try
            {
                DispatchRecipientsInternalService.InsertInternal(item, jsonData);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.ProfileNameHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult Create(DispatchBO item, string jsonData = default(string))
        {
            var result = false;
            try
            {
                if (jsonData == null || jsonData == "[]")
                {
                    this.ShowNotify(Common.Resource.Responsibiliti);
                }
                else
                {
                    DispatchService.InsertDispatch(item, jsonData);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.ProfileNameHasBeenExistedException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }



        public ActionResult Update(DispatchBO item, string jsonData = default(string))
        {
            var result = false;
            try
            {
                if (jsonData == null || jsonData == "[]")
                {
                    this.ShowNotify(Common.Resource.Responsibiliti);
                }
                else
                {
                    DispatchService.UpdateDispatch(item, jsonData);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }


        public ActionResult Delete(Guid Id)
        {
            var result = false;
            try
            {
                DispatchService.Delete(Id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }



        public ActionResult DeleteResponsibility(Guid ResponsibilityId)
        {
            var result = false;
            try
            {
                if (ResponsibilityId == Guid.Empty)
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    DispatchResponsibilityService.Delete(ResponsibilityId);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

    }
}