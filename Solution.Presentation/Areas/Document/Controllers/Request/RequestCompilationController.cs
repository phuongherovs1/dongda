﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;
using System.Diagnostics;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class RequestCompilationController : FrontController
    {
        #region Danh sách yêu cầu biên soạn

        /// <summary>
        /// Danh sách trạng thái trên danh sách yêu cầu biên soạn tài liệu
        /// </summary>
        /// <returns></returns>
        public ActionResult ListCompilation()
        {
            ViewData["lstRequestCompilation"] = iDAS.Service.Common.Resource.GetRequestStatus();
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }

        /// <summary>
        /// Lấy danh sách yêu cầu biên soạn tài liệu theo người tạo yêu cầu
        /// </summary>
        /// <param name="param"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult GetListCompilation(StoreRequestParameters param, iDAS.Service.Common.Resource.RequestStatus type)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentRequestService.GetPeformByCurrentUser(pageIndex, pageSize, out count, type, false).ToList();
            return this.Store(data, count);
        }
        /// <summary>
        /// Gọi form yêu cầu biên soạn
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CreateCompilation(string id = default(string))
        {
            try
            {
                object request = null;
                if (id == default(string))
                {
                    request = new DocumentRequestBO() { Document = new DocumentBO() };
                }
                else
                {
                    var item = DocumentRequestService.GetById(new Guid(id));
                    if (item != null)
                        request = DocumentRequestService.CreateDefault(item.Document.Id, id);
                    else
                        request = new DocumentRequestBO() { Document = new DocumentBO() };
                }
                return new Ext.Net.MVC.PartialViewResult { Model = request };
            }
            catch
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Chức năng yêu cầu biên soạn đồng thời gửi yêu cầu biên soạn
        /// </summary>
        /// <param name="data"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public ActionResult CreatRequestCompile(DocumentRequestBO data, string jsonData = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentRequestService.SendAssign(data, false);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Chức năng chỉ lưu thông tin yêu cầu biên soạn
        /// </summary>
        /// <param name="data"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public ActionResult SaveRequestCompile(DocumentRequestBO data, string jsonData = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    //DocumentRequestService.Save(data, jsonData);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Xóa yêu cầu biên soạn
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentRequestService.Delete(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}