﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class ReviewSuggestController : FrontController
    {
        public ActionResult Index()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }
        #region đề nghị kiểm tra
        public ActionResult ListReview()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }
        #endregion


        #region đề nghị kiểm duyệt
        #region Lấy danh sách đề nghị kiểm duyệt
        /// <summary>
        /// Lấy danh sách đề nghị kiểm duyệt
        /// </summary>
        /// <param name="param"></param>
        /// <param name="approveStatus"></param>
        /// <returns></returns>
        public ActionResult GetData(StoreRequestParameters param, Service.Common.Resource.ApproveStatus approveStatus = Service.Common.Resource.ApproveStatus.New)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            object data = null; //DocumentReviewSuggestService.GetAll(pageIndex, pageSize, out count, approveStatus);
            return this.Store(data, count);
        }
        #endregion

        #region Form chi tiết thông tin đề nghị kiểm duyệt
        /// <summary>
        /// Form chi tiết thông tin đề nghị kiểm duyệt
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public ActionResult Form(string id)
        {
            if (string.IsNullOrEmpty(id))
                return this.Direct();
            object model = null;// DocumentReviewSuggestService.CreateDefault(null, id);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        #endregion

        #region Gửi thông tin đề nghị kiểm duyệt
        /// <summary>
        /// Gửi thông tin đề nghị kiểm duyệt
        /// </summary>
        /// <param name="adjustSuggest"></param>
        /// <returns></returns>
        public ActionResult SendApprove(DocumentReviewSuggestBO reviewSuggest, string jsonData = "")
        {
            object result = null;
            try
            {
                //reviewSuggest.Id = DocumentReviewSuggestService.Send(reviewSuggest, jsonData);
                result = new { success = true, data = reviewSuggest.Id, message = Common.Resource.SystemSuccess };
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
        #endregion

        #region Lưu thông tin đề nghị kiểm duyệt tài liệu
        /// <summary>
        /// Lưu thông tin đề nghị kiểm duyệt tài liệu
        /// </summary>
        /// <param name="review"></param>
        /// <returns></returns>
        public ActionResult Save(DocumentReviewSuggestBO reviewSuggest)
        {
            object result = null;
            try
            {
                //reviewSuggest.ApproveBy = reviewSuggest.EmployeeApproveBy.Id;
                //if (reviewSuggest.Id == Guid.Empty)
                //    reviewSuggest.Id = DocumentReviewSuggestService.Insert(reviewSuggest);
                //else
                //    DocumentReviewSuggestService.Update(reviewSuggest);
                result = new { success = true, data = reviewSuggest.Id, message = Common.Resource.SystemSuccess };
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
        #endregion

        #region Thu hồi đề nghị kiểm duyệt
        /// <summary>
        /// Thu hồi đề nghị kiểm duyệt
        /// </summary>
        /// <param name="reviewSuggest"></param>
        /// <returns></returns>
        public ActionResult Obsolate(DocumentReviewSuggestBO reviewSuggest)
        {
            object result = null;
            try
            {
              //  DocumentReviewSuggestService.Revert(reviewSuggest.Id);
                result = new { success = true, data = reviewSuggest.Id, message = Common.Resource.SystemSuccess };
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
        #endregion
        #endregion

        #region Duyệt đề nghị kiểm duyệt
        #region Form danh sách duyệt đề nghị kiểm duyệt
        /// <summary>
        /// Form danh sách duyệt đề nghị kiểm duyệt tài liệu
        /// </summary>
        /// <returns></returns>
        public ActionResult ListApproveReviewSuggest()
        {
            ViewData.Add("StatusApprove", iDAS.Service.Common.Resource.GetStatusApproveSuggest());
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        #endregion

        #region Danh sách duyệt đề nghị kiểm duyệt
        /// <summary>
        /// Lấy danh sách duyệt đề nghị kiểm duyệt được gửi đến người đăng nhập
        /// </summary>
        /// <param name="param"></param>
        /// <param name="approveStatus"></param>
        /// <returns></returns>
        public ActionResult GetByApprover(StoreRequestParameters param, Service.Common.Resource.ApproveStatus approveStatus = Service.Common.Resource.ApproveStatus.All)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            object data = null;// DocumentReviewSuggestService.GetByApprover(pageIndex, pageSize, out count, approveStatus);
            return this.Store(data, count);
        }
        #endregion

        #region Gọi form duyệt đề nghị kiểm duyệt
        public ActionResult FormApproveReviewSuggest(string id)
        {
            if (string.IsNullOrEmpty(id))
                return this.Direct();
            object model = null;// DocumentReviewSuggestService.CreateDefault(null, id);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        #endregion

        #region Duyệt đề nghị
        /// <summary>
        /// Duyệt đề nghị kiểm duyệt tài liệu
        /// </summary>
        /// <param name="review"></param>
        /// <returns></returns>
        public ActionResult Approve(DocumentReviewSuggestBO reviewSuggest)
        {
            object result = null;
            try
            {
                if (ModelState.IsValid)
                {
                   // DocumentReviewSuggestService.ApproveSuggest(reviewSuggest);
                    result = new { success = true, data = reviewSuggest.Id, message = Common.Resource.SystemSuccess };
                }
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
        #endregion

        #region Gọi form xem chi tiết đề nghị kiểm duyệt
        public ActionResult FormApproveDetail(string id)
        {
            if (string.IsNullOrEmpty(id))
                return this.Direct();
            object model = null;// DocumentReviewSuggestService.CreateDefault(null, id);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        #endregion
        #endregion

        /// <summary>
        /// Gọi form đề nghị kiểm tra
        /// </summary>
        /// <returns></returns>
        public ActionResult FormReviewSuggest()
        {
            object model = null;
            model = new DocumentReviewSuggestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Gọi form kết quả kiểm tra
        /// </summary>
        /// <returns></returns>
        public ActionResult FormResultReviewSuggest(string id = "")
        {
            object model = null;
            if (String.IsNullOrEmpty(id) || new Guid(id.ToString()) == Guid.Empty)
            {
                model = new DocumentReviewSuggestBO();
            }
            else
            {
                model = null;// DocumentReviewSuggestService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Gọi form chi tiết đề nghị kiểm tra
        /// </summary>
        /// <returns></returns>
        public ActionResult FormInfoReviewSuggest(string id = "", string role = "")
        {
            object model = null;
            try
            {
                ViewData.Add("Role", role);
                if (String.IsNullOrEmpty(id))
                    model = new DocumentReviewSuggestBO() { Document = new DocumentBO() };
                else
                    model = null;// DocumentReviewSuggestService.GetById(new Guid(id.ToString()));
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        /// <summary>
        /// Gọi form chi tiết đề nghị 
        /// </summary>
        /// <returns></returns>
        public ActionResult FormInfoSuggestReviewSuggest(string id = "")
        {
            object model = null;
            try
            {
                if (String.IsNullOrEmpty(id))
                    model = new DocumentReviewSuggestBO() { Document = new DocumentBO() };
                else
                    model = null;// DocumentReviewSuggestService.GetById(new Guid(id.ToString()));
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        /// <summary>
        /// Gọi form chi tiết đề nghị phê duyệt
        /// </summary>
        /// <returns></returns>
        public ActionResult FormInfoApproveReviewSuggest(string id = "", string role = "")
        {
            object model = null;
            try
            {
                ViewData.Add("Role", role);
                if (String.IsNullOrEmpty(id))
                    model = new DocumentReviewSuggestBO() { Document = new DocumentBO() };
                else
                    model = null;// DocumentReviewSuggestService.GetById(new Guid(id.ToString()));
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
    }
}