﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class RequestController : FrontController
    {
        /// <summary>
        /// Danh sách yêu cầu
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewData["StatusApprove"] = iDAS.Service.Common.Resource.GetStatusApproveSuggest();
            return PartialView();
        }

        #region Danh sách

        public ActionResult GeneralList()
        {
            return PartialView();
        }

        public ActionResult GetDataAll(StoreRequestParameters param, string search = "")
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            //var query = string.Format("Select * From DocumentRequests Order By [Id] Offset {0} Rows Fetch Next {1} Rows Only",(pageIndex-1) * pageSize, pageSize);
            var query = "sp_Get_Document_Request_List";
            string connectionString = ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand
            {
                Connection = conn,
                CommandText = query,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = 6000,
                
            };
            cmd.Parameters.AddWithValue("@search", search);

            var db = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(db);
            da.Dispose();
            var data = new List<object>();
            foreach (DataRow r in db.Rows)
            {
                var ra = r.ItemArray;
                var o = new
                {
                    Id = Guid.Parse(ra[0].ToString()),
                    Department = ra[1].ToString(),
                    DocumentId = Guid.Parse(ra[2].ToString()),
                    Code = ra[3].ToString(),
                    Name = ra[4].ToString(),
                    Content = ra[5].ToString(),
                    Date = (ra[6]!= null && !string.IsNullOrEmpty(ra[6].ToString()) && ra[7] != null && !string.IsNullOrEmpty(ra[7].ToString())) ? (DateTime.Parse(ra[6].ToString()).ToString("dd/MM/yyyy") + " - " + DateTime.Parse(ra[7].ToString()).ToString("dd/MM/yyyy")): "",
                    CreatedDate = DateTime.Parse(ra[8].ToString()).ToString("dd/MM/yyyy"),
                    CreatedBy = ra[9].ToString()
                };
                data.Add(o);
            }
            return this.Store(data, data.Count);
        }
        public ActionResult ListRoleEmployee(string id = default(string))
        {

            var model = new DocumentRequestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        #endregion

        #region Thực hiện yêu cầu biên soạn
        public ActionResult RoleEmployeeDocument(string id = default(string))
        {

            var model = new DocumentRequestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        /// <summary>
        /// Danh sách trạng thái trên danh sách yêu cầu biên soạn tài liệu
        /// </summary>
        /// <returns></returns>
        public ActionResult ListCompilation()
        {
            ViewData["lstRequestCompilation"] = iDAS.Service.Common.Resource.GetRequestStatus();
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }

        /// <summary>
        /// Lấy danh sách yêu cầu biên soạn tài liệu theo người tạo yêu cầu
        /// </summary>
        /// <param name="param"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ActionResult GetListCompilation(StoreRequestParameters param, iDAS.Service.Common.Resource.RequestStatus type = iDAS.Service.Common.Resource.RequestStatus.All)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentRequestService.GetRequestByCurrentUser(pageIndex, pageSize, out count, true, type).ToList();
            return this.Store(data, count);
        }
        /// <summary>
        /// Gọi form yêu cầu biên soạn
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CreateCompilation(Guid defaultCategory, string id = default(string))
        {
            try
            {
                object request = null;
                if (id == default(string))
                {
                    request = new DocumentRequestBO() { Document = new DocumentBO() };
                }
                else
                {
                    var item = DocumentRequestService.GetById(new Guid(id));
                    if (item != null)
                        request = DocumentRequestService.CreateDefault(item.Document.Id, id);
                    else
                        request = new DocumentRequestBO() { Document = new DocumentBO() };
                }
                return new Ext.Net.MVC.PartialViewResult { Model = request };
            }
            catch
            {
                return this.Direct();
            }
        }
        #endregion

        #region Yêu cầu soạn thảo

        /// <summary>
        /// Yêu cầu tạo mới
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CreateWrite(string id = default(string))
        {
            var model = new DocumentRequestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Form Tạo yêu cầu lưu trữ
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public ActionResult FormRequestReceive(string documentId = default(string))
        {
            var model = DocumentRequestService.GetDataRequestArchives(iDAS.Service.Common.Utilities.ConvertToGuid(documentId), iDAS.Service.Common.Resource.DocumentProcessRole.Archiver);
            return new Ext.Net.MVC.PartialViewResult { Model = model };

        }
        /// <summary>
        /// Lưu yêu cầu lưu trữ
        /// </summary>
        /// <param name="item"></param>
        /// <param name="references"></param>
        /// <returns></returns>  


        public ActionResult RequestAjust(string id = default(string))
        {

            var model = new DocumentRequestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };


        }

        public ActionResult RequestReceive(string id = default(string))
        {

            var model = new DocumentRequestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        #endregion

        #region Soạn thảo
        public ActionResult FormWrite(Guid? requestId, string id = "")
        {
            object model = null;// DocumentReviewSuggestService.CreateDefault(requestId, id);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        public ActionResult Send(DocumentReviewSuggestBO item, string references)
        {
            var result = false;
            try
            {
                //   DocumentReviewSuggestService.Send(item, references);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult Save(DocumentReviewSuggestBO item, string references)
        {
            var result = false;
            try
            {
                // DocumentReviewSuggestService.Save(item, references);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        #region thực hiện biên soạn
       
     

        public ActionResult GetDataPerformCompilation(StoreRequestParameters param, iDAS.Service.Common.Resource.RequestStatus type = iDAS.Service.Common.Resource.RequestStatus.All)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentRequestService.GetPeformByCurrentUser(pageIndex, pageSize, out count, type).ToList();
            return this.Store(data, count);
        }

        #endregion
        //public ActionResult UpdateRequestWrite(DocumentRequestBO data)
        //{
        //    DocumentRequestService.Send(data);
        //    return this.Direct();
        //}
        #endregion

        #region Chi tiết
        public ActionResult DetailRequest(string Id)
        {
            var model = DocumentRequestService.GetById(iDAS.Service.Common.Utilities.ConvertToGuid(Id));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        #endregion

        #region Khác
        public ActionResult FillColor()
        {
            return this.Store(iDAS.Service.Common.Utilities.RenderColor());
        }

        public ActionResult AdjustForm(string documentId = "")
        {
            try
            {
                if (string.IsNullOrEmpty(documentId))
                    return this.Direct();
                var model = DocumentRequestService.CreateAdjustRequest(iDAS.Service.Common.Utilities.ConvertToGuid(documentId));
                model.Document.Id = iDAS.Service.Common.Utilities.ConvertToGuid(documentId);
                model.DepartmentId = model.Document.DepartmentId;
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }

        public ActionResult SuggestCreate()
        {
            var model = new DocumentRequestBO();
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Đề nghị tạo mới
        /// </summary>
        /// <param name="data">DocumentRequest</param>
        /// <returns></returns>
        public ActionResult CreateSuggest(DocumentRequestBO data)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    //DocumentRequestService.CreateSuggest(data);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        public ActionResult RequestCreate()
        {
            try
            {
                var model = new DocumentRequestBO();
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }



        #endregion

        #region Danh sách yêu cầu
        /// <summary>
        /// Tab danh sách các yêu cầu
        /// </summary>
        /// <returns></returns>
        public ActionResult ListRequest()
        {
            return PartialView();
        }
        #endregion

        /// <summary>
        /// Gọi form thông tin yêu cầu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult FormInfoRequest(string id = default(string))
        {
            object model = null;
            if (id == default(string) || new Guid(id) == Guid.Empty)
            {
                model = new DocumentRequestBO() { Document = new DocumentBO() };
            }
            else
            {
                model = DocumentRequestService.GetById(new Guid(id));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// View danh sách yêu cầu sửa đổi
        /// </summary>
        /// <returns></returns>
        public ActionResult RequestAdjustIndex(string containerId)
        {
            ViewData["TypeRequestDocument"] = iDAS.Service.Common.Resource.GetRequestProcessStatusText();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }

        /// <summary>
        /// View danh sách yêu cầu phân phối
        /// </summary>
        /// <param name="containerId"></param>
        /// <returns></returns>
        public ActionResult ListDistributeRequest(string containerId)
        {
            ViewData["TypeRequestDocument"] = iDAS.Service.Common.Resource.GetRequestStatus();
            ViewData["TypeGetAssignerStatus"] = iDAS.Service.Common.Resource.GetAssignAreaRoleType();

            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }

        /// <summary>
        /// Form thông tin yêu cầu phân phối
        /// </summary>
        /// <returns></returns>
        public ActionResult FormInfoDistributeRequest()
        {
            object model = null;
            model = new DocumentReviewSuggestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Form yêu cầu phân phối
        /// </summary>
        /// <returns></returns>
        public ActionResult FormDistributeRequest()
        {
            object model = null;
            model = new DocumentReviewSuggestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(DocumentRequestBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentRequestService.Insert(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ChangePerform(DocumentRequestBO item)
        {
            var result = false;
            try
            {
                DocumentRequestService.ChangePerform(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #region viDH
        /// <summary>
        /// Thu hồi yêu cầu lưu trữ
        /// </summary>
        /// <returns></returns>
        public ActionResult DetailRequestArchive(Guid id, string name = default(string))
        {
            ViewData["Title"] = name;
            var model = DocumentRequestService.GetdataDetailRequest(id);
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        /// <summary>
        /// Thu hoi yeu cau huy
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public ActionResult DetailInfoRequestDestroy(Guid id, string name = default(string))
        {
            ViewData["Title"] = name;
            var model = DocumentRequestService.GetDetailRequestDistribute(id);
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        /// <summary>
        /// Yêu cầu soạn thảo xuất phát bởi yêu cầu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RequestWrite(Guid? id)
        {
            ViewData["lstTypeRequestWriteDocument"] = iDAS.Service.Common.Resource.GetRequestStatus();
            var model = new DocumentRequestBO()
            {

            };
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = model };
        }
        /// <summary>
        /// Lưu yêu cầu lưu trữ
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveRequestReceive(DocumentRequestBO item)
        {
            var result = false;

            try
            {
                DocumentRequestService.SaveRequestReceive(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }
        /// <summary>
        /// Lấy danh sách yêu cầu ban hành gửi tới tôi
        /// </summary>
        /// <returns></returns>
        public ActionResult GetRequestPromulgateCurrentUser(StoreRequestParameters parameter)
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = DocumentRequestService.GetRequestByTypeRole(pageIndex, pageSize, out count, iDAS.Service.Common.Resource.DocumentProcessRole.Promulgate).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        public ActionResult FormNextRoleRequest(DocumentRequestBO item, string departmentId = default(string), int order = 0, int typeRole = 0)
        {
            try
            {

                var data = new DocumentRequestBO() { TypeRole = typeRole, DepartmentId = new Guid(departmentId), Order = order };
                return new Ext.Net.MVC.PartialViewResult { Model = data };
            }
            catch
            {
                return this.Direct();
            }
        }
        /// <summary>
        /// Yêu cầu soạn thảo xuất phát bởi đề nghị
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RequestWriteBySuggest(Guid? id)
        {
            ViewData["lstTypeRequestWriteDocument"] = iDAS.Service.Common.Resource.GetRequestStatus();
            var model = new DocumentRequestBO()
            {

            };

            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = model };
        }

        public ActionResult ResquestDesTroyForm(string documentId = "")
        {
            try
            {
                if (string.IsNullOrEmpty(documentId))
                    return this.Direct();
                var model = DocumentRequestService.CreateDestroyRequest(iDAS.Service.Common.Utilities.ConvertToGuid(documentId));
                model.Document.Id = iDAS.Service.Common.Utilities.ConvertToGuid(documentId);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }
        /// <summary>
        /// List yêu cầu hủy
        /// </summary>
        /// <param name="containerId"></param>
        /// <returns></returns>
        public ActionResult ListResquestDesTroy(string containerId)
        {
            ViewData["TypeRequestDocument"] = iDAS.Service.Common.Resource.GetRequestStatus();
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        /// <summary>
        /// Yêu cầu hủy
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult SaveRequestDestroy(DocumentRequestBO item)
        {
            var result = false;
                try
                {
                    item.DocumentId = item.Document.Id;
                    DocumentRequestService.InsertRequestDestroy(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            return this.Direct(result: result);
        }
        /// <summary>
        /// Hủy tài liệu
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult DestroyRequestDocument(string Id)
        {
            var result = false;
                try
                {
                    DocumentRequestService.DestroyRequestDocument(new Guid(Id));
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            return this.Direct(result: result);
        }

        
        /// <summary>
        /// Insert yêu cầu sửa đổi
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult SaveRequestAdjust(DocumentRequestBO item)
        {
            var result = false;
          
                try
                {
                    item.DocumentId = item.Document.Id;
                    item.ProcessStatus = (int)iDAS.Service.Common.Resource.RequestProcessStatus.New;
                    DocumentRequestService.SaveRequestAdjust(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            
            return this.Direct(result: result);
        }

        /// <summary>
        /// gửi yêu cầu sửa đổi
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>

        public ActionResult SendRequestAdjust(DocumentRequestBO item)
        {
            var result = false;
            try
            {
                item.DocumentId = item.Document.Id;
                DocumentRequestService.SendRequestAdjust(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }


        /// <summary>
        /// Yêu cầu soạn thảo
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>

        public ActionResult SaveRequestWriteNew(DocumentRequestBO item)
        {
            var result = false;
          
                try
                {
                    item.ProcessStatus = (int)iDAS.Service.Common.Resource.RequestProcessStatus.New;  
                    DocumentRequestService.SaveRequestWriteNew(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            
            return this.Direct(result: result);
        }


        /// <summary>
        /// Yêu cầu phân phối
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>

        public ActionResult SaveRequestDistribute(DocumentRequestBO item)
        {
            var result = false;

            try
            {
                item.DocumentId = item.Document.Id;
                DocumentRequestService.SaveRequestDistribute(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }

        /// <summary>
        /// gửi yêu cầu viết mới
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>

        public ActionResult SendRequestWriteNew(DocumentRequestBO item)
        {
            var result = false;
            try
            {

                DocumentRequestService.SendRequestWriteNew(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }
        /// <summary>
        /// Gửi yêu cầu phân phối
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult SendRequestDistribute(DocumentRequestBO item)
        {
            var result = false;
            try
            {
                item.DocumentId = item.Document.Id;
                DocumentRequestService.SendRequestDistribute(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }
        /// <summary>
        /// Xóa yêu cầu soạn thảo
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public ActionResult DeleteRequestWriteNew(DocumentRequestBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentRequestService.DeleteRequestWriteNew(item.Id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Get all data request destroy
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult GetRequestDestroy(StoreRequestParameters param, iDAS.Service.Common.Resource.RequestStatus type = iDAS.Service.Common.Resource.RequestStatus.All)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentRequestService.GetRequestDestroy(pageIndex, pageSize, out count, type).ToList();
            return this.Store(data, count);
        }

        /// <summary>
        /// get yêu cầu sửa đổi
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult GetRequestAdjust(StoreRequestParameters param, iDAS.Service.Common.Resource.RequestProcessStatus type = iDAS.Service.Common.Resource.RequestProcessStatus.None)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentRequestService.GetRequestAdjust(pageIndex, pageSize, out count, type).ToList();
            return this.Store(data, count);
        }

        /// <summary>
        /// Get phong ban dc phan cong
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult GetDepartmentAllowRequest(StoreRequestParameters param)
        {
            var data = DocumentProcessService.GetDepartmentAllowRequest(iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument);
            return this.Store(data);
        }

        /// <summary>
        /// Get yêu cầu soạn thảo
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult GetRequestWriteNew(StoreRequestParameters param, iDAS.Service.Common.Resource.RequestProcessStatus type = iDAS.Service.Common.Resource.RequestProcessStatus.None, iDAS.Service.Common.Resource.DocumentEmployeeRole status = iDAS.Service.Common.Resource.DocumentEmployeeRole.All)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentRequestService.GetRequestWriteNew(pageIndex, pageSize, out count, type, status).ToList();
            return this.Store(data, count);
        }
        /// <summary>
        /// Get yêu cầu phân phối
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult GetRequestDistribute(StoreRequestParameters param, iDAS.Service.Common.Resource.RequestStatus type = iDAS.Service.Common.Resource.RequestStatus.All, iDAS.Service.Common.Resource.DocumentEmployeeRole status = iDAS.Service.Common.Resource.DocumentEmployeeRole.All)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentRequestService.GetRequestDistribute(pageIndex, pageSize, out count, type, status).ToList();
            return this.Store(data, count);
        }



        ///// <summary>
        ///// Get yeu cau huy
        ///// </summary>
        ///// <param name="param"></param>
        ///// <returns></returns>
        //public ActionResult GetRequestDesTroy(StoreRequestParameters param)
        //{
        //    var data = DocumentRequestService.GetRequestDesTroy().ToList();
        //    return this.Store(data);
        //}

        
        /// <summary>
        /// Chi tiết yêu cầu soạn thảo
        /// </summary>
        /// <returns></returns>
        public ActionResult DetailRequestWrite(string RequestId = default(string))
        {
            var check = DocumentProcessService.CheckRoleTypeExits(iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument);
            if (check)
            {
                object model = null;
                if (RequestId == default(string))
                {
                    model = new DocumentRequestBO() { Check=true, Document = new DocumentBO() };
                    ViewData.Add("Operation", Common.Operation.Create);
                    ViewData["TypeRequestDocument"] = iDAS.Service.Common.Resource.GetRequestStatus();
                }
                else
                {
                    model = DocumentRequestService.GetDetailRequestWriteNew(iDAS.Service.Common.Utilities.ConvertToGuid(RequestId));
                    ViewData.Add("Operation", Common.Operation.Update);
                    ViewData["TypeRequestDocument"] = iDAS.Service.Common.Resource.GetRequestStatus();
                }

                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                X.Msg.Alert("Thông báo", Common.Resource.AccessDeny).Show();
                return this.Direct("");
            }
        }


        /// <summary>
        /// Yêu cầu phân phối 
        /// </summary>
        /// <returns></returns>
        public ActionResult DistributeRequest(string Id = default(string))
        {
            var check = DocumentProcessService.CheckRoleTypeExits(iDAS.Service.Common.Resource.DocumentProcessRole.ApproveDocument);
            if (check)
            {
                object model = null;
                if (Id == default(string))
                {
                    model = new DocumentRequestBO() { Document = new DocumentBO() };
                    ViewData.Add("Operation", Common.Operation.Create);
                    ViewData["TypeRequestDocument"] = iDAS.Service.Common.Resource.GetRequestStatus();
                }
                else
                {
                    model = DocumentRequestService.GetDetailRequestWriteNew(iDAS.Service.Common.Utilities.ConvertToGuid(Id));
                    ViewData.Add("Operation", Common.Operation.Update);
                    ViewData["TypeRequestDocument"] = iDAS.Service.Common.Resource.GetRequestStatus();
                }

                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                X.Msg.Alert("Thông báo", Common.Resource.AccessDeny).Show();
                return this.Direct("");
            }
        }


        /// <summary>
        /// Chi tiết  Yêu cầu phân phối 
        /// </summary>
        /// <returns></returns>
        public ActionResult DetailDistributeRequest(string Id = default(string))
        {
            var model = DocumentRequestService.GetDetailRequestDistribute(iDAS.Service.Common.Utilities.ConvertToGuid(Id));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }



        /// <summary>
        ///   Yêu cầu phân phối 
        /// </summary>
        /// <returns></returns>
        public ActionResult FormInforDistributeRequest(string documentId = default(string), string DocumentSuggestId = default(string))
        {
            try
            {
                if (string.IsNullOrEmpty(documentId))
                    return this.Direct();
                var model = DocumentRequestService.CreateDistributeRequest(iDAS.Service.Common.Utilities.ConvertToGuid(documentId));
                    
                model.Document.Id = iDAS.Service.Common.Utilities.ConvertToGuid(documentId);
                model.DocumentSuggestId = iDAS.Service.Common.Utilities.ConvertToGuid(DocumentSuggestId);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }


        /// <summary>
        /// Chi tiết yêu cầu hủy
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public ActionResult DetailResquestDesTroy(string RequestId = "")
        {
            var model = DocumentRequestService.GetdataDetailRequest(iDAS.Service.Common.Utilities.ConvertToGuid(RequestId));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Chi tiết yêu cầu sửa đổi
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public ActionResult DetailResquestAdjust(string RequestId = "")
        {
            var model = DocumentRequestService.GetdataDetailRequestAdjust(iDAS.Service.Common.Utilities.ConvertToGuid(RequestId));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        /// <summary>
        /// Insert yêu cầu hủy
        /// </summary>
        /// <returns></returns>
        public ActionResult ResquestAdjustNewForm()
        {
            var model = new DocumentRequestBO() { Check=true,Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        /// <summary>
        /// Form yêu cầu hủy
        /// </summary>
        /// <returns></returns>
        public ActionResult ResquestDesTroyNewForm(string DocumentId = default(string))
        {
            if (DocumentId == default(string))
            {
                var model = new DocumentRequestBO() { Document = new DocumentBO() };
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            else
            {
                var model = DocumentRequestService.GetDataRequestDestroy(iDAS.Service.Common.Utilities.ConvertToGuid(DocumentId));
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
        }
        /// <summary>
        /// update yêu cầu
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult UpdateRequest(DocumentRequestBO item)
        {
            var result = false;

            try
            {
                DocumentRequestService.UpdateRequest(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }


        /// <summary>
        /// Xóa yêu cầu biên soạn
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(Guid Id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentRequestService.Remove(Id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// View danh sách yêu cầu viết mới
        /// </summary>
        /// <returns></returns>
        public Ext.Net.MVC.PartialViewResult RequestNewIndex(string containerId)
        {
            ViewData["TypeRequestDocument"] = iDAS.Service.Common.Resource.GetRequestProcessStatusText();
            ViewData["TypeGetAssignerStatus"] = iDAS.Service.Common.Resource.GetAssignAreaRoleType();

            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }

        public ActionResult InfoRequestWrite(string Id = default(string), string Name = "")
        {
            var model = new DocumentRequestBO() { Document = new DocumentBO() };
            model.Id = iDAS.Service.Common.Utilities.ConvertToGuid(Id);
            model.Document.Name = Name;
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }


        #endregion
        #region CuongPC

        /// <summary>
        /// Lấy danh sách yêu cầu lưu trữ
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public ActionResult GetDataArchive(StoreRequestParameters parameter)
        {
            var pageIndex = parameter.Page;
            var pageSize = parameter.Limit;
            var count = 0;
            var data = DocumentRequestService.GetRequestByTypeRole(pageIndex, pageSize, out count, iDAS.Service.Common.Resource.DocumentProcessRole.Archiver).ToList();
            return this.Store(data, count);
        }
        public ActionResult Recover(string id = default(string))
        {
            var result = false;
            try
            {
                DocumentRequestService.Recover(new Guid(id));
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}