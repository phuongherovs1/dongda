﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class AssignController : FrontController
    {
        public ActionResult Index()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetData()
        {
            try
            {
                var data = DocumentRequestService.GetDataAssignArea().ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetTableData(StoreRequestParameters param, iDAS.Service.Common.Resource.RequestStatus status = Service.Common.Resource.RequestStatus.All)
        {
            try
            {
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                var data = DocumentRequestService.GetDataCreateByAssigner(pageIndex, pageSize, out count, status).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetDepartmentData()
        {
            try
            {
                var data = DepartmentService.GetAll().ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult Save(DocumentRequestBO request)
        {
            var result = false;
            try
            {
                DocumentRequestService.SaveAssign(request.NewRequest);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult Send(DocumentRequestBO request)
        {
            var result = false;
            try
            {
                DocumentRequestService.SendAssign(request.NewRequest);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// Yêu cầu soạn thảo xuất phát bởi yêu cầu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RequestWrite(Guid? id)
        {
            ViewData["lstTypeRequestWriteDocument"] = iDAS.Service.Common.Resource.GetRequestStatus();
            var model = new DocumentRequestBO()
            {

            };
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = model };
        }
        /// <summary>
        /// Yêu cầu soạn thảo xuất phát bởi đề nghị
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RequestWriteBySuggest(Guid? id)
        {
            ViewData["lstTypeRequestWriteDocument"] = iDAS.Service.Common.Resource.GetRequestStatus();
            var model = new DocumentRequestBO()
            {
            };
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = model };
        }
        /// <summary>
        /// Yêu cầu soạn thảo xuất phát bởi đề nghị
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Revert(Guid id)
        {
            var result = false;
            try
            {
                DocumentRequestService.RevertAssign(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(Guid id)
        {
            var result = false;
            try
            {
                DocumentRequestService.Delete(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Destroy(Guid id)
        {
            var result = false;
            try
            {
                DocumentRequestService.DestroyAssign(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #region Phân công

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isSuggest"></param>
        /// <returns></returns>
        public ActionResult AssignForm(Guid id, bool isSuggest)
        {
            try
            {
                var model = DocumentRequestService.GetAssignInfoByRequestId(id);
                var viewName = model.IsSuggest ? (model.TypeWrite == true ? "RequestWriteBySuggest" : "RequestAdjustBySuggest") : "RequestWrite";
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = viewName };
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct();
            }
        }

        public ActionResult GetEmployeeRelation(Guid suggestId)
        {
            try
            {
                var data = DocumentWriteSuggestService.GetEmployeeRoleBySuggest(suggestId);
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        #endregion

        #region Phân công tạo mới yêu cầu soạn thảo
        /// <summary>
        /// Form yêu cầu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RequestWriteByAssigner(string id)
        {
            try
            {
                var model = DocumentRequestService.GetFormRequestWriteByAssigner(id);
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = model };
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct();
            }

        }
        /// <summary>
        /// Lưu yêu cầu
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult SaveRequestWriteByAssigner(DocumentRequestBO request)
        {
            var result = false;
            try
            {
                DocumentRequestService.SaveRequestWriteByAssigner(request);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// Gửi yêu cầu
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult SendRequestWriteByAssigner(DocumentRequestBO request)
        {
            var result = false;
            try
            {
                DocumentRequestService.SendRequestWriteByAssigner(request);
                result = true;
                Notify(TitleResourceNotifies.RequestWriteDocument + " \""+request.Document.Name+"\"", request.Note, request.EmployeePerform.Id, UrlResourceNotifies.RequestWriteByAssigner, new { id = request.Id });
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult RevertRequestWriteByAssigner(Guid id)
        {
            var result = false;
            try
            {
                DocumentRequestService.RevertRequestWriteByAssigner(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult DeleteRequestWriteByAssigner(Guid id)
        {
            var result = false;
            try
            {
                DocumentRequestService.Delete(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetDepartmentWithAssignRole()
        {
            try
            {
                var departments = DocumentProcessService.GetDepartmentByAssignRole().ToList(); //DocumentProcessService.GetDeparmentByRoleType((int)iDAS.Service.Common.Resource.DocumentProcessRole.AssignmentWriteAndEdit).ToList();
                return this.Store(departments);
            }
            catch (Exception)
            {

            }
            return this.Direct();
        }
        #endregion

        #region Yêu cầu phân công phát sinh từ đề nghị
        public ActionResult SuggestToAssignerByReviewer(string id)
        {
            try
            {
                var data = DocumentRequestService.GetSuggestToAssignItem(new Guid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = data };
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);

            }
            return this.Direct();
        }
        public ActionResult SuggestToAssignerBySuggestor(string id)
        {
            try
            {
                var data = DocumentRequestService.GetSuggestToAssignItem(new Guid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = data };
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);

            }
            return this.Direct();
        }
        #endregion
    }
}