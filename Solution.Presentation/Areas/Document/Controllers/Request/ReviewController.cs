﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class ReviewController : FrontController
    {
        public ActionResult Index()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }

        /// <summary>
        /// Lấy danh sách đề nghị kiểm duyệt
        /// </summary>
        /// <param name="param"></param>
        /// <param name="approveStatus"></param>
        /// <returns></returns>
        public ActionResult GetData(StoreRequestParameters param, Service.Common.Resource.ApproveStatus approveStatus = Service.Common.Resource.ApproveStatus.New)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            object data = null;///DocumentReviewService.GetAll(pageIndex, pageSize, out count, approveStatus);
            return this.Store(data, count);
        }

        /// <summary>
        /// Form chi tiết thông tin đề nghị kiểm duyệt
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public ActionResult Form(string documentId, string id)
        {
            if (string.IsNullOrEmpty(documentId))
                return this.Direct();
            object model = null; //DocumentReviewService.CreateDefault(iDAS.Service.Common.Utilities.ConvertToGuid(documentId), id);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Gửi thông tin đề nghị kiểm duyệt
        /// </summary>
        /// <param name="adjustSuggest"></param>
        /// <returns></returns>
        public ActionResult SendApprove(DocumentReviewBO review)
        {
            object result = null;
            try
            {
               // review.Id = DocumentReviewService.Send(review);
                result = new { success = true, data = review.Id, message = Common.Resource.SystemSuccess };
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }

        /// <summary>
        /// Lưu thông tin đề nghị kiểm duyệt tài liệu
        /// </summary>
        /// <param name="review"></param>
        /// <returns></returns>
        public ActionResult Save(DocumentReviewBO review)
        {
            object result = null;
            try
            {
                //review.ReviewBy = review.EmployeeReviewBy.Id;
                //if (review.Id == Guid.Empty)
                //    review.Id = DocumentReviewService.Insert(review);
                //else
                //    DocumentReviewService.Update(review);
                result = new { success = true, data = review.Id, message = Common.Resource.SystemSuccess };
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }

    }
}