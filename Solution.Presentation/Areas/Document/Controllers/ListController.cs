﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using System.Diagnostics;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    // Danh sách bao gồm cây thư mục chứa tài liệu bên trong, tài liệu bên ngoài và tài liệu phân phối
    // và tài liệu xử lý tương ứng
    public class ListController : FrontController
    {
        public ActionResult Index()
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            ViewData["Status"] = iDAS.Service.Common.Resource.GetDocumentStatusOnCategory();
            return PartialView();
        }
        // Đếm số danh mục tài liệu nội bộ cho phòng ban
        public ActionResult CountInternalDocumentCategory(Guid? departmentId)
        {
            var defaultValue = 10;
            return Json(defaultValue);
        }
        // Đến số tài liệu bên ngoài cho phòng ban
        public ActionResult CountExternalDocumentCategory(Guid? departmentId)
        {
            var defaultValue = 0;
            return Json(defaultValue);
        }
        // Đếm số tài liệu phân phối
        public ActionResult CountDistributeDocumentCategory(Guid? departmentId)
        {
            var defaultValue = 0;
            return Json(defaultValue);
        }

        public ActionResult LoadDocumentApprove()
        {
            var data = DocumentSummaryService.GetDocumentApproveByCurrentUser().ToList();
            return this.Store(data);
        }
        public ActionResult LoadDocumentApprovePagging(StoreRequestParameters param,string filterName = "")
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentSummaryService.GetDocumentPaggingApproveByCurrentUser(pageIndex, pageSize, out count, filterName);
            return this.Store(data, count);
        }
        public ActionResult LoadPromulgate()
        {
            var data = DocumentSummaryService.GetPromulgate();
            return this.Store(data);
        }
        public ActionResult LoadPaggingPromulgate(StoreRequestParameters param, string filterName = "")
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentSummaryService.GetPaggingPromulgate(pageIndex, pageSize, out count, filterName);
            return this.Store(data, count);
        }
        public ActionResult LoadArchive()
        {
            var data = DocumentSummaryService.GetArchive();
            return this.Store(data);
        }
        public ActionResult LoadPaggingArchive(StoreRequestParameters param, string filterName = "")
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentSummaryService.GetPaggingArchive(pageIndex, pageSize, out count, filterName);
            return this.Store(data, count);
        }
        #region Dùng chung
        public ActionResult DocumentSelect(bool isDestroy = false, string actionName = "", string controllerName = "", string areaName = "", string param = "", string customHandler = "" , bool selectMulti = false)
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
                ViewData["IsDestroy"] = isDestroy;
                ViewData["ActionName"] = actionName;
                ViewData["ControllerName"] = controllerName;
                ViewData["AreaName"] = areaName;
                ViewData["Param"] = param;
                ViewData["CustomHanler"] = customHandler;
                ViewData["SelectMulti"] = selectMulti;
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            return null;
        }

        public ActionResult DocumentSelectAll()
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            return null;
        }

        #endregion
    }
}