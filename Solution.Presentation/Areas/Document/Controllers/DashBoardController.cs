﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class DashBoardController : FrontController
    {
        //
        // GET: /Document/DashBoard/
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                WrapByScriptTag = false
            };
        }
        public ActionResult DocumentList()
        {
            return PartialView();
        }
        public ActionResult FormNotifyMore()
        {
            return PartialView();
        }
                 
        #region Trách nhiệm liên quan đến tôi
        public ActionResult AssignmentByMe()
        {
            try
            {
                ViewData["RequestStatus"] = iDAS.Service.Common.Resource.GetRequestStatus();
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData};
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct();
            }
        }

        public ActionResult WriteByMe()
        {
            try
            {
                ViewData["RequestStatus"] = iDAS.Service.Common.Resource.GetRequestProcessStatusText();
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct();
            }

        }
        public ActionResult ApprovalByMe()
        {
            try
            {
                ViewData["TypeRequestDocument"] = iDAS.Service.Common.Resource.GetRequestProcessStatusText();
                ViewData["TypeGetAssignerStatus"] = iDAS.Service.Common.Resource.GetAssignAreaRoleType();
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct();
            }
        }
        public ActionResult PublishByMe()
        {
            try
            {
                ViewData["TypeRequestDocument"] = iDAS.Service.Common.Resource.GetRequestProcessStatusText();
                ViewData["TypeGetAssignerStatus"] = iDAS.Service.Common.Resource.GetAssignAreaRoleType();
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct();
            }

        }
        public ActionResult ArchiveByMe()
        {
            try
            {
                ViewData["TypeRequestDocument"] = iDAS.Service.Common.Resource.GetRequestProcessStatusText();
                ViewData["TypeGetAssignerStatus"] = iDAS.Service.Common.Resource.GetAssignAreaRoleType();
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct();
            }

        } 
        #endregion
	}
}