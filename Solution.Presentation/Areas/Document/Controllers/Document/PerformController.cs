﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;
using System.Diagnostics;
namespace iDAS.Presentation.Areas.Document.Controllers
{
    /// <summary>
    /// Controller thực hiện việc soạn thảo
    /// </summary>
    public class PerformController : FrontController
    {
        public ActionResult Form(Guid requestId)
        {
            try
            {
                var model = DocumentRequestService.GetWriteInfoByRequestId(requestId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = model.TypeAdjust == true ? "FormAdjust" : "Form" };
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        /// <summary>
        /// Form thực hiện soạn thảo với trường hợp sửa đổi
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public ActionResult FormAdjust(Guid requestId)
        {
            try
            {
                var model = DocumentRequestService.GetWriteInfoByRequestId(requestId);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult GetData()
        {
            try
            {
                var data = DocumentRequestService.GetDataWritenArea().ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult GetCompilationData(Guid requestId)
        {
            try
            {
                var data = DocumentRequestService.GetRequestWriteByCompilation(requestId).ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        /// <summary>
        /// Yêu cầu soạn thảo mới
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public ActionResult RequestWriteForm(string id, Guid parentId)
        {
            try
            {
                var model = DocumentRequestService.GetRequestWriteFormItem(id, parentId);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Danh sách yêu cầu soạn thảo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CompilationForm(Guid id)
        {
            return new Ext.Net.MVC.PartialViewResult { Model = id };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Send(DocumentRequestBO item)
        {
            var result = false;
            try
            {
                DocumentRequestService.SendRequestWrite(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult CompleteByCompile(DocumentRequestBO item)
        {
            var result = false;
            try
            {
                DocumentRequestService.CompleteByCompile(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult SendById(Guid id)
        {
            var result = false;
            try
            {
                DocumentRequestService.Send(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Revert(Guid id)
        {
            var result = false;
            try
            {
                DocumentRequestService.RevertPerform(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(Guid id)
        {
            var result = false;
            try
            {
                DocumentRequestService.Delete(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Destroy(Guid id)
        {
            var result = false;
            try
            {
                DocumentRequestService.DestroyAssign(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Save(DocumentRequestBO item)
        {
            var result = false;
            try
            {
                DocumentRequestService.SaveRequestWrite(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult Complete(DocumentRequestBO request)
        {
            var result = false;
            try
            {
                DocumentRequestService.CompleteWrite(request);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }

        public ActionResult Approve(Guid id)
        {
            var result = false;
            try
            {
                DocumentRequestService.Approve(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        public ActionResult Disaprrove(Guid id)
        {
            var result = false;
            try
            {
                DocumentRequestService.Disapprove(id);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }

        #region biên soạn gửi đề nghị ban hành
        [HttpPost]
        public ActionResult SendReviewForm(DocumentRequestBO request)
        {
            object result = false;
            try
            {
                var data = DocumentRequestService.SaveRequestWrite(request);
                return this.Json(data);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }

        public ActionResult ReviewForm(string extendId, string documentId, string departmentId, string order)
        {
            try
            {
                var documentRequest = DocumentRequestService.GetById(new Guid(extendId));
                var data = new DocumentResponsibilityBO()
                {
                    ExtendId = new Guid(extendId),
                    DocumentId = new Guid(documentId),
                    DepartmentId = new Guid(departmentId),
                    Order = string.IsNullOrEmpty(order) ? (int?)null : Convert.ToInt16(order)
                };
                if (documentRequest.CheckerBy.HasValue)
                {
                    var employeePerform = EmployeeService.GetEmployeeGeneralInfo(documentRequest.CheckerBy.Value);
                    data.EmployeeRecive = new DocumentEmployeeBO()
                    {
                        Id = employeePerform.Id,
                        Name = employeePerform.Name,
                        AvatarUrl = employeePerform.AvatarUrl,
                        RoleNames = employeePerform.RoleNames,
                    };
                }
                return new Ext.Net.MVC.PartialViewResult { Model = data };
            }
            catch (Exception e)
            {
            }
            return this.Direct();
        }

        [HttpPost]
        public ActionResult SendPublishSuggest(DocumentResponsibilityBO responsibility)
        {
            var result = false;
            try
            {
                DocumentRequestService.SendPublishSuggest(responsibility);
                result = true;
                if(responsibility.RoleType == 5)
                    Notify(TitleResourceNotifies.RequestCheckDocument + (Session["DocumentName"] != null? "\"" + Session["DocumentName"] + "\"" : ""), responsibility.ContentSuggest, responsibility.EmployeeRecive.Id, UrlResourceNotifies.RequestCheckByAssigner, new { id = responsibility.Id, name= "Đề nghị kiểm tra" });
                else if (responsibility.RoleType == 6)
                    Notify(TitleResourceNotifies.RequestApproveDocument + (Session["DocumentName"] != null ? "\"" + Session["DocumentName"] + "\"" : ""), responsibility.ContentSuggest, responsibility.EmployeeRecive.Id, UrlResourceNotifies.RequestApproveByAssigner, new { id = responsibility.Id, name = "Đề nghị phê duyệt" });
                else if (responsibility.RoleType == 7)
                    Notify(TitleResourceNotifies.RequestPorDocument + (Session["DocumentName"] != null ? "\"" + Session["DocumentName"] + "\"" : ""), responsibility.ContentSuggest, responsibility.EmployeeRecive.Id, UrlResourceNotifies.RequestPublishByAssigner, new { id = responsibility.Id, name = "Đề nghị ban hành" });
                this.ShowNotify(iDAS.Presentation.Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }

        #endregion

        #region Tự tạo tài liệu soạn thảo

        public ActionResult GetDeparmentWithRoleTypeWrite()
        {
            try
            {
                var listDepartment = DepartmentService.GetAll().ToList().OrderBy(x => x.Name);
                return this.Store(listDepartment);
                //var data = DepartmentService.GetDepartmentsByCurrentUser();//.GetDeparmentByRoleType((int)iDAS.Service.Common.Resource.DocumentProcessRole.Write);
                //return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult NewDocument(string id)
        {
            try
            {
                var model = DocumentRequestService.GetFormWriteByComiplator(id);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        [HttpPost]
        public ActionResult SendDocument(DocumentRequestBO request)
        {
            try
            {
                var data = DocumentRequestService.SendDocumentByCompilator(request);
                return this.Json(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult SaveDocument(DocumentRequestBO request)
        {
            var result = false;
            try
            {
                DocumentRequestService.SaveDocumentByCompilator(request);
                result = true;
                this.ShowNotify(iDAS.Presentation.Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        public ActionResult CompleteDocument(DocumentRequestBO request)
        {
            var result = false;
            try
            {
                DocumentRequestService.CompleteDocumentByCompilator(request);
                result = true;
                this.ShowNotify(iDAS.Presentation.Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }

        public ActionResult DeleteDocument(Guid Id)
        {
            var result = false;
            try
            {
                DocumentRequestService.DeleteDocumentByRequestId(Id);
                result = true;
                this.ShowNotify(iDAS.Presentation.Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(iDAS.Presentation.Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        public ActionResult GetTableData(StoreRequestParameters param, iDAS.Service.Common.Resource.RequestStatus status = Service.Common.Resource.RequestStatus.All)
        {
            try
            {
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                var data = DocumentRequestService.GetDataCreateByCompilator(pageIndex, pageSize, out count, status).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        #endregion



    }
}