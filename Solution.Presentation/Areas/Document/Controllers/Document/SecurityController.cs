﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using Ext.Net.MVC;
using System.Web.Mvc;
using iDAS.Service;
using iDAS.Service.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class SecurityController : FrontController
    {
        //
        // GET: /Document/Security/

        #region Danh sách
        public ActionResult Index(string departmentName, string departmentId)
        {
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        public ActionResult Form(string alignTo, string renderTo, string storeId)
        {
            ViewData["AlignToId"] = string.IsNullOrEmpty(alignTo) ? "ctDesktop" : alignTo;
            ViewData["RenderToId"] = string.IsNullOrEmpty(renderTo) ? "ctDesktop" : renderTo;
            ViewData["storeId"] = string.IsNullOrEmpty(storeId) ? "storeSecurity" : storeId;
            var model = new DocumentSecurityBO();
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = model };
        }

        /// <summary>
        /// Get all document sercurity
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAll()
        {
            var data = DocumentSecurityService.GetAll();
            return this.Store(data);
        }

        //public ActionResult GetData(StoreRequestParameters param, string departmentId)
        //{
        //    try
        //    {
        //        var pageIndex = param.Page;
        //        var pageSize = param.Limit;
        //        var count = 0;
        //        var data = DocumentSecurityService.GetByDepartment(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(departmentId)).ToList();
        //        return this.Store(data, count);

        //    }
        //    catch (Exception)
        //    {
        //        return this.Direct();
        //    }
        //}
        #endregion
        #region Thêm mới
        [ValidateInput(false)]
        public ActionResult Create(DocumentSecurityBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentSecurityService.Insert(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
      
        #region chi tiết
        public ActionResult Delete(Guid Id, Guid departmentId)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentSecurityService.Delete(Id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);

        }

        [ValidateInput(false)]
        public ActionResult Update(DocumentSecurityBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    //DocumentSecurityService.UpdateDocumentSecurity(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        #endregion
      
      
    }
}