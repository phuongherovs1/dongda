﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class ReferenceController : FrontController
    {
        public ActionResult Index(Guid documentId, string storeDocumentId)
        {
            ViewData["DocumentId"] = documentId;
            ViewData["StoreDocumentId"] = storeDocumentId;
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }

        /// <summary>
        /// Lấy danh sách tài liệu tham chiếu theo tài liệu đã có
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public ActionResult GetData(StoreRequestParameters paramer, Guid documentId)
        {
            var count = 0;
            var pageIndex = paramer.Page;
            var pageSize = paramer.Limit;
            var data = DocumentReferenceService.GetByDocumentId(documentId, pageIndex, pageSize, out count).ToList();
            return this.Store(data, count);
        }

        public ActionResult InsertDocumentReference(List<Guid> data, Guid documentId)
        {
            try
            {
                DocumentReferenceService.InsertBySelectDocument(data, documentId);
            }
            catch (Exception)
            {
                
            }
            return this.Direct();
        }

        public ActionResult Form(string Id, Guid? documentId)
        {
            try
            {
                var model = DocumentReferenceService.GetFormItem(Id, documentId==null?Guid.Empty:documentId.Value);

                if (Id != null)
                {
                    ViewData.Add("Operation", Common.Operation.Update);
                }
                else
                {
                    ViewData.Add("Operation", Common.Operation.Create);
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception)
            {
            }
            return this.Direct();
        }

        public ActionResult GetPublishDocumentReference(StoreRequestParameters param, string departmentId, string filterName)
        {
            try
            {
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                var data = DocumentService.GetDocumentDistributeToCurrentUser(pageIndex, pageSize, out count, filterName, departmentId).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
            }
            return this.Direct();
        }

        public ActionResult SelectDocument(Guid documentId)
        {
            try
            {
                ViewData["DocumentId"] = documentId;
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData};
            }
            catch (Exception)
            {
            }
            return this.Direct();
        }

        public ActionResult ReferenceTypeForm()
        {
            return new Ext.Net.MVC.PartialViewResult();
        }

        [HttpPost]
        public ActionResult CreateReferenceType(DocumentReferenceTypeBO data)
        {
            var result = false;
            try
            {
                DocumentReferenceTypeService.Insert(data);
                result = true;
            }
            catch (Exception)
            {
            }
            return this.Direct(result);
        }

        [HttpPost]
        public ActionResult SaveReference(DocumentReferenceBO data)
        {
            try
            {
                DocumentReferenceService.Save(data);
            }
            catch (Exception)
            {
            }
            return this.Direct();
        }

        public ActionResult Delete(Guid Id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentReferenceService.Delete(Id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }


        public ActionResult UpdateReference(DocumentReferenceBO data)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentReferenceService.UpdateReference(data);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetReferenceType()
        {
            try
            {
                var data = DocumentReferenceTypeService.GetAll();
                return this.Store(data);
            }
            catch (Exception)
            {

            }
            return this.Direct();
        }
    }
}