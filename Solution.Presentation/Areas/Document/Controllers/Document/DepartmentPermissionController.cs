﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class DepartmentPermissionController : FrontController
    {
        /// <summary>
        /// Insert/Update
        /// </summary>
        /// <param name="data"></param>
        /// <param name="departmentId"></param>
        /// <param name="strTitle"></param>
        /// <param name="strEmployee"></param>
        /// <returns></returns>
        public ActionResult Create(DocumentDepartmentPermissionBO data, string objects)
        {
            object result = null;
            try
            {
                DocumentDepartmentPermissionService.InsertPermission(data, objects);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
        public ActionResult UpdatePermission(Guid id, bool isSettingProcess = false, bool isSettingCategory = false, bool isArchive = false)
        {
            object result = null;
            try
            {
                DocumentDepartmentPermissionService.UpdateRecordPermission(id, isSettingProcess, isSettingCategory, isArchive);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(DocumentDepartmentPermissionBO item)
        {
            var result = false;
            try
            {
                DocumentDepartmentPermissionService.UpdatePermission(item);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// Danh sách
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult GetData(StoreRequestParameters param, string departmentId = default(string), string role = default(string))
        {
            try
            {
                var departmentSplitId = Guid.Empty;
                if (departmentId.Contains('_'))
                {
                    departmentSplitId = new Guid(departmentId.Split('_')[1]);
                }
                else
                {
                    if (!string.IsNullOrEmpty(departmentId))
                    {
                        departmentSplitId = new Guid(departmentId);
                    }
                }
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                var data = DocumentDepartmentPermissionService.GetAll(pageSize, pageIndex, out count, departmentSplitId);
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }

        }

        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentDepartmentPermissionService.Delete(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        public ActionResult GetDepartmentRole()
        {
            object result = null;
            try
            {
                var departmentIds = DocumentDepartmentPermissionService.GetDepartmentRoleSettingCategoryByUser().ToList();
                return Json(departmentIds);
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }

        [HttpPost]
        public ActionResult CheckDepartmentRole(Guid departmentId)
        {
            object result = null;
            try
            {
                var checkRole = (DocumentDepartmentPermissionService.CheckRoleSettingCategory(departmentId) || DocumentDepartmentPermissionService.CheckRoleArchive(departmentId));
                return Json(checkRole);
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
    }
}