﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;
using System.Diagnostics;
using System.Web.UI;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class DocumentRoleController : FrontController
    {
        //
        // GET: /Document/DocumentRole/
        public ActionResult Index()
        {
            ViewData["lstTypeDocumentRole"] = iDAS.Service.Common.Resource.GetDocumentRole();
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }


        public ActionResult ListRole(string nameRole, string IdRole, string name)
        {
            ViewData["name"] = name;
            var data = DocumentService.GetRoleByText(nameRole, IdRole);
            return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
        }

        public ActionResult ShowRightRole(string nameRole, string IdRole)
        {
            return new Ext.Net.MVC.PartialViewResult {  };
        }

        public ActionResult Form()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }
    }
    
}
