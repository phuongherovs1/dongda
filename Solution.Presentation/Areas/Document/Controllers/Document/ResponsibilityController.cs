﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class ResponsibilityController : FrontController
    {
        //
        // GET: /Document/Responsibility/
        public ActionResult AddRole(string documentRequestId = default(string))
        {
            var request = DocumentRequestService.GetById(new Guid(documentRequestId));
            var data = new DocumentResponsibilityBO() { DocumentId = new Guid(request.DocumentId.ToString()), Order = request.Order, DepartmentId = request.DepartmentId.Value };
            return new Ext.Net.MVC.PartialViewResult { Model = data };
        }
        public ActionResult CheckDocument(string id = default(string), string name = default(string))
        {
            try
            {
                ViewData["Tilte"] = name;
                var data = DocumentResponsibilityService.GetDetail(new Guid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult FormChangeRole(string id = default(string))
        {
            try
            {
                var data = DocumentResponsibilityService.GetDetail(new Guid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = data };
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult FormNextRole(string extendId = default(string), string documentId = default(string), string departmentId = default(string), int order = 0, string handler = default(string), int? roleType = null)
        {
            try
            {
                ViewData["Handler"] = handler;
                var data = new DocumentResponsibilityBO() { ExtendId = !string.IsNullOrEmpty(extendId) ? new Guid(extendId) : new Nullable<Guid>(), DocumentId = new Guid(documentId), DepartmentId = new Guid(departmentId), Order = order , RoleType = roleType };
                return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
            }
            catch(Exception ex)
            {
                return this.Direct();
            }
        }
        public ActionResult FormNextRoleApprove(string extendId = default(string), string documentId = default(string), string departmentId = default(string), int order = 0, string handler = default(string))
        {
            try
            {
                ViewData["Handler"] = handler;
                var data = new DocumentResponsibilityBO() { ExtendId = !string.IsNullOrEmpty(extendId) ? new Guid(extendId) : new Nullable<Guid>(), DocumentId = new Guid(documentId), DepartmentId = new Guid(departmentId), Order = order };
                return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult FormNextRoleArchivePublish(string documentId = default(string))
        {
            try
            {
                var obj = DocumentResponsibilityService.GetResponsibilitybyPublish(new Guid(documentId));
                var data = new DocumentResponsibilityBO() { DocumentId = new Guid(obj.DocumentId.ToString()), DepartmentId = new Guid(obj.DepartmentId.ToString()), Order = obj.Order };
                return new Ext.Net.MVC.PartialViewResult { Model = data };
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult RevertApproval(string id = default(string))
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentResponsibilityService.RevertApproval(new Guid(id));
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        public ActionResult Cancel(string id = default(string))
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentResponsibilityService.Cancel(new Guid(id));
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        public ActionResult ChangeRole(DocumentResponsibilityBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentResponsibilityService.ChangeRole(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        public ActionResult ApprovalDocument(string id = default(string), string name = default(string))
        {
            try
            {
                ViewData["Tilte"] = name;
                var data = DocumentResponsibilityService.GetDetail(new Guid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult GetRoleRelationship(StoreRequestParameters paramer, string documentId = default(string))
        {
            var count = 0;
            var pageIndex = paramer.Page;
            var pageSize = paramer.Limit;
            var data = DocumentResponsibilityService.GetByDocument(iDAS.Service.Common.Utilities.ConvertToGuid(documentId), pageIndex, pageSize, out count).ToList();
            return this.Store(data, count);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(DocumentResponsibilityBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentResponsibilityService.InsertResponsibility(item);
                    result = true;
                    if (item.RoleType == 5)
                        Notify(TitleResourceNotifies.RequestCheckDocument + (Session["DocumentName"] != null ? "\"" + Session["DocumentName"] + "\"" : ""), item.ContentSuggest, item.EmployeeRecive.Id, UrlResourceNotifies.RequestCheckByAssigner, new { id = item.Id, name = "Đề nghị kiểm tra" });
                    else if (item.RoleType == 6)
                        Notify(TitleResourceNotifies.RequestApproveDocument + (Session["DocumentName"] != null ? "\"" + Session["DocumentName"] + "\"" : ""), item.ContentSuggest, item.EmployeeRecive.Id, UrlResourceNotifies.RequestApproveByAssigner, new { id = item.Id, name = "Đề nghị phê duyệt" });
                    else if (item.RoleType == 7)
                        Notify(TitleResourceNotifies.RequestPorDocument + (Session["DocumentName"] != null ? "\"" + Session["DocumentName"] + "\"" : ""), item.ContentSuggest, item.EmployeeRecive.Id, UrlResourceNotifies.RequestPublishByAssigner, new { id = item.Id, name = "Đề nghị ban hành" });
                    else if (item.RoleType == 8)
                        Notify(TitleResourceNotifies.RequestArchivesDocument + (Session["DocumentName"] != null ? "\"" + Session["DocumentName"] + "\"" : ""), item.ContentSuggest, item.EmployeeRecive.Id, UrlResourceNotifies.RequestArchivesByAssigner, new { id = item.Id, name = "Đề nghị lưu chữ" });
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(DocumentResponsibilityBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentResponsibilityService.UpdateResponsibility(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Approval(DocumentResponsibilityBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentResponsibilityService.Approval(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentResponsibilityService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult RevertRole(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentResponsibilityService.RevertRole(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}