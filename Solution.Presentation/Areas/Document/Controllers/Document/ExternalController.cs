﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class ExternalController : FrontController
    {
        public ActionResult Form()
        {
            var model = new DocumentExternalBO();
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        public ActionResult Index()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }

        public ActionResult Insert(DocumentExternalBO data)
        {
            object result;
            try
            {
                data.Id = DocumentExternalService.Insert(data);
                result = new { success = true, message = Common.Resource.SystemSuccess, data = data.Id };
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
                throw;
            }
            return this.Direct(result);
        }
        /// <summary>
        /// Lấy danh sách nơi nhận bên ngoài
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentExternalService.Page(out count, pageIndex, pageSize).ToList();
            return this.Store(data, count);
        }
        public ActionResult GetEmployByDepartmentID(StoreRequestParameters param, string DepartmentID = default(string))
        {
            object data = null;

            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            if (DepartmentID.Contains('_'))
            {
                string[] DepartmentIdsl = DepartmentID.Trim().Split('_');
                DepartmentID = DepartmentIdsl[1];
            }
            data = EmployeeService.GetByDepartment(iDAS.Service.Common.Utilities.ConvertToGuid(DepartmentID));
                return this.Store(data, count);
            
            return this.Store(data, count);
        }
        /// <summary>
        /// Gọi form chọn nơi nhận
        /// </summary>
        /// <returns></returns>
        public ActionResult FormChooseDestination(string DocumentId = default(string), string DistributeId = default(string), string DocumentRequestId = default(string))
        {
            if (DistributeId == null)
            {
                var model = new DocumentDistributeBO() { DocumentExternal = new DocumentExternalBO() };
                model.DocumentId = iDAS.Service.Common.Utilities.ConvertToGuid(DocumentId);
                model.RequestId = iDAS.Service.Common.Utilities.ConvertToGuid(DocumentRequestId);
                model.IsExternal = false;
                ViewData.Add("Operation", Common.Operation.Create);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            else
            {
                var model = DocumentDistributeService.GetDetailDistribute(iDAS.Service.Common.Utilities.ConvertToGuid(DistributeId));
                ViewData.Add("Operation", Common.Operation.Read);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }

        }

        public ActionResult GetAll()
        {
            var data = DocumentExternalService.GetAll();
            return this.Store(data);
        }
    }
}