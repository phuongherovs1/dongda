﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;
using System.Diagnostics;
namespace iDAS.Presentation.Areas.Document.Controllers
{
    /// <summary>
    /// Controller thực hiện việc đổi người phân công
    /// </summary>
    public class TransferController : FrontController
    {
        public ActionResult Form(Guid requestId, string containerId, string alignTo, string fromWindow, int roleType, Guid departmentId)
        {
            var model = new DocumentRequestTransferBO()
            {
                RequestId = requestId
            };
            ViewData["ContainerId"] = string.IsNullOrEmpty(containerId) ? "ctDesktop" : containerId;
            ViewData["AlignTo"] = string.IsNullOrEmpty(alignTo) ? "ctDesktop" : alignTo;
            ViewData["RenderTo"] =  string.IsNullOrEmpty(fromWindow) ? "ctDesktop" : fromWindow;
            ViewData["RoleType"] = roleType;
            ViewData["DepartmentId"] = departmentId;
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        /// <summary>
        /// Gửi phân công 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult Send(DocumentRequestTransferBO item)
        {
            var result = false;
            try
            {
                DocumentRequestTransferService.Send(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// Lấy ra danh sách người có trách nhiệm để chuyển phân công
        /// </summary>
        /// <returns></returns>
        public ActionResult GetUserTransfer()
        {
            return this.Direct();
        }
    }
}