﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;
using System.Diagnostics;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    /// <summary>
    /// Controller thực hiện phân quyền truy cập cho tài liệu
    /// </summary>
    public class PermissionController : FrontController
    {
        //
        // GET: /Document/Permission/
        public ActionResult Index(Guid documentId)
        {
            return new Ext.Net.MVC.PartialViewResult { Model = documentId };
            
        }

        public ActionResult GetData(Guid documentId)
        {
            var data = DocumentPermissionService.GetByDocument(documentId).ToList();
            return this.Store(data);
        }

        public ActionResult Update(string dataJson)
        {
            DocumentPermissionService.Update(dataJson);
            return this.Direct();
        }
    }
}