﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;
using System.Diagnostics;
using iDAS.ADO;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class DocumentController : FrontController
    {
        //
        // GET: /Document/Document/
        public ActionResult Index()
        {
            ViewData["DocumentStatus"] = iDAS.Service.Common.Resource.GetEDocumentStatus();
            return PartialView();
        }

        public ActionResult GetAllByRequestDestroy()
        {
            var data = DocumentService.GetAllByRequestDestroy();
            return this.Store(data);
        }

        public ActionResult GetAllByRequestDistribute()
        {
            var data = DocumentService.GetAllByRequestDistribute();
            return this.Store(data);
        }
        public ActionResult GetAllByRequestAdjust(StoreRequestParameters param, bool isDestroy = false, string filterName = "", bool isDistribute = false, string departmentId = "")
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentService.GetAllByRequestAdjust(pageIndex, pageSize, out count, isDestroy, filterName, isDistribute, departmentId);
            return this.Store(data, count);
        }
        #region GetAll: Danh sách Document không phân trang dùng cho Combobox
        /// <summary>
        /// Danh sách tài liệu
        /// </summary>
        /// <returns></returns>
        public ActionResult GetAll()
        {
            var data = DocumentService.GetReferences();
            return this.Store(data);
        }

        #endregion

        public ActionResult Publish()
        {
            return PartialView();
        }
        public ActionResult Approval()
        {
            ViewData["TypeRequestDocument"] = iDAS.Service.Common.Resource.GetRequestStatus();
            return PartialView();
        }
        public ActionResult FillColor()
        {
            return this.Store(iDAS.Service.Common.Utilities.RenderColor());
        }
        /// <summary>
        /// Insert tài liệu vào danh mục noi bo
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult InsertDocumentCategory(DocumentCategoryBO item)
        {
            var result = false;
            try
            {
                DocumentService.InsertDocumentCategory(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// iNSERT DANH MUC tai lieu phan phoi
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult InsertDocumentCategoryDistribute(DocumentCategoryBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentService.InsertDocumentCategoryDistribute(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Xóa DANH MUC tai lieu phan phoi
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult DeleteDocumentCategoryDistribute(DocumentCategoryBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentService.DeleteDocumentCategoryDistribute(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// Xóa tài liệu khỏi danh mục
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult DeleteDocumentCategory(string DocumentId)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentService.DeleteDocumentCategory(iDAS.Service.Common.Utilities.ConvertToGuid(DocumentId));
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Lấy danh sách tài liệu đã ban hành
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="filterName">Tìm kiếm theo tên tài liệu hoặc mã tài liệu</param>
        /// <param name="isDistribute">True: tài liệu phân phối đến tôi, False: tài liệu không phân phối đến tôi</param>
        /// <param name="departmentId">Tài liệu theo phòng ban</param>
        /// <returns></returns>
        public ActionResult GetDataPublish(StoreRequestParameters param, bool isDestroy = false, string filterName = "", bool isDistribute = false, string departmentId = "")
        {
            try
            {
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                var data = DocumentService.GetDocumentPublish(pageIndex, pageSize, out count, isDestroy, filterName, isDistribute, departmentId);
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        #region Gọi Form
        /// <summary>
        /// Gọi Form chi tiết đề nghị ban hành
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Form(string id)
        {
            object model = null;
            ViewData["Status"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetAddEDocumentStatus());
            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new DocumentBO()
                {
                    IsExternal = false
                };
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = DocumentService.GetDetail(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        public ActionResult RemoveOnCategory(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ///
                    DocumentService.RemoveFromCategory(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// Gọi form tạo mới tài liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult FormCreate(string id = default(string))
        {
            object model = null;
            if (id == default(string) || String.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new DocumentBO();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = DocumentService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }


        public ActionResult InternalForm(Guid defaultCategory, string id, Guid departmentId)
        {
            try
            {
                var model = DocumentService.GetFormItem(defaultCategory, id, departmentId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }

        public ActionResult ExternalForm(Guid defaultCategory, string id, Guid departmentId)
        {
            try
            {
                ViewData["Status"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetExternalDocumentStatus());
                var model = DocumentService.GetFormItem(defaultCategory, id, departmentId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }

        /// <summary>
        /// Form thêm mới tài liệu bên ngoài
        /// </summary>
        /// <param name="defaultCategory"></param>
        /// <param name="id"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public ActionResult FormDocumentExternal(string id = default(string))
        {
            try
            {
                object model = null;
                ViewData["Status"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetExternalDocumentStatus());
                if (String.IsNullOrEmpty(id))
                    model = new DocumentBO() { Id = new Guid() };
                else
                    model = DocumentService.GetById(new Guid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult DistributeForm(Guid defaultCategory, string id, Guid departmentId)
        {
            try
            {
                var model = DocumentService.GetFormItem(defaultCategory, id, departmentId);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.AccessDenyException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }

        public ActionResult GetDepartmentCanUpdate()
        {
            try
            {
                var data = DepartmentService.GetDepartmentCanUpdateDocument().ToList();
                return this.Store(data);
            }
            catch (Exception)
            {

            }
            return this.Direct();
        }

        #endregion

        #region GetData danh sách Document có phân trang
        /// <summary>
        /// Get list Document
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult GetDataInternal(StoreRequestParameters param, string categoryId, string filterName, iDAS.Service.Common.Resource.DocumentStatus status)
        {
            try
            {
                if (!string.IsNullOrEmpty(categoryId))
                {
                    var pageIndex = param.Page;
                    var pageSize = param.Limit;
                    var count = 0;
                    var data = DocumentService.GetByCategory(new Guid(categoryId), pageIndex, pageSize, out count, filterName, status).ToList();
                    return this.Store(data.OrderBy(x => x.Code), count);
                }
                return this.Direct();
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Get list Document
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult GetDataExternal(StoreRequestParameters param, string categoryId, string filterName, iDAS.Service.Common.Resource.DocumentStatus status)
        {
            try
            {
                if (!string.IsNullOrEmpty(categoryId))
                {
                    var pageIndex = param.Page;
                    var pageSize = param.Limit;
                    var count = 0;
                    var data = DocumentService.GetByCategory(new Guid(categoryId), pageIndex, pageSize, out count, filterName, status).ToList();
                    return this.Store(data, count);
                }
                return this.Direct();
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult GetDocuments(StoreRequestParameters param, string categoryId, string filterName = default(string), iDAS.Service.Common.Resource.EDocumentStatus status = 0)
        {
            try
            {
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                var data = DocumentService.GetAllDocument(pageIndex, pageSize, out count, filterName, status);
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        public ActionResult LoadDocumentCurrentUser(StoreRequestParameters param, string filterName = default(string), iDAS.Service.Common.Resource.EDocumentStatus status = 0)
        {
            try
            {
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                var data = DocumentService.GetDocumentForCurrentUser(pageIndex, pageSize, out count, filterName, status).ToList();
                return this.Store(data.OrderBy(x => x.Code), count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Get list Document
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult GetDataDistribute(StoreRequestParameters param, string categoryId, string filterName, iDAS.Service.Common.Resource.DocumentStatus status)
        {
            try
            {
                if (!string.IsNullOrEmpty(categoryId))
                {
                    var pageIndex = param.Page;
                    var pageSize = param.Limit;
                    var count = 0;
                    var data = DocumentService.GetDistributeByCategory(new Guid(categoryId), pageIndex, pageSize, out count, filterName, status).ToList();
                    return this.Store(data, count);
                }
            }
            catch (Exception)
            {

            }
            return this.Direct();
        }

        /// <summary>
        /// Danh sách tài liệu ban hành
        /// </summary>
        /// <param name="param"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public ActionResult GetDataDocumentPublish(StoreRequestParameters param, string search = "")
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentService.GetdataPublish(pageIndex, pageSize, out count, search).ToList();
            return this.Store(data, count);
        }
        #endregion

        #region Create
        /// <summary>
        /// Create Document
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult Create(DocumentBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentService.CreatePublishDocument(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                    {
                        Common.WriteLog.WriteLogEntry("Create- Document", ex);
                        this.ShowNotify(Common.Resource.SystemFailure);
                    }

                }
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Thêm mới tài liệu bên ngoài trong danh sách ban hành
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult CreateDocument(DocumentBO item, string referenceJson)
        {
            var result = false;
            try
            {
                DocumentService.InsertDocument(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Update
        /// <summary>
        /// Update Document
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="dataPropertySettings"></param>
        /// <param name="dataResourceRelates"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(DocumentBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentService.UpdateDocument(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }


        public ActionResult UpdateDocumentCategory(Guid Id, Guid CategoryId)
        {
            var result = false;

            try
            {
                DocumentService.UpdateDocumentCategory(Id, CategoryId);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }
        /// <summary>
        /// Cập nhật tài liệu bên ngoài trong danh sách ban hành
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateDocument(DocumentBO item)
        {
            var result = false;
            try
            {
                DocumentService.UpdateDocument(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }

            return this.Direct(result: result);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Delete Document
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid? id)
        {
            var result = false;
            if (id == null || id == Guid.Empty)
                return this.Direct();
            if (ModelState.IsValid)
            {
                try
                {
                    Sql_ADO.idasAdoService.ExecuteNoquery("sp_Document_Delete", parameter: new { Id = id.Value });
                    //DocumentService.Delete(id.Value);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
        /// <summary>
        /// Gọi form chọn người phê duyệt sau khi nhấn nút gửi soạn thảo tài liệu
        /// </summary>
        /// <returns></returns>
        public ActionResult FormChooseReviewer(string id = "")
        {
            object model = null;
            if (String.IsNullOrEmpty(id) || new Guid(id.ToString()) == Guid.Empty)
            {
                model = new DocumentReviewSuggestBO();
            }
            else
            {
                // model = DocumentReviewSuggestService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        #region Đề nghị ban hành
        /// <summary>
        /// Form đề nghị ban hành.
        /// Menu context đề nghị ban hành
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public ActionResult PublishSuggestForm(Guid documentId)
        {
            try
            {
                var documentPublishSuggest = DocumentPublishSuggestService.GetSuggestForm(string.Empty, documentId);
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = documentPublishSuggest };
            }
            catch (Exception ex)
            {
            }
            return this.Direct();
        }

        public ActionResult SendPublishSuggestForm(DocumentPublishSuggestBO item)
        {
            var result = false;
            try
            {
                DocumentPublishSuggestService.SendPublishSuggest(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}