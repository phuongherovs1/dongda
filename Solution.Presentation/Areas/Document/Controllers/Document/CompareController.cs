﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;
using System.Diagnostics;

namespace iDAS.Presentation.Areas.Document.Controllers.Document
{
    public class CompareController : FrontController
    {
        //
        // GET: /Document/Compare/
        public ActionResult Index(Guid documentId)
        {
            var data = DocumentService.GetDetail(documentId);
            var files = FileService.GetByIds(data.FileAttachs.Files).ToList();
            ViewData["SourceFiles"] = files;
            return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
        }

        public ActionResult GetData(string documentCode)
        {
            var data = DocumentService.GetByCode(documentCode).ToList();
            return this.Store(data);
        }

        public ActionResult GetFileByDocumentId(Guid documentId)
        {
            var data = DocumentService.GetFileByDocumentId(documentId).ToList();
            return this.Store(data);
        }

        public ActionResult DoCompare(Guid src, Guid dest)
        {
            try
            {
                var result = DocumentService.Compare(src, dest);
                ViewData["Result"] = result;
                return Json(result);
            }
            catch (Exception err)
            {
                WriteLog.WriteLogEntry("DoCompare ", err);
            }
            return this.Direct();

        }
    }
}