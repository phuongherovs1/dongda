﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class SuggestAdjustController : FrontController
    {
        public ActionResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                WrapByScriptTag = false
            };
        }
        public ActionResult LoadData(StoreRequestParameters param,
          Resource.DocumentProcessRoleFilterSuggest roleType = Resource.DocumentProcessRoleFilterSuggest.All,
            Resource.DocumentSuggestStatusType status = iDAS.Service.Common.Resource.DocumentSuggestStatusType.All)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentAdjustSuggestService.GetByCurrentUser(pageIndex, pageSize, out count, roleType, status).ToList();
            return this.Store(data, count);
        }

        #region Thêm, sửa, xóa
        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                return new Ext.Net.MVC.PartialViewResult { };
            }
            catch
            {
                return this.Direct();
            }
        }

        [HttpPost]
        public ActionResult Create(DocumentAdjustSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                DocumentAdjustSuggestService.InsertAdjustSuggest(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult ViewFromDocument(string documentId = default(string))
        {
            try
            {
                var viewName = string.Empty;
                /// quyền truy cập tài liệu
               
                var model = DocumentAdjustSuggestService.GetDetailForDocument(iDAS.Service.Common.Utilities.ConvertToGuid(documentId));
                if (model.Id == Guid.Empty)
                {
                    viewName = "CreateFromDocument";
                }
                else
                {
                    switch (model.RoleType)
                    {
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit:
                            {
                                viewName = model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.New ? "Update" : "ReviewDetail";
                            }
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit:
                            {
                                viewName = model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.Review ? "Review" : "ReviewDetail";
                            }
                            break;
                        case (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit:
                            {
                                viewName = model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.Approve
                                    || model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.ApproveAccept
                                    || model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.ApproveFail ? "Approve" : "ReviewDetail";
                            }
                            break;
                        default:
                            break;
                    }
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = viewName };
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }
        [HttpGet]
        public ActionResult Update(string id = default(string))
        {
            var model = DocumentAdjustSuggestService.GetDetailForSugest(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            if (model.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit)
            {
                if (model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.New)
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "Update" };
                }
                else
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "ReviewDetail" };
                }
            }
            if (model.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit)
            {
                if (model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.Review)
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "Review" };
                }
                else
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "ReviewDetail" };
                }
            }
            if (model.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit)
            {
                if (model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.Approve
                     || model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.ApproveAccept
                    || model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.ApproveFail)
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "Approve" };
                }
                else
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "ReviewDetail" };
                }
            }
            return this.Direct();
        }
        [HttpPost]
        public ActionResult Update(DocumentAdjustSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                DocumentAdjustSuggestService.UpdateAdjustSuggest(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentAdjustSuggestService.DeleteByReposibility(id);
                    //X.GetCmp<Store>("storeListAdjustSuggest").Reload();
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Gửi, Thu hồi, Hủy, luân chuyển

        [HttpPost]
        public ActionResult SendAndSave(DocumentAdjustSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                DocumentAdjustSuggestService.SendAndSave(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Send(string id = default(string))
        {
            var result = false;
            try
            {
                if (!string.IsNullOrWhiteSpace(id))
                {
                    var guiId = iDAS.Service.Common.Utilities.ConvertToGuid(id);
                    // Kiểm tra nếu là trạng thái mới, mới cho phép gửi
                    if (DocumentAdjustSuggestService.SendFromSuggest(guiId))
                    {
                        //X.GetCmp<Store>("storeListAdjustSuggest").Reload();
                        result = true;
                        this.ShowNotify(Common.Resource.SystemSuccess);
                    }
                    else
                    {
                        result = true;
                        this.ShowNotify(Common.Resource.SendComplete);
                    }
                }
                else
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult RevertDocument(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    if (DocumentAdjustSuggestService.RevertDocument(id))
                    {
                        //X.GetCmp<Store>("storeListAdjustSuggest").Reload();
                        result = true;
                        this.ShowNotify(Common.Resource.SystemSuccess);
                    }
                    else
                    {
                        result = true;
                        this.ShowNotify(Common.Resource.NotAllowRevert);
                    }
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        public ActionResult Destroy(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    //DocumentAdjustSuggestService.Destroy(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpGet]
        public ActionResult ChangePerform(string id = default(string))
        {
            try
            {
                var model = DocumentAdjustSuggestService.GetDetail(iDAS.Service.Common.Utilities.ConvertToGuid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch
            {
                return this.Direct();
            }
        }
        [HttpPost]
        public ActionResult ChangePerform(DocumentAdjustSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                if (DocumentAdjustSuggestService.ChangePerform(data))
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Xem kết quả xem xét
        [HttpGet]
        public ActionResult ReviewDetail(string id = default(string))
        {
            var model = DocumentAdjustSuggestService.GetDetail(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "ReviewDetail" };
        }
        public ActionResult LoadSuggestRole(string id = default(string))
        {
            var data = DocumentAdjustSuggestService.GetEmployeeRoleBySuggest(iDAS.Service.Common.Utilities.ConvertToGuid(id)).ToList();
            return this.Store(data);
        }
        #endregion

        #region Xem xét đề nghị
        [HttpGet]
        public ActionResult Review(string id = default(string))
        {
            try
            {
                var model = DocumentAdjustSuggestService.GetDetail(iDAS.Service.Common.Utilities.ConvertToGuid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch
            {
                return this.Direct();
            }
        }
        [HttpPost]
        public ActionResult Review(DocumentAdjustSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                DocumentAdjustSuggestService.Review(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpGet]
        public ActionResult SendReview(string id = default(string))
        {
            var model = DocumentAdjustSuggestService.GetDetail(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            if (model.IsApprove == true && model.IsAccept == true)
            {
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "SendReview" };
            }
            return this.Direct();
        }
        [HttpPost]
        public ActionResult SendReview(DocumentAdjustSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                if (DocumentAdjustSuggestService.SendFromReview(data))
                {
                    //X.GetCmp<Store>("storeListAdjustSuggest").Reload();
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SendComplete);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Phê duyệt đề nghị
        [HttpGet]
        public ActionResult Approve(string id = default(string))
        {
            try
            {
                var model = DocumentAdjustSuggestService.GetDetail(iDAS.Service.Common.Utilities.ConvertToGuid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch
            {
                return this.Direct();
            }
        }
        [HttpPost]
        public ActionResult Approve(DocumentAdjustSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                DocumentAdjustSuggestService.Approve(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpGet]
        public ActionResult SendAssign(string id = default(string))
        {
            var model = DocumentAdjustSuggestService.GetDetail(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            if (model.IsApprove == true && model.IsAccept == true)
            {
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "SendAssign" };
            }
            return this.Direct();
        }
        [HttpPost]
        public ActionResult SendAssign(DocumentAdjustSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                if (DocumentAdjustSuggestService.SendFromApprove(data))
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SendComplete);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}