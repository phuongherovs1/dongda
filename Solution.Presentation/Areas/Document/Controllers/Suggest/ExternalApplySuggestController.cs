﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class ExternalApplySuggestController : FrontController
    {
        /// <summary>
        /// Form đề nghị phê duyệt áp dụng cho tài liệu bên ngoài
        /// </summary>
        /// <returns></returns>
        /// <param name="publishId"></param>
        public ActionResult FormApplySuggestDocumentExternal(string documentId = "")
        {
            object model = null;
            try
            {
                ViewData["RoleStatus"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetExternalApplyStatus());
                model = DocumentExternalApplySuggestService.CreateDefault(documentId);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        /// <summary>
        /// Form phê duyệt
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult FormApprove(string id = "")
        {
            object model = null;
            try
            {
                ViewData["RoleStatus"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetExternalApplyStatus());
                if (!String.IsNullOrEmpty(id))
                {
                    model = DocumentExternalApplySuggestService.GetById(new Guid(id));
                }
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        /// <summary>
        /// Form gửi người tiếp theo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult FormSend(string id = "")
        {
            object model = null;
            try
            {
                ViewData["NextRoleStatus"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetExternalApplyStatus());
                if (!String.IsNullOrEmpty(id))
                    model = DocumentExternalApplySuggestService.GetById(new Guid(id));
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }

        /// <summary>
        /// Gửi duyệt đề nghị phê duyệt áp dụng đối với tài liệu bên ngoài
        /// </summary>
        /// <param name="applySuggest"></param>
        /// <returns></returns>
        public ActionResult SendApprove(DocumentExternalApplySuggestBO applySuggest)
        {
            object result = null;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentExternalApplySuggestService.Send(applySuggest);
                    result = new { success = true, data = applySuggest.Id, message = Common.Resource.SystemSuccess };
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    result = new { success = false, message = Common.Resource.SystemFailure };
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result);
        }

        /// <summary>
        /// Thu hồi đề nghị
        /// </summary>
        /// <param name="applySuggest"></param>
        /// <returns></returns>
        public ActionResult Obsolate(DocumentExternalApplySuggestBO applySuggest)
        {
            object result = null;
            if (ModelState.IsValid)
            {
                try
                {
                    applySuggest.ApproveBy = applySuggest.EmployeeApprove.Id;
                    applySuggest.IsSend = false;
                    DocumentExternalApplySuggestService.Update(applySuggest);
                    result = new { success = true, data = applySuggest.Id, message = Common.Resource.SystemSuccess };
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    result = new { success = false, message = Common.Resource.SystemFailure };
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result);
        }

        /// <summary>
        /// Phê duyệt
        /// </summary>
        /// <param name="applySuggest"></param>
        /// <returns></returns>
        public ActionResult Approve(DocumentExternalApplySuggestBO applySuggest)
        {
            object result = null;
            if (ModelState.IsValid)
            {
                try
                {
                    //applySuggest.ApproveBy = applySuggest.EmployeeApprove.Id;
                    //applySuggest.IsApprove = true;
                    DocumentExternalApplySuggestService.Approve(applySuggest);
                    result = new { success = true, data = applySuggest.Id, message = Common.Resource.SystemSuccess };
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    result = new { success = false, message = Common.Resource.SystemFailure };
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result);
        }

        /// <summary>
        /// Gửi bước tiếp theo nếu là người kiểm tra
        /// </summary>
        /// <param name="applySuggest"></param>
        /// <returns></returns>
        public ActionResult NextSend(DocumentExternalApplySuggestBO applySuggest)
        {
            object result = null;
            try
            {
                DocumentExternalApplySuggestService.NextSend(applySuggest);
                result = new { success = true, data = applySuggest.Id, message = Common.Resource.SystemSuccess };
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }

        /// <summary>
        /// Danh sách nhân sự kiểm tra hoặc phê duyệt
        /// </summary>
        /// <param name="applySuggestId"></param>
        /// <returns></returns>
        public ActionResult LoadEmployee(string applySuggestId = default(string))
        {
            object model = null;
            try
            {
                if (!String.IsNullOrEmpty(applySuggestId))
                    model = DocumentExternalApplySuggestService.GetEmployeesReview(iDAS.Service.Common.Utilities.ConvertToGuid(applySuggestId)).ToList();
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Store(model);
        }
    }
}