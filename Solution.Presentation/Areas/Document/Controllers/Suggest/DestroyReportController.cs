﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class DestroyReportController : FrontController
    {
        /// <summary>
        /// Gọi form tạo biên bản hủy từ yêu cầu hủy
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public ActionResult DocumentReportDestroyFromRequest(string requestId = "")
        {
            try
            {
                object model = null;
                ViewData.Add("RequestDestroy", requestId);
                // Tạo biên bản hủy từ yêu cầu hủy
                if (String.IsNullOrEmpty(requestId))
                {
                    model = new DocumentDestroyReportBO()
                    {
                        Document = new DocumentBO(),
                        Id = Guid.Empty
                    };
                }
                else
                {
                    model = DocumentDestroyReportService.GetReportByRequestDestroyId(new Guid(requestId));
                    if (model == null)
                    {
                        model = new DocumentDestroyReportBO()
                        {
                            Document = new DocumentBO(),
                            Id = Guid.Empty
                        };
                    }
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }

        public ActionResult DocumentReportDestroyFromDistribute(string destroySuggestId = "")
        {
            try
            {
                object model = null;
                ViewData.Add("DestroySuggest", destroySuggestId);
                // Tạo biên bản hủy từ đề nghị hủy
                if (String.IsNullOrEmpty(destroySuggestId))
                {
                    model = new DocumentDestroyReportBO()
                    {
                        Document = new DocumentBO(),
                        Id = Guid.Empty
                    };
                }
                else
                {
                    model = DocumentDestroyReportService.GetReportByDestroySuggestId(new Guid(destroySuggestId));
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Danh sách tài liệu của đề nghị hủy
        /// </summary>
        /// <param name="param"></param>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public ActionResult GetDocumentDestroySuggest(StoreRequestParameters param, string destroySuggestId = "")
        {
            try
            {
                object data = null;
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                if (!String.IsNullOrEmpty(destroySuggestId))
                    data = DocumentDestroySuggestDetailService.GetByDestroySuggestId(new Guid(destroySuggestId));
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Danh sách tài liệu yêu cầu hủy
        /// </summary>
        /// <param name="param"></param>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public ActionResult GetDocumentRequest(string requestId = "")
        {
            try
            {
                object data = null;
                if (!String.IsNullOrEmpty(requestId))
                    data = DocumentDestroyReportService.GetDocumentByRequestId(new Guid(requestId));
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Tạo mới/Sửa biên bản hủy
        /// </summary>
        /// <param name="report"></param>
        /// <param name="jsonDocument"></param>
        /// <param name="jsonDetail"></param>
        /// <param name="destroySuggestId"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public ActionResult Create(DocumentDestroyReportBO report, string jsonDocument = "", string jsonDetail = "", string destroySuggestId = "", string requestId = "")
        {
            object result = null;
            //if (ModelState.IsValid)
            //{
            try
            {
                if (!String.IsNullOrEmpty(destroySuggestId))
                    report.DestroySuggestId = new Guid(destroySuggestId);
                if (!String.IsNullOrEmpty(requestId))
                    report.RequestId = new Guid(requestId);
                if (report.DestroySuggestId != null && report.DestroySuggestId != Guid.Empty)
                    DocumentDestroyReportService.Create(report, jsonDocument, jsonDetail);
                if (report.RequestId != null && report.RequestId != Guid.Empty)
                    DocumentDestroyReportService.CreateFromRequest(report, jsonDetail);
                result = new { success = true, data = report.Id, message = Common.Resource.SystemSuccess };
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            //}
            return this.Direct(result);
        }

        /// <summary>
        /// Tạo mới biên bản hủy từ yêu cầu hủy
        /// </summary>
        /// <param name="report"></param>
        /// <param name="jsonDetail"></param>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public ActionResult CreateFormRequest(DocumentDestroyReportBO report, string jsonDetail = "", string requestId = "")
        {
            object result = null;
            //if (ModelState.IsValid)
            //{
            try
            {
                if (!String.IsNullOrEmpty(requestId))
                    report.RequestId = new Guid(requestId);
                DocumentDestroyReportService.CreateFromRequest(report, jsonDetail);
                result = new { success = true, data = report.Id, message = Common.Resource.SystemSuccess };
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            //}
            return this.Direct(result);
        }

        /// <summary>
        /// Tạo biên bản hủy từ đề nghị hủy tài liệu phân phối
        /// </summary>
        /// <param name="report"></param>
        /// <param name="jsonDetail"></param>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public ActionResult CreateReportDistribute(DocumentDestroyReportBO report, string jsonDetail = "", string destroySuggestId = "")
        {
            object result = null;
            try
            {
                if (!String.IsNullOrEmpty(destroySuggestId))
                    report.DestroySuggestId = new Guid(destroySuggestId);
                DocumentDestroyReportService.CreateReportDistribute(report, jsonDetail);
                result = new { success = true, data = report.Id, message = Common.Resource.SystemSuccess };
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
    }
}