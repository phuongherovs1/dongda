﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using Ext.Net.MVC;
using System.Web.Mvc;
using iDAS.Service;
using iDAS.Service.Common;
using System.Diagnostics;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class SuggestWriteNewController : FrontController
    {
        /// <summary>
        /// View danh sách đề nghị viết mới
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                WrapByScriptTag = false
            };
        }
        public ActionResult LoadData(StoreRequestParameters param,
          Resource.DocumentProcessRoleFilterSuggest roleType = Resource.DocumentProcessRoleFilterSuggest.All,
            Resource.DocumentSuggestStatusType status = iDAS.Service.Common.Resource.DocumentSuggestStatusType.All)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentWriteSuggestService.GetByCurrentUser(pageIndex, pageSize, out count, roleType, status).ToList();
            return this.Store(data, count);
        }
        // Lấy danh sách phòng ban của người đăng nhập mà cho phép
        public ActionResult GetDepartmentAllowSuggests()
        {
            var data = DocumentWriteSuggestService.GetDepartmentAllowSuggests();
            return this.Store(data);
        }

        #region Thêm, sửa, xóa
        [HttpGet]
        public ActionResult CreateAndPerform()
        {
            try
            {
                return new Ext.Net.MVC.PartialViewResult { };
            }
            catch
            {
                return this.Direct();
            }
        }
        [HttpGet]
        public ActionResult Create(string id = default(string))
        {
            try
            {
                return new Ext.Net.MVC.PartialViewResult { };
                //if (DocumentWriteSuggestService.AllowSuggestByCurrentUser())
                //{
                //    return new Ext.Net.MVC.PartialViewResult { };
                //}
                //else
                //{
                //    this.ShowAlert(Common.Resource.SettingProcessSuggestWriteNew);
                //    return this.Direct();
                //}
            }
            catch
            {
                return this.Direct();
            }
        }
        [HttpPost]
        public ActionResult Create(DocumentWriteSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                DocumentWriteSuggestService.InsertAndSaveRole(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult ViewFromDocument(string documentId = default(string))
        {
            var model = DocumentWriteSuggestService.GetDetailForDocument(iDAS.Service.Common.Utilities.ConvertToGuid(documentId));
            if (model.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit)
            {
                if (model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.New)
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "Update" };
                }
                else
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "ReviewDetail" };
                }
            }
            if (model.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit)
            {
                if (model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.Review)
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "Review" };
                }
                else
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "ReviewDetail" };
                }
            }
            if (model.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit)
            {
                if (model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.Approve
                      || model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.ApproveAccept
                     || model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.ApproveFail)
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "Approve" };
                }
                else
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "ReviewDetail" };
                }
            }
            return this.Direct();
        }
        [HttpGet]
        public ActionResult Update(string id = default(string))
        {
            var model = DocumentWriteSuggestService.GetDetailForSugest(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            if (model.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.SuggesterWriteAndEdit)
            {
                if (model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.New)
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "Update" };
                }
                else
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "ReviewDetail" };
                }
            }
            if (model.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.ReviewWriteAndEdit)
            {
                if (model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.Review)
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "Review" };
                }
                else
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "ReviewDetail" };
                }
            }
            if (model.RoleType == (int)iDAS.Service.Common.Resource.DocumentProcessRole.ApprovalWriteAndEdit)
            {
                if (model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.Approve
                     || model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.ApproveAccept
                    || model.StatusType == (int)iDAS.Service.Common.Resource.DocumentSuggestStatusType.ApproveFail)
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "Approve" };
                }
                else
                {
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "ReviewDetail" };
                }
            }
            return this.Direct();
        }
        [HttpPost]
        public ActionResult Update(DocumentWriteSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                DocumentWriteSuggestService.UpdateAndSaveRole(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentWriteSuggestService.DeleteByResponsibility(id);
                    //X.GetCmp<Store>("storeListSuggestWrite").Reload();
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Gửi, Thu hồi, Hủy, luân chuyển

        [HttpPost]
        public ActionResult SendAndSave(DocumentWriteSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                DocumentWriteSuggestService.SendAndSave(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Send(string id = default(string))
        {
            var result = false;
            try
            {
                if (!string.IsNullOrWhiteSpace(id))
                {
                    var guiId = iDAS.Service.Common.Utilities.ConvertToGuid(id);
                    // Kiểm tra nếu là trạng thái mới, mới cho phép gửi
                    if (DocumentWriteSuggestService.SendFromSuggest(guiId))
                    {
                        //X.GetCmp<Store>("storeListSuggestWrite").Reload();
                        result = true;
                        this.ShowNotify(Common.Resource.SystemSuccess);
                    }
                    else
                    {
                        result = true;
                        this.ShowNotify(Common.Resource.SendComplete);
                    }
                }
                else
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult RevertDocument(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    if (DocumentWriteSuggestService.RevertDocument(id))
                    {
                        //X.GetCmp<Store>("storeListSuggestWrite").Reload();
                        result = true;
                        this.ShowNotify(Common.Resource.SystemSuccess);
                    }
                    else
                    {
                        result = true;
                        this.ShowNotify(Common.Resource.NotAllowRevert);
                    }
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        public ActionResult Destroy(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    //DocumentWriteSuggestService.Destroy(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpGet]
        public ActionResult ChangePerform(string id = default(string))
        {
            try
            {
                var model = DocumentWriteSuggestService.GetDetail(iDAS.Service.Common.Utilities.ConvertToGuid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch
            {
                return this.Direct();
            }
        }
        [HttpPost]
        public ActionResult ChangePerform(DocumentWriteSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                if (DocumentWriteSuggestService.ChangePerform(data))
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Xem kết quả xem xét
        [HttpGet]
        public ActionResult ReviewDetail(string id = default(string))
        {
            var model = DocumentWriteSuggestService.GetDetail(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "ReviewDetail" };
        }
        public ActionResult LoadSuggestRole(string id = default(string))
        {
            var data = DocumentWriteSuggestService.GetEmployeeRoleBySuggest(iDAS.Service.Common.Utilities.ConvertToGuid(id)).ToList();
            return this.Store(data);
        }
        #endregion

        #region Xem xét đề nghị
        [HttpGet]
        public ActionResult Review(string id = default(string))
        {
            try
            {
                var model = DocumentWriteSuggestService.GetDetail(iDAS.Service.Common.Utilities.ConvertToGuid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch
            {
                return this.Direct();
            }
        }
        [HttpPost]
        public ActionResult Review(DocumentWriteSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                DocumentWriteSuggestService.Review(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpGet]
        public ActionResult SendReview(string id = default(string))
        {
            var model = DocumentWriteSuggestService.GetDetail(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            if (model.IsApprove == true && model.IsAccept == true)
            {
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "SendReview" };
            }
            return this.Direct();
        }
        [HttpPost]
        public ActionResult SendReview(DocumentWriteSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                if (DocumentWriteSuggestService.SendFromReview(data))
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SendComplete);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Phê duyệt đề nghị
        [HttpGet]
        public ActionResult Approve(string id = default(string))
        {
            try
            {
                var model = DocumentWriteSuggestService.GetDetail(iDAS.Service.Common.Utilities.ConvertToGuid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch
            {
                return this.Direct();
            }
        }
        [HttpPost]
        public ActionResult Approve(DocumentWriteSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                DocumentWriteSuggestService.Approve(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpGet]
        public ActionResult SendAssign(string id = default(string))
        {
            var model = DocumentWriteSuggestService.GetDetail(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            if (model.IsApprove == true && model.IsAccept == true)
            {
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "SendAssign" };
            }
            return this.Direct();
        }
        [HttpPost]
        public ActionResult SendAssign(DocumentWriteSuggestResponsibilityBO data)
        {
            var result = false;
            try
            {
                if (DocumentWriteSuggestService.SendFromApprove(data))
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SendComplete);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}