﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class SuggestController : FrontController
    {
        /// <summary>
        /// Danh sách đề nghị
        /// </summary>
        /// <returns></returns>
        public ActionResult SuggestList()
        {
            return PartialView();
        }

        /// <summary>
        /// Danh sách yêu cầu
        /// </summary>
        /// <returns></returns>
        public ActionResult RequestList()
        {
            return PartialView();
        }

        public ActionResult AdjustWriteSuggest(string id = default(string))
        {
            try
            {
                return new Ext.Net.MVC.PartialViewResult { };
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult ApproveList(string id = default(string))
        {
            try
            {
                return new Ext.Net.MVC.PartialViewResult { };
            }
            catch
            {
                return this.Direct();
            }
        }
        /// <summary>
        /// View danh sách đề nghị sửa đổi
        /// </summary>
        /// <returns></returns>
        public ActionResult SuggestAdjustIndex(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,

                WrapByScriptTag = false
            };
        }

        #region CuongPC
        
        //public ActionResult SendReleaseDocument(string id = default(string))
        //{
        //    try
        //    {
        //        var suggest = DocumentSuggestResponsibilityService.GetDetail(new Guid(id));
        //        var data = new DocumentRequestBO() { DocumentId = suggest.DocumentSuggest.Document.Id, DocumentSuggestId = suggest.DocumentSuggestId, DepartmentId = suggest.DocumentSuggest.DepartmentId, Order= suggest.DocumentSuggest.Order.HasValue?suggest.DocumentSuggest.Order.Value:0 };
        //        return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
        //    }
        //    catch
        //    {
        //        return this.Direct();
        //    }
        //}
        //public ActionResult RecoverSuggest(string id = default(string), string name = default(string))
        //{
        //    try
        //    {
        //        ViewData["Tilte"] = name;
        //        var data = DocumentSuggestResponsibilityService.GetDetail(new Guid(id));
        //        return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
        //    }
        //    catch
        //    {
        //        return this.Direct();
        //    }
        //}
        //public ActionResult ApproveSuggest(string id = default(string), string name = default(string))
        //{
        //    try
        //    {
        //        ViewData["Tilte"] = name;
        //        var data = DocumentSuggestResponsibilityService.GetDetail(new Guid(id));
        //        return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
        //    }
        //    catch
        //    {
        //        return this.Direct();
        //    }
        //}
        public ActionResult ApproveSuggestAction(Guid documentRequestId)
        {
            try
            {
                var data = new DocumentResponsibilityBO() { DocumentId = documentRequestId };
                return new Ext.Net.MVC.PartialViewResult { Model = data };
            }
            catch
            {
                return this.Direct();
            }
        }
       

       
        public ActionResult ReviewSuggestAction(Guid documentSuggestId)
        {
            try
            {
                var data = new DocumentSuggestCheckBO() { DocumentSuggestId = documentSuggestId };
                return new Ext.Net.MVC.PartialViewResult { Model = data };
            }
            catch
            {
                return this.Direct();
            }
        }
        //public ActionResult SendApprovalAction(Guid documentRequestId)
        //{
        //    try
        //    {
        //        var request = DocumentRequestService.GetById(documentRequestId);
        //        var data = new DocumentSuggestCheckBO() { DocumentRequestId = documentRequestId };
        //        data.Suggest = new DocumentSuggestBO() { DepartmentId = request.DepartmentId };
        //        return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
        //    }
        //    catch
        //    {
        //        return this.Direct();
        //    }
        //}
        //public ActionResult ReviewSuggest(string id = default(string), string name = default(string))
        //{
        //    try
        //    {
        //        ViewData["Tilte"] = name;
        //        var data = DocumentSuggestResponsibilityService.GetDetail(new Guid(id));
        //        return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
        //    }
        //    catch
        //    {
        //        return this.Direct();
        //    }
        //}
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(DocumentSuggestCheckBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                   // DocumentSuggestCheckService.InsertCheckSuggest(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        //[HttpPost]
        //[ValidateInput(false)]
        //public ActionResult Send(DocumentSuggestResponsibilityBO item)
        //{
        //    var result = false;
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            DocumentSuggestResponsibilityService.Send(item);
        //            result = true;
        //            this.ShowNotify(Common.Resource.SystemSuccess);
        //        }
        //        catch (Exception ex)
        //        {
        //            if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
        //                this.ShowNotify(ex.Message);
        //            else
        //                this.ShowNotify(Common.Resource.SystemFailure);
        //        }
        //    }
        //    return this.Direct(result: result);
        //}
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(DocumentSuggestCheckBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                   // DocumentSuggestCheckService.Update(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        //public ActionResult Cancel(string id = default(string))
        //{
        //    var result = false;
        //    try
        //    {
        //        DocumentSuggestService.Cancel(new Guid(id));
        //        result = true;
        //        this.ShowNotify(Common.Resource.SystemSuccess);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
        //            this.ShowNotify(ex.Message);
        //        else
        //            this.ShowNotify(Common.Resource.SystemFailure);
        //    }
        //    return this.Direct(result: result);
        //}
        //public ActionResult Recover(DocumentSuggestResponsibilityBO item)
        //{
        //    var result = false;
        //    try
        //    {
        //        DocumentSuggestResponsibilityService.RevertDocument(item);
        //        result = true;
        //        this.ShowNotify(Common.Resource.SystemSuccess);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
        //            this.ShowNotify(ex.Message);
        //        else
        //            this.ShowNotify(Common.Resource.SystemFailure);
        //    }
        //    return this.Direct(result: result);
        //}
        //[HttpPost]
        //[ValidateInput(false)]
        //public ActionResult Approval(DocumentSuggestResponsibilityBO item)
        //{
        //    var result = false;
        //    try
        //    {
        //        DocumentSuggestResponsibilityService.UpdateApproval(item);
        //        result = true;
        //        this.ShowNotify(Common.Resource.SystemSuccess);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
        //            this.ShowNotify(ex.Message);
        //        else
        //            this.ShowNotify(Common.Resource.SystemFailure);
        //    }
        //    return this.Direct(result: result);
        //}
        //[HttpPost]
        //[ValidateInput(false)]
        //public ActionResult SaveChangeApproval(DocumentSuggestResponsibilityBO item)
        //{
        //    var result = false;
        //    try
        //    {
        //        DocumentSuggestResponsibilityService.ChangeEmployeeApproval(item);
        //        result = true;
        //        this.ShowNotify(Common.Resource.SystemSuccess);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
        //            this.ShowNotify(ex.Message);
        //        else
        //            this.ShowNotify(Common.Resource.SystemFailure);
        //    }
        //    return this.Direct(result: result);
        //}

        //public ActionResult RevertApproval(string id = default(string))
        //{
        //    var result = false;
        //    try
        //    {
        //        DocumentSuggestResponsibilityService.RevertApproval(new Guid(id));
        //        result = true;
        //        this.ShowNotify(Common.Resource.SystemSuccess);
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
        //            this.ShowNotify(ex.Message);
        //        else
        //            this.ShowNotify(Common.Resource.SystemFailure);
        //    }
        //    return this.Direct(result: result);
        //}

        #endregion

    }
}