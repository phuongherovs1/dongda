﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class DestroyReportDetailController : FrontController
    {
        /// <summary>
        /// Danh sách người tham gia
        /// </summary>
        /// <param name="param"></param>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public ActionResult GetDetail(StoreRequestParameters param, string reportId = "")
        {
            try
            {
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                object data = null;
                if (!String.IsNullOrEmpty(reportId) || new Guid(reportId) != Guid.Empty)
                    data = DocumentDestroyReportDetailService.GetByReportId(pageIndex, pageSize, out count, new Guid(reportId));
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Xóa thông tin đề nghị
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentDestroyReportDetailService.Delete(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}