﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class DestroySuggestController : FrontController
    {
        public ActionResult Index(string departmentId)
        {
            ViewData["StatusApprove"] = iDAS.Service.Common.Resource.GetStatusApproveSuggest();
            var show = DocumentDestroySuggestService.ShowOrHideButtonCreate(departmentId);
            ViewData["Show"] = show;
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }

        #region Danh sách đề nghị hủy GetData
        /// <summary>
        /// Danh sách đề nghị hủy
        /// </summary>
        /// <param name="param"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public ActionResult GetData(StoreRequestParameters param, iDAS.Service.Common.Resource.DocumentSuggestStatusType status = iDAS.Service.Common.Resource.DocumentSuggestStatusType.All,
            iDAS.Service.Common.Resource.DocumentProcessRoleFilterSuggest roleType = iDAS.Service.Common.Resource.DocumentProcessRoleFilterSuggest.All)
        {
            try
            {
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                var data = DocumentDestroySuggestService.GetAll(pageIndex, pageSize, out count, status, roleType);
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        #endregion

        #region Danh sách tài liệu chi tiết
        /// <summary>
        /// Danh sách tài liệu chi tiết của đề nghị hủy
        /// </summary>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public ActionResult SuggestDetail(string destroySuggestId = "")
        {
            object data = null;
            if (!String.IsNullOrEmpty(destroySuggestId))
                data = DocumentDestroySuggestDetailService.GetByDestroySuggestId(new Guid(destroySuggestId));
            return this.ComponentConfig("SuggestDetail", data);
        }
        public ActionResult LoadSuggestDetail(string destroySuggestId = "")
        {
            object data = null;
            if (!String.IsNullOrEmpty(destroySuggestId))
                data = DocumentDestroySuggestDetailService.GetByDestroySuggestId(new Guid(destroySuggestId));
            return this.Store(data);
        }
        #endregion

        #region Form tạo mới đề nghị hủy Form
        /// <summary>
        /// Gọi form tạo mới đề nghị hủy
        /// </summary>
        /// <param name="id"></param>
        /// <param name="documentIds"></param>
        /// <param name="isCreate"></param>
        /// <returns></returns>
        public ActionResult Form(string id = "", string documentIds = "", bool isCreate = true)
        {
            try
            {
                object model = null;
                var viewName = string.Empty;
                ViewData.Add("DocumentIds", documentIds);
                ViewData.Add("IsCreate", isCreate);
                ViewData.Add("ResponsibilityId", id);
                ViewData["RoleType"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetRoleTypeDestroySuggest());
                if (String.IsNullOrEmpty(id) && String.IsNullOrEmpty(documentIds))
                {
                    model = new DocumentDestroySuggestBO()
                    {
                        Document = new DocumentBO(),
                        Id = Guid.Empty
                    };
                }
                else
                {
                    var data = DocumentDestroySuggestService.GetByResponsibilityId(id);
                    model = data;
                    if (data.IsArchiver != true)
                        viewName = "DetailFormCreateFromDocument";
                    else if (data.CountDocument > 1)
                        viewName = "Form";
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData, ViewName = viewName };
            }
            catch
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Form tạo mới đề nghị hủy từ danh sách tài liệu phân phối
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public ActionResult FormCreateFromDocument(string documentId = "", string departmentId = "")
        {
            try
            {
                object model = null;
                if (string.IsNullOrEmpty(documentId) || string.IsNullOrEmpty(departmentId))
                {
                    throw new Exception();
                }
                else
                {
                    var checkAccess = DocumentPermissionService.CheckPermission(new Guid(documentId));
                    if (!checkAccess)
                        throw new AccessDenyException();
                    model = DocumentDestroySuggestService.CreateDefault(documentId, departmentId);
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct();
        }
        #endregion


        /// <summary>
        /// Form tạo mới đề nghị hủy từ danh sách tài liệu phân phối
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public ActionResult DetailFormCreateFromDocument(string id = "")
        {
            var model = DocumentDestroySuggestResponsibitityService.GetDataDestroySuggestByResponsibilityId(new Guid(id));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }


        #region GỌi form kiểm tra FormReview
        /// <summary>
        /// Gọi form kiểm tra
        /// </summary>
        /// <param name="responsibilityId"></param>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public ActionResult FormReview(string id = "", string DocumentDestroyId = "")
        {
            try
            {
                object model = null;
                ViewData["DestroySuggestId"] = DocumentDestroyId;
                if (String.IsNullOrEmpty(id))
                {
                    model = new DocumentDestroySuggestResponsibitityBO()
                    {
                        DestroySuggest = new DocumentDestroySuggestBO(),
                        Id = Guid.Empty
                    };
                }
                else
                {
                    model = DocumentDestroySuggestResponsibitityService.GetDestroySuggestByResponsibilityId(new Guid(id));
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }
        #endregion

        #region Form phê duyệt
        /// <summary>
        /// Gọi form kiểm tra
        /// </summary>
        /// <param name="responsibilityId"></param>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public ActionResult FormApprove(string id = "", string DocumentDestroyId = "")
        {
            try
            {
                object model = null;
                ViewData["DestroySuggestId"] = DocumentDestroyId;
                if (String.IsNullOrEmpty(id))
                {
                    model = new DocumentDestroySuggestResponsibitityBO()
                    {
                        DestroySuggest = new DocumentDestroySuggestBO(),
                        Id = Guid.Empty
                    };
                }
                else
                {
                    model = DocumentDestroySuggestResponsibitityService.GetDestroySuggestByResponsibilityId(new Guid(id));
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Gọi form kiểm tra
        /// </summary>
        /// <param name="responsibilityId"></param>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public ActionResult FormApprovedestroySuggest(string id = "", string DocumentDestroyId = "")
        {
            try
            {
                object model = null;
                ViewData["DestroySuggestId"] = DocumentDestroyId;
                if (String.IsNullOrEmpty(id))
                {
                    model = new DocumentDestroySuggestResponsibitityBO()
                    {
                        DestroySuggest = new DocumentDestroySuggestBO(),
                        Id = Guid.Empty
                    };
                }
                else
                {
                    //model = DocumentDestroySuggestResponsibitityService.GetDataDestroySuggestByResponsibilityId(new Guid(id), new Guid(DocumentDestroyId));
                    var data = DocumentDestroySuggestResponsibitityService.GetDestroySuggestByResponsibilityId(new Guid(id));
                    model = data;
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData, ViewName = "FormApprovedestroySuggest" };
            }
            catch
            {
                return this.Direct();
            }
        }


        /// <summary>
        /// Gọi form kiểm tra
        /// </summary>
        /// <param name="responsibilityId"></param>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public ActionResult FormPublishDestroySuggest(string id = "", string DocumentDestroyId = "")
        {
            try
            {
                object model = null;
                ViewData["DestroySuggestId"] = DocumentDestroyId;
                if (String.IsNullOrEmpty(id))
                {
                    model = new DocumentDestroySuggestResponsibitityBO()
                    {
                        DestroySuggest = new DocumentDestroySuggestBO(),
                        Id = Guid.Empty
                    };
                }
                else
                {
                    //model = DocumentDestroySuggestResponsibitityService.GetDataDestroySuggestByResponsibilityId(new Guid(id), new Guid(DocumentDestroyId));
                    model = DocumentDestroySuggestResponsibitityService.GetDestroySuggestByResponsibilityId(new Guid(id));
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData, ViewName = "FormPublishDestroySuggest" };
            }
            catch
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Phê duyệt
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult Approve(DocumentDestroySuggestResponsibitityBO data)
        {
            var result = false;
            try
            {
                DocumentDestroySuggestResponsibitityService.Approve(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Phê duyệt đề nghị xong gửi sang người tạo đề nghị lập biên bản
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult SendApproveDestroySuggest(DocumentDestroySuggestResponsibitityBO data)
        {
            var result = false;
            try
            {
                DocumentDestroySuggestResponsibitityService.SendApproveDestroySuggest(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Load danh sách nhân sự liên quan
        /// </summary>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public ActionResult LoadEmployeeRole(string destroySuggestId = default(string))
        {
            object model = null;
            try
            {
                if (!String.IsNullOrEmpty(destroySuggestId))
                    model = DocumentDestroySuggestResponsibitityService.GetEmployeeRoleByDestroySuggest(iDAS.Service.Common.Utilities.ConvertToGuid(destroySuggestId)).ToList();
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Store(model);
        }

        /// <summary>
        /// Load danh sách nhân sự liên quan theo quy trình ban hành
        /// </summary>
        /// <param name="destroySuggestId"></param>
        /// <returns></returns>
        public ActionResult GetEmployeeRoleByPublishProcess(string destroySuggestId = default(string))
        {
            object model = null;
            try
            {
                if (!String.IsNullOrEmpty(destroySuggestId))
                    model = DocumentDestroySuggestResponsibitityService.GetEmployeeRoleByPublishProcess(iDAS.Service.Common.Utilities.ConvertToGuid(destroySuggestId)).ToList();
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Store(model);
        }
        #endregion
        /// <summary>
        /// Hủy tài liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult FormPublishDestroySuggestDocument(string id = default(string))
        {
            var result = false;
            try
            {
                DocumentDestroySuggestResponsibitityService.FormPublishDestroySuggestDocument(iDAS.Service.Common.Utilities.ConvertToGuid(id));
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        #region Phê duyệt xem xét Review
        /// <summary>
        /// Phê duyệt xem xét
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult Review(DocumentDestroySuggestResponsibitityBO data)
        {
            var result = false;
            try
            {
                DocumentDestroySuggestResponsibitityService.Review(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Gửi xem xét bước tiếp theo SendReview
        /// <summary>
        /// Gọi form gửi xem xét bước tiếp theo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SendReview(string id = default(string))
        {
            ViewData["RoleType"] = Common.Utilities.GetSelectItem(iDAS.Service.Common.Resource.GetRoleTypeDestroySuggest());
            var model = DocumentDestroySuggestResponsibitityService.GetDestroySuggestByResponsibilityId(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            if (model.IsApprove == true && model.IsAccept == true)
            {
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "SendReview", ViewData = ViewData };
            }
            return this.Direct();
        }

        /// <summary>
        /// Lưu thông tin phê duyệt và gửi bước tiếp theo
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendReview(DocumentDestroySuggestResponsibitityBO data)
        {
            var result = false;
            try
            {
                if (DocumentDestroySuggestResponsibitityService.SendFromReview(data))
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SendComplete);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Gửi cho người kiểm tra hoặc phê duyệt (không theo quy trình)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult SendReviewDestroySuggest(DocumentDestroySuggestResponsibitityBO data)
        {
            var result = false;
            try
            {
                if (DocumentDestroySuggestResponsibitityService.SendReviewDestroySuggest(data))
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SendComplete);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Gửi phe duyet bước tiếp theo SendApprove
        /// <summary>
        /// Gọi form gửi xem xét bước tiếp theo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SendApproveDestroy(string id = default(string))
        {
            var model = DocumentDestroySuggestResponsibitityService.GetDestroySuggestByResponsibilityId(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            if (model.IsApprove == true && model.IsAccept == true)
            {
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "SendApproveDestroy" };
            }
            return this.Direct();
        }

        /// <summary>
        /// Lưu thông tin phê duyệt và gửi bước tiếp theo
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendApproveDestroy(DocumentDestroySuggestResponsibitityBO data)
        {
            var result = false;
            try
            {
                if (DocumentDestroySuggestResponsibitityService.SendFromReview(data))
                {
                    // X.GetCmp<Store>("stDocumentDestroySuggest").Reload();
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SendComplete);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion
        #region Gửi phe duyet bước tiếp theo SendApprove
        /// <summary>
        /// Gọi form gửi xem xét bước tiếp theo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SendPublishApproveDestroy(string id)
        {
            var model = DocumentDestroySuggestResponsibitityService.GetDestroySuggestByResponsibilityId(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "SendPublishApproveDestroy" };
        }

        /// <summary>
        /// Lưu thông tin phê duyệt và gửi bước tiếp theo
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendPublishApproveDestroy(DocumentDestroySuggestResponsibitityBO data)
        {
            var result = false;
            try
            {
                if (DocumentDestroySuggestResponsibitityService.SendFromReview(data))
                {
                    // X.GetCmp<Store>("stDocumentDestroySuggest").Reload();
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SendComplete);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Gửi phe duyet bước tiếp theo SendApprove
        /// <summary>
        /// Gọi form gửi xem xét bước tiếp theo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult SendPublishDestroy(string id = default(string))
        {
            var model = DocumentDestroySuggestResponsibitityService.GetDestroySuggestByResponsibilityId(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            if (model.IsApprove == true && model.IsAccept == true)
            {
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "SendPublishDestroy" };
            }
            return this.Direct();
        }

        /// <summary>
        /// Lưu thông tin phê duyệt và gửi bước tiếp theo
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendPublishDestroy(DocumentDestroySuggestResponsibitityBO data)
        {
            var result = false;
            try
            {
                if (DocumentDestroySuggestResponsibitityService.SendFromReview(data))
                {
                    X.GetCmp<Store>("stDocumentDestroySuggest").Reload();
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SendComplete);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion

        #region form gửi tạo biên bản hủy SendReport
        /// <summary>
        /// Gọi form gửi cho người tạo biên bản hủy
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult SendReport(string id = default(string))
        {
            var model = DocumentDestroySuggestResponsibitityService.GetDestroySuggestByResponsibilityId(iDAS.Service.Common.Utilities.ConvertToGuid(id));
            if (model.IsApprove == true && model.IsAccept == true)
            {
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "SendReport" };
            }
            return this.Direct();
        }

        /// <summary>
        /// Form biên bản hủy
        /// </summary>
        /// <returns></returns>
        public ActionResult DocumentReportDestroy(string destroySuggestId = "")
        {
            try
            {
                object model = null;
                ViewData.Add("DestroySuggest", destroySuggestId);
                // Tạo biên bản hủy từ đề nghị hủy
                if (String.IsNullOrEmpty(destroySuggestId))
                {
                    model = new DocumentDestroyReportBO()
                    {
                        Document = new DocumentBO(),
                        Id = Guid.Empty
                    };
                }
                else
                {
                    model = DocumentDestroyReportService.GetReportByDestroySuggestId(new Guid(destroySuggestId));
                    if (model == null)
                    {
                        model = new DocumentDestroyReportBO()
                        {
                            Document = new DocumentBO(),
                            Id = Guid.Empty
                        };
                    }
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }
        #endregion

        #region Danh sách tài liệu đề nghị hủy
        /// <summary>
        /// Danh sách tài liệu ban hành dùng cho form tạo mới
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult GetDocumentPublish(StoreRequestParameters param, bool isCreate = true, string destroySuggestId = "")
        {
            try
            {
                var pageIndex = param.Page;
                var pageSize = param.Limit;
                var count = 0;
                var data = DocumentPublishService.GetAllForDestroySuggest(pageIndex, pageSize, out count, isCreate, destroySuggestId);
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        #endregion

        #region Gửi duyệt đề nghị hủy
        /// <summary>
        /// Gửi duyệt đề nghị hủy tài liệu
        /// </summary>
        /// <param name="destroySuggest"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public ActionResult SendApprove(DocumentDestroySuggestBO destroySuggest, string jsonData = "")
        {
            object result = null;
            try
            {
                destroySuggest.Temp = true;
                DocumentDestroySuggestService.Send(destroySuggest, jsonData);
                result = new { success = true, data = destroySuggest.Id, message = Common.Resource.SystemSuccess };
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        #endregion


        #region Xem kết quả kiểm tra
        [HttpGet]
        public ActionResult ReviewDetailDestroy(string id = "", string DocumentDestroyId = "")
        {
            try
            {
                object model = null;
                ViewData["DestroySuggestId"] = DocumentDestroyId;
                if (String.IsNullOrEmpty(id))
                {
                    model = new DocumentDestroySuggestResponsibitityBO()
                    {
                        DestroySuggest = new DocumentDestroySuggestBO(),
                        Id = Guid.Empty
                    };
                }
                else
                {
                    //model = DocumentDestroySuggestResponsibitityService.GetDataDestroySuggestByResponsibilityId(new Guid(id), new Guid(DocumentDestroyId));
                    var data = DocumentDestroySuggestResponsibitityService.GetDestroySuggestByResponsibilityId(new Guid(id));
                    model = data;
                }
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData, ViewName = "ReviewDetailDestroy" };
            }
            catch
            {
                return this.Direct();
            }
        }
        #endregion

        #region Gửi duyệt đề nghị hủy từ tài liệu nội bộ
        /// <summary>
        /// Gửi duyệt đề nghị hủy tài liệu
        /// </summary>
        /// <param name="destroySuggest"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public ActionResult SendSuggestDestroy(DocumentDestroySuggestBO destroySuggest)
        {
            object result = null;
            try
            {
                destroySuggest.DepartmentId = destroySuggest.Document.DepartmentId;
                destroySuggest.Temp = true;
                DocumentDestroySuggestService.SendSuggestDestroy(destroySuggest);
                result = new { success = true, data = destroySuggest.Id, message = Common.Resource.SystemSuccess };
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        #endregion

        #region Lưu thông tin đề nghị
        /// <summary>
        /// Lưu thông tin đề nghị hủy
        /// </summary>
        /// <param name="destroySuggest"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public ActionResult Save(DocumentDestroySuggestBO destroySuggest, string jsonData = "")
        {
            object result = null;
            try
            {
                destroySuggest.Temp = false;
                DocumentDestroySuggestService.Create(destroySuggest, jsonData);
                result = new { success = true, data = destroySuggest.Id, message = Common.Resource.SystemSuccess };
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }

        /// <summary>
        /// Lưu thông tin đề nghị hủy
        /// </summary>
        /// <param name="destroySuggest"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public ActionResult SaveDestroySuggest(DocumentDestroySuggestBO destroySuggest, string jsonData = "")
        {
            object result = null;
            try
            {
                destroySuggest.Temp = false;
                DocumentDestroySuggestService.SaveDestroySuggest(destroySuggest, jsonData);
                result = new { success = true, data = destroySuggest.Id, message = Common.Resource.SystemSuccess };
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        #endregion

        #region Thu hồi đề nghị Obsolate
        /// <summary>
        /// Thu hồi đề nghị có cho phép chỉnh sửa
        /// Chức năng này thực hiện trên form chi tiết
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult Obsolate(DocumentDestroySuggestBO item, string responsibilityId)
        {
            object result = null;
            try
            {
                //item.ApproveBy = item.EmployeeApprove.Id;
                //item.RoleType = item.EmployeeApprove.RoleType;
                DocumentDestroySuggestService.Update(item);
                if (!String.IsNullOrEmpty(responsibilityId))
                    DocumentDestroySuggestResponsibitityService.Revert(new Guid(responsibilityId));
                result = new { success = true, data = item.Id, message = Common.Resource.SystemSuccess };
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        #endregion

        #region Thu hồi vai trò RevertResponsibility
        /// <summary>
        /// Thu hồi vai trò
        /// </summary>
        /// <param name="responsibility"></param>
        /// <returns></returns>
        public ActionResult RevertResponsibility(Guid responsibilityId)
        {
            object result = null;
            try
            {
                DocumentDestroySuggestResponsibitityService.Destroy(responsibilityId);
                result = new { success = true, data = responsibilityId, message = Common.Resource.SystemSuccess };
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }

        /// <summary>
        /// Thu hồi đề nghị hủy chọn nhiều tài liệu
        /// </summary>
        /// <param name="responsibilityId"></param>
        /// <returns></returns>
        public ActionResult RevertDestroySuggestResponsibility(Guid responsibilityId)
        {
            object result = null;
            try
            {
                DocumentDestroySuggestResponsibitityService.RevertResponsibility(responsibilityId);
                result = new { success = true, data = responsibilityId, message = Common.Resource.SystemSuccess };
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        #endregion

        #region Thu hồi đề nghị Revert
        /// <summary>
        /// Chức năng thu hồi đề nghị thực hiện trực tiếp trên danh sách đề nghị
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Revert(Guid id)
        {
            object result = null;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentDestroySuggestResponsibitityService.Revert(id);
                    result = new { success = true, data = id, message = Common.Resource.SystemSuccess };
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    result = new { success = false, message = Common.Resource.SystemFailure };
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result);
        }
        #endregion

        #region Xóa đề nghị
        /// <summary>
        /// Xóa thông tin đề nghị
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        public ActionResult Delete(Guid id, Guid responsibilityId)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentDestroySuggestDetailService.DeleteDetail(id);
                    DocumentDestroySuggestService.Delete(id);
                    DocumentDestroySuggestResponsibitityService.Delete(responsibilityId);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Hủy vai trò
        /// <summary>
        /// Hủy vai trò
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Destroy(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentDestroySuggestResponsibitityService.Revert(id);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Chuyển người thực hiện

        /// <summary>
        /// Form chuyển người thực hiện
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult FormChangePerform(string id = default(string), string containerId = "WindowDestroySuggestReview")
        {
            try
            {
                ViewData["ContainerId"] = containerId;
                var model = DocumentDestroySuggestResponsibitityService.GetDestroySuggestByResponsibilityId(iDAS.Service.Common.Utilities.ConvertToGuid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Lưu thông tin chuyển người thực hiện
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ChangePerform(DocumentDestroySuggestResponsibitityBO data)
        {
            var result = false;
            try
            {
                if (DocumentDestroySuggestResponsibitityService.ChangePerform(data))
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                else
                {
                    result = true;
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}