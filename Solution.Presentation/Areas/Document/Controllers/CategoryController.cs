﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ext.Net;
using Ext.Net.MVC;
using System.Web.Mvc;
using iDAS.Service;
using iDAS.Service.Common;
using System.Diagnostics;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class CategoryController : FrontController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UpdateCategoryExternal(string Id = default(string))
        {
            try
            {
                var model = DocumentCategoryService.GetByCategory(iDAS.Service.Common.Utilities.ConvertToGuid(Id));
                model.DocumentId = iDAS.Service.Common.Utilities.ConvertToGuid(Id);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch
            {
                return this.Direct();
            }
        }

        public ActionResult UpdateCategory(string Id = default(string))
        {
            try
            {
                var model = DocumentCategoryService.GetByCategory(iDAS.Service.Common.Utilities.ConvertToGuid(Id));
                model.DocumentId = iDAS.Service.Common.Utilities.ConvertToGuid(Id);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch
            {
                return this.Direct();
            }
        }

        public ActionResult UpdateCategoryDistribute(string Id = default(string))
        {
            try
            {
                var model = DocumentCategoryService.GetByCategoryDistribute(iDAS.Service.Common.Utilities.ConvertToGuid(Id));
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch
            {
                return this.Direct();
            }
        }

        #region Tài liệu nội bộ
        public ActionResult DocumentInternal()
        {
            return PartialView();
        }
        // Danh sách danh mục tài liệu nội bộ
        public ActionResult GetDataInternal(string node, string DepartmentId)
        {
            try
            {
                var nodes = new NodeCollection();
                if (DepartmentId.Contains('_'))
                {
                    string[] DepartmentIdsl = DepartmentId.Trim().Split('_');
                    DepartmentId = DepartmentIdsl[1];
                }
                var documentCategoryID = node == "root" ? (Guid?)null : new Guid(Convert.ToString(node));
                var documentCategories = DocumentCategoryService.GetTreeDocumentCategory(documentCategoryID, new Guid(DepartmentId), Resource.DocumentCategoryType.Internal).OrderBy(x => x.NameFormat).ToList();
                foreach (var category in documentCategories)
                {
                    nodes.Add(createNodeCategory(category));
                }
                return this.Content(nodes.ToJson());
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }

        private Node createNodeCategory(DocumentCategoryBO category)
        {
            var node = new Node();
            node.NodeID = category.Id.ToString();
            node.Text = " " + category.NameFormat;
            node.IconCls = !category.ParentId.HasValue ? "x-fa fa-folder" : "x-fa fa-folder-o";
            node.CustomAttributes.Add(new ConfigItem { Name = "ParentId", Value = category.ParentId.ToString(), Mode = ParameterMode.Value });
            node.Leaf = !category.IsParent;
            node.Expanded = false;
            return node;
        }

        #region Thêm mới danh mục tài liệu
        public ActionResult Form(string id, string departmentId, string departmentName, string parentId, Resource.DocumentCategoryType type, Common.Operation? operation = null)
        {
            try
            {
                ViewData.Add("Operation", operation);
                DocumentCategoryBO model = null;
                ViewData.Add("DocumentCategoryType", type);
                if (string.IsNullOrEmpty(id))
                {
                    if (departmentId.Contains('_'))
                    {
                        string[] DepartmentIdsl = departmentId.Trim().Split('_');
                        departmentId = DepartmentIdsl[1];
                    }
                    ViewData.Add("departmentName", departmentName);
                    ViewData.Add("departmentId", departmentId);
                    model = new DocumentCategoryBO()
                    {
                        ParentId = !string.IsNullOrEmpty(parentId) ? new Guid(parentId) : (Guid?)null,
                        DepartmentId = new Guid(departmentId),
                        IsInternal = type == Resource.DocumentCategoryType.Internal,
                        IsDistribute = type == Resource.DocumentCategoryType.Distribute,
                        IsExternal = type == Resource.DocumentCategoryType.External
                    };
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
                }
                else
                {
                    model = DocumentCategoryService.GetById(iDAS.Service.Common.Utilities.ConvertToGuid(id));
                    ViewData.Add("departmentId", model.DepartmentId.ToString());
                    return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
                }
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        // thêm mới danh mục tài liệu
        [ValidateInput(false)]
        public ActionResult Create(DocumentCategoryBO item)
        {
            var result = false;
            try
            {
                DocumentCategoryService.Insert(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        // Lấy danh sách tài liệu cho một phòng ban
        public ActionResult GetDataDepartmentId(Guid? departmentId, Resource.DocumentCategoryType type)
        {
            try
            {
                var data = DocumentCategoryService.GetByDepartment(departmentId, type).ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        #endregion

        /// <summary>
        /// Lay danh muc ở phong ban ban
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCategorybyDepartment()
        {
            try
            {
                var data = DocumentCategoryService.GetCategorybyDepartment().ToList();
                return this.Store(data);

            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        /// <summary>
        /// lAY DANH MUC TAI LIEU BEN NGOAI PHONG BAN NO
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCategorybyDepartmentExternal()
        {
            try
            {
                var data = DocumentCategoryService.GetCategorybyDepartmentExternal().ToList();
                return this.Store(data);

            }
            catch (Exception)
            {
                return this.Direct();
            }
        }


        public ActionResult GetCategorybyDepartmentDistribute(string Id = default(string))
        {
            try
            {
                var data = DocumentCategoryService.GetCategorybyDepartmentDistribute(iDAS.Service.Common.Utilities.ConvertToGuid(Id)).ToList();
                return this.Store(data);

            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// lay phong ban phan phoi
        /// </summary>
        /// <returns></returns>
        public ActionResult GetDepartmentDistribute(string Id = default(string))
        {
            try
            {
                var data = DepartmentService.GetDepartmentDistribute(iDAS.Service.Common.Utilities.ConvertToGuid(Id)).ToList();
                return this.Store(data);

            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        #region Chi tiết danh mục tài liệu
        /// <summary>
        /// Sửa DocumentCategory
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult Update(DocumentCategoryBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentCategoryService.Update(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        /// <summary>
        /// Xóa DocumentCategory
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(string id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentCategoryService.Delete(new Guid(id));
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.CategoryHasDocumentException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Thay đổi danh mục tài liệu

        public ActionResult ChangeParentCategory(Guid? id)
        {
            return View();
        }
        #endregion

        // Tạo mới tài liệu
        public ActionResult CreateDocument()
        {
            return View();
        }

        // Đếm số danh mục tài liệu nội bộ cho phòng ban
        public ActionResult CountInternalCategory(string department)
        {
            int count = 0;
            try
            {
                var departmentId = Guid.Empty;
                if (department.Contains('_'))
                {
                    departmentId = new Guid(department.Split('_')[1]);
                }
                else
                {
                    departmentId = new Guid(department);
                }
                count = DocumentCategoryService.CountCategoryByDepartment(departmentId, Resource.DocumentCategoryType.Internal);
            }
            catch (Exception)
            {

            }
            return Json(count);
        }

        #endregion

        #region Tài liệu bên ngoài

        public ActionResult CountExternalCategory(string department)
        {
            int count = 0;
            try
            {
                var departmentId = Guid.Empty;
                if (department.Contains('_'))
                {
                    departmentId = new Guid(department.Split('_')[1]);
                }
                else
                {
                    departmentId = new Guid(department);
                }
                count = DocumentCategoryService.CountCategoryByDepartment(departmentId, Resource.DocumentCategoryType.External);
            }
            catch (Exception)
            {

            }
            return Json(count);
        }

        public ActionResult DocumentExternal()
        {
            return PartialView();
        }
        /// <summary>
        /// Danh sách danh mục tài liệu bên ngoài
        /// </summary>
        /// <param name="node"></param>
        /// <param name="DepartmentId"></param>
        /// <returns></returns>
        public ActionResult GetDataExternal(string node, string DepartmentId)
        {
            try
            {

                var nodes = new NodeCollection();
                if (DepartmentId.Contains('_'))
                {
                    string[] DepartmentIdsl = DepartmentId.Trim().Split('_');
                    DepartmentId = DepartmentIdsl[1];
                }
                var documentCategoryID = node == "root" ? (Guid?)null : new Guid(Convert.ToString(node));
                var documentCategories = DocumentCategoryService.GetTreeDocumentCategory(documentCategoryID, new Guid(DepartmentId), Resource.DocumentCategoryType.External).ToList();
                foreach (var category in documentCategories)
                {
                    nodes.Add(createNodeCategory(category));
                }
                return this.Content(nodes.ToJson());
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }
        #endregion

        #region Tài liệu phân phối

        public ActionResult CountDistribute(string department)
        {
            int count = 0;
            try
            {
                var departmentId = Guid.Empty;
                if (department.Contains('_'))
                {
                    departmentId = new Guid(department.Split('_')[1]);
                }
                else
                {
                    departmentId = new Guid(department);
                }
                count = DocumentCategoryService.CountCategoryByDepartment(departmentId, Resource.DocumentCategoryType.Distribute);
            }
            catch (Exception)
            {

            }
            return Json(count);
        }

        public ActionResult Otherdepartments()
        {
            return View();
        }

        public ActionResult GetDataDistribute(string node, string DepartmentId)
        {
            try
            {
                var nodes = new NodeCollection();
                if (DepartmentId.Contains('_'))
                {
                    string[] DepartmentIdsl = DepartmentId.Trim().Split('_');
                    DepartmentId = DepartmentIdsl[1];
                }
                var documentCategoryID = node == "root" ? (Guid?)null : new Guid(Convert.ToString(node));
                var documentCategories = DocumentCategoryService.GetTreeDocumentCategory(documentCategoryID, new Guid(DepartmentId), Resource.DocumentCategoryType.Distribute).ToList();
                foreach (var category in documentCategories)
                {
                    nodes.Add(createNodeCategory(category));
                }
                return this.Content(nodes.ToJson());
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }
        #endregion

        #region khác

        #endregion

    }
}