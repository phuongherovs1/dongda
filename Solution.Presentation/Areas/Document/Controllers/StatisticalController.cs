﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class StatisticalController : FrontController
    {
        //
        // GET: /Document/Statistical/
        public ActionResult Index()
        {
            return PartialView();
        }
        public StoreResult GetDataDocumentStatusAnalytic(string departmentId = default(string))
        {

            var summary = DocumentSummaryService.DocumentStatusAnalytic(iDAS.Service.Common.Utilities.ConvertToGuid(departmentId));
            return new StoreResult(summary);

        }
        public StoreResult GetDataDocumentTypeAnalytic(string departmentId = default(string))
        {

            var summary = DocumentSummaryService.DocumentTypeAnalytic(iDAS.Service.Common.Utilities.ConvertToGuid(departmentId));
            return new StoreResult(summary);

        }
        
        public StoreResult GetDataDocumentApproveActorAnalytic()
        {

            var summary = DocumentSummaryService.DocumentApproveActorAnalytic();
            return new StoreResult(summary);

        }
        public StoreResult GetDataDocumentPubishActorAnalytic()
        {

            var summary = DocumentSummaryService.DocumentPubishActorAnalytic();
            return new StoreResult(summary);

        }
        public StoreResult GetDataDocumentArchiveActorAnalytic()
        {

            var summary = DocumentSummaryService.DocumentArchiveActorAnalytic();
            return new StoreResult(summary);

        }
      

	}
}