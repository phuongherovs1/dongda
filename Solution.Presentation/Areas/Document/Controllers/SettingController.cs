﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;
using iDAS.Service.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{

    public class SettingController : FrontController
    {
        //
        // GET: /Document/Setting/
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var exitRole = UserRoleService.RoleExits("ProcessDocumentCreater");
            ViewData["DocumentProcesses"] = Resource.GetDocumentTypeProcess();
            ViewData["ExitSettingRole"] = exitRole;
            var allowSetting = DocumentDepartmentPermissionService.GetDepartmentRoleSettingProcessByUser().Count() > 0;
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                ViewData["departmentName"] = department.Name;
                ViewData["departmentId"] = department.Id;
            }
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = exitRole ? "Index" : allowSetting ? "SettingProcess" : "ProcessDetail",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult Process_1(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {

                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Process_1",
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult Process_2(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {

                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Process_2",
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult ProcessDetail_1(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {

                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "ProcessDetail_1",
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult ProcessDetail_2(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {

                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "ProcessDetail_2",
                WrapByScriptTag = false
            };
        }

        public Ext.Net.MVC.PartialViewResult Process_3(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {

                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Process_3",
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult Process_4(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {

                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Process_4",
                WrapByScriptTag = false
            };
        }
        public ActionResult ViewEmployee(string idBtn = default(string), string align = default(string), string departmentId = default(string), Resource.DocumentProcessRole typeRole = 0, Resource.DocumentTypeProcess typeProcess = 0)
        {
            object model = null;
            var departmentSplitId = Guid.Empty;
            if (departmentId.Contains('_'))
            {
                departmentSplitId = new Guid(departmentId.Split('_')[1]);
            }
            else
            {
                if (!string.IsNullOrEmpty(departmentId))
                {
                    departmentSplitId = new Guid(departmentId);
                }
            }
            var max = DocumentProcessService.GetMaxOrder(departmentSplitId, (int)typeProcess, (int)typeRole);
            model = new DocumentProcessBO() { TypeProcess = (int)typeProcess, DepartmentId = departmentSplitId, TypeRole = (int)typeRole };
            ViewData.Add("btn", idBtn);
            ViewData.Add("align", align);
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult Form(string id = default(string), string departmentId = default(string), Resource.DocumentTypeProcess typeProcess = 0)
        {
            object model = null;
            if (id == default(string))
            {
                var departmentSplitId = Guid.Empty;
                if (departmentId.Contains('_'))
                {
                    departmentSplitId = new Guid(departmentId.Split('_')[1]);
                }
                else
                {
                    if (!string.IsNullOrEmpty(departmentId))
                    {
                        departmentSplitId = new Guid(departmentId);
                    }
                }
                ViewData.Add("Operation", Common.Operation.Create);
                model = new DocumentProcessBO() { TypeProcess = (int)typeProcess, DepartmentId = departmentSplitId };
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = DocumentProcessService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult FormCopy(string departmentId = default(string), int process = 0)
        {
            var departmentSplitId = Guid.Empty;
            if (departmentId.Contains('_'))
            {
                departmentSplitId = new Guid(departmentId.Split('_')[1]);
            }
            else
            {
                if (!string.IsNullOrEmpty(departmentId))
                {
                    departmentSplitId = new Guid(departmentId);
                }
            }
            ViewData.Add("DepartmentID", departmentSplitId);
            ViewData.Add("Process", process);
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        /// <summary>
        /// Gọi form cập nhật danh mục tài liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult FormUpdateCategory(string id = default(string))
        {
            object model = null;
            model = DocumentDepartmentPermissionService.GetById(new Guid(id.ToString()));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult AddPermission(string departmentId = default(string))
        {
            object model = null;
            var departmentSplitId = Guid.Empty;
            if (departmentId.Contains('_'))
            {
                departmentSplitId = new Guid(departmentId.Split('_')[1]);
            }
            else
            {
                if (!string.IsNullOrEmpty(departmentId))
                {
                    departmentSplitId = new Guid(departmentId);
                }
            }
            model = new DocumentDepartmentPermissionBO() { DepartmentId = departmentSplitId, DepartmentName = DepartmentService.GetById(departmentSplitId).Name };
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult GetAllowDepartment()
        {
            var departmentIds = DocumentDepartmentPermissionService.GetDepartmentRoleSettingProcessByUser();
            var data = DepartmentService.GetByIds(departmentIds);
            return this.Store(data);
        }
        #region Method
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param, string departmentId, int process)
        {
            var departmentSplitId = Guid.Empty;
            if (departmentId.Contains('_'))
            {
                departmentSplitId = new Guid(departmentId.Split('_')[1]);
            }
            else
            {
                if (!string.IsNullOrEmpty(departmentId))
                {
                    departmentSplitId = new Guid(departmentId);
                }
            }
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentProcessService.GetAll(pageIndex, pageSize, out count, departmentSplitId, process);
            return this.Store(data, count);
        }
        public ActionResult GetEmployeeByRole(StoreRequestParameters param, string departmentId, int process, int role)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentProcessService.GetEmployeeByRole(pageIndex, pageSize, out count, new Guid(departmentId), process, role).ToList();
            return this.Store(data, count);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Copy(string departmentId = default(string), string departmentCopyId = default(string), int process = 0)
        {
            var result = false;
            try
            {
                DocumentProcessService.Copy(new Guid(departmentId), new Guid(departmentCopyId), process);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(DocumentProcessBO item)
        {
            var result = false;
            try
            {
                if (item.TypeRole == (int)Resource.DocumentProcessRole.SuggesterWriteAndEdit || item.TypeRole == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit)
                {
                    if (DocumentProcessService.CheckExitRoleInProcess(item.DepartmentId, item.TypeProcess.Value, item.TypeRole.Value))
                    {
                        iDAS.Presentation.Common.Utilities.ShowMessageBox("Thông báo", "Trách nhiệm chỉ cho phép thiết lập một vai trò!", MessageBox.Icon.WARNING);
                        return this.Direct(result: false);
                    }
                }

                DocumentProcessService.InsertProcess(item);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(DocumentProcessBO item)
        {
            var result = false;
            try
            {
                DocumentProcessService.UpdateProcess(item);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentProcessService.DeleteProcess(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        public ActionResult UpdateOrder(bool up, Guid id)
        {
            try
            {
                var res = DocumentProcessService.UpdateOrder(up, id);
                if (!res)
                {
                    iDAS.Presentation.Common.Utilities.ShowMessageBox("Thông báo", "Chức năng chỉ áp dụng với những bản ghi cùng trách nhiệm !", MessageBox.Icon.WARNING, MessageBox.Button.OK);
                }
                return this.Direct(result: true);
            }
            catch
            {
                return this.Direct(result: false);
            }
        }
        public ActionResult LoadRoleTypeByProcess(iDAS.Service.Common.Resource.DocumentTypeProcess process = 0)
        {
            var data = new List<ComboboxBO>();
            switch (process)
            {
                //Quy trình đề nghị viết mới và sửa đổi
                case Resource.DocumentTypeProcess.WriteAndEditSuggest:
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.ReviewWriteAndEdit,
                        Name = Resource.DocumentProcessRoleText.ReviewWriteAndEdit
                    });
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit,
                        Name = Resource.DocumentProcessRoleText.ApprovalWriteAndEdit
                    });
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit,
                        Name = Resource.DocumentProcessRoleText.AssignmentWriteAndEdit
                    });
                    break;

                //Quy trình soạn thảo và ban hành
                case Resource.DocumentTypeProcess.WriteAndRelation:
                    //data.Add(new ComboboxBO
                    //{
                    //    ID = (int)Resource.DocumentProcessRole.Write,
                    //    Name = Resource.DocumentProcessRoleText.Write
                    //});
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.Check,
                        Name = Resource.DocumentProcessRoleText.Check
                    });
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.ApproveDocument,
                        Name = Resource.DocumentProcessRoleText.ApproveDocument
                    });
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.Promulgate,
                        Name = Resource.DocumentProcessRoleText.Promulgate
                    });
                    break;

                //Quy trình đề nghị phân phối tài liệu
                case Resource.DocumentTypeProcess.DistributeSuggest:
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.DistributeSuggestReviewer,
                        Name = Resource.DocumentProcessRoleText.DistributeSuggestReviewer
                    });
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.DistributeSuggestApproval,
                        Name = Resource.DocumentProcessRoleText.DistributeSuggestApproval
                    });
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.Distributer,
                        Name = Resource.DocumentProcessRoleText.Distributer
                    });
                    break;
                //Quy trình đề nghị hủy tài liệu
                case Resource.DocumentTypeProcess.CancelSuggest:
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.DestroyReviewer,
                        Name = Resource.DocumentProcessRoleText.DestroyReviewer
                    });
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.DestroyApproval,
                        Name = Resource.DocumentProcessRoleText.DestroyApproval
                    });
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.DestroyBillCreater,
                        Name = Resource.DocumentProcessRoleText.DestroyBillCreater
                    });
                    break;
                default:
                    break;
            }
            return this.Store(data);
        }
        /// <summary>
        /// Lấy trách nhiệm tiếp theo trong quy trình thuộc phòng ban
        /// </summary>
        /// <param name="departmentId">Id phòng ban</param>
        /// <param name="process">Quy trình</param>
        /// <param name="order">Thứ tự trách nhiệm</param>
        /// <param name="startRole">Trách nhiệm bắt đầu được bỏ qua</param>
        /// <returns></returns>
        public ActionResult GetRoleType(string departmentId = default(string), int process = 0, int order = 0, int? startRole = null)
        {

            var data = new List<ComboboxBO>();
            //Quy trình đề nghị phân phối tài liệu  ------------------------
            if (process == (int)Resource.DocumentTypeProcess.DistributeSuggest)
            {
                var roleNextDistribute = DocumentProcessService.GetDistributeRoleNext(new Guid(departmentId), order);
                bool exitDistribute = roleNextDistribute != null;
                if ((exitDistribute && roleNextDistribute.TypeRole == (int)Resource.DocumentProcessRole.Check) || !exitDistribute)
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.Check,
                        Name = Resource.DocumentProcessRoleText.DistributeSuggestReviewer,
                        Process = process,
                        Order = roleNextDistribute != null ? (roleNextDistribute.Order.HasValue ? roleNextDistribute.Order.Value : order) : order
                    });
                if ((exitDistribute && roleNextDistribute.TypeRole == (int)Resource.DocumentProcessRole.ApproveDocument) || !exitDistribute)
                    data.Add(new ComboboxBO
                    {
                        ID = (int)Resource.DocumentProcessRole.ApproveDocument,
                        Name = Resource.DocumentProcessRoleText.DistributeSuggestApproval,
                        Process = process,
                        Order = roleNextDistribute != null ? (roleNextDistribute.Order.HasValue ? roleNextDistribute.Order.Value : order) : order
                    });
                return this.Store(data);
            }
            order = order + 1;
            if (departmentId == default(string) || string.IsNullOrEmpty(departmentId))
                return this.Store(data);
            else
            {
                //Kiểm tra xem phòng ban có chứa quy trình hay không.
                bool exits = DocumentProcessService.CheckExitProcess(new Guid(departmentId), process);
                //Nếu phòng ban chứa quy trình thì lấy ra trách nhiệm tiếp theo theo Order
                var roleNext = new DocumentProcessBO();
                if (exits)
                {
                    DocumentProcessService.GetRoleNext(new Guid(departmentId), process, order, ref roleNext, startRole);
                    process = roleNext.TypeProcess.HasValue ? roleNext.TypeProcess.Value : process;
                }
                switch (process)
                {
                    //Quy trình đề nghị viết mới và sửa đổi
                    case (int)Resource.DocumentTypeProcess.WriteAndEditSuggest:
                        if ((exits && roleNext.TypeRole == (int)Resource.DocumentProcessRole.ReviewWriteAndEdit) || !exits)
                            data.Add(new ComboboxBO
                            {
                                ID = (int)Resource.DocumentProcessRole.ReviewWriteAndEdit,
                                Name = Resource.DocumentProcessRoleText.ReviewWriteAndEdit,
                                Process = process,
                                Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                            });
                        if ((exits && roleNext.TypeRole == (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit) || !exits)
                            data.Add(new ComboboxBO
                            {
                                ID = (int)Resource.DocumentProcessRole.ApprovalWriteAndEdit,
                                Name = Resource.DocumentProcessRoleText.ApprovalWriteAndEdit,
                                Process = process,
                                Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                            });
                        if ((exits && roleNext.TypeRole == (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit) || !exits)
                            data.Add(new ComboboxBO
                            {
                                ID = (int)Resource.DocumentProcessRole.AssignmentWriteAndEdit,
                                Name = Resource.DocumentProcessRoleText.AssignmentWriteAndEdit,
                                Process = process,
                                Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                            });
                        break;

                    //Quy trình soạn thảo và ban hành
                    case (int)Resource.DocumentTypeProcess.WriteAndRelation:
                        if ((exits && roleNext.TypeRole == (int)Resource.DocumentProcessRole.Write) || !exits)
                            data.Add(new ComboboxBO
                            {
                                ID = (int)Resource.DocumentProcessRole.Write,
                                Name = Resource.DocumentProcessRoleText.Write,
                                Process = process,
                                Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                            });
                        if ((exits && roleNext.TypeRole == (int)Resource.DocumentProcessRole.Check)
                            || !exits)
                            data.Add(new ComboboxBO
                            {
                                ID = (int)Resource.DocumentProcessRole.Check,
                                Name = Resource.DocumentProcessRoleText.Check,
                                Process = process,
                                Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                            });
                        if ((exits && roleNext.TypeRole == (int)Resource.DocumentProcessRole.ApproveDocument) || !exits)
                            data.Add(new ComboboxBO
                            {
                                ID = (int)Resource.DocumentProcessRole.ApproveDocument,
                                Name = Resource.DocumentProcessRoleText.ApproveDocument,
                                Process = process,
                                Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                            });
                        if ((exits && roleNext.TypeRole == (int)Resource.DocumentProcessRole.Promulgate) || !exits)
                            data.Add(new ComboboxBO
                            {
                                ID = (int)Resource.DocumentProcessRole.Promulgate,
                                Name = Resource.DocumentProcessRoleText.Promulgate,
                                Process = process,
                                Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                            });
                        if ((exits && roleNext.TypeRole == (int)Resource.DocumentProcessRole.Archiver) || !exits)
                            data.Add(new ComboboxBO
                            {
                                ID = (int)Resource.DocumentProcessRole.Archiver,
                                Name = Resource.DocumentProcessRoleText.Archiver,
                                Process = process,
                                Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                            });
                        break;
                    //Quy trình đề nghị hủy tài liệu ------------------------
                    case (int)Resource.DocumentTypeProcess.CancelSuggest:
                        if ((exits && roleNext.TypeRole == (int)Resource.DocumentProcessRole.DestroyCreater) || !exits)
                            data.Add(new ComboboxBO
                           {
                               ID = (int)Resource.DocumentProcessRole.DestroyCreater,
                               Name = Resource.DocumentProcessRoleText.DestroyCreater,
                               Process = process,
                               Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                           });
                        if ((exits && roleNext.TypeRole == (int)Resource.DocumentProcessRole.DestroyReviewer) || !exits)
                            data.Add(new ComboboxBO
                            {
                                ID = (int)Resource.DocumentProcessRole.DestroyReviewer,
                                Name = Resource.DocumentProcessRoleText.DestroyReviewer,
                                Process = process,
                                Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                            });
                        if ((exits && roleNext.TypeRole == (int)Resource.DocumentProcessRole.DestroyApproval) || !exits)
                            data.Add(new ComboboxBO
                            {
                                ID = (int)Resource.DocumentProcessRole.DestroyApproval,
                                Name = Resource.DocumentProcessRoleText.DestroyApproval,
                                Process = process,
                                Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                            });
                        if ((exits && roleNext.TypeRole == (int)Resource.DocumentProcessRole.DestroyBillCreater) || !exits)
                            data.Add(new ComboboxBO
                            {
                                ID = (int)Resource.DocumentProcessRole.DestroyBillCreater,
                                Name = Resource.DocumentProcessRoleText.DestroyBillCreater,
                                Process = process,
                                Order = roleNext.Order.HasValue ? roleNext.Order.Value : order
                            });
                        break;
                    default:
                        break;
                }
                return this.Store(data);
            }
        }
        public ActionResult GetNextRoleType(string departmentId = default(string), int typeProcess = 0, int order = 0, int? startRole = null)
        {
            try
            {
                var roleData = DocumentProcessService.GetNextRoleType(new Guid(departmentId),
                    typeProcess: (int)typeProcess, roleType: startRole, order: order);
                return this.Store(roleData);
            }
            catch
            {
                return this.Direct(result: false);
            }
        }
        #endregion
    }
}