﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class DistributeSuggestController : FrontController
    {

        public ActionResult Index()
        {
            ViewData.Add("StatusApprove", iDAS.Service.Common.Resource.GetStatusApprove());
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        public ActionResult ListDistributeSuggest(string containerId)
        {
            ViewData.Add("StatusApprove", iDAS.Service.Common.Resource.GetStatusApprove());
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }

        public ActionResult FormDistributeSuggest()
        {
            object model = null;
            model = new DocumentReviewSuggestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Form tạo mới đề nghị phân phối
        /// </summary>
        /// <returns></returns>
        public ActionResult UserDistributeSuggest(string id = "")
        {
            object model = null;
            try
            {
                if (String.IsNullOrEmpty(id))
                {
                    model = new DocumentDistributeSuggestBO()
                    {
                        Document = new DocumentBO(),
                        Id = Guid.Empty
                    };
                    ViewData.Add("Operation", Common.Operation.Create);
                }
                else
                {
                    ViewData.Add("Operation", Common.Operation.Update);
                    var distributeSuggest = DocumentDistributeSuggestService.GetById(new Guid(id));
                    model = DocumentDistributeSuggestService.CreateDefault(distributeSuggest.DocumentId.Value, id);
                }
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        /// <summary>
        /// Gọi form phân phối tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public ActionResult Form(string documentId)
        {
            if (string.IsNullOrEmpty(documentId))
                return this.Direct();
            var model = DocumentDistributeSuggestService.CreateDefault(iDAS.Service.Common.Utilities.ConvertToGuid(documentId));
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewName = "FormDistributeSuggest" };
        }

        #region Đề nghị phân phối
        #region Form chi tiết phân phối tài liệu
        /// <summary>
        /// Form chi tiết phân phối tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="id">ReviewSuggestId</param>
        /// <param name="type">0: Từ danh sách đề nghị kiểm duyệt gọi form chi tiết, 1: Từ danh sách duyệt đề nghị kiểm duyệt gọi form chi tiết</param>
        /// <returns></returns>
        public ActionResult Detail(string documentId, string id = default(string), int type = 0)
        {
            ViewData.Add("Type", type);
            if (string.IsNullOrEmpty(documentId))
                return this.Direct();
            var model = DocumentDistributeSuggestService.CreateDefault(iDAS.Service.Common.Utilities.ConvertToGuid(documentId), id);
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion

        #region Tạo đề nghị phân phối tài liệu, chỉ lưu chưa gửi phê duyệt
        /// <summary>
        /// Tạo đề nghị phân phối tài liệu, chỉ lưu chưa gửi phê duyệt
        /// </summary>
        /// <param name="adjustSuggest"></param>
        /// <returns></returns>
        public ActionResult Create(DocumentDistributeSuggestBO distributeSuggest)
        {
            object result = null;
            if (ModelState.IsValid)
            {
                try
                {
                    distributeSuggest.ApproveBy = distributeSuggest.EmployeeApproveBy.Id;
                    distributeSuggest.DocumentId = distributeSuggest.Document.Id;
                    if (distributeSuggest.Id == null || distributeSuggest.Id == Guid.Empty)
                        DocumentDistributeSuggestService.Insert(distributeSuggest);
                    else
                        DocumentDistributeSuggestService.Update(distributeSuggest);
                    result = new { success = true, data = distributeSuggest.Id, message = Common.Resource.SystemSuccess };
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception)
                {
                    result = new { success = false, message = Common.Resource.SystemFailure };
                }
            }
            return this.Direct(result);
        }
        #endregion

        #region Danh sách đề nghị phân phối tài liệu gửi đi
        /// <summary>
        /// Lấy danh sách đề nghị phân phối tài liệu
        /// </summary>
        /// <param name="param"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public ActionResult GetData(StoreRequestParameters param, iDAS.Service.Common.Resource.ApproveStatus status = iDAS.Service.Common.Resource.ApproveStatus.All)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentDistributeSuggestService.GetAll(pageIndex, pageSize, out count, status);
            return this.Store(data, count);
        }
        #endregion

        #region Gửi thông tin đề nghị phân phối
        /// <summary>
        /// Gửi thông tin đề nghị phân phối
        /// </summary>
        /// <param name="adjustSuggest"></param>
        /// <returns></returns>
        public ActionResult SendApprove(DocumentDistributeSuggestBO distributeSuggest)
        {
            object result = null;
            try
            {
                distributeSuggest.DocumentId = distributeSuggest.Document.Id;
                DocumentDistributeSuggestService.Send(distributeSuggest);
                result = new { success = true, data = distributeSuggest.Id, message = Common.Resource.SystemSuccess };
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
        #endregion

        #region Xóa đề nghị phân phối tài liệu
        /// <summary>
        /// Xóa đề nghị phân phối tài liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentDistributeSuggestService.Delete(id);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion

        #region Thu hồi đề nghị phân phối tài liệu
        /// <summary>
        /// Xóa đề nghị phân phối tài liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Revert(DocumentDistributeSuggestBO distributeSuggest)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    distributeSuggest.DocumentId = distributeSuggest.Document.Id;
                    DocumentDistributeSuggestService.Revert(distributeSuggest);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
        #endregion

        #region Duyệt đề nghị phân phối
        #region Form danh sách duyệt đề nghị phân phối tài liệu
        /// <summary>
        /// Form danh sách duyệt đề nghị phân phối tài liệu
        /// </summary>
        /// <returns></returns>
        public ActionResult ListDocumentDistributeSuggest()
        {
            ViewData.Add("StatusApprove", iDAS.Service.Common.Resource.GetStatusApproveSuggest());
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        #endregion

        #region Danh sách duyệt đề nghị phân phối tài liệu gửi đi
        /// <summary>
        /// Lấy danh sách duyệt đề nghị phân phối tài liệu
        /// </summary>
        /// <param name="param"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public ActionResult GetByApprover(StoreRequestParameters param, iDAS.Service.Common.Resource.ApproveStatus status = iDAS.Service.Common.Resource.ApproveStatus.All)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentDistributeSuggestService.GetByApprover(pageIndex, pageSize, out count, status);
            return this.Store(data, count);
        }
        #endregion

        #region Form duyệt đề nghị phân phối tài liệu
        /// <summary>
        /// Form chi tiết phân phối tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="id">ReviewSuggestId</param>
        /// <param name="type">0: Từ danh sách đề nghị kiểm duyệt gọi form chi tiết, 1: Từ danh sách duyệt đề nghị kiểm duyệt gọi form chi tiết</param>
        /// <returns></returns>
        public ActionResult FormApprove(string documentId, string id = default(string))
        {
            if (string.IsNullOrEmpty(documentId))
                return this.Direct();
            var model = DocumentDistributeSuggestService.CreateDefault(iDAS.Service.Common.Utilities.ConvertToGuid(documentId), id);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        #endregion

        #region Duyệt đề nghị phân phối
        /// <summary>
        /// Duyệt đề nghị phân phối
        /// </summary>
        /// <param name="distributeSuggest"></param>
        /// <returns></returns>
        public ActionResult Approve(DocumentDistributeSuggestBO distributeSuggest)
        {
            object result = null;
            try
            {
                distributeSuggest.ApproveAt = DateTime.Now;
                distributeSuggest.ApproveBy = distributeSuggest.EmployeeApproveBy.Id;
                distributeSuggest.IsApprove = true;
                DocumentDistributeSuggestService.Update(distributeSuggest);
                result = new { success = true, data = distributeSuggest.Id, message = Common.Resource.SystemSuccess };
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
        #endregion
        #endregion

        /// <summary>
        /// Gọi form chọn người phân phối sau khi nhấn nút gửi đề nghị phân phối
        /// </summary>
        /// <returns></returns>
        public ActionResult FormChooseDistributer(string id = "")
        {
            object model = null;
            if (String.IsNullOrEmpty(id) || new Guid(id.ToString()) == Guid.Empty)
            {
                model = new DocumentDistributeSuggestBO();
            }
            else
            {
                model = DocumentDistributeSuggestService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Form chọn tài liệu đề nghị phân phối
        /// </summary>
        /// <returns></returns>
        public ActionResult FormChooseDocument()
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            return null;
        }

        /// <summary>
        /// Danh sách tài liệu đã ban hành dùng cho form chọn tài liệu khi tạo mới đề nghị phân phối
        /// </summary>
        /// <param name="param"></param>
        /// <param name="filter"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public ActionResult GetDocumentPublishForDistributeSuggest(StoreRequestParameters param, string filter = "", string departmentId = "")
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentPublishService.GetAllForDistributeSuggest(pageIndex, pageSize, out count, filter, departmentId);
            return this.Store(data, count);
        }

        public ActionResult FormInfoDocument(string documentId = "")
        {
            object model = null;
            try
            {
                if (!String.IsNullOrEmpty(documentId) && new Guid(documentId.ToString()) != Guid.Empty)
                {
                    model = DocumentService.GetById(new Guid(documentId));
                }
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        #region Thực hiện đề luồng đề nghị mới : thanhnv
        [HttpGet]
        public ActionResult SendToPromulgate(string documentId, string id)
        {
            object model = null;
            try
            {
                if (!string.IsNullOrEmpty(documentId))
                    model = DocumentDistributeSuggestService.CreateDefault(documentId: new Guid(documentId.ToString()), id: id);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        [HttpPost]
        public ActionResult SendToPromulgate(DocumentDistributeSuggestBO distributeSuggest)
        {
            object result = null;
            try
            {
                distributeSuggest.DocumentId = distributeSuggest.Document.Id;
                DocumentDistributeSuggestService.SendToPromulgate(distributeSuggest);
                this.ShowNotify(Common.Resource.SystemSuccess);
                result = new { success = true, data = distributeSuggest.Id, message = Common.Resource.SystemSuccess };
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }

        // Thực hiện đề nghị 
        public ActionResult Perform(string id)
        {
            object model = null;
            try
            {
                if (!string.IsNullOrEmpty(id))
                    model = DocumentDistributeSuggestService.GetDetailPerform(id: new Guid(id));
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        // Thêm mới đề nghị
        public ActionResult CreatePerform(DocumentDistributeSuggestBO distributeSuggest)
        {
            bool result = false;
            try
            {
                DocumentDistributeSuggestService.Insert(distributeSuggest);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return this.Direct(result);
        }
        // Lấy danh sách trách nhiệm liên quan
        public ActionResult LoadResponsibilities(string suggestId = "")
        {
            var data = DocumentDistributeSuggestResponsibilityService.GetBySuggest(new Guid(suggestId));
            return this.Store(data);
        }
        // Lấy danh sách người ban hành ở quy trình 
        public ActionResult LoadDetailPromulgateEmployees(StoreRequestParameters param, string query = "", string departmenId = "")
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentDistributeSuggestService.GetPromulgateEmployees(pageIndex, pageSize, out count, query, departmenId);
            return this.Store(data, count);
        }
        public ActionResult SendToResponsibility(string suggestId = default(string), string departmentId = default(string), int order = 0)
        {
            try
            {
                if (DocumentDistributeSuggestService.ExitsResponsibility(new Guid(suggestId)))
                {
                    this.ShowAlert(Common.Resource.SendComplete);
                    return this.Direct();
                }
                var data = new DocumentDistributeSuggestResponsibilityBO()
                {
                    DocumentDistributeSuggestId = !string.IsNullOrEmpty(suggestId) ? new Guid(suggestId) : new Nullable<Guid>(),
                    DepartmentId = new Guid(departmentId),
                    Order = order
                };
                return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData, ViewName = "SuggestNextRole" };
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult SuggestNextRole(string id = default(string), string suggestId = default(string), string departmentId = default(string), int order = 0)
        {
            try
            {
                var data = new DocumentDistributeSuggestResponsibilityBO()
                {
                    Id = !string.IsNullOrEmpty(id) ? new Guid(id) : Guid.Empty,
                    DocumentDistributeSuggestId = !string.IsNullOrEmpty(suggestId) ? new Guid(suggestId) : new Nullable<Guid>(),
                    DepartmentId = new Guid(departmentId),
                    Order = order
                };
                return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateResponsibility(DocumentDistributeSuggestResponsibilityBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentDistributeSuggestResponsibilityService.InsertResponsibility(item);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        this.ShowAlert(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        public ActionResult PerformApprove(string id)
        {
            object model = null;
            try
            {
                if (!string.IsNullOrEmpty(id))
                    model = DocumentDistributeSuggestResponsibilityService.GetDetailPerform(id: new Guid(id));
                this.ShowAlert(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult ApproveSave(DocumentDistributeSuggestResponsibilityBO data)
        {
            bool result = false;
            try
            {
                DocumentDistributeSuggestResponsibilityService.ApproveSave(data);
                this.ShowNotify(Common.Resource.SystemSuccess);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        public ActionResult AcceptDistributte(string id)
        {
            bool result = false;
            try
            {
                DocumentDistributeSuggestService.AcceptDistributte(new Guid(id));
                this.ShowNotify(Common.Resource.SystemSuccess);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }

        public ActionResult UpdateSuggestDistributte(string id)
        {
            bool result = false;
            try
            {
                DocumentDistributeSuggestService.updateSuggestDistribute(new Guid(id));
                this.ShowNotify(Common.Resource.SystemSuccess);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowAlert(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result);
        }
        public ActionResult DistributeRequest(string suggestId, string documentId = "", string documentName = "")
        {
            var model = new DocumentRequestBO()
            {
                DocumentSuggestId = new Guid(suggestId),
                Document = new DocumentBO()
                {
                    Id = new Guid(documentId),
                    Name = documentName
                }
            };
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #endregion
    }
}