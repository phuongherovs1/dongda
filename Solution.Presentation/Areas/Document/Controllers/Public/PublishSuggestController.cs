﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;
namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class PublishSuggestController : FrontController
    {
        //
        // GET: /Document/PublishSuggest/
        public ActionResult Index(bool isSuggest = true)
        {
            ViewData.Add("IsSuggest", isSuggest);
            ViewData.Add("StatusPublishSuggest", iDAS.Service.Common.Resource.GetStatusApproveSuggest());
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }

        /// <summary>
        /// Gọi form chi tiết
        /// </summary>
        /// <param name="id"></param>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public ActionResult Form(string id = default(string), string documentId = default(string))
        {
            object model = null;
            if (id == default(string))
            {
                if (documentId == default(string))
                    model = new DocumentPublishSuggestBO() { Document = new DocumentBO() };
                else
                {
                    model = DocumentPublishSuggestService.CreateDefault(iDAS.Service.Common.Utilities.ConvertToGuid(documentId), id);
                }
            }
            else
            {
                if (documentId == default(string))
                    return this.Direct();
                model = DocumentPublishSuggestService.CreateDefault(iDAS.Service.Common.Utilities.ConvertToGuid(documentId), id);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Lưu thông tin đề nghị ban hành
        /// </summary>
        /// <param name="suggest"></param>
        /// <returns></returns>
        public ActionResult Save(DocumentPublishSuggestBO suggest, string jsonData = "")
        {
            object result = null;
            if (ModelState.IsValid)
            {
                try
                {
                    suggest.Id = DocumentPublishSuggestService.Save(suggest, jsonData);
                    result = new { success = true, data = suggest.Id, message = Common.Resource.SystemSuccess };
                }
                catch (Exception)
                {
                    result = new { success = false, message = Common.Resource.SystemFailure };
                }
            }
            return this.Direct(result);
        }

        /// <summary>
        /// Gửi duyệt ban hành
        /// </summary>
        /// <param name="suggest"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public ActionResult Send(DocumentPublishSuggestBO suggest, string jsonData = "")
        {
            object result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    suggest.Id = DocumentPublishSuggestService.Send(suggest, jsonData);
                    result = new { success = true, data = suggest.Id, message = Common.Resource.SystemSuccess };
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    result = new { success = false, message = Common.Resource.SystemFailure };
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result);
        }

        /// <summary>
        /// Chức năng thu hồi đề nghị ban hành
        /// Cập nhật lại trạng thái IsSend về false
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult Obsolate(DocumentPublishSuggestBO item)
        {
            object result = null;
            if (ModelState.IsValid)
            {
                try
                {
                    item.ApproveBy = item.EmployeeApproveBy.Id;
                    item.IsSend = false;
                    DocumentPublishSuggestService.Update(item);
                    result = new { success = true, data = item.Id, message = Common.Resource.SystemSuccess };
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    result = new { success = false, message = Common.Resource.SystemFailure };
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result);
        }

        /// <summary>
        /// Danh sách đề nghị ban hành
        /// </summary>
        /// <param name="param"></param>
        /// <param name="approveStatus"></param>
        /// <param name="isSuggest">isSuggest = true: Lấy danh sách đề nghị được tạo bởi User đăng nhập, isSuggest = false: Lấy danh sách User đăng nhập có quyền phê duyệt</param>
        /// <returns></returns>
        public ActionResult GetData(StoreRequestParameters param, iDAS.Service.Common.Resource.ApproveStatus approveStatus = iDAS.Service.Common.Resource.ApproveStatus.All, bool isSuggest = true)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentPublishSuggestService.GetCreateByCurrentUser(pageIndex, pageSize, out count, approveStatus, isSuggest);
            return this.Store(data, count);
        }
        public ActionResult GetDataApprove(StoreRequestParameters param, iDAS.Service.Common.Resource.ApproveStatus approveStatus = iDAS.Service.Common.Resource.ApproveStatus.All)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentPublishSuggestService.GetApproveByCurrentUser(pageIndex, pageSize, out count, approveStatus);
            return this.Store(data, count);
        }

        /// <summary>
        /// Form duyệt đề nghị ban hành
        /// </summary>
        /// <param name="id"></param>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public ActionResult FormApprove(string id = default(string), string documentId = default(string))
        {
            object model = null;
            if (id == default(string))
            {
                if (documentId == default(string))
                    model = new DocumentPublishSuggestBO() { Document = new DocumentBO() };
                else
                {
                    model = DocumentPublishSuggestService.CreateDefault(iDAS.Service.Common.Utilities.ConvertToGuid(documentId), id);
                }
            }
            else
            {
                if (documentId == default(string))
                    return this.Direct();
                model = DocumentPublishSuggestService.CreateDefault(iDAS.Service.Common.Utilities.ConvertToGuid(documentId), id);
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        public ActionResult ApproveIndex()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }

        /// <summary>
        /// Chức năng duyệt
        /// </summary>
        /// <param name="publishSuggest"></param>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        public ActionResult Approve(DocumentPublishSuggestBO publishSuggest, string jsonData = "")
        {
            object result = null;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentPublishSuggestService.Approve(publishSuggest, jsonData);
                    result = new { success = true, data = publishSuggest.Id, message = Common.Resource.SystemSuccess };
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    result = new { success = false, message = Common.Resource.SystemFailure };
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result);
        }

        /// <summary>
        /// Gọi form chọn người phê duyệt ban hành sau khi nhấn nút gửi đề nghị ban hành
        /// </summary>
        /// <returns></returns>
        public ActionResult FormChoosePublisher(string id = "")
        {
            object model = null;
            if (String.IsNullOrEmpty(id) || new Guid(id.ToString()) == Guid.Empty)
            {
                model = new DocumentPublishSuggestBO();
            }
            else
            {
                model = DocumentPublishSuggestService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        // Gọi fomr chọn người phê duyệt
        public ActionResult FormChooseApprovePublisher(string id = "")
        {
            object model = null;
            if (String.IsNullOrEmpty(id) || new Guid(id.ToString()) == Guid.Empty)
            {
                model = new DocumentPublishSuggestBO();
            }
            else
            {
                model = DocumentPublishSuggestService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Gọi form đề nghị ban hành
        /// </summary>
        /// <returns></returns>
        public ActionResult FormPublishSuggest()
        {
            object model = null;
            model = new DocumentPublishSuggestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Gọi form kết quả kiểm tra
        /// </summary>
        /// <returns></returns>
        public ActionResult FormResultPublishSuggest(string id = "")
        {
            object model = null;
            if (String.IsNullOrEmpty(id) || new Guid(id.ToString()) == Guid.Empty)
            {
                model = new DocumentPublishSuggestBO();
            }
            else
            {
                model = DocumentPublishSuggestService.GetById(new Guid(id.ToString()));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
    }
}