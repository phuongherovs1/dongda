﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class PublishController : FrontController
    {
        public ActionResult Form(string documentId = "")
        {
            if (string.IsNullOrEmpty(documentId))
                return this.Direct();
            var model = DocumentPublishService.CreateDefault(new Guid(documentId));
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        /// <summary>
        /// Danh sách tài liệu ban hành
        /// </summary>
        /// <param name="param"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public ActionResult GetdataPublish(StoreRequestParameters param, string search = "")
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentPublishService.GetdataPublish(pageIndex, pageSize, out count, search).ToList();
            return this.Store(data, count);
        }

        /// <summary>
        /// Get tai lieu phan phoi
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ActionResult GetDistribute(StoreRequestParameters param)
        {
            var data = DocumentService.GetDistribute();
            return this.Store(data);
        }
        /// <summary>
        /// Danh sách các lần ban hành của tài liệu có cùng mã Code
        /// </summary>
        /// <param name="param"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult GetCoutPublish(StoreRequestParameters param, String Id)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentPublishService.GetCoutPublish(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(Id));
            return this.Store(data, count);
        }
        public ActionResult Create(DocumentPublishBO publish, string jsonData = "")
        {
            object result = null;
            try
            {
                publish.Id = DocumentPublishService.Insert(publish, jsonData);
                result = new { success = true, data = publish.Id, message = Common.Resource.SystemSuccess };
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
        public ActionResult PublishList()
        {
            ViewData["DocumentStatus"] = iDAS.Service.Common.Resource.GetEDocumentStatus();
            return PartialView();
        }
        public ActionResult ApproveList(string DocumentId = default(string))
        {
            try
            {
                ViewData.Add("documentId", DocumentId);
                var model = new DocumentResponsibilityBO();
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };

            }
            catch
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// Danh sách các lần ban hành của tài liệu cùng Code
        /// </summary>
        /// <param name="Id">PublishId</param>
        /// <returns></returns>
        public ActionResult RevisionList(string Id = default(string))
        {
            try
            {
                ViewData.Add("PublishId", Id);
                var model = new DocumentPublishBO() { Document = new DocumentBO() };
                model.Id = iDAS.Service.Common.Utilities.ConvertToGuid(Id);
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }

        public ActionResult InfoPublish()
        {
            var model = new DocumentPublishSuggestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        /// <summary>
        /// Thông tin yêu cầu ban hành
        /// </summary>
        /// <param name="Id">RequestId</param>
        /// <param name="Name"></param>
        /// <param name="PublishAt"></param>
        /// <param name="IsHard"></param>
        /// <param name="IsSoft"></param>
        /// <returns></returns>
        public ActionResult InfoRequestPublish(Guid id, string name = default(string))
        {
            ViewData["Title"] = name;
            var model = DocumentPublishService.GetRequestPromulgateByUser(id);
            model.RequestId = id;
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        /// <summary>
        /// Ban hành tài liệu
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ActionResult SavePublish(DocumentPublishBO item)
        {
            var result = false;
            try
            {
                DocumentPublishService.IsertPublish(item);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is DataHasBeenExistedException == false)
                    this.ShowNotify(ex.Message);
                else if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult DestinationDocument()
        {
            var model = new DocumentPublishSuggestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult Retrieve()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }
        public ActionResult RecoverReleaseDocument(string id = default(string), string name = default(string))
        {
            try
            {
                ViewData["Tilte"] = name;
                var data = DocumentRequestService.GetById(new Guid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }
        /// <summary>
        /// Gọi form chuyển người thực hiện 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult FormChangePerformBy(string requestId = default(string))
        {
            try
            {
                var data = DocumentResponsibilityService.GetById(new Guid(requestId));
                return new Ext.Net.MVC.PartialViewResult { Model = data };
            }
            catch
            {
                return this.Direct();
            }
        }
        /// <summary>
        /// Cập nhật người thực hiện
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult ChangePerform(DocumentRequestBO request)
        {
            var result = false;
            try
            {
                request.PerformBy = request.EmployeePerformBy.Id;
                DocumentRequestService.Update(request);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        #region 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="extendId"></param>
        /// <param name="documentId"></param>
        /// <param name="departmentId"></param>
        /// <param name="handler"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public ActionResult SendArchiveForm(string extendId, string documentId, string departmentId, string handler, int order = 0)
        {
            try
            {
                ViewData["Handler"] = handler;
                var data = new DocumentResponsibilityBO()
                {
                    ExtendId = !string.IsNullOrEmpty(extendId) ? new Guid(extendId) : new Nullable<Guid>(),
                    DocumentId = new Guid(documentId),
                    DepartmentId = new Guid(departmentId),
                    Order = order
                };
                return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public ActionResult GetEmployeeArchive(string departmentId, string query)
        {
            try
            {
                var data = EmployeeService.GetArchiveRoleByDepartment( new Guid(departmentId), query).ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult SendArchiveRequest(DocumentResponsibilityBO item)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentResponsibilityService.SendRequestArchive(item);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}