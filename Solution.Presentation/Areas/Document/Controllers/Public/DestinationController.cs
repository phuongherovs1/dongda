﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class DestinationController : FrontController
    {

        public ActionResult Index(Guid documentId)
        {
            return PartialView();
        }

        public ActionResult GetData(Guid documentId)
        {
            var data = DocumentDestinationService.GetByDocument(documentId);
            return this.Store(data);
        }

        /// <summary>
        /// Danh sách nơi nhận theo tài liệu
        /// </summary>
        /// <param name="param"></param>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public ActionResult GetDataByDocument(StoreRequestParameters param, Guid? documentId)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentDestinationService.GetByDocumentId(pageIndex, pageSize, out count, documentId);
            return this.Store(data, count);
        }
    }
}