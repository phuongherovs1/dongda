﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class ArchiveController : FrontController
    {
        //
        // GET: /Document/Archive/
        public ActionResult Index()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }

        public ActionResult ArchiveDocument()
        {
            var model = new DocumentRequestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult InfoArchive()
        {
            object model = null;
            model = new DocumentReviewSuggestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        public ActionResult NextRequestAction(string id = default(string))
        {
            try
            {
                var data = DocumentRequestService.GetById(new Guid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = data };
            }
            catch
            {
                return this.Direct();
            }
        }
        public ActionResult FormArchive(string id = default(string), string name = default(string))
        {
            try
            {
                ViewData["Tilte"] = name;
                var data = DocumentResponsibilityService.GetDetail(new Guid(id));
                return new Ext.Net.MVC.PartialViewResult { Model = data, ViewData = ViewData };
            }
            catch
            {
                return this.Direct();
            }
        }
        #region Method
        [ValidateInput(false)]
        public ActionResult GetData(StoreRequestParameters param)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentArchiveService.GetAll(pageIndex, pageSize, out count).ToList();
            return this.Store(data, count);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SaveArchive(DocumentResponsibilityBO item)
        {
            var result = false;
            try
            {
                DocumentArchiveService.SaveArchive(item.Id, item.Archive);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(DocumentResponsibilityBO item)
        {
            var result = false;
            try
            {
                DocumentArchiveService.UpdateArchive(item.Id, item.Archive);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentArchiveService.Delete(id);
                    result = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion

    }
}