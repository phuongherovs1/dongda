﻿using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service.Common;
using iDAS.Presentation.Common;

namespace iDAS.Presentation.Areas.Document.Controllers
{
    public class DistributeController : FrontController
    {

        public ActionResult Index(Guid documentId)
        {
            return PartialView();
        }

        public ActionResult DeleteDistribute(Guid DistributeId)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    DocumentDistributeService.DeleteDistribute(DistributeId);
                    this.ShowNotify(Common.Resource.SystemSuccess);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        /// <summary>
        /// Get Distribute
        /// </summary>
        /// <param name="param"></param>
        /// <param name="DocumentId"></param>
        /// <returns></returns>
        public ActionResult GetAll(StoreRequestParameters param, Guid DocumentId)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = DocumentDistributeService.GetAll(DocumentId,pageIndex, pageSize, out count).ToList();
            return this.Store(data, count);
        }
       
        /// <summary>
        /// Gọi form phân phối tài liệu
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public ActionResult ListDistribute(string documentId)
        {
            var model = new DocumentRequestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
    
        public ActionResult DetailDistribute(string documentId)
        {
            var model = new DocumentRequestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        public ActionResult Form(string documentId)
        {
            var model = new DocumentRequestBO() { Document = new DocumentBO() };
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }



        public ActionResult InsertDistribute(DocumentDistributeBO data, string DepartmentId= default(string))
        {
            var result = false;
          
                try
                {
                    if (DepartmentId != null)
                    {
                        if (DepartmentId.Contains('_'))
                        {
                            string[] DepartmentIdsl = DepartmentId.Trim().Split('_');
                            DepartmentId = DepartmentIdsl[1];
                            data.DepartmentId = iDAS.Service.Common.Utilities.ConvertToGuid(DepartmentId);
                        }
                    }
                    DocumentDistributeService.InsertDistribute(data);
                    result = true;
                    this.ShowNotify(Common.Resource.SystemSuccess);
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            
            return this.Direct(result: result);
        }

        /// <summary>
        /// Tạo đề nghị phân phối tài liệu
        /// </summary>
        /// <param name="adjustSuggest"></param>
        /// <returns></returns>
        public ActionResult Create(DocumentDistributeBO distribute)
        {
            object result = null;
            try
            {
             //   distribute.ReceiveBy = distribute.EmployeeReceive.Id;
                distribute.Id = DocumentDistributeService.Insert(distribute);
                result = new { success = true, data = distribute.Id, message = Common.Resource.SystemSuccess };
            }
            catch (Exception)
            {
                result = new { success = false, message = Common.Resource.SystemFailure };
            }
            return this.Direct(result);
        }
    }
}