﻿using Ext.Net;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Main.Controllers
{
    public class DashBoardController : Controller
    {
        //
        // GET: /Main/Dashboard/
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                WrapByScriptTag = false
            };
        }
    }
}