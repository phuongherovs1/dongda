﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.KPI.Controllers
{
    public class KPITaskSourceController : FrontController
    {
        // GET: KPI/KPITaskCategorie
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        public ActionResult Form(Guid id)
        {
            var model = new TaskSourceBO();
            if (id != Guid.Empty)
                model = TaskSourceService.GetById(id);
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = model };
        }
        public ActionResult GetDataTaskSource(StoreRequestParameters param, string filterName = default)
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var data = TaskSourceService.GetAll(pageIndex, pageSize, out int count);
            if (!string.IsNullOrEmpty(filterName))
                data = TaskSourceService.GetByfilterName(pageIndex, pageSize, filterName, out count);
            var result = from item in data
                         select new
                         {
                             item.Id,
                             item.Name,
                             item.Content,
                             item.CreateBy,
                             CreateByString = item.CreateBy.HasValue ? EmployeeService.GetById((Guid)item.CreateBy).Name : "",
                             item.CreateAt,
                             HasJob = TaskService.Get(i => i.TaskSourceId == item.Id).Count() > 0
                         };
            return this.Store(result, count);
        }
        public ActionResult GetGrid(string id)
        {
            List<TaskBO> data = new List<TaskBO>();
            data = TaskService.Get(x => x.TaskSourceId.HasValue && x.TaskSourceId.Value.ToString() == id).ToList();
            return this.Store(data);
        }
        public ActionResult Delete(Guid id)
        {
            try
            {
                TaskSourceService.Delete(id);
                return this.Direct(result: true);
            }
            catch
            {
                return this.Direct(result: false);
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(TaskSourceBO asset)
        {
            var result = false;
            try
            {
                TaskSourceService.Update(asset);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
    }
}