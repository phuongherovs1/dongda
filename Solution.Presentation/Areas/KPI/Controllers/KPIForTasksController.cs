﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.KPI.Controllers
{
    public class KPIForTasksController : FrontController
    {
        public ActionResult Index()
        {
            ViewData["lstYear"] = iDAS.Service.Common.Resource.Task.GetYear();
            return PartialView();
        }
        // GET: KPI/KPIForTasks
        public ActionResult Point(Guid taskId)
        {
            ViewData["TaskID"] = taskId;
            TaskBO task = TaskService.GetById(taskId);
            if (task.KPICatagoryId.HasValue)
            {
                var item = KPICategoryService.GetById(task.KPICatagoryId.Value);
                var currentEmployee = EmployeeService.GetUserCurrent();
                ViewData["MaxScore"] = item.Maxscore.HasValue ? item.Maxscore.Value : 0;
            }
            else
                ViewData["MaxScore"] = 0;
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        public ActionResult NotifyPointEmployee(Guid id, Guid taskId)
        {

            KPIForTaskBO model = KPIForTaskService.GetById(id);
            var task = TaskService.GetById(taskId);
            model.Employee = EmployeeService.GetById(model.CreateBy.Value);
            model.Employee.RoleNames = string.Join(", ", EmployeeService.GetRoleNames(model.Employee.Id));
            model.TaskName = task.Name;
            if (task.KPICatagoryId.HasValue)
                model.MaxScore = KPICategoryService.GetById(task.KPICatagoryId.Value).Maxscore;
            //tính tổng điểm trong tháng tại tháng Notify được tạo gửi
            var employeeCurrent = EmployeeService.GetUserCurrent();
            DateTime DateFrom = new DateTime(model.CreateAt.Value.Year, model.CreateAt.Value.Month, 1);
            DateTime DateTo = new DateTime(model.CreateAt.Value.Year, model.CreateAt.Value.Month, DateTime.DaysInMonth(model.CreateAt.Value.Year, model.CreateAt.Value.Month));
            var listKPIForTask = KPIForTaskService.Get(x => x.CreateAt.Value.Date.Subtract(DateFrom.Date).Days >= 0 && x.CreateAt.Value.Date.Subtract(DateTo.Date).Days <= 0 && x.IsDelete == false && x.EmployeeId.Value.ToString() == employeeCurrent.Id.ToString());
            foreach (KPIForTaskBO item in listKPIForTask)
            {
                if (model.SumScore.HasValue)
                    model.SumScore += item.Score.Value * item.Weightscore.Value;
                else
                    model.SumScore = item.Score.Value * item.Weightscore.Value;
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult SendPointAdmin()
        {
            var claim = (ClaimsIdentity)User.Identity;
            var userId = claim.FindFirst(ClaimTypes.UserData).Value;
            if (string.IsNullOrEmpty(userId))
                userId = Guid.Empty.ToString();
            List<Guid> lstIDIsparent = new List<Guid>();
            lstIDIsparent = Common.DBConnect.GetLstIDIsParent(userId.ToString());
            if (lstIDIsparent.Count > 0)
                ViewData["lstIDIsparent"] = string.Join(",", lstIDIsparent);
            else
                ViewData["lstIDIsparent"] = "";
            ViewData["LstPermission"] = "," + Common.DBConnect.GetLstPermission(userId.ToString()) + ",";
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        public ActionResult GetDataScoreOfEmployee(DateTime DateFrom, DateTime DateTo, StoreRequestParameters parameter, string DepartmentId, string filterName = default(string))
        {
            try
            {
                var count = 0;
                var IdDep = Guid.Empty;
                if (!string.IsNullOrEmpty(DepartmentId))
                    IdDep = new Guid(DepartmentId.Split('_')[1]);
                var data = KPIForTaskService.GetDataScoreOfEmployee(DateFrom, DateTo, IdDep, parameter.Page, parameter.Limit, out count, filterName);
                return this.Store(data, count);
            }
            catch
            {
                return this.Store(null);
            }
        }
        public ActionResult GetDataScoreOfEmployeeMark(DateTime DateFrom, DateTime DateTo, Guid? EmployeeId)
        {
            try
            {
                var data = KPIForTaskService.GetDataScoreOfEmployeeMark(DateFrom, DateTo, EmployeeId);
                return this.Store(data);
            }
            catch
            {
                return this.Store(null);
            }
        }
        public ActionResult LoadData(Guid taskId, StoreRequestParameters parameter)
        {
            try
            {
                //Lấy hệ số mặc định theo chức danh người đăng nhập
                var employee = EmployeeService.GetUserCurrent();
                var listTitleID = EmployeeTitleService.Get(x => x.EmployeeId == employee.Id).Select(x => x.TitleId);
                var count = 0;
                var data = TaskPerformService.GetAll(taskId, parameter.Page, parameter.Limit, out count);
                var currentEmployee = EmployeeService.GetUserCurrent();
                var KPIForTasks = KPIForTaskService.GetAll();
                foreach (TaskPerformBO item in data)
                {
                    var KPIForTaskitem = KPIForTasks.FirstOrDefault(i => i.CreateBy.Value == currentEmployee.Id && i.TaskId.Value == taskId && i.EmployeeId.Value == item.EmployeeId);
                    if (KPIForTaskitem != null)
                    {
                        item.Score = KPIForTaskitem.Score;
                        item.Weightscore = KPIForTaskitem.Weightscore;
                    }
                }
                return this.Store(data, count);
            }
            catch
            {
                return this.Store(null);
            }
        }
        public ActionResult GetDataToSendPointAdmin(StoreRequestParameters parameter, bool IsParent, string filter = default(string), string TaskIdSearch = default(string))
        {
            try
            {

                var data = KPIForTaskService.GetDataToSendPointAdmin(IsParent, filter, TaskIdSearch);
                int count = data.ToList().Count;
                data = TaskPerformService.Page(data, parameter.Page, parameter.Limit);
                return this.Store(data, count);
            }
            catch (Exception e)
            {
                return this.Store(null);
            }
        }
        [HttpPost]
        public ActionResult SendPoint(Guid TaskId, string KPIForTasks)
        {
            var message = default(string);
            var employee = EmployeeService.GetUserCurrent();
            List<KPIForTaskItem> ForTaskItem = new List<KPIForTaskItem>();
            ForTaskItem = JSON.Deserialize<List<KPIForTaskItem>>(KPIForTasks);
            var taskitem = TaskService.GetById(TaskId);
            foreach (KPIForTaskItem data in ForTaskItem)
            {
                KPIForTaskBO item = new KPIForTaskBO();
                item.TaskId = TaskId;
                item.EmployeeId = data.EmployeeId;
                item.EmployeeName = data.EmployeeName;
                item.Score = data.Score;
                item.Score_Value = data.Score_Value;
                item.Score_Time = data.Score_Time;
                item.Score_Plus = data.Score_Plus;
                item.Score_Minus = data.Score_Minus;
                item.Note = data.Note;

                int sum = 0;
                if (data.Score_Value.HasValue)
                    sum += data.Score_Value.Value;
                if (data.Score_Time.HasValue)
                    sum += data.Score_Time.Value;
                if (data.Score_Plus.HasValue)
                    sum += data.Score_Plus.Value;
                if (data.Score_Minus.HasValue)
                    sum -= data.Score_Minus.Value;

                item.Attitude1 = data.Attitude1;
                item.Attitude2 = data.Attitude2;
                item.Attitude3 = data.Attitude3;
                item.Attitude4 = data.Attitude4;
                item.AttitudePlus = data.AttitudePlus;
                item.AttitudeMinus = data.AttitudeMinus;

                int sumAttitude = 0;
                if (data.Attitude1.HasValue)
                    sumAttitude += data.Attitude1.Value;
                if (data.Attitude2.HasValue)
                    sumAttitude += data.Attitude2.Value;
                if (data.Attitude3.HasValue)
                    sumAttitude += data.Attitude3.Value;
                if (data.Attitude4.HasValue)
                    sumAttitude += data.Attitude4.Value;
                if (data.AttitudePlus.HasValue)
                    sumAttitude += data.AttitudePlus.Value;
                if (data.AttitudeMinus.HasValue)
                    sumAttitude -= data.AttitudeMinus.Value;
                item.Score_Attitude = sumAttitude;
                sum += sumAttitude;

                item.Manager = data.Manager;
                item.ManagerPlus = data.ManagerPlus;
                item.ManagerMinus = data.ManagerMinus;
                if (data.Manager.HasValue)
                    sum += data.Manager.Value;
                if (data.ManagerPlus.HasValue)
                    sum += data.ManagerPlus.Value;
                if (data.ManagerMinus.HasValue)
                    sum -= data.ManagerMinus.Value;

                var existCode = KPIForTaskService.Get(i => i.TaskId.Value.ToString() == item.TaskId.Value.ToString() && i.EmployeeId.Value.ToString() == item.EmployeeId.Value.ToString() && i.IsDelete == false);
                if (existCode != null && existCode.ToList().Count > 0)
                {
                    item.Id = existCode.ToList()[0].Id;
                    if (employee.Id.ToString() == item.EmployeeId.ToString())
                    {
                        item.Score_Value = existCode.ToList()[0].Score_Value;
                        item.Score_Time = existCode.ToList()[0].Score_Time;
                        item.Score_Plus = existCode.ToList()[0].Score_Plus;
                        item.Score_Minus = existCode.ToList()[0].Score_Minus;
                        item.Note = existCode.ToList()[0].Note;
                        item.Attitude1 = existCode.ToList()[0].Attitude1;
                        item.Attitude2 = existCode.ToList()[0].Attitude2;
                        item.Attitude3 = existCode.ToList()[0].Attitude3;
                        item.Attitude4 = existCode.ToList()[0].Attitude4;
                        item.AttitudePlus = existCode.ToList()[0].AttitudePlus;
                        item.AttitudeMinus = existCode.ToList()[0].AttitudeMinus;

                        item.Manager = existCode.ToList()[0].Manager;
                        item.ManagerPlus = existCode.ToList()[0].ManagerPlus;
                        item.ManagerMinus = existCode.ToList()[0].ManagerMinus;
                    }
                    KPIForTaskService.Update(item, true);
                }
                else
                {
                    item.Id = KPIForTaskService.Insert(item, true);
                    if (employee.Id.ToString() != item.EmployeeId.ToString())
                        Notify(TitleResourceNotifies.SendPointKPI, "Bạn nhận được " + sum + " điểm từ công việc \"" + taskitem.Name + "\"", item.EmployeeId.Value, UrlResourceNotifies.SendPointKPI, new { id = item.Id, taskId = TaskId });
                }
            }
            return this.DirectFormat(message);
        }
        [HttpPost]
        public ActionResult RemovePoint(Guid taskId, Guid employeeId)
        {
            var message = default(string);
            var employee = EmployeeService.GetUserCurrent();
            var existItem = KPIForTaskService.Get(i => i.CreateBy.Value == employee.Id && i.TaskId.Value.ToString() == taskId.ToString() && i.EmployeeId.Value.ToString() == employeeId.ToString() && i.IsDelete == false);
            if (existItem != null)
                KPIForTaskService.Delete(existItem.ToList()[0].Id, true);
            return this.DirectFormat(message);
        }
    }
    public class KPIForTaskItem
    {
        public string EmployeeName { get; set; }

        public string Note { get; set; }

        public Guid? EmployeeId { get; set; }

        public int? Score { get; set; }

        //Điểm theo chất lượng
        public int? Score_Value { get; set; }
        //Điểm theo thời gian
        public int? Score_Time { get; set; }
        //Điểm cộng
        public int? Score_Plus { get; set; }
        //Điểm trừ
        public int? Score_Minus { get; set; }
        //Điểm thái độ
        public int? Score_Attitude { get; set; }

        public int? Weightscore { get; set; }

        public int? Attitude1 { get; set; }
        public int? Attitude2 { get; set; }
        public int? Attitude3 { get; set; }
        public int? Attitude4 { get; set; }
        public int? AttitudePlus { get; set; }
        public int? AttitudeMinus { get; set; }
        public int? Manager { get; set; }
        public int? ManagerPlus { get; set; }
        public int? ManagerMinus { get; set; }
    }
}