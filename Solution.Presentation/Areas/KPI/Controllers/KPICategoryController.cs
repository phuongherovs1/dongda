﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.KPI.Controllers
{
    public class KPICategoryController : FrontController
    {
        #region view
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }
        public ActionResult Form(string id = "", bool allowDelete = false)
        {
            KPICategoryBO model;
            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = KPICategoryService.GetFormItem();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = KPICategoryService.GetById(new Guid(id));
                if (model.ParrentId.HasValue)
                    model.ParrentText = KPICategoryService.GetById(model.ParrentId.Value).Name;
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult FormAuthorization(string id = "")
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                ViewData["departmentName"] = department.Name;
                ViewData["departmentId"] = department.Id;
            }
            ViewData.Add("Id", id);
            string Type = "";
            var model = KPICategoryService.GetById(new Guid(id));
            if (model != null && model.Type.HasValue)
                Type = model.Type.Value.ToString();
            ViewData.Add("Type", Type);
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        #endregion
        #region Base
        public ActionResult Create(KPICategoryBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    KPICategoryService.Insert(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(KPICategoryBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    KPICategoryService.Update(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    KPICategoryService.Delete(id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
        #region Rule
        public ActionResult GetDataLevel(StoreRequestParameters param, int level, Guid? id, string filterName = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = KPICategoryService.GetByfilterName(pageIndex, pageSize, level, id, filterName, out count);
            return this.Store(data, count);
        }
        public ActionResult GetDataParrent(StoreRequestParameters param)
        {
            var data = KPICategoryService.GetAll();
            return this.Store(data);
        }
        private Node createNodeDepartment(KPICategoryBO department, bool showCheckBox = false)
        {
            Node node = new Node();
            node.NodeID = department.Id.ToString();
            node.Text = " " + department.Name;
            node.IconCls = "";
            if (showCheckBox)
                node.Checked = false;
            if (department.Maxscore.HasValue)
                node.CustomAttributes.Add(new ConfigItem { Name = "Maxscore", Value = department.Maxscore.Value.ToString(), Mode = ParameterMode.Value });
            node.CustomAttributes.Add(new ConfigItem { Name = "Note", Value = department.Note, Mode = ParameterMode.Value });
            node.CustomAttributes.Add(new ConfigItem { Name = "Description", Value = department.Description, Mode = ParameterMode.Value });
            node.Leaf = !department.IsParent;
            return node;
        }
        public ActionResult LoadDataTree(string node, bool showCheckBox = false, string filterName = default(string))
        {
            try
            {
                NodeCollection nodes = new NodeCollection();
                var departmentID = node == "root" ? (Guid?)null : new Guid(Convert.ToString(node));
                var departments = KPICategoryService.GetTreeDepartment(departmentID, filterName);
                departments.OrderByDescending(x => x.Name);
                foreach (var department in departments)
                {
                    nodes.Add(createNodeDepartment(department, showCheckBox));
                }
                return this.Content(nodes.ToJson());
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }

        public ActionResult ApproveAuthorization(string Id, string kpiid, bool IsPermission, string Type)
        {
            string store = "Insert_KPIEmployee_Config";
            if (IsPermission == false)
                store = "Delete_KPIEmployee_Config";
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = store;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@EmployeeId", Id);
                    if (IsPermission == true)
                        cmd.Parameters.AddWithValue("@KPIId", kpiid);
                    else
                        cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.ExecuteNonQuery();
                }
            }
            return this.Direct();
        }
        public ActionResult LoadData(StoreRequestParameters parameter, string Type, string query = default(string), bool isAll = true, string departmentId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                if (isAll)
                {
                    var data = EmployeeService.GetAllHasRole(parameter.Page, parameter.Limit, out count, query: query, allowGetRoleName: true);
                    foreach (EmployeeBO item in data)
                    {
                        item.IsPermission = Common.DBConnect.CheckPermissionKPI(Type, item.Id.ToString());
                    }
                    return this.Store(data, count);
                }
                else
                {
                    if (!string.IsNullOrEmpty(departmentId))
                    {
                        var data = EmployeeService.GetByDepartment(parameter.Page, parameter.Limit, out count, new Guid(departmentId), query, allowGetRoleName: true);
                        foreach (EmployeeBO item in data)
                        {
                            item.IsPermission = Common.DBConnect.CheckPermissionKPI(Type, item.Id.ToString());
                        }
                        return this.Store(data, count);
                    }
                    else
                    {
                        var data = EmployeeService.GetAllHasRole(parameter.Page, parameter.Limit, out count, query: query, allowGetRoleName: true);
                        foreach (EmployeeBO item in data)
                        {
                            item.IsPermission = Common.DBConnect.CheckPermissionKPI(Type, item.Id.ToString());
                        }
                        return this.Store(data, count);
                    }
                }
            }
            catch
            {
                return this.Store(null);
            }
        }
        #endregion
    }
}