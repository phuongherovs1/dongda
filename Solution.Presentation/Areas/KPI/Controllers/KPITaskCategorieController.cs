﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.KPI.Controllers
{
    public class KPITaskCategorieController : FrontController
    {
        // GET: KPI/KPITaskCategorie
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        public ActionResult Form(string id = "")
        {
            ViewData["DepartmentIDCategory"] = TaskCategoryService.GetDepartmentByUser();
            var model = new TaskCategoryBO();
            if (!string.IsNullOrEmpty(id) && new Guid(id) != Guid.Empty)
                model = TaskCategoryService.GetById(new Guid(id));
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData, Model = model };
        }
        public ActionResult GetDataTaskcategory(StoreRequestParameters param, string filterName = default(string), string DepartmentId = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = TaskCategoryService.GetAll(pageIndex, pageSize, out count);
            Guid deId = Guid.Empty;
            if (!string.IsNullOrEmpty(DepartmentId))
                deId = Guid.Parse(DepartmentId.Split('_')[1].ToString());
            if (!string.IsNullOrEmpty(filterName) || !string.IsNullOrEmpty(DepartmentId))
                data = TaskCategoryService.GetByfilterName(pageIndex, pageSize, filterName, deId, out count);
            return this.Store(data, count);
        }
        public ActionResult GetGrid(string id)
        {
            List<TaskBO> data = new List<TaskBO>();
            data = TaskService.Get(x => x.TaskCategoryId.HasValue && x.TaskCategoryId.Value.ToString() == id).ToList();
            return this.Store(data);
        }
        public ActionResult Delete(Guid id)
        {
            try
            {
                TaskCategoryService.Delete(id);
                return this.Direct(result: true);
            }
            catch
            {
                return this.Direct(result: false);
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(TaskCategoryBO asset)
        {
            var result = false;
            try
            {
                if(asset.Id != Guid.Empty)
                    TaskCategoryService.Update(asset);
                else
                    TaskCategoryService.Insert(asset);
                result = true;
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
    }
    public class Result
    {
        public string name { get; set; }
        public double percent { get; set; }
        public int value { get; set; }
    }
}