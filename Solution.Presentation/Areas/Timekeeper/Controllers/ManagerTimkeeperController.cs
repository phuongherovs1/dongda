﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service.API.Timekeeper;
using iDAS.Service.POCO.Timekeeper;
using System;
using System.Globalization;
using System.Text;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Timekeeper.Controllers
{
    public class ManagerTimkeeperController : FrontController
    {
        protected IManagerTimkeeperService managerTimkeeperService { set; get; }
        public ManagerTimkeeperController(IManagerTimkeeperService _managerTimkeeperService)
        {
            managerTimkeeperService = _managerTimkeeperService;
        }
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }
        private static Ext.Net.MVC.PartialViewResult FormCreate { set; get; }
        public ActionResult Form(string id = default(string))
        {
            TimekeeperBO model;
            if (id == default(string))
            {
                if (FormCreate == null)
                {
                    model = new TimekeeperBO();
                    ViewData.Add("Operation", Common.Operation.Create);
                    FormCreate = new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
                    return FormCreate;

                }
                else
                {
                    return FormCreate;
                }

            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = managerTimkeeperService.SelectTimkeeper(Convert.ToInt32(id));
                return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
            }
        }
        public ActionResult Create(TimekeeperBO item)
        {
            return this.Direct(result: managerTimkeeperService.CreateTimkeeper(item));
        }
        public ActionResult Update(TimekeeperBO item)
        {
            return this.Direct(result: managerTimkeeperService.UpdateTimkeeper(item));
        }
        public ActionResult Delete(string id = default(string))
        {
            return this.Direct(result: managerTimkeeperService.DeleteTimkeeper(Convert.ToInt32(id)));
        }
        public ActionResult Loaddata()
        {
            return this.Store(managerTimkeeperService.SelectTimkeeper());
        }
        public ActionResult Inputdatalog(int idTimkeeper, string IP, int Post)
        {
            string error;
            return this.Store(managerTimkeeperService.InputTimekeeping(idTimkeeper, IP, Post, out error));
        }
        protected void ExportExcel(string filename, string data)
        {
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + filename + ".xls");
            StringBuilder TableHTML = new StringBuilder(@"<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x ='urn:schemas-microsoft-com:office:excel'xmlns ='http://www.w3.org/TR/REC-html40'>
            <head>
              <!--[if gte mso 9]>
                    <xml>
                     <x:ExcelWorkbook>
                       <x:ExcelWorksheets>
                          <x:ExcelWorksheet>
                             <x:Name>Bang cham cong</x:Name>
                                 <x:WorksheetOptions>
                                   <x:DisplayGridlines/>
                                   </x:WorksheetOptions>
                                  </x:ExcelWorksheet>
                                 </x:ExcelWorksheets>
                                 </x:ExcelWorkbook>
                                 </xml>
                              <![endif]-->
                          </head>
                          <body>");
            TableHTML.Append(data);
            TableHTML.Append("</body></html>");
            Response.Write(TableHTML.ToString());
            Response.End();
        }
        public void Export(string date)
        {
            DateTime todate = DateTime.ParseExact("22/" + date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime fromdate = DateTime.ParseExact("23/" + todate.AddMonths(-1).ToString("MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            ExportExcel("Export", managerTimkeeperService.GetdataExpost(fromdate, todate));
        }
    }
}