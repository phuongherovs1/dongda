﻿using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Timekeeper
{
    public class TimekeeperAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Timekeeper";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Timekeeper_default",
                "Timekeeper/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}