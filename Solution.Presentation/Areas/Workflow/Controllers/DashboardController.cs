﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Workflow.Controllers
{
    public class DashboardController : FrontController
    {
        ////
        // GET: /Workflow/Dashboard/
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
            };
        }  
        public ActionResult StopProcess(string documentId, bool isstop)
        {
            var result = false;
            ISOProcessBO data = new ISOProcessBO();
            data.Id = new Guid(documentId);
            data.IsStop = isstop;
            try
            {
                ISOProcessService.Update(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                {
                    Common.WriteLog.WriteLogEntry("Create - Workflow", ex);
                    this.ShowNotify(Common.Resource.SystemFailure);
                }

            }
;           return this.Direct(result: result);
        }

    }
}