﻿using iDAS.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;

namespace iDAS.Presentation.Areas.Workflow.Controllers
{
    public class HomeController : FrontController
    {

        // GET: /Workflow/Home/
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                WrapByScriptTag = false
            };
        }

        public ActionResult SelectRole()
        {
            return new Ext.Net.MVC.PartialViewResult { };
        }

        public ActionResult GetData()
        {
            return this.Store(null);
        }

    }
}