﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;

namespace iDAS.Presentation.Areas.Workflow.Controllers
{
    public class WorkflowController : FrontController
    {
        //
        // GET: /Workflow/Workflow/
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,

                ViewName = "Index",
                WrapByScriptTag = false
            };
        }

        public ActionResult GetData(Guid? departmentId)
        {
            var data = ISOProcessService.GetByDepartment(departmentId).ToList();
            return this.Store(data);
        }

        public ActionResult GetDepartment()
        {
            var data = DepartmentService.GetAll().ToList();
            return this.Store(data);
        }

        public ActionResult Form(string id)
        {
            var model = ISOProcessService.GetFormItem(id);
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }

        public ActionResult Create(ISOProcessBO data)
        {

            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ISOProcessService.Insert(data);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                    {
                        Common.WriteLog.WriteLogEntry("Create - Workflow", ex);
                        this.ShowNotify(Common.Resource.SystemFailure);
                    }

                }
            }
            return this.Direct(result: result);
        }

        public ActionResult Update(ISOProcessBO data)
        {

            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ISOProcessService.Update(data);
                    result = false;
                }
                catch (Exception ex)
                {
                    if (ex is ArgumentException)
                        this.ShowNotify(ex.Message);
                    else
                    {
                        Common.WriteLog.WriteLogEntry("Create - Workflow", ex);
                        this.ShowNotify(Common.Resource.SystemFailure);
                    }

                }
            }
            return this.Direct(result: result);
        }

        public ActionResult Design(Guid id)
        {
            try
            {
                var model = ISOProcessService.GetGraphData(id);
                return new Ext.Net.MVC.PartialViewResult { Model = model };
            }
            catch(Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
                return this.Direct();
            }
        }

        public ActionResult GetDataRelation(StoreRequestParameters parameter, string StepId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                var data = ISOProcessRelationshipService.GetDataRelationship(pageIndex, pageSize, out count, iDAS.Service.Common.Utilities.ConvertToGuid(StepId)).ToList();
                return this.Store(data, count);
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }


        public ActionResult Save(ISOProcessBO data)
        {
            var result = false;
            try
            {
                ISOProcessStepService.Save(data);
                result = true;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                if (ex is iDAS.Service.Common.DataHasBeenExistedException || ex is ArgumentException)
                    this.ShowNotify(ex.Message);
                else
                    this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

    }
}