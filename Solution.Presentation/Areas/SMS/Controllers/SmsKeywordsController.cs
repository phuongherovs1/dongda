﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.SMS.Controllers
{
    public class SmsKeywordsController : FrontController
    {
        public ActionResult Index()
        {
            SetViewDataGroupKeyword();
            return PartialView();
        }
        private void SetViewDataGroupKeyword()
        {
            var dataGroupKeyword = SmsKeywordCategoriesService.GetAll().ToList();
            var dic = new Dictionary<string, string>();
            dic.Add(string.Empty, "Tất cả");
            for (int i = 0; i < dataGroupKeyword.Count; i++)
                dic.Add(dataGroupKeyword[i].Id.ToString(), dataGroupKeyword[i].Name);
            ViewData["dataGroupKeyword"] = dic;
        }
        public ActionResult GetDataKeywords(StoreRequestParameters param, string filterName = default(string), string keywordCategoryId = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = SmsKeywordsService.GetAll(pageIndex, pageSize, out count);
            if (!string.IsNullOrEmpty(filterName) || !string.IsNullOrEmpty(keywordCategoryId))
                data = SmsKeywordsService.GetByfilterName(pageIndex, pageSize, filterName, !string.IsNullOrEmpty(keywordCategoryId) ? Guid.Parse(keywordCategoryId) : Guid.Empty, out count);
            return this.Store(data, count);
        }
        public ActionResult Form(string id = "")
        {
            var selectReview = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetLogicReview());
            var selectObject = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetObjectRelative());
            ViewData.Add("SelectReview", selectReview);
            ViewData.Add("SelectObject", selectObject);
            SmsKeywordsBO model;
            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = SmsKeywordsService.GetFormItem();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = SmsKeywordsService.GetById(new Guid(id));
            }
            SetViewDataGroupKeyword();
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(SmsKeywordsBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsKeywordsService.Insert(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            SetViewDataGroupKeyword();
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(SmsKeywordsBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsKeywordsService.Update(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            SetViewDataGroupKeyword();
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsKeywordsService.Delete(id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            SetViewDataGroupKeyword();
            return this.Direct(result: result);
        }
    }
}