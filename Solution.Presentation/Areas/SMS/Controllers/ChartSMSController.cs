﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.SMS.Controllers
{
    public class ChartSMSController : FrontController
    {
        //public ActionResult Index()
        //{
        //    return PartialView();
        //}
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }
        public ActionResult ChartSMS()
        {
            return PartialView();
        }
        public ActionResult TableViewSMS()
        {
            return PartialView();
        }
        public StoreResult GetDataPie(DateTime DateFrom, DateTime DateTo)
        {
            var lstsms = SmsManagersSynchronizedService.Get(x => x.SendDate.Value.Subtract(DateFrom.Date).Hours >= 0 && x.SendDate.Value.Subtract(DateTo.Date).Hours < 0);
            List<Result> results1 = (
                from p in lstsms
                group p by p.NetworkProvider into g
                select new Result()
                {
                    name = g.Key,
                    value = g.ToList().Count
                }
                ).ToList();
            results1.Cast<object>().ToArray();
            return new StoreResult(results1);
        }
        public StoreResult GetDataCol(DateTime DateFrom, DateTime DateTo)
        {
            List<Result> lstResult = new List<Result>();
            var lstsms = SmsKeywordCategoriesService.GetAll();
            foreach (SmsKeywordCategoriesBO item in lstsms)
            {
                Result results1 = new Result();
                results1.name = item.Name;
                List<Guid> lstGuid = SmsTypeService.Get(x => x.IdType == item.Id).Select(x => x.IdSms.Value).ToList();
                results1.value = SmsManagersSynchronizedService.GetByIds(lstGuid).Where(x => x.SendDate.Value.Subtract(DateFrom.Date).Hours >= 0 && x.SendDate.Value.Subtract(DateTo.Date).Hours < 0).ToList().Count;
                lstResult.Add(results1);
            }
            var data = lstResult.OrderByDescending(x => x.value).Take(5).Cast<object>().ToArray();
            return new StoreResult(data);
        }
        public ActionResult GetDataNetwordProvider(DateTime DateFrom, DateTime DateTo)
        {
            var lstsms = SmsManagersSynchronizedService.Get(x => x.SendDate.Value.Subtract(DateFrom.Date).Hours >= 0 && x.SendDate.Value.Subtract(DateTo.Date).Hours < 0);
            List<Result> results1 = (
                from p in lstsms
                group p by p.NetworkProvider into g
                select new Result()
                {
                    name = g.Key,
                    value = g.ToList().Count
                }
                ).ToList();
            foreach (Result item in results1)
            {
                item.percent = Percent(item.value, results1.Sum(x => x.value));
            }
            return new StoreResult(results1);
        }
        public ActionResult Top15KeywordGroup(DateTime DateFrom, DateTime DateTo)
        {
            List<Result> lstResult = new List<Result>();
            var lstsms = SmsKeywordCategoriesService.GetAll();
            foreach (SmsKeywordCategoriesBO item in lstsms)
            {
                Result results1 = new Result();
                results1.name = item.Name;
                List<Guid> lstGuid = SmsTypeService.Get(x => x.IdType == item.Id).Select(x => x.IdSms.Value).ToList();
                results1.value = SmsManagersSynchronizedService.GetByIds(lstGuid).Where(x => x.SendDate.Value.Subtract(DateFrom.Date).Hours >= 0 && x.SendDate.Value.Subtract(DateTo.Date).Hours < 0).ToList().Count;
                lstResult.Add(results1);
            }
            foreach (Result item in lstResult)
            {
                item.percent = Percent(item.value, lstResult.ToList().Sum(x => x.value));
            }
            var data = lstResult.OrderByDescending(x => x.value).Take(15);
            return new StoreResult(data);
        }
        public ActionResult GetDataSMSChart(DateTime DateFrom, DateTime DateTo)
        {
            List<Result> lstResult = new List<Result>();
            Result dauso = new Result();
            dauso.name = "Đầu số dịch vụ";
            dauso.value = 0;
            Result thuebao = new Result();
            thuebao.name = "Thuê bao";
            thuebao.value = 0;
            Result brandname = new Result();
            brandname.name = "Định danh";
            brandname.value = 0;
            var lstSMS = SmsBlockListService.Get(x => x.SendDate.Value.Subtract(DateFrom.Date).Hours >= 0 && x.SendDate.Value.Subtract(DateTo.Date).Hours < 0);

            string regexThuebao = @"(09[0-9]{8}|\(?84\)?9[0-9]{8}|\(?84\)?1[2|6|8|9]|01[2|6|8|9][0-9]{8})";
            string regexBrandname = @"[a-z]+";

            foreach (SmsBlockListBO item in lstSMS)
            {
                if (Regex.Match(item.SendNumber, regexThuebao, RegexOptions.IgnoreCase).Success)
                    thuebao.value++;
                else if (Regex.Match(item.SendNumber, regexBrandname, RegexOptions.IgnoreCase).Success)
                    brandname.value++;
                else
                    dauso.value++;
            }
            if (dauso.value == 0)
                dauso.percent = 0;
            else
                dauso.percent = Math.Round((double)dauso.value / (double)lstSMS.ToList().Count * 100, 2, MidpointRounding.AwayFromZero);
            lstResult.Add(dauso);

            if (brandname.value == 0)
                brandname.percent = 0;
            else
                brandname.percent = Math.Round((double)brandname.value / (double)lstSMS.ToList().Count * 100, 2, MidpointRounding.AwayFromZero);
            lstResult.Add(brandname);

            if ((brandname.value + dauso.value) == lstSMS.ToList().Count)
                thuebao.percent = 0;
            else
                thuebao.percent = (100 - (brandname.percent + dauso.percent));
            lstResult.Add(thuebao);

            return this.Store(lstResult, lstResult.Count);
        }
        public ActionResult GetDataDauSo(DateTime DateFrom, DateTime DateTo)
        {
            string regexThuebao = @"(09[0-9]{8}|\(?84\)?9[0-9]{8}|\(?84\)?1[2|6|8|9]|01[2|6|8|9][0-9]{8})";
            string regexBrandname = @"[a-z]+";
            List<Result> lstResult = new List<Result>();
            var lstSMS = SmsBlockListService.Get(x => !Regex.Match(x.SendNumber, regexThuebao, RegexOptions.IgnoreCase).Success && !Regex.Match(x.SendNumber, regexBrandname, RegexOptions.IgnoreCase).Success).Where(x => x.SendDate.Value.Subtract(DateFrom.Date).Hours >= 0 && x.SendDate.Value.Subtract(DateTo.Date).Hours < 0);
            lstResult = (
                from p in lstSMS
                group p by p.SendNumber into g
                select new Result()
                {
                    name = g.Key,
                    value = g.ToList().Count
                }
                ).ToList();
            foreach (Result item in lstResult)
            {
                item.percent = Percent(item.value, lstSMS.ToList().Count);
            }
            return this.Store(lstResult, lstResult.Count);
        }
        public ActionResult GetDataThueBao(DateTime DateFrom, DateTime DateTo)
        {
            string regexThuebao = @"(09[0-9]{8}|\(?84\)?9[0-9]{8}|\(?84\)?1[2|6|8|9]|01[2|6|8|9][0-9]{8})";
            List<Result> lstResult = new List<Result>();
            var lstSMS = SmsBlockListService.Get(x => Regex.Match(x.SendNumber, regexThuebao, RegexOptions.IgnoreCase).Success).Where(x => x.SendDate.Value.Subtract(DateFrom.Date).Hours >= 0 && x.SendDate.Value.Subtract(DateTo.Date).Hours < 0);
            lstResult = (
                from p in lstSMS
                group p by p.NetworkProvider into g
                select new Result()
                {
                    name = g.Key,
                    value = g.ToList().Count
                }
                ).ToList();
            foreach (Result item in lstResult)
            {
                item.percent = Percent(item.value, lstSMS.ToList().Count);
            }
            return this.Store(lstResult, lstResult.Count);
        }
        public ActionResult GetDataBrandname(DateTime DateFrom, DateTime DateTo)
        {
            List<Result> lstResult = new List<Result>();
            var lstSMS = SmsBlockListService.Get(x => x.SendDate.Value.Subtract(DateFrom.Date).Hours >= 0 && x.SendDate.Value.Subtract(DateTo.Date).Hours < 0);
            lstResult = (
                from p in lstSMS
                group p by p.SendNumber into g
                select new Result()
                {
                    name = g.Key,
                    value = g.ToList().Count
                }
                ).ToList();
            foreach (Result item in lstResult)
            {
                item.percent = Percent(item.value, lstResult.Count);
            }
            var abc = lstResult.OrderByDescending(x => x.value);
            return this.Store(lstResult.OrderByDescending(x => x.value).Take(10));
        }
        public double Percent(int a, int sum)
        {
            if (sum == 0)
                return 0;
            else
                return Math.Round((double)a / (double)sum * 100, 2, MidpointRounding.AwayFromZero);
        }
    }
    public class Result
    {
        public string name { get; set; }
        public double percent { get; set; }
        public int value { get; set; }
    }
}