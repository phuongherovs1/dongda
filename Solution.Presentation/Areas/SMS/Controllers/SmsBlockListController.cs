﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.SMS.Controllers
{
    public class SmsBlockListController : FrontController
    {
        //public ActionResult Index()
        //{
        //    return PartialView();
        //}
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult CheckBlockList()
        {
            var selectReview = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetLogicReview());
            var selectObject = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetObjectRelative());
            ViewData.Add("SelectReview", selectReview);
            ViewData.Add("SelectObject", selectObject);
            SmsManagersSynchronizedBO model = new SmsManagersSynchronizedBO();
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult SmsHistoryBlock()
        {
            return PartialView();
        }
        public ActionResult Form(string id = "", bool allowDelete = false)
        {
            var selectReview = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetLogicReview());
            var selectObject = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetObjectRelative());
            ViewData.Add("SelectReview", selectReview);
            ViewData.Add("SelectObject", selectObject);
            SmsBlockListBO model;
            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = SmsBlockListService.GetFormItem();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = SmsBlockListService.GetJustById(new Guid(id));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult GetData(StoreRequestParameters param, DateTime DateFrom, DateTime DateTo, string filterName = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = SmsBlockListService.GetByfilterName(pageIndex, pageSize, filterName, DateFrom.Date, DateTo.Date, out count);
            return this.Store(data, count);
        }
        public ActionResult GetDataManagersSynchronized(StoreRequestParameters param, string filterName = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = SmsManagersSynchronizedService.GetAll(pageIndex, pageSize, out count);
            if (!string.IsNullOrEmpty(filterName))
                data = SmsManagersSynchronizedService.GetByfilterName(pageIndex, pageSize, filterName, out count);
            return this.Store(data, count);
        }
        public ActionResult GetDataHistory(StoreRequestParameters param, DateTime DateFrom, DateTime DateTo, string filterName = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = SmsBlockListService.GetAll(pageIndex, pageSize, out count, true);
            if (!string.IsNullOrEmpty(filterName))
                data = SmsBlockListService.GetByfilterName(pageIndex, pageSize, filterName, DateFrom, DateTo, out count, true);
            return this.Store(data, count);
        }

        public ActionResult Create(SmsBlockListBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsBlockListService.Insert(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(SmsBlockListBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsBlockListService.Update(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsBlockListService.Delete(id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }

        private Node createTreeNode(OrganizationTreeBO nodeItem)
        {
            var node = new Node();
            node.NodeID = nodeItem.Id.ToString();
            node.Icon = 0;
            node.Text = " " + nodeItem.Name;
            node.Checked = false;
            node.Leaf = nodeItem.IsLeaf;
            return node;
        }
        public StoreResult LoadTreeData()
        {
            var nodes = new NodeCollection();
            var departments = SmsKeywordCategoriesService.GetByNode();
            foreach (var department in departments)
            {
                nodes.Add(createTreeNode(department));
            }
            return this.Store(nodes);
        }

    }
}