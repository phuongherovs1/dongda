﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Common;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using systemIO = System.IO;
using ThreadingTask = System.Threading.Tasks;

namespace iDAS.Presentation.Areas.SMS.Controllers
{
    public class SmsManagersController : FrontController
    {
        //public ActionResult Index()
        //{
        //    return PartialView();
        //}
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }
        public ActionResult Import()
        {
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Import", ViewData = ViewData };
        }
        public ActionResult Form(string id = "")
        {
            var selectReview = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetLogicReview());
            var selectObject = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetObjectRelative());
            ViewData.Add("SelectReview", selectReview);
            ViewData.Add("SelectObject", selectObject);
            SmsManagersBO model;
            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = SmsManagersService.GetFormItem();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = SmsManagersService.GetById(new Guid(id));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult GetData(StoreRequestParameters param, string filterName = "")
        {
            List<SmsManagersBO> data = new List<SmsManagersBO>();
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            DataTable dataSMSManagers = new DataTable();
            DBConnect dbconnect = new DBConnect();
            DataSet getDataAll;
            getDataAll = dbconnect.myDataset("sp_SmsManagers_GetData", "@filterName", filterName, "@pageIndex", pageIndex, "@pageSize", pageSize);
            dataSMSManagers = getDataAll.Tables[0];
            count = (int)getDataAll.Tables[1].Rows[0][0];
            for (int i = 0; i < dataSMSManagers.Rows.Count; i++)
            {
                SmsManagersBO item = new SmsManagersBO();
                item.Id = new Guid(dataSMSManagers.Rows[i]["Id"].ToString());
                item.SendNumber = dataSMSManagers.Rows[i]["SendNumber"].ToString();
                item.RecieveNumber = dataSMSManagers.Rows[i]["RecieveNumber"].ToString();
                item.MessageContent = dataSMSManagers.Rows[i]["MessageContent"].ToString();
                item.NetworkProvider = dataSMSManagers.Rows[i]["NetworkProvider"].ToString();
                item.SendDate = Convert.ToDateTime(dataSMSManagers.Rows[i]["SendDate"]);
                if (!string.IsNullOrEmpty(dataSMSManagers.Rows[i]["SynchronizedDate"].ToString()))
                    item.SynchronizedDate = Convert.ToDateTime(dataSMSManagers.Rows[i]["SynchronizedDate"]);
                item.IsSynchronized = !string.IsNullOrEmpty(dataSMSManagers.Rows[i]["IsSynchronized"].ToString()) ? Convert.ToBoolean(dataSMSManagers.Rows[i]["IsSynchronized"]) : false;
                data.Add(item);
            }
            return this.Store(data, count);
        }

        public ActionResult Create(SmsManagersBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsManagersService.Insert(asset, dataPropertySettings, dataResourceRelates);
                    var lstSmsSysn = SmsManagersSynchronizedService.Get(x => x.SendNumber == asset.SendNumber).ToList();
                    if (lstSmsSysn != null && lstSmsSysn.Count > 0)
                    {
                        var index = lstSmsSysn.FindIndex(x => x.MessageContent != asset.MessageContent);
                        if (index > -1)
                        {
                            SmsManagersSynchronizedBO itemUpdate = lstSmsSysn[index];
                            itemUpdate.MessageContent += "#@#" + asset.MessageContent;
                            SmsManagersSynchronizedService.Update(itemUpdate);
                        }
                    }
                    else
                        SmsManagersSynchronizedService.Insert(new SmsManagersSynchronizedBO()
                        {
                            Id = Guid.NewGuid(),
                            SendNumber = asset.SendNumber,
                            RecieveNumber = asset.RecieveNumber,
                            SendDate = asset.SendDate,
                            MessageContent = asset.MessageContent,
                            NetworkProvider = asset.NetworkProvider
                        });
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(SmsManagersBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsManagersService.Update(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsManagersService.Delete(id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #region Import file
        private DataTable ReadFileExcel(string directory)
        {
            FileStream fStream = new FileStream(directory, FileMode.Open);
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook(fStream);
            Aspose.Cells.Worksheet worksheet = workbook.Worksheets[0];
            DataTable datatable = worksheet.Cells.ExportDataTable(0, 0, worksheet.Cells.Rows.Count, 4, false);
            datatable = worksheet.Cells.ExportDataTable(1, 0, worksheet.Cells.Rows.Count - 1, 4, false);
            //Sort column SendDate
            datatable.DefaultView.Sort = "Column1 asc";
            datatable = datatable.DefaultView.ToTable(true);
            fStream.Close();
            systemIO.File.Delete(directory);
            return datatable;

        }
        private DataTable ReadFileCSV(string directory)
        {
            DataTable dataTable = new DataTable();

            try
            {
                StreamReader reader = new StreamReader(directory);
                CsvReader csvReader = new CsvReader(reader, false, ',', 200000);
                for (int i = 1; i < 6; i++)
                {
                    dataTable.Columns.Add("Columns" + i);
                }
                bool isheader = true;
                while (csvReader.ReadNextRecord())
                {
                    if (!isheader)
                    {
                        DataRow row = dataTable.NewRow();
                        #region SMS
                        try
                        {
                            row["Columns1"] = csvReader[0].ToString().Replace('"', ' ').Trim(); //sô nhận
                        }
                        catch (Exception ex) { }
                        try
                        {
                            row["Columns2"] = csvReader[1].ToString().Replace('"', ' ').Trim(); //SendNumber
                        }
                        catch (Exception ex) { }
                        try
                        {
                            row["Columns3"] = csvReader[2].ToString().Replace('"', ' ').Trim();//SendDate
                        }
                        catch (Exception ex) { }
                        try
                        {
                            row["Columns4"] = csvReader[3].ToString().Replace('"', ' ').Trim();//MessageContent
                        }
                        catch (Exception ex) { }
                        try
                        {
                            row["Columns5"] = csvReader[4].ToString().Replace('"', ' ').Trim();//NetworkProvider
                        }
                        catch (Exception ex) { }

                        #endregion
                        dataTable.Rows.Add(row);
                    }
                    else
                    {
                        isheader = false;
                    }
                }
                reader.Dispose();
                if (dataTable != null && dataTable.Rows.Count > 0)
                    return dataTable;
                systemIO.File.Delete(directory);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return null;
        }
        public ActionResult DataObjectImport(StoreRequestParameters parameters, string direction)
        {
            var result = new List<object>();

            if (!string.IsNullOrEmpty(direction))
            {
                var table = ReadFileCSV(Server.MapPath(direction));
                for (var i = 1; i < table.Rows.Count; i++)
                {
                    try
                    {
                        var obj = new ExpandoObject() as IDictionary<string, object>;
                        obj.Add("RecieveNumber", table.Rows[i][0]);
                        obj.Add("SendNumber", table.Rows[i][1]);
                        obj.Add("MessageContent", table.Rows[i][2]);
                        obj.Add("NetworkProvider", table.Rows[i][3]);
                        obj.Add("SendDate", table.Rows[i][4]);
                        result.Add(obj);
                    }
                    catch (Exception e) { }
                }

            }
            return this.Store(result);
        }
        public ActionResult SelectImportFile()
        {
            var fileUploadField = X.GetCmp<FileUploadField>("FileImportField");

            var direction = Common.Resource.FileImportUrl + Guid.NewGuid().ToString() + ".csv";
            if (fileUploadField.HasFile)
            {
                if (fileUploadField.PostedFile.ContentLength < 20 * 1024 * 1024 + 1)
                {
                    if (fileUploadField.FileName.Split('.').LastOrDefault().ToUpper().Equals("CSV"))
                    {
                        if (!systemIO.File.Exists(Server.MapPath(direction)))
                        {
                            fileUploadField.PostedFile.SaveAs(Server.MapPath(direction));
                        }
                    }
                    else
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING,
                            Title = "Thông báo",
                            Message = "Hệ thống chỉ hỗ trợ file có đuôi .csv!"
                        });
                        return this.Direct();
                    }
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = "Chỉ cho phép dung lượng import tối đa là 15MB!"
                    });
                    return this.Direct();
                }
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Không có thông tin cho file upload!"
                });
            }
            return this.Direct(direction);
        }
        private bool CheckTitle(DataTable table)
        {
            string a0 = table.Rows[0][0].ToString().ToLower();
            string a1 = table.Rows[0][1].ToString().ToLower();
            string a2 = table.Rows[0][2].ToString().ToLower();
            string a3 = table.Rows[0][3].ToString().ToLower();
            string a123 = table.Rows[0][4].ToString().ToLower();
            string a4 = table.Rows[0][5].ToString().ToLower();
            if (table.Rows[0][0].ToString().ToLower() == "stt"
                && table.Rows[0][1].ToString().ToLower() == "sendnumber"
                && table.Rows[0][2].ToString().ToLower() == "recievenumber"
                && table.Rows[0][3].ToString().ToLower() == "messagecontent"
                && table.Rows[0][4].ToString().ToLower() == "networkprovider"
                && table.Rows[0][5].ToString().ToLower() == "senddate"
            )
                return true;
            return false;
        }
        [HttpPost]
        public ActionResult Synchronized()
        {
            var objectImportSynchronized = new List<SmsManagersSynchronizedBO>();
            Hashtable hashValidation = new Hashtable();
            int timeConfig = 2;
            string config = ConfigTableService.GetByConfigKey("TimeSynchronized");
            if (!string.IsNullOrEmpty(config))
                timeConfig = Int32.Parse(config);
            var lstObj = SmsManagersService.Get(x => x.IsSynchronized == false && x.IsDelete == false).OrderBy(x => x.SendDate);
            if (lstObj.ToList().Count > 0)
            {
                #region Chuẩn hóa SMS
                foreach (SmsManagersBO item in lstObj)
                {
                    var obj = new ExpandoObject() as IDictionary<string, object>;
                    obj.Add("SendNumber", item.SendNumber);
                    obj.Add("RecieveNumber", item.RecieveNumber);
                    obj.Add("MessageContent", item.MessageContent);
                    obj.Add("NetworkProvider", item.NetworkProvider);
                    obj.Add("SendDate", item.SendDate);
                    #region Validate SMS same
                    var objExist = new ExpandoObject() as IDictionary<string, object>;
                    bool isExist = false;
                    string keyHash = "";
                    string sendNum = item.SendNumber;
                    string reNum = item.RecieveNumber;
                    int s = timeConfig;
                    while (s >= 0)
                    {
                        DateTime sendate = DateTime.Parse(item.SendDate.ToString());
                        sendate = sendate.AddSeconds(-s);
                        keyHash = sendNum + "|" + sendate;
                        if (hashValidation != null && hashValidation[keyHash] != null)
                        {
                            isExist = true;
                            objExist = hashValidation[keyHash] as IDictionary<string, object>;
                            break;
                        }
                        else
                        {
                            s--;
                            continue;
                        }
                    }
                    #endregion
                    if (isExist)
                    {
                        string msgContent = objExist["MessageContent"].ToString() + obj["MessageContent"].ToString();
                        obj.Remove("MessageContent");
                        obj.Add("MessageContent", msgContent);
                        hashValidation.Remove(keyHash);
                    }
                    string _key = sendNum + "|" + obj["SendDate"].ToString();
                    hashValidation.Add(_key, obj);
                }
                #endregion
                #region Insert to DB
                var taskInsert = InsertSmsManagersSynchronized(hashValidation);
                var taskUpdate = UpdateSmsManagers(lstObj.ToList());
                ThreadingTask.Task.WaitAll(taskInsert, taskUpdate);
                #endregion
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO,
                    Title = "Thông báo",
                    Message = "Chuẩn hóa dữ liệu thành công.!"
                });
                return this.Direct();
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO,
                    Title = "Thông báo",
                    Message = "Không có dữ liệu chưa được chuân hóa.!"
                });
                return this.Direct();
            }
        }
        private async ThreadingTask.Task<string> InsertSmsManagersSynchronized(Hashtable hashValidation)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = conn;
            var userId = UserService.GetUserCurrent().Id;

            string SQLInsert = "INSERT INTO dbo.SmsManagersSynchronized (Id,SendNumber,RecieveNumber,MessageContent,NetworkProvider,SendDate,IsDelete,CreateAt,CreateBy) VALUES ";
            int countString = SQLInsert.Length;
            foreach (var item in hashValidation.Keys)
            {
                var obj = new ExpandoObject() as IDictionary<string, object>;
                obj = hashValidation[item] as IDictionary<string, object>;
                string query = "('" + Guid.NewGuid() + "','" + obj["SendNumber"].ToString() + "','" + obj["RecieveNumber"].ToString() + "',N'" + obj["MessageContent"].ToString().Replace("'", "''") + "',N'" + obj["NetworkProvider"].ToString().Replace("'", "''") + "','" + DateTime.Parse(obj["SendDate"].ToString()) + "','" + false + "','" + DateTime.Now + "','" + userId + "')";
                if (SQLInsert.Length < 50000)
                {
                    if (SQLInsert.Length == countString)
                        SQLInsert += query;
                    else
                        SQLInsert += "," + query;
                    if (hashValidation.Keys.OfType<object>().Last() == item)
                    {
                        sqlCommand.CommandText = SQLInsert;
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                else
                {
                    try
                    {
                        SQLInsert += "," + query;
                        sqlCommand.CommandText = SQLInsert;
                        sqlCommand.ExecuteNonQuery();
                        SQLInsert = "INSERT INTO dbo.SmsManagersSynchronized (Id,SendNumber,RecieveNumber,MessageContent,NetworkProvider,SendDate,IsDelete,CreateAt,CreateBy) VALUES ";
                    }
                    catch (Exception ex) { }
                }
            }
            conn.Close();
            conn.Dispose();
            return null;
        }
        private async ThreadingTask.Task<string> UpdateSmsManagers(List<SmsManagersBO> lstSMS)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = conn;
            #region Update Smsmanager
            string queryUpdate = "";
            string SQLUpdate = "Update SmsManagers set IsSynchronized = 1,SynchronizedDate = '" + DateTime.Now + "' where id IN ";
            foreach (var item in lstSMS)
            {
                string query = "'" + item.Id + "'";
                if ((SQLUpdate.Length + queryUpdate.Length) < 50000)
                {
                    if (string.IsNullOrEmpty(queryUpdate))
                        queryUpdate = query;
                    else
                        queryUpdate += "," + query;

                    if (lstSMS.Last().Id == item.Id || (lstSMS.FindIndex(x => x.Id == item.Id) >= 1000 && lstSMS.FindIndex(x => x.Id == item.Id) % 1000 == 0))
                    {
                        sqlCommand.CommandText = SQLUpdate + "(" + queryUpdate + ")";
                        sqlCommand.ExecuteNonQuery();
                        SQLUpdate = "Update SmsManagers set IsSynchronized = 1,SynchronizedDate = '" + DateTime.Now + "' where id IN ";
                    }
                }
                else
                {
                    try
                    {
                        queryUpdate += "," + query;
                        sqlCommand.CommandText = SQLUpdate + "(" + queryUpdate + ")";
                        sqlCommand.ExecuteNonQuery();
                        SQLUpdate = "Update SmsManagers set IsSynchronized = 1,SynchronizedDate = '" + DateTime.Now + "' where id IN ";
                        queryUpdate = "";
                    }
                    catch (Exception ex) { }
                }
            }
            #endregion
            conn.Close();
            conn.Dispose();
            return null;
        }
        [HttpPost]
        public ActionResult SettingImport(string direction)
        {
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    if (!string.IsNullOrEmpty(direction))
                    {
                        var table = ReadFileCSV(Server.MapPath(direction));
                        if (table != null)
                        {
                            #region Import
                            var objectImport = new List<SmsManagersBO>();
                            string query = "";

                            int numberInportFail = 0;
                            DateTime dDate;
                            SqlConnection conn = new SqlConnection(connectionString);
                            conn.Open();
                            SqlCommand sqlCommand = new SqlCommand();
                            sqlCommand.Connection = conn;

                            var userId = UserService.GetUserCurrent().Id;
                            string SQLInsert = "INSERT INTO dbo.SmsManagers (Id,SendNumber,RecieveNumber,MessageContent,NetworkProvider,SendDate,IsSynchronized,IsDelete,CreateAt,CreateBy,UpdateAt,UpdateBy) VALUES ";
                            int countString = SQLInsert.Length;
                            for (var i = 0; i < table.Rows.Count; i++)
                            {
                                try
                                {
                                    query = "('" + Guid.NewGuid() + "',N'" + table.Rows[i][1].ToString().Replace("'", "''") + "',N'" + table.Rows[i][0].ToString().Replace("'", "''") + "',N'" + table.Rows[i][2].ToString().Replace("'", "''") + "',N'" + table.Rows[i][3].ToString().Replace("'", "''") + "','" + DateTime.Parse(table.Rows[i][4].ToString()) + "','" + false + "','" + false + "',N'" + DateTime.Now + "',N'" + userId + "',N'" + DateTime.Now + "',N'" + userId + "')";
                                    if (SQLInsert.Length + query.Length < 50000)
                                    {
                                        if (SQLInsert.Length == countString)
                                            SQLInsert += query;
                                        else
                                            SQLInsert += "," + query;
                                        if (i == table.Rows.Count || (i >= 1000 && i % 1000 == 0))
                                        {
                                            sqlCommand.CommandText = SQLInsert;
                                            sqlCommand.ExecuteNonQuery();
                                            SQLInsert = "INSERT INTO dbo.SmsManagers (Id,SendNumber,RecieveNumber,MessageContent,NetworkProvider,SendDate,IsSynchronized,IsDelete,CreateAt,CreateBy,UpdateAt,UpdateBy) VALUES ";
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            SQLInsert += "," + query;
                                            sqlCommand.CommandText = SQLInsert;
                                            sqlCommand.ExecuteNonQuery();
                                            SQLInsert = "INSERT INTO dbo.SmsManagers (Id,SendNumber,RecieveNumber,MessageContent,NetworkProvider,SendDate,IsSynchronized,IsDelete,CreateAt,CreateBy,UpdateAt,UpdateBy) VALUES ";
                                        }
                                        catch (Exception ex) { }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    numberInportFail++;
                                }
                            }
                            conn.Close();
                            conn.Dispose();
                            systemIO.File.Delete(Server.MapPath(direction));
                            if (numberInportFail > 0)
                            {
                                X.Msg.Show(new MessageBoxConfig
                                {
                                    Buttons = MessageBox.Button.OK,
                                    Icon = MessageBox.Icon.INFO,
                                    Title = "Thông báo",
                                    Message = "Có " + numberInportFail + " bản ghi không được nhập liệu do mã đã tồn tại hoặc sai định dạng ngày tháng. Vui lòng kiểm tra lại dữ liệu trước khi nhập liệu tự động!"
                                });
                                return this.Direct();
                            }
                            else
                            {
                                X.Msg.Show(new MessageBoxConfig
                                {
                                    Buttons = MessageBox.Button.OK,
                                    Icon = MessageBox.Icon.INFO,
                                    Title = "Thông báo",
                                    Message = "Nhập liệu vào hệ thống thành công!"
                                });
                                return this.Direct();
                            }
                            #endregion
                        }
                        else
                        {
                            X.Msg.Show(new MessageBoxConfig
                            {
                                Buttons = MessageBox.Button.OK,
                                Icon = MessageBox.Icon.INFO,
                                Title = "Thông báo",
                                Message = "File excel không đúng fomat!"
                            });
                            return this.Direct();
                        }
                    }
                    else
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING,
                            Title = "Thông báo",
                            Message = "Vui lòng lựa chọn tệp cần nhập liệu vào hệ thống!"
                        });
                        return this.Direct();
                    }
                }
            }
            catch (Exception e)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Có lỗi trong quá trình nhập dữ liệu!"
                });
                return this.Direct();
            }
        }
        #endregion
    }
}