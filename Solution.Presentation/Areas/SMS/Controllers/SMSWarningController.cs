﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.SMS.Controllers
{
    public class SMSWarningController : FrontController
    {

        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "SMSWarning",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult GetDataSMSWarning(StoreRequestParameters param, string filterName = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = SmsBlockListService.GetDataSMSWarning(pageIndex, pageSize, filterName, out count);
            return this.Store(data, count);
        }
    }
}