﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.SMS.Controllers
{
    public class SmsFormContentController : FrontController
    {
        // GET: SMS/SmsFormContent
        public ActionResult Index()
        {
            return PartialView();
        }
        public ActionResult Form(string id = "")
        {
            SmsFormContentBO model;
            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = SmsFormContentService.GetFormItem();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = SmsFormContentService.GetById(new Guid(id));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult GetData(StoreRequestParameters param, string filterName = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = SmsFormContentService.GetAll(pageIndex, pageSize, out count);
            if (!string.IsNullOrEmpty(filterName))
                data = SmsFormContentService.GetByfilterName(pageIndex, pageSize, filterName, out count);
            return this.Store(data, count);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(SmsFormContentBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsFormContentService.Insert(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(SmsFormContentBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsFormContentService.Update(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsFormContentService.Delete(id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}