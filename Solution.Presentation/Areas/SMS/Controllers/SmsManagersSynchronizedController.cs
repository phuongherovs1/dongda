﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Common;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.SMS.Controllers
{
    public class SmsManagersSynchronizedController : FrontController
    {
        //public ActionResult Index()
        //{
        //    return PartialView();
        //}

        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult Form(string id = "")
        {
            var selectReview = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetLogicReview());
            var selectObject = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetObjectRelative());
            ViewData.Add("SelectReview", selectReview);
            ViewData.Add("SelectObject", selectObject);
            SmsManagersSynchronizedBO model;
            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = SmsManagersSynchronizedService.GetFormItem();
            }
            else
            {
                var lstType = SmsTypeService.GetDataByStringQuery("SELECT * FROM SmsType Where IdSms = '" + id + "'");
                if (lstType != null && lstType.ToList().Count > 0)
                {
                    string query = "SELECT * FROM SmsKeywordCategories Where Id in( '" + string.Join("','", lstType.Select(x => x.IdType.Value.ToString()).ToList()) + "')";
                    var lstGroupKey = SmsKeywordCategoriesService.GetDataByStringQuery(query);
                    if (lstGroupKey != null && lstGroupKey.ToList().Count > 0)
                        ViewData["lstFroupKey"] = lstGroupKey.Select(x => x.Name).ToList();
                }
                ViewData.Add("Operation", Common.Operation.Update);
                model = SmsManagersSynchronizedService.GetById(new Guid(id));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult GetData(StoreRequestParameters param, string filterName = default(string))
        {
            List<SmsManagersSynchronizedBO> data = new List<SmsManagersSynchronizedBO>();
            List<TypeSMSName> dataTypeName = new List<TypeSMSName>();
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            DataTable dataSMSManagers = new DataTable();
            DataTable SMSTypeName = new DataTable();
            DBConnect dbconnect = new DBConnect();
            DataSet getDataAll;
            getDataAll = dbconnect.myDataset("sp_SmsManagersSynchronized_GetData", "@filterName", filterName, "@pageIndex", pageIndex, "@pageSize", pageSize);
            dataSMSManagers = getDataAll.Tables[0];
            SMSTypeName = getDataAll.Tables[1];
            count = (int)getDataAll.Tables[2].Rows[0][0];

            for (int i = 0; i < SMSTypeName.Rows.Count; i++)
            {
                TypeSMSName item = new TypeSMSName();
                item.Id = new Guid(SMSTypeName.Rows[i]["Id"].ToString());
                item.Name = SMSTypeName.Rows[i]["Name"].ToString();
                dataTypeName.Add(item);
            }
            for (int i = 0; i < dataSMSManagers.Rows.Count; i++)
            {
                SmsManagersSynchronizedBO item = new SmsManagersSynchronizedBO();
                item.Id = new Guid(dataSMSManagers.Rows[i]["Id"].ToString());
                item.SendNumber = dataSMSManagers.Rows[i]["SendNumber"].ToString();
                item.RecieveNumber = dataSMSManagers.Rows[i]["RecieveNumber"].ToString();
                item.MessageContent = dataSMSManagers.Rows[i]["MessageContent"].ToString();
                item.NetworkProvider = dataSMSManagers.Rows[i]["NetworkProvider"].ToString();
                item.SendDate = Convert.ToDateTime(dataSMSManagers.Rows[i]["SendDate"]);
                if (!string.IsNullOrEmpty(dataSMSManagers.Rows[i]["TypingDate"].ToString()))
                    item.TypingDate = Convert.ToDateTime(dataSMSManagers.Rows[i]["TypingDate"]);
                item.IsTyping = !string.IsNullOrEmpty(dataSMSManagers.Rows[i]["IsTyping"].ToString()) ? Convert.ToBoolean(dataSMSManagers.Rows[i]["IsTyping"]) : false;
                item.lstFroupKey = string.Join(",", dataTypeName.Where(x => x.Id == item.Id).Select(x => x.Name));
                data.Add(item);
            }
            return this.Store(data, count);
        }
        [HttpPost]
        public ActionResult TypingSms()
        {
            List<SmsTypeBO> lstType = new List<SmsTypeBO>();
            List<SmsKeywordsBO> dataKeyword = (List<SmsKeywordsBO>)SmsKeywordsService.GetAll();
            var objectImportSynchronized = SmsManagersSynchronizedService.Get(x => x.IsTyping == false && x.IsDelete == false);
            int count = objectImportSynchronized.ToList().Count;
            if (count > 0)
            {
                foreach (SmsManagersSynchronizedBO item in objectImportSynchronized)
                {
                    #region add item TypeTable, BlockListTable
                    List<SmsKeywordsBO> lstkeywords = dataKeyword.Where(x => item.MessageContent.Contains(x.KeywordValue)).ToList();
                    if (lstkeywords != null && lstkeywords.Count > 0)
                    {
                        foreach (SmsKeywordsBO kw in lstkeywords)
                        {
                            if (lstType.FindIndex(x => x.IdSms == item.Id && x.IdType == kw.Id) == -1)
                            {
                                lstType.Add(new SmsTypeBO()
                                {
                                    Id = Guid.NewGuid(),
                                    IdSms = item.Id,
                                    IdType = kw.KeywordCategoryId
                                });
                            }
                        }
                    }
                    #endregion
                }
                string connectionString = ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString;
                SqlConnection conn = new SqlConnection(connectionString);
                conn.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = conn;
                var userId = UserService.GetUserCurrent().Id;

                string SQLInsert = "INSERT INTO dbo.SmsType (Id,IdSms,IdType,IsDelete,CreateAt,CreateBy) VALUES ";
                int countString = SQLInsert.Length;
                foreach (var item in lstType)
                {
                    string query = "('" + item.Id + "','" + item.IdSms + "','" + item.IdType + "','" + false + "','" + DateTime.Now + "','" + userId + "')";
                    if (SQLInsert.Length < 50000)
                    {
                        if (SQLInsert.Length == countString)
                            SQLInsert += query;
                        else
                            SQLInsert += "," + query;

                        if (lstType.ToList().Last().Id == item.Id || (lstType.FindIndex(x => x.Id == item.Id) >= 1000 && lstType.FindIndex(x => x.Id == item.Id) % 1000 == 0))
                        {
                            sqlCommand.CommandText = SQLInsert;
                            sqlCommand.ExecuteNonQuery();
                            SQLInsert = "INSERT INTO dbo.SmsType (Id,IdSms,IdType,IsDelete,CreateAt,CreateBy) VALUES ";
                        }
                    }
                    else
                    {
                        try
                        {
                            SQLInsert += "," + query;
                            sqlCommand.CommandText = SQLInsert;
                            sqlCommand.ExecuteNonQuery();
                            SQLInsert = "INSERT INTO dbo.SmsType (Id,IdSms,IdType,IsDelete,CreateAt,CreateBy) VALUES ";
                        }
                        catch (Exception ex) { }
                    }
                }
                #region Update SmsManagersSynchronized
                string queryUpdate = "";
                string SQLUpdate = "Update SmsManagersSynchronized set IsTyping = 1,TypingDate = '" + DateTime.Now + "' where id IN ";
                foreach (var item in objectImportSynchronized)
                {
                    string query = "'" + item.Id + "'";
                    if ((SQLUpdate.Length + queryUpdate.Length) < 50000)
                    {
                        if (string.IsNullOrEmpty(queryUpdate))
                            queryUpdate = query;
                        else
                            queryUpdate += "," + query;

                        if (objectImportSynchronized.ToList().Last().Id == item.Id)
                        {
                            sqlCommand.CommandText = SQLUpdate + "(" + queryUpdate + ")";
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        try
                        {
                            queryUpdate += "," + query;
                            sqlCommand.CommandText = SQLUpdate + "(" + queryUpdate + ")";
                            sqlCommand.ExecuteNonQuery();
                            SQLUpdate = "Update SmsManagersSynchronized set IsTyping = 1,TypingDate = '" + DateTime.Now + "' where id IN ";
                            queryUpdate = "";
                        }
                        catch (Exception ex) { }
                    }
                }
                #endregion
                conn.Close();
                conn.Dispose();
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO,
                    Title = "Thông báo",
                    Message = "Phân loại thành công.!"
                });
                return this.Direct();
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO,
                    Title = "Thông báo",
                    Message = "Tất cả dữ liệu đã được phân loại trước đó, hãy thêm dữ liệu mới để xử lý.!"
                });
                return this.Direct();
            }
        }
        [HttpPost]
        public ActionResult FilterPhoneNumberFromContent()
        {
            List<SmsBlockListBO> lstBlock = new List<SmsBlockListBO>();
            string regexBrandname = @"[a-z]+";
            var objectImportSynchronized = SmsManagersSynchronizedService.Get(x => x.FilterContentStatus == false && x.IsDelete == false);
            if (objectImportSynchronized.ToList().Count > 0)
            {
                foreach (SmsManagersSynchronizedBO item in objectImportSynchronized)
                {
                    #region add item BlockListTable
                    string sdt = string.Empty;
                    if (item.MessageContent.ToUpper().StartsWith("TB ") && item.MessageContent.Trim().Split(' ').ToList().Count == 2)
                    {
                        string typePhone = "";
                        if (item.SendNumber == "456")
                            typePhone = iDAS.Presentation.Common.Resource.HeadPhone;
                        else if (Regex.Match(item.SendNumber, regexBrandname, RegexOptions.IgnoreCase).Success)
                            typePhone = iDAS.Presentation.Common.Resource.Brandname;
                        else
                            typePhone = iDAS.Presentation.Common.Resource.LongPhone;
                        var lst = item.MessageContent.Split(' ').ToList();
                        if (!lstBlock.Any(x => x.SendNumber == lst[1]))
                            lstBlock.Add(new SmsBlockListBO()
                            {
                                Id = Guid.NewGuid(),
                                SendNumber = lst[1],
                                SendDate = item.SendDate,
                                IdSms = item.Id,
                                NetworkProvider = item.NetworkProvider,
                                MessageContent = item.MessageContent,
                                Type = typePhone
                            });
                    }
                    #endregion
                }
                #region Update end insert to DB
                string connectionString = ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString;
                SqlConnection conn = new SqlConnection(connectionString);
                conn.Open();
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = conn;
                var userId = UserService.GetUserCurrent().Id;
                string SQLInsert = "INSERT INTO dbo.SmsBlockList (Id,SendNumber,SendDate,IdSms,NetworkProvider,MessageContent,Type,IsDelete,CreateAt,CreateBy) VALUES ";
                int countString = SQLInsert.Length;
                foreach (var item in lstBlock)
                {
                    string query = "('" + item.Id + "','" + item.SendNumber + "','" + item.SendDate + "','" + item.IdSms + "',N'" + item.NetworkProvider + "',N'" + item.MessageContent + "','" + item.Type + "','" + false + "','" + DateTime.Now + "','" + userId + "')";
                    if (SQLInsert.Length < 50000)
                    {
                        if (SQLInsert.Length == countString)
                            SQLInsert += query;
                        else
                            SQLInsert += "," + query;

                        if (lstBlock.Last().Id == item.Id)
                        {
                            sqlCommand.CommandText = SQLInsert;
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        try
                        {
                            SQLInsert += "," + query;
                            sqlCommand.CommandText = SQLInsert;
                            sqlCommand.ExecuteNonQuery();
                            SQLInsert = "INSERT INTO dbo.SmsBlockList (Id,SendNumber,SendDate,IdSms,NetworkProvider,MessageContent,Type,IsDelete,CreateAt,CreateBy) VALUES ";
                        }
                        catch (Exception ex) { }
                    }
                }
                #region Update SmsManagersSynchronized
                string queryUpdate = "";
                string SQLUpdate = "Update SmsManagersSynchronized set FilterContentStatus = 1,FilterContentDate = '" + DateTime.Now + "' where id IN ";
                foreach (var item in objectImportSynchronized.ToList())
                {
                    string query = "'" + item.Id + "'";
                    if ((SQLUpdate.Length + queryUpdate.Length) < 50000)
                    {
                        if (string.IsNullOrEmpty(queryUpdate))
                            queryUpdate = query;
                        else
                            queryUpdate += "," + query;

                        if (objectImportSynchronized.ToList().Last().Id == item.Id)
                        {
                            sqlCommand.CommandText = SQLUpdate + "(" + queryUpdate + ")";
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        try
                        {
                            queryUpdate += "," + query;
                            sqlCommand.CommandText = SQLUpdate + "(" + queryUpdate + ")";
                            sqlCommand.ExecuteNonQuery();
                            SQLUpdate = "Update SmsManagersSynchronized set FilterContentStatus = 1,FilterContentDate = '" + DateTime.Now + "' where id IN ";
                            queryUpdate = "";
                        }
                        catch (Exception ex) { }
                    }
                }
                #endregion
                conn.Close();
                conn.Dispose();
                #endregion
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO,
                    Title = "Thông báo",
                    Message = "Xử lý thành công.!"
                });
                return this.Direct();
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.INFO,
                    Title = "Thông báo",
                    Message = "Tất cả dữ liệu đã được xử lý trước đó, hãy thêm dữ liệu mới để xử lý.!"
                });
                return this.Direct();
            }
        }
        public ActionResult Create(SmsManagersSynchronizedBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsManagersSynchronizedService.Insert(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(SmsManagersSynchronizedBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsManagersSynchronizedService.Update(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsManagersSynchronizedService.Delete(id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
    public class TypeSMSName
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}