﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.SMS.Controllers
{
    public class DashboardController : FrontController
    {
        //public Ext.Net.MVC.PartialViewResult Index(string containerId, int index)
        //{
        //    ViewData["index"] = index;
        //    return new Ext.Net.MVC.PartialViewResult
        //    {
        //        RenderMode = RenderMode.AddTo,
        //        ContainerId = containerId,
        //        ViewData = ViewData,
        //        ViewName = "Index",
        //        WrapByScriptTag = false
        //        //check view
        //    };
        //}
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "Index",
                WrapByScriptTag = false
                //check view
            };
        }

        public ActionResult Form(string id = "")
        {
            var selectReview = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetLogicReview());
            var selectObject = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetObjectRelative());
            ViewData.Add("SelectReview", selectReview);
            ViewData.Add("SelectObject", selectObject);
            SmsKeywordCategoriesBO model;
            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = SmsKeywordCategoriesService.GetFormItem();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = SmsKeywordCategoriesService.GetById(new Guid(id));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        #region Method
        public ActionResult GetData(StoreRequestParameters param, string filterName = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = SmsKeywordCategoriesService.GetAll(pageIndex, pageSize, out count);
            if (!string.IsNullOrEmpty(filterName))
                data = SmsKeywordCategoriesService.GetByfilterName(pageIndex, pageSize, filterName, out count);
            return this.Store(data, count);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(SmsKeywordCategoriesBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsKeywordCategoriesService.Insert(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(SmsKeywordCategoriesBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsKeywordCategoriesService.Update(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    SmsKeywordCategoriesService.Delete(id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        #endregion
    }
}