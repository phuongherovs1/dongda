﻿using Ext.Net;
using iDAS.Presentation.Controllers;
using System;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.System.Controllers
{
    //[Authorize(Roles = Common.Role.Administrator)]
    public class DashBoardController : FrontController
    {
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            var tap = Request.Params["Tap"];
            if (tap != null)
            {
                ViewData["Tap"] = tap;
            }
            else
            {
                ViewData["Tap"] = "ALL";
            }
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1"? true:false;
            else
                ViewData["isroleedit"] = false;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                WrapByScriptTag = false,
                ViewData = ViewData
            };
        }

        public ActionResult Database()
        {
            return PartialView();
        }
        public ActionResult History()
        {
            return PartialView();
        }
    }
}