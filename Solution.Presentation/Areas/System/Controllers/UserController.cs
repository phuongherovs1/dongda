﻿
using Ext.Net;
using Ext.Net.MVC;
using iDAS.ADO;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.System.Controllers
{
    public class UserController : FrontController
    {
        private UserManager<UserBO, Guid> userManager { get; set; }
        private RoleManager<RoleBO, Guid> roleManager { get; set; }
        private readonly IVPHumanProfileCuriculmViateService _vPHumanProfileCuriculmViateService;


        public UserController(IUserService userStoreService, IRoleService roleStoreService, IVPHumanProfileCuriculmViateService vPHumanProfileCuriculmViateService)
        {
            var provider = new DpapiDataProtectionProvider();
            userManager = new UserManager<UserBO, Guid>(userStoreService);
            userManager.UserTokenProvider = new DataProtectorTokenProvider<UserBO, Guid>(provider.Create("key"));
            roleManager = new RoleManager<RoleBO, Guid>(roleStoreService);
            _vPHumanProfileCuriculmViateService = vPHumanProfileCuriculmViateService;
        }

        public ActionResult Index()
        {
            var RoleEdit = Request.Params["isroleedit"];
            if (RoleEdit != null)
                ViewData["isroleedit"] = RoleEdit == "1" ? true : false;
            else
                ViewData["isroleedit"] = false;
            return PartialView(ViewData = ViewData);
        }
        public ActionResult LoadData(StoreRequestParameters parameter, string query = "")
        {
            var count = 0;
            CultureInfo culture = new CultureInfo("vi-VN");
            DataTable data = Sql_ADO.idasAdoService.ExecuteNoqueryTable("sp_Employees_EmployeeSystems", parameter: new { query = query });
            List<UserBO> result = new List<UserBO>();
            foreach (DataRow item in data.Rows)
            {
                UserBO userBO = new UserBO();
                userBO.Id = Guid.Parse(item["Id"].ToString());
                userBO.Avatar.Data = item["Avatar"] != null && !string.IsNullOrEmpty(item["Avatar"].ToString()) ? Encoding.ASCII.GetBytes(item["Avatar"].ToString()) : new byte[0];
                userBO.FullName = item["Name"] != null ? item["Name"].ToString() : "";
                userBO.UserName = item["UserName"] != null ? item["UserName"].ToString() : "";
                userBO.PhoneNumber = item["PhoneNumber"] != null ? item["PhoneNumber"].ToString() : "";
                userBO.Email = item["Email"] != null ? item["Email"].ToString() : "";
                userBO.Status = (item["LockoutEnabled"] != null && !string.IsNullOrEmpty(item["LockoutEnabled"].ToString())) ? (bool.Parse(item["LockoutEnabled"].ToString()) == true ? "Đã bị khóa" : "Bình thường") : "Bình thường";
                userBO.DepartmentName = item["DepartmentName"] != null ? item["DepartmentName"].ToString() : "";
                result.Add(userBO);
            }
            result = result.Select(i => { i.LastName = i.FullName.Split(' ').Last(); return i; }).ToList();
            return this.Store(result.OrderBy(x => x.LastName, StringComparer.Create(culture, false)), count);
        }
        public ActionResult GetEmployeeHasCode()
        {
            var count = 0;
            var data = EmployeeService.Get(x => !string.IsNullOrEmpty(x.Code) && x.IsDelete == false);
            return this.Store(data, count);
        }
        public ActionResult Form(string id = default(string))
        {
            object model = null;
            if (id == default(string))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = new UserBO();
            }
            else
            {
                var exist = UserRoleService.RoleExits("ViewEmployeeProfile");
                var isAdmin = UserRoleService.RoleExits("Administrator");
                var isCurrent = UserRoleService.CurrentUser(new Guid(id));
                DepartmentTitleRoleService s = new DepartmentTitleRoleService();
                var titleRole = s.CheckTitleRole_View(new Guid(id));
                if (exist || isCurrent || isAdmin || titleRole)
                {
                    ViewData.Add("Operation", Common.Operation.Update);
                    var Userinfo = UserService.GetById(new Guid(id.ToString()));
                    var employerinfo = EmployeeService.GetById(new Guid(id.ToString()));
                    Userinfo.Name = employerinfo.Name;
                    model = Userinfo;
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = "Bạn không có quyền truy cập chức năng này!"
                    });
                    return this.Direct();
                }
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public new ActionResult Profile(string id = default(string))
        {
            var model = EmployeeService.GetById(new Guid(id.ToString()));
            model.FileAvatar.Data = model.Avatar;
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        //[Authorize(Roles = Common.Role.Administrator)]
        [HttpPost]
        public ActionResult Create(UserBO user)
        {
            var isAdmin = UserRoleService.RoleExits("Administrator");
            var isCurrent = UserRoleService.CurrentUser(user.Id);
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckTitleRole_Edit(user.Id);
            if (!isCurrent && !isAdmin && !titleRole)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }
            bool success = false;
            if (ModelState.IsValid)
            {
                try
                {
                    var result = userManager.Create(user, user.Password);
                    VPHumanProfileCuriculmViateBO vPHumanProfileCuriculmViateBO = new VPHumanProfileCuriculmViateBO
                    {
                        HumanEmployeeId = user.Id
                    };
                    _vPHumanProfileCuriculmViateService.Insert(vPHumanProfileCuriculmViateBO);
                    success = result.Succeeded;
                    if (!success)
                    {
                        this.ShowNotify(Common.Resource.SystemFailure);
                    }
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(success);
        }
        [HttpPost]
        public ActionResult Update(EmployeeBO user, UserBO u)
        {
            var isAdmin = UserRoleService.RoleExits("Administrator");
            var isCurrent = UserRoleService.CurrentUser(u.Id);
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckTitleRole_Edit(u.Id);
            if (!isCurrent && !isAdmin && !titleRole)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }
            try
            {
                user.Avatar = user.FileAvatar.Data;
                var userUpdate = new UserBO()
                {
                    Id = user.Id,
                    FullName = user.Name,
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,
                    IsExistEmployee = true,
                    Locked = u.Locked
                };
                userManager.Update(userUpdate);
                EmployeeService.Update(user);
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: true);
        }
        [HttpPost]
        public ActionResult ResetPassword(UserBO user)
        {
            var isAdmin = UserRoleService.RoleExits("Administrator");
            var isCurrent = UserRoleService.CurrentUser(user.Id);
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckTitleRole_Edit(user.Id);
            if (!isCurrent && !isAdmin && !titleRole)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }
            bool success = false;
            try
            {
                var resetToken = userManager.GeneratePasswordResetToken(user.Id);
                var result = userManager.ResetPassword(user.Id, resetToken, user.Password);
                success = result.Succeeded;
                this.ShowNotify(Common.Resource.SystemSuccess);
            }
            catch (Exception ex)
            {
                Common.WriteLog.WriteLogEntry("ResetPassword", ex);
                this.ShowNotify(Common.Resource.SystemFailure + ex.Message);
            }

            return this.Direct(success);
        }
        //[Authorize(Roles = Common.Role.Administrator)]
        [HttpPost]
        public ActionResult Delete(UserBO user)
        {
            var isAdmin = UserRoleService.RoleExits("Administrator");
            var isCurrent = UserRoleService.CurrentUser(user.Id);
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckTitleRole_Edit(user.Id);
            if (!isCurrent && !isAdmin && !titleRole)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền thực hiện chức năng này"
                });
                return this.Direct();
            }
            bool success = false;
            try
            {
                var result = userManager.Delete(user);
                success = result.Succeeded;
                if (!success)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            catch (Exception ex)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(success);
        }
        [HttpGet]
        public ActionResult ChangePassword()
        {
            var model = UserService.GetCurrentUser();
            return new Ext.Net.MVC.PartialViewResult { Model = model };
        }
        [HttpPost]
        public ActionResult ChangePassword(UserBO user)
        {
            bool rs = false;
            string outputvalidate = "";
            if (!Common.Utilities.ValidatePassword(user.NewPassword, out outputvalidate))
            {
                this.ShowNotify(outputvalidate);
                return this.Direct(rs);
            }
            else
            {
                var result = userManager.ChangePassword(new Guid(User.Identity.GetUserId()), user.Password, user.NewPassword);
                rs = true;
                return this.Direct(rs);
            }
        }

        [HttpGet]
        public ActionResult ResetPassword(Guid userId)
        {
            var exist = UserRoleService.RoleExits("ViewEmployeeProfile");
            var isAdmin = UserRoleService.RoleExits("Administrator");
            var isCurrent = UserRoleService.CurrentUser(userId);
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckTitleRole_View(userId);
            if (exist || isCurrent || isAdmin || titleRole)
            {
                var user = new UserBO()
                {
                    Id = userId
                };
                return new Ext.Net.MVC.PartialViewResult { Model = user };
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền truy cập chức năng này!"
                });
                return this.Direct();
            }
        }

        //[HttpPost]
        //public ActionResult ResetPassword(UserBO user)
        //{
        //    var token = userManager.GeneratePasswordResetToken(user.Id);
        //    var result = userManager.ResetPassword(user.Id, token,user.NewPassword);
        //    return this.Direct();
        //}
    }
}