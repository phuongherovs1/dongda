﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Common;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using iDAS.Service.API.System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.System.Controllers
{
    //[Authorize(Roles = Common.Role.Administrator)]

    public class PermissionController : FrontController
    {
        private IPermissionService permissionService { set; get; }
        public PermissionController(IPermissionService _permissionService)
        {
            permissionService = _permissionService;
        }
        #region Theo người dùng
        public ActionResult Index(string userId = default(string))
        {
            var source = RoleService.GetModules();
            var menus = new List<Ext.Net.CheckMenuItem>();
            menus.Add(new Ext.Net.CheckMenuItem()
            {
                Text = Resource.AllModules,
                Icon = Icon.Table,
                Checked = true
            });
            for (int i = 0; i < source.Count(); i++)
            {
                var item = source.ElementAt(i);
                menus.Add(new Ext.Net.CheckMenuItem()
                {
                    Text = item,
                    Icon = Icon.Table
                });
            }
            ViewData.Add("Modules", menus);
            ViewData.Add("UserId", userId);
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }
        [Authorize(Roles = Roles.ManagerUser)]
        public ActionResult IndexNew(string userId = default(string))
        {
            var exist = UserRoleService.RoleExits("ViewEmployeeProfile");
            var isAdmin = UserRoleService.RoleExits("Administrator");
            var isCurrent = UserRoleService.CurrentUser(new Guid(userId));
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckTitleRole_View(new Guid(userId));
            var titleRoleEdit = s.CheckTitleRole_Edit(new Guid(userId));
            if (isAdmin || isCurrent)
                titleRoleEdit = true;
            if (exist || isCurrent || isAdmin || titleRole)
            {
                var source = RoleService.GetModules();
                var menus = new List<Ext.Net.CheckMenuItem>();
                menus.Add(new Ext.Net.CheckMenuItem()
                {
                    Text = Resource.AllModules,
                    Icon = Icon.Table,
                    Checked = true
                });
                for (int i = 0; i < source.Count(); i++)
                {
                    var item = source.ElementAt(i);
                    menus.Add(new Ext.Net.CheckMenuItem()
                    {
                        Text = item,
                        Icon = Icon.Table
                    });
                }
                ViewData.Add("Modules", menus);
                ViewData.Add("UserId", userId);
                ViewData.Add("RoleEdit", titleRoleEdit);
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền truy cập chức năng này!"
                });
                return this.Direct();
            }
        }
        public StoreResult GetRoles(string node)
        {
            NodeCollection nodes = new Ext.Net.NodeCollection();
            var data = permissionService.GetListTreeRole(Convert.ToInt32(node));
            foreach (DataRow dataRow in data.Rows)
            {
                Node asyncNode = new Node();
                asyncNode.Text = " " + dataRow["Name"];
                asyncNode.NodeID = dataRow["RoleID"].ToString();
                asyncNode.IconCls = "x-fa fa-home";
                bool Supervisor = Convert.ToBoolean(dataRow["Supervisor"] == DBNull.Value ? false : dataRow["Supervisor"]);
                if (!Supervisor)
                {
                    asyncNode.Leaf = true;
                }
                nodes.Add(asyncNode);
            }
            return this.Store(nodes);
        }
        public ActionResult GetRolesUser(string UserId, int RoleID)
        {
            Guid guid = new Guid(UserId);
            return this.Store(permissionService.GetListRolePermission(guid, RoleID));
        }
        public ActionResult UpdateRolesUser(string UserId, int RoleID, string IsPermission = default(string), string IsRoleEdit = default(string))
        {
            Guid guid = new Guid(UserId);
            return this.Direct(permissionService.UpdateUserRole(guid, RoleID, IsPermission, IsRoleEdit));
        }

        public ActionResult GetRolesUserTitle(string DepartmentTitleId, int RoleID)
        {
            Guid departmentTitleId = new Guid(DepartmentTitleId);
            return this.Store(permissionService.GetListRoleTitlePermission(departmentTitleId, RoleID));
        }
        public ActionResult UpdateRolesUserTitle(string DepartmentTitleId, int RoleID, string IsPermission = default(string), string IsRoleEdit = default(string))
        {
            Guid departmentTitleId = new Guid(DepartmentTitleId);
            return this.Direct(permissionService.UpdateUserRoleTitle(departmentTitleId, RoleID, IsPermission, IsRoleEdit));
        }

        public ActionResult LoadData(StoreRequestParameters parameter, Guid userId, string module = Resource.AllModules)
        {
            IEnumerable<RoleBO> data;
            int count = 0;
            if (module == Resource.AllModules)
                data = RoleService.GetAll(userId, parameter.Page, parameter.Limit, out count);
            else
                data = RoleService.GetAll(userId, module, parameter.Page, parameter.Limit, out count);
            return this.Store(data, count);
        }
        [HttpPost]
        public ActionResult Update(UserRoleBO userRole)
        {
            bool success = false;
            if (ModelState.IsValid)
            {
                try
                {
                    UserRoleService.Update(userRole);
                    success = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(success);
        }
        #endregion
        #region Theo chức danh
        public ActionResult PermissionTitle(string titleId = default(string))
        {
            var source = RoleService.GetModules();
            var menus = new List<Ext.Net.CheckMenuItem>();
            menus.Add(new Ext.Net.CheckMenuItem()
            {
                Text = Resource.AllModules,
                Icon = Icon.Table,
                Checked = true
            });
            for (int i = 0; i < source.Count(); i++)
            {
                var item = source.ElementAt(i);
                menus.Add(new Ext.Net.CheckMenuItem()
                {
                    Text = item,
                    Icon = Icon.Table
                });
            }
            ViewData.Add("Modules", menus);
            ViewData.Add("TitleId", titleId);
            return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
        }

        public ActionResult PermissionTitleNew(string titleId = default(string))
        {
            UserRoleService roleService = new UserRoleService();
            var isAdmin = roleService.RoleExits("Administrator");
            DepartmentTitleRoleService s = new DepartmentTitleRoleService();
            var titleRole = s.CheckRoleForTitles_View(new Guid(titleId));
            var titleRoleEdit = s.CheckRoleForTitles_Edit(new Guid(titleId));
            if (isAdmin)
                titleRoleEdit = true;
            if (isAdmin || titleRole)
            {
                ViewData.Add("TitleId", titleId);
                ViewData.Add("RoleEdit", titleRoleEdit);
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Bạn không có quyền truy cập chức năng này!"
                });
                return this.Direct();
            }
        }
        public ActionResult LoadDataTitle(StoreRequestParameters parameter, Guid titleId, string module = Resource.AllModules)
        {
            IEnumerable<RoleBO> data;
            int count = 0;
            if (module == Resource.AllModules)
                data = RoleService.GetAllByTittle(titleId, parameter.Page, parameter.Limit, out count);
            else
                data = RoleService.GetAllByTittle(titleId, module, parameter.Page, parameter.Limit, out count);
            return this.Store(data, count);
        }
        [HttpPost]
        public ActionResult UpdateTitle(TitleRoleBO titleRole)
        {
            bool success = false;
            if (ModelState.IsValid)
            {
                try
                {
                    TitleRoleService.Update(titleRole);
                    success = true;
                }
                catch (Exception ex)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(success);
        }
        #endregion

    }
}