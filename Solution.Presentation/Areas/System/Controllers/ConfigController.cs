﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.System.Controllers
{
    public class ConfigController : FrontController
    {
        public ActionResult Index()
        {
            return PartialView();
        }
        public ActionResult Form(string id = "")
        {
            var selectReview = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetLogicReview());
            var selectObject = iDAS.Presentation.Common.Utilities.GetSelectItem(iDAS.Service.Common.Utilities.GetObjectRelative());
            ViewData.Add("SelectReview", selectReview);
            ViewData.Add("SelectObject", selectObject);
            ConfigTableBO model;
            if (string.IsNullOrEmpty(id))
            {
                ViewData.Add("Operation", Common.Operation.Create);
                model = ConfigTableService.GetFormItem();
            }
            else
            {
                ViewData.Add("Operation", Common.Operation.Update);
                model = ConfigTableService.GetById(new Guid(id));
            }
            return new Ext.Net.MVC.PartialViewResult { Model = model, ViewData = ViewData };
        }
        public ActionResult GetData(StoreRequestParameters param, string filterName = default(string))
        {
            var pageIndex = param.Page;
            var pageSize = param.Limit;
            var count = 0;
            var data = ConfigTableService.GetAll(pageIndex, pageSize, out count);
            if (!string.IsNullOrEmpty(filterName))
                data = ConfigTableService.GetByfilterName(pageIndex, pageSize, filterName, out count);
            return this.Store(data, count);
        }
        [ValidateInput(false)]
        public ActionResult Create(ConfigTableBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ConfigTableService.Insert(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(ConfigTableBO asset, string dataPropertySettings = "", string dataResourceRelates = "")
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ConfigTableService.Update(asset, dataPropertySettings, dataResourceRelates);
                    result = true;
                }
                catch (Exception ex)
                {
                    if (ex is iDAS.Service.Common.DataHasBeenExistedException)
                        this.ShowNotify(ex.Message);
                    else
                        this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var result = false;
            if (ModelState.IsValid)
            {
                try
                {
                    ConfigTableService.Delete(id);
                    result = true;
                }
                catch (Exception)
                {
                    this.ShowNotify(Common.Resource.SystemFailure);
                }
            }
            return this.Direct(result: result);
        }
    }
}