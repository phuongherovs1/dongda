﻿using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Chat.Controllers
{
    public class UserOnlineController : FrontController
    {
        //
        // GET: /Chat/UserOnline/
        public ActionResult Index()
        {
            try
            {
                return new Ext.Net.MVC.PartialViewResult { };
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }

        public ActionResult GetMember(int start, int limit, string name = default(string))
        {
            try
            {
                var count = 0;
                var data = EmployeeService.GetUserChatList(start, limit, out count, name);
                return this.Store(data, count);
            }
            catch (Exception)
            {

            }
            return this.Direct();
        }

    }
}