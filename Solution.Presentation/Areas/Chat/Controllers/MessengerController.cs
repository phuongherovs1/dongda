﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Presentation.Hubs;
using System;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Chat.Controllers
{
    public class MessengerController : FrontController
    {
        //
        // GET: /Chat/Messenger/

        private ChatHub _chat;
        private ChatHub chat { get { return _chat = _chat ?? new ChatHub(); } }
        public ActionResult Index()
        {
            return View();
        }
        public Ext.Net.MVC.PartialViewResult LoadMore(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "LoadMore",
                WrapByScriptTag = false
            };
        }
        public Ext.Net.MVC.PartialViewResult RightChat(string containerId, string toUserId = default(string))
        {
            var currentUser = EmployeeService.GetWithLoginInfo();
            var toUser = EmployeeService.GetWithLoginInfo(new Guid(toUserId));
            ViewData["CurrentUser"] = currentUser;
            ViewData["ToUser"] = toUser;
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewData = ViewData,
                ViewName = "RightChat",
                WrapByScriptTag = false
            };
        }
        public ActionResult GetDataMore(int start, int limit)
        {
            var count = 0;
            return this.Store(ChatMessengerService.GetMoreByCurrentUser(start, limit, out count), count);
        }
        public ActionResult SingleChat(Guid userId)
        {
            try
            {
                var currentUser = EmployeeService.GetWithLoginInfo();
                var toUser = EmployeeService.GetWithLoginInfo(userId);
                ViewData["CurrentUser"] = currentUser;
                ViewData["ToUser"] = toUser;
                return new Ext.Net.MVC.PartialViewResult { ViewData = ViewData };
            }
            catch (Exception)
            {
                return this.Direct();
            }
        }
        public ActionResult GetData(string userId = default(string))
        {
            try
            {
                var data = ChatMessengerService.GetDataChatToUser(iDAS.Service.Common.Utilities.ConvertToGuid(userId)).ToList();
                return this.Store(data);
            }
            catch (Exception)
            {
            }
            return this.Direct();
        }
        public ActionResult Create(string content, string fromId, string toId, string toUser)
        {
            try
            {
                ChatMessengerService.NewMessage(new Guid(toId), content);
                chat.NewMessage(content, fromId, toId, UserName, toUser);
            }
            catch (Exception)
            {
            }
            return this.Direct();
        }
        public ActionResult Read(string id = default(string))
        {
            var notifyNewTotal = ChatMessengerService.UpdateRead(new Guid(id));
            return this.Direct(notifyNewTotal);
        }
        public ActionResult HiddenMessenger(Guid? id)
        {
            var result = false;
            if (id == null || id == Guid.Empty)
                return this.Direct();
            try
            {
                ChatMessengerService.Delete(id.Value);
                result = true;
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
        public ActionResult Revert(Guid? id)
        {
            var result = false;
            if (id == null || id == Guid.Empty)
                return this.Direct();
            try
            {
                ChatMessengerService.Revert(id.Value);
                result = true;
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }

        public ActionResult UpdateRead(Guid? id, bool isRead = true)
        {
            var result = false;
            if (id == null || id == Guid.Empty)
                return this.Direct();

            try
            {
                ChatMessengerService.UpdateRead(id.Value, isRead);
                result = true;
            }
            catch (Exception)
            {
                this.ShowNotify(Common.Resource.SystemFailure);
            }
            return this.Direct(result: result);
        }
    }
}