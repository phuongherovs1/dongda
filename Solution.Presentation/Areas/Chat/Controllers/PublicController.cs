﻿using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Presentation.Hubs;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Chat.Controllers
{
    public class PublicController : FrontController
    {

        private ChatHub _chat;
        private ChatHub chat { get { return _chat = _chat ?? new ChatHub(); } }
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult GetData()
        {
            var data = ChatPublicService.GetAll().ToList();
            return this.Store(data);
        }
        //fromId: currentUserId, fromUserName: currentUserName, fromFullName: currentFullName
        public ActionResult Create(string content, string fromId, string fromUserName, string fromFullName, string fromAvatarUrl)
        {
            ChatPublicService.NewMessage(content);
            chat.NewPublicMessage(content, fromId, fromUserName, fromFullName, fromAvatarUrl);
            return this.Direct();
        }
    }
}