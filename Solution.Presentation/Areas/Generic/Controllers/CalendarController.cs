﻿using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using System;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Generic.Controllers
{
    public class CalendarController : FrontController
    {
        public ActionResult FormulaTime(DateTime? startAt, DateTime? endAt, Guid? calendarId)
        {
            if (!startAt.HasValue || !endAt.HasValue)
                return this.Direct(string.Empty);
            var time = endAt - startAt ?? new TimeSpan();
            if (calendarId.HasValue)
                time = CalendarService.GetWorkingDay(calendarId.Value, startAt.Value, endAt.Value);
            var textday = time.Days > 0 ? time.Days + " ngày, " : string.Empty;
            var texthour = time.Hours > 0 ? time.Hours + " giờ, " : string.Empty;
            var textminus = time.Minutes > 0 ? time.Minutes + " phút" : string.Empty;
            var result = (textday + texthour + textminus).Trim(' ', ',');
            return this.Direct(result);
        }
    }
}