﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
namespace iDAS.Presentation.Areas.Generic.Controllers
{
    public class EmployeeController : FrontController
    {
        public ActionResult Index(string parameter, string urlStore, string Type = default(string))
        {
            var data = Ext.Net.JSON.Deserialize<Dictionary<string, object>>(parameter ?? string.Empty);
            urlStore = urlStore ?? Url.Action("LoadData", "Employee", new { area = "Generic" });
            ViewData["Parameters"] = data ?? new Dictionary<string, object>();
            ViewData["UrlStore"] = urlStore;
            ViewData["Type"] = !string.IsNullOrEmpty(Type) ? Type : "";
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                ViewData["departmentName"] = department.Name;
                ViewData["departmentId"] = department.Id;
            }
            return new Ext.Net.MVC.PartialViewResult
            {
                ViewData = ViewData,
            };
        }
        /// <summary>
        /// Danh sách nhân sự
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public ActionResult LoadData(StoreRequestParameters parameter, string query = default(string), bool isAll = true, string departmentId = default(string))
        {
            try
            {
                var pageIndex = parameter.Page;
                var pageSize = parameter.Limit;
                var count = 0;
                if (isAll)
                {
                    var data = EmployeeService.GetAllHasRole(parameter.Page, parameter.Limit, out count, query: query, allowGetRoleName: true);
                    return this.Store(data, count);
                }
                else
                {
                    if (!string.IsNullOrEmpty(departmentId))
                    {
                        var data = EmployeeService.GetByDepartment(parameter.Page, parameter.Limit, out count, new Guid(departmentId), query, allowGetRoleName: true);
                        return this.Store(data, count);
                    }
                    else
                    {
                        var data = EmployeeService.GetAllHasRole(parameter.Page, parameter.Limit, out count, query: query, allowGetRoleName: true);
                        return this.Store(data, count);
                    }
                }
            }
            catch
            {
                return this.Store(null);
            }
        }

        /// <summary>
        /// Danh sách nhan sự có quyền phê duyệt ban hành
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public ActionResult GetByPublishApprove(Guid? categoryId)
        {
            if (categoryId == null || categoryId == Guid.Empty)
                return this.Direct();
            var data = new List<EmployeeBO>();
            try
            {
                data = EmployeeService.GetByPublishApprove(categoryId.Value).ToList();
            }
            catch (Exception)
            {
            }
            return this.Store(data);
        }

        /// <summary>
        /// Danh sách nhân sự có quyền phê duyệt kiểm duyệt
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public ActionResult GetByReviewApprove(Guid? categoryId)
        {
            if (categoryId == null || categoryId == Guid.Empty)
                return this.Direct();
            var data = new List<EmployeeBO>();
            try
            {
                data = EmployeeService.GetByReviewApprove(categoryId.Value).ToList();
            }
            catch (Exception)
            {
            }
            return this.Store(data);
        }

        /// <summary>
        /// Danh sách nhân sự có quyền soạn thảo
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public ActionResult GetByPerformWrite(Guid? categoryId)
        {
            if (categoryId == null)
                return this.Direct();
            var data = new List<EmployeeBO>();
            try
            {
                data = EmployeeService.GetByPerformWrite(categoryId.Value).ToList();
            }
            catch (Exception)
            {
            }
            return this.Store(data);
        }
        // lấy thông tin chi tiết nhân sự
        public ActionResult GetEmployee(string id = default(string))
        {
            try
            {
                var employee = EmployeeService.GetDetail(new Guid(id));
                return this.Direct(employee);
            }
            catch
            {
                return this.Direct(new EmployeeBO());
            }
        }
    }
}