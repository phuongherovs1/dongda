﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Common;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using iDAS.Service.FileServiceReference;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace iDAS.Presentation.Areas.Generic.Controllers
{
    public class FileController : FrontController
    {
        public ActionResult LoadFiles(StoreRequestParameters parameter, IEnumerable<Guid> fileIds)
        {
            try
            {
                if (fileIds != null)
                {
                    var data = FileService.GetByIds(fileIds);
                    return this.Store(data);
                }
                else
                {
                    return this.Store(new List<FileBO>());
                }
            }
            catch
            {
                return this.Store(null);
            }
        }
        public ActionResult ViewFile(string fileId)
        {
            ViewData["FileID"] = fileId;
            return new Ext.Net.MVC.PartialViewResult
            {
                ViewData = ViewData,
            };
        }
        public ActionResult ViewFiles(string files)
        {
            ViewData["FileIDs"] = files;
            return new Ext.Net.MVC.PartialViewResult() { ViewData = ViewData };
        }
        public ActionResult LoadFile(Guid fileId)
        {
            try
            {
                FileBO result = FileService.GetById(fileId);
                FileStream filestream = new FileStream(Server.MapPath("/Upload/" + fileId.ToString() + "." + result.Extension), FileMode.Open);
                result.Stream = filestream;
                //var result = FileService.View(fileId);
                var name = result.Name.Replace(",", string.Empty);
                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Disposition", "inline; filename=\"" + name + "\"");
                Response.BufferOutput = false;
                return new FileStreamResult(result.Stream, result.Type);
            }
            catch (Exception ex)
            {
                var FileNotViewError = @"<div style='text-align:center; width:100%;'>
                                                <div style='background-image: url(/Content/images/info.png) !important;background-repeat: no-repeat; background-position: center;height: 172px;'></div>
                                                <div style='font-size: 0.9em'>" + ex.Message + "</div></div>";
                return this.Content(FileNotViewError);
            }
        }
        [HttpPost]
        public ActionResult DownloadFile(Guid fileId)
        {
            try
            {
                FileBO result = FileService.GetById(fileId);
                FileStream filestream = new FileStream(Server.MapPath("/Upload/" + fileId.ToString() + "." + result.Extension), FileMode.Open);
                result.Stream = filestream;
                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + result.Name + "\"");
                Response.AddHeader("Content-Transfer-Encoding", "Binary");
                Response.BufferOutput = false;
                return new FileStreamResult(result.Stream, "application/octet-stream");
            }
            catch
            {
                MessageBox msg = new MessageBox();
                msg.Configure(new MessageBoxConfig()
                {
                    Title = "Thông báo",
                    Message = "Đã xảy ra lỗi trong quá trình tải tệp",
                    Icon = MessageBox.Icon.WARNING,
                    Buttons = MessageBox.Button.OK,
                }).Show();
                return this.Direct();
            }
        }

        //[HttpPost]
        //public ActionResult Delete(string fileId)
        //{
        //    try
        //    {
        //        ResponseFileInfo fileResult = new ResponseFileInfo();
        //        fileResult = fileSV.Delete(fileId);

        //        if (fileResult.Error == FileError.Success)
        //        {
        //            return new EmptyResult();
        //        }
        //        else
        //        {
        //            return RedirectToAction("Error404", "Home", new { code = User.Code });
        //        }
        //    }
        //    catch
        //    {
        //        return RedirectToAction("Error500", "Home", new { code = User.Code });
        //    }
        //}

        //[HttpPost]
        //public ActionResult View(string fileId)
        //{
        //    try
        //    {
        //        ResponseFileInfo fileResult = new ResponseFileInfo();
        //        fileResult = fileSV.View(fileId);

        //        if (fileResult.Error == FileError.Success)
        //        {
        //            Response.Clear();
        //            Response.ClearHeaders();
        //            Response.AddHeader("Content-Disposition", "inline; filename=" + fileResult.Name.Replace(",", string.Empty));
        //            Response.BufferOutput = false;
        //            return new FileStreamResult(fileResult.Stream, fileResult.Type);
        //        }
        //        else
        //        {
        //            return RedirectToAction("Error404", "Home", new { code = User.Code });
        //        }
        //    }
        //    catch
        //    {
        //        return RedirectToAction("Error500", "Home", new { code = User.Code });
        //    }
        //}

        //[HttpPost]
        //public ActionResult ViewExport(string fileId)
        //{
        //    try
        //    {
        //        ResponseFileInfo fileResult = new ResponseFileInfo();
        //        fileResult = fileSV.ViewExport(fileId);

        //        if (fileResult.Error == FileError.Success)
        //        {
        //            Response.Clear();
        //            Response.ClearHeaders();
        //            Response.AddHeader("Content-Disposition", "inline; filename=" + fileResult.Name);
        //            Response.BufferOutput = false;
        //            return new FileStreamResult(fileResult.Stream, fileResult.Type);
        //        }
        //        else
        //        {
        //            return RedirectToAction("Error404", "Home", new { code = User.Code });
        //        }
        //    }
        //    catch
        //    {
        //        return RedirectToAction("Error500", "Home", new { code = User.Code });
        //    }
        //}

        //[HttpPost]
        //public ActionResult DownloadExport(string fileId)
        //{
        //    try
        //    {
        //        ResponseFileInfo fileResult = new ResponseFileInfo();
        //        fileResult = fileSV.DownloadExport(fileId);

        //        if (fileResult.Error == FileError.Success)
        //        {
        //            Response.Clear();
        //            Response.ClearHeaders();
        //            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileResult.Name + "\"");
        //            Response.AddHeader("Content-Transfer-Encoding", "Binary");
        //            Response.BufferOutput = false;
        //            var contentType = "application/octet-stream";
        //            return new FileStreamResult(fileResult.Stream, contentType);
        //        }
        //        else
        //        {
        //            return RedirectToAction("Error404", "Home", new { code = User.Code });
        //        }
        //    }
        //    catch
        //    {
        //        return RedirectToAction("Error500", "Home", new { code = User.Code });
        //    }
        //}

        //[HttpPost]
        //public ActionResult Update(List<HttpPostedFileBase> file, string fileId)
        //{
        //    var result = true;
        //    var message = string.Empty;
        //    try
        //    {
        //        ResponseFileInfo fileResult = new ResponseFileInfo();
        //        fileResult = fileSV.Update(file[0], fileId);
        //        if (fileResult.Error != FileError.Success)
        //        {
        //            message = fileSV.GetMessageError(fileResult.Error);
        //        }
        //        result = string.IsNullOrEmpty(message);
        //    }
        //    catch
        //    {
        //        result = false;
        //    }
        //    return Json(new { success = result, message = message }, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public ActionResult Update(HttpPostedFileBase file, string fileId)
        {
            var result = true;
            var message = string.Empty;
            try
            {
                ResponseFileInfo fileResult = new ResponseFileInfo();
                fileResult = FileUploadService.Update(file, fileId);
                if (fileResult.Error != FileError.Success)
                {
                    message = FileUploadService.GetMessageError(fileResult.Error);
                }
                result = string.IsNullOrEmpty(message);
            }
            catch
            {
                result = false;
            }
            return Json(new { success = result, message = message }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Hàm xuất dữ liệu trên gridpanel dưới dạng Excl, Csv, Xml
        /// </summary>
        /// <param name="json">dữ liệu xuất dưới dạng json</param>
        /// <param name="name">Tên file xuất</param>
        /// <param name="format">định dạng file cần xuất: ví dụ: xls | csv, xml</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Export(string json, string nameFile = "Tệp xuất", string format = default(string))
        {
            try
            {
                Ext.Net.SubmitHandler submitData = new Ext.Net.SubmitHandler(json);
                XmlNode xml = submitData.Xml;
                string contentType = string.Empty;
                byte[] resultFileContent = Encoding.UTF8.GetBytes(xml.OuterXml);
                switch (format)
                {
                    case "xml":
                        contentType = "application/xml";
                        break;
                    case "xls":
                    case "csv":
                        byte[] filecontent = ExcelExportHelper.ExportExcelJson(json);
                        return File(filecontent, "application/octet-stream", nameFile + DateTime.Today.ToString("dd/MM/yyyy hh:mm:tt") + ".xlsx");
                        //XslCompiledTransform transform = new XslCompiledTransform();
                        //if (format == "xls")
                        //{
                        //    contentType = "application/vnd.ms-excel";
                        //    transform.Load(Server.MapPath("~/Content/export/Excel.xsl"));
                        //}
                        //else
                        //{
                        //    contentType = "application/octet-stream";
                        //    transform.Load(Server.MapPath("~/Content/export/Csv.xsl"));
                        //}
                        //MemoryStream m = new MemoryStream(resultFileContent);
                        //XPathDocument xpathDoc = new XPathDocument(new StreamReader(m));
                        //StringBuilder resultString = new StringBuilder();
                        //SystemXML.XmlWriter writer = SystemXML.XmlWriter.Create(resultString);
                        //transform.Transform(xpathDoc, writer);
                        //resultFileContent = Encoding.UTF8.GetBytes(resultString.ToString());
                        //break;
                }
                var result = new FileContentResult(resultFileContent, contentType);
                result.FileDownloadName = string.Format(nameFile + ".{0}", format);
                return result;
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }
    }
}