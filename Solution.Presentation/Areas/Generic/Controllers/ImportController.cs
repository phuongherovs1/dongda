﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using systemIO = System.IO;
using systemnet = System.Net;

namespace iDAS.Presentation.Areas.Generic.Controllers
{
    public class ImportController : FrontController
    {
        //
        // GET: /Generic/Import/
        #region ImportData
        #region Các hàm cứng không sửa đổi
        public ActionResult Import()
        {
            return new Ext.Net.MVC.PartialViewResult { ViewName = "Import", ViewData = ViewData };
        }
        private DataTable ReadFileExcel(string directory)
        {
            FileStream fStream = new FileStream(directory, FileMode.Open);
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook(fStream);
            Aspose.Cells.Worksheet worksheet = workbook.Worksheets[0];
            DataTable datatable = worksheet.Cells.ExportDataTable(0, 0, worksheet.Cells.Rows.Count, worksheet.Cells.Columns.Count == 0 ? worksheet.Cells.MaxColumn+1 : worksheet.Cells.Columns.Count, true);
            fStream.Close();
            return datatable;
        }
        public ActionResult DataObjectImport(StoreRequestParameters parameters, string direction)
        {
            var result = new List<object>();
            if (!string.IsNullOrEmpty(direction))
            {
                try
                {
                    var table = ReadFileExcel(Server.MapPath(direction));
                    for (var i = 0; i < table.Rows.Count; i++)
                    {
                        var obj = new ExpandoObject() as IDictionary<string, object>;
                        for (int j = 0; j < table.Columns.Count; j++)
                        {
                            obj.Add("Column" + j, table.Rows[i][j]);
                        }
                        result.Add(obj);
                    }
                }
                catch (Exception e)
                {

                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = e.Message
                    });
                    return this.Direct();
                }
            }
            return this.Store(result);
        }
        public ActionResult SelectImportFile()
        {
            var fileUploadField = X.GetCmp<FileUploadField>("FileImportField");

            var direction = Common.Resource.FileImportUrl + Guid.NewGuid().ToString() + ".xlsx";
            if (fileUploadField.HasFile)
            {
                if (fileUploadField.PostedFile.ContentLength < 30 * 1024 * 1024 + 1)
                {
                    if (fileUploadField.FileName.Split('.').LastOrDefault().ToUpper().Equals("XLSX") || fileUploadField.FileName.Split('.').LastOrDefault().ToUpper().Equals("XLS"))
                    {
                        if (!systemIO.File.Exists(Server.MapPath(direction)))
                        {
                            fileUploadField.PostedFile.SaveAs(Server.MapPath(direction));
                        }
                    }
                    else
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING,
                            Title = "Thông báo",
                            Message = "Hệ thống chỉ hỗ trợ file có đuôi .xls và .xlsx!"
                        });
                        return this.Direct();
                    }
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = "Chỉ cho phép dung lượng import tối đa là 30MB!"
                    });
                    return this.Direct();
                }
            }
            else
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Không có thông tin cho file upload!"
                });
            }
            return this.Direct(direction);
        }
        [HttpGet]
        public ActionResult SettingImport(string direction)
        {
            ViewData["Direction"] = direction;
            try
            {
                if (direction != null)
                {
                    var table = ReadFileExcel(Server.MapPath(direction));
                    ViewData["NumberColumn"] = table.Columns.Count;
                }
                else
                {
                    ViewData["NumberColumn"] = 0;
                }
                return new Ext.Net.MVC.PartialViewResult { ViewName = "SettingImport", ViewData = ViewData };
            }
            catch (Exception)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Vui lòng lựa chọn tệp cần nhập liệu vào hệ thống!"
                });
                return this.Direct();
            }
        }
        #endregion

        #region Hàm thay đổi với các đối tượng bảng khác nhau
        [HttpPost]
        public ActionResult SettingImport(AssetImportObject data, Guid cateId, string direction)
        {
            try
            {
                if (!string.IsNullOrEmpty(direction))
                {
                    var table = ReadFileExcel(Server.MapPath(direction));
                    var objectImport = new List<AssetBO>();
                    for (var i = 0; i < table.Rows.Count; i++)
                    {
                        objectImport.Add(new AssetBO
                        {
                            AssetGroupID = cateId,
                            Name = table.Rows[i][data.Name].ToString(),
                            Code = table.Rows[i][data.Code].ToString(),
                            Quantity = double.Parse(table.Rows[i][data.Quantity].ToString()),
                            Unit = table.Rows[i][data.Unit].ToString()
                        });
                    }
                    var numberInportFail = AssetService.Import(objectImport);
                    systemIO.File.Delete(Server.MapPath(direction));
                    if (numberInportFail > 0)
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO,
                            Title = "Thông báo",
                            Message = "Có " + numberInportFail + " bản ghi không được nhập liệu do mã đã tồn tại. Vui lòng kiểm tra lại dữ liệu trước khi nhập liệu tự động!"
                        });
                        return this.Direct();
                    }
                    else
                    {
                        X.Msg.Show(new MessageBoxConfig
                        {
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.INFO,
                            Title = "Thông báo",
                            Message = "Nhập liệu vào hệ thống thành công!"
                        });
                        return this.Direct();
                    }
                }
                else
                {
                    X.Msg.Show(new MessageBoxConfig
                    {
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Title = "Thông báo",
                        Message = "Vui lòng lựa chọn tệp cần nhập liệu vào hệ thống!"
                    });
                    return this.Direct();
                }
            }
            catch (Exception ex)
            {
                X.Msg.Show(new MessageBoxConfig
                {
                    Buttons = MessageBox.Button.OK,
                    Icon = MessageBox.Icon.WARNING,
                    Title = "Thông báo",
                    Message = "Có lỗi trong quá trình nhập dữ liệu!"
                });
                return this.Direct();
            }
        }
        #endregion
        #endregion
        [HttpGet]
        public ActionResult Download(string fileName)
        {
            try
            {
                if (string.IsNullOrEmpty(fileName))
                    fileName = "template_asset.xlsx";
                var fullpath = AppDomain.CurrentDomain.BaseDirectory + "/Upload/TemplateImport/" + fileName;
                byte[] filedata = systemIO.File.ReadAllBytes(fullpath);
                string contentType = MimeMapping.GetMimeMapping(fullpath);
                var cd = new systemnet.Mime.ContentDisposition
                {
                    FileName = fileName,
                    Inline = true
                };
                Response.AddHeader("Content-Disposition", cd.ToString());
                return File(filedata, contentType);
            }
            catch (Exception)
            {
                return new EmptyResult();
            }
        }
    }
}