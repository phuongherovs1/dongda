﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Controllers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Statistical.Controllers
{
    public class DashBoardController : FrontController
    {
        public Ext.Net.MVC.PartialViewResult Index(string containerId)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                ViewName = "Index",
                ViewData = ViewData,
                WrapByScriptTag = false
            };
        }
        // public ActionResult LoadData(string node)
        //{
        //    try
        //    {
        //        NodeCollection nodes = new NodeCollection();
        //        var departmentID = node == "root" ? (Guid?)null : new Guid(Convert.ToString(node));
        //        var departments = DepartmentService.GetTreeDepartment(departmentID);
        //        foreach (var department in departments)
        //        {
        //            nodes.Add(createNodeDepartment(department));
        //        }
        //        return this.Content(nodes.ToJson());
        //    }
        //    catch (Exception ex)
        //    {
        //        return this.Direct();
        //    }
        //}
        public ActionResult LoadData(string node, string departmentId)
        {
            try
            {
                NodeCollection nodes = new NodeCollection();
                var depId = new Guid();
                if (node == "root" && departmentId != "")
                {
                    if (departmentId.Contains('_'))
                    {
                        var department = departmentId.Split('_').ToList();
                        if (department != null && department.Count == 2)
                        {
                            depId = new Guid(department[1]);
                        }
                    }
                    else
                    {
                        depId = new Guid(departmentId);
                    }
                    var departmentRoot = DepartmentService.GetDepartmentDetail(depId, checkParent: true);
                    nodes.Add(createNodeDepartment(departmentRoot));
                }
                else
                {
                    var departments = node == "root" ? DepartmentService.GetTreeDepartment(null)
                                    : DepartmentService.GetTreeDepartment(new Guid(Convert.ToString(node)));
                    foreach (var department in departments)
                    {
                        nodes.Add(createNodeDepartment(department));
                    }
                }
                return this.Content(nodes.ToJson());
            }
            catch (Exception ex)
            {
                return this.Direct();
            }
        }
        private Node createNodeDepartment(iDAS.Service.DepartmentBO department)
        {
            Node node = new Node();
            node.NodeID = department.Id.ToString();
            node.Text = " " + department.Name;
            node.IconCls = "x-fa fa-home";
            node.CustomAttributes.Add(new ConfigItem { Name = "ParentId", Value = department.ParentId.ToString(), Mode = ParameterMode.Value });
            node.Leaf = !department.IsParent;
            return node;
        }
        public ActionResult Document()
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            return PartialView();
        }
        public ActionResult Profile()
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            return PartialView();
        }
        public ActionResult Task()
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            return PartialView();
        }
        public ActionResult Quality()
        {
            var department = DepartmentService.GetDepartmentsByCurrentUser().FirstOrDefault();
            if (department != null)
            {
                var departmentName = department.Name;
                var departmentId = department.Id;
                ViewData["departmentName"] = departmentName;
                ViewData["departmentId"] = departmentId;
            }
            return PartialView();
        }
        public ActionResult Human()
        {
            return PartialView();
        }
        public ActionResult ChangeChart(string containerId, string chartName)
        {
            return new Ext.Net.MVC.PartialViewResult
            {
                ClearContainer = true,
                RenderMode = RenderMode.AddTo,
                ContainerId = containerId,
                WrapByScriptTag = false,
                ViewName = chartName
            };
        }
    }
}