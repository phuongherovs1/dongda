﻿using System.Web.Mvc;

namespace iDAS.Presentation.Areas.Statistical
{
    public class StatisticalAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Statistical";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Statistical_default",
                "Statistical/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}