﻿using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.Presentation.Common
{
    public class Role
    {
        [Role(Module = Module.System, Description = RoleDescription.Administrator)]
        public const string Administrator = "Quản trị hệ thống";
        [Role(Module = Module.Task, Description = RoleDescription.Assign)]
        public const string Assign = "Giao việc";
        [Role(Module = Module.Task, Description = RoleDescription.Reviewer)]
        public const string Reviewer = "Kiểm tra công việc";
        [Role(Module = Module.Task, Description = RoleDescription.Viewer)]
        public const string Viewer = "Theo dõi công việc";
        [Role(Module = Module.Task, Description = RoleDescription.Performer)]
        public const string Performer = "Thực hiện công việc";
        [Role(Module = Module.Document, Description = RoleDescription.ProcessDocumentCreater)]
        public const string ProcessDocumentCreater = "Thiết lập";
        [Role(Module = Module.Profile, Description = RoleDescription.ProcessProfileCreater)]
        public const string ProcessProfileCreater = "Thiết lập danh mục";
        [Role(Module = Module.Employee, Description = RoleDescription.EmployeeProfile)]
        public const string ViewEmployeeProfile = "Xem hồ sơ nhân sự";

        public static bool IsAdmin()
        {
            var AdminID = System.Configuration.ConfigurationManager.AppSettings["AdminID"];
            List<string> lstIDAdmin = new List<string>();
            if (!string.IsNullOrEmpty(AdminID))
            {
                if (AdminID.Contains(","))
                    lstIDAdmin = AdminID.Split(',').ToList();
                else
                    lstIDAdmin.Add(AdminID);
            }
            EmployeeService employeeService = new EmployeeService();
            var CurrentUser = employeeService.GetCurrentUser();
            if (lstIDAdmin.Contains(CurrentUser.Id.ToString()))
                return true;
            else
                return false;
        }
    }
    public class Roles
    {
        public const string ManagerUser = "ManagerUser";
        public const string ManagerSystem = "ManagerSystem";
        public const string ManagerTimekeeper = "ManagerTimekeeper";
        public const string EmployeeManage = "EmployeeManage";
        public const string RecruitmentCriteria = "RecruitmentCriteria";
        public const string RecruitmentRequirement = "RecruitmentRequirement";
        public const string RecruitmentPlan = "RecruitmentPlan";
        public const string Recruitment = "Recruitment";
        public const string ProfileWorkTrial = "ProfileWorkTrial";
        public const string ProfileWorkTrialAuditManager = "ProfileWorkTrialAuditManager";
        public const string ProfileWorkTrialPropose = "ProfileWorkTrialPropose";
        public const string ProfileWorkTrialAuditEmployee = "ProfileWorkTrialAuditEmployee";
        public const string KPICategory = "KPICategory";
        public const string ManagerTask = "ManagerTask";
        public const string ManagerKPI = "ManagerKPI";
        public const string ListDocument = "ListDocument";
        public const string ManagerDocumentEdit = "ManagerDocumentEdit";
        public const string listProfile = "listProfile";
        public const string Organization = "Organization";
        public const string Position = "Position";
        public const string TrainingRequirement = "TrainingRequirement";
        public const string TrainingPlan = "TrainingPlan";
        public const string Practioners = "Practioners";
        public const string ResultTraining = "ResultTraining";
        public const string Question = "Question";
        public const string Quality = "Quality";
        public const string SMS = "SMS";
        public const string Botnet = "Botnet";
        public const string KPI = "KPI";
        public const string Asset = "Asset";
        public const string Risk = "Risk";
        public const string Statistical = "Statistical";
        public const string Workflow = "Workflow";
        public const string PermissionTimekeeping = "PermissionTimekeeping";
        public const string Explanation = "Explanation";
        public const string EmployeeTimeOff = "EmployeeTimeOff";
        public const string EmployeeWorkOut = "EmployeeWorkOut";
        public const string EmployeeOT = "EmployeeOT";
        public const string OffDay = "OffDay";
        public const string InfoSysList = "InfoSysList";
        public const string InfoSysAccessList = "InfoSysAccessList";
        public const string InfoSysRemoteAccessList = "InfoSysRemoteAccessList";
        public const string MainIndex = "MainIndex";
        public const string DispatchGo = "DispatchGo";
        public const string DispatchTo = "DispatchTo";
        public const string DispatchSave = "DispatchSave";
        public const string EmployeeSearch = "EmployeeSearch";
        public const string TimeKeeping = "TimeKeeping";
    }
    public class RoleDescription
    {
        public const string Administrator = "Quản trị hệ thống";
        public const string Assign = "Người giao việc";
        public const string Reviewer = "Người kiểm soát";
        public const string Viewer = "Người theo dõi";
        public const string Performer = "Người thực hiện";
        public const string ProcessDocumentCreater = "Thiết lập";
        public const string ProcessProfileCreater = "Thiết lập danh mục";
        public const string EmployeeProfile = "Quản lý nhân sự";
    }
    public class RoleAttribute : Attribute
    {
        public string Module { get; set; }
        public string Description { get; set; }
    }
}