﻿namespace iDAS.Presentation.Common
{
    public enum Operation
    {
        Create,
        Read,
        Update,
        Delete,
    }
}