﻿using Ext.Net;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;

namespace iDAS.Presentation.Common
{
    public class Utilities
    {
        public static void ShowMessageBox(string title = "Thông báo", string message = "", MessageBox.Icon icon = MessageBox.Icon.NONE, MessageBox.Button button = MessageBox.Button.OK)
        {
            MessageBox msg = new MessageBox();
            msg.Configure(new MessageBoxConfig()
            {
                Title = title,
                Message = message,
                Icon = icon,
                Buttons = button,
            });
            msg.Show();
        }
        public static DateTime FirstDayOfMonth()
        {
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        }
        public static DateTime LastDayOfMonth()
        {
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
        }
        public static DateTime StartOfWeek(DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }

        public static List<Ext.Net.CheckMenuItem> GetSourceMenu(Dictionary<int, string> source)
        {
            var menus = new List<Ext.Net.CheckMenuItem>();
            if (source != null)
            {
                for (int i = 0; i < source.Count(); i++)
                {
                    var item = source.ElementAt(i);
                    menus.Add(new Ext.Net.CheckMenuItem()
                    {
                        ItemID = item.Key.ToString(),
                        Text = item.Value,
                        IconCls = "x-fa fa-filter"
                    });
                }
            }
            return menus;
        }
        public static List<Ext.Net.CheckMenuItem> GetSourceMenuGuid(Dictionary<Guid, string> source)
        {
            var menus = new List<Ext.Net.CheckMenuItem>();
            if (source != null)
            {
                for (int i = 0; i < source.Count(); i++)
                {
                    var item = source.ElementAt(i);
                    menus.Add(new Ext.Net.CheckMenuItem()
                    {
                        ItemID = item.Key.ToString(),
                        Text = item.Value,
                        IconCls = "x-fa fa-home"
                    });
                }
            }
            return menus;
        }
        public static List<Ext.Net.ListItem> GetSelectItem(Dictionary<int, string> source)
        {
            var menus = new List<Ext.Net.ListItem>();
            for (int i = 0; i < source.Count(); i++)
            {
                var item = source.ElementAt(i);
                menus.Add(new Ext.Net.ListItem()
                {
                    Value = item.Key.ToString(),
                    Text = item.Value
                });
            }
            return menus;
        }
        public static List<Ext.Net.ListItem> GetSelectGuidItem(Dictionary<Guid, string> source)
        {
            var menus = new List<Ext.Net.ListItem>();
            for (int i = 0; i < source.Count(); i++)
            {
                var item = source.ElementAt(i);
                menus.Add(new Ext.Net.ListItem()
                {
                    Value = item.Key.ToString(),
                    Text = item.Value
                });
            }
            return menus;
        }
        public static List<Ext.Net.ListItem> GetSelectItem(Dictionary<iDAS.Service.Common.Resource.DocumentStatus, string> source)
        {
            var menus = new List<Ext.Net.ListItem>();
            for (int i = 0; i < source.Count(); i++)
            {
                var item = source.ElementAt(i);
                menus.Add(new Ext.Net.ListItem()
                {
                    Value = item.Key.ToString(),
                    Text = item.Value
                });
            }
            return menus;
        }
        public static List<Ext.Net.ListItem> GetSelectItemText(Dictionary<string, string> source)
        {
            var menus = new List<Ext.Net.ListItem>();
            for (int i = 0; i < source.Count(); i++)
            {
                var item = source.ElementAt(i);
                menus.Add(new Ext.Net.ListItem()
                {
                    Value = item.Key.ToString(),
                    Text = item.Value
                });
            }
            return menus;
        }

        public static List<object> GetSelectItemToObject(Dictionary<int, string> source)
        {
            var menus = new List<object>();
            for (int i = 0; i < source.Count(); i++)
            {
                var item = source.ElementAt(i);
                menus.Add(new
                {
                    Value = item.Key.ToString(),
                    Text = item.Value
                });
            }
            return menus;
        }
        public static bool ValidatePassword(string password, out string ErrorMessage)
        {
            var input = password;
            ErrorMessage = string.Empty;


            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMiniMaxChars = new Regex(@".{8,150}");
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

            if (string.IsNullOrWhiteSpace(input))
            {
                ErrorMessage = Common.Resource.ValidatePassword;
                return false;
            }
            else
            if (!hasLowerChar.IsMatch(input))
            {
                ErrorMessage = Common.Resource.ValidatePassword;
                return false;
            }
            else if (!hasUpperChar.IsMatch(input))
            {
                ErrorMessage = Common.Resource.ValidatePassword;
                return false;
            }
            else if (!hasMiniMaxChars.IsMatch(input))
            {
                ErrorMessage = Common.Resource.ValidatePassword;
                return false;
            }
            else if (!hasNumber.IsMatch(input))
            {
                ErrorMessage = Common.Resource.ValidatePassword;
                return false;
            }

            else if (!hasSymbols.IsMatch(input))
            {
                ErrorMessage = Common.Resource.ValidatePassword;
                return false;
            }
            else
            {
                return true;
            }
        }
        public static bool IsPhoneNumber(string number, out string ErrorMessage)
        {
            ErrorMessage = string.Empty;
            if (string.IsNullOrWhiteSpace(number))
            {
                ErrorMessage = Common.Resource.ValidatePhone;
                return false;
            }
            else
            if (!Regex.Match(number, @"[0-9]+").Success)
            {
                ErrorMessage = Common.Resource.ValidatePhone;
                return false;
            }
            if (!Regex.Match(number, @".{6,20}").Success)
            {
                ErrorMessage = Common.Resource.ValidatePhone;
                return false;
            }
            else
            {
                return true;
            }
        }
        public static string[] Name = new[] { "Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy" };
        public static string GetNameDayOfWeek(DateTime date)
        {
            int day = (int)date.DayOfWeek;
            return Name[day];
        }
        public static List<string> ConvertDataIDToString(DataTable dataTable)
        {
            
            List<string> lstID = new List<string>();
            if(dataTable != null)
            {
                foreach (DataRow item in dataTable.Rows)
                {
                    lstID.Add(item["HumanEmployeeId"].ToString());
                }
            }
            return lstID;
        }
        public static void ExportDataTableToExcel(DataTable dt, string filename)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "UTF-8";
            HttpContext.Current.Response.HeaderEncoding = System.Text.Encoding.UTF8;
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Unicode;
            HttpContext.Current.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + filename + ".xls");
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    System.Web.UI.WebControls.GridView gv = new System.Web.UI.WebControls.GridView();
                    gv.DataSource = dt;
                    gv.DataBind();
                    gv.RenderControl(htw);
                    string swString = sw.ToString();
                    if(swString.Contains("<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">"))
                    {
                        swString = swString.Replace("<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">", "<table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\"><tr><td colspan=\""+ dt.Columns.Count + "\" style=\"text-align: center;font-weight: bold;\">"+ filename.ToUpper() + "</td></tr>");
                    }
                    HttpContext.Current.Response.Write(swString);
                    HttpContext.Current.Response.End();
                }
            }
        }
    }
}