﻿using System.Collections.Generic;

namespace iDAS.Presentation.Common
{
    public class Module
    {
        public const string System = "Quản lý hệ thống";
        public const string Department = "Quản lý tổ chức";
        public const string Human = "Quản lý nhân sự";
        public const string Task = "Quản lý công việc";
        public const string Document = "Quản lý tài liệu";
        public const string Profile = "Quản lý hồ sơ";
        public const string Quality = "Quản lý chất lượng";
        public const string Asset = "Quản lý tài sản";
        public const string Risk = "Quản lý rủi ro";
        public const string Employee = "Quản lý cán bộ";
        public const string Dispatch = "Quản lý công văn";
        public const string SMS = "Quản lý tin nhắn rác";
        public const string BotnetInfection = "Quản lý máy nhiễm độc";
        public static Dictionary<string, string> ModuleKeyValue = new Dictionary<string, string>()
        {
          { "Quản lý hệ thống","System"  },
          { "Quản lý tổ chức" , "Department"},
          { "Quản lý nhân sự","Human" },
          { "Quản lý công việc", "Task" },
          { "Quản lý tài liệu",  "Document" },
          { "Quản lý hồ sơ","Profile" },
          { "Quản lý chất lượng","Quality" },
          { "Quản lý tài sản","Asset" },
          { "Quản lý rủi ro", "Risk"},
          { "Quản lý cán bộ","Employee"},
          { "Quản lý công văn","Dispatch" },
          { "Quản lý tin nhắn rác","SMS" },
          { "Quản lý máy nhiễm độc","BotnetInfection" }
        };
    }
}