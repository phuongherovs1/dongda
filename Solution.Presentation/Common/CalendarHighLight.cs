﻿namespace iDAS.Presentation.Common
{
    public class CalendarHighLight
    {
        public string Title { get; set; }
        public string Date { get; set; }
        public int StyleID { get; set; }
    }
}