﻿namespace iDAS.Presentation
{
    public class UrlResourceNotifies
    {
        public const string ApprovalSuggestBorrow = "/Profile/ProfileSuggestBorrow/Approval";
        public const string AssignTask = "/Task/Perform/Report";
        public const string ReportTask = "/Task/Manage/Control";
        public const string TaskFrom = "/Task/Manage/Form";
        public const string CommunicationTask = "/Task/Communication/TaskCommunication";
        public const string SendProfileDestroy = "/Profile/DestroySuggest/FormApproval";
        public const string SendProfileWorkTrial = "/Organization/ProfileWorkTrialAuditManager/EditAuditForm";
        public const string SendProfileWorkTrialPropose = "/Organization/ProfileWorkTrialPropose/frmApprove";
        public const string SendProfileWorkTrialResult = "Organization/ProfileWorkTrialAuditEmployee/AuditForm";
        public const string ApprovedProfileDestroy = "/Profile/DestroyReport/Form";
        public const string DeliveredAssign = "/Profile/ProfileComponent/Index";
        public const string ApprovalAuditPlanDetail = "/Quality/AuditPlanDetail/Approval";
        public const string AuditPlanDetail = "/Quality/AuditPlanDetail/Detail";
        public const string ApprovalTrainingSuggest = "/Organization/TrainingRequirement/FrmApproval";
        public const string ApprovalRecruitmentRequest = "/Organization/RecruitmentRequirement/frmApprove?";
        public const string DetailTrainingSuggest = "/Organization/TrainingRequirement/DetailApproval";
        public const string ApprovalTrainingPlan = "/Organization/TrainingPlan/FrmApproval";
        public const string DetailTrainingPlan = "/Organization/TrainingPlan/DetailApproval";
        public const string ApprovalMeeting = "/Quality/Meeting/Form";
        public const string ApprovalRecruitmentPlan = "/Organization/RecruitmentPlan/Update?";
        public const string SendPointKPI = "/KPI/KPIForTasks/NotifyPointEmployee?";
        public const string Explanation = "/TimeKeeping/Explanation/ResultListExplanation?";
        public const string WorkOut = "/TimeKeeping/EmployeeWorkOut/Form?";
        public const string TimeOff = "/TimeKeeping/EmployeeTimeOff/Form?";
        public const string WorkOT = "/TimeKeeping/EmployeeOT/Form?";
        public const string RequestWriteByAssigner = "/Document/Assign/RequestWriteByAssigner?";
        public const string RequestCheckByAssigner = "/Document/Responsibility/CheckDocument?";
        public const string RequestApproveByAssigner = "/Document/Responsibility/ApprovalDocument?";
        public const string RequestPublishByAssigner = "/Document/Publish/InfoRequestPublish?";
        public const string RequestArchivesByAssigner = "/Document/Archive/FormArchive?";
    }
    public class UrlResourceRoutine
    {
        public const string QualityMeetingPlan = "";
        public const string QualityAuditPlan = "/Quality/Explanation/Create?planId=";
    }
}