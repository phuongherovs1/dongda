﻿namespace iDAS.Presentation
{
    public class TitleResourceNotifies
    {
        public const string ApprovalSuggestBorrow = "gửi đề nghị mượn hồ sơ";
        public const string AssignTask = "đã giao một công việc";
        public const string AssignRequetsTask = "đã yêu cầu";
        public const string ReportTask = "đã báo cáo công việc";
        public const string CommunicationTask = "có một trao đổi công việc";
        public const string SendProfileDestroy = "gửi duyệt đề nghị hủy hồ sơ";
        public const string SendProfileWorkTrial = "gửi yêu cầu đánh giá";
        public const string SendProfileWorkTrialResult = "gửi yêu cầu tự đánh giá";
        public const string SendApproveWorkTrial = "gửi duyệt đề xuất tuyển dụng";
        public const string ApprovedProfileDestroy = "đã duyệt đề nghị hủy hồ sơ";
        public const string DeliveredAssign = "đã giao cập nhật thành phần hồ sơ";
        public const string ApprovalQualiyPlanAudit = "gửi duyệt kế hoạch đánh giá";
        public const string ApprovalPlanAudit = "đã duyệt kế hoạch đánh giá";
        public const string SendApprovalTrainingSuggest = "gửi duyệt Yêu cầu đào tạo";
        public const string ApprovalTrainingSuggest = "đã duyệt Yêu cầu đào tạo";
        public const string SendApprovalTrainingPlan = "gửi duyệt kế hoạch đào tạo";
        public const string ApprovalTrainingPlan = "đã duyệt kế hoạch đào tạo";
        public const string SendApprovalRecruitmentPlan = "Gửi duyệt kế hoạch tuyển dụng";
        public const string SendApprovalRecruitmentRequest = "Gửi duyệt yêu cầu tuyển dụng";
        public const string SendApprovalApprovalMeeting = "Gửi duyệt kế hoạch họp";
        public const string SendPointKPI = "đã cho điểm";
        public const string Explanation = "đã yêu cầu giải trình";
        public const string WorkOut = "đã gửi yêu cầu xin làm việc bên ngoài";
        public const string WorkOT = "đã gửi yêu cầu xin làm thêm giờ(OT)";
        public const string TimeOff = "đã gửi yêu cầu xin nghỉ phép";
        public const string RequestWriteDocument = "đã gửi yêu cầu soạn thảo tài liệu";
        public const string RequestCheckDocument = "đã gửi yêu cầu kiểm tra tài liệu";
        public const string RequestApproveDocument = "đã gửi yêu cầu phê duyệt tài liệu";
        public const string RequestPorDocument = "đã gửi yêu cầu ban hành tài liệu";
        public const string RequestArchivesDocument = "đã gửi yêu cầu lưu chữ tài liệu";
    }
}