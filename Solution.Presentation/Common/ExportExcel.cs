﻿using iDAS.Service.Common;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace iDAS.Presentation.Common
{
    public class ExcelExportHelper
    {
        public static string ExcelContentType
        {
            get
            { return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; }
        }

        public static DataTable ListToDataTable<T>(List<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable dataTable = new DataTable();

            for (int i = 0; i < properties.Count; i++)
            {
                PropertyDescriptor property = properties[i];
                dataTable.Columns.Add(property.Name, Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType);
            }

            object[] values = new object[properties.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = properties[i].GetValue(item);
                }

                dataTable.Rows.Add(values);
            }
            return dataTable;
        }
        public static byte[] ExportExcel(DataTable dataTable, Dictionary<string, string> cellvalue, string heading = "", bool showSrNo = false, params string[] columnsToTake)
        {

            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.Add(String.Format("{0} Data", heading));
                int startRowFrom = (cellvalue == null) ? 1 : (cellvalue.Count + 1);
                startRowFrom = startRowFrom + (String.IsNullOrEmpty(heading) ? 0 : 1);
                if (showSrNo)
                {
                    DataColumn dataColumn = dataTable.Columns.Add("STT", typeof(int));
                    dataColumn.SetOrdinal(0);
                    int index = 1;
                    foreach (DataRow item in dataTable.Rows)
                    {
                        item[0] = index;
                        index++;
                    }
                }

                //format font
                workSheet.Cells[1, 1, startRowFrom + dataTable.Rows.Count, dataTable.Columns.Count].Style.Font.SetFromFont(new Font("Times New Roman", 12));
                // add the content into the Excel file  
                workSheet.Cells["A" + startRowFrom].LoadFromDataTable(dataTable, true);

                // autofit width of cells with small content  
                int columnIndex = 1;
                workSheet.Column(columnIndex).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                workSheet.Column(columnIndex).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                foreach (DataColumn column in dataTable.Columns)
                {
                    ExcelRange columnCells = workSheet.Cells[workSheet.Dimension.Start.Row, columnIndex, workSheet.Dimension.End.Row, columnIndex];
                    if (columnCells.Value.ToString().Count() < 150)
                    {
                        workSheet.Column(columnIndex).AutoFit();
                    }
                    columnIndex++;
                }

                // format header - bold, yellow on black  
                for (int k = 1; k <= startRowFrom + dataTable.Rows.Count; k++)
                {
                    workSheet.Row(k).Height = 24;
                }
                using (ExcelRange r = workSheet.Cells[startRowFrom, 1, startRowFrom, dataTable.Columns.Count])
                {
                    r.Style.Font.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Font.Bold = true;
                    r.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    r.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#1fb5ad"));
                }

                // format cells - add borders  
                using (ExcelRange r = workSheet.Cells[startRowFrom, 1, startRowFrom + dataTable.Rows.Count, dataTable.Columns.Count])
                {
                    r.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    r.Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                }
                if (cellvalue != null && cellvalue.Count > 0)
                {
                    foreach (var item in cellvalue)
                    {
                        workSheet.Cells[item.Key].Value = item.Value;
                        workSheet.Cells[item.Key + ":I2"].Merge = true;
                    }
                }
                if (!String.IsNullOrEmpty(heading))
                {
                    workSheet.Cells["B1"].Value = heading;
                    workSheet.Cells["B1"].Style.Font.Size = 15;
                    workSheet.Cells["B1:I1"].Merge = true;
                    workSheet.Cells["B1"].Style.Font.Bold = true;
                }

                result = package.GetAsByteArray();
            }

            return result;
        }
        public static byte[] ExportExcelOrigin(DataTable dataTable, string heading = "", bool showSrNo = false, params string[] columnsToTake)
        {

            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.Add(String.Format("{0} Data", heading));
                int startRowFrom = String.IsNullOrEmpty(heading) ? 1 : 3;

                if (showSrNo)
                {
                    DataColumn dataColumn = dataTable.Columns.Add("#", typeof(int));
                    dataColumn.SetOrdinal(0);
                    int index = 1;
                    foreach (DataRow item in dataTable.Rows)
                    {
                        item[0] = index;
                        index++;
                    }
                }


                // add the content into the Excel file  
                workSheet.Cells["A" + startRowFrom].LoadFromDataTable(dataTable, true);

                // autofit width of cells with small content  
                int columnIndex = 1;
                foreach (DataColumn column in dataTable.Columns)
                {
                    ExcelRange columnCells = workSheet.Cells[workSheet.Dimension.Start.Row, columnIndex, workSheet.Dimension.End.Row, columnIndex];
                    //int maxLength = columnCells.Max(cell => cell.Value.ToString().Count());
                    if (columnCells.Value.ToString().Count() < 150)
                    {
                        workSheet.Column(columnIndex).AutoFit();
                    }
                    columnIndex++;
                }

                // format header - bold, yellow on black  
                using (ExcelRange r = workSheet.Cells[startRowFrom, 1, startRowFrom, dataTable.Columns.Count])
                {
                    r.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    r.Style.Font.Bold = true;
                    r.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    r.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#1fb5ad"));
                }

                // format cells - add borders  
                using (ExcelRange r = workSheet.Cells[startRowFrom + 1, 1, startRowFrom + dataTable.Rows.Count, dataTable.Columns.Count])
                {
                    r.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    r.Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                }

                // removed ignored columns  
                for (int i = dataTable.Columns.Count - 1; i >= 0; i--)
                {
                    if (i == 0 && showSrNo)
                    {
                        continue;
                    }
                    if (!columnsToTake.Contains(dataTable.Columns[i].ColumnName))
                    {
                        workSheet.DeleteColumn(i + 1);
                    }
                }

                if (!String.IsNullOrEmpty(heading))
                {
                    workSheet.Cells["A1"].Value = heading;
                    workSheet.Cells["A1"].Style.Font.Size = 20;

                    workSheet.InsertColumn(1, 1);
                    workSheet.InsertRow(1, 1);
                    workSheet.Column(1).Width = 5;
                }

                result = package.GetAsByteArray();
            }

            return result;
        }
        public static byte[] ExportExcelJson(string json, string heading = "", bool showSrNo = false)
        {

            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                DataTable dataTable = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));
                ExcelWorksheet workSheet = package.Workbook.Worksheets.Add(String.Format("{0} Data", heading));
                int startRowFrom = String.IsNullOrEmpty(heading) ? 1 : 3;

                if (showSrNo)
                {
                    DataColumn dataColumn = dataTable.Columns.Add("#", typeof(int));
                    dataColumn.SetOrdinal(0);
                    int index = 1;
                    foreach (DataRow item in dataTable.Rows)
                    {
                        item[0] = index;
                        index++;
                    }
                }


                // add the content into the Excel file  
                workSheet.Cells["A" + startRowFrom].LoadFromDataTable(dataTable, true);

                // autofit width of cells with small content  
                int columnIndex = 1;
                foreach (DataColumn column in dataTable.Columns)
                {
                    ExcelRange columnCells = workSheet.Cells[workSheet.Dimension.Start.Row, columnIndex, workSheet.Dimension.End.Row, columnIndex];
                    //int maxLength = columnCells.Max(cell => cell.Value.ToString().Count());
                    try
                    {
                        if (columnCells.Value.ToString().Count() < 150)
                        {
                            workSheet.Column(columnIndex).AutoFit();
                        }
                        columnIndex++;
                    }
                    catch (Exception e) { }
                }

                // format header - bold, yellow on black  
                using (ExcelRange r = workSheet.Cells[startRowFrom, 1, startRowFrom, dataTable.Columns.Count])
                {
                    r.Style.Font.Color.SetColor(System.Drawing.Color.White);
                    r.Style.Font.Bold = true;
                    r.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    r.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#1fb5ad"));
                }

                // format cells - add borders  
                using (ExcelRange r = workSheet.Cells[startRowFrom + 1, 1, startRowFrom + dataTable.Rows.Count, dataTable.Columns.Count])
                {
                    r.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    r.Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    r.Style.Border.Top.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Left.Color.SetColor(System.Drawing.Color.Black);
                    r.Style.Border.Right.Color.SetColor(System.Drawing.Color.Black);
                }

                if (!String.IsNullOrEmpty(heading))
                {
                    workSheet.Cells["A1"].Value = heading;
                    workSheet.Cells["A1"].Style.Font.Size = 20;

                    workSheet.InsertColumn(1, 1);
                    workSheet.InsertRow(1, 1);
                    workSheet.Column(1).Width = 5;
                }

                result = package.GetAsByteArray();
            }

            return result;
        }


    }
    public static class ExcelExport
    {
      
        public static bool ExportSummaryRelityEmployee(this Controller controllers, List<string> IDEmployee = null, string FileName = "")
        {
            StringBuilder stringBuilder = new StringBuilder(@"<table id='viewdatagriview' style='width:100%' cellpadding='0' cellspacing='0'>
                            <thead>
                            <tr>
                                <th colspan='16' style='text-align:center;border:0;padding-bottom:50px'>
                                    THỐNG KÊ THỰC TRẠNG ĐỘI NGŨ VIÊN CHỨC PHÒNG TƯ VẤN ĐÀO TẠO
                                </th>
                            </tr>
                             <tr><th colspan='16'></th></tr>
                             <tr><th colspan='16'></th></tr>
                            <tr>
                                <th  style='border:0.5px solid;'  rowspan='2'>Số TT</th>
                                <th  style='border:0.5px solid;'  rowspan='2'>Đơn vị/Họ tên</th>
                                <th  style='border:0.5px solid;'  colspan='2'>Ngày tháng năm sinh</th>
                                <th  style='border:0.5px solid;'  rowspan='2'>trức vụ trức danh</th>
                                <th  style='border:0.5px solid;'  rowspan='2'>Các nhiệm vụ đảm nhận</th>
                                <th  style='border:0.5px solid;'  colspan='2'>Năm tuyển dụng</th>
                                <th  style='border:0.5px solid;'  rowspan='2'>Ngạch (Chức danh nghề nghiệp) hiện đang giữ</th>
                                <th  style='border:0.5px solid;'  colspan='4'>Trình độ chuyên môn cao nhất</th>
                                <th  style='border:0.5px solid;'  rowspan='2'>Trình độ ngoại ngữ</th>
                                <th  style='border:0.5px solid;'  rowspan='2'>Trình độ tin học</th>
                                <th  style='border:0.5px solid;'  rowspan='2'>Chứng chỉ, bồi dưỡng nghiệp vụ</th>
                            </tr>
                            <tr>
                                <th style='border:0.5px solid;'>Nam</th>
                                <th style='border:0.5px solid;'>Nữ</th>
                                <th style='border:0.5px solid;'>Vào cơ quan nhà nước</th>
                                <th style='border:0.5px solid;'>Vào đơn vị đang làm việc</th>
                                <th style='border:0.5px solid;'>Trình độ đào tạo</th>
                                <th style='border:0.5px solid;'>Chuyên ngành đào tạo</th>
                                <th style='border:0.5px solid;'>Hệ đào tạo</th>
                                <th style='border:0.5px solid;'>Trường đào tạo</th>
                            </tr>
                              <tr>
                                <th style='border:0.5px solid;'>1</th>
                                <th style='border:0.5px solid;'>2</th>
                                <th style='border:0.5px solid;'>3</th>
                                <th style='border:0.5px solid;'>4</th>
                                <th style='border:0.5px solid;'>5</th>
                                <th style='border:0.5px solid;'>6</th>
                                <th style='border:0.5px solid;'>7</th>
                                <th style='border:0.5px solid;'>8</th>
                                <th style='border:0.5px solid;'>9</th>
                                <th style='border:0.5px solid;'>10</th>
                                <th style='border:0.5px solid;'>11</th>
                                <th style='border:0.5px solid;'>12</th>
                                <th style='border:0.5px solid;'>13</th>
                                <th style='border:0.5px solid;'>14</th>
                                <th style='border:0.5px solid;'>15</th>
                                <th style='border:0.5px solid;'>16</th>
                            </tr>
                        </thead>
                        <tbody>");
            StringBuilder ListEmployeeid = new StringBuilder(""); ;
            if (IDEmployee != null)
            {
                if (IDEmployee.Count == 0)
                {
                    Export(controllers, stringBuilder.ToString(), (FileName == "" ? "FileEXport" : FileName));
                    return true;
                }
                ListEmployeeid.Append("(");
                foreach (var item in IDEmployee)
                {
                    ListEmployeeid.Append("'" + item + "',");
                }
                ListEmployeeid.Remove(ListEmployeeid.Length - 1, 1);
                ListEmployeeid.Append(")");

            }

            DataSet dataSet = DataExport.ExportSummaryRelityEmployee(ListEmployeeid.ToString());
            DataTable ListEmployeee = dataSet.Tables[0];
            DataTable ListTitle = dataSet.Tables[1];
            DataTable Listdiploma = dataSet.Tables[2];
            DataTable WorkExperiences = dataSet.Tables[3];
            DataTable Certificates = dataSet.Tables[4];
            DataTable EmployeeWorkExperiences = dataSet.Tables[5];

            int index = 0;
            foreach (DataRow item in ListEmployeee.Rows)
            {
                index++;
                var Employeediplomas = Listdiploma.Select("Employeeid='" + item["id"] + "'");
                var EmployeeTitle = ListTitle.Select("Employeeid='" + item["id"] + "'");
                var EmployeeCertificates = Certificates.Select("HumanEmployeeId='" + item["id"] + "'");
                var Experiencesdate = EmployeeWorkExperiences.Select("HumanEmployeeId='" + item["id"] + "'");
                string StartDate = string.Empty;
                foreach (DataRow dataRow in Experiencesdate)
                {
                    StartDate = (dataRow["StartDate"] == DBNull.Value ? "" : ((DateTime)dataRow["StartDate"]).ToString("dd/MM/yyyy"));
                }
                string title = "";
                var sex = Convert.ToInt32(item["sex"]);
                var DateOfBirth = item["DateOfBirth"] == DBNull.Value ? "" : ((DateTime)item["DateOfBirth"]).ToString("dd/MM/yyyy");
                foreach (DataRow dataRow in EmployeeTitle)
                {
                    title += dataRow["Nametitle"].ToString() + "  ";
                }
                var EmployeeExperiences = WorkExperiences.Select("Employeeid='" + item["id"] + "'");
                string Experiences = string.Empty;
                foreach (DataRow dataRow in EmployeeExperiences)
                {
                    Experiences += dataRow["JobDescription"].ToString() + "  ";
                }
                string SCertificates = string.Empty;
                foreach (DataRow dataRow in EmployeeCertificates)
                {
                    SCertificates += dataRow["Name"].ToString() + "  ";
                }
                if (Employeediplomas.Length > 1)
                {

                    stringBuilder.Append("<tr><td  rowspan = '" + Employeediplomas.Length + "'  style='border:0.5px solid;'>" + index + "</td>");
                    stringBuilder.Append("<td  rowspan = '" + Employeediplomas.Length + "'  style='border:0.5px solid;'>" + item["Name"] + "</td>");
                    stringBuilder.Append("<td  rowspan = '" + Employeediplomas.Length + "'  style='border:0.5px solid;'>" + (sex == 1 ? DateOfBirth : "") + "</td>");
                    stringBuilder.Append("<td  rowspan = '" + Employeediplomas.Length + "'  style='border:0.5px solid;'>" + (sex == 2 ? DateOfBirth : "") + "</td>");
                    stringBuilder.Append("<td  rowspan = '" + Employeediplomas.Length + "'  style='border:0.5px solid;'>" + title + "</td>");
                    stringBuilder.Append("<td  rowspan = '" + Employeediplomas.Length + "'  style='border:0.5px solid;'>" + Experiences + "</td>");
                    stringBuilder.Append("<td  rowspan = '" + Employeediplomas.Length + "'  style='border:0.5px solid;'></td>");
                    stringBuilder.Append("<td  rowspan = '" + Employeediplomas.Length + "'  style='border:0.5px solid;'>" + StartDate + "</td>");
                    stringBuilder.Append("<td  rowspan = '" + Employeediplomas.Length + "'  style='border:0.5px solid;'></td>");
                    stringBuilder.Append("<td  style='border:0.5px solid;'>" + Employeediplomas[0]["levelName"] + "</td>");
                    stringBuilder.Append("<td  style='border:0.5px solid;'>" + Employeediplomas[0]["MajorName"] + "</td>");
                    stringBuilder.Append("<td  style='border:0.5px solid;'>" + Employeediplomas[0]["FormOfTrainningName"] + "</td>");
                    stringBuilder.Append("<td  style='border:0.5px solid;'>" + Employeediplomas[0]["placeName"] + "</td>");
                    stringBuilder.Append("<td  rowspan = '" + Employeediplomas.Length + "'  style='border:0.5px solid;'></td>");
                    stringBuilder.Append("<td  rowspan = '" + Employeediplomas.Length + "'  style='border:0.5px solid;'></td>");
                    stringBuilder.Append("<td  rowspan = '" + Employeediplomas.Length + "'style='border:0.5px solid;'>" + SCertificates + "</td></tr>");
                    for (int i = 1; i < Employeediplomas.Length; i++)
                    {
                        stringBuilder.Append("<tr><td style='border:0.5px solid;'>" + Employeediplomas[i]["levelName"] + "</td>");
                        stringBuilder.Append("<td style='border:0.5px solid;'>" + Employeediplomas[i]["MajorName"] + "</td>");
                        stringBuilder.Append("<td style='border:0.5px solid;'>" + Employeediplomas[i]["FormOfTrainningName"] + "</td>");
                        stringBuilder.Append("<td style='border:0.5px solid;'>" + Employeediplomas[i]["placeName"] + "</td></tr>");
                    }
                }
                else
                {
                    stringBuilder.Append("<tr><td style='border:0.5px solid;'>" + index + "</td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'>" + item["Name"] + "</td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'>" + (sex == 1 ? DateOfBirth : "") + "</td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'>" + (sex == 2 ? DateOfBirth : "") + "</td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'>" + title + "</td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'>" + Experiences + "</td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'></td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'>" + StartDate + "</td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'></td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'>" + (Employeediplomas.Length > 0 ? Employeediplomas[0]["levelName"] : "") + "</td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'>" + (Employeediplomas.Length > 0 ? Employeediplomas[0]["MajorName"] : "") + "</td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'>" + (Employeediplomas.Length > 0 ? Employeediplomas[0]["FormOfTrainningName"] : "") + "</td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'>" + (Employeediplomas.Length > 0 ? Employeediplomas[0]["placeName"] : "") + "</td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'></td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'></td>");
                    stringBuilder.Append("<td style='border:0.5px solid;'>" + SCertificates + "</td></tr>");
                }
            }

            stringBuilder.Append("</tbody></table>");
            Export(controllers, stringBuilder.ToString(), (FileName == "" ? "FileEXport" : FileName));
            return true;
        }
        private static void Export(Controller controller, string data, string filename)
        {
            controller.Response.ContentType = "application/vnd.ms-excel";
            controller.Response.AddHeader("Content-Disposition", "attachment;filename=" + filename + ".xls");
            StringBuilder TableHTML = new StringBuilder(@"<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x ='urn:schemas-microsoft-com:office:excel'xmlns ='http://www.w3.org/TR/REC-html40'>
            <head>
              <!--[if gte mso 9]>
                    <xml>
                     <x:ExcelWorkbook>
                       <x:ExcelWorksheets>
                          <x:ExcelWorksheet>
                             <x:Name>Danh sách cán bộ</x:Name>
                                 <x:WorksheetOptions>
                                   <x:DisplayGridlines/>
                                   </x:WorksheetOptions>
                                  </x:ExcelWorksheet>
                                 </x:ExcelWorksheets>
                                 </x:ExcelWorkbook>
                                 </xml>
                              <![endif]-->
                          </head>
                          <body>");
            TableHTML.Append(data);
            TableHTML.Append("</body></html>");
            controller.Response.Write(TableHTML.ToString());
            controller.Response.End();
        }

    }
}