﻿namespace iDAS.Presentation.Common
{
    public class Resource
    {
        public static string Brandname = "1";
        public static string LongPhone = "2";
        public static string HeadPhone = "3";
        public static string FileImportUrl = "/Upload/";
        #region Button
        public const string BrowseImage = "Chọn ảnh...";
        public const string Avatar = "Ảnh đại diện";
        public const string Create = "Thêm mới";
        public const string Update = "Cập nhật";
        public const string Delete = "Xóa";
        public const string Detail = "Chi tiết";
        public const string Assign = "Giao việc";
        public const string Send = "Gửi";
        public const string Save = "Lưu";
        public const string SaveAndExit = "Lưu và thoát";
        public const string Confirm = "Xác nhận";
        public const string Transfer = "Trao đổi";
        public const string Attachment = "Đính kèm";
        public const string Cancel = "Hủy";
        public const string Review = "Kiểm soát";
        public const string View = "Theo dõi";
        public const string CheckAll = "Chọn tất cả";
        public const string Change = "Thay đổi";
        public const string SendAccountInfo = "Gửi tài khoản";
        public const string Permission = "Phân quyền";
        public const string Close = "Đóng";
        public const string Pause = "Tạm dừng";
        public const string Default = "Mặc định";
        public const string Exit = "Thoát";
        public const string LevelImportant = "Thiết lập mức độ quan trọng";
        public const string BrowseColor = "Chọn màu";
        #endregion
        #region Panel title
        public const string ListOfTask = "Danh sách công việc";
        public const string TaskDetailInfomation = "Thông tin chi tiết công việc";
        public const string TaskStageDetailInfomation = "Thông tin giai đoạn chi tiết";
        public const string TaskResourceInfomation = "Danh sách nhân sự liên quan đến công việc";
        public const string SelectResource = "Lựa chọn nhân sự";
        public const string TaskPerformDetail = "Thông tin chi tiết thực hiện công việc";
        public const string TaskAssignDetail = "Thông tin chi tiết giao việc thực hiện";
        public const string ListOfTaskPerform = "Danh sách công việc thực hiện";
        public const string ResourcePerform = "Danh sách nhân sự thực hiện";
        public const string ResourceRelate = "Nhân sự liên quan";

        #endregion
        #region User role

        #endregion
        #region TaskGroup
        public const string TaskGroupInformation = "Thông tin nhóm công việc";
        public const string TaskGroupName = "Tên nhóm công việc";
        public const string TaskGroup = "Nhóm công việc";
        #endregion
        #region TaskCommunication
        public const string TaskCommunicationTypeInfomation = "Thông tin loại thông tin trao đổi";
        #endregion
        #region Calendar
        public const string CalendarInfomation = "Thông tin lịch làm việc";
        public const string CalendarName = "Tên lịch làm việc";
        public const string BaseCalendar = "Lịch cơ sở";
        public const string CalendarActive = "Áp dụng thực hiện";
        public const string CalendarShiftInfomation = "Thông tin ca làm việc";
        #endregion
        #region TaskPerform
        public const string TaskPerformTiming = "Thời gian thực hiện";
        public const string TypeReasonOfCancel = "Nhập lý do hủy";
        #endregion
        #region Cấu hình điểm thái độ
        public const string _31 = "Thái độ 1";
        public const string _32 = "Thái độ 2";
        public const string _33 = "Thái độ 3";
        public const string _34 = "Thái độ 4";
        #endregion
        #region Document
        public const string DocumentCode = "Mã tài liệu";
        public const string DocumentName = "Tên tài liệu";
        public const string DocumentVersion = "Phiên bản";
        public const string DocumentShortContent = "Trích yếu nội dung";
        public const string DocumentSecurity = "Mức độ mật";
        public const string DocumentNote = "Ghi chú";
        public const string DocumentPublishDate = "Ngày ban hành";
        public const string DocumentPublishTime = "Lần ban hành";
        public const string DocumentApplyDate = "Ngày có hiệu lực";
        public const string DocumentExpireDate = "Ngày hết hiệu lực";
        public const string DocumentAttachment = "Đính kèm";
        public const string DocumentExist = "Tài liệu hiện hành";
        public const string DocumentExistInfomation = "Thông tin tài liệu hiện hành";
        public const string DocumentCategory = "Danh mục tài liệu";
        public const string DocumentExternalPublishName = "Đơn vị ban hành";
        public const string DocumentStatus = "Trạng thái tài liệu";
        #endregion
        public const string MethodName = "Tên phương pháp";
        public const string UserName = "Tài khoản";
        public const string PhoneNumber = "Số điện thoại";
        public const string Email = "Email";
        public const string Active = "Kích hoạt";
        public const string LockUser = "Khóa tài khoản";
        public const string FullName = "Họ và tên";

        public const string TaskManageTable = "Bảng kiểm soát công việc";
        public const string AssignInfomation = "Thông tin giao việc";

        public const string InformationBase = "Thông tin cơ bản";
        public const string InformationExtend = "Thông tin bổ sung";

        public const string CheckBoxDate = "Theo ngày";
        public const string CheckBoxTime = "Theo giờ";
        public const string AllowChangeTime = "Cho phép thực hiện quá thời gian";
        public const string CurrentYear = "Năm nay";

        public const string StartDate = "Từ ngày";
        public const string StartTime = "Bắt đầu";
        public const string EndDate = "Đến ngày";
        public const string EndTime = "Kết thúc";
        public const string StartDateBase = "Theo ngày";
        public const string StartTimeBase = "Theo giờ";
        public const string EndDateBase = "Theo ngày";
        public const string EndTimeBase = "Theo giờ";

        public const string ParentTask = "Thuộc công việc";
        public const string TaskType = "Loại công việc";
        public const string TaskLevel = "Mức độ công việc";
        public const string Note = "Ghi chú";

        public const string TaskStage = "Giai đoạn thực hiện";
        public const string TaskAssociate = "Công việc liên quan";

        public const string OutOfDate = "Quá hạn";

        public const string Unit = "Khối lượng công việc";
        public const string SelectStatus = "Trạng thái";

        public const string SystemManagement = "Quản trị hệ thống";
        public const string UserInformation = "Thông tin người dùng";
        public const string TaskInformation = "Thông tin công việc";
        public const string ImageProfile = "Ảnh";
        public const string Password = "Mật khẩu";
        public const string ReasonOfLockUser = "Lý do khóa tài khoản";
        public const string Description = "Mô tả";

        #region Asset
        public const string AssetGroup = "Nhóm tài sản";
        public const string AssetTitle = "Danh mục tài sản";
        public const string AssetName = "Tên tài sản (*)";
        public const string AssetTypeName = "Tên loại tài sản (*)";
        public const string AssetGroupTitle = "Danh sách nhóm tài sản";
        public const string AssetGroupName = "Tên nhóm tài sản (*)";
        public const string AssetPropertyName = "Tên thuộc tính (*)";
        public const string AssetTypeSetting = "Quy tắc phân loại";
        public const string PropertyType = "Kiểu dữ liệu";
        public const string PropertyList = "Danh sách thuộc tính";
        public const string AssetTempSettingTitle = "Quy tắc gán nhãn tài sản";
        public const string AssetTempSettingName = "Trường hợp sử dụng";
        public const string AssetTempSettingUse = "Sử dụng";
        public const string AssetTempSettingRule = "Quy tắc";
        public const string AssetTempSettingMethod = "Phương thức";
        public const string AssetTempSettingTemplate = "Tem mẫu";
        public const string AssetReviewSettingTitle = "Quy tắc đánh giá tài sản (*)";
        public const string AssetReviewSettingName = "Tiêu đề (*)";
        public const string AssetReviewSettingLogicValue = "Giá trị";
        public const string AssetReviewSettingColor = "Màu sắc";
        public const string AssetReviewSettingFrom = "Từ";
        public const string AssetReviewSettingTo = "Đến";
        public const string AssetResourceRelative = "Lý lịch tài sản";
        public const string AssetInfo = "Thông tin tài sản";
        public const string AssetRoleRelative = "Trách nhiệm liên quan";
        public const string AsseHistorytUse = "Theo dõi sử dụng";
        public const string AssetHistoryAddLable = "Lịch sử gán nhãn";
        public const string AssetHistoryReview = "Lịch sử đánh giá";
        public const string AssetComboUse = "Đang sử dụng";
        public const string AssetComboFail = "Hỏng";
        public const string AssetComboCancel = "Hủy";
        public const string AssetComboFix = "Bảo dưỡng";
        #endregion
        #region Mesage
        public const string Notify = "Thông báo";
        public const string AccessDeny = "Bạn không có quyền truy cập vào chức năng này!";
        public const string AccountLocked = "Tài khoản của bạn đã bị khóa!";
        public const string AccountExist = "Tài khoản của bạn đã tồn tại!";
        public const string LoginFailure = "Đăng nhập hệ thống không thành công!";
        public const string SystemSuccess = "Hệ thống xử lý thành công!";
        public const string Responsibiliti = "Bạn chưa thiết lập trách nhiệm liên quan!";
        public const string SystemFailure = "Hệ thống xử lý phát sinh lỗi!";
        public const string RoleTypeExits = "Bạn không có quyền này!";
        public const string SettingProcess = "Thiết lập quy trình để sử dụng tính năng này!";
        public const string SettingProcessSuggestWriteNew = "Thiết lập quy trình đề nghị viết mới để sử dụng tính năng này!";
        public const string ConfirmDelete = "Bạn có chắc chắn muốn xóa dữ liệu này không ?";
        public const string ConfirmDestroy = "Bạn có chắc chắn muốn hủy dữ liệu này không ?";
        public const string ConfirmRevert = "Bạn có chắc chắn muốn thu hồi dữ liệu này không ?";
        public const string ConfirmDistribute = "Bạn có chắc chắn muốn phân phối tài liệu này không ?";
        public const string ConfirmUpdateFile = "Bạn có chắc chắn muốn thay thế tệp đính kèm này không ?";
        public const string NotAllowRevert = "Không cho phép thu hồi tài liệu này!";
        public const string SendComplete = "Bản ghi đã được gửi!";
        public const string ConfirmYes = "Đồng ý";
        public const string ConfirmNo = "Không";
        public const string ThisFieldMustBeRequired = "Trường này không được để trống";
        public const string ThisFieldMustBeRequiredAndSetting = "Giá trị chưa được chọn hoặc chưa thiết lập";
        public const string TypeProtiesBeingConflict = "Loại tài sản đã bao gồm tất cả các thuộc tính trong các loại tài sản sau:";
        public const string ConfirmOverrideTypeProtiesBeingConflict = "Bạn có muốn tạo mới một loại tài sản thay thế cho toàn bộ các loại tài sản trên?<br/> Có - Tạo mới loại tài sản/ Không - Tiếp tục sửa.";
        public const string ConfirmContinueTypeProtiesBeingConflict = "Bạn có muốn tiếp tục thao tác?";
        public const string ConfirmNotUse = "Bạn có chắc chắn không sử dụng?";
        public const string ConfirmEliminateProfile = "Bạn chắc chắn muốn loại bỏ thành phần hồ sơ này không ?";
        public const string ConfirmUseProfile = "Bạn có chắc chắn muốn sử dụng thành phần hồ sơ này không?";
        #endregion
        public const string PermissionUser = "Phân quyền người dùng";
        public const string AllModules = "Tất cả Modules";
        public const string FileNotFoundError = "Hệ thống đã không tìm thấy file này!";
        public const string FileNotViewError = "Hệ thống không thể xem những file định dạng này";
        public const string TemplateNotFoundError = "Hệ thống không tồn tại file biểu mẫu xuất";
        public const string ValidatePassword = "Mật khẩu không được để trống, phải chứa kí tự chữ thường, chữ hoa, kí tự số ,kí tự đặc biệt, ít nhất 8 kí tự";
        public const string ValidatePhone = "Điện thoại không được để trống và chỉ chứa kí tự số";
        #region Document
        public const string DefaultVersion = "1";
        public const string ApproveDocument = "Phê duyệt tài liệu";
        #endregion

        #region Validate
        public const string TextMaxLength200 = "Không được nhập dữ liệu vượt quá 200 ký tự!";
        public const string BlankText = "Không được để trống trường này";
        #endregion

        #region Regex
        /// <summary>
        /// Bắt đầu khác khoảng trắng
        /// </summary>
        public const string RegexStartWithoutWhiteSpace = @"^\S";
        /// <summary>
        /// Chỉ có ký tự trắng
        /// </summary>
        public const string RegexWhiteSpaceOnly = @"^\S";

        public const string RegexHtmlWhiteSpaceStartFilter = @"^[^\s<>][^<>]*$";
        /// <summary>
        /// Nhập loại bỏ các ký tự đặc biệt
        /// </summary>
        public const string RegexSpecialSymbolFilter = @"^[^\s~`!@#$%^&*()_+={}|\\;:'""<>,./?][^~`!@#$%^&*()_+={}|\\;:'""<>,./?]*$";
        public const string RegexHtmlFilter = @"<\/*[^<>\n]+\/*>";
        public const string RegexSpecialSymbolMessage = "Không nhập ký tự đặc biệt.";
        public const string RegexHtmlMessage = "Không nhập thẻ html, javascript";
        public const string RegexHtmlWhiteSpaceStartMessage = "Không nhập ký tự đặc biệt, dấu cách ở đầu tiên";
        #endregion

        #region Chat
        /// <summary>
        /// 0: Avatar người gửi
        /// 1: Tên người gửi
        /// 2: Thời gian gửi
        /// 3: Nội dung
        /// </summary>
        public const string FromOtherUser = "<li class='left clearfix'><span class='pull-left'><img width='30' height='30' src='{0}' class='img-circle' title='{1}' /></span><div class='cmsg-body clearfix'><p class='pshow' title='{2}'>{3}</p></div></li>";
        public const string FromOtherUserHide = "<li class='left clearfix'><span class='pull-left'></span><div class='cmsg-body clearfix'><p class='phide' title='{0}'>{1}</p></div></li>";
        /// <summary>
        /// 0: Avatar người gửi
        /// 1: Thời gian gửi
        /// 3: Nội dung
        /// </summary>
        public const string FromCurrentUser = "<li class='right clearfix'><span class='clsavatar pull-right'><img width='30' height='30' src='{0}' class='img-circle' title='Tôi' /></span><div class='pull-right cmsg-body clearfix'><p class='pshow pull-right' title='{1}'>{2}</p></div></li>";
        public const string FromCurrentUserHide = "<li class='right clearfix'><span class='clsavatar pull-right'></span><div class='pull-right cmsg-body clearfix'><p class='phide pull-right' title='{0}'>{1}</p></div></li>";


        public const string UserOnlineItem = "";

        #endregion

        #region QualityIssue
        public const string QualityIssue = "Vấn đề";
        #endregion

        #region TimeOff
        public const string EmployeeName = "Nhân Viên";
        public const string DepartmentName = "Phòng Ban";
        public const string DepartmentTitle = "Chức Vụ";
        public const string StartTimeOff = "Bắt đầu";
        public const string EndTimeOff = "Đến";
        public const string Reason = "Lý do nghỉ";
        public const string EmployeeReplace = "Nhân viên thay thế công việc";
        public const string EmployeeApproveName = "Nhân viên xét duyệt";
        public const string Status = "Trạng thái";
        public const string Wait = "Chờ phê duyệt";
        public const string Approved = "Phê duyệt";
        public const string Rejected = "Từ chối";
        public const string All = "Tất cả";
        #endregion

    }
}