﻿using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace iDAS.Presentation.Common
{
    public class DBConnect
    {
        static SqlConnection _connection;
        static string connectionString = ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString;
        public static SqlConnection GetConnection()
        {
            _connection = new SqlConnection(connectionString);
            if (_connection.State != ConnectionState.Open)
                _connection.Open();
            return _connection;
        }

        public DataTable myTable(string strQuerry)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(strQuerry, DBConnect.GetConnection());

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                da.SelectCommand = cmd;
                da.SelectCommand.CommandTimeout = 120;
                da.Fill(dt);
            }
            catch (Exception e)
            {
                throw e;// Bắt lỗi
            }
            finally
            {
                cmd.Connection.Close();
            }

            return dt;
        }

        public DataTable myTable(string store, params object[] paramater)
        {

            SqlCommand myCon = new SqlCommand(store, DBConnect.GetConnection());
            myCon = addParamater(store, paramater);
            myCon.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter myAdapter = new SqlDataAdapter(myCon);
            myAdapter.SelectCommand = myCon;
            myAdapter.SelectCommand.CommandTimeout = 120;
            DataTable myTable = new DataTable();
            try
            {
                myAdapter.Fill(myTable);
            }
            catch (Exception e)
            {
                throw e; // Bắt lỗi
            }
            finally
            {
                myCon.Connection.Close();
            }

            return myTable;
        }

        public static SqlCommand addParamater(string strQuery, params Object[] paramater)
        {
            SqlCommand command = new SqlCommand(strQuery, DBConnect.GetConnection());
            for (int i = 0; i < paramater.Length; i += 2)
            {
                command.Parameters.Add(paramater[i].ToString(), paramater[i + 1]);
            }
            return command;
        }

        public static bool RunQuery(string strQuery, params Object[] paramater)
        {
            using (SqlConnection connection = DBConnect.GetConnection())
            {
                connection.Open();

                SqlCommand command = addParamater(strQuery, paramater);
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 200;
                SqlTransaction transaction;
                // Start a local transaction.
                transaction = connection.BeginTransaction("SampleTransaction");

                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.ExecuteNonQuery();
                    transaction.Commit();
                    return true;
                }
                catch (SqlException ex)
                {
                    transaction.Rollback();
                    return false;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public static int RunQueryReturnID(string strQuery, params Object[] paramater)
        {

            using (SqlConnection connection = DBConnect.GetConnection())
            {
                connection.Open();

                SqlCommand command = addParamater(strQuery, paramater);
                command.CommandType = CommandType.StoredProcedure;
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("SampleTransaction");

                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    int newID = Convert.ToInt32(command.ExecuteScalar().ToString());
                    transaction.Commit();
                    return newID;
                }
                catch (SqlException ex)
                {
                    transaction.Rollback();
                    return -1;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        // xử lý dữ liệu trả về datasset
        public DataSet myDataset(string store, params object[] paramater)
        {
            SqlCommand command = new SqlCommand(store, DBConnect.GetConnection());
            command = addParamater(store, paramater);
            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = 1000;

            SqlDataAdapter myAdapter = new SqlDataAdapter(command);
            myAdapter.SelectCommand = command;
            myAdapter.SelectCommand.CommandTimeout = 72000;
            DataSet myDataset = new DataSet();
            try
            {
                myAdapter.Fill(myDataset);
            }
            catch (Exception e)
            {
                throw e;// Bắt lỗi
            }
            finally
            {
                command.Connection.Close();
            }

            return myDataset;
        }

        public static bool CheckEsxitItem(string store, params object[] paramater)
        {

            SqlCommand myCon = new SqlCommand(store, DBConnect.GetConnection());
            myCon = addParamater(store, paramater);
            myCon.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter myAdapter = new SqlDataAdapter(myCon);
            myAdapter.SelectCommand = myCon;
            myAdapter.SelectCommand.CommandTimeout = 120;
            DataTable myTable = new DataTable();
            try
            {
                myAdapter.Fill(myTable);
            }
            catch (Exception e)
            {
                throw e; // Bắt lỗi
            }
            finally
            {
                myCon.Connection.Close();
            }
            return (int)myTable.Rows[0][0] > 0;
        }

        public static DataSet Fill(DataSet table, String sql, params Object[] parameters)
        {
            using (SqlConnection connection = DBConnect.GetConnection())
            {
                connection.Open();
                SqlCommand command = DBConnect.CreateCommand(sql, parameters);
                command.Connection = connection;
                new SqlDataAdapter(command).Fill(table);
                connection.Close();
                return table;
            }
        }

        public static DataSet GetData(String sql, params Object[] parameters)
        {
            return DBConnect.Fill(new DataSet(), sql, parameters);
        }

        public static int ExecuteNonQuery(String sql, params Object[] parameters)
        {

            using (SqlConnection connection = DBConnect.GetConnection())
            {
                connection.Open();
                SqlCommand command = DBConnect.CreateCommand(sql, parameters);
                command.Connection = connection;
                int rows = command.ExecuteNonQuery();
                connection.Close();
                return rows;
            }

        }

        public static object ExecuteScalar(String sql, params Object[] parameters)
        {

            using (SqlConnection connection = DBConnect.GetConnection())
            {
                connection.Open();
                SqlCommand command = DBConnect.CreateCommand(sql, parameters);
                command.Connection = connection;
                object value = command.ExecuteScalar();
                connection.Close();
                return value;
            }
        }


        private static SqlCommand CreateCommand(String sql, params Object[] parameters)
        {

            SqlCommand command = new SqlCommand(sql, DBConnect.GetConnection());
            for (int i = 0; i < parameters.Length; i += 2)
            {
                command.Parameters.AddWithValue(parameters[i].ToString(), parameters[i + 1]);
            }

            return command;
        }

        public static EmployeeBO EmployeeGetById(string id)
        {
            EmployeeBO employeeBO = new EmployeeBO();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "sp_Employees_GetById";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Id", id);

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            employeeBO.Id = (Guid)dr["Id"];
                            if (dr["Avatar"].GetType() != typeof(DBNull))
                                employeeBO.Avatar = (byte[])dr["Avatar"];

                            employeeBO.Code = dr["Code"].ToString();
                            employeeBO.Name = dr["Name"].ToString();
                            employeeBO.PhoneNumber = dr["PhoneNumber"].ToString();
                            employeeBO.Email = dr["Email"].ToString();
                            //employeeBO.Note = dr["Note"].ToString();
                            //employeeBO.Sex = (int)dr["Sex"];
                        }
                    }
                }
            }
            return employeeBO;
        }
        public static EmployeeBOExt EmployeeExtGetById(string id)
        {
            EmployeeBOExt employeeBO = new EmployeeBOExt();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "sp_Employees_GetById";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Id", id);

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            employeeBO.Id = (Guid)dr["Id"];
                            if (dr["Avatar"].GetType() != typeof(DBNull))
                                employeeBO.Avatar = (byte[])dr["Avatar"];

                            employeeBO.Code = dr["Code"].ToString();
                            employeeBO.Name = dr["Name"].ToString();
                            employeeBO.PhoneNumber = dr["PhoneNumber"].ToString();
                            employeeBO.Email = dr["Email"].ToString();
                            //employeeBO.Note = dr["Note"].ToString();
                            //employeeBO.Sex = (int)dr["Sex"];
                        }
                    }
                }
            }
            return employeeBO;
        }
        public static List<Guid> GetLstIDIsParent(string ID)
        {
            List<Guid> lstIDEmployee = new List<Guid>();
            lstIDEmployee = GetIDEmployeeListParent(new Guid(ID), lstIDEmployee);
            return lstIDEmployee;
        }
        private static List<Guid> GetIDEmployeeListParent(Guid ID, List<Guid> LstOut)
        {
            if (LstOut == null || LstOut.Count == 0)
                LstOut = new List<Guid>();
            List<Guid> employees = new List<Guid>();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "sp_Employees_GetIsParent";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Id", ID.ToString());
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                            employees.Add((Guid)dr["Id"]);
                    }
                }
            }
            if (employees.Count > 0)
            {
                LstOut.AddRange(employees);
                foreach (Guid item in employees)
                    GetIDEmployeeListParent(item, LstOut);
            }
            return LstOut;
        }
        public static bool CheckPermissionKPI(string Type, string Id)
        {
            DataTable dataTable = new DataTable();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "Check_Permission_KPI";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@Id", Id);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dataTable);
                    return dataTable.Rows.Count > 0;
                }
            }
        }
        public static string GetLstPermission(string Id)
        {
            DataTable dataTable = new DataTable();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionEntities"].ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "Get_lstPermission_ByEmployeeId";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dataTable);
                    return string.Join(",", dataTable.AsEnumerable().Select(x => x["Type"].ToString()));
                }
            }
        }
    }
}