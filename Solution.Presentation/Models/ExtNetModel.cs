namespace iDAS.Presentation.Models
{
    public class ExtNetModel
    {
        public string Title { get; set; }
        public string TextAreaEmptyText { get; set; }
    }
}