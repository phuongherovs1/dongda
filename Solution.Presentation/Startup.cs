﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(iDAS.Presentation.Startup))]
namespace iDAS.Presentation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
