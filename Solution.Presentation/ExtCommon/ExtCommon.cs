﻿using Ext.Net;
using Ext.Net.MVC;
using iDAS.Presentation.Common;
using iDAS.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ResourceService = iDAS.Service.Common.Resource;
namespace iDAS.Presentation
{
    public static class ExtCommon
    {
        public class SlideConfig
        {
            private string _containerId = "ctDesktop";
            private double _ratioWidhParent = 0.5;
            private double _ratioHeightParent = 1;
            private int _timeSlide = 500;
            private string _slideFrom = "r";
            private bool _shapebyParent = true;
            public string containerId
            {
                get
                {

                    return _containerId;
                }
                set
                {
                    _containerId = value;
                }
            }
            public double ratioWidhParent
            {
                get
                {

                    return _ratioWidhParent;
                }
                set
                {
                    _ratioWidhParent = value;
                }
            }
            public double ratioHeightParent
            {
                get
                {

                    return _ratioHeightParent;
                }
                set
                {
                    _ratioHeightParent = value;
                }
            }
            public int timeSlide
            {
                get
                {

                    return _timeSlide;
                }
                set
                {
                    _timeSlide = value;
                }
            }
            public string slideFrom
            {
                get
                {

                    return _slideFrom;
                }
                set
                {
                    _slideFrom = value;
                }
            }
            public bool shapebyParent
            {
                get
                {

                    return _shapebyParent;
                }
                set
                {
                    _shapebyParent = value;
                }
            }
        }
        public static void ShowNotify(this Controller controller, string message)
        {
            message = message ?? Resource.SystemSuccess;
            var config = new Ext.Net.NotificationConfig()
            {
                IconCls = "x-fa fa-exclamation-circle",
                Title = Common.Resource.Notify,
                Html = message,
                HideDelay = 3000,
                ShowMode = Ext.Net.ShowMode.Stack
            };
            Ext.Net.X.Msg.Notify(config).Show();
        }

        public static void ShowAlert(this Controller controller, string message)
        {
            message = message ?? Resource.SystemSuccess;
            X.Msg.Alert(Common.Resource.Notify, message).Show();
        }
        public static DirectResult DirectFormat(this Controller controller, string message = null)
        {
            controller.ShowNotify(message);
            return controller.Direct();
        }
        public static TProperty GetValue<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression)
        {
            var arrKey = expression.Body.ToString().Split('.');
            Object obj = X.HtmlHelper.ViewData.Model;
            if (obj != null && arrKey.Count() > 1)
            {
                for (var i = 1; i < arrKey.Count(); i++)
                {
                    obj = obj.GetType().GetProperty(arrKey[i]).GetValue(obj);
                }
            }
            return (TProperty)obj;
        }
        public static TProperty GetValue<TModel, TProperty>(this BuilderFactory<TModel> X, string propertyKey)
        {
            var arrKey = propertyKey.ToString().Split('.');
            Object obj = X.HtmlHelper.ViewData.Model;
            if (obj != null && arrKey.Count() > 0)
            {
                for (var i = 0; i < arrKey.Count(); i++)
                {
                    obj = obj.GetType().GetProperty(arrKey[i]).GetValue(obj);
                }
            }
            return (TProperty)obj;
        }
        public static string GetKey<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression)
        {
            var arrKey = expression.Body.ToString().Split('.');
            string result = string.Empty;
            if (arrKey.Count() > 1)
            {
                for (var i = 1; i < arrKey.Count(); i++)
                {
                    result += arrKey[i];
                    result += ".";
                }
            }
            return result;
        }
        public static string GetComponentID(this BuilderFactory X)
        {
            var componentId = "component" + Guid.NewGuid().ToString().Replace("-", "");
            return componentId;
        }
        public static Window.Builder WindowSlide(this BuilderFactory X)
        {
            var component = X.Window().SlideConfigFormat(new SlideConfig()).Draggable(false).Resizable(false)
                            .IconCls("x-fa fa-pencil-square-o")
                            .Height(480).Width(800)
                            .BodyPadding(0).Modal(true).Constrain(true)
                            .Maximizable(true)
                            .Closable(false)
                            .Shadow(true);
            return component;
        }
        public static Window.Builder WindowLayout(this Window.Builder window, LayoutType layout)
        {
            switch (layout)
            {
                case LayoutType.Fit: window.Layout(LayoutType.Fit); break;
                case LayoutType.VBox:
                    window.Layout(LayoutType.VBox)
                          .LayoutConfig(new VBoxLayoutConfig { Align = VBoxAlign.Stretch }); break;
                case LayoutType.HBox:
                    window.Layout(LayoutType.HBox)
                          .LayoutConfig(new HBoxLayoutConfig { Align = HBoxAlign.Stretch }); break;

            }
            return window;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="window"></param>
        /// <param name="sildeConfig"></param>
        /// <returns></returns>
        public static Window.Builder SlideConfigFormat(this Window.Builder window, SlideConfig slideConfig)
        {
            return window.Listeners(
            ls =>
            {
                ls.BeforeShow.Handler = @"var parentSilde= Ext.getCmp('" + slideConfig.containerId + @"');" +
                    (slideConfig.shapebyParent ? ("this.setHeight(parentSilde.getHeight()*" + slideConfig.ratioHeightParent + @");
                    this.setWidth(parentSilde.getWidth()*" + slideConfig.ratioWidhParent + @");") : "")
                    + " this.alignTo(parentSilde,'br-br'); this.el.slideIn('" + slideConfig.slideFrom + @"',{duration :" + slideConfig.timeSlide + "});";
                ls.MaskClick.Handler = "this.destroy();";
            });
        }
        public static Window.Builder WindowFormat(this BuilderFactory X)
        {
            var loadMask = X.Container().ItemID("loadMask").Border(false).Height(2).Hidden(true)
                            .Html("<div class='load-bar'><div class='bar'></div><div class='bar'></div></div>");
            var loadMaskEmpty = X.Container().ItemID("loadMaskEmpty").Border(false).Height(2).Hidden(false);
            var component = X.Window()
                            .IconCls("x-fa fa-pencil-square-o")
                            .Height(480).Width(800)
                            .BodyPadding(0).Modal(true).Constrain(true)
                            .Maximizable(true)
                            .Closable(false)
                            .Shadow(true)
                            .Layout(LayoutType.VBox)
                            .LayoutConfig(new VBoxLayoutConfig { Align = VBoxAlign.Stretch })
                            .Items(loadMask, loadMaskEmpty);
            return component;
        }
        public static Window.Builder WindowFitFormat(this BuilderFactory X)
        {
            var loadMask = X.Container().ItemID("loadMask").Border(false).Height(2).Hidden(true)
                            .Html("<div class='load-bar'><div class='bar'></div><div class='bar'></div></div>");
            var loadMaskEmpty = X.Container().ItemID("loadMaskEmpty").Border(false).Height(2).Hidden(false);
            var component = X.Window()
                            .IconCls("x-fa fa-pencil-square-o")
                            .Height(450)
                            .Width(730)
                            .BodyPadding(0)
                            .Modal(true)
                            .Constrain(true)
                            .Maximizable(true)
                            .Closable(false)
                            .Layout(LayoutType.Fit)
                            .Items(loadMask, loadMaskEmpty);
            return component;
        }
        #region WidgetFormat
        public static Panel.Builder AllowFullScreen(this Panel.Builder Panel, bool isFull = false)
        {
            if (isFull)
            {
                Panel.Tools(c => c.Add(
                    Html.X().Tool().Type(ToolType.Maximize).Listeners(ls => ls.Click.Handler = "var panel = this.up('panel'); fullWindowFromPanel(panel, panel.title, panel.iconCls)")
                    )
                );
            }
            return Panel;
        }
        public static Panel.Builder IDLoadding(this Panel.Builder Panel, string idLoadding = default(string))
        {
            if (idLoadding.Length > 0)
            {
                Panel.Tools(c => c.Add(
                    Html.X().Tool().Cls("loadding").Type(ToolType.Search).ID(idLoadding)
                    )
                );
            }
            return Panel;
        }
        public static Panel.Builder HandlerReload(this Panel.Builder Panel, string handlerReload = default(string))
        {
            if (handlerReload.Length > 0)
            {
                Panel.Tools(c => c.Add(
                    Html.X().Tool().Type(ToolType.Refresh).Listeners(ls => ls.Click.Handler = handlerReload)
                    )
                );
            }
            return Panel;
        }
        public static Panel.Builder PanelWidgetFormat(this BuilderFactory X)
        {
            var component = X.Panel()
                .Layout(LayoutType.Fit);
            return component;
        }
        #endregion
        public static Panel.Builder PanelFormat(this BuilderFactory X)
        {
            var component = X.Panel()
                            .Layout(LayoutType.VBox)
                            .BodyPadding(0).Border(false).Flex(1).Header(false)
                            .Defaults(X.Parameter().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                            .LayoutConfig(new VBoxLayoutConfig { Align = VBoxAlign.Stretch });
            return component;
        }
        public static Panel.Builder PanelHbox(this BuilderFactory X)
        {
            var component = X.Panel()
                            .Layout(LayoutType.HBox)
                            .BodyPadding(0).Border(false).Flex(1).Header(false)
                            .Defaults(X.Parameter().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                            .LayoutConfig(new HBoxLayoutConfig { Align = HBoxAlign.Stretch });
            return component;
        }
        public static Panel.Builder PanelFitFormat(this BuilderFactory X)
        {
            var component = X.Panel()
                            .BodyPadding(0)
                            .Border(false)
                            .Flex(1)
                            .Header(false)
                            .Layout(LayoutType.Fit);
            return component;
        }
        public static FormPanel.Builder FormPanelVBoxFormat(this BuilderFactory X)
        {
            var btnId = X.GetComponentID();
            var component = X.FormPanel()
                            .BodyPadding(0).Border(false).Flex(1).Header(false)
                            .Defaults(X.Parameter().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                            .Layout(LayoutType.VBox)
                            .LayoutConfig(new VBoxLayoutConfig { Align = VBoxAlign.Stretch });
            return component;
        }
        public static FormPanel.Builder FormPanelHBoxFormat(this BuilderFactory X)
        {
            var component = X.FormPanel()
                            .BodyPadding(0).Border(false).Flex(1).Header(false)
                            .Defaults(X.Parameter().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                            .Layout(LayoutType.HBox)
                            .LayoutConfig(new HBoxLayoutConfig { Align = HBoxAlign.Stretch });
            return component;
        }
        public static FormPanel.Builder FormPanelFormat(this BuilderFactory X)
        {
            var btnId = X.GetComponentID();
            var component = X.FormPanel()
                            .BodyPadding(6).Border(false).Flex(1).Header(false)
                            .Defaults(X.Parameter().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                            .Layout(LayoutType.VBox)
                            .LayoutConfig(new VBoxLayoutConfig { Align = VBoxAlign.Stretch })
                            .DefaultButton(btnId)
                            .Buttons(X.Button().ID(btnId).Hidden(true));
            return component;
        }
        #region Button
        public static Button.Builder ButtonAdd(this BuilderFactory X)
        {
            var component = X.Button()
                            .ItemID("btnAdd")
                            .IconCls("x-fa fa-plus-circle").Text("Thêm").DirectClickAction("Form");
            return component;
        }
        public static Button.Builder ButtonDetail(this BuilderFactory X)
        {
            var component = X.Button()
            .ItemID("btnDetail")
            .IconCls("x-fa fa-search").Text("Chi tiết").Disabled(true).DirectClickAction("Form")
            .Parameter(new Parameter("id", "this.up('grid').selModel.getSelection()[0].data.Id", ParameterMode.Raw));
            return component;
        }
        public static Button.Builder ButtonExport(this BuilderFactory X, string name = "Tệp xuất")
        {
            var component = X.Button()
                            .Text("Xuất dữ liệu")
                            .IconCls("x-fa fa-download")
                            .ID(X.GetComponentID())
                            .Menu(X.Menu()
                                  .Items(
                                      X.MenuItem()
                                       .Text("Dưới dạng Excel")
                                        .ID(X.GetComponentID())
                                        .IconCls("x-fa fa-file-excel-o")
                                        .DirectEvents(de =>
                                        {
                                            de.Click.IsUpload = true;
                                            de.Click.ExtraParams.Add(new { json = new JRawValue("Ext.encode(this.up('grid').getRowsValues({ selectedOnly: false }))") });
                                            de.Click.ExtraParams.Add(new { nameFile = name });
                                            de.Click.ExtraParams.Add(new { format = "xls" });
                                            de.Click.Url = X.XController().UrlHelper.Action("Export", "File", new { Area = "Generic" });
                                        }),
                                      X.MenuItem()
                                        .Text("Dưới dạng CSV")
                                        .ID(X.GetComponentID())
                                        .IconCls("x-fa fa-file-archive-o")
                                       .DirectEvents(de =>
                                       {
                                           de.Click.IsUpload = true;
                                           de.Click.ExtraParams.Add(new { json = new JRawValue("Ext.encode(this.up('grid').getRowsValues({ selectedOnly: false }))") });
                                           de.Click.ExtraParams.Add(new { nameFile = name });
                                           de.Click.ExtraParams.Add(new { format = "csv" });
                                           de.Click.Url = X.XController().UrlHelper.Action("Export", "File", new { Area = "Generic" });
                                       }),
                                      X.MenuItem()
                                       .Text("Dưới dạng XML")
                                        .ID(X.GetComponentID())
                                        .IconCls("x-fa fa-file-code-o")
                                       .DirectEvents(de =>
                                       {
                                           de.Click.IsUpload = true;
                                           de.Click.ExtraParams.Add(new { json = new JRawValue("Ext.encode(this.up('grid').getRowsValues({ selectedOnly: false }))") });
                                           de.Click.ExtraParams.Add(new { nameFile = name });
                                           de.Click.ExtraParams.Add(new { format = "xml" });
                                           de.Click.Url = X.XController().UrlHelper.Action("Export", "File", new { Area = "Generic" });
                                       })
                               )
                           );
            return component;
        }
        public static Button.Builder ButtonRefreshPanel(this BuilderFactory X, string toolTip = "Tải lại")
        {
            var component = X.Button().IconCls("x-fa fa-refresh").ToolTip(toolTip)
                            .Handler("var panel = this.up('panel');if(panel && panel.xtype == 'grid'){ store = panel.store;if(store){store.reload();}}");
            return component;
        }
        public static Button.Builder ButtonSubmit(this BuilderFactory X)
        {
            var component = X.Button()
            .DirectEvents(de =>
            {
                de.Click.Method = HttpMethod.POST;
                de.Click.Before = "if (!this.up('window').down('form').getForm().isValid()) { return false; }; this.setDisabled(true); this.up('window').down('#loadMask').show(); this.up('window').down('#loadMaskEmpty').hide();";
                de.Click.Success = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); this.up('window').close();";
                de.Click.Failure = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); eval(result.script); this.setDisabled(false);";
            });
            return component;
        }
        public static Button.Builder ButtonCancel(this BuilderFactory X)
        {
            var component = X.Button()
                            .ItemID("btnCancel")
                            .Text("Hủy").IconCls("x-fa fa-ban")
                            .DirectClickAction("Cancel")
                            .Method(HttpMethod.GET)
                            .Parameter(new Parameter("id", "App.Id.value", ParameterMode.Raw));
            return component;
        }
        public static Button.Builder ButtonPause(this BuilderFactory X)
        {
            var component = X.Button()
                            .ItemID("btnPause")
                            .Text("Tạm dừng").IconCls("x-fa fa-pause")
                            .DirectClickAction("Pause")
                            .Method(HttpMethod.GET)
                            .Parameter(new Parameter("id", "App.Id.value", ParameterMode.Raw))
                            .DirectEvents(de => de.Click.Type = DirectEventType.Load);
            return component;
        }
        public static Button.Builder ButtonCancelGrid(this BuilderFactory X)
        {
            var component = X.Button()
                            .ItemID("btnCancel")
                            .Text("Hủy").IconCls("x-fa fa-ban")
                            .DirectClickAction("Cancel")
                            .Disabled(true)
                            .Method(HttpMethod.GET)
                            .Parameter(new Parameter("id", "this.up('grid').selModel.getSelection()[0].data.Id", ParameterMode.Raw));
            return component;
        }
        public static Button.Builder ButtonPauseGrid(this BuilderFactory X)
        {
            var component = X.Button()
                            .ItemID("btnPause")
                            .Text("Tạm dừng").IconCls("x-fa fa-pause")
                            .DirectClickAction("Pause")
                            .Disabled(true)
                            .Method(HttpMethod.GET)
                            .Parameter(new Parameter("id", "this.up('grid').selModel.getSelection()[0].data.Id", ParameterMode.Raw));
            return component;
        }
        public static Button.Builder ButtonFinish(this BuilderFactory X, ResourceService.Operation operation)
        {
            var component = X.Button()
                            .ItemID("btnFinish")
                            .Text("Xác nhận kết thúc").IconCls("x-fa fa-check")
                            .DirectClickAction("Finish")
                            .Hidden(operation == ResourceService.Operation.Create)
                            .Method(HttpMethod.GET)
                            .Parameter(new Parameter("id", "App.Id.value", ParameterMode.Raw));
            return component;
        }
        public static Button.Builder ButtonConfirm(this BuilderFactory X)
        {
            var component = X.Button()
                            .ItemID("btnConfirm")
                            .Text("Xác nhận").IconCls("x-fa fa-check")
                            .DirectClickAction("Confirm")
                            .Disabled(true)
                            .Method(HttpMethod.GET)
                            .Parameter(new Parameter("id", "this.up('grid').selModel.getSelection()[0].data.Id", ParameterMode.Raw));
            return component;
        }
        public static Button.Builder ButtonCheck(this BuilderFactory X)
        {
            var component = X.Button()
                            .ItemID("btnCheck")
                            .Text("Kiểm tra")
                            .IconCls("x-fa x-fa fa-check")
                            .DirectClickAction("Check")
                            .Disabled(true)
                            .Method(HttpMethod.GET)
                            .Parameter(new Parameter("id", "this.up('grid').selModel.getSelection()[0].data.Id", ParameterMode.Raw));
            return component;
        }
        public static Button.Builder ButtonDelete(this BuilderFactory X)
        {
            var component = X.Button()
            .Text(Resource.Delete)
            .IconCls("x-fa fa-trash-o")
            .DirectEvents(de =>
            {
                de.Click.Method = HttpMethod.POST;
                de.Click.Confirmation.ConfirmRequest = true;
                de.Click.Confirmation.Message = Resource.ConfirmDelete;
                de.Click.Confirmation.Title = Resource.Confirm;
                de.Click.Confirmation.BeforeConfirm = "Ext.Msg.buttonText.yes='" + Resource.ConfirmYes + "'; Ext.Msg.buttonText.no='" + Resource.ConfirmNo + "';";
                de.Click.Before = "this.up('window').down('#loadMask').show(); this.up('window').down('#loadMaskEmpty').hide(); this.setDisabled(true);";
                de.Click.Url = X.XController().UrlHelper.Action("Delete");
                de.Click.Success = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); this.up('window').close();";
                de.Click.Failure = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); eval(result.script); this.setDisabled(false);";
            });
            return component;
        }
        public static Button.Builder ButtonCreate(this BuilderFactory X, bool hasDataEmployee = false)
        {
            var component = X.Button()
             .Text(Resource.Save)
            .IconCls("x-fa fa-floppy-o")
            .DirectEvents(de =>
            {
                de.Click.Method = HttpMethod.POST;
                if (hasDataEmployee)
                    de.Click.ExtraParams.Add(new { DataResourceEmployees = new JRawValue("getDataResourceEmployees()") });
                de.Click.Url = X.XController().UrlHelper.Action("Create");
                de.Click.Before = "this.setDisabled(true); this.up('window').down('#loadMask').show(); this.up('window').down('#loadMaskEmpty').hide();";
                de.Click.Success = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); this.up('window').close();";
                de.Click.Failure = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); eval(result.script); this.setDisabled(false);";
            });
            return component;
        }
        public static Button.Builder ButtonUpdate(this BuilderFactory X)
        {
            var component = X.Button()
           .Text(Resource.Save)
           .IconCls("x-fa fa-floppy-o")
           .DirectEvents(de =>
           {
               de.Click.Method = HttpMethod.POST;
               de.Click.Url = X.XController().UrlHelper.Action("Update");
               de.Click.Before = "this.setDisabled(true); this.up('window').down('#loadMask').show(); this.up('window').down('#loadMaskEmpty').hide();";
               de.Click.Success = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); this.up('window').close();";
               de.Click.Failure = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); eval(result.script); this.setDisabled(false);";
           });
            return component;
        }
        public static Button.Builder ButtonDelete(this BuilderFactory X, ResourceService.Operation operation)
        {
            var component = X.Button()
            .Text(Resource.Delete)
            .IconCls("x-fa fa-trash-o")
            .DirectEvents(de =>
            {
                de.Click.Method = HttpMethod.POST;
                de.Click.Confirmation.ConfirmRequest = true;
                de.Click.Confirmation.Message = Resource.ConfirmDelete;
                de.Click.Confirmation.Title = Resource.Confirm;
                de.Click.Confirmation.BeforeConfirm = "Ext.Msg.buttonText.yes='" + Resource.ConfirmYes + "'; Ext.Msg.buttonText.no='" + Resource.ConfirmNo + "';";
                de.Click.Before = "this.up('window').down('#loadMask').show(); this.up('window').down('#loadMaskEmpty').hide(); this.setDisabled(true);";
                de.Click.Url = X.XController().UrlHelper.Action("Delete");
                de.Click.ExtraParams.Add(X.Parameter().Name("id").Value("App.Id.getValue()").Mode(ParameterMode.Raw));
                de.Click.Success = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); this.up('window').close();";
                de.Click.Failure = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); eval(result.script); this.setDisabled(false);";
            })
            .Hidden(operation != ResourceService.Operation.UpdateDelete);
            return component;
        }
        public static Button.Builder ButtonCreate(this BuilderFactory X, ResourceService.Operation operation)
        {
            var component = X.Button()
            .Text(Resource.Save)
            .IconCls("x-fa fa-floppy-o")
            .DirectEvents(de =>
            {
                de.Click.Method = HttpMethod.POST;
                de.Click.Url = X.XController().UrlHelper.Action("Create");
                de.Click.Before = "if (!this.up('window').down('form').getForm().isValid()) { return false; }; this.setDisabled(true); this.up('window').down('#loadMask').show(); this.up('window').down('#loadMaskEmpty').hide();";
                de.Click.Success = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); this.up('window').close();";
                de.Click.Failure = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); eval(result.script); this.setDisabled(false);";
            })
            .Hidden(operation != ResourceService.Operation.Create);
            return component;
        }
        public static Button.Builder ButtonUpdate(this BuilderFactory X, ResourceService.Operation operation)
        {
            var component = X.Button()
            .Text(Resource.Save)
            .IconCls("x-fa fa-floppy-o")
            .DirectEvents(de =>
            {
                de.Click.Method = HttpMethod.POST;
                de.Click.Url = X.XController().UrlHelper.Action("Update");
                de.Click.Before = "if (!this.up('window').down('form').getForm().isValid()) { return false; }; this.setDisabled(true); this.up('window').down('#loadMask').show(); this.up('window').down('#loadMaskEmpty').hide();";
                de.Click.Success = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); this.up('window').close();";
                de.Click.Failure = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); eval(result.script); this.setDisabled(false);";
            })
            .Hidden(operation != ResourceService.Operation.Update && operation != ResourceService.Operation.UpdateDelete);
            return component;
        }
        public static Button.Builder ButtonSave(this BuilderFactory X)
        {
            var component = X.Button()
            .Text(Resource.Save)
            .IconCls("x-fa fa-floppy-o")
            .DirectEvents(de =>
            {
                de.Click.Method = HttpMethod.POST;
                de.Click.Before = "if (!this.up('window').down('form').getForm().isValid()) { return false; }; this.setDisabled(true); this.up('window').down('#loadMask').show(); this.up('window').down('#loadMaskEmpty').hide();";
                de.Click.Success = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show();if(result.result != false) this.up('window').close(); if(App.windowConfirmSChedule) ReloadScheduleForm(); if(App.grTaskSchedule) ReloadScheduleApprove();";
                de.Click.Failure = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); eval(result.script);";
                de.Click.After = "this.setDisabled(false);";
            });
            return component;
        }
        public static Button.Builder ButtonSend(this BuilderFactory X)
        {
            var component = X.Button()
                            .Text(Resource.Send).IconCls("x-fa fa-share-square-o")
                            .DirectEvents(de =>
                            {
                                de.Click.Method = HttpMethod.POST;
                                de.Click.Url = X.XController().UrlHelper.Action("Send");
                                de.Click.Before = "if (!this.up('window').down('form').getForm().isValid()) { return false; }; this.setDisabled(true); this.up('window').down('#loadMask').show(); this.up('window').down('#loadMaskEmpty').hide();";
                                de.Click.Success = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); this.up('window').close();";
                                de.Click.Failure = "this.up('window').down('#loadMask').hide(); this.up('window').down('#loadMaskEmpty').show(); eval(result.script); this.setDisabled(false);";
                            });
            return component;
        }
        public static Button.Builder ButtonExit(this BuilderFactory X)
        {
            var component = X.Button().Text(Resource.Exit).IconCls("x-fa fa-times-circle").OnClientClick("this.up('window').doDestroy();");
            return component;
        }

        public static Button.Builder ButtonSlideOut(this BuilderFactory X, string config = "r")
        {
            var component = X.Button().Text(Resource.Exit).IconCls("x-fa fa-times-circle").OnClientClick(string.Format("WindowSlideOut(this.up('window'),'{0}');", config));
            return component;
        }
        public static Button.Builder ButtonFormulaTime(this BuilderFactory X, string startId, string endId, string resultId, string calendarId = "")
        {
            var script = "Ext.Date.format(App.{0}.getValue(),App.{0}.submitFormat)";
            var scriptStart = string.Format(script, startId);
            var scriptEnd = string.Format(script, endId);
            var scriptCalendar = calendarId != "" ? string.Format("App.{0}.value", calendarId) : string.Empty;
            var handle = string.Format("App.{0}.setValue(result.result.length > 0 ? App.{0}.emptyText+': '+result.result : 'Chưa thiết lập thời gian');", resultId);
            var url = X.XController().UrlHelper.Action("FormulaTime", "Calendar", new { area = "Generic" });

            var component = X.Button().IconCls("x-fa fa-calculator").MarginSpec("0 -1 0 0").Url(url)
                            .Parameter(new Parameter("startAt", scriptStart, ParameterMode.Raw))
                            .Parameter(new Parameter("endAt", scriptEnd, ParameterMode.Raw))
                            .SuccessHandle(handle, false)
                            .Method(HttpMethod.GET)
                            .DirectEvents(de => de.Click.Type = DirectEventType.Load);
            if (scriptCalendar != "")
            {
                component.Parameter(new Parameter("calendarId", scriptCalendar, ParameterMode.Raw));
            }
            return component;
        }
        public static Button.Builder ButtonTaskPerform(this BuilderFactory X, string startId, string endId, string employeeId)
        {
            var script = "Ext.Date.format(App.{0}.getValue(),App.{0}.submitFormat)";
            var scriptStart = string.Format(script, startId);
            var scriptEnd = string.Format(script, endId);
            var scriptEmployee = string.Format("App.{0}.getSelectionModel().getLastSelected().data.EmployeeId", employeeId);
            var url = X.XController().UrlHelper.Action("Task", "Perform", new { area = "Task" });

            var component = X.Button().ID("btnTaskPerform").Text("Danh sách công việc").IconCls("x-fa fa-tasks").Url(url)
                            .Parameter(new Parameter("startAt", scriptStart, ParameterMode.Raw))
                            .Parameter(new Parameter("endAt", scriptEnd, ParameterMode.Raw))
                            .Parameter(new Parameter("employeeId", scriptEmployee, ParameterMode.Raw));
            return component;
        }
        public static Button.Builder ButtonSelectEmployee(this BuilderFactory X, string Type = default(string))
        {
            var script = @" var grid = this.up('window').down('grid');
                            var records = grid.selModel.getSelection();
                            selectEmployee(records);
                            this.up('window').close();";
            var scriptOther = @" var grid = this.up('window').down('grid');
                            var records = grid.selModel.getSelection();
                            selectEmployeeOther(records);
                            this.up('window').close();";

            var component = X.Button().IconCls("x-fa fa-check-circle-o").Text("Chọn").Handler(Type == "Other" ? scriptOther : script);
            return component;
        }
        #endregion
        public static Button.Builder SuccessHandle(this Button.Builder Button, string handle, bool allowCloseWindow = true)
        {
            var component = Button.ToComponent();
            var script = component.DirectEvents.Click.Success;
            if (!allowCloseWindow)
            {
                script = script.Replace("this.up('window').close();", string.Empty);
            }
            Button.DirectEvents(de =>
            {
                de.Click.Success = script + handle;
            });
            return Button;
        }
        public static Button.Builder FailureHandle(this Button.Builder Button, string handle)
        {
            var component = Button.ToComponent();
            Button.DirectEvents(de =>
            {
                de.Click.Failure = component.DirectEvents.Click.Failure + handle;
            });
            return Button;
        }
        public static Button.Builder GridIdForReload(this Button.Builder Button, string id)
        {
            var handle = "App." + id + ".getStore().reload();";
            var component = Button.ToComponent();
            Button.DirectEvents(de =>
            {
                de.Click.Success = component.DirectEvents.Click.Success + handle;
            });
            return Button;
        }
        public static Button.Builder Parameter(this Button.Builder Button, Parameter parameter)
        {
            Button.DirectEvents(de => de.Click.ExtraParams.Add(parameter));
            return Button;
        }
        public static Button.Builder RequestParameter(this Button.Builder Button, Parameter parameter)
        {
            var button = Button.ToComponent();
            var handle = button.Listeners.Click.Handler;
            var param = string.Empty;
            if (handle.IndexOf("parameter") > 0)
            {
                param = "params = {parameter :{" + parameter + ",";
                handle = handle.Replace("params = {parameter :{", param);
            }
            else
            {
                param = "params = {parameter :{" + parameter + "},";
                handle = handle.Replace("params = {", param);
            }
            button.Listeners.Click.Handler = handle;
            return Button;
        }
        public static Button.Builder RequestUrl(this Button.Builder Button, string url, bool isOther = false)
        {
            var script = @"var btn = this; btn.setDisabled(true); 
                           var params = {}; 
                           Ext.net.DirectMethod.request({
                               method: 'post',
                               params: params,
                               url: '[URL]',
                               success: function (result) {
                                   btn.setDisabled(false);
                               },
                               failure: function (result) {
                                   btn.setDisabled(false);
                               },
                         });";
            if (isOther)
            {
                script = @"var btn = this; btn.setDisabled(true); 
                           var params = { Type : 'Other'}; 
                           Ext.net.DirectMethod.request({
                               method: 'post',
                               params: params,
                               url: '[URL]',
                               success: function (result) {
                                   btn.setDisabled(false);
                               },
                               failure: function (result) {
                                   btn.setDisabled(false);
                               },
                         });";
            }
            Button.Listeners(ls => ls.Click.Handler = script.Replace("[URL]", url));
            return Button;
        }
        public static Button.Builder Method(this Button.Builder Button, HttpMethod method)
        {
            Button.DirectEvents(de => de.Click.Method = method);
            return Button;
        }
        public static Button.Builder Url(this Button.Builder Button, string url)
        {
            Button.DirectClickUrl(url);
            return Button;
        }
        public static Button.Builder FormID(this Button.Builder Button, string formId)
        {
            Button.DirectEvents(de => de.Click.FormID = formId);
            return Button;
        }
        public static Container.Builder ContainerFit(this BuilderFactory X)
        {
            var component = X.Container()
                            .Layout(LayoutType.Fit);
            return component;
        }
        public static Container.Builder ContainerHBox(this BuilderFactory X)
        {
            var component = X.Container()
                            .Defaults(new Parameter().ToBuilder().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                            .Layout(LayoutType.HBox)
                            .LayoutConfig(new HBoxLayoutConfig { Align = HBoxAlign.Stretch });
            return component;
        }
        public static Container.Builder ContainerHBoxTop(this BuilderFactory X)
        {
            var component = X.Container()
                            .Defaults(new Parameter().ToBuilder().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                            .Layout(LayoutType.HBox)
                            .LayoutConfig(new HBoxLayoutConfig { Align = HBoxAlign.Top });
            return component;
        }
        public static Container.Builder ContainerHBoxMiddle(this BuilderFactory X)
        {
            var component = X.Container()
                            .Defaults(new Parameter().ToBuilder().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                            .Layout(LayoutType.HBox)
                            .LayoutConfig(new HBoxLayoutConfig { Align = HBoxAlign.Middle });
            return component;
        }
        public static Container.Builder ContainerVBox(this BuilderFactory X)
        {
            var component = X.Container()
                            .Defaults(new Parameter().ToBuilder().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                            .Layout(LayoutType.VBox)
                            .LayoutConfig(new VBoxLayoutConfig { Align = VBoxAlign.Stretch });
            return component;
        }
        public static DateField.Builder DateTimeFieldFormat(this BuilderFactory X)
        {
            var component = X.DateField().Format("dd/MM/yyyy HH:mm").InvalidText("Không đúng định dạng!").FormatText(string.Empty).SubmitFormat(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FullDateTimePattern).HideLabel(true);
            return component;
        }
        public static DateField.Builder Validate(this DateField.Builder DateField, bool isEndAt)
        {
            if (isEndAt)
            {
                var script = "return App.EndAt.rawValue == '' || App.StartAt.rawValue == '' || App.EndAt.value > App.StartAt.value";
                DateField.Validator(i => i.Handler = script).ValidatorText("Thời gian kết thúc phải lớn hơn thời gian bắt đầu!");
            }
            DateField.Listeners(ls => ls.Change.Handler = "App.EndAt.validate();");
            return DateField;
        }
        public static DateField.Builder ValidateTarget(this DateField.Builder DateField, string targetId)
        {
            DateField.Listeners(ls => ls.Change.Handler = string.Format("App.{0}.validate();", targetId));
            return DateField;
        }
        public static DateField.Builder ValidateGrandThan(this DateField.Builder DateField, string targetId, string error = null)
        {
            var script = string.Format("return this.rawValue == '' || App.{0}.rawValue == '' || this.value > App.{0}.value", targetId);
            DateField.Validator(i => i.Handler = script).ValidatorText(error ?? "Thời gian kết thúc phải lớn hơn thời gian bắt đầu!");
            return DateField;
        }
        public static DateField.Builder ValidateGrandOrEqual(this DateField.Builder DateField, string targetId, string error = null)
        {
            var script = string.Format("return this.rawValue == '' || App.{0}.rawValue == '' || this.value >= App.{0}.value", targetId);
            DateField.Validator(i => i.Handler = script).ValidatorText(error ?? "Thời gian kết thúc phải lớn hơn hoặc bằng thời gian bắt đầu!");
            return DateField;
        }
        public static DateField.Builder ValidateLessOrEqual(this DateField.Builder DateField, string targetId, string error = null)
        {
            var script = string.Format("return this.rawValue == '' || App.{0}.rawValue == '' || this.value <= App.{0}.value", targetId);
            DateField.Validator(i => i.Handler = script).ValidatorText(error ?? "Thời gian bắt đầu phải nhỏ hơn hoặc bằng thời gian kết thúc!");
            return DateField;
        }
        public static DateField.Builder DateTimeSecondsFieldFormatFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null)
        {
            var value = X.GetValue(expression);
            var component = X.DateFieldFor(expression, setId, convert, format).Value(value).Format("dd/MM/yyyy HH:mm:ss").FormatText("dd/MM/yyyy HH:mm:ss").InvalidText("Không đúng định dạng!").SubmitFormat(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FullDateTimePattern).HideLabel(true);
            return component;
        }
        public static DateField.Builder DateTimeFieldFormatFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null)
        {
            var value = X.GetValue(expression);
            var component = X.DateFieldFor(expression, setId, convert, format).Value(value).Format("dd/MM/yyyy HH:mm").FormatText(string.Empty).InvalidText("Không đúng định dạng!").SubmitFormat(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FullDateTimePattern).HideLabel(true);
            return component;
        }
        public static DateField.Builder DateFieldFormatFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null)
        {
            var component = X.DateFieldFor(expression, setId, convert, format).Format("dd/MM/yyyy").InvalidText("Không đúng định dạng!").FormatText(string.Empty).HideLabel(true);
            return component;
        }
        public static TimeField.Builder TimeFieldFormatFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null)
        {
            var component = X.TimeFieldFor(expression, setId, convert, format).Format("HH:mm").InvalidText("Không đúng định dạng!").FormatText(string.Empty).SubmitFormat("HH:mm").HideLabel(true);
            return component;
        }
        public static Checkbox.Builder CheckboxFormatFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null)
        {
            var component = X.CheckboxFor(expression, setId, convert, format).EmptyValue(false).HideLabel(true).UncheckedValue("false");
            return component;
        }
        public static TextField.Builder TextFieldFormatFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null)
        {
            var value = Convert.ToString(X.GetValue(expression));
            var component = X.TextFieldFor(expression, setId, convert, format).Margin(0).HideLabel(true).Value(HttpUtility.HtmlDecode(value))
                            .Regex(@"^[\s\S]*\S+[\s\S]*$").RegexText("Cần nhập ký tự khác khoảng trắng!");
            return component;
        }
        public static TextArea.Builder Expand(this TextArea.Builder TextArea, bool expand = true)
        {
            if (expand)
            {
                TextArea.RightButtons(Html.X().Button()
                    .IconCls("x-fa fa-expand")
                    .MaxHeight(20)
                    .MaxWidth(20)
                    .StyleSpec("border: none;")
                    .Listeners(ls => ls.Click.Handler = "expandArea(this.findParentByType('textarea'));"));
            }
            return TextArea;
        }
        public static TextArea.Builder TextAreaFormatFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null)
        {
            var value = Convert.ToString(X.GetValue(expression));
            var component = X.TextAreaFor(expression, setId, convert, format).HideLabel(true).Margin(0).Value(HttpUtility.HtmlDecode(value))
                            .Regex(@"^[\s\S]*\S+[\s\S]*$").RegexText("Cần nhập ký tự khác khoảng trắng!"); ;
            return component;
        }
        public static ComboBox.Builder DisplayFieldEncodeFormat(this ComboBox.Builder ComboBox, string fieldEncode)
        {
            ComboBox.ListConfig(
                Html.X().BoundList()
                    .LoadingText("Đang tìm...")
                    .LoadMask(false)
                    .ItemTpl(
                    Html.X().XTemplate()
                        .Html("<text><div class='list-combo-item'>{" + fieldEncode + ":htmlEncode}</div></text>")
                        )
                    );
            return ComboBox;
        }
        #region Combobox DepartmentFor
        public static FieldSet.Builder ComboboxTreeFormatFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression)
             where TProperty : TreeDepartmentBO
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var dropdownId = "DR" + X.GetComponentID();
            var script = @"<script> 
                    var getValues = function (tree) {
                            var result = [], selNodes = tree.getChecked();
                            Ext.each(selNodes, function (node) {
                                result.push(node.data.id);
                            });
                            return result.join(',');
                        };
                        var getText = function (tree) {
                            var result = [], selNodes = tree.getChecked();
                            Ext.each(selNodes, function (node) {
                                result.push(node.data.text);
                            });
                            return result.join(',');
                        };
                    var filterTree = function (tf, e) {
                        var tree = tf.up('treepanel'),
                                store = tree.store,
                                logic = tree,
                                text = tf.getRawValue();
                            logic.clearFilter();
                            if (Ext.isEmpty(text, false)) {
                                return;
                            }
                            if (e.getKey() === Ext.EventObjectImpl.ESC) {
                                clearFilter();
                            } else {
                                var re = new RegExp('.*' + text + '.*', 'i');
                                logic.filterBy(function (node) {
                                    return re.test(node.data.text);
                                });
                            }
                        };
                        var clearFilter = function () {
                            var field = this,
                                tree = this.up('treepanel'),
                                store = tree.store,
                                logic = tree;
                            field.setValue('');
                            logic.clearFilter(true);
                            tree.getView().focus();
                        };
                     </script>";
            var container = X.FieldSet().Layout(LayoutType.Fit).Border(false).Padding(0);
            var hdfValue = X.Hidden().ID(key + "Value").Name(key + "Value");
            if (value != null) hdfValue.Value(value.Value);
            var hdfFromView = X.Hidden().ID(key + "FromView").Name(key + "FromView").Value(true);
            var component = X.DropDownFieldFor(expression)
                .SubmitValue(false)
                .AllowBlank(false)
                .ID(dropdownId)
                .Mode(DropDownMode.ValueText)
                .Text(value == null ? string.Empty : value.Text)
                .ToolTips(c => c.Add(X.ToolTip(new ToolTip.Config { Title = value == null ? string.Empty : value.Text, Width = 300, Height = 200 })))
                .Name(key + "Text")
                .HtmlBin(c => script)
                .EmptyText("Lựa chọn phòng ban, chức danh, nhân sự...")
                .Editable(false)
                .Listeners(ls => ls.Change.Handler = "Ext.getCmp('" + key + "Value').setValue(this.getValue());");

            var tree = X.TreePanel()
                .Border(true)
                .Header(false)
                .Expand(true)
                .Height(300)
                .MinWidth(320)
                .IconCls("x-fa fa-home")
                .RootVisible(false)
                .SingleExpand(true)
                .Shadow(false);
            tree.TopBarItem(X.TextField().Flex(1).IconCls("x-fa fa-search").EmptyText("Nhập từ khóa tìm kiếm...")
                .EnableKeyEvents(true).Listeners(l => { l.KeyUp.Fn = "filterTree"; l.KeyUp.Buffer = 250; l.TriggerClick.Fn = "clearFilter"; }));
            var treeStore = X.TreeStore().Proxy(X.AjaxProxy().Url(X.XController()
                .UrlHelper.Action("LoadTreeData", "OrganizationTree", new { Area = "Organization" })));

            tree.Store(treeStore);
            tree.SelectionModel(Html.X().TreeSelectionModel().Mode(SelectionMode.Single));
            tree.ViewConfig(Html.X().TreeView().LoadMask(false))
            .Listeners(ls =>
            {
                ls.Load.Handler = "var node = this.getRootNode(); node.expandChildren();";
                ls.CheckChange.Handler = "this.dropDownField.setValue(getValues(this), getText(this), false);";
            });
            component.Component(c => c.Add(tree));


            container.Add(component);
            container.Add(hdfValue);
            container.Add(hdfFromView);
            return container;
        }
        public static FieldSet.Builder ComboboxTitleFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression
             , string title = "")
            where TProperty : TreeDepartmentBO
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var script = @"<script> 
                     var getValues = function (tree) {
                            var result = [], selNodes = tree.getChecked();
                            Ext.each(selNodes, function (node) {
                                result.push(node.data.id);
                            });
                            return result.join(',');
                        };
                        var getText = function (tree) {
                            var result = [], selNodes = tree.getChecked();
                            Ext.each(selNodes, function (node) {
                                result.push(node.data.text);
                            });
                            return result.join(',');
                        };
                    var filterTree = function (tf, e) {
                        var tree = tf.up('treepanel'),
                                store = tree.store,
                                logic = tree,
                                text = tf.getRawValue();
                            logic.clearFilter();
                            if (Ext.isEmpty(text, false)) {
                                return;
                            }
                            if (e.getKey() === Ext.EventObjectImpl.ESC) {
                                clearFilter();
                            } else {
                                var re = new RegExp('.*' + text + '.*', 'i');
                                logic.filterBy(function (node) {
                                    return re.test(node.data.text);
                                });
                            }
                        };
                        var clearFilter = function () {
                            var field = this,
                                tree = this.up('treepanel'),
                                store = tree.store,
                                logic = tree;
                            field.setValue('');
                            logic.clearFilter(true);
                            tree.getView().focus();
                        };
                     </script>";
            var container = X.FieldSet().Layout(LayoutType.Fit).Border(false).Padding(0);
            var hdfValue = X.Hidden().ID(key + "Value").Name(key + "Value");
            var hdfFromView = X.Hidden().ID(key + "FromView").Name(key + "FromView").Value(true);
            var component = X.DropDownFieldFor(expression)
                .SubmitValue(true).AllowBlank(false).Mode(DropDownMode.ValueText).Name(key + "Text").HtmlBin(c => script).EmptyText("Lựa chọn chức danh...").Editable(false)
                .Listeners(ls => ls.Change.Handler = "Ext.getCmp('" + key + "Value').setValue(this.getValue());");
            var tree = X.TreePanel().Border(true).Header(false).Width(250).MaxHeight(400).Expand(false).IconCls("x-fa fa-home").RootVisible(false).SingleExpand(true).Shadow(false);
            tree.TopBarItem(X.TextField().Flex(1).IconCls("x-fa fa-search").EmptyText("Nhập từ khóa tìm kiếm...").EnableKeyEvents(true).Listeners(l => { l.KeyUp.Fn = "filterTree"; l.KeyUp.Buffer = 250; l.TriggerClick.Fn = "clearFilter"; }));
            var treeStore = X.TreeStore().Proxy(X.AjaxProxy().Url(X.XController().UrlHelper.Action("LoadTreeData", "OrganizationTree", new { Area = "Organization" })));
            treeStore.Parameters(ps =>
                    {
                        ps.Add(new StoreParameter("showTitle", true.ToString(), ParameterMode.Value));
                        ps.Add(new StoreParameter("disableSelectDepartment", true.ToString(), ParameterMode.Value));
                    });
            tree.Store(treeStore);
            tree.SelectionModel(Html.X().TreeSelectionModel().Mode(SelectionMode.Single));
            tree.ViewConfig(Html.X().TreeView().LoadMask(false))
                .Listeners(ls =>
                {
                    ls.CheckChange.Handler = "this.dropDownField.setValue(getValues(this), getText(this), false);";
                });
            component.Component(c => c.Add(tree));
            container.Add(component);
            container.Add(hdfValue);
            container.Add(hdfFromView);
            return container;
        }
        #endregion
        #region Combobox Department
        public static FieldSet.Builder FieldLabel(this FieldSet.Builder fieldSet, string FieldLabel = "", int LabelWidth = 100)
        {
            if (!string.IsNullOrEmpty(FieldLabel))
            {
                var container = fieldSet.ToComponent();
                var drop = container.Items.Where(i => i is DropDownField).Cast<DropDownField>().FirstOrDefault();
                drop.FieldLabel = FieldLabel;
                drop.LabelWidth = LabelWidth;
            }
            return fieldSet;
        }
        /// <summary>
        /// Cho phép chọn nhiều giá trị trên tree
        /// </summary>
        /// <param name="dropDownField"></param>
        /// <param name="multiple"></param>
        /// <returns></returns>
        public static DropDownField.Builder Multiple(this DropDownField.Builder dropDownField, bool multiple = true)
        {
            if (!multiple)
            {
                var container = dropDownField.ToComponent();
                var treepanel = container.Component.Where(i => i is TreePanel).Cast<TreePanel>().FirstOrDefault();
                treepanel.Listeners.BeforeCheckChange.Handler = "this.clearChecked();";
            }
            return dropDownField;
        }
        /// <summary>
        /// Cho phép hiện thị chức danh trên tree
        /// </summary>
        /// <param name="dropDownField"></param>
        /// <param name="showTitle"></param>
        /// <returns></returns>
        public static DropDownField.Builder ShowTitle(this DropDownField.Builder dropDownField, bool showTitle = false)
        {
            var container = dropDownField.ToComponent();
            var treepanel = container.Component.Where(i => i is TreePanel).Cast<TreePanel>().FirstOrDefault();
            var treeStore = treepanel.Store.Where(i => i is TreeStore).Cast<TreeStore>().FirstOrDefault();
            treeStore.Parameters.Add(new StoreParameter("showTitle", showTitle.ToString(), ParameterMode.Value));
            return dropDownField;
        }
        /// <summary>
        /// Không cho phép chọn phòng ban
        /// </summary>
        /// <param name="dropDownField"></param>
        /// <param name="disabled"></param>
        /// <returns></returns>
        public static DropDownField.Builder DisableSelectDepartment(this DropDownField.Builder dropDownField, bool disabled = false)
        {
            var container = dropDownField.ToComponent();
            var treepanel = container.Component.Where(i => i is TreePanel).Cast<TreePanel>().FirstOrDefault();
            var treeStore = treepanel.Store.Where(i => i is TreeStore).Cast<TreeStore>().FirstOrDefault();
            treeStore.Parameters.Add(new StoreParameter("disableSelectDepartment", disabled.ToString(), ParameterMode.Value));
            return dropDownField;
        }
        /// <summary>
        /// Không cho phép chọn chức danh
        /// </summary>
        /// <param name="dropDownField"></param>
        /// <param name="disabled"></param>
        /// <returns></returns>
        public static DropDownField.Builder DisableSelectTitle(this DropDownField.Builder dropDownField, bool disabled = false)
        {
            var container = dropDownField.ToComponent();
            var treepanel = container.Component.Where(i => i is TreePanel).Cast<TreePanel>().FirstOrDefault();
            var treeStore = treepanel.Store.Where(i => i is TreeStore).Cast<TreeStore>().FirstOrDefault();
            treeStore.Parameters.Add(new StoreParameter("disableSelectTitle", disabled.ToString(), ParameterMode.Value));
            return dropDownField;
        }
        /// <summary>
        /// Không cho phép chọn nhân sự
        /// </summary>
        /// <param name="dropDownField"></param>
        /// <param name="disabled"></param>
        /// <returns></returns>
        public static DropDownField.Builder DisableSelectEmployee(this DropDownField.Builder dropDownField, bool disabled = false)
        {
            var container = dropDownField.ToComponent();
            var treepanel = container.Component.Where(i => i is TreePanel).Cast<TreePanel>().FirstOrDefault();
            var treeStore = treepanel.Store.Where(i => i is TreeStore).Cast<TreeStore>().FirstOrDefault();
            treeStore.Parameters.Add(new StoreParameter("disableSelectEmployee", disabled.ToString(), ParameterMode.Value));
            return dropDownField;
        }
        /// <summary>
        /// Hiện thị nhân sự trên tree
        /// </summary>
        /// <param name="dropDownField"></param>
        /// <param name="showEmployee"></param>
        /// <returns></returns>
        public static DropDownField.Builder ShowEmployee(this DropDownField.Builder dropDownField, bool showEmployee = false)
        {
            var container = dropDownField.ToComponent();
            var treepanel = container.Component.Where(i => i is TreePanel).Cast<TreePanel>().FirstOrDefault();
            var treeStore = treepanel.Store.Where(i => i is TreeStore).Cast<TreeStore>().FirstOrDefault();
            treeStore.Parameters.Add(new StoreParameter("showEmployee", showEmployee.ToString(), ParameterMode.Value));
            return dropDownField;
        }
        public static DropDownField.Builder showCheckBox(this DropDownField.Builder dropDownField, bool showCheckBox = false)
        {
            var container = dropDownField.ToComponent();
            var treepanel = container.Component.Where(i => i is TreePanel).Cast<TreePanel>().FirstOrDefault();
            var treeStore = treepanel.Store.Where(i => i is TreeStore).Cast<TreeStore>().FirstOrDefault();
            treeStore.Parameters.Add(new StoreParameter("showCheckBox", showCheckBox.ToString(), ParameterMode.Value));
            return dropDownField;
        }
        public static DropDownField.Builder OnlyTitle(this DropDownField.Builder dropDownField, string onlyTitle = default(string))
        {
            if (onlyTitle != default(string))
            {
                var container = dropDownField.ToComponent();
                var treepanel = container.Component.Where(i => i is TreePanel).Cast<TreePanel>().FirstOrDefault();
                var treeStore = treepanel.Store.Where(i => i is TreeStore).Cast<TreeStore>().FirstOrDefault();
                treeStore.Parameters.Add(new StoreParameter("onlyTitle", onlyTitle, ParameterMode.Value));
            }
            return dropDownField;
        }
        public static ComboBox.Builder ComboboxEmployeeFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null)
             where TProperty : EmployeeBO
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var component = X.ComboBox()
                .DisplayField("Name")
                .ValueField("Id")
                .HideLabel(true)
                .TypeAhead(false).PageSize(5)
                .HideTrigger(true)
                .EmptyText("Nhập tên nhân sự ....")
                .MinChars(0).CheckChangeBuffer(200)
                .TriggerAction(TriggerAction.Query)
                .ListConfig(Html.X().BoundList()
                .LoadingText("Tìm kiếm...")
                .ItemTpl(Html.X().XTemplate()
                            .Html(@"<text>
                                    <img src='{AvatarUrl}' style='margin-top: 2px; width: 30px; height: 30px; border-radius: 50%; 
                                    position:absolute;border: 1px solid'>
                                    <span style='margin-left: 44px;'>{FullNameFormat}</span><br/>
                                    <div style='margin-left: 54px;font-size:10px;'>{RoleNames}</div>
                                </text>")
                        )
                )
                .Store(Html.X().Store()
                    .AutoLoad(false)
                    .Proxy(Html.X().AjaxProxy()
                    .Url(X.XController()
                        .UrlHelper.Action("LoadEmployeesQuery", "Organization", new { Area = "Organization" })
                        )
                    .ActionMethods(am => am.Read = HttpMethod.POST)
                    .Reader(Html.X().JsonReader().RootProperty("data"))
                    )
                    .Model(Html.X().Model().IDProperty("Id")
                        .Fields(
                            Html.X().ModelField().Name("Id").Type(ModelFieldType.Auto),
                            Html.X().ModelField().Name("AvatarUrl").Type(ModelFieldType.String),
                            Html.X().ModelField().Name("Name").Type(ModelFieldType.String),
                            Html.X().ModelField().Name("RoleNames").Type(ModelFieldType.String),
                            Html.X().ModelField().Name("FullNameFormat").Type(ModelFieldType.String)
                        )
                    )
                );
            if (value != null)
            {
                var c = component.ToComponent();
                c.Text = value.Name;
            }
            return component;
        }
        public static DropDownField.Builder ComboboxTreeFormat(this BuilderFactory X)
        {
            var script = @"<script> 
                     var getValues = function (tree) {
                            var result = [], selNodes = tree.getChecked();
                            Ext.each(selNodes, function (node) {
                                result.push(node.data.id);
                            });
                            return result.join(',');
                        };
                        var getText = function (tree) {
                            var result = [], selNodes = tree.getChecked();
                            Ext.each(selNodes, function (node) {
                                result.push(node.data.text);
                            });
                            return result.join(',');
                        };
                      var getValues1 = function (tree) {
                            var result = [], selNodes = tree.getSelectedNodes();
                            Ext.each(selNodes, function (node) {
                                node.Checked(true);
                                result.push(node.value);
                            });
                            return result.join(',');
                           
                        };
                      var getText1 = function (tree) {
                            var result = [], selNodes = tree.getSelectedNodes();
                            Ext.each(selNodes, function (node) {
                                result.push(node.text);
                            });
                            return result.join(',');
                        };
                    var filterTree = function (tf, e) {
                        var tree = tf.up('treepanel'),
                                store = tree.store,
                                logic = tree,
                                text = tf.getRawValue();
                            logic.clearFilter();
                            if (Ext.isEmpty(text, false)) {
                                return;
                            }
                            if (e.getKey() === Ext.EventObjectImpl.ESC) {
                                clearFilter();
                            } else {
                                var re = new RegExp('.*' + text + '.*', 'i');
                                logic.filterBy(function (node) {
                                    return re.test(node.data.text);
                                });
                            }
                        };
                        var clearFilter = function () {
                            var field = this,
                                tree = this.up('treepanel'),
                                store = tree.store,
                                logic = tree;
                            field.setValue('');
                            logic.clearFilter(true);
                            tree.getView().focus();
                        };
                        var EventClick = function () {
                            $('.x-grid-item-selected .x-tree-expander').click();
                        };
                     </script>";
            var component = X.DropDownField()
                .AllowBlank(false)
                .Mode(DropDownMode.ValueText)
                .HtmlBin(c => script)
                .EmptyText("Lựa chọn phòng ban....")
                .HideLabel(true)
                .Editable(false);
            var tree = X.TreePanel()
                .Border(true)
                .Header(false)
                .ToFrontOnShow(true)
                .Constrain(true)
                .Height(300)
                .MinWidth(320)
                .IconCls("x-fa fa-home")
                .RootVisible(false)
                .SingleExpand(true)
                .Shadow(true);
            tree.TopBarItem(X.TextField().Flex(1).IconCls("x-fa fa-search").EmptyText("Nhập từ khóa tìm kiếm...").EnableKeyEvents(true).Listeners(l => { l.KeyUp.Fn = "filterTree"; l.KeyUp.Buffer = 250; l.TriggerClick.Fn = "clearFilter"; }));
            var treeStore = X.TreeStore().Proxy(X.AjaxProxy().Url(X.XController().UrlHelper.Action("LoadTreeData", "OrganizationTree", new { Area = "Organization" })));
            tree.Store(treeStore);
            tree.SelectionModel(Html.X().TreeSelectionModel().Mode(SelectionMode.Single));
            tree.ViewConfig(Html.X().TreeView().LoadMask(false))
            .Listeners(ls =>
            {
                ls.Load.Handler = "var node = this.getRootNode(); node.expandChildren(); this.selModel.select(node.firstChild);";
                ls.CellClick.Handler = "EventClick();this.dropDownField.setValue(record.data.id,record.data.text,false);if(App.ComboBoxRole) {App.ComboBoxRole.clearValue(); App.ComboBoxRole.getStore().load();}";

                //this.dropDownField.setValue(getValues1(this), getText1(this), false); if (App.ComboBoxRole) { App.ComboBoxRole.clearValue(); App.ComboBoxRole.getStore().load(); }
                ls.CheckChange.Handler = "this.dropDownField.setValue(getValues(this), getText(this), false);if(App.tab2_NewTask) addResourceEmployees();if(App.ComboBoxRole) {App.ComboBoxRole.clearValue(); App.ComboBoxRole.getStore().load();} if(App.stMnKPIForTask) App.stMnKPIForTask.reload();";
            });
            component.Component(c => c.Add(tree));
            return component;
        }
        public static DropDownField.Builder ComboboxTreeFormatKPI(this BuilderFactory X)
        {
            var script = @"<script> 
                     var getValues = function (tree) {
                            var result = [], selNodes = tree.getChecked();
                            Ext.each(selNodes, function (node) {
                                result.push(node.data.id);
                            });
                            return result.join(',');
                        };
                        var getText = function (tree) {
                            var result = [], selNodes = tree.getChecked();
                            Ext.each(selNodes, function (node) {
                                result.push(node.data.text);
                            });
                            return result.join(',');
                        };
                    var filterTree = function (tf, e) {
                        var tree = tf.up('treepanel'),
                                store = tree.store,
                                logic = tree,
                                text = tf.getRawValue();
                            logic.clearFilter();
                            if (Ext.isEmpty(text, false)) {
                                return;
                            }
                            if (e.getKey() === Ext.EventObjectImpl.ESC) {
                                clearFilter();
                            } else {
                                var re = new RegExp('.*' + text + '.*', 'i');
                                logic.filterBy(function (node) {
                                    return re.test(node.data.text);
                                });
                            }
                        };
                        var clearFilter = function () {
                            var field = this,
                                tree = this.up('treepanel'),
                                store = tree.store,
                                logic = tree;
                            field.setValue('');
                            logic.clearFilter(true);
                            tree.getView().focus();
                        };
                        var EventClick = function () {
                            $('#' + App.KPIArchiveId.component.id +' .x-grid-item-selected .x-tree-expander').click();
                        };
                     </script>";
            var component = X.DropDownField()
                .Mode(DropDownMode.ValueText)
                .HtmlBin(c => script)
                .EmptyText("Lựa chọn KPI....")
                .HideLabel(true)
                .Draggable(false)
                .RepeatTriggerClick(false)
                .Editable(false);
            var tree = X.TreePanel()
                .Border(true)
                .Header(false)
                .ToFrontOnShow(false)
                .Constrain(true)
                .Height(300)
                .MinWidth(320)
                .IconCls("x-fa fa-home")
                .RootVisible(false)
                .SingleExpand(true)
                .Shadow(true);
            tree.TopBarItem(X.TextField().Flex(1).IconCls("x-fa fa-search").EmptyText("Nhập từ khóa tìm kiếm...").EnableKeyEvents(true).Listeners(l => { l.KeyUp.Fn = "filterTree"; l.KeyUp.Buffer = 250; l.TriggerClick.Fn = "clearFilter"; }));
            var treeStore = X.TreeStore().Proxy(X.AjaxProxy().Url(X.XController().UrlHelper.Action("LoadDataTree", "KPICategory", new { Area = "KPI" })));
            tree.Store(treeStore);
            tree.SelectionModel(Html.X().TreeSelectionModel().Mode(SelectionMode.Multi));
            tree.ViewConfig(Html.X().TreeView().LoadMask(false))
            .Listeners(ls =>
            {
                ls.CheckChange.Handler = "this.dropDownField.setValue(getValues(this), getText(this), false);if(App.HiddenKPICatagoryId) App.HiddenKPICatagoryId.setValue(getValues(this)); if(App.txtMaxScore) setTxTMaxScore(getValues(this))";
                ls.CellClick.Handler = "EventClick();";
            });
            component.Component(c => c.Add(tree));
            return component;
        }
        public static DropDownField.Builder ComboboxTreeWindowFormat(this BuilderFactory X)
        {
            var script = @"<script> 
                     var getValues = function (tree) {
                            var result = [], selNodes = tree.getChecked();
                            Ext.each(selNodes, function (node) {
                                result.push(node.data.id);
                            });
                            return result.join(',');
                        };
                        var getText = function (tree) {
                            var result = [], selNodes = tree.getChecked();
                            Ext.each(selNodes, function (node) {
                                result.push(node.data.text);
                            });
                            return result.join(',');
                        };
                    var filterTree = function (tf, e) {
                        var tree = tf.up('treepanel'),
                                store = tree.store,
                                logic = tree,
                                text = tf.getRawValue();
                            logic.clearFilter();
                            if (Ext.isEmpty(text, false)) {
                                return;
                            }
                            if (e.getKey() === Ext.EventObjectImpl.ESC) {
                                clearFilter();
                            } else {
                                var re = new RegExp('.*' + text + '.*', 'i');
                                logic.filterBy(function (node) {
                                    return re.test(node.data.text);
                                });
                            }
                        };
                        var clearFilter = function () {
                            var field = this,
                                tree = this.up('treepanel'),
                                store = tree.store,
                                logic = tree;
                            field.setValue('');
                            logic.clearFilter(true);
                            tree.getView().focus();
                        };
                     </script>";
            var component = X.DropDownField().AllowBlank(false).Mode(DropDownMode.ValueText).HtmlBin(c => script).EmptyText("Lựa chọn phòng ban, chức danh, nhân sự...").HideLabel(true).Editable(false);
            var window = X.Window()
                .Closable(false)
                .Listeners(
                    ls =>
                    {
                        ls.BeforeShow.Handler = "this.setHeight(App.ctDesktop.getHeight() /2);this.setWidth(this.dropDownField.getWidth()); this.alignTo(this.up('window').id,'tr-tr'); this.el.slideIn('t',{duration : 500});";
                        ls.BeforeClose.Handler = "this.el.slideOut('t',{duration : 500}); return false;";
                    }).Draggable(false);
            var tree = X.TreePanel().Border(true).Header(false).Expand(false).IconCls("x-fa fa-home").RootVisible(false).SingleExpand(true).Shadow(false);
            tree.TopBarItem(X.TextField().Flex(1).IconCls("x-fa fa-search").EmptyText("Nhập từ khóa tìm kiếm...").EnableKeyEvents(true).Listeners(l => { l.KeyUp.Fn = "filterTree"; l.KeyUp.Buffer = 250; l.TriggerClick.Fn = "clearFilter"; }));
            var treeStore = X.TreeStore().Proxy(X.AjaxProxy().Url(X.XController().UrlHelper.Action("LoadTreeData", "OrganizationTree", new { Area = "Organization" })));
            tree.Store(treeStore);
            tree.SelectionModel(Html.X().TreeSelectionModel().Mode(SelectionMode.Single));
            tree.ViewConfig(Html.X().TreeView().LoadMask(false))
                .Listeners(ls =>
                {
                    ls.CheckChange.Handler = "this.dropDownField.setValue(getValues(this), getText(this), false);";
                });
            window.Add(tree);
            component.Component(c => c.Add(window));
            return component;
        }
        #endregion
        #region FileUpload
        public static FieldSet.Builder Multiple(this FieldSet.Builder FieldSet, bool multiple = true)
        {
            var config = "this.fileInputEl.set({ accept: '*' });";
            if (multiple)
            {
                config = config + "this.fileInputEl.set({ multiple: true });";
            }
            var container = FieldSet.ToComponent();
            var container1 = container.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var fileUploadField = container1.Items.Where(i => i is FileUploadField).Cast<FileUploadField>().FirstOrDefault();
            fileUploadField.Listeners.AfterRender.Handler = config;
            return FieldSet;
        }
        public static FieldSet.Builder AllowBlank(this FieldSet.Builder FieldSet, bool allowBlank = true)
        {
            var container = FieldSet.ToComponent();
            var container1 = container.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var displayField = container1.Items.Where(i => i is TextField).Cast<TextField>().FirstOrDefault();
            displayField.AllowBlank = allowBlank;
            displayField.BlankText = "Trường bắt buộc nhập!";
            return FieldSet;
        }
        public static FieldSet.Builder Note(this FieldSet.Builder FieldSet, string note = default(string))
        {
            var container = FieldSet.ToComponent();
            var container1 = container.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var displayField = container1.Items.Where(i => i is TextField).Cast<TextField>().FirstOrDefault();
            var fileUploadField = container1.Items.Where(i => i is FileUploadField).Cast<FileUploadField>().FirstOrDefault();
            var buttons = container1.Items.Where(i => i is Button).Cast<Button>();
            displayField.Note = note;
            displayField.MarginSpec = "0 -1 0 0";
            fileUploadField.MarginSpec = "17 -3 0 -2";
            foreach (var button in buttons)
            {
                button.MarginSpec = "17 0 0 0";
                button.Height = 32;
            }

            displayField.NoteAlign = NoteAlign.Top;
            return FieldSet;
        }
        public static FieldSet.Builder FieldLabel(this FieldSet.Builder FieldSet, string fieldLabel = default(string))
        {
            var container = FieldSet.ToComponent();
            var container1 = container.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var displayField = container1.Items.Where(i => i is TextField).Cast<TextField>().FirstOrDefault();
            displayField.FieldLabel = fieldLabel;
            displayField.HideLabel = false;
            return FieldSet;
        }
        public static FieldSet.Builder ImgUploadReadOnly(this FieldSet.Builder FieldSet, bool readOnly = false)
        {
            var container = FieldSet.ToComponent();
            var fileUploadField = container.Items.Where(i => i is FileUploadField).Cast<FileUploadField>().FirstOrDefault();
            if (readOnly)
            {
                fileUploadField.Hidden = readOnly;
            }
            return FieldSet;
        }
        public static FieldSet.Builder FileUploadReadOnly(this FieldSet.Builder FieldSet, bool readOnly = false)
        {
            var container = FieldSet.ToComponent();
            var container1 = container.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var fileUploadField = container1.Items.Where(i => i is FileUploadField).Cast<FileUploadField>().FirstOrDefault();
            var tol = container1.Items.Where(i => i is Button).Cast<Button>().LastOrDefault();
            var tos = tol.Menu.Where(i => i is Menu).Cast<Menu>().FirstOrDefault();
            var grid = tos.Items.Where(i => i is GridPanel).Cast<GridPanel>().FirstOrDefault();
            var toolbar = grid.TopBar.Where(i => i is ToolbarBase).Cast<ToolbarBase>().FirstOrDefault();
            var btnDelete = toolbar.Items.Where(i => i is Button).Cast<Button>().Where(i => i.ItemID == "btnDelete").FirstOrDefault();
            if (readOnly)
            {
                btnDelete.Hidden = readOnly;
                fileUploadField.Hidden = readOnly;
            }
            return FieldSet;
        }
        public static FieldSet.Builder LabelWidth(this FieldSet.Builder FieldSet, int labelWidth = 100)
        {
            var container = FieldSet.ToComponent();
            var container1 = container.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var displayField = container1.Items.Where(i => i is TextField).Cast<TextField>().FirstOrDefault();
            displayField.LabelWidth = labelWidth;
            return FieldSet;
        }
        public static FieldSet.Builder FileUploadFieldFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression)
          where TProperty : FileUploadBO
        {
            var key = X.GetKey(expression);
            var idKey = key.Replace(".", string.Empty) + X.GetComponentID();
            var value = X.GetValue(expression);
            var script = @"<script> 
                    var addFile" + idKey + @" = function (fb) {   
                            for (var i = 0; i < fb.fileInputEl.dom.files.length; i++) { 
                                var f = fb.fileInputEl.dom.files[i], filename = f.name, extension = f.name.split('.').pop(), size = f.size;  
                                if (Ext.StoreMgr.lookup(App." + idKey + @"dymanicStoreFile).findExact('Name', filename) < 0) {
                                    App." + idKey + @"dymanicStoreFile.add({Name: filename, Extension: extension,  Size: size, IsNew: true });
                                }
                            }                          
                        };
                    var deleteFile" + idKey + @" = function (obj, record) {
                        var hdfFilesRemove" + idKey + @" = Ext.getCmp('" + key + "ListFileRemove'); " +
                        @"if(hdfFilesRemove" + idKey + @".value) 
                            {
                                var deleteArray = hdfFilesRemove" + idKey + @".value.split(',');
                                var exits = false; for (var i = 0; i < deleteArray.length; i++) {
                                    if (deleteArray[i] == record.data.Id) { exits = true; };
                                };
                                if (!exits) { deleteArray.push(record.data.Id); };
                                hdfFilesRemove" + idKey + @".setValue(deleteArray.toString());
                                }
                                else {
                                    hdfFilesRemove" + idKey + @".setValue(record.data.Id);
                                }
                                var textField = hdfFilesRemove" + idKey + @".next();
                                if (hdfFilesRemove" + idKey + @".value) {
                                    var textFileValue = 'Đã chọn: ' + App." + idKey + @"dymanicStoreFile.count() + ' tệp đính kèm (Xóa: ' + hdfFilesRemove" + idKey + @".value.split(',').length + ')';
                                    textField.setValue(textFileValue);
                                    record.set({ IsDelete: true });
                                    obj.hide();
                                    obj.next().show();
                                };
                            };
                var rolBackFile" + idKey + @" = function (obj, record) {
                    var hdfFilesRemove" + idKey + @" = Ext.getCmp('" + key + "ListFileRemove'); " +
                    @"var deleteArray = hdfFilesRemove" + idKey + @".value.split(',');
                            if (deleteArray) {
                                var newArray = new Array();
                                for (var i = 0; i < deleteArray.length; i++) {
                                if (deleteArray[i] != record.data.Id) {
                                    newArray.push(deleteArray[i]);
                                }
                            };
                        hdfFilesRemove" + idKey + @".setValue(newArray.toString());
                        var textField = hdfFilesRemove" + idKey + @".next();
                        if (hdfFilesRemove" + idKey + @".value) {
                            var textFileValue = 'Đã chọn: ' + App." + idKey + @"dymanicStoreFile.count() + ' tệp đính kèm (Xóa: ' + newArray.length + ')';
                            textField.setValue(textFileValue);
                            }else{
                            var textFileValueNotRemove = 'Đã chọn: ' + App." + idKey + @"dymanicStoreFile.count() + ' tệp đính kèm';
                            textField.setValue(textFileValueNotRemove);
                            }
                        }
                        record.set({ IsDelete: false });
                        obj.hide();
                        obj.prev().show();
                    };
            var refreshMenuFile" + idKey + @" = function (grid) {
                    var records = grid.selModel.getSelection();
                    switch (records.length) {
                        case 0:
                            grid.queryById('btnView').setDisabled(true);
                            grid.queryById('btnDownload').setDisabled(true);
                            grid.queryById('btnDelete').setDisabled(true);
                            grid.queryById('btnRestore').setDisabled(true);
                            break;
                        case 1:
                           if(records[0].data.IsDelete){
                            grid.queryById('btnView').setDisabled(true);
                            grid.queryById('btnDownload').setDisabled(true);
                            grid.queryById('btnDelete').hide();
                            grid.queryById('btnRestore').show();
                            }else{
                                if(records[0].data.IsNew){
                                    grid.queryById('btnView').setDisabled(true);
                                    grid.queryById('btnDownload').setDisabled(true);
                                    grid.queryById('btnDelete').show();
                                    grid.queryById('btnDelete').setDisabled(true);
                                    grid.queryById('btnRestore').hide();
                                }else{
                                    grid.queryById('btnView').setDisabled(false);
                                    grid.queryById('btnDownload').setDisabled(false);
                                    grid.queryById('btnDelete').setDisabled(false);
                                    grid.queryById('btnDelete').show();
                                    grid.queryById('btnRestore').hide();
                                }
                            }
                            break;
                        default:
                            grid.queryById('btnView').setDisabled(true);
                            grid.queryById('btnDownload').setDisabled(true);
                            grid.queryById('btnDelete').setDisabled(true);
                            grid.queryById('btnRestore').hide();
                            break;
                    }
                };
            </script>";
            var container = X.FieldSet().Layout(LayoutType.Fit).Border(false).Padding(0).MarginSpec("0 0 5 0");
            var container1 = X.Container().ID(key + "ctFileDisplay").Flex(1).Layout(LayoutType.HBox).LayoutConfig(new HBoxLayoutConfig { Align = HBoxAlign.Top }).Padding(0).AutoScroll(true).Margin(0).Defaults(X.Parameter().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value));
            var hdfReadOnly = X.Hidden().ID(key + "ReadOnly").Name(key + "ReadOnly").Value(false);
            var hdfFileRemove = X.Hidden().ID(key + "ListFileRemove").Name(key + "ListFileRemove");
            var fileCount = value != null ? (value.Files == null || value.Files.Count() == 0 ? string.Empty : "Đã chọn: " + value.Files.Count() + " tệp đính kèm") : string.Empty;
            var textFieldDisplay = X.TextField().ReadOnly(true).Value(fileCount).Flex(2).ID(idKey + "txtDisplayFile").HideLabel(true).EmptyText("Chọn tệp đính kèm ...").MarginSpec("0 -1 0 0");
            var fileUploadFieldAdd = X.FileUploadField().Name(key + "FileAttachments").ID(idKey + "FileUpload").EmptyText("Chọn tệp đính kèm").ButtonText("").MarginSpec("0 -3 0 -2").BorderSpec("1 1 1 0").ButtonOnly(true).Height(26).Width(30).IconCls("x-fa fa-paperclip fa-margin-top-3 fa-margin-bottom-3").HtmlBin(c => script)
                .Listeners(l =>
                {
                    l.AfterRender.Handler = "this.fileInputEl.set({ accept: '*' }); this.fileInputEl.set({ multiple: true });";
                    l.Change.Handler = "addFile" + idKey + @"(this); this.hide(); if(this.fileInputEl.dom.files.length>0){Ext.getCmp('" + idKey + "Reset').show();}else{Ext.getCmp('" + idKey + "Reset').hide();};  Ext.getCmp('" + idKey + "txtDisplayFile').setValue('Đã thêm: '+ this.fileInputEl.dom.files.length+' tệp đính kèm');";
                });
            var buttonReset = X.Button().IconCls("x-fa fa-remove").ID(idKey + "Reset").Hidden(true).MarginSpec("0 -1 0 0").ToolTip("Hủy tệp đã chọn")
                .OnClientClick("Ext.getCmp('" + idKey + "FileUpload').fileInputEl.setValue(null); Ext.getCmp('" + idKey + "FileUpload').show(); Ext.getCmp('" + idKey + "txtDisplayFile').setValue(null); App." + idKey + @"dymanicStoreFile.reload(); this.hide();");
            var buttonViewFiles = X.Button()
                           .MenuAlign("tr-br")
                           .ToolTip("Xem danh sách tệp đính kèm")
                           .IconCls("x-fa fa-navicon")
                           .Width(30)
                           .Height(32)
                           .ItemID(idKey)
                           .ArrowVisible(false)
                           .Menu(
                               X.Menu()
                                .MinHeight(70)
                                .MaxHeight(250)
                                .Width(350).Layout(LayoutType.Fit)
                                 .ItemID(idKey)
                                .ShowSeparator(false)
                                .Resizable(false)
                                .Listeners(ls =>
                                {
                                    ls.BeforeShow.Handler = "var w = Ext.getCmp('" + key + "ctFileDisplay').getWidth(); this.setWidth(w);";
                                })
                                .Layout(LayoutType.Fit)
                                .Floating(true)
                                .PaddingSpec("1 0 0 0")
                                .BodyBorder(0)
                                .Border(false)
                                .StyleSpec("border:none !important;")
                                .Items(
                                     X.GridPanel()
                                      .ItemID(idKey).AutoScroll(true)
                                             .ColumnLines(true)
                                             .Listeners(ls =>
                                             {
                                                 ls.BeforeShow.Handler = "var w = Ext.getCmp('" + key + "ctFileDisplay').getWidth(); this.setWidth(w);";
                                             })
                                             .Flex(1)
                                             .Header(false)
                                             .TopBarItem(
                                                 X.Button().IconCls("x-fa fa-search").ItemID("btnView").Disabled(true).Text("Xem").Handler("viewFile(this.up('grid').selModel.getSelection()[0].data.Id)").StyleSpec("top:0px !important"),
                                                 X.Button().IconCls("x-fa fa-download").ItemID("btnDownload").Disabled(true).Text("Tải tệp").Handler("downloadFile(this.up('grid').selModel.getSelection()[0].data.Id)").StyleSpec("top:0px !important"),
                                                 X.Button().IconCls("x-fa fa-trash-o").Disabled(true).ItemID("btnDelete").Text("Xóa").Handler("deleteFile" + idKey + @"(this,this.up('grid').selModel.getSelection()[0])").StyleSpec("top:0px !important"),
                                                 X.Button().Icon(Icon.Reload).Text("Phục hồi").Hidden(true).ItemID("btnRestore").Handler("rolBackFile" + idKey + @"(this,this.up('grid').selModel.getSelection()[0])").StyleSpec("top:0px !important")
                                             )
                                             .Icon(Icon.Table)
                                             .Store(
                                             X.Store()
                                             .ID(idKey + "dymanicStoreFile")
                                             .AutoLoad(true)
                                             .Proxy(X.AjaxProxy()
                                             .Url(X.XController().UrlHelper
                                             .Action("LoadFiles", "File", new { Area = "Generic" }))
                                             .Reader(X.JsonReader().RootProperty("data")))
                                             .Parameters(i =>
                                             {
                                                 i.Add(new StoreParameter("fileIds", value != null ? value.Files : new List<Guid>()));
                                             })
                                             .RemotePaging(false)
                                             .Model(X.Model()
                                             .IDProperty("Id")
                                             .Fields(
                                                     X.ModelField().Name("Id").Type(ModelFieldType.Auto),
                                                     X.ModelField().Name("Name").Type(ModelFieldType.String),
                                                     X.ModelField().Name("Size").Type(ModelFieldType.Float),
                                                     X.ModelField().Name("Extension").Type(ModelFieldType.String),
                                                     X.ModelField().Name("IsDelete").Type(ModelFieldType.Boolean),
                                                     X.ModelField().Name("IsNew").Type(ModelFieldType.Boolean)
                                                    )
                                                 )
                                             )
                                             .ColumnModel(
                                                     X.RowNumbererColumn().Text("STT").Width(40).Align(ColumnAlign.Center),
                                                     X.Column().DataIndex("Extension").Width(35).Renderer("TypeRenderer").Align(ColumnAlign.Center),
                                                     X.Column().DataIndex("Name").Text("Tên tệp").Flex(1),
                                                     X.Column().DataIndex("Size").Text("Kích cỡ").Width(90).Renderer(RendererFormat.FileSize),
                                                     X.Column().DataIndex("IsDelete").Width(32).Renderer("return (record.data.IsNew?'<span title=\"Tệp chưa được lưu\" style=\"color:red\" class=\"x-fa fa-plus\"></span>':(value?'<span title=\"Đã xóa\" style=\"color:red\" class=\"x-fa fa-ban\"></span>':''))")
                                                 )
                                                 .SelectionModel(X.RowSelectionModel().Mode(SelectionMode.Single))
                                                 .Header(false)
                                                 .HideHeaders(true)
                                                 .Listeners(ls =>
                                                 {
                                                     ls.SelectionChange.Handler = "refreshMenuFile" + idKey + @"(this)";
                                                     ls.RowDblClick.Handler = "viewFile(record.data.Id)";
                                                 })
                                                 .View(Html.X().GridView())
                                            )
                                        );
            container1.Add(hdfReadOnly);
            container1.Add(hdfFileRemove);
            container1.Add(textFieldDisplay);
            container1.Add(buttonReset);
            container1.Add(fileUploadFieldAdd);
            container1.Add(buttonViewFiles);
            container.Add(container1);
            return container;
        }

        #region TuNT FileUploadFieldFor Maintain
        public static FieldSet.Builder FileUploadFieldLimitFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, long sizeLimit = 25)
          where TProperty : FileUploadBO
        {
            var key = X.GetKey(expression);
            var hiddenKeyID = X.Hidden().ID("hidden" + key + "KeyID");
            var idKey = key.Replace(".", string.Empty) + X.GetComponentID();
            hiddenKeyID.Value(idKey);
            var value = X.GetValue(expression);
            var script = @"<script> 
                    var addFile" + idKey + @" = function (fb) {   
                            debugger;
                            for (var i = 0; i < fb.fileInputEl.dom.files.length; i++) { 
                                var f = fb.fileInputEl.dom.files[i], filename = f.name, extension = f.name.split('.').pop(), size = f.size;  
                                if (Ext.StoreMgr.lookup(App." + idKey + @"dymanicStoreFile).findExact('Name', filename) < 0) {
                                    debugger;
                                    App." + idKey + @"dymanicStoreFile.add({Name: filename, Extension: extension,  Size: size, IsNew: true });
                                }
                            }                          
                        };
                    var deleteFile" + idKey + @" = function (obj, record) {
                        var hdfFilesRemove" + idKey + @" = Ext.getCmp('" + key + "ListFileRemove'); " +
                        @"if(hdfFilesRemove" + idKey + @".value) 
                            {
                                var deleteArray = hdfFilesRemove" + idKey + @".value.split(',');
                                var exits = false; for (var i = 0; i < deleteArray.length; i++) {
                                    if (deleteArray[i] == record.data.Id) { exits = true; };
                                };
                                if (!exits) { deleteArray.push(record.data.Id); };
                                hdfFilesRemove" + idKey + @".setValue(deleteArray.toString());
                                }
                                else {
                                    hdfFilesRemove" + idKey + @".setValue(record.data.Id);
                                }
                                var textField = hdfFilesRemove" + idKey + @".next();
                                if (hdfFilesRemove" + idKey + @".value) {
                                    var textFileValue = 'Đã chọn: ' + App." + idKey + @"dymanicStoreFile.count() + ' tệp đính kèm (Xóa: ' + hdfFilesRemove" + idKey + @".value.split(',').length + ')';
                                    textField.setValue(textFileValue);
                                    record.set({ IsDelete: true });
                                    obj.hide();
                                    obj.next().show();
                                };
                            };
                var rolBackFile" + idKey + @" = function (obj, record) {
                    var hdfFilesRemove" + idKey + @" = Ext.getCmp('" + key + "ListFileRemove'); " +
                    @"var deleteArray = hdfFilesRemove" + idKey + @".value.split(',');
                            if (deleteArray) {
                                var newArray = new Array();
                                for (var i = 0; i < deleteArray.length; i++) {
                                if (deleteArray[i] != record.data.Id) {
                                    newArray.push(deleteArray[i]);
                                }
                            };
                        hdfFilesRemove" + idKey + @".setValue(newArray.toString());
                        var textField = hdfFilesRemove" + idKey + @".next();
                        if (hdfFilesRemove" + idKey + @".value) {
                            var textFileValue = 'Đã chọn: ' + App." + idKey + @"dymanicStoreFile.count() + ' tệp đính kèm (Xóa: ' + newArray.length + ')';
                            textField.setValue(textFileValue);
                            }else{
                            var textFileValueNotRemove = 'Đã chọn: ' + App." + idKey + @"dymanicStoreFile.count() + ' tệp đính kèm';
                            textField.setValue(textFileValueNotRemove);
                            }
                        }
                        record.set({ IsDelete: false });
                        obj.hide();
                        obj.prev().show();
                    };
            var refreshMenuFile" + idKey + @" = function (grid) {
                    var records = grid.selModel.getSelection();
                    switch (records.length) {
                        case 0:
                            grid.queryById('btnView').setDisabled(true);
                            grid.queryById('btnDownload').setDisabled(true);
                            grid.queryById('btnDelete').setDisabled(true);
                            grid.queryById('btnRestore').setDisabled(true);
                            break;
                        case 1:
                           if(records[0].data.IsDelete){
                            grid.queryById('btnView').setDisabled(true);
                            grid.queryById('btnDownload').setDisabled(true);
                            grid.queryById('btnDelete').hide();
                            grid.queryById('btnRestore').show();
                            }else{
                                if(records[0].data.IsNew){
                                    grid.queryById('btnView').setDisabled(true);
                                    grid.queryById('btnDownload').setDisabled(true);
                                    grid.queryById('btnDelete').show();
                                    grid.queryById('btnDelete').setDisabled(true);
                                    grid.queryById('btnRestore').hide();
                                }else{
                                    grid.queryById('btnView').setDisabled(false);
                                    grid.queryById('btnDownload').setDisabled(false);
                                    grid.queryById('btnDelete').setDisabled(false);
                                    grid.queryById('btnDelete').show();
                                    grid.queryById('btnRestore').hide();
                                }
                            }
                            break;
                        default:
                            grid.queryById('btnView').setDisabled(true);
                            grid.queryById('btnDownload').setDisabled(true);
                            grid.queryById('btnDelete').setDisabled(true);
                            grid.queryById('btnRestore').hide();
                            break;
                    }
                };
            </script>";
            var container = X.FieldSet().Layout(LayoutType.Fit).Border(false).Padding(0).MarginSpec("0 0 5 0");
            var container1 = X.Container().ID(key + "ctFileDisplay").Flex(1).Layout(LayoutType.HBox).Padding(0).AutoScroll(true).Margin(0).Defaults(X.Parameter().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value)).LayoutConfig(new HBoxLayoutConfig { Align = HBoxAlign.Stretch });
            var hdfReadOnly = X.Hidden().ID(key + "ReadOnly").Name(key + "ReadOnly").Value(false);
            var hdfFileRemove = X.Hidden().ID(key + "ListFileRemove").Name(key + "ListFileRemove");
            var fileCount = value != null ? (value.Files == null || value.Files.Count() == 0 ? string.Empty : "Đã chọn: " + value.Files.Count() + " tệp đính kèm") : string.Empty;
            var textFieldDisplay = X.TextField().ReadOnly(true).Value(fileCount).Flex(2).ID(idKey + "txtDisplayFile").HideLabel(true).EmptyText("Chọn tệp đính kèm ...").MarginSpec("0 -1 0 0");
            var fileUploadFieldAdd = X.FileUploadField().Name(key + "FileAttachments").ID(idKey + "FileUpload").EmptyText("Chọn tệp đính kèm").ButtonText("").MarginSpec("0 -3 0 -2").BorderSpec("1 1 1 0").ButtonOnly(true).Height(26).Width(30).IconCls("x-fa fa-paperclip fa-margin-top-3 fa-margin-bottom-3").HtmlBin(c => script)
                .Listeners(l =>
                {
                    l.AfterRender.Handler = "this.fileInputEl.set({ accept: '*' }); this.fileInputEl.set({ multiple: true });";
                    l.Change.Handler = "addFile" + idKey + @"(this); this.hide(); if(this.fileInputEl.dom.files.length>0){Ext.getCmp('" + idKey + "Reset').show();}else{Ext.getCmp('" + idKey + "Reset').hide();};  Ext.getCmp('" + idKey + "txtDisplayFile').setValue('Đã thêm: '+ this.fileInputEl.dom.files.length+' tệp đính kèm');";
                });
            var buttonReset = X.Button().IconCls("x-fa fa-remove").ID(idKey + "Reset").Hidden(true).MarginSpec("0 -1 0 0").ToolTip("Hủy tệp đã chọn")
                .OnClientClick("Ext.getCmp('" + idKey + "FileUpload').fileInputEl.setValue(null); Ext.getCmp('" + idKey + "FileUpload').show(); Ext.getCmp('" + idKey + "txtDisplayFile').setValue(null); App." + idKey + @"dymanicStoreFile.reload(); this.hide();");
            var btnAddFileScript = string.Format("debugger; App.{0}FileUpload.show()", idKey);//string.Format("addFile{0}(App.{0}FileUpload)", idKey);
            var buttonViewFiles = X.Button()
                           .MenuAlign("tr-br")
                           .ToolTip("Xem danh sách tệp đính kèm")
                           .IconCls("x-fa fa-navicon")
                           .Width(30)
                           .ItemID(idKey)
                           .ArrowVisible(false)
                           .Menu(
                               X.Menu()
                                .Height(250)
                                .Width(350)
                                 .ItemID(idKey)
                                .ShowSeparator(false)
                                .Resizable(false)
                                .Constrain(true)
                                .Listeners(ls =>
                                {
                                    ls.BeforeShow.Handler = "var w = Ext.getCmp('" + key + "ctFileDisplay').getWidth(); this.setWidth(w);";
                                })
                                .Layout(LayoutType.Fit)
                                .Floating(true)
                                .PaddingSpec("1 0 0 0")
                                .BodyBorder(0)
                                .Border(false)
                                .StyleSpec("border:none !important;")
                                .Items(
                                     X.GridPanel()
                                      .ItemID(idKey)
                                             .ColumnLines(true)
                                             .Listeners(ls =>
                                             {
                                                 ls.BeforeShow.Handler = "var w = Ext.getCmp('" + key + "ctFileDisplay').getWidth(); this.setWidth(w);";
                                             })
                                             .Flex(1)
                                             .Header(false)
                                             .TopBarItem(
                                                X.Button().IconCls("x-fa fa-paperclip").Text("Thêm file").OnClientClick(btnAddFileScript).StyleSpec("top:0px !important"),
                                                 X.Button().IconCls("x-fa fa-search").ItemID("btnView").Disabled(true).Text("Xem").Handler("viewFile(this.up('grid').selModel.getSelection()[0].data.Id)").StyleSpec("top:0px !important"),
                                                 X.Button().IconCls("x-fa fa-download").ItemID("btnDownload").Disabled(true).Text("Tải tệp").Handler("downloadFile(this.up('grid').selModel.getSelection()[0].data.Id)").StyleSpec("top:0px !important"),
                                                 X.Button().IconCls("x-fa fa-trash-o").Disabled(true).ItemID("btnDelete").Text("Xóa").Handler("deleteFile" + idKey + @"(this,this.up('grid').selModel.getSelection()[0])").StyleSpec("top:0px !important"),
                                                 X.Button().Icon(Icon.Reload).Text("Phục hồi").Hidden(true).ItemID("btnRestore").Handler("rolBackFile" + idKey + @"(this,this.up('grid').selModel.getSelection()[0])").StyleSpec("top:0px !important")
                                             )
                                             .Icon(Icon.Table)
                                             .Store(
                                             X.Store()
                                             .ID(idKey + "dymanicStoreFile")
                                             .AutoLoad(true)
                                             .Proxy(X.AjaxProxy()
                                             .Url(X.XController().UrlHelper
                                             .Action("LoadFiles", "File", new { Area = "Generic" }))
                                             .Reader(X.JsonReader().RootProperty("data")))
                                             .Parameters(i =>
                                             {
                                                 i.Add(new StoreParameter("fileIds", value != null ? value.Files : new List<Guid>()));
                                             })
                                             .RemotePaging(false)
                                             .Model(X.Model()
                                             .IDProperty("Id")
                                             .Fields(
                                                     X.ModelField().Name("Id").Type(ModelFieldType.Auto),
                                                     X.ModelField().Name("Name").Type(ModelFieldType.String),
                                                     X.ModelField().Name("Size").Type(ModelFieldType.Float),
                                                     X.ModelField().Name("Extension").Type(ModelFieldType.String),
                                                     X.ModelField().Name("IsDelete").Type(ModelFieldType.Boolean),
                                                     X.ModelField().Name("IsNew").Type(ModelFieldType.Boolean)
                                                    )
                                                 )
                                             )
                                             .ColumnModel(
                                                     X.RowNumbererColumn().Text("STT").Width(40).Align(ColumnAlign.Center),
                                                     X.Column().DataIndex("Extension").Width(35).Renderer("TypeRenderer").Align(ColumnAlign.Center),
                                                     X.Column().DataIndex("Name").Text("Tên tệp").Flex(1),
                                                     X.Column().DataIndex("Size").Text("Kích cỡ").Width(90).Renderer(RendererFormat.FileSize),
                                                     X.Column().DataIndex("IsDelete").Width(32).Renderer("return (record.data.IsNew?'<span title=\"Tệp chưa được lưu\" style=\"color:red\" class=\"x-fa fa-plus\"></span>':(value?'<span title=\"Đã xóa\" style=\"color:red\" class=\"x-fa fa-ban\"></span>':''))")
                                                 )
                                                 .SelectionModel(X.RowSelectionModel().Mode(SelectionMode.Single))
                                                 .Header(false)
                                                 .HideHeaders(true)
                                                 .Listeners(ls =>
                                                 {
                                                     ls.SelectionChange.Handler = "refreshMenuFile" + idKey + @"(this)";
                                                     ls.RowDblClick.Handler = "viewFile(record.data.Id)";
                                                 })
                                                 .View(Html.X().GridView())
                                            )
                                        );

            container1.Add(hdfReadOnly);
            container1.Add(hdfFileRemove);
            container1.Add(textFieldDisplay);
            container1.Add(buttonReset);
            container1.Add(fileUploadFieldAdd);
            container1.Add(buttonViewFiles);
            container1.Add(hiddenKeyID);
            container.Add(container1);

            return container;
        }
        #endregion

        #endregion
        public static FieldSet.Builder SecurityFieldFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression) where TProperty : DocumentSecurityBO
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var container = X.FieldSet().Layout(LayoutType.Fit).Border(false).Padding(0).MarginSpec("0 0 5 0");
            var container1 = X.Container().ID(key + "cbbSecurity").Flex(1).Layout(LayoutType.HBox).Padding(0).AutoScroll(true).Margin(0).Defaults(X.Parameter().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value)).LayoutConfig(new HBoxLayoutConfig { Align = HBoxAlign.Stretch });
            var gridSecurity = X.GridPanel().Flex(1)
                                .ID(key + "_Security")
                                .UI(UI.Success)
                                .TopBarItem(
                                    X.Button().Text(Resource.Create).ToolTip(Resource.Create).IconCls("x-fa fa-plus-circle")
                                    )
                                .Header(false).Border(false).ColumnLines(true).Icon(Icon.Table)
                                .Store(X.Store().ID(key + "_Security").PageSize(10).Proxy(X.AjaxProxy().Url(X.XController().UrlHelper.Action("GetData", "Security", new { Area = "Document" })).Reader(X.JsonReader().RootProperty("data")))
                                .Model(X.Model()
                                    .Fields(
                                        X.ModelField().Name("Id").Type(ModelFieldType.Auto),
                                        X.ModelField().Name("Name").Type(ModelFieldType.String),
                                        X.ModelField().Name("Color").Type(ModelFieldType.String),
                                        X.ModelField().Name("Description").Type(ModelFieldType.String)
                                    )
                                )
                            )
                        .ColumnModel(
                            X.RowNumbererColumn().Text("Stt").Width(40).Align(ColumnAlign.Center),
                            X.Column().Text("Mức độ").DataIndex("Name").Flex(1).Renderer(new Renderer { Fn = "function(value, meta, record) {return '<span><i style=\"background-color: '+ record.data.Color +'; width: 15px; height: 15px; border-radius: 50%; position: absolute;border: 1px solid\"></i><span style=\"margin-left: 24px;\">'+ Ext.htmlEncode(value) +'</span></span>'; }" }),
                            X.ColumnEncodeFormat().Text(Resource.Description).DataIndex("Description").Flex(2)
                        )
                        .SelectionModel(
                                        X.RowSelectionModel().Mode(SelectionMode.Single)
                                        )
                        .BottomBar(X.PagingToolbarFormat());
            container.Add(container1);
            return container;
        }
        #region Tài liệu
        //        public static TextField.Builder TextFieldDocumentFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression)
        //        {
        //            var script = @"<script>
        //                                        var selectDepartment = function(records) {
        //                                            if (records != null && records.length > 0){
        //                                                var data = '';
        //                                                records.forEach(function (obj) {
        //                                                    var item = obj.data.id + ',';
        //                                                    data = data + item;
        //                                                });
        //                                                var departmentContainer = App." + key.Replace(".", "") + @"Container;
        //                                                var text = records[0].data.text;    
        //                                                if(records.length > 1){
        //                                                    text = 'Đã chọn: '+ records.length +' Phòng ban';
        //                                                };        
        //                                                departmentContainer.queryById('hdfDepartmentID').setValue(data);
        //                                                departmentContainer.queryById('txtDepartmentName').setValue(text);
        //                                            };
        //                                        };
        //                                    </script>";
        //            var button = X.Button().Icon(Icon.FolderHome).ToolTip("Lựa chọn phòng ban");
        //            var textfield = X.TextFieldFor(expression).RightButtons(Html.X().Button().IconCls("x-fa fa-pencil").Handler(""));
        //            return textfield;
        //        }

        /// <summary>
        /// Control chọn tài liệu từ những tài liệu đã ban hành
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="X"></param>
        /// <param name="expression"></param>
        /// <param name="isDestroy">True: Dùng cho form tạo yêu cầu hủy, False: mặc định</param>
        /// <param name="action">action phải return về danh sách DocumentBO</param>
        /// <param name="controller">Tên controller chứa action</param>
        /// <param name="areaName">controller thuộc area nào</param>
        /// <param name="param">Cấu trúc param theo mẫu: "param1:value1;param2:value2;param3:value3;..."</param>
        /// <returns></returns>
        public static Container.Builder TextFieldDocumentFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression
            , bool isDestroy = false, string action = "", string controller = "", string areaName = "", string param = "")
            where TProperty : DocumentBO
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var name = key + "Name";
            var id = key + "Id";
            var script = @"<script>
                                var selectDocumentContainer;
                                var onSelectDocumentInDocumentControl = function(records) {
                                    if (records != null && records.length > 0){
                                        var documentName = records[0].data.Name;
                                        var documentId = records[0].data.Id;
                                        var documentCode = records[0].data.Code;
                                        var adjustNote = records[0].data.AdjustNote;
                                        var adjustStartAt = records[0].data.AdjustStartAt;
                                        var adjustEndAt = records[0].data.AdjustEndAt;
                                        var adjustRequestId = records[0].data.RequestId;       
                                        selectDocumentContainer.queryById('hdfDocumentID').setValue(documentId);
                                        selectDocumentContainer.queryById('validDocument').setValue(documentName);
                                        selectDocumentContainer.queryById('hdfDocumentCode').setValue(documentCode);
                                        var ctAdjustNote = Ext.getCmp('txtAdjustNewNote');
                                        if(ctAdjustNote != null && ctAdjustNote != undefined){
                                            ctAdjustNote.setValue(adjustNote);
                                        }
                                        var ctAdjustStartAt = Ext.getCmp('dfAdjustStartAt');
                                        if(ctAdjustStartAt != null && ctAdjustStartAt != undefined){
                                            ctAdjustStartAt.setValue(Ext.Date.format(adjustStartAt,'d/m/Y'));
                                        }
                                        var ctAdjustEndAt = Ext.getCmp('dfAdjustEndAt');
                                        if(ctAdjustEndAt != null && ctAdjustEndAt != undefined){
                                            ctAdjustEndAt.setValue(Ext.Date.format(adjustEndAt,'d/m/Y'));
                                        }
                                        var ctAdjustRequestId = Ext.getCmp('hdfAdjustRequestId');
                                        if(ctAdjustRequestId != null && ctAdjustRequestId != undefined){
                                            ctAdjustRequestId.setValue(adjustRequestId);
                                        }
                                    };
                                };
                                var resetDocument = function(){
                                    selectDocumentContainer.queryById('validDocument').reset();
                                    selectDocumentContainer.queryById('hdfDocumentID').reset();
                                    selectDocumentContainer.queryById('hdfDocumentCode').reset();
                                };
                            </script>";
            var textfield = X.TextFieldFor(name).ID("txtcontrolDocumentName").RightButtons(Html.X().Button().IconCls("x-fa fa-pencil").ToolTip("Lựa chọn tài liệu")
                            .RequestUrl(X.XController().UrlHelper.Action("DocumentSelect", "List", new
                            {
                                Area = "Document",
                                isDestroy = isDestroy,
                                actionName = action,
                                controllerName = controller,
                                areaName = areaName,
                                param = param
                            })))
                            .ReadOnly(true).AllowBlank(false).BlankText("Chưa chọn tài liệu!").ItemID("validDocument").HideLabel(true)
                            .Note("Tên tài liệu").NoteAlign(NoteAlign.Top).EmptyText("Lựa chọn tài liệu");
            if (value != null && value.Id != Guid.Empty)
            {
                textfield.Value(value.Id.ToString());
            }
            else
            {
                textfield.Value("Lựa chọn tài liệu");
            }
            var component = X.ContainerVBox()
                            .Listeners(ls => ls.BoxReady.Handler = "selectDocumentContainer = this")
                            .HtmlBin(c => script)
                            .Items(
                                textfield,
                                X.HiddenFor(id).ID("hdfcontrolDocumentId").ItemID("hdfDocumentID"),
                                X.HiddenFor(key + "Code").ID("hdfcontrolDocumentCode").ItemID("hdfDocumentCode")
                            );
            return component;
        }

        public static Button.Builder ButtonSelectDocument(this BuilderFactory X, string customerHandler = "")
        {
            var script = !string.IsNullOrEmpty(customerHandler) ? customerHandler :
                            @" var grid = this.up('window').down('grid');
                            var records = grid.selModel.getSelection();
                            onSelectDocumentInDocumentControl(records);
                            this.up('window').close();";
            var component = X.Button().IconCls("x-fa fa-check-circle").Text("Chọn").Handler(script);
            return component;
        }

        #endregion
        #region InmageUpload
        public static FieldSet.Builder ImageUploadFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression)
           where TProperty : FileBO
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var fieldset = X.FieldSet()
            .Title(Resource.Avatar)
            .BorderSpec("1")
            .Margin(0).Padding(1)
            .Width(120).Height(152)
            .StyleSpec("border-color: #d1d1d1")
            .LayoutConfig(new VBoxLayoutConfig { Align = VBoxAlign.Stretch });
            var imageId = "img" + Guid.NewGuid().ToString().Replace("-", "");
            var imageButton = X.Image().ID(imageId).Flex(1).ImageUrl(value != null ? value.Url : "/Content/images/underfind.jpg").StyleSpec("border: 2px solid #b5b8c8;").MarginSpec("0 7 5 7");
            var loadImgHandle = "var file = this.fileInputEl.dom.files[0];"
                                + "var url = URL.createObjectURL(file);"
                                + "App." + imageId + ".setImageUrl(url);";
            var imageUpload = X.FileUploadField()
                                .Name(key + "File")
                                .Margin(1).Padding(0)
                                .ReadOnly(true).ButtonOnly(true)
                                .ButtonText(Resource.BrowseImage)
                                .ButtonMargin(0)
                                .IconCls("x-fa fa-picture-o")
                                .Listeners(ls => ls.AfterRender.Fn = "function(cmp){cmp.fileInputEl.set({ accept:'image/*' });}")
                                .Listeners(ls => ls.Change.Handler = loadImgHandle);
            fieldset.Add(imageButton);
            fieldset.Add(imageUpload);
            return fieldset;
        }
        #endregion
        public static ComboBox.Builder ComboBoxFormatFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null)
        {
            var component = X.ComboBoxFor(expression, setId, convert, format)
                            .Margin(0).HideLabel(true)
                            .DisplayField("Name").ValueField("Id")
                            .Validator(i => i.Handler = "return this.value != this.rawValue;")
                            .ValidatorText("Giá trị đã nhập không phù hợp!");
            return component;
        }
        public static Store.Builder StoreComboBox(this BuilderFactory X)
        {
            var component = X.Store()
            .AutoLoad(true).RemoteSort(true).RemotePaging(true)
            .Model(
                X.Model().Fields(
                    new ModelField("Id", ModelFieldType.Auto),
                    new ModelField("Name", ModelFieldType.String)
                )
            );
            return component;
        }
        public static Store.Builder StoreFormat(this BuilderFactory X)
        {
            var component = X.Store()
            .AutoLoad(true)
            .PageSize(10);
            return component;
        }
        public static Store.Builder ModelType(this Store.Builder Store, Type type)
        {
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            Store.Model(
                Ext.Net.Html.X().Model().IDProperty("Id")
                .Fields(i =>
                {
                    foreach (var item in properties)
                    {
                        var modelType = ModelFieldType.String;
                        if (item.PropertyType.IsAssignableFrom(typeof(DateTime)))
                            modelType = ModelFieldType.Date;
                        if (item.PropertyType.IsAssignableFrom(typeof(Guid)))
                            modelType = ModelFieldType.Auto;
                        if (item.PropertyType.IsAssignableFrom(typeof(Boolean)))
                            modelType = ModelFieldType.Boolean;
                        if (item.PropertyType.IsClass)
                            modelType = ModelFieldType.Object;
                        i.Add(new ModelField(item.Name, modelType));
                    }
                })
            );
            return Store;
        }
        public static Store.Builder Url(this Store.Builder Store, string url)
        {
            Store.Proxy(Html.X().AjaxProxy().Url(url).Reader(Html.X().JsonReader().RootProperty("data")));
            return Store;
        }
        public static ImageButton.Builder RequestUrl(this ImageButton.Builder Button, string url)
        {
            var script = @"var btn = this; btn.setDisabled(true); 
                           var params = {}; 
                           Ext.net.DirectMethod.request({
                               method: 'post',
                               params: params,
                               url: '[URL]',
                               success: function (result) {
                                   btn.setDisabled(false);
                               },
                               failure: function (result) {
                                   btn.setDisabled(false);
                               },
                         });";
            Button.Listeners(ls => ls.Click.Handler = script.Replace("[URL]", url));
            return Button;
        }
        public static Container.Builder EmployeeField(this BuilderFactory X, EmployeeBO employee, string handle = "")
        {
            var script = @"<script>
                            var employeeContainer;
                            var selectEmployee = function(records) { 
                                var record = records[0];
                                employeeContainer.queryById('EmployeeID').setValue(record.get('Id'));
                                employeeContainer.queryById('EmployeeAvatarUrl').setImageUrl(record.get('AvatarUrl'));
                                employeeContainer.queryById('EmployeeName').setText(record.get('Name'));
                            };
                        </script>";

            var component = X.Container()
                            .Listeners(ls => ls.BoxReady.Handler = "employeeContainer = this")
                            .HtmlBin(c => script)
                            .Items(
                            X.Hidden().ItemID("EmployeeID").Value(employee != null ? employee.Id : Guid.NewGuid())
                                .Listeners(ls => ls.Change.Handler = handle),
                                X.Label().StyleSpec("font-weight:bold").Text(employee != null ? employee.Name : string.Empty).ItemID("EmployeeName").MarginSpec("2 5 0 0"),
                                X.ImageButton().ItemID("EmployeeAvatarUrl").Height(23).Width(23).ImageUrl(employee != null ? employee.AvatarUrl : string.Empty).Cls("img-circle")
                                .RequestUrl(X.XController().UrlHelper.Action("Index", "Employee", new { Area = "Generic" }))
                            );
            return component;
        }
        /// <summary>
        /// Control sử dụng để tự động tìm ra trách nhiệm tiếp theo trong quy trình kiểm soát tài liệu thuộc phòng ban
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="X"></param>
        /// <param name="expression"></param>
        /// <param name="typeProcess">Quy trình cần tìm trách nhiệm tiếp theo</param>
        /// <param name="departmentId">Id phòng ban: Ví dụ: 'xxx-xxx-xxx-xxx-xxx-xxx-xxx' hoặc 'App.cbDepartment.getValue()'</param>
        /// <param name="order">Thứ tự của trách nhiệm trước</param>
        /// <param name="allowBlank">Cho phép trống: Ví dụ: true|false</param>
        /// <param name="startRole">Trách nhiệm bắt đầu sẽ được bỏ qua khi chuyển trách nhiệm tiếp theo</param>
        /// <returns></returns>
        //        public static Container.Builder DocumentEmployeeFieldFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression,
        //            ResourceService.DocumentTypeProcess? typeProcess = null, string departmentId = default(string), int order = 0, bool allowBlank = false, ResourceService.DocumentProcessRole? startRole = null, bool setHidden = false)
        //            where TProperty : DocumentEmployeeBO
        //        {
        //            var key = X.GetKey(expression);
        //            var value = X.GetValue(expression);
        //            var idComboRole = X.GetComponentID(); var idComboEmployee = X.GetComponentID(); var storeId = X.GetComponentID(); var processHidden = X.GetComponentID(); var idOrder = X.GetComponentID();
        //            var script = @"<script> var employeeContainer;
        //                             var changeValueEmployee = function (value, record) {
        //                                    employeeContainer.queryById('hdfID').setValue(record.Id);
        //                                    employeeContainer.queryById('dplEmployeeTitle').setValue(record.RoleNames);
        //                                    employeeContainer.queryById('imgAvatarUrl').setImageUrl(record.AvatarUrl);
        //                                };
        //                                var resetValueEmployee = function () {
        //                                    employeeContainer.queryById('hdfID').reset();
        //                                    employeeContainer.queryById('dplEmployeeTitle').reset();
        //                                    employeeContainer.queryById('imgAvatarUrl').setImageUrl('/Content/images/underfind.jpg');
        //                                };
        //                             var reloadDocumentEmployee = function () {
        //                                    employeeContainer.queryById('cbRoleType').getStore().reload();
        //                                };
        //                            </script>";
        //            var container = X.Container().Defaults(new Parameter().ToBuilder().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value)).Layout(LayoutType.VBox).Listeners(ls => ls.BoxReady.Handler = "employeeContainer = this;").LayoutConfig(new VBoxLayoutConfig { Align = VBoxAlign.Stretch });
        //            var hiddenProcess = X.Hidden().ID(processHidden); var hiddenOrder = X.HiddenFor(key + "NextOrder").ID(idOrder); var hiddenDepartment = X.HiddenFor(key + "DepartmentId").ItemID("itemIdDepartmentId"); var hiddenId = X.HiddenFor(key + "Id").ItemID("hdfID");
        //            if (!departmentId.Contains("getValue")) hiddenDepartment.Value(departmentId);
        //            if (value != null) order = value.NextOrder - 1;
        //            var comboRole = X.ComboBoxFor(key + "RoleType")
        //                            .Note("Vai trò người nhận")
        //                            .HideLabel(true)
        //                            .ID(idComboRole)
        //                            .AllowBlank(allowBlank)
        //                            .ItemID("cbRoleType")
        //                            .Hidden(setHidden)
        //                            .NoteAlign(NoteAlign.Top)
        //                            .MarginSpec("0 0 5 0")
        //                            .Editable(false)
        //                            .BlankText("Trường không được để trống!")
        //                            .DisplayField("Name")
        //                            .ValueField("ID")
        //                            .Listeners(ls =>
        //                            {
        //                                ls.Change.Handler = @"var record = App." + storeId + ".findRecord('ID',this.value); Ext.getCmp('" + processHidden + "').setValue(record.data.Process); Ext.getCmp('" + idOrder + "').setValue(record.data.Order); Ext.getCmp('" + idComboEmployee + "').getStore().reload();";
        //                                ls.Select.Handler = @"var comboEmployee = Ext.getCmp('" + idComboEmployee + "'); comboEmployee.reset(); comboEmployee.getStore().reload();";
        //                            })
        //                            .Store(X.Store()
        //                            .ID(storeId)
        //                            .AutoLoad(true)
        //                            .Proxy(Html.X().AjaxProxy()
        //                            .Url(X.XController().UrlHelper.Action("GetRoleType", "Setting", new { Area = "Document" }))
        //                            .Reader(Html.X().JsonReader().RootProperty("data"))
        //                            )
        //                            .Parameters(ps =>
        //                            {
        //                                ps.Add(new StoreParameter("departmentId", departmentId, departmentId.Contains("getValue") ? ParameterMode.Raw : ParameterMode.Value));
        //                                ps.Add(new StoreParameter("process", ((int)typeProcess).ToString(), ParameterMode.Value));
        //                                ps.Add(new StoreParameter("startRole", ((int?)startRole).ToString(), ParameterMode.Value));
        //                                ps.Add(new StoreParameter("order", order.ToString(), ParameterMode.Value));
        //                            })
        //                            .Listeners(ls =>
        //                            {
        //                                ls.Load.Delay = 200;
        //                                ls.Load.Handler = @"var comboRole = Ext.getCmp('" + idComboRole + "'); if(records.length > 0){comboRole.setValue(records[0].get(comboRole.valueField));}";
        //                            }));

        //            var comboboxEmployee = X.ComboBox()
        //                            .DisplayField("Name")
        //                            .ValueField("Id")
        //                            .ID(idComboEmployee)
        //                            .HideLabel(true)
        //                            .TypeAhead(false)
        //                            .PageSize(5)
        //                            .AllowBlank(allowBlank)
        //                            .EmptyText("Nhập tên nhân sự ....")
        //                            .MinChars(0)
        //                            .CheckChangeBuffer(200)
        //                            .TriggerAction(TriggerAction.Query)
        //                            .ListConfig(Html.X().BoundList()
        //                            .LoadingText("Tìm kiếm...")
        //                            .ItemTpl(Html.X().XTemplate()
        //                            .Html(@"<text>
        //                                    <span>
        //                                    <img src='{AvatarUrl}' style='margin-top: 15px; width: 30px; height: 30px; border-radius: 50%; position: absolute; border: 1px solid'>
        //                                    <span style='margin-left: 44px;'>{FullNameFormat}</span><br/>
        //                                    <span style='margin-left: 44px;font-size:10px;'>{RoleNames}</span></span>
        //                                    </span>
        //                                </text>")
        //                                )
        //                            )
        //                            .Store(Html.X().Store()
        //                            .AutoLoad(false)
        //                            .Proxy(Html.X().AjaxProxy()
        //                            .Url(X.XController()
        //                            .UrlHelper.Action("LoadProcessEmployeeQuery", "Organization", new { Area = "Organization" })
        //                            )
        //                            .ActionMethods(am => am.Read = HttpMethod.POST)
        //                            .Reader(Html.X().JsonReader().RootProperty("data"))
        //                            )
        //                            .Model(Html.X().Model()
        //                            .IDProperty("Id")
        //                            .Fields(
        //                                Html.X().ModelField().Name("Id").Type(ModelFieldType.Auto),
        //                                Html.X().ModelField().Name("AvatarUrl").Type(ModelFieldType.String),
        //                                Html.X().ModelField().Name("Name").Type(ModelFieldType.String),
        //                                Html.X().ModelField().Name("RoleNames").Type(ModelFieldType.String)
        //                            )
        //                            )
        //                            .Parameters(ps =>
        //                            {
        //                                ps.Add(new StoreParameter("departmentId", departmentId, departmentId.Contains("getValue") ? ParameterMode.Raw : ParameterMode.Value));
        //                                ps.Add(new StoreParameter("process", "Ext.getCmp('" + processHidden + "').getValue()", ParameterMode.Raw));
        //                                ps.Add(new StoreParameter("order", "Ext.getCmp('" + idOrder + "').getValue()", ParameterMode.Raw));
        //                            })
        //                            );
        //            if (value != null) comboboxEmployee.ToComponent().Text = value.Name;
        //            var component = X.ContainerHBox()
        //                    .HtmlBin(c => script)
        //                    .Items(
        //                        X.ImageButton()
        //                        .ItemID("imgAvatarUrl")
        //                        .ImageUrl(value != null ? value.AvatarUrl : "/Content/images/underfind.jpg")
        //                        .Height(69)
        //                        .Width(60)
        //                        .StyleSpec("border: 2px solid #b5b8c8;")
        //                        .Listeners(ls => ls.Click.Handler = @"var id = this.up().queryById('comboEmployeeID').getValue(); if (id == '' || id == 0) { return; }; openEmployeeDetail(id);"),
        //                        X.ContainerVBox()
        //                            .StyleSpec("border: 1px solid #b5b8c8;")
        //                            .MarginSpec("0 0 0 2")
        //                            .Padding(0)
        //                            .Flex(1)
        //                            .Items(
        //                        X.Container()
        //                            .StyleSpec("border-bottom: 1px solid #b5b8c8;")
        //                            .Height(31)
        //                            .Layout(LayoutType.Fit)
        //                            .Items(
        //                            hiddenId,
        //                            comboboxEmployee
        //                            .MarginSpec("-1 -1 0 -1")
        //                            .ItemID("comboEmployeeID")
        //                            .Listeners(ls =>
        //                            {
        //                                ls.Change.Handler = @"var comboValue = this.value;
        //                                    var record = this.store.getById(comboValue);
        //                                    if(this.store.data.length > 0 && record)
        //                                    {changeValueEmployee(comboValue,record.data)}
        //                                    else{resetValueEmployee();}";
        //                                ls.Select.Handler = @"var comboValue = this.value; 
        //                                    var record = this.store.getById(comboValue); 
        //                                    if(this.store.data.length > 0 && record)
        //                                    {changeValueEmployee(comboValue,record.data)}";
        //                            })),
        //                        X.DisplayFieldFor(key + "RoleNames")
        //                            .ItemID("dplEmployeeTitle")
        //                            .LabelStyle("font-size: 1em;font-weight: bold;")
        //                            .MarginSpec("-4 0 0 5")
        //                            .Flex(1)
        //                            .HideLabel(true)
        //                            .Listeners(ls =>
        //                            {
        //                                ls.Change.Handler = "if(this.value!=null && this.value.length > 30){this.setValue(this.value.substring(0,30) + '...');}";
        //                            })
        //                    )
        //            );
        //            if (value == null)
        //            {
        //                var c = comboboxEmployee.ToComponent();
        //                var store = c.Store.Where(i => i is Store).Cast<Store>().FirstOrDefault();
        //                store.Listeners.Load.Delay = 100;
        //                store.Listeners.Load.Handler = @"if(records.length==1){var comboEmployee = Ext.getCmp('" + idComboEmployee + "'); comboEmployee.setValue(records[0].get(comboEmployee.valueField)); comboEmployee.setReadOnly(true);}";
        //            }
        //            container.Add(hiddenDepartment);
        //            container.Add(hiddenOrder);
        //            container.Add(hiddenProcess);
        //            container.Add(comboRole);
        //            container.Add(component);
        //            return container;
        //        }
        public static Container.Builder DocumentEmployeeFieldFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression,
           ResourceService.DocumentTypeProcess? typeProcess = null, string departmentId = default(string), int order = 0, bool allowBlank = false, ResourceService.DocumentProcessRole? startRole = null, bool setHidden = false, string documentRequest = default(string))
           where TProperty : DocumentEmployeeBO
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var idComboRole = X.GetComponentID(); var idComboEmployee = X.GetComponentID(); var storeId = X.GetComponentID(); var processHidden = X.GetComponentID(); var idOrder = X.GetComponentID();
            var script = @"<script> var employeeContainer;
                                        var changeValueEmployee = function (value, record) {
                                                employeeContainer.queryById('hdfID').setValue(record.Id);
                                                employeeContainer.queryById('dplEmployeeTitle').setValue(record.RoleNames);
                                                employeeContainer.queryById('imgAvatarUrl').setImageUrl(record.AvatarUrl);
                                            };
                                            var resetValueEmployee = function () {
                                                employeeContainer.queryById('hdfID').reset();
                                                employeeContainer.queryById('dplEmployeeTitle').reset();
                                                employeeContainer.queryById('imgAvatarUrl').setImageUrl('/Content/images/underfind.jpg');
                                            };
                                         var reloadDocumentEmployee = function () {
                                                employeeContainer.queryById('cbRoleType').getStore().reload();
                                            };
                                        </script>";
            var container = X.Container().Defaults(new Parameter().ToBuilder().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value)).Layout(LayoutType.VBox).Listeners(ls => ls.BoxReady.Handler = "employeeContainer = this;").LayoutConfig(new VBoxLayoutConfig { Align = VBoxAlign.Stretch });
            var hiddenProcess = X.Hidden().ID(processHidden); var hiddenOrder = X.HiddenFor(key + "NextOrder").ID(idOrder); var hiddenDepartment = X.HiddenFor(key + "DepartmentId").ItemID("itemIdDepartmentId"); var hiddenId = X.HiddenFor(key + "Id").ItemID("hdfID");
            if (!departmentId.Contains("getValue")) hiddenDepartment.Value(departmentId);
            if (value != null) order = value.NextOrder - 1;
            #region danh sách trách nhiệm
            var comboRole = X.ComboBoxFor(key + "RoleType").Note("Vai trò người nhận").HideLabel(true).ID(idComboRole).AllowBlank(allowBlank).ItemID("cbRoleType")
                            .Hidden(setHidden).NoteAlign(NoteAlign.Top).MarginSpec("0 0 5 0").Editable(false).BlankText("Trường không được để trống!")
                            .DisplayField("Name").ValueField("ID")
                            .Listeners(ls =>
                            {
                                ls.Change.Handler = @"var record = App." + storeId + ".findRecord('ID',this.value); Ext.getCmp('" + processHidden + "').setValue(record.data.ProcessType); Ext.getCmp('" + idOrder + "').setValue(record.data.Order); Ext.getCmp('" + idComboEmployee + "').getStore().reload();";
                                ls.Select.Handler = @"var comboEmployee = Ext.getCmp('" + idComboEmployee + "'); comboEmployee.reset(); comboEmployee.getStore().reload();";
                            })
                            .Store(X.Store().ID(storeId).AutoLoad(true)
                                    .Proxy(Html.X().AjaxProxy()
                                                .Url(X.XController().UrlHelper.Action("GetNextRoleType", "Setting", new { Area = "Document" }))
                                                .Reader(Html.X().JsonReader().RootProperty("data"))
                                            )
                                    .Parameters(ps =>
                                    {
                                        ps.Add(new StoreParameter("departmentId", departmentId, departmentId.Contains("getValue") ? ParameterMode.Raw : ParameterMode.Value));
                                        ps.Add(new StoreParameter("typeProcess", ((int)typeProcess).ToString(), ParameterMode.Value));
                                        ps.Add(new StoreParameter("startRole", ((int?)startRole).ToString(), ParameterMode.Value));
                                        ps.Add(new StoreParameter("order", order.ToString(), ParameterMode.Value));
                                    })
                                    .Listeners(ls =>
                                    {
                                        ls.Load.Delay = 200;
                                        ls.Load.Handler = @"var comboRole = Ext.getCmp('" + idComboRole + "'); if(records.length > 0){comboRole.setValue(records[0].get(comboRole.valueField));}";
                                    })
                             );
            #endregion
            var comboboxEmployee = X.ComboBox()
                            .DisplayField("Name")
                            .ValueField("Id")
                            .ID(idComboEmployee)
                            .HideLabel(true)
                            .TypeAhead(false)
                            .PageSize(5)
                            .EmptyText("Nhập tên nhân sự ....")
                            .MinChars(0)
                            .CheckChangeBuffer(200)
                            .TriggerAction(TriggerAction.Query)
                            .ListConfig(Html.X().BoundList()
                            .LoadingText("Tìm kiếm...")
                            .ItemTpl(Html.X().XTemplate()
                            .Html(@"<text>
                                    <span>
                                    <img src='{AvatarUrl}' style='margin-top: 15px; width: 30px; height: 30px; border-radius: 50%; position: absolute; border: 1px solid'>
                                    <span style='margin-left: 44px;'>{FullNameFormat}</span><br/>
                                    <span style='margin-left: 44px;font-size:10px;'>{RoleNames}</span></span>
                                    </span>
                                </text>")
                                )
                            )
                            .Store(Html.X().Store()
                            .AutoLoad(false)
                            .Proxy(Html.X().AjaxProxy()
                            .Url(X.XController()
                            .UrlHelper.Action("LoadProcessEmployeeQuery", "Organization", new { Area = "Organization" })
                            )
                            .ActionMethods(am => am.Read = HttpMethod.POST)
                            .Reader(Html.X().JsonReader().RootProperty("data"))
                            )
                            .Model(Html.X().Model()
                            .IDProperty("Id")
                            .Fields(
                                Html.X().ModelField().Name("Id").Type(ModelFieldType.Auto),
                                Html.X().ModelField().Name("AvatarUrl").Type(ModelFieldType.String),
                                Html.X().ModelField().Name("Name").Type(ModelFieldType.String),
                                Html.X().ModelField().Name("RoleNames").Type(ModelFieldType.String)
                            )
                            )
                            .Parameters(ps =>
                            {
                                ps.Add(new StoreParameter("departmentId", departmentId, departmentId.Contains("getValue") ? ParameterMode.Raw : ParameterMode.Value));
                                ps.Add(new StoreParameter("process", "1", ParameterMode.Value));
                                ps.Add(new StoreParameter("startRole", "4", ParameterMode.Value));
                                ps.Add(new StoreParameter("documentrequest", documentRequest, ParameterMode.Value));
                                ps.Add(new StoreParameter("order", "Ext.getCmp('" + idOrder + "').getValue()", ParameterMode.Raw));
                                ps.Add(new StoreParameter("singlerole", "Ext.getCmp('" + idComboRole + "').getValue()", ParameterMode.Raw));
                            })
                            .Listeners(ls =>
                            {
                                ls.Load.Handler = @"var comboEmployee = Ext.getCmp('" + idComboEmployee + "');if(comboEmployee.getStore().data.items.length == 1) {comboEmployee.select(comboEmployee.getStore().getAt(0));}";
                            })
                            );
            if (value != null) comboboxEmployee.ToComponent().Text = value.Name;
            var component = X.ContainerHBox()
                    .HtmlBin(c => script)
                    .Items(
                            X.ImageButton().Height(69).Width(60).ItemID("imgAvatarUrl")
                            .ImageUrl(value != null ? value.AvatarUrl : "/Content/images/underfind.jpg")
                            .StyleSpec("border: 2px solid #b5b8c8;")
                            .Listeners(ls => ls.Click.Handler = @"var id = this.up().queryById('comboEmployeeID').getValue(); if (id == '' || id == 0) { return; }; openEmployeeDetail(id);"),
                            X.ContainerVBox().StyleSpec("border: 1px solid #b5b8c8;").MarginSpec("0 0 0 2").Padding(0).Flex(1)
                                .Items(
                                        X.Container()
                                            .StyleSpec("border-bottom: 1px solid #b5b8c8;")
                                            .Height(31)
                                            .Layout(LayoutType.Fit)
                                            .Items(
                                            hiddenId,
                                            comboboxEmployee
                                            .MarginSpec("-1 -1 0 -1")
                                            .ItemID("comboEmployeeID")
                                            .Listeners(ls =>
                                            {
                                                ls.Change.Handler = @"var comboValue = this.value;
                                                    var record = this.store.getById(comboValue);
                                                    if(this.store.data.length > 0 && record)
                                                    {changeValueEmployee(comboValue,record.data)}
                                                    else{resetValueEmployee();}";
                                                ls.Select.Handler = @"var comboValue = this.value; 
                                                    var record = this.store.getById(comboValue); 
                                                    if(this.store.data.length > 0 && record)
                                                    {changeValueEmployee(comboValue,record.data)}";
                                            })),
                                        X.DisplayFieldFor(key + "RoleNames")
                                            .ItemID("dplEmployeeTitle")
                                            .LabelStyle("font-size: 1em;font-weight: bold;")
                                            .MarginSpec("-4 0 0 5")
                                            .Flex(1)
                                            .HideLabel(true)
                                            .Listeners(ls =>
                                            {
                                                ls.Change.Handler = "if(this.value!=null && this.value.length > 30){this.setValue(this.value.substring(0,30) + '...');}";
                                            })
                                        )
                        );
            if (value == null)
            {
                var c = comboboxEmployee.ToComponent();
                var store = c.Store.Where(i => i is Store).Cast<Store>().FirstOrDefault();
                store.Listeners.Load.Delay = 100;
                store.Listeners.Load.Handler = @"if(records.length==1){var comboEmployee = Ext.getCmp('" + idComboEmployee + "'); comboEmployee.setValue(records[0].get(comboEmployee.valueField));}";
            }
            container.Add(hiddenDepartment);
            container.Add(hiddenOrder);
            container.Add(hiddenProcess);
            container.Add(comboRole);
            container.Add(component);
            return container;
        }
        public static Container.Builder DocumentEmployeeFieldFor1<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression,
           ResourceService.DocumentTypeProcess? typeProcess = null, string departmentId = default(string), int order = 0, bool allowBlank = true, ResourceService.DocumentProcessRole? startRole = null, bool setHidden = false)
           where TProperty : DocumentEmployeeBO
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var idComboRole1 = X.GetComponentID(); var idComboEmployee1 = X.GetComponentID(); var storeId1 = X.GetComponentID(); var processHidden1 = X.GetComponentID(); var idOrder1 = X.GetComponentID();
            var script = @"<script> var employeeContainer1;
                                         var changeValueEmployee1 = function (value, record) {
                                                employeeContainer1.queryById('hdfID1').setValue(record.Id);
                                                employeeContainer1.queryById('dplEmployeeTitle1').setValue(record.RoleNames);
                                                employeeContainer1.queryById('imgAvatarUrl1').setImageUrl(record.AvatarUrl);
                                            };
                                         var resetValueEmployee1 = function () {
                                                employeeContainer1.queryById('hdfID1').reset();
                                                employeeContainer1.queryById('dplEmployeeTitle1').reset();
                                                employeeContainer1.queryById('imgAvatarUrl1').setImageUrl('/Content/images/underfind.jpg');
                                            };
                                         var reloadDocumentEmployee = function () {
                                                employeeContainer1.queryById('cbRoleType1').getStore().reload();
                                            };
                                        </script>";
            var container = X.Container().Defaults(new Parameter().ToBuilder().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value)).Layout(LayoutType.VBox).Listeners(ls => ls.BoxReady.Handler = "employeeContainer1 = this;").LayoutConfig(new VBoxLayoutConfig { Align = VBoxAlign.Stretch });
            var hiddenProcess1 = X.Hidden().ID(processHidden1); var hiddenOrder1 = X.HiddenFor(key + "NextOrder").ID(idOrder1); var hiddenDepartment1 = X.HiddenFor(key + "DepartmentId").ItemID("itemIdDepartmentId1"); var hiddenId1 = X.HiddenFor(key + "Id").ItemID("hdfID1");
            if (!departmentId.Contains("getValue")) hiddenDepartment1.Value(departmentId);
            if (value != null) order = value.NextOrder - 1;
            #region danh sách trách nhiệm
            var comboRole = X.ComboBoxFor(key + "RoleType").Note("Vai trò người nhận").HideLabel(true).ID(idComboRole1).AllowBlank(allowBlank).ItemID("cbRoleType1")
                            .Hidden(setHidden).NoteAlign(NoteAlign.Top).MarginSpec("0 0 5 0").Editable(false).BlankText("Trường không được để trống!")
                            .DisplayField("Name").ValueField("ID")
                            .Listeners(ls =>
                            {
                                ls.Change.Handler = @"var record = App." + storeId1 + ".findRecord('ID',this.value); Ext.getCmp('" + processHidden1 + "').setValue(record.data.ProcessType); Ext.getCmp('" + idOrder1 + "').setValue(record.data.Order); Ext.getCmp('" + idComboEmployee1 + "').getStore().reload();";
                                ls.Select.Handler = @"var comboEmployee = Ext.getCmp('" + idComboEmployee1 + "'); comboEmployee.reset(); comboEmployee.getStore().reload();";
                            })
                            .Store(X.Store().ID(storeId1).AutoLoad(true)
                                    .Proxy(Html.X().AjaxProxy()
                                                .Url(X.XController().UrlHelper.Action("GetNextRoleType", "Setting", new { Area = "Document" }))
                                                .Reader(Html.X().JsonReader().RootProperty("data"))
                                            )
                                    .Parameters(ps =>
                                    {
                                        ps.Add(new StoreParameter("departmentId", departmentId, departmentId.Contains("getValue") ? ParameterMode.Raw : ParameterMode.Value));
                                        ps.Add(new StoreParameter("typeProcess", ((int)typeProcess).ToString(), ParameterMode.Value));
                                        ps.Add(new StoreParameter("startRole", ((int?)startRole).ToString(), ParameterMode.Value));
                                        ps.Add(new StoreParameter("order", order.ToString(), ParameterMode.Value));
                                    })
                                    .Listeners(ls =>
                                    {
                                        ls.Load.Delay = 200;
                                        ls.Load.Handler = @"var comboRole = Ext.getCmp('" + idComboRole1 + "'); if(records.length > 0){comboRole.setValue(records[0].get(comboRole.valueField));}";
                                    })
                             );
            #endregion
            var comboboxEmployee = X.ComboBox()
                            .DisplayField("Name")
                            .ValueField("Id")
                            .ID(idComboEmployee1)
                            .HideLabel(true)
                            .TypeAhead(false)
                            .PageSize(5)
                            .EmptyText("Nhập tên nhân sự ....")
                            .MinChars(0)
                            .CheckChangeBuffer(200)
                            .TriggerAction(TriggerAction.Query)
                            .ListConfig(Html.X().BoundList()
                            .LoadingText("Tìm kiếm...")
                            .ItemTpl(Html.X().XTemplate()
                            .Html(@"<text>
                                    <span>
                                    <img src='{AvatarUrl}' style='margin-top: 15px; width: 30px; height: 30px; border-radius: 50%; position: absolute; border: 1px solid'>
                                    <span style='margin-left: 44px;'>{FullNameFormat}</span><br/>
                                    <span style='margin-left: 44px;font-size:10px;'>{RoleNames}</span></span>
                                    </span>
                                </text>")
                                )
                            )
                            .Store(Html.X().Store()
                            .AutoLoad(false)
                            .Proxy(Html.X().AjaxProxy()
                            .Url(X.XController()
                            .UrlHelper.Action("LoadProcessEmployeeQuery", "Organization", new { Area = "Organization" })
                            )
                            .ActionMethods(am => am.Read = HttpMethod.POST)
                            .Reader(Html.X().JsonReader().RootProperty("data"))
                            )
                            .Model(Html.X().Model()
                            .IDProperty("Id")
                            .Fields(
                                Html.X().ModelField().Name("Id").Type(ModelFieldType.Auto),
                                Html.X().ModelField().Name("AvatarUrl").Type(ModelFieldType.String),
                                Html.X().ModelField().Name("Name").Type(ModelFieldType.String),
                                Html.X().ModelField().Name("RoleNames").Type(ModelFieldType.String)
                            )
                            )
                            .Parameters(ps =>
                            {
                                ps.Add(new StoreParameter("departmentId", departmentId, departmentId.Contains("getValue") ? ParameterMode.Raw : ParameterMode.Value));
                                ps.Add(new StoreParameter("process", "1", ParameterMode.Raw));
                                ps.Add(new StoreParameter("startRole", "5", ParameterMode.Raw));
                                ps.Add(new StoreParameter("order", "Ext.getCmp('" + idOrder1 + "').getValue()", ParameterMode.Raw));
                            })
                            );
            if (value != null) comboboxEmployee.ToComponent().Text = value.Name;
            var component = X.ContainerHBox()
                    .HtmlBin(c => script)
                    .Items(
                            X.ImageButton().Height(69).Width(60).ItemID("imgAvatarUrl1")
                            .ImageUrl(value != null ? value.AvatarUrl : "/Content/images/underfind.jpg")
                            .StyleSpec("border: 2px solid #b5b8c8;")
                            .Listeners(ls => ls.Click.Handler = @"var id = this.up().queryById('comboEmployeeID1').getValue(); if (id == '' || id == 0) { return; }; openEmployeeDetail(id);"),
                            X.ContainerVBox().StyleSpec("border: 1px solid #b5b8c8;").MarginSpec("0 0 0 2").Padding(0).Flex(1)
                                .Items(
                                        X.Container()
                                            .StyleSpec("border-bottom: 1px solid #b5b8c8;")
                                            .Height(31)
                                            .Layout(LayoutType.Fit)
                                            .Items(
                                            hiddenId1,
                                            comboboxEmployee
                                            .MarginSpec("-1 -1 0 -1")
                                            .ItemID("comboEmployeeID1")
                                            .Listeners(ls =>
                                            {
                                                ls.Change.Handler = @"var comboValue = this.value;
                                                    var record = this.store.getById(comboValue);
                                                    if(this.store.data.length > 0 && record)
                                                    {changeValueEmployee1(comboValue,record.data)}
                                                    else{resetValueEmployee1();}";
                                                ls.Select.Handler = @"var comboValue = this.value; 
                                                    var record = this.store.getById(comboValue); 
                                                    if(this.store.data.length > 0 && record)
                                                    {changeValueEmployee1(comboValue,record.data)}";
                                            })),
                                        X.DisplayFieldFor(key + "RoleNames")
                                            .ItemID("dplEmployeeTitle1")
                                            .LabelStyle("font-size: 1em;font-weight: bold;")
                                            .MarginSpec("-4 0 0 5")
                                            .Flex(1)
                                            .HideLabel(true)
                                            .Listeners(ls =>
                                            {
                                                ls.Change.Handler = "if(this.value!=null && this.value.length > 30){this.setValue(this.value.substring(0,30) + '...');}";
                                            })
                                        )
                        );
            if (value == null)
            {
                var c = comboboxEmployee.ToComponent();
                var store = c.Store.Where(i => i is Store).Cast<Store>().FirstOrDefault();
                store.Listeners.Load.Delay = 100;
                store.Listeners.Load.Handler = @"if(records.length==1){var comboEmployee1 = Ext.getCmp('" + idComboEmployee1 + "'); comboEmployee1.setValue(records[0].get(comboEmployee1.valueField));}";
            }
            container.Add(hiddenDepartment1);
            container.Add(hiddenOrder1);
            container.Add(hiddenProcess1);
            container.Add(comboRole);
            container.Add(component);
            return container;
        }
        public static Container.Builder DocumentEmployeeFieldFor2<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression,
           ResourceService.DocumentTypeProcess? typeProcess = null, string departmentId = default(string), int order = 0, bool allowBlank = true, ResourceService.DocumentProcessRole? startRole = null, bool setHidden = false)
           where TProperty : DocumentEmployeeBO
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var idComboRole2 = X.GetComponentID(); var idComboEmployee2 = X.GetComponentID(); var storeId2 = X.GetComponentID(); var processHidden2 = X.GetComponentID(); var idOrder2 = X.GetComponentID();
            var script = @"<script> var employeeContainer2;
                                         var changeValueEmployee2 = function (value, record) {
                                                employeeContainer2.queryById('hdfID2').setValue(record.Id);
                                                employeeContainer2.queryById('dplEmployeeTitle2').setValue(record.RoleNames);
                                                employeeContainer2.queryById('imgAvatarUrl2').setImageUrl(record.AvatarUrl);
                                            };
                                         var resetValueEmployee2 = function () {
                                                employeeContainer2.queryById('hdfID2').reset();
                                                employeeContainer2.queryById('dplEmployeeTitle2').reset();
                                                employeeContainer2.queryById('imgAvatarUrl2').setImageUrl('/Content/images/underfind.jpg');
                                            };
                                         var reloadDocumentEmployee = function () {
                                                employeeContainer2.queryById('cbRoleType2').getStore().reload();
                                            };
                                        </script>";
            var container = X.Container().Defaults(new Parameter().ToBuilder().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value)).Layout(LayoutType.VBox).Listeners(ls => ls.BoxReady.Handler = "employeeContainer2 = this;").LayoutConfig(new VBoxLayoutConfig { Align = VBoxAlign.Stretch });
            var hiddenProcess2 = X.Hidden().ID(processHidden2); var hiddenOrder2 = X.HiddenFor(key + "NextOrder").ID(idOrder2); var hiddenDepartment2 = X.HiddenFor(key + "DepartmentId").ItemID("itemIdDepartmentId2"); var hiddenId2 = X.HiddenFor(key + "Id").ItemID("hdfID2");
            if (!departmentId.Contains("getValue")) hiddenDepartment2.Value(departmentId);
            if (value != null) order = value.NextOrder - 1;
            #region danh sách trách nhiệm
            var comboRole = X.ComboBoxFor(key + "RoleType").Note("Vai trò người nhận").HideLabel(true).ID(idComboRole2).AllowBlank(allowBlank).ItemID("cbRoleType2")
                            .Hidden(setHidden).NoteAlign(NoteAlign.Top).MarginSpec("0 0 5 0").Editable(false).BlankText("Trường không được để trống!")
                            .DisplayField("Name").ValueField("ID")
                            .Listeners(ls =>
                            {
                                ls.Change.Handler = @"var record = App." + storeId2 + ".findRecord('ID',this.value); Ext.getCmp('" + processHidden2 + "').setValue(record.data.ProcessType); Ext.getCmp('" + idOrder2 + "').setValue(record.data.Order); Ext.getCmp('" + idComboEmployee2 + "').getStore().reload();";
                                ls.Select.Handler = @"var comboEmployee = Ext.getCmp('" + idComboEmployee2 + "'); comboEmployee.reset(); comboEmployee.getStore().reload();";
                            })
                            .Store(X.Store().ID(storeId2).AutoLoad(true)
                                    .Proxy(Html.X().AjaxProxy()
                                                .Url(X.XController().UrlHelper.Action("GetNextRoleType", "Setting", new { Area = "Document" }))
                                                .Reader(Html.X().JsonReader().RootProperty("data"))
                                            )
                                    .Parameters(ps =>
                                    {
                                        ps.Add(new StoreParameter("departmentId", departmentId, departmentId.Contains("getValue") ? ParameterMode.Raw : ParameterMode.Value));
                                        ps.Add(new StoreParameter("typeProcess", ((int)typeProcess).ToString(), ParameterMode.Value));
                                        ps.Add(new StoreParameter("startRole", ((int?)startRole).ToString(), ParameterMode.Value));
                                        ps.Add(new StoreParameter("order", order.ToString(), ParameterMode.Value));
                                    })
                                    .Listeners(ls =>
                                    {
                                        ls.Load.Delay = 200;
                                        ls.Load.Handler = @"var comboRole = Ext.getCmp('" + idComboRole2 + "'); if(records.length > 0){comboRole.setValue(records[0].get(comboRole.valueField));}";
                                    })
                             );
            #endregion
            var comboboxEmployee = X.ComboBox()
                            .DisplayField("Name")
                            .ValueField("Id")
                            .ID(idComboEmployee2)
                            .HideLabel(true)
                            .TypeAhead(false)
                            .PageSize(5)
                            .EmptyText("Nhập tên nhân sự ....")
                            .MinChars(0)
                            .CheckChangeBuffer(200)
                            .TriggerAction(TriggerAction.Query)
                            .ListConfig(Html.X().BoundList()
                            .LoadingText("Tìm kiếm...")
                            .ItemTpl(Html.X().XTemplate()
                            .Html(@"<text>
                                    <span>
                                    <img src='{AvatarUrl}' style='margin-top: 15px; width: 30px; height: 30px; border-radius: 50%; position: absolute; border: 1px solid'>
                                    <span style='margin-left: 44px;'>{FullNameFormat}</span><br/>
                                    <span style='margin-left: 44px;font-size:10px;'>{RoleNames}</span></span>
                                    </span>
                                </text>")
                                )
                            )
                            .Store(Html.X().Store()
                            .AutoLoad(false)
                            .Proxy(Html.X().AjaxProxy()
                            .Url(X.XController()
                            .UrlHelper.Action("LoadProcessEmployeeQuery", "Organization", new { Area = "Organization" })
                            )
                            .ActionMethods(am => am.Read = HttpMethod.POST)
                            .Reader(Html.X().JsonReader().RootProperty("data"))
                            )
                            .Model(Html.X().Model()
                            .IDProperty("Id")
                            .Fields(
                                Html.X().ModelField().Name("Id").Type(ModelFieldType.Auto),
                                Html.X().ModelField().Name("AvatarUrl").Type(ModelFieldType.String),
                                Html.X().ModelField().Name("Name").Type(ModelFieldType.String),
                                Html.X().ModelField().Name("RoleNames").Type(ModelFieldType.String)
                            )
                            )
                            .Parameters(ps =>
                            {
                                ps.Add(new StoreParameter("departmentId", departmentId, departmentId.Contains("getValue") ? ParameterMode.Raw : ParameterMode.Value));
                                ps.Add(new StoreParameter("process", "1", ParameterMode.Raw));
                                ps.Add(new StoreParameter("startRole", "6", ParameterMode.Raw));
                                ps.Add(new StoreParameter("order", "Ext.getCmp('" + idOrder2 + "').getValue()", ParameterMode.Raw));
                            })
                            );
            if (value != null) comboboxEmployee.ToComponent().Text = value.Name;
            var component = X.ContainerHBox()
                    .HtmlBin(c => script)
                    .Items(
                            X.ImageButton().Height(69).Width(60).ItemID("imgAvatarUrl2")
                            .ImageUrl(value != null ? value.AvatarUrl : "/Content/images/underfind.jpg")
                            .StyleSpec("border: 2px solid #b5b8c8;")
                            .Listeners(ls => ls.Click.Handler = @"var id = this.up().queryById('comboEmployeeID2').getValue(); if (id == '' || id == 0) { return; }; openEmployeeDetail(id);"),
                            X.ContainerVBox().StyleSpec("border: 1px solid #b5b8c8;").MarginSpec("0 0 0 2").Padding(0).Flex(1)
                                .Items(
                                        X.Container()
                                            .StyleSpec("border-bottom: 1px solid #b5b8c8;")
                                            .Height(31)
                                            .Layout(LayoutType.Fit)
                                            .Items(
                                            hiddenId2,
                                            comboboxEmployee
                                            .MarginSpec("-1 -1 0 -1")
                                            .ItemID("comboEmployeeID2")
                                            .Listeners(ls =>
                                            {
                                                ls.Change.Handler = @"var comboValue = this.value;
                                                    var record = this.store.getById(comboValue);
                                                    if(this.store.data.length > 0 && record)
                                                    {changeValueEmployee2(comboValue,record.data)}
                                                    else{resetValueEmployee2();}";
                                                ls.Select.Handler = @"var comboValue = this.value; 
                                                    var record = this.store.getById(comboValue); 
                                                    if(this.store.data.length > 0 && record)
                                                    {changeValueEmployee2(comboValue,record.data)}";
                                            })),
                                        X.DisplayFieldFor(key + "RoleNames")
                                            .ItemID("dplEmployeeTitle2")
                                            .LabelStyle("font-size: 1em;font-weight: bold;")
                                            .MarginSpec("-4 0 0 5")
                                            .Flex(1)
                                            .HideLabel(true)
                                            .Listeners(ls =>
                                            {
                                                ls.Change.Handler = "if(this.value!=null && this.value.length > 30){this.setValue(this.value.substring(0,30) + '...');}";
                                            })
                                        )
                        );
            if (value == null)
            {
                var c = comboboxEmployee.ToComponent();
                var store = c.Store.Where(i => i is Store).Cast<Store>().FirstOrDefault();
                store.Listeners.Load.Delay = 100;
                store.Listeners.Load.Handler = @"if(records.length==1){var comboEmployee2 = Ext.getCmp('" + idComboEmployee2 + "'); comboEmployee2.setValue(records[0].get(comboEmployee2.valueField));}";
            }
            container.Add(hiddenDepartment2);
            container.Add(hiddenOrder2);
            container.Add(hiddenProcess2);
            container.Add(comboRole);
            container.Add(component);
            return container;
        }
        public static Container.Builder DocumentEmployeeFieldFor3<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression,
           ResourceService.DocumentTypeProcess? typeProcess = null, string departmentId = default(string), int order = 0, bool allowBlank = true, ResourceService.DocumentProcessRole? startRole = null, bool setHidden = false)
           where TProperty : DocumentEmployeeBO
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var idComboRole3 = X.GetComponentID(); var idComboEmployee3 = X.GetComponentID(); var storeId3 = X.GetComponentID(); var processHidden3 = X.GetComponentID(); var idOrder3 = X.GetComponentID();
            var script = @"<script> var employeeContainer3;
                                         var changeValueEmployee3 = function (value, record) {
                                                employeeContainer3.queryById('hdfID3').setValue(record.Id);
                                                employeeContainer3.queryById('dplEmployeeTitle3').setValue(record.RoleNames);
                                                employeeContainer3.queryById('imgAvatarUrl3').setImageUrl(record.AvatarUrl);
                                            };
                                         var resetValueEmployee3 = function () {
                                                employeeContainer3.queryById('hdfID3').reset();
                                                employeeContainer3.queryById('dplEmployeeTitle3').reset();
                                                employeeContainer3.queryById('imgAvatarUrl3').setImageUrl('/Content/images/underfind.jpg');
                                            };
                                         var reloadDocumentEmployee = function () {
                                                employeeContainer3.queryById('cbRoleType3').getStore().reload();
                                            };
                                        </script>";
            var container = X.Container().Defaults(new Parameter().ToBuilder().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value)).Layout(LayoutType.VBox).Listeners(ls => ls.BoxReady.Handler = "employeeContainer3 = this;").LayoutConfig(new VBoxLayoutConfig { Align = VBoxAlign.Stretch });
            var hiddenProcess3 = X.Hidden().ID(processHidden3); var hiddenOrder3 = X.HiddenFor(key + "NextOrder").ID(idOrder3); var hiddenDepartment3 = X.HiddenFor(key + "DepartmentId").ItemID("itemIdDepartmentId3"); var hiddenId3 = X.HiddenFor(key + "Id").ItemID("hdfID3");
            if (!departmentId.Contains("getValue")) hiddenDepartment3.Value(departmentId);
            if (value != null) order = value.NextOrder - 1;
            #region danh sách trách nhiệm
            var comboRole = X.ComboBoxFor(key + "RoleType").Note("Vai trò người nhận").HideLabel(true).ID(idComboRole3).AllowBlank(allowBlank).ItemID("cbRoleType3")
                            .Hidden(setHidden).NoteAlign(NoteAlign.Top).MarginSpec("0 0 5 0").Editable(false).BlankText("Trường không được để trống!")
                            .DisplayField("Name").ValueField("ID")
                            .Listeners(ls =>
                            {
                                ls.Change.Handler = @"var record = App." + storeId3 + ".findRecord('ID',this.value); Ext.getCmp('" + processHidden3 + "').setValue(record.data.ProcessType); Ext.getCmp('" + idOrder3 + "').setValue(record.data.Order); Ext.getCmp('" + idComboEmployee3 + "').getStore().reload();";
                                ls.Select.Handler = @"var comboEmployee = Ext.getCmp('" + idComboEmployee3 + "'); comboEmployee.reset(); comboEmployee.getStore().reload();";
                            })
                            .Store(X.Store().ID(storeId3).AutoLoad(true)
                                    .Proxy(Html.X().AjaxProxy()
                                                .Url(X.XController().UrlHelper.Action("GetNextRoleType", "Setting", new { Area = "Document" }))
                                                .Reader(Html.X().JsonReader().RootProperty("data"))
                                            )
                                    .Parameters(ps =>
                                    {
                                        ps.Add(new StoreParameter("departmentId", departmentId, departmentId.Contains("getValue") ? ParameterMode.Raw : ParameterMode.Value));
                                        ps.Add(new StoreParameter("typeProcess", ((int)typeProcess).ToString(), ParameterMode.Value));
                                        ps.Add(new StoreParameter("startRole", ((int?)startRole).ToString(), ParameterMode.Value));
                                        ps.Add(new StoreParameter("order", order.ToString(), ParameterMode.Value));
                                    })
                                    .Listeners(ls =>
                                    {
                                        ls.Load.Delay = 200;
                                        ls.Load.Handler = @"var comboRole = Ext.getCmp('" + idComboRole3 + "'); if(records.length > 0){comboRole.setValue(records[0].get(comboRole.valueField));}";
                                    })
                             );
            #endregion
            var comboboxEmployee = X.ComboBox()
                            .DisplayField("Name")
                            .ValueField("Id")
                            .ID(idComboEmployee3)
                            .HideLabel(true)
                            .TypeAhead(false)
                            .PageSize(5)
                            .EmptyText("Nhập tên nhân sự ....")
                            .MinChars(0)
                            .CheckChangeBuffer(200)
                            .TriggerAction(TriggerAction.Query)
                            .ListConfig(Html.X().BoundList()
                            .LoadingText("Tìm kiếm...")
                            .ItemTpl(Html.X().XTemplate()
                            .Html(@"<text>
                                    <span>
                                    <img src='{AvatarUrl}' style='margin-top: 15px; width: 30px; height: 30px; border-radius: 50%; position: absolute; border: 1px solid'>
                                    <span style='margin-left: 44px;'>{FullNameFormat}</span><br/>
                                    <span style='margin-left: 44px;font-size:10px;'>{RoleNames}</span></span>
                                    </span>
                                </text>")
                                )
                            )
                            .Store(Html.X().Store()
                            .AutoLoad(false)
                            .Proxy(Html.X().AjaxProxy()
                            .Url(X.XController()
                            .UrlHelper.Action("LoadProcessEmployeeQuery", "Organization", new { Area = "Organization" })
                            )
                            .ActionMethods(am => am.Read = HttpMethod.POST)
                            .Reader(Html.X().JsonReader().RootProperty("data"))
                            )
                            .Model(Html.X().Model()
                            .IDProperty("Id")
                            .Fields(
                                Html.X().ModelField().Name("Id").Type(ModelFieldType.Auto),
                                Html.X().ModelField().Name("AvatarUrl").Type(ModelFieldType.String),
                                Html.X().ModelField().Name("Name").Type(ModelFieldType.String),
                                Html.X().ModelField().Name("RoleNames").Type(ModelFieldType.String)
                            )
                            )
                            .Parameters(ps =>
                            {
                                ps.Add(new StoreParameter("departmentId", departmentId, departmentId.Contains("getValue") ? ParameterMode.Raw : ParameterMode.Value));
                                ps.Add(new StoreParameter("process", "1", ParameterMode.Raw));
                                ps.Add(new StoreParameter("startRole", "7", ParameterMode.Raw));
                                ps.Add(new StoreParameter("order", "Ext.getCmp('" + idOrder3 + "').getValue()", ParameterMode.Raw));
                            })
                            );
            if (value != null) comboboxEmployee.ToComponent().Text = value.Name;
            var component = X.ContainerHBox()
                    .HtmlBin(c => script)
                    .Items(
                            X.ImageButton().Height(69).Width(60).ItemID("imgAvatarUrl3")
                            .ImageUrl(value != null ? value.AvatarUrl : "/Content/images/underfind.jpg")
                            .StyleSpec("border: 2px solid #b5b8c8;")
                            .Listeners(ls => ls.Click.Handler = @"var id = this.up().queryById('comboEmployeeID3').getValue(); if (id == '' || id == 0) { return; }; openEmployeeDetail(id);"),
                            X.ContainerVBox().StyleSpec("border: 1px solid #b5b8c8;").MarginSpec("0 0 0 2").Padding(0).Flex(1)
                                .Items(
                                        X.Container()
                                            .StyleSpec("border-bottom: 1px solid #b5b8c8;")
                                            .Height(31)
                                            .Layout(LayoutType.Fit)
                                            .Items(
                                            hiddenId3,
                                            comboboxEmployee
                                            .MarginSpec("-1 -1 0 -1")
                                            .ItemID("comboEmployeeID3")
                                            .Listeners(ls =>
                                            {
                                                ls.Change.Handler = @"var comboValue = this.value;
                                                    var record = this.store.getById(comboValue);
                                                    if(this.store.data.length > 0 && record)
                                                    {changeValueEmployee3(comboValue,record.data)}
                                                    else{resetValueEmployee3();}";
                                                ls.Select.Handler = @"var comboValue = this.value; 
                                                    var record = this.store.getById(comboValue); 
                                                    if(this.store.data.length > 0 && record)
                                                    {changeValueEmployee3(comboValue,record.data)}";
                                            })),
                                        X.DisplayFieldFor(key + "RoleNames")
                                            .ItemID("dplEmployeeTitle3")
                                            .LabelStyle("font-size: 1em;font-weight: bold;")
                                            .MarginSpec("-4 0 0 5")
                                            .Flex(1)
                                            .HideLabel(true)
                                            .Listeners(ls =>
                                            {
                                                ls.Change.Handler = "if(this.value!=null && this.value.length > 30){this.setValue(this.value.substring(0,30) + '...');}";
                                            })
                                        )
                        );
            if (value == null)
            {
                var c = comboboxEmployee.ToComponent();
                var store = c.Store.Where(i => i is Store).Cast<Store>().FirstOrDefault();
                store.Listeners.Load.Delay = 100;
                store.Listeners.Load.Handler = @"if(records.length==1){var comboEmployee3 = Ext.getCmp('" + idComboEmployee3 + "'); comboEmployee3.setValue(records[0].get(comboEmployee3.valueField));}";
            }
            container.Add(hiddenDepartment3);
            container.Add(hiddenOrder3);
            container.Add(hiddenProcess3);
            container.Add(comboRole);
            container.Add(component);
            return container;
        }
        public static Container.Builder EmployeeFieldFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null)
            where TProperty : EmployeeBO
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var button = X.Button()
            .ItemID("btnSelectEmployee")
            .IconCls("x-fa fa-pencil")
            .Margin(2)
            .ToolTip("Lựa chọn nhân sự")
            .RequestUrl(X.XController().UrlHelper.Action("Index", "Employee", new { Area = "Generic" }));
            var script = @"<script>
                            var employeeContainer;
                            var changeValueEmployee = function (value, record) {
                                    employeeContainer.queryById('hdfemployeeID').setValue(record.Id);
                                    employeeContainer.queryById('dplEmployeeTitle').setValue(record.RoleNames);
                                    employeeContainer.queryById('imgAvatarUrl').setImageUrl(record.AvatarUrl);
                                };
                            var selectEmployee = function(records) { 
                                var record = records[0];
                                employeeContainer.queryById('hdfemployeeID').setValue(record.get('Id')); 
                                employeeContainer.queryById('itemCBEmployee').addItem(record.get('Name'),record.get('Id'));                               
                                employeeContainer.queryById('itemCBEmployee').setValue(record.get('Id'));
                                employeeContainer.queryById('imgAvatarUrl').setImageUrl(record.get('AvatarUrl'));
                                employeeContainer.queryById('dplEmployeeTitle').setValue(record.get('RoleNames'));
                            };
                            var resetEmployee = function(){                              
                                employeeContainer.queryById('hdfemployeeID').reset();
                                employeeContainer.queryById('itemCBEmployee').reset();
                                employeeContainer.queryById('imgAvatarUrl').setImageUrl('/Content/images/underfind.jpg');
                                employeeContainer.queryById('dplEmployeeTitle').setValue('');
                            };
                            var setButtonEmployeeHide = function(hidden){
                                employeeContainer.queryById('btnSelectEmployee').setHidden(hidden);
                            };
                            </script>";
            //var property = expression.Body.ToString().Split('.').Last();
            //var attribute = X.HtmlHelper.ViewData.Model.GetType().GetProperty(property).GetCustomAttribute<ValidationAttribute>();
            //var isRequired = attribute != null;
            //var errorMessage = attribute != null ? attribute.ErrorMessage : string.Empty;
            //errorMessage = errorMessage == string.Empty ? "Bạn cần nhập thông tin!" : errorMessage;
            var combobox = X.ComboboxEmployeeFor(expression, setId, convert, format);
            //            var textField = X.TextField().ItemID("validEmployee")
            //                            .Hidden(true).HideLabel(true)
            //                            .AllowBlank(!isRequired)
            //                            .Listeners(ls => ls.ValidityChange.Handler =
            //                            @"if(!isValid){ 
            //                                            employeeContainer.queryById('imgAvatarUrl').addCls('cls_validate');
            //                                            employeeContainer.queryById('imgAvatarUrl').setTooltip('<ul class=\'x-list-plain x-form-required-field\'><li><i style=\'color:red\' class=\'x-fa fa-info-circle\'></i> " + errorMessage + @"</li></ul>');
            //                                        } 
            //                                        else{
            //                                            employeeContainer.queryById('imgAvatarUrl').removeCls('cls_validate'); 
            //                                            employeeContainer.queryById('imgAvatarUrl').setTooltip('');
            //                                        }");
            //if (value != null && value.Id != Guid.Empty)
            //{
            //    textField.Value(value.Id.ToString());
            //}
            var component = X.ContainerHBox()
            .Listeners(ls => ls.BoxReady.Handler = "employeeContainer = this")
                .HtmlBin(c => script)
                .Items(
                X.HiddenFor(key + "Id")
                .ItemID("hdfemployeeID"),
                X.ImageButton()
                .ItemID("imgAvatarUrl")
                .ImageUrl(value != null ? value.AvatarUrl : "/Content/images/underfind.jpg").Height(69).Width(60).StyleSpec("border: 2px solid #b5b8c8;")
                .Listeners(ls => ls.Click.Handler = @"var id = this.up().queryById('hdfemployeeID').getValue(); if (id == '' || id == 0) { return; }; openEmployeeDetail(id);"),
                X.ContainerVBox()
                .StyleSpec("border: 1px solid #b5b8c8;")
                .MarginSpec("0 0 0 2")
                .Padding(0).Flex(1)
                    .Items(
                    X.ContainerHBox()
                    .StyleSpec("border-bottom: 1px solid #b5b8c8;")
                    .Height(31)
                    .Items(
                        combobox
                        .ItemID("itemCBEmployee")
                        .Padding(0)
                        .MarginSpec("-1 -1 -1 -1")
                        .HideLabel(true)
                        .Flex(1)
                        .Listeners(ls =>
                        {
                            ls.Select.Handler = "var comboValue = this.value; var record = this.store.getById(comboValue); if(this.store.data.length > 0 && record){changeValueEmployee(comboValue,record.data)}; ";
                        }),
                        button
                    ),
                X.DisplayFieldFor(key + "RoleNames").ItemID("dplEmployeeTitle")
                    .LabelStyle("font-size: 1em;font-weight: bold;").MarginSpec("-4 0 0 5").Flex(1).HideLabel(true)
                    .Listeners(ls => ls.Change.Handler = "if(this.value.length > 30){this.setValue(this.value.substring(0,30) + '...');}")
                )
            );
            return component;
        }
        public static Container.Builder EmployeeFieldOtherFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null)
            where TProperty : EmployeeBO
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var button = X.Button()
            .ItemID("btnSelectEmployeeOther")
            .IconCls("x-fa fa-pencil")
            .Margin(2)
            .ToolTip("Lựa chọn nhân sự")
            .RequestUrl(X.XController().UrlHelper.Action("Index", "Employee", new { Area = "Generic" }), true);
            var script = @"<script>
                            var employeeContainerOther;
                            var changeValueEmployeeOther = function (value, record) {
                                    employeeContainerOther.queryById('hdfemployeeIDOther').setValue(record.Id);
                                    employeeContainerOther.queryById('dplEmployeeTitleOther').setValue(record.RoleNames);
                                    employeeContainerOther.queryById('imgAvatarUrlOther').setImageUrl(record.AvatarUrl);
                                };
                            var selectEmployeeOther = function(records) { 
                                var record = records[0];
                                employeeContainerOther.queryById('hdfemployeeIDOther').setValue(record.get('Id')); 
                                employeeContainerOther.queryById('itemCBEmployeeOther').addItem(record.get('Name'),record.get('Id'));                               
                                employeeContainerOther.queryById('itemCBEmployeeOther').setValue(record.get('Id'));
                                employeeContainerOther.queryById('imgAvatarUrlOther').setImageUrl(record.get('AvatarUrl'));
                                employeeContainerOther.queryById('dplEmployeeTitleOther').setValue(record.get('RoleNames'));
                            };
                            var resetEmployeeOther = function(){                              
                                employeeContainerOther.queryById('hdfemployeeIDOther').reset();
                                employeeContainerOther.queryById('itemCBEmployeeOther').reset();
                                employeeContainerOther.queryById('imgAvatarUrlOther').setImageUrl('/Content/images/underfind.jpg');
                                employeeContainerOther.queryById('dplEmployeeTitleOther').setValue('');
                            };
                            var setButtonEmployeeHideOther = function(hidden){
                                employeeContainerOther.queryById('btnSelectEmployeeOther').setHidden(hidden);
                            };
                            </script>";
            var combobox = X.ComboboxEmployeeFor(expression, setId, convert, format);
            var component = X.ContainerHBox()
            .Listeners(ls => ls.BoxReady.Handler = "employeeContainerOther = this")
                .HtmlBin(c => script)
                .Items(
                X.HiddenFor(key + "IdOther").ID("hdfemployeeIDOtherTOGetValue")
                .ItemID("hdfemployeeIDOther"),
                X.ImageButton()
                .ItemID("imgAvatarUrlOther")
                .ImageUrl(value != null ? value.AvatarUrl : "/Content/images/underfind.jpg").Height(69).Width(60).StyleSpec("border: 2px solid #b5b8c8;")
                .Listeners(ls => ls.Click.Handler = @"var id = this.up().queryById('hdfemployeeIDOther').getValue(); if (id == '' || id == 0) { return; }; openEmployeeDetail(id);"),
                X.ContainerVBox()
                .StyleSpec("border: 1px solid #b5b8c8;")
                .MarginSpec("0 0 0 2")
                .Padding(0).Flex(1)
                    .Items(
                    X.ContainerHBox()
                    .StyleSpec("border-bottom: 1px solid #b5b8c8;")
                    .Height(31)
                    .Items(
                        combobox
                        .ItemID("itemCBEmployeeOther")
                        .Padding(0)
                        .MarginSpec("-1 -1 -1 -1")
                        .HideLabel(true)
                        .Flex(1)
                        .Listeners(ls =>
                        {
                            ls.Select.Handler = "var comboValue = this.value; var record = this.store.getById(comboValue); if(this.store.data.length > 0 && record){changeValueEmployeeOther(comboValue,record.data)}";
                        }),
                        button
                    ),
                X.DisplayFieldFor(key + "RoleNames").ItemID("dplEmployeeTitleOther")
                    .LabelStyle("font-size: 1em;font-weight: bold;").MarginSpec("-4 0 0 5").Flex(1).HideLabel(true)
                    .Listeners(ls => ls.Change.Handler = "if(this.value.length > 30){this.setValue(this.value.substring(0,30) + '...');}")
                )
            );
            return component;
        }
        public static Container.Builder EmployeeExtFieldFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null)
          where TProperty : EmployeeBOExt
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var button = X.Button()
            .ItemID("btnSelectEmployee")
            .IconCls("x-fa fa-pencil")
            .Margin(2)
            .ToolTip("Lựa chọn nhân sự")
            .RequestUrl(X.XController().UrlHelper.Action("Index", "Employee", new { Area = "Generic" }));
            var script = @"<script>
                            var employeeContainer;
                            var changeValueEmployee = function (value, record) {
                                    employeeContainer.queryById('hdfemployeeID').setValue(record.Id);
                                    employeeContainer.queryById('dplEmployeeTitle').setValue(record.RoleNames);
                                    employeeContainer.queryById('imgAvatarUrl').setImageUrl(record.AvatarUrl);
                                };
                            var selectEmployee = function(records) { 
                                var record = records[0];
                                employeeContainer.queryById('hdfemployeeID').setValue(record.get('Id')); 
                                employeeContainer.queryById('itemCBEmployee').addItem(record.get('Name'),record.get('Id'));                               
                                employeeContainer.queryById('itemCBEmployee').setValue(record.get('Id'));
                                employeeContainer.queryById('imgAvatarUrl').setImageUrl(record.get('AvatarUrl'));
                                employeeContainer.queryById('dplEmployeeTitle').setValue(record.get('RoleNames'));
                            };
                            var resetEmployee = function(){                              
                                employeeContainer.queryById('hdfemployeeID').reset();
                                employeeContainer.queryById('itemCBEmployee').reset();
                                employeeContainer.queryById('imgAvatarUrl').setImageUrl('/Content/images/underfind.jpg');
                                employeeContainer.queryById('dplEmployeeTitle').setValue('');
                            };
                            var setButtonEmployeeHide = function(hidden){
                                employeeContainer.queryById('btnSelectEmployee').setHidden(hidden);
                            };
                            </script>";
            //var property = expression.Body.ToString().Split('.').Last();
            //var attribute = X.HtmlHelper.ViewData.Model.GetType().GetProperty(property).GetCustomAttribute<ValidationAttribute>();
            //var isRequired = attribute != null;
            //var errorMessage = attribute != null ? attribute.ErrorMessage : string.Empty;
            //errorMessage = errorMessage == string.Empty ? "Bạn cần nhập thông tin!" : errorMessage;
            var combobox = X.ComboboxEmployeeExtFor(expression, setId, convert, format);
            //            var textField = X.TextField().ItemID("validEmployee")
            //                            .Hidden(true).HideLabel(true)
            //                            .AllowBlank(!isRequired)
            //                            .Listeners(ls => ls.ValidityChange.Handler =
            //                            @"if(!isValid){ 
            //                                            employeeContainer.queryById('imgAvatarUrl').addCls('cls_validate');
            //                                            employeeContainer.queryById('imgAvatarUrl').setTooltip('<ul class=\'x-list-plain x-form-required-field\'><li><i style=\'color:red\' class=\'x-fa fa-info-circle\'></i> " + errorMessage + @"</li></ul>');
            //                                        } 
            //                                        else{
            //                                            employeeContainer.queryById('imgAvatarUrl').removeCls('cls_validate'); 
            //                                            employeeContainer.queryById('imgAvatarUrl').setTooltip('');
            //                                        }");
            //if (value != null && value.Id != Guid.Empty)
            //{
            //    textField.Value(value.Id.ToString());
            //}
            var component = X.ContainerHBox()
            .Listeners(ls => ls.BoxReady.Handler = "employeeContainer = this")
                .HtmlBin(c => script)
                .Items(
                X.HiddenFor(key + "Id")
                .ItemID("hdfemployeeID"),
                X.ImageButton()
                .ItemID("imgAvatarUrl")
                .ImageUrl(value != null ? value.AvatarUrl : "/Content/images/underfind.jpg").Height(69).Width(60).StyleSpec("border: 2px solid #b5b8c8;")
                .Listeners(ls => ls.Click.Handler = @"var id = this.up().queryById('hdfemployeeID').getValue(); if (id == '' || id == 0) { return; }; openEmployeeDetail(id);"),
                X.ContainerVBox()
                .StyleSpec("border: 1px solid #b5b8c8;")
                .MarginSpec("0 0 0 2")
                .Padding(0).Flex(1)
                    .Items(
                    X.ContainerHBox()
                    .StyleSpec("border-bottom: 1px solid #b5b8c8;")
                    .Height(31)
                    .Items(
                        combobox
                        .ItemID("itemCBEmployee")
                        .Padding(0)
                        .MarginSpec("-1 -1 -1 -1")
                        .HideLabel(true)
                        .Flex(1)
                        .Listeners(ls =>
                        {
                            ls.Select.Handler = "var comboValue = this.value; var record = this.store.getById(comboValue); if(this.store.data.length > 0 && record){changeValueEmployee(comboValue,record.data)}; ";
                        }),
                        button
                    ),
                X.DisplayFieldFor(key + "RoleNames").ItemID("dplEmployeeTitle")
                    .LabelStyle("font-size: 1em;font-weight: bold;").MarginSpec("-4 0 0 5").Flex(1).HideLabel(true)
                    .Listeners(ls => ls.Change.Handler = "if(this.value.length > 30){this.setValue(this.value.substring(0,30) + '...');}")
                )
            );
            return component;
        }
        public static ComboBox.Builder ComboboxEmployeeExtFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null)
           where TProperty : EmployeeBOExt
        {
            var key = X.GetKey(expression);
            var value = X.GetValue(expression);
            var component = X.ComboBox()
                .DisplayField("Name")
                .ValueField("Id")
                .HideLabel(true)
                .TypeAhead(false).PageSize(5)
                .HideTrigger(true)
                .EmptyText("Nhập tên nhân sự ....")
                .MinChars(0).CheckChangeBuffer(200)
                .TriggerAction(TriggerAction.Query)
                .ListConfig(Html.X().BoundList()
                .LoadingText("Tìm kiếm...")
                .ItemTpl(Html.X().XTemplate()
                            .Html(@"<text>
                                    <img src='{AvatarUrl}' style='margin-top: 2px; width: 30px; height: 30px; border-radius: 50%; 
                                    position:absolute;border: 1px solid'>
                                    <span style='margin-left: 44px;'>{FullNameFormat}</span><br/>
                                    <div style='margin-left: 54px;font-size:10px;'>{RoleNames}</div>
                                </text>")
                        )
                )
                .Store(Html.X().Store()
                    .AutoLoad(false)
                    .Proxy(Html.X().AjaxProxy()
                    .Url(X.XController()
                        .UrlHelper.Action("LoadEmployeesQuery", "Organization", new { Area = "Organization" })
                        )
                    .ActionMethods(am => am.Read = HttpMethod.POST)
                    .Reader(Html.X().JsonReader().RootProperty("data"))
                    )
                    .Model(Html.X().Model().IDProperty("Id")
                        .Fields(
                            Html.X().ModelField().Name("Id").Type(ModelFieldType.Auto),
                            Html.X().ModelField().Name("AvatarUrl").Type(ModelFieldType.String),
                            Html.X().ModelField().Name("Name").Type(ModelFieldType.String),
                            Html.X().ModelField().Name("RoleNames").Type(ModelFieldType.String),
                            Html.X().ModelField().Name("FullNameFormat").Type(ModelFieldType.String)
                        )
                    )
                );
            if (value != null)
            {
                var c = component.ToComponent();
                c.Text = value.Name;
            }
            return component;
        }
        public static Container.Builder EmployeeReadOnly(this Container.Builder Container, bool readOnly)
        {
            var container1 = Container.ToComponent();
            var container2 = container1.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var container3 = container2.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var combobox = container3.Items.Where(i => i is ComboBox).Cast<ComboBox>().FirstOrDefault();
            var button = container3.Items.Where(i => i is Button).Cast<Button>().FirstOrDefault();
            if (readOnly) { combobox.ReadOnly = true; }
            if (readOnly) { button.Hidden = true; }
            return Container;
        }
        public static Container.Builder AllowBlank(this Container.Builder Container, bool allowBlank)
        {
            var container1 = Container.ToComponent();
            var container2 = container1.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var container3 = container2.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var combobox = container3.Items.Where(i => i is ComboBox).Cast<ComboBox>().FirstOrDefault();
            combobox.AllowBlank = allowBlank;
            return Container;
        }
        public static Container.Builder UrlEmployeeStore(this Container.Builder Container, string url)
        {
            var container1 = Container.ToComponent();
            var container2 = container1.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var container3 = container2.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var button = container3.Items.Where(i => i is Button).Cast<Button>().FirstOrDefault();

            var handle = button.Listeners.Click.Handler;
            var param = "params = {urlStore: '" + url + "',";
            handle = handle.Replace("params = {", param);
            button.Listeners.Click.Handler = handle;

            if (!string.IsNullOrEmpty(url))
            {
                var combobox = container3.Items.Where(i => i is ComboBox).Cast<ComboBox>().FirstOrDefault();
                var store = combobox.Store.Where(i => i is Store).Cast<Store>().FirstOrDefault();
                var ajaxProxy = store.Proxy.Where(i => i is AjaxProxy).Cast<AjaxProxy>().FirstOrDefault().ToBuilder();
                ajaxProxy.Url(url);
            }

            return Container;
        }
        public static Container.Builder Parameter(this Container.Builder Container, StoreParameter parameter)
        {
            var container1 = Container.ToComponent();
            var container2 = container1.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var container3 = container2.Items.Where(i => i is Container).Cast<Container>().FirstOrDefault();
            var button = container3.Items.Where(i => i is Button).Cast<Button>().FirstOrDefault();


            var handle = button.Listeners.Click.Handler;
            var param = string.Empty;
            if (handle.IndexOf("parameter") > 0)
            {
                param = "params = {parameter :{" + parameter + ",";
                handle = handle.Replace("params = {parameter :{", param);
            }
            else
            {
                param = "params = {parameter :{" + parameter + "},";
                handle = handle.Replace("params = {", param);
            }
            button.Listeners.Click.Handler = handle;

            if (parameter != null)
            {
                var combobox = container3.Items.Where(i => i is ComboBox).Cast<ComboBox>().FirstOrDefault();
                var store = combobox.Store.Where(i => i is Store).Cast<Store>().FirstOrDefault();
                store.Parameters.Add(parameter);
            }

            return Container;
        }
        public static RowNumbererColumn.Builder RowNumbererColumnFormat(this BuilderFactory X)
        {
            var component = X.RowNumbererColumn().Text("STT").Width(46).StyleSpec("border-left:none !important;").Align(ColumnAlign.Center);
            return component;
        }
        public static PagingToolbar.Builder PagingToolbarFormat(this BuilderFactory X, string itemselect = "25")
        {
            var items = new string[] { "5", "10", "15", "25", "45", "65", "100" };
            var component = X.PagingToolbar()
                            .PaddingSpec("2 6 2 6")
                            .EmptyMsg("Hiện không có dữ liệu")
                            .NextText("Trang kế tiếp")
                            .PrevText("Trang trước")
                            .LastText("Trang cuối cùng")
                            .FirstText("Trang đầu tiên")
                            .DisplayMsg("Hiển thị {0}-{1} của {2} bản ghi")
                            .BeforePageText("Trang")
                            .AfterPageText("của {0}")
                            .RefreshText("Tải lại dữ liệu")
                            .Items(Html.X().Label("Số bản ghi trên trang :"),
                                Html.X().ToolbarSpacer(10),
                                Html.X().ComboBox()
                                    .Width(80)
                                    .Items(items)
                                    .SelectedItems(itemselect)
                                    .Listeners(ls =>
                                    {
                                        ls.Select.Fn = "onComboBoxSelect";
                                    }
                                )
                            )
                            .Listeners(ls =>
                            {
                                ls.AfterRender.Handler = "showOrHideCustomPaging(this)";
                                ls.Change.Handler = "showOrHideCustomPaging(this)";
                            });
            return component;
        }
        public static PagingToolbar.Builder PagingToolbarShortFormat(this BuilderFactory X)
        {
            var component = X.PagingToolbar()
                .PaddingSpec("2 6 2 6")
                .NextText("")
                .PrevText("")
                .LastText("")
                .FirstText("")
                .AfterPageText("của {0}")
                .DisplayInfo(false)
                .BeforePageText("Trang");
            return component;
        }
        public static GridPanel.Builder GridPanelFormat(this BuilderFactory X)
        {
            var component = X.GridPanel()
                            .ColumnLines(true).RowLines(true).Header(false).Resizable(false).AutoScroll(true)
                            .Border(false).BodyPadding(0).BodyBorder(0).Margin(-1)
                            .View(X.GridView().TrackOver(false).LoadingText("Đang tải ..."))
                            .SelectionModel(X.RowSelectionModel().Mode(SelectionMode.Single))
                            .BottomBar(X.PagingToolbarFormat());
            return component;
        }
        public static GridPanel.Builder GridPanelPaggingShortFormat(this BuilderFactory X)
        {
            var component = X.GridPanel()
                            .ColumnLines(true).RowLines(true).Header(false).Resizable(false).AutoScroll(true)
                            .Border(false).BodyPadding(0).BodyBorder(0).Margin(-1)
                            .View(X.GridView().TrackOver(false).LoadingText("Đang tải ..."))
                            .SelectionModel(X.RowSelectionModel().Mode(SelectionMode.Single))
                            .BottomBar(X.PagingToolbarShortFormat());
            return component;
        }
        public static Column.Builder AvatarColumnFormat(this BuilderFactory X)
        {
            var render = "var tpl = \"<img src='{0}' style='height: 35px !important; width: 30px !important; border: 2px solid #E6E6E6 !important; display: inline-block !important;'/>\"; return Ext.String.format(tpl,value);";
            var component = X.Column().Renderer(render).Width(50).Filterable(false).Align(ColumnAlign.Center);
            return component;
        }
        public static Column.Builder ColumnEncodeFormat(this BuilderFactory X)
        {
            var component = X.Column().Renderer(RendererFormat.HtmlEncode)
                .Width(50)
                .Filterable(false);
            return component;
        }
        public static Column.Builder NotifyColumnFormat<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression)
        {
            var key = X.GetKey(expression).Trim('.');
            var render = "var notify = record.get('" + key + "'); return notify.length > 0 ? ('<ul style=\"float:right\" class=\"x-list-plain x-form-required-field\"><li><i style=\"color:red\" title=\"' + notify + '\" class=\"x-fa fa-exclamation-circle\"></i></li></ul>') + value : value;";
            var component = X.Column().Renderer(render);
            return component;
        }
        public static Column.Builder StatusColumnFormat<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression)
        {
            var key = X.GetKey(expression).Trim('.');
            var render = "var tpl = \"<span style=\'font-weight:600;font-size:11px;color: {0};\'> {1} </span>\"; return Ext.String.format(tpl, record.get('" + key + "'), value);";
            var component = X.Column().Renderer(render).Align(ColumnAlign.Center).Text("Trạng thái").Width(120);
            return component;
        }
        public static DateColumn.Builder DateColumnFormat(this BuilderFactory X)
        {
            var component = X.DateColumn().Format("dd/MM/yyyy");
            return component;
        }
        public static DateColumn.Builder DateTimeColumnFormat(this BuilderFactory X)
        {
            var component = X.DateColumn().Format("dd/MM/yyyy HH:mm");
            return component;
        }
        public static ImageCommandColumn.Builder ColumnFileFormat<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression)
        {
            var key = X.GetKey(expression).Trim('.');
            var keyName = X.GetKey(expression).Trim('.').Replace(".", string.Empty);
            var script = @"<script> 
                        var prepareCommandFile" + keyName + @" = function (grid, command, record, row) {
                        if (command.command == 'View' && record.data." + key + @".length == 1) {
                            command.hidden = false;
                            command.hideMode = 'display';
                        }
                        else if (command.command == 'Views' && record.data." + key + @".length > 1) {
                            command.hidden = false;
                            command.hideMode = 'display';
                        }
                    };
                </script>";
            var viewfileColumn = X.ImageCommandColumn()
                .HtmlBin(c => script);
            viewfileColumn.Align(ColumnAlign.Right);
            var view = X.ImageCommand().CommandName("View").Icon(Icon.Attach).Hidden(true).HideMode(HideMode.Display).Style("width: 100%; background-position: center;").ToolTip(tt => tt.Text = "Xem file");
            var views = X.ImageCommand().CommandName("Views").Icon(Icon.Attach).Hidden(true).HideMode(HideMode.Display).Style("width: 100%; background-position: center;").ToolTip(tt => tt.Text = "Xem danh sách file");
            viewfileColumn
                .Commands(
                view,
                views
              )
            .PrepareCommand("prepareCommandFile" + keyName)
            .Listeners(ls =>
            {
                ls.Command.Handler = "ViewFiles(command, record.data." + key + ");";
            });
            return viewfileColumn;
        }
        public static ComponentColumn.Builder ProcessColumn(this BuilderFactory X)
        {
            var component = X.ComponentColumn()
                            .Text("Tiến độ").Filterable(false).Width(120)
                            .Component(X.ProgressBar().Text("%").Margin(0))
                            .Align(ColumnAlign.Center);
            return component;
        }
        public static ComponentColumn.Builder ProcessDataIndex<TModel, TProperty>(this ComponentColumn.Builder ProcessColumn, TModel model, Expression<Func<TModel, TProperty>> expression)
        {
            var body = expression.Body.ToString();
            var dataIndex = body.Substring(body.IndexOf('.') + 1);
            ProcessColumn.Listeners(ls => ls.Bind.Handler = "cmp.updateProgress(record.get('" + dataIndex + "')/100, (record.get('" + dataIndex + "').length == 0 ? '0' : record.get('" + dataIndex + "')) + '%');");
            return ProcessColumn;
        }
        public static CycleButton.Builder CycleButtonFormat(this BuilderFactory X)
        {
            var component = X.CycleButton().ShowText(true).Height(25);
            return component;
        }
        public static CycleButton.Builder MenuSource(this CycleButton.Builder Button, object source)
        {
            var menu = iDAS.Presentation.Common.Utilities.GetSourceMenu((Dictionary<int, string>)source);
            Button.Menu(Html.X().Menu().Add(menu));
            return Button;
        }
        public static CycleButton.Builder MenuSourceByGuid(this CycleButton.Builder Button, object source)
        {
            Dictionary<Guid, string> lstItem = (Dictionary<Guid, string>)source;
            var menu = iDAS.Presentation.Common.Utilities.GetSourceMenuGuid(lstItem);
            Button.Height(32).Menu(Html.X().Menu().Add(menu));
            return Button;
        }
        public static Toolbar.Builder ToolbarHbox(this BuilderFactory X)
        {
            var component = X.Toolbar().Border(false)
                                .Defaults(X.Parameter().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                                .LayoutConfig(new HBoxLayoutConfig { Align = HBoxAlign.Middle });
            return component;
        }
        public static FieldSet.Builder FieldSetVBox(this BuilderFactory X)
        {
            var component = X.FieldSet()
                            .Padding(2)
                            .Defaults(new Parameter().ToBuilder().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                            .Layout(LayoutType.VBox)
                            .LayoutConfig(new VBoxLayoutConfig { Align = VBoxAlign.Stretch });
            return component;
        }
        public static FieldSet.Builder FieldSetHBox(this BuilderFactory X)
        {
            var component = X.FieldSet()
                            .Padding(2)
                            .Defaults(new Parameter().ToBuilder().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                            .Layout(LayoutType.HBox)
                            .LayoutConfig(new HBoxLayoutConfig { Align = HBoxAlign.Stretch });
            return component;
        }
        public static FieldSet.Builder FieldSetHBoxTop(this BuilderFactory X)
        {
            var component = X.FieldSet()
                            .Padding(2)
                            .Defaults(new Parameter().ToBuilder().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                            .Layout(LayoutType.HBox)
                            .LayoutConfig(new HBoxLayoutConfig { Align = HBoxAlign.Top });
            return component;
        }
        public static RadioGroup.Builder RadioGroupHBox(this BuilderFactory X)
        {
            var component = X.RadioGroup()
                            .Defaults(new Parameter().ToBuilder().Name("margins").Value("0 0 0 0").Mode(ParameterMode.Value))
                            .Layout(LayoutType.HBox)
                            .LayoutConfig(new HBoxLayoutConfig { Align = HBoxAlign.Stretch });
            return component;
        }
        public static TextField.Builder TextFieldDecodeFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null, bool autoDecode = true)
        {
            var value = Convert.ToString(X.GetValue(expression));
            var component = X.TextFieldFor(expression, setId, convert, format)
                          .HideLabel(true)
                          .AllowOnlyWhitespace(false)
                          .Value(autoDecode == true ? HttpUtility.HtmlDecode(value) : value);
            return component;
        }
        public static TextArea.Builder TextAreaDecodeFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null, bool autoDecode = true)
        {
            var value = Convert.ToString(X.GetValue(expression));
            var component = X.TextAreaFor(expression, setId, convert, format)
                          .HideLabel(true)
                          .Value(autoDecode == true ? HttpUtility.HtmlDecode(value) : value);
            return component;
        }
        public static Hidden.Builder HiddenDecodeFor<TModel, TProperty>(this BuilderFactory<TModel> X, Expression<Func<TModel, TProperty>> expression, bool setId = true, Func<object, object> convert = null, string format = null, bool autoDecode = true)
        {
            var value = Convert.ToString(X.GetValue(expression));
            var component = X.HiddenFor(expression, setId, convert, format)
                          .HideLabel(true)
                          .Value(autoDecode == true ? HttpUtility.HtmlDecode(value) : value);
            return component;
        }
        public static IHtmlString Required(this HtmlHelper html)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<span style='color:red'> (*)</span>");
            return MvcHtmlString.Create(sb.ToString());
        }
    }
}