﻿using Ext.Net;
using System;
using System.Collections.Generic;
using System.Data;

namespace iDAS.Presentation
{
    public static class ExtCommonMenu
    {
        public static Menu.Builder ViewMenu(this BuilderFactory X, DataTable data, Menu.Builder Menu = null, DataRow[] dataRows = null)
        {
            if (Menu == null)
            {
                Menu = X.Menu();
                var datarow = data.Select("SupervisorID is null", "Sort");
                List<AbstractComponent> MenuItems = new List<AbstractComponent>();
                foreach (var item in datarow)
                {
                    MenuItems.Add(ViewMenuItem(X, item, data));
                    MenuItems.Add(X.MenuSeparator());
                }
                Menu.Items(MenuItems);
            }
            else
            {

                List<AbstractComponent> MenuItems = new List<AbstractComponent>();
                foreach (DataRow item in dataRows)
                {
                    MenuItems.Add(ViewMenuItem(X, item, data));
                    MenuItems.Add(X.MenuSeparator());
                }
                Menu.Items(MenuItems);
            }
            Menu.Listeners(ls =>
            {
                ls.Click.Handler = @"if(menuItem.hrefTarget != null && menuItem.hrefTarget != '' && menuItem.hrefTarget != undefined){
                                            showLoadMask();
                                            var key = menuItem.itemId;
                                            var item = App.pnMainSystem.items.getByKey(key);
                                            App.pnMainSystem.items.each(function (e, d) {
                                                e.hide();
                                            });
                                            if (item != null) {
                                                item.show();
                                                hideLoadMask();
                                            }
                                            else {
                                                App.pnMainSystem.getLoader().load({ url: menuItem.hrefTarget, success: function (result) { hideLoadMask(); } });
                                            }
                                        }
                                        return false;";
            });
            return Menu;
        }
        private static MenuItem.Builder ViewMenuItem(BuilderFactory X, DataRow dataRow, DataTable data)
        {
            MenuItem.Builder items = X.MenuItem().Text(dataRow["Name"].ToString());
            bool Supervisor = (dataRow["Supervisor"] == DBNull.Value ? false : Convert.ToBoolean(dataRow["Supervisor"]));
            string Icon = (dataRow["Icon"] == DBNull.Value ? string.Empty : dataRow["Icon"].ToString());
            if (Icon != string.Empty)
            {
                items.IconCls(Icon);
            }
            if (Supervisor)
            {
                DataRow[] dataRows = data.Select("SupervisorID = " + dataRow["RoleID"]);
                items.Menu(ViewMenu(X, data, X.Menu(), dataRows));
            }
            else
            {
                items.HrefTarget(dataRow["Url"].ToString());
            }
            return items;
        }
    }
}