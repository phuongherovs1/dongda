﻿Ext.define('Gnt.examples.baseline.view.Gantt', {
    extend: 'Gnt.examples.lib.GanttPanel',
    id: 'gantt',
    requires: [
        'Gnt.column.Name'
    ],
    // enables showing baseline for tasks
    enableBaseline: true,
    // show baselines from start
    baselineVisible: true,
    viewPreset: 'weekAndDay',
    rowHeight: 50,
    eventBorderWidth: 0,
    highlightWeekends: false,

    columns: [
         {
             header: '',
             width: 50,
             dataIndex: 'ResourceImage',
             renderer: function (value) {
                 return '<img src="' + value + '" style="height: 35px !important; width: 30px !important; border: 2px solid #E6E6E6 !important; display: inline-block !important;"/>';
             }
         },
        {
            header: 'Người thực hiện',
            dataIndex: "ResourceName",
            // xtype: 'namecolumn',
            width: 210
        }
    ],
    plugins: [
        {
            // plugin that allows gantt to be panned by dragging the view around
            ptype: 'scheduler_pan'
        }
    ],
    taskBodyTemplate: '<div class="sch-gantt-progress-bar" style="width:{progressBarWidth}px;{progressBarStyle}" unselectable="on">' +
   '<span class="sch-gantt-progress-bar-label">{[Math.round(values.percentDone)]}%</span>' +
   '</div>',

    // Define an HTML template for the tooltip
    tooltipTpl: '<strong class="tipHeader">{TaskName}</strong>' +
    '<table class="taskTip">' +
    '<tr><td>Bắt đầu:</td> <td align="right">{StartDateText}</td></tr>' +
    '<tr><td>Kết thúc:</td> <td align="right">{StartEndText}</td></tr>' +
     '<tr><td>Bắt đầu theo kế hoạch:</td> <td align="right">{BaselineStartDateText}</td></tr>' +
    '<tr><td>Kết thúc theo kế hoạch:</td> <td align="right">{BaselineEndDateText}</td></tr>' +
    '<tr><td>Tiến độ:</td><td align="right">{[ Math.round(values.PercentDone)]}%</td></tr>' +
    '<tr><td>Trạng thái:</td><td align="right">{Status}</td></tr>' +
    '</table>',
    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            taskStore: taskStore

        });
        me.callParent();
    }
});