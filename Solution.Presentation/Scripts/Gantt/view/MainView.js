﻿Ext.define('Gnt.examples.assigningresources.view.MainView', {
    extend: 'Ext.container.Container',
    requires: [
        'Gnt.examples.assigningresources.view.GanttChart',
        'Gnt.examples.assigningresources.view.AssignmentGrid'
    ],
    layout: 'border',
    region: 'center',
    initComponent: function () {
        var me = this,
            gantt = Ext.widget({
                xtype: 'assigningresourcesgantt',
                title: me.tittle || 'Biểu đồ Gantt thể hiện tiến độ thực hiện công việc'
            });

        Ext.apply(me, {
            items: [
                gantt, {
                    xtype: 'assignmentgridpanel',
                    partnerTimelinePanel: gantt
                }
            ]
        });
        me.callParent();
    }
});
