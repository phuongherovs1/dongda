Ext.define('Gnt.examples.baseline.view.Timeline', {
    extend   : 'Gnt.panel.Timeline',
    xtype    : 'advanced-timeline',
    requires: ['Gnt.examples.baseline.view.ControlHeader'],
    split  : false,
    border : false,
    header   : {
        xtype : 'controlheader'
    }
});
