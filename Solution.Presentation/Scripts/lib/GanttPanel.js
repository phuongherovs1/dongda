/**
 * Gantt-panel with more features activated out of the box.
 */
Ext.define('Gnt.examples.lib.GanttPanel', {
    extend:'Gnt.panel.Gantt',

    requires: [
        
    ],

    defaultPlugins: [
    ],

    dependencyViewConfig: {
       
    },

   initComponent: function() {
       var me = this;

       me.plugins = [].concat(me.plugins || []);

       // only add plugins not already there :)
       // rebuilt to not use private addPlugin()
       Ext.each(me.defaultPlugins, function(plugin) {
           if (!me.hasPlugin(plugin)) me.plugins.push(plugin);
       });

       me.callParent();
   },

    // replacement for findPlugin, to also work before init
    hasPlugin: function(ptype) {
        return Ext.Array.some(this.plugins, function(plugin) {
            return (plugin === ptype || plugin.ptype === ptype);
        });
    }
});
