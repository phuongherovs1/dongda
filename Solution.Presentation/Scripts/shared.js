(function() {

    /* <errorlogger>
    During release we cut these lines. The examples_tracking.js file is added right to the bundle and obfuscated for online examples. */
    /* jshint ignore:start */
    //document.write('<script src="../../../ExtScheduler5.x/util/examples_tracking.js" type="text/javascript"></script>');
    /* jshint ignore:end */
    /* </errorlogger> */

    Ext.Loader.setConfig({
        enabled : true,
        paths   : {
            'Sch.locale': '/Scripts/Sch/locale',
            'Gnt.locale': '/Scripts/Gnt/locale',
            'Gnt.examples': '/Scripts',
            'Sch.examples': '/Scripts', // for DetailsPanel
            'Ext.ux'       : ''
        }
    });

    // if "bundle" specified in query string we switch to the "bundle-mode"
    // when we:
    // 1) load gnt-all-debug.js
    // 2) do not provide paths for on-demand classes loading (except for classes that are always meant to be loaded on demand, like locales)
    // 3) can load few extra bundles provided via "bundles=" request parameter
    if (document.location.href.match(/\?.*bundle/g)) {

        // include gnt-all-debug
        var bundles = ['/Scripts/gnt-all-debug.js'],
            match;

        // and extra bundle urls from "bundles" parameter (if provided)
        if (match = window.location.href.match(/bundles=(.+)/)) {
            bundles = bundles.concat(match[1].split(','));
        }

        /* jshint ignore:start */
        Ext.Array.forEach(bundles, function(url) {
            document.write('<script src="' + url + '" type="text/javascript"></script>');
        });
        /* jshint ignore:end */

    } else {
        Ext.Loader.setConfig({
            enabled : true,
            paths   : {
                'Robo'   : '../../packages/bryntum-gantt-pro/src/Robo',
                'Sch': '/Scripts/Sch',
                'Gnt': '/Scripts/Gnt',
                'Kanban' : '../../../TaskBoard2.x/lib/Kanban'
            }
        });
    }

    // override XTemplate to not hide exceptions
    Ext.XTemplate.override({ strict : true });

    // mute ARIA warnings
    Ext.ariaWarn = Ext.emptyFn;
    Ext.enableAriaPanels = false;

    Ext.require([
        // load before logging starts to avoid logging...
        'Sch.examples.lib.DetailsPanel'
    ]);

    // override loadScripts to log LOD files
    var oldLoadScripts = Ext.Loader.loadScripts;

    window.Sch = window.Sch || {};
    Sch.loadedFiles = [];

    Ext.Loader.loadScripts = function(params) {
        Sch.loadedFiles = Ext.Array.union(Sch.loadedFiles, params.url);

        return oldLoadScripts.apply(this, arguments);
    };
    // EOF override loadScripts to log LOD files

    Ext.onReady(function() {
        // add .demo class to containers to separate css in kitchensink
        Ext.getBody().addCls('demo');

        if (window.location.href.match(/^file:\/\/\//)) {
            Ext.Msg.alert('ERROR: You must use a web server', 'You must run the examples in a web server (not using the file:/// protocol)');
        }
    });
}());
