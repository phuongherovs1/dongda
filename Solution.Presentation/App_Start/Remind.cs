﻿using iDAS.Service;
using System;
using System.Timers;

namespace iDAS.Presentation.Controllers
{
    public class Remind
    {
        Timer _timer;
        //private DateTime _checkTime = DateTime.Now.AddSeconds(2);
        //public DateTime CheckTime
        //{
        //    get
        //    {
        //        return _checkTime;
        //    }
        //    set
        //    {
        //        _checkTime = value;
        //    }
        //}
        public Remind()
        {
            _timer = new System.Timers.Timer();
            _timer.Elapsed += SendTimerElapsed;
            _timer.Interval = 100 * 60 * 60;
            _timer.Start();
        }
        public void SendTimerElapsed(object sender, ElapsedEventArgs e)
        {
            //if (e.SignalTime.ToString("dd/MM/yyyy hh:mm:ss") == _checkTime.ToString("dd/MM/yyyy hh:mm:ss")) CallTask();
            CallTask();
        }
        public void CallTask()
        {
            var systemRoutineService = new SystemRoutineService();
            systemRoutineService.Perform(DateTime.Now);
        }
    }
}
