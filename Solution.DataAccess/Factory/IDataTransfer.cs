﻿using System.Data.Entity;

namespace iDAS.DataAccess
{
    public interface IDataTransfer
    {
        IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class;
    }
}
