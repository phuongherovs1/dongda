﻿using iDAS.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace iDAS.DataAccess
{
	public class VPHumanProfileAllowanceDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileAllowance, TPOCO>(context);
		}
	}

	public class AssetDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<Asset, TPOCO>(context);
		}
	}

	public class AssetGroupDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<AssetGroup, TPOCO>(context);
		}
	}

	public class AssetPropertyDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<AssetProperty, TPOCO>(context);
		}
	}

	public class AssetPropertySettingDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<AssetPropertySetting, TPOCO>(context);
		}
	}

	public class AssetResourceRelateDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<AssetResourceRelate, TPOCO>(context);
		}
	}

	public class AssetReviewDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<AssetReview, TPOCO>(context);
		}
	}

	public class AssetReviewSettingDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<AssetReviewSetting, TPOCO>(context);
		}
	}

	public class AssetTempDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<AssetTemp, TPOCO>(context);
		}
	}

	public class AssetTempSettingDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<AssetTempSetting, TPOCO>(context);
		}
	}

	public class AssetTypeDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<AssetType, TPOCO>(context);
		}
	}

	public class AssetTypePropertyDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<AssetTypeProperty, TPOCO>(context);
		}
	}

	public class BotnetInfectionDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<BotnetInfection, TPOCO>(context);
		}
	}

	public class BotnetManagerDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<BotnetManager, TPOCO>(context);
		}
	}

	public class CalendarDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<Calendar, TPOCO>(context);
		}
	}

	public class CalendarDayOverrideDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<CalendarDayOverride, TPOCO>(context);
		}
	}

	public class CalendarDayOverrideShiftDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<CalendarDayOverrideShift, TPOCO>(context);
		}
	}

	public class CalendarWeekShiftDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<CalendarWeekShift, TPOCO>(context);
		}
	}

	public class ChatGroupDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ChatGroup, TPOCO>(context);
		}
	}

	public class ChatGroupMemberDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ChatGroupMember, TPOCO>(context);
		}
	}

	public class ChatMessengerDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ChatMessenger, TPOCO>(context);
		}
	}

	public class ChatPublicDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ChatPublic, TPOCO>(context);
		}
	}

	public class ChatUserOnlineDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ChatUserOnline, TPOCO>(context);
		}
	}

	public class ConfigTableDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ConfigTable, TPOCO>(context);
		}
	}

	public class DepartmentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<Department, TPOCO>(context);
		}
	}

	public class DepartmentTitleRoleDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DepartmentTitleRole, TPOCO>(context);
		}
	}

	public class DepartmentTitleDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DepartmentTitle, TPOCO>(context);
		}
	}

	public class DispatchDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<Dispatch, TPOCO>(context);
		}
	}

	public class DispatchArchiveDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DispatchArchive, TPOCO>(context);
		}
	}

	public class DispatchAttachmentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DispatchAttachment, TPOCO>(context);
		}
	}

	public class DispatchCategoriToDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DispatchCategoriTo, TPOCO>(context);
		}
	}

	public class DispatchCategoryDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DispatchCategory, TPOCO>(context);
		}
	}

	public class DispatchRecipientsExternalDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DispatchRecipientsExternal, TPOCO>(context);
		}
	}

	public class DispatchRecipientsInternalDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DispatchRecipientsInternal, TPOCO>(context);
		}
	}

	public class DispatchResponsibilityDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DispatchResponsibility, TPOCO>(context);
		}
	}

	public class DispatchSecurityDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DispatchSecurity, TPOCO>(context);
		}
	}

	public class DispatchSendmethodDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DispatchSendmethod, TPOCO>(context);
		}
	}

	public class DispatchTaskDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DispatchTask, TPOCO>(context);
		}
	}

	public class DispatchUrgencyDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DispatchUrgency, TPOCO>(context);
		}
	}

	public class DocumentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<Document, TPOCO>(context);
		}
	}

	public class DocumentAdjustHistoryDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentAdjustHistory, TPOCO>(context);
		}
	}

	public class DocumentAdjustSuggestDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentAdjustSuggest, TPOCO>(context);
		}
	}

	public class DocumentArchiveDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentArchive, TPOCO>(context);
		}
	}

	public class DocumentAttachmentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentAttachment, TPOCO>(context);
		}
	}

	public class DocumentCategoryDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentCategory, TPOCO>(context);
		}
	}

	public class DocumentDepartmentPermissionDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentDepartmentPermission, TPOCO>(context);
		}
	}

	public class DocumentDestinationDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentDestination, TPOCO>(context);
		}
	}

	public class DocumentDestroyDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentDestroy, TPOCO>(context);
		}
	}

	public class DocumentDestroyReportDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentDestroyReport, TPOCO>(context);
		}
	}

	public class DocumentDestroyReportDetailDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentDestroyReportDetail, TPOCO>(context);
		}
	}

	public class DocumentDestroySuggestDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentDestroySuggest, TPOCO>(context);
		}
	}

	public class DocumentDestroySuggestDetailDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentDestroySuggestDetail, TPOCO>(context);
		}
	}

	public class DocumentDestroySuggestResponsibitityDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentDestroySuggestResponsibitity, TPOCO>(context);
		}
	}

	public class DocumentDistributeDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentDistribute, TPOCO>(context);
		}
	}

	public class DocumentDistributeSuggestDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentDistributeSuggest, TPOCO>(context);
		}
	}

	public class DocumentDistributeSuggestResponsibilityDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentDistributeSuggestResponsibility, TPOCO>(context);
		}
	}

	public class DocumentExternalDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentExternal, TPOCO>(context);
		}
	}

	public class DocumentExternalApplySuggestDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentExternalApplySuggest, TPOCO>(context);
		}
	}

	public class DocumentHistoryDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentHistory, TPOCO>(context);
		}
	}

	public class DocumentPermissionDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentPermission, TPOCO>(context);
		}
	}

	public class DocumentProcessDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentProcess, TPOCO>(context);
		}
	}

	public class DocumentPublishDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentPublish, TPOCO>(context);
		}
	}

	public class DocumentPublishSuggestDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentPublishSuggest, TPOCO>(context);
		}
	}

	public class DocumentReferenceDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentReference, TPOCO>(context);
		}
	}

	public class DocumentReferenceAttachmentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentReferenceAttachment, TPOCO>(context);
		}
	}

	public class DocumentReferenceTypeDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentReferenceType, TPOCO>(context);
		}
	}

	public class DocumentRequestDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentRequest, TPOCO>(context);
		}
	}

	public class DocumentRequestResponsibilityDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentRequestResponsibility, TPOCO>(context);
		}
	}

	public class DocumentRequestResultDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentRequestResult, TPOCO>(context);
		}
	}

	public class DocumentRequestResultAttachmentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentRequestResultAttachment, TPOCO>(context);
		}
	}

	public class DocumentRequestTransferDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentRequestTransfer, TPOCO>(context);
		}
	}

	public class DocumentResponsibilityDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentResponsibility, TPOCO>(context);
		}
	}

	public class DocumentRetriveDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentRetrive, TPOCO>(context);
		}
	}

	public class DocumentSecurityDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentSecurity, TPOCO>(context);
		}
	}

	public class DocumentWriteSuggestDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<DocumentWriteSuggest, TPOCO>(context);
		}
	}

	public class EmployeeDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<Employee, TPOCO>(context);
		}
	}

	public class EmployeeJobTitleDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<EmployeeJobTitle, TPOCO>(context);
		}
	}

	public class EmployeeOTDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<EmployeeOT, TPOCO>(context);
		}
	}

	public class EmployeeProfileDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<EmployeeProfile, TPOCO>(context);
		}
	}

	public class EmployeeTimeOffDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<EmployeeTimeOff, TPOCO>(context);
		}
	}

	public class EmployeeTitleDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<EmployeeTitle, TPOCO>(context);
		}
	}

	public class EmployeeWorkOutDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<EmployeeWorkOut, TPOCO>(context);
		}
	}

	public class JobTitleDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<JobTitle, TPOCO>(context);
		}
	}

	public class IPManagerDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<IPManager, TPOCO>(context);
		}
	}

	public class SmsManagerDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SmsManager, TPOCO>(context);
		}
	}

	public class FileDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<File, TPOCO>(context);
		}
	}

	public class FormApproveStatusDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<FormApproveStatus, TPOCO>(context);
		}
	}

	public class ISOProcessGraphicDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ISOProcessGraphic, TPOCO>(context);
		}
	}

	public class ISOProcessDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ISOProcess, TPOCO>(context);
		}
	}

	public class ISOProcessRelationshipDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ISOProcessRelationship, TPOCO>(context);
		}
	}

	public class ISOProcessStepDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ISOProcessStep, TPOCO>(context);
		}
	}

	public class ISOProcessStepDependencyDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ISOProcessStepDependency, TPOCO>(context);
		}
	}

	public class ISOStandardDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ISOStandard, TPOCO>(context);
		}
	}

	public class ISOTermDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ISOTerm, TPOCO>(context);
		}
	}

	public class ProfileDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<Profile, TPOCO>(context);
		}
	}

	public class ProfileAssignDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileAssign, TPOCO>(context);
		}
	}

	public class ProfileAttachmentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileAttachment, TPOCO>(context);
		}
	}

	public class ProfileBorrowDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileBorrow, TPOCO>(context);
		}
	}

	public class ProfileCategoryDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileCategory, TPOCO>(context);
		}
	}

	public class ProfileComponentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileComponent, TPOCO>(context);
		}
	}

	public class ProfileComponentAttachmentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileComponentAttachment, TPOCO>(context);
		}
	}

	public class ProfileDepartmentArchiveRoleDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileDepartmentArchiveRole, TPOCO>(context);
		}
	}

	public class ProfileDepartmentPermissionDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileDepartmentPermission, TPOCO>(context);
		}
	}

	public class ProfileDestroyDetailDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileDestroyDetail, TPOCO>(context);
		}
	}

	public class ProfileDestroyReportDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileDestroyReport, TPOCO>(context);
		}
	}

	public class ProfileDestroyReportDetailDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileDestroyReportDetail, TPOCO>(context);
		}
	}

	public class ProfileDestroySuggestDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileDestroySuggest, TPOCO>(context);
		}
	}

	public class ProfilePrincipleDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfilePrinciple, TPOCO>(context);
		}
	}

	public class ProfilePrincipleAttachmentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfilePrincipleAttachment, TPOCO>(context);
		}
	}

	public class ProfileResponsibilityDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileResponsibility, TPOCO>(context);
		}
	}

	public class ProfileSecurityDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileSecurity, TPOCO>(context);
		}
	}

	public class ProfileSuggestBorrowDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileSuggestBorrow, TPOCO>(context);
		}
	}

	public class ProfileTransferDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileTransfer, TPOCO>(context);
		}
	}

	public class ProfileTransferDetailDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileTransferDetail, TPOCO>(context);
		}
	}

	public class ProfileUpdateAssignmentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileUpdateAssignment, TPOCO>(context);
		}
	}

	public class QualityAuditMemberDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityAuditMember, TPOCO>(context);
		}
	}

	public class QualityAuditNotebookDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityAuditNotebook, TPOCO>(context);
		}
	}

	public class QualityAuditNotebookNCDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityAuditNotebookNC, TPOCO>(context);
		}
	}

	public class QualityAuditPlanDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityAuditPlan, TPOCO>(context);
		}
	}

	public class QualityAuditPlanDetailDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityAuditPlanDetail, TPOCO>(context);
		}
	}

	public class QualityAuditPlanStandardDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityAuditPlanStandard, TPOCO>(context);
		}
	}

	public class QualityAuditProgramDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityAuditProgram, TPOCO>(context);
		}
	}

	public class QualityAuditProgramDepartmentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityAuditProgramDepartment, TPOCO>(context);
		}
	}

	public class QualityAuditProgramTermDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityAuditProgramTerm, TPOCO>(context);
		}
	}

	public class QualityAuditReportDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityAuditReport, TPOCO>(context);
		}
	}

	public class QualityCAPADTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityCAPA, TPOCO>(context);
		}
	}

	public class QualityCAPADetailDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityCAPADetail, TPOCO>(context);
		}
	}

	public class QualityNCConfirmPermissionDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityNCConfirmPermission, TPOCO>(context);
		}
	}

	public class QualityTargetResponsibilityDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityTargetResponsibility, TPOCO>(context);
		}
	}

	public class QualityIssueDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityIssue, TPOCO>(context);
		}
	}

	public class QualityMeetingPlanDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityMeetingPlan, TPOCO>(context);
		}
	}

	public class QualityMeetingPlanDetailDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityMeetingPlanDetail, TPOCO>(context);
		}
	}

	public class QualityMeetingPlanNCDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityMeetingPlanNC, TPOCO>(context);
		}
	}

	public class QualityMeetingPlanReportDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityMeetingPlanReport, TPOCO>(context);
		}
	}

	public class QualityMeetingPlanReportIssueDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityMeetingPlanReportIssue, TPOCO>(context);
		}
	}

	public class QualityMeetingRecordDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityMeetingRecord, TPOCO>(context);
		}
	}

	public class QualityMeetingRecordMemberDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityMeetingRecordMember, TPOCO>(context);
		}
	}

	public class QualityNCDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityNC, TPOCO>(context);
		}
	}

	public class QualityNCRelationDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityNCRelation, TPOCO>(context);
		}
	}

	public class QualityPlanDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityPlan, TPOCO>(context);
		}
	}

	public class QualityTargetDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityTarget, TPOCO>(context);
		}
	}

	public class QualityTargetIssueDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityTargetIssue, TPOCO>(context);
		}
	}

	public class QualityTaskDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityTask, TPOCO>(context);
		}
	}

	public class QualityUnitDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<QualityUnit, TPOCO>(context);
		}
	}

	public class RiskAssetAuditDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<RiskAssetAudit, TPOCO>(context);
		}
	}

	public class RiskEventLogDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<RiskEventLog, TPOCO>(context);
		}
	}

	public class RiskHandleDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<RiskHandle, TPOCO>(context);
		}
	}

	public class RiskIncidentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<RiskIncident, TPOCO>(context);
		}
	}

	public class RiskIncidentHandlerDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<RiskIncidentHandler, TPOCO>(context);
		}
	}

	public class RiskLevelOfRiskDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<RiskLevelOfRisk, TPOCO>(context);
		}
	}

	public class RiskLikelihoodDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<RiskLikelihood, TPOCO>(context);
		}
	}

	public class RiskHandlingMethodDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<RiskHandlingMethod, TPOCO>(context);
		}
	}

	public class RiskPlanAuditDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<RiskPlanAudit, TPOCO>(context);
		}
	}

	public class RiskTargetDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<RiskTarget, TPOCO>(context);
		}
	}

	public class RiskTargetPlanDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<RiskTargetPlan, TPOCO>(context);
		}
	}

	public class RiskTargetTaskDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<RiskTargetTask, TPOCO>(context);
		}
	}

	public class SystemNotifyDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SystemNotify, TPOCO>(context);
		}
	}

	public class SystemRoleDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SystemRole, TPOCO>(context);
		}
	}

	public class SystemRoutineDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SystemRoutine, TPOCO>(context);
		}
	}

	public class SystemTitleRoleDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SystemTitleRole, TPOCO>(context);
		}
	}

	public class SystemUserDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SystemUser, TPOCO>(context);
		}
	}

	public class SystemUserLoginDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SystemUserLogin, TPOCO>(context);
		}
	}

	public class SystemUserRoleDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SystemUserRole, TPOCO>(context);
		}
	}

	public class TaskDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<Task, TPOCO>(context);
		}
	}

	public class TaskAssociateDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<TaskAssociate, TPOCO>(context);
		}
	}

	public class TaskSecurityDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<TaskSecurity, TPOCO>(context);
		}
	}

	public class TaskCategoryDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<TaskCategory, TPOCO>(context);
		}
	}

	public class TaskSourceDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<TaskSource, TPOCO>(context);
		}
	}

	public class TaskCommunicationDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<TaskCommunication, TPOCO>(context);
		}
	}

	public class TaskCommunicationFileDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<TaskCommunicationFile, TPOCO>(context);
		}
	}

	public class TaskCommunicationShareDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<TaskCommunicationShare, TPOCO>(context);
		}
	}

	public class TaskFileDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<TaskFile, TPOCO>(context);
		}
	}

	public class TaskHistoryDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<TaskHistory, TPOCO>(context);
		}
	}

	public class TaskPerformDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<TaskPerform, TPOCO>(context);
		}
	}

	public class TaskResourceDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<TaskResource, TPOCO>(context);
		}
	}

	public class TaskScheduleDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<TaskSchedule, TPOCO>(context);
		}
	}

	public class TaskSchedulePerformDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<TaskSchedulePerform, TPOCO>(context);
		}
	}

	public class VPHumanProfileAssessDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileAssess, TPOCO>(context);
		}
	}

	public class VPHumanProfileAttachmentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileAttachment, TPOCO>(context);
		}
	}

	public class VPHumanProfileAttachmentFileDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileAttachmentFile, TPOCO>(context);
		}
	}

	public class VPHumanProfileCertificateDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileCertificate, TPOCO>(context);
		}
	}

	public class VPHumanProfileContractDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileContract, TPOCO>(context);
		}
	}

	public class VPHumanProfileCuriculmViateDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileCuriculmViate, TPOCO>(context);
		}
	}

	public class VPHumanProfileDiplomaDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileDiploma, TPOCO>(context);
		}
	}

	public class VPHumanProfileDisciplineDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileDiscipline, TPOCO>(context);
		}
	}

	public class VPHumanProfileInsuranceDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileInsurance, TPOCO>(context);
		}
	}

	public class VPHumanProfileRelationshipDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileRelationship, TPOCO>(context);
		}
	}

	public class VPHumanProfileRewardDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileReward, TPOCO>(context);
		}
	}

	public class VPHumanProfileSalaryDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileSalary, TPOCO>(context);
		}
	}

	public class VPHumanProfileTrainingDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileTraining, TPOCO>(context);
		}
	}

	public class VPHumanProfileWorkExperienceDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileWorkExperience, TPOCO>(context);
		}
	}

	public class VPHumanProfileWorkTrialDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileWorkTrial, TPOCO>(context);
		}
	}

	public class VPHumanProfileWorkTrialResultDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanProfileWorkTrialResult, TPOCO>(context);
		}
	}

	public class VPHumanRecruitmentCriteriaDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanRecruitmentCriteria, TPOCO>(context);
		}
	}

	public class VPHumanRecruitmentInterviewDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanRecruitmentInterview, TPOCO>(context);
		}
	}

	public class VPHumanRecruitmentPlanDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanRecruitmentPlan, TPOCO>(context);
		}
	}

	public class VPHumanRecruitmentPlanInterviewDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanRecruitmentPlanInterview, TPOCO>(context);
		}
	}

	public class VPHumanRecruitmentPlanRequirementDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanRecruitmentPlanRequirement, TPOCO>(context);
		}
	}

	public class VPHumanRecruitmentProfileDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanRecruitmentProfile, TPOCO>(context);
		}
	}

	public class VPHumanRecruitmentProfileInterviewDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanRecruitmentProfileInterview, TPOCO>(context);
		}
	}

	public class VPHumanRecruitmentProfileResultDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanRecruitmentProfileResult, TPOCO>(context);
		}
	}

	public class VPHumanRecruitmentRequirementDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanRecruitmentRequirement, TPOCO>(context);
		}
	}

	public class VPHumanRecruitmentReviewDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanRecruitmentReview, TPOCO>(context);
		}
	}

	public class VPHumanRecruitmentTaskDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanRecruitmentTask, TPOCO>(context);
		}
	}

	public class VPHumanTrainingPlanDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanTrainingPlan, TPOCO>(context);
		}
	}

	public class VPHumanTrainingPlanDetailDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanTrainingPlanDetail, TPOCO>(context);
		}
	}

	public class VPHumanTrainingPlanRequirementDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanTrainingPlanRequirement, TPOCO>(context);
		}
	}

	public class VPHumanTrainingPractionerDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanTrainingPractioner, TPOCO>(context);
		}
	}

	public class VPHumanTrainingQuestionDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanTrainingQuestion, TPOCO>(context);
		}
	}

	public class VPHumanTrainingQuestionCategoryDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanTrainingQuestionCategory, TPOCO>(context);
		}
	}

	public class VPHumanTrainingRequirementDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanTrainingRequirement, TPOCO>(context);
		}
	}

	public class VPHumanTrainingRequirementEmployeeDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<VPHumanTrainingRequirementEmployee, TPOCO>(context);
		}
	}

	public class SmsKeywordCategoriesDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SmsKeywordCategories, TPOCO>(context);
		}
	}

	public class SmsKeywordsDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SmsKeywords, TPOCO>(context);
		}
	}

	public class SmsBlockListDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SmsBlockList, TPOCO>(context);
		}
	}

	public class SmsManagersSynchronizedDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SmsManagersSynchronized, TPOCO>(context);
		}
	}

	public class SmsTypeDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SmsType, TPOCO>(context);
		}
	}

	public class HistoryBotnetDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<HistoryBotnet, TPOCO>(context);
		}
	}

	public class KPIForTaskDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<KPIForTask, TPOCO>(context);
		}
	}

	public class KPICategoryDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<KPICategory, TPOCO>(context);
		}
	}

	public class ProfileTypeDestroyDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<ProfileTypeDestroy, TPOCO>(context);
		}
	}

	public class SmsFormContentDTO : IDataTransfer
    {
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class {
		    return new RepositoryAdapter<SmsFormContent, TPOCO>(context);
		}
	}

	public class VPHumanProfilePracticingDTO : IDataTransfer
	{
		public IRepository<TPOCO> DataTransferObject<TPOCO>(DbContext context) where TPOCO : class
		{
			return new RepositoryAdapter<VPHumanProfilePracticing, TPOCO>(context);
		}
	}
}