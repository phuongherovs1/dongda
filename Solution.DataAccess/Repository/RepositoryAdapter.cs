﻿using iDAS.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace iDAS.DataAccess
{
    class RepositoryAdapter<TSource, TDest> : IRepository<TDest>
        where TSource : class, IEntity
        where TDest : class
    {
        private Repository<TSource> repository;
        private Mapper mapper;
        public RepositoryAdapter(DbContext context)
        {
            this.repository = new Repository<TSource>(context);
            this.mapper = new Mapper();
        }
        public IEnumerable<TDest> GetAll(bool allowDeleted = false)
        {
            var data = repository.Get(allowDeleted);
            var result = mapper.MapMulti<TSource, TDest>(data);
            return result;
        }
        public IEnumerable<TDest> Get(
            Expression<Func<TDest, bool>> filter = null,
            Func<IQueryable<TDest>, IQueryable<TDest>> include = null,
            Func<IQueryable<TDest>, IOrderedQueryable<TDest>> orderBy = null,
            Func<IQueryable<TDest>, IOrderedQueryable<TDest>> orderByDescending = null,
            bool allowDeleted = false)
        {
            IQueryable<TDest> query = mapper.MapMulti<TSource, TDest>(repository.Get(allowDeleted)).AsQueryable();
            if (filter != null)
            {
                query = query.Where(filter);
            }
            if (include != null)
            {
                query = include(query);
            }
            if (orderBy != null)
            {
                query = orderBy(query);
            }
            if (orderByDescending != null)
            {
                query = orderByDescending(query);
            }
            return query;
        }
        public IQueryable<TDest> Get(bool allowDeleted = false)
        {
            var data = repository.Get(allowDeleted);
            IQueryable<TDest> query = mapper.MapQuery<TSource, TDest>(data);
            return query;
        }
        public IEnumerable<TDest> GetDataByStringQuery(string query)
        {
            var data = repository.GetDataByStringQuery(query);
            var result = mapper.MapQuery<TSource, TDest>(data);
            return result;
        }
        public TDest GetById(Guid id, bool allowDeleted = false)
        {
            var entity = repository.GetById(id, allowDeleted);
            return mapper.MapSingle<TSource, TDest>(entity);
        }
        public TDest GetJustById(Guid id)
        {
            var entity = repository.GetJustById(id);
            return mapper.MapSingle<TSource, TDest>(entity);
        }
        public IEnumerable<TDest> GetByIds(IEnumerable<Guid> ids, bool allowDeleted = false)
        {
            var entities = repository.GetByIds(ids, allowDeleted);
            return mapper.MapMulti<TSource, TDest>(entities);
        }
        public IQueryable<TDest> GetByIds(IQueryable<Guid> ids, bool allowDeleted = false)
        {
            var entities = repository.GetByIds(ids, allowDeleted);
            return mapper.MapQuery<TSource, TDest>(entities);
        }
        public IEnumerable<TDest> GetNotInIds(IEnumerable<Guid> ids, bool allowDeleted = false)
        {
            var entities = repository.GetNotInIds(ids, allowDeleted);
            return mapper.MapMulti<TSource, TDest>(entities);
        }
        public IQueryable<TDest> GetNotInIds(IQueryable<Guid> ids, bool allowDeleted = false)
        {
            var entities = repository.GetNotInIds(ids, allowDeleted);
            return mapper.MapQuery<TSource, TDest>(entities);
        }
        public Guid Insert(TDest data)
        {
            var entity = mapper.MapSingle<TDest, TSource>(data);
            repository.Insert(entity);
            return entity.Id;
        }
        public IEnumerable<Guid> InsertRange(IEnumerable<TDest> data)
        {
            var entities = mapper.MapMulti<TDest, TSource>(data);
            repository.InsertRange(entities);
            return entities.Select(i => i.Id);
        }
        public void Update(TDest data, bool ignoreValueNull = true)
        {
            var entity = mapper.MapSingle<TDest, TSource>(data);
            repository.Update(entity, ignoreValueNull);
        }
        public void Delete(Guid id)
        {
            repository.Delete(id);
        }
        public void DeleteRange(IEnumerable<Guid> ids)
        {
            repository.DeleteRange(ids);
        }
        public void Remove(Guid id)
        {
            repository.Remove(id);
        }
        public void RemoveRange(IEnumerable<Guid> ids)
        {
            repository.RemoveRange(ids);
        }
        public void Revert(Guid id)
        {
            repository.Revert(id);
        }
        public void RevertRange(IEnumerable<Guid> ids)
        {
            repository.RevertRange(ids);
        }
        public void SaveChanges()
        {
            repository.Save();
        }
        public Task<int> SaveChangesAsync()
        {
            return repository.SaveAsync();
        }
        public Guid GetUserId()
        {
            return repository.UserId;
        }
        public IEnumerable<TDest> Page(IEnumerable<TDest> data, int pageIndex, int pageSize)
        {
            return data.Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }
        public IQueryable<TDest> Page(IQueryable<TDest> data, int pageIndex, int pageSize)
        {
            return data.Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }

    }
}
