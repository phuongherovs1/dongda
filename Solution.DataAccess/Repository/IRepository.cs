﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace iDAS.DataAccess
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetDataByStringQuery(string query);
        IEnumerable<T> GetAll(bool allowDeleted = false);
        IEnumerable<T> Get(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IQueryable<T>> include = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderByDescending = null,
            bool allowDeleted = false);
        IQueryable<T> Get(bool allowDeleted = false);
        T GetById(Guid id, bool allowDeleted = false);
        IEnumerable<T> GetByIds(IEnumerable<Guid> ids, bool allowDeleted = false);
        T GetJustById(Guid ids);
        IQueryable<T> GetByIds(IQueryable<Guid> ids, bool allowDeleted = false);
        IEnumerable<T> GetNotInIds(IEnumerable<Guid> ids, bool allowDeleted = false);
        IQueryable<T> GetNotInIds(IQueryable<Guid> ids, bool allowDeleted = false);
        Guid Insert(T data);
        IEnumerable<Guid> InsertRange(IEnumerable<T> data);
        void Update(T data, bool ignoreValueNull = true);
        void Delete(Guid id);
        void DeleteRange(IEnumerable<Guid> ids);
        void Remove(Guid id);
        void RemoveRange(IEnumerable<Guid> ids);
        void Revert(Guid id);
        void RevertRange(IEnumerable<Guid> ids);
        void SaveChanges();

        Task<int> SaveChangesAsync();
        Guid GetUserId();
        IEnumerable<T> Page(IEnumerable<T> data, int pageIndex, int pageSize);
        IQueryable<T> Page(IQueryable<T> data, int pageIndex, int pageSize);
    }
}
