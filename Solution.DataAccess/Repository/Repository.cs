﻿using iDAS.DataModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace iDAS.DataAccess
{
    class Repository<TEntity> where TEntity : class, IEntity
    {
        private DbContext context;
        private DbSet<TEntity> dbSet;
        public Repository() { }
        public Repository(DbContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }
        public virtual IQueryable<TEntity> Get(bool allowDeleted = false)
        {
            IQueryable<TEntity> query = dbSet;
            if (!allowDeleted)
                query = query.Where(i => i.IsDelete != true);
            else
                query = query.Where(i => i.IsDelete == true);
            return query;
        }
        public virtual TEntity GetById(Guid id, bool allowDeleted = false)
        {
            var entity = dbSet.Find(id);
            if (!allowDeleted && entity != null)
                entity = entity.IsDelete != true ? entity : null;
            return entity;
        }
        public virtual TEntity GetJustById(Guid id)
        {
            var entity = dbSet.Find(id);
            if (entity == null)
                return null;
            return entity;
        }
        public virtual IQueryable<TEntity> GetDataByStringQuery(string query)
        {
            var entity = dbSet.SqlQuery(query).AsQueryable();
            if (entity == null)
                return null;
            return entity;
        }
        public virtual IEnumerable<TEntity> GetByIds(IEnumerable<Guid> ids, bool allowDeleted = false)
        {
            var query = Get(allowDeleted);
            //return query.Where(i => ids.Any(z => z == i.Id));
            return query.Where(i => ids.Contains(i.Id));
        }
        public virtual IQueryable<TEntity> GetByIds(IQueryable<Guid> ids, bool allowDeleted = false)
        {
            var query = Get(allowDeleted);
            // query = query.Where(i => ids.Any(z => z == i.Id));
            return query.Where(i => ids.Contains(i.Id));
        }
        public virtual IEnumerable<TEntity> GetNotInIds(IEnumerable<Guid> ids, bool allowDeleted = false)
        {
            var query = Get(allowDeleted);
            return query.Where(i => !ids.Contains(i.Id));
        }
        public virtual IQueryable<TEntity> GetNotInIds(IQueryable<Guid> ids, bool allowDeleted = false)
        {
            var query = Get(allowDeleted);
            return query.Where(i => !ids.Contains(i.Id));
        }
        public virtual void Insert(TEntity entity)
        {
            setInfoEntity(entity);
            dbSet.Add(entity);
        }
        public virtual void InsertRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                setInfoEntity(entity);
            }
            dbSet.AddRange(entities);
        }
        public virtual void Update(TEntity entity, bool ignoreValueNull = true)
        {
            var item = dbSet.Local.FirstOrDefault(i => i.Id == entity.Id);
            if (item != null)
                context.Entry(item).State = EntityState.Detached;

            dbSet.Attach(entity);
            setInfoEntity(entity, false, null);
            var entry = context.Entry(entity);

            if (!ignoreValueNull)
            {
                entry.State = EntityState.Modified;
            }
            else
            {
                foreach (var property in entry.OriginalValues.PropertyNames)
                {
                    var value = entry.OriginalValues.GetValue<object>(property);
                    if (value != null)
                    {
                        entry.Property(property).IsModified = true;
                        if (value is DateTime && Convert.ToDateTime(value) == DateTime.MinValue)
                        {
                            entry.Property(property).CurrentValue = null;
                        }
                        if (value is Guid && (Guid)value == Guid.Empty)
                        {
                            entry.Property(property).CurrentValue = null;
                        }
                    }
                }
            }
        }
        public virtual void Delete(Guid id)
        {
            var entity = dbSet.Find(id);
            setInfoEntity(entity, false, true);
        }
        public virtual void DeleteRange(IEnumerable<Guid> ids)
        {
            var entities = dbSet.Where(i => ids.Contains(i.Id));
            foreach (var entity in entities)
            {
                setInfoEntity(entity, false, true);
            }
        }
        public virtual void Remove(Guid id)
        {
            var entity = dbSet.Find(id);
            dbSet.Remove(entity);
        }
        public virtual void RemoveRange(IEnumerable<Guid> ids)
        {
            var entities = dbSet.Where(i => ids.Contains(i.Id));
            dbSet.RemoveRange(entities);
        }
        public virtual void Revert(Guid id)
        {
            TEntity entity = dbSet.Find(id);
            setInfoEntity(entity, false, false);
        }
        public virtual void RevertRange(IEnumerable<Guid> ids)
        {
            var entities = dbSet.Where(i => ids.Contains(i.Id));
            foreach (var entity in entities)
            {
                setInfoEntity(entity, false, false);
            }
        }

        //public virtual void BulkInsert(IEnumerable<TEntity> entities)
        //{
        //    int count = 0;
        //    foreach (var entity in entities)
        //    {
        //        setInfoEntity(entity);
        //        ++count;
        //        dbSet.Add(entity);
        //        if (count % 100 == 0)
        //        {
        //            context.SaveChanges();

        //            context = new MyDbContext();
        //            context.Configuration.AutoDetectChangesEnabled = false;
        //        }
        //    }
        //    dbSet.AddRange(entities);
        //}
        public virtual void Save()
        {
            context.SaveChanges();
        }
        public virtual Task<int> SaveAsync()
        {
            return context.SaveChangesAsync();
        }
        private void setInfoEntity(TEntity entity, bool IsNew = true, bool? IsDelete = null)
        {
            if (IsNew)
            {
                if (entity.Id == null || entity.Id == Guid.Empty)
                    entity.Id = Guid.NewGuid();
                entity.CreateAt = DateTime.Now;
                entity.CreateBy = UserId;
            }
            else
            {
                entity.UpdateAt = DateTime.Now;
                entity.UpdateBy = UserId;
            }
            if (IsDelete != null)
                entity.IsDelete = IsDelete;
        }
        private Guid userId;
        public Guid UserId
        {
            get
            {
                if (userId == Guid.Empty)
                {
                    var data = HttpContext.Current != null ? HttpContext.Current.User.Identity.GetUserId() : null;
                    if (data != null)
                        userId = Guid.Parse(data);
                    else
                        userId = Guid.Empty;
                }
                return userId;
            }
        }
    }
}