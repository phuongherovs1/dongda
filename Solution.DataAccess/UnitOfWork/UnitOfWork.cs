﻿using iDAS.DataModel;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Web;

namespace iDAS.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private string contextKey = "DBCONTEXT";
        private DbContext context;
        protected DbContext Context
        {
            get
            {
                if (context == null)
                {
                    if (HttpContext.Current != null)
                    {
                        if (!HttpContext.Current.Items.Contains(contextKey))
                        {
                            context = new SolutionEntities();
                            HttpContext.Current.Items[contextKey] = context;
                        }
                        else
                        {
                            context = HttpContext.Current.Items[contextKey] as DbContext;
                        }
                    }
                    else
                    {
                        context = new SolutionEntities();
                    }
                }
                return context;
            }
            set
            {
                context = value;
            }
        }
        public void SetDbContext(IUnitOfWork unitOfWork)
        {
            this.Context = (unitOfWork as UnitOfWork).Context;
        }
        public bool MigrationDbContext(string connectionString)
        {
            var success = true;
            try
            {
                var connectionInfo = new DbConnectionInfo(connectionString, "System.Data.SqlClient");
                var migrationConfig = new DbMigrationsConfiguration<SolutionEntities>();
                migrationConfig.AutomaticMigrationDataLossAllowed = true;
                migrationConfig.AutomaticMigrationsEnabled = true;
                migrationConfig.TargetDatabase = connectionInfo;
                var migrator = new DbMigrator(migrationConfig);
                migrator.Update();
            }
            catch
            {
                success = false;
            }
            return success;
        }
    }

    public class UnitOfWork<TDTO, TPOCO> : UnitOfWork, IUnitOfWork<TPOCO>
        where TDTO : IDataTransfer
        where TPOCO : class
    {
        private IRepository<TPOCO> repository;
        public IRepository<TPOCO> Repository
        {
            get
            {
                return repository ?? (repository = Activator.CreateInstance<TDTO>().DataTransferObject<TPOCO>(Context));
            }
        }
    }
}
