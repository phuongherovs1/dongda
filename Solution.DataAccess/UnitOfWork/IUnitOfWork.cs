﻿namespace iDAS.DataAccess
{
    public interface IUnitOfWork
    {
        void SetDbContext(IUnitOfWork unitOfWork);
    }
    public interface IUnitOfWork<TPOCO> : IUnitOfWork where TPOCO : class
    {
        IRepository<TPOCO> Repository { get; }
    }
}
