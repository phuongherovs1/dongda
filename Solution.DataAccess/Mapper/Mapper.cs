﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System.Collections.Generic;
using System.Linq;

namespace iDAS.DataAccess
{
    class Mapper
    {
        public IQueryable<TDest> MapQuery<TSource, TDest>(IQueryable<TSource> source)
        {
            var config = getConfig<TSource, TDest>();
            return source.ProjectTo<TDest>(config);
        }
        public IQueryable<TDest> MapQuery<TSource, TDest>(IQueryable<TSource> source, IQueryable<TDest> dest)
        {
            var config = getConfig<TSource, TDest>();
            return source.Map(dest, config);
        }
        public TDest MapSingle<TSource, TDest>(TSource source)
        {
            return getMapper<TSource, TDest>().Map<TDest>(source);
        }
        public IEnumerable<TDest> MapMulti<TSource, TDest>(IEnumerable<TSource> source)
        {
            return getMapper<TSource, TDest>().Map<IEnumerable<TDest>>(source);
        }
        private IMapper getMapper<TSource, TDest>()
        {
            return getConfig<TSource, TDest>().CreateMapper();
        }
        private IConfigurationProvider getConfig<TSource, TDest>()
        {
            return new MapperConfiguration(cfg => { cfg.AllowNullCollections = true; cfg.CreateMap<TSource, TDest>(); });
        }
    }
}
